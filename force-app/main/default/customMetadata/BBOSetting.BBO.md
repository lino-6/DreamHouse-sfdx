<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BBO</label>
    <protected>false</protected>
    <values>
        <field>Enabled__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Production_Authorization_Token__c</field>
        <value xsi:type="xsd:string">eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIn0.TxRstvLwtIS9ASMoFYGIgiGW_fgFXAX2uR_S5gR3Em8xaPmTiF6mlg.OFr9TYyl8V-Af6PLz_O_MQ.-4U7RO5BhOicRdGZNgAUPWJAtnxUA-33B6hi0WkuTX_d-j_GmrkN-HL6CIMo9TrfiK2qIsZvXQP9k3KNOBPWroEzxYRQKWsX8nHbuFRA-hKNIvN5UCebka9RcsBqzGkakFSqISMQ-CjFFJtx6y1dg4f5eN4xRjx5rTpFveSsWDgz9aU-iLJ7KCYvv3zPhCQ7rOsBIFKHAAjkmfpV3u0-R_5ezwKt6P7ko60vKHoxTrgzPu8Yv3y6yaCGi64dge-exbHhkEMQIPcRFZh5ImnXNk79SbPplQOZrAIV6-j0wNw-ViQz89cDtl25T6D8BQmbE_8U_nXLFBrCk2sYAcnWoRset5xrzCkKZZiOKfVaDKhmhCI3I8gzhlUhaywztHoqy-mEthCTWYGenRz6musK_5OidktBU6wUk82Ya4azdOhNWWBxysgzNEXfcoEDM-Ux25H5XL89onBNguyj_rTH62ZMLwa_DQ_D73mslZ-ASGnl7PAdlhj7wTQDAaHQBmJh7O1MxA8yklcAtqn4psqNUq4ojGp77uvgK4W9wNuNAkk.q1lYVjSCob2d-cavYhj3KA</value>
    </values>
    <values>
        <field>Production_Environment__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Production_Organisation_ID__c</field>
        <value xsi:type="xsd:string">c8fTGKPVda2Oj21aJDOaa040mtMhagB</value>
    </values>
    <values>
        <field>Production_Service_URL__c</field>
        <value xsi:type="xsd:string">https://smartsignals2.smart-digital-solutions.de/api/v1/CustomerEncryption</value>
    </values>
    <values>
        <field>Staging_Authorization_Token__c</field>
        <value xsi:type="xsd:string">eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIn0.sC-esT8Aww0EBaefC0h4cJQLxakPSjU_f-0DOCxMVVBpX18_QAF1vA.klUiYvbdA0tbrWFRh4KOSw.jbdGSt0oBUAcfhaTf2TsCWG4c69hrUTk9tHqtTwNO_g-rtuIuwltrR_4AFpeo6_0CdQSVV0BAQQ3e4yt8iFbto4D_CWLOHlwTQS5Q41Xvklt422Q5LsB-IKAdF8IEV2PJyw_L-Iewsn68IJeBYuzTS5GmNwRFgtnlHuw-eHp-KPP3-qy2_J_M_tLIXPZ9IoNcd6l6CetjxEdyekValD5KgPO3IVCy2maufaIy7P3O_pgiW47AR8yK_rpkqffGkFKocV_i7s1wdpL2OUsixBM0L_vJJoTiJ5bgsfCrYsh1u64Rch7xHDQXGGCS9Pyf3tMV6koFHFrV4QsnKOwEfQRpso4zb3SgA7fiOayqPNBo1Mr82lkgfYzrUzbDwxzGIrvKMxjIpn91sxDzuHGF23SGingwHgfFVtPOrid9pjV03kebwN0eDpd9oaLjVE70JgryMblrVTt9wJ_T0tP68L7ti1uazcxJ1BdZWzw4LPUYBoCz6-iqrF5MCd3opb16J41LgJpIVuBAnVdO5p3gZKYa1_zkHaH5Mudvc54DSoHGh0.JaV3Iiv9zUwXXFMpRha1mw</value>
    </values>
    <values>
        <field>Staging_Organisation_ID__c</field>
        <value xsi:type="xsd:string">xSuBFMMIlagmlVqLztH5gu20A0UzZTS</value>
    </values>
    <values>
        <field>Staging_Service_URL__c</field>
        <value xsi:type="xsd:string">https://smasi2-stage.smart-digital-solutions.de/api/v1/CustomerEncryption</value>
    </values>
</CustomMetadata>
