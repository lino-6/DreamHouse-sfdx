<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>dsfs__DocuSign_Admin</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>DocuSign For Salesforce</label>
    <logo>dsfs__DocuSign_Images/dsfs__docusign_for_salesforce_logo.png</logo>
    <tabs>dsfs__DocuSign_Status__c</tabs>
    <tabs>dsfs__DocuSign_Admin</tabs>
    <tabs>standard-Opportunity</tabs>
    <tabs>standard-Account</tabs>
    <tabs>standard-Contact</tabs>
    <tabs>standard-Lead</tabs>
    <tabs>standard-Contract</tabs>
</CustomApplication>
