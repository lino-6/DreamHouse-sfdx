@isTest
public class VGA_SendBatchSMSTracker 
{
    @testsetup
    Static void insertData()
    {
        
        list<VGA_MuleUrl__c> customsettingdata =   VGA_CommonTracker.buildCustomSettingforMule();
        insert customsettingdata;
        
        Lead    objLead = new Lead();
        objLead.VGA_Brand__c = 'Skoda';  
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.VGA_Primary_id__c = '1234';
        objLead.postalcode = '1234';
        insert objLead; 
        
        Lead    objLead1 = new Lead();
        objLead1.VGA_Brand__c = 'Volkswagen';  
        objLead1.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead1.VGA_Model_of_Interest__c = 'abc';
        objLead1.FirstName  = 'TestLeadFirst';
        objLead1.LastName = 'TestLeadLastName';
        objLead1.Company = 'TestCompany';
        objLead1.Status = 'New';
        objLead1.VGA_Campaign_Source__c = 'Web Site';
        objLead1.VGA_Primary_id__c = '1234';
        objLead1.postalcode = '1234';
        insert objLead1; 
        
        
        VGA_Dealer_Notification__c   objDealerNotification = new VGA_Dealer_Notification__c();
        objDealerNotification.VGA_Inserted_From_Batch__c = true;
        objDealerNotification.recordtypeID = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
        objDealerNotification.VGA_Sent_To__c = '+917053315190';
        objDealerNotification.VGA_Message_Body__c = 'Hi';
        objDealerNotification.VGA_Lead__c = objLead.id;
        
        insert objDealerNotification;
        VGA_Dealer_Notification__c   objDealerNotification1 = new VGA_Dealer_Notification__c();
        objDealerNotification1.VGA_Inserted_From_Batch__c = true;
        objDealerNotification1.recordtypeID = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
        objDealerNotification1.VGA_Sent_To__c = '+917053315190';
        objDealerNotification1.VGA_Message_Body__c = 'Hi';
        objDealerNotification1.VGA_Lead__c = objLead1.id;
        
        insert objDealerNotification1;
    } 
    
    static testmethod void unitTest()
    {
        VGA_SendBatchSMS obj = new VGA_SendBatchSMS();
        
        Database.executeBatch(obj);
        
    }
    static testmethod void ScheduleTest()
    {
        VGA_ScheduleSendBatchSMS obj = new VGA_ScheduleSendBatchSMS();
        
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Test Territory Check', sch, obj);
        Test.stopTest(); 
    }
    
    
}