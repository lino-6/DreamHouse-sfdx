public class Muleapi 
{    
    
    public String email_address = '';
    public String first_name = '';
    public String last_name = '';
    public String home_phone = '';
    public String mobile_phone = '';
    public String address1 = '';
    public String city = '';
    public String post_code = '';
    public String region_code = '';
    public String brand = '';
    public String company_name = '';
    public String phone = '';
    public String ABN = '';
    public String area = ''; 
    public String region = '';
    
    
    
    // Constructor
    public Muleapi() {}
 //   public Muleapi(contactUsController contactUs) {}
    
    public String personAccountSearch() 
    {
    
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
    
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-type', 'application/json');
        
         if(this.brand=='Volkswagen')
        {
        
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);  
        }
        else
        {
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Skoda').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Skoda').Secret_Id__c);  
        }

        req.setEndpoint('callout:Mule_Account_Search/getMatchScoreAccount');
       //req.setEndpoint('https://vga-services-uat.au-s1.cloudhub.io/v1/getMatchScoreAccount');
      
        req.setMethod('POST');  

        req.setBody('{"first_name":"' + this.first_name + '", ' 
                    + '"last_name":"'+ this.last_name + '", '
                    + '"home_phone":"'+ this.home_phone + '", '
                    + '"mobile_phone":"'+ this.mobile_phone + '", '
                    + '"email_address":"'+ this.email_address + '", '
                    + '"address1":"'+ this.address1 + '", '
                    + '"city":"'+ this.city + '", '
                    + '"post_code":"'+ this.post_code + '", '
                    + '"region_code":"'+ this.region_code + '", '
                    + '"brand":"'+ this.brand + '" }');  

        try 
        {
            res = http.send(req);
            String body = res.getBody();
            System.debug(body);
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(body);
            String primary_id = (String)m.get('primary_id');
         //   System.debug(primary_id);
            return primary_id;
        } catch(System.CalloutException e) 
        {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
        
        return '';
    }
    
    public String businessAccountSearch() 
    {
    
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
    
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-type', 'application/json');
        
        
        if(this.brand=='Volkswagen')
        {
        
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);  
        }
        else
        {
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Skoda').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Skoda').Secret_Id__c);  
        }

        req.setEndpoint('callout:Mule_Account_Search/getMatchScoreBAccount');
        //req.setEndpoint('https://vga-services-uat.au-s1.cloudhub.io/v1/getMatchScoreBAccount');
        req.setMethod('POST');  

        req.setBody('{"email_address":"' + this.email_address + '", ' 
                    + '"company_name":"'+ this.company_name + '", '
                    + '"phone":"'+ this.phone + '", '
                    + '"ABN":"'+ this.ABN + '", '
                    + '"address1":"'+ this.address1 + '", '
                    + '"city":"'+ this.city + '", '
                    + '"post_code":"'+ this.post_code + '", '
                    + '"region_code":"'+ this.region_code + '", '
                    + '"brand":"'+ this.brand + '" }');  

        try 
        {
            res = http.send(req);
            String body = res.getBody();
            System.debug(body);
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(body);
            String primary_id = (String)m.get('primary_id');
         //   System.debug(primary_id);
            return primary_id;
        } catch(System.CalloutException e) 
        {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
        
        return '';
    }    
    
    public String businessContactSearch() 
    {
    
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
    
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-type', 'application/json');
      
        if(this.brand=='Volkswagen')
        {
        
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);  
        }
        else
        {
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Skoda').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Skoda').Secret_Id__c);  
        }
          
        req.setEndpoint('callout:Mule_Account_Search/getMatchScoreBAccount');
        // req.setEndpoint('https://vga-services-uat.au-s1.cloudhub.io/v1/getMatchScoreBAccount');
        req.setMethod('POST');  

        req.setBody('{"email_address":"' + this.email_address + '", ' 
                    + '"company_name":"'+ this.company_name + '", '
                    + '"phone":"'+ this.phone + '", '
                    + '"ABN":"'+ this.ABN + '", '
                    + '"first_name":"' + this.first_name + '", ' 
                    + '"last_name":"'+ this.last_name + '", '
                    + '"address1":"'+  + '", '
                    + '"city":"'+  + '", '
                    + '"post_code":"'+  + '", '
                    + '"region_code":"'+  + '", '
                    + '"brand":"'+ this.brand + '" }');  

        try 
        {
            res = http.send(req);
            String body = res.getBody();
            System.debug(body);
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(body);
            String contact_id = (String)m.get('contact_id');
         //   System.debug(primary_id);
            return contact_id;
        } catch(System.CalloutException e) 
        {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
        
        return '';
    }    
    
 
       
}