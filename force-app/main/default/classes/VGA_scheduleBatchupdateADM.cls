global class VGA_scheduleBatchupdateADM implements Schedulable
{
	global void execute(SchedulableContext sc)
    {
        VGA_BatchUpdateAllianzDataMapping obj = new VGA_BatchUpdateAllianzDataMapping();
        Database.executeBatch(obj,10);
    }
}