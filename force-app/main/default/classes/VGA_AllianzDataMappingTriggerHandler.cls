/*
   
    Change history:
   
    Date: 2018-05-15
    Modified By: Alfahad 
    Change: Added update logic to set VGA_Transaction_Code__c value.
      
              
   */

public with sharing class VGA_AllianzDataMappingTriggerHandler 
{
    public void runTrigger()
    {
        /*
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<VGA_Allianz_Data_Mapping__c>) trigger.new);
        }
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<VGA_Allianz_Data_Mapping__c>) trigger.new, (Map<Id, VGA_Allianz_Data_Mapping__c>) trigger.OldMap);
        }   
*/
    }
    /*
    public void onBeforeInsert(List<VGA_Allianz_Data_Mapping__c> lstTriggerNew)
    {
    updateLastModifiedDate(lstTriggerNew,null);
    }
    public void onBeforeUpdate(List<VGA_Allianz_Data_Mapping__c> lstTriggerNew, Map<Id,VGA_Allianz_Data_Mapping__c> triggeroldmap)
    {
        updateLastModifiedDate(lstTriggerNew,triggeroldmap);  
    }
    
    private static void updateLastModifiedDate(list<VGA_Allianz_Data_Mapping__c> lsttriggernew,map<Id,VGA_Allianz_Data_Mapping__c> triggeroldmap)
    {
        if(lsttriggernew !=  null && !lsttriggernew.isEmpty())
        {
            for(VGA_Allianz_Data_Mapping__c obj : lsttriggernew)
            {
                obj.VGA_Last_Modified_Date_Time__c = system.now();
                
                if(triggeroldmap != null && triggeroldmap.containskey(obj.id)) 
                {
                    // obj.VGA_Transaction_Code__c = 'C';
                }  
            }
        }
    }
*/
}