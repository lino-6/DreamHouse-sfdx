@isTest
global class VGA_ResetDealerContactPasswordSOAPTest implements WebServiceMock {

    global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           VGA_ResetDealerContactPasswordSOAP.resetPasswordResponse_element respElement = 
               new VGA_ResetDealerContactPasswordSOAP.resetPasswordResponse_element();
           
           new VGA_ResetDealerContactPasswordSOAP.address();  
           new VGA_ResetDealerContactPasswordSOAP.DebuggingHeader_element();
           new VGA_ResetDealerContactPasswordSOAP.LogInfo();        
           new VGA_ResetDealerContactPasswordSOAP.location();        
                   
           respElement.result = true;
           response.put('response_x', respElement); 
       }    
    
    @isTest static void test1() {              
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new VGA_ResetDealerContactPasswordSOAPTest());
        
        VGA_ResetDealerContactPasswordSOAP.VGA_ResetDealerContactPassword stub = new VGA_ResetDealerContactPasswordSOAP.VGA_ResetDealerContactPassword();
        // Call the method that invokes a callout
        Boolean output = stub.resetPassword('123', '123');
        
        // Verify that a fake result is returned
        System.assertEquals(true, output); 
    }    
    
}