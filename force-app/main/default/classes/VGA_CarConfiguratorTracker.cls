@isTest
private class VGA_CarConfiguratorTracker {
    
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c CustomTrigger =new VGA_Triggers__c();
        CustomTrigger = VGA_CommonTracker.buildCustomSettingTrigger('Product2',true);
        insert CustomTrigger; 
        list<VGA_MuleUrl__c> vwobj =   VGA_CommonTracker.buildCustomSettingforMule();
        
        insert vwobj;
        
        list<VGA_Active_Brochure__c> aclist=new list<VGA_Active_Brochure__c>();
        
        VGA_Active_Brochure__c actb=VGA_CommonTracker.createActiveBrochure();
        actb.Name='GOLF';
        actb.VGA_Brand__c='Volkswagen';
        actb.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
        actb.VGA_Brochure_Label__c = 'Golf';
        
        
        VGA_Active_Brochure__c actb1=VGA_CommonTracker.createActiveBrochure();
        actb1.Name='tiguan';
        actb1.VGA_Brand__c='Volkswagen';
        actb1.VGA_Sub_Brand__c='Commercial Vehicles (CV)';
        actb1.VGA_Brochure_Label__c='tiguan';
        aclist.add(actb);
        aclist.add(actb1); 
        insert aclist;
        Product2 objModel = new product2();
        objModel.name='Test';
        objModel.VGA_Model_Alias__c = 'Golf';
        objModel.ProductCode = 'BQ14NZ/18';
        Insert objModel;
        
        Product2 objModel1 = new product2();
        objModel1.name='Test1';
        objModel1.VGA_Model_Alias__c = 'tiguan';
        objModel1.ProductCode = 'AD148T/18';
        Insert objModel1;
        
        VGA_MuleUrl__c vwset=new VGA_MuleUrl__c();
       vwset.Name='Volkswagen';
       vwset.Client_ID__c='12121212121';
       vwset.Secret_Id__c='1212';
       insert vwset;
        
        
    }
    
    /**
    * @author Ashok Chandra
    * @date 17/10/2018
    * @description Method that tests the VGA_CarConfigurator class.
    *              It makes sure a new Lead is created with brand Volkswagen
    *       and Sub brand Passenger Vehicles (PV).
    *               
    
    */
    @isTest static void testCallout() {
        
        Test.startTest();
        // Set mock callout class
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        string ddurl = 'http:testurl/en/golf/30316';
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        ApexPages.CurrentPage().getParameters().put('bbo1', 'https://www.volkswagen.com.au/app/configurator/vw-au/en/golf/30316/36620/110tsi-highline/BQ14NZ-GPB3PB3-GPH2PH2-GPJ6PJ6-GPLAPLA-GPNCPNC-GPS1PS1-GPU2PU2-GPXAPXA-GWL1WL1-GWZDWZD-GW4BW4B-GZEXZEX/2018/1/F14 0Q0Q/F56     TW/ ?page=summary');
        VGA_CarConfigurator config = new VGA_CarConfigurator();
        config.firstName = 'FirstName';
        config.lastName = 'LastName';
        config.emailaddress = 'alfahad.p@katzion.com';
        config.postCode1 = '2222';
        config.modelname='golf';
        //config.modelcode='BQ14NZ';
        config.brand = 'Volkswagen';
        config.Dealerbackcheckbox = true;
        config.checkbox=true;
        config.options = '+';
        config.submit();
        Test.stopTest();
        
        System.assertEquals(config.modelname,'GOLF');
    }
    
    /**
    * @author Ashok Chandra
    * @date 17/10/2018
    * @description Method that tests the VGA_CarConfigurator class.
    *              It makes sure a new Lead is created with brand Volkswagen
    *       and Sub brand Commercial Vehicles (CV).
    *               
    
    */     
    @isTest static void testCallout1() {
        
        Test.startTest();
        // Set mock callout class
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        string ddurl = 'http:testurl/en/tiguan/31101';
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock.
        ApexPages.CurrentPage().getParameters().put('bbo1', 'https://www.volkswagen.com.au/app/configurator/vw-au/en/tiguan/31101/36660/140tdi-highline/AD148T-GPALPAL-GPE1PE1-GPFXPFX-GWA2WA2-GWLKWLK-GWLRWLR-GWPAWPA-GZMEZME/2018/1/F14 2T2T/F56     TW/ ?page=summary');
        VGA_CarConfigurator config = new VGA_CarConfigurator();
        config.firstName = 'FirstName';
        config.lastName = 'LastName';
        config.emailaddress = 'alfahad.p@katzion.com';
        config.postCode1 = '2222';
        config.brand = 'Volkswagen';
        config.modelname='tiguan';
        config.Dealerbackcheckbox = false;
        config.checkbox=true;
        config.options = '';
        config.submit();
        Test.stopTest();
        
        System.assertEquals(config.modelname,'tiguan');
        
        //config.parseDDBurl( ddurl);
    }
}