/**
*@author Surabhi Ranjan
*@date 25-Oct-2017 
*@description This  Handler class for VGA_InvaildEmailTrigger
*----------------------------
*@Update on 06/05/2019 By Hijith NS
*@description  Added new method 'setBrand'
*/
public class VGA_InvaildEmailHandler 
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<VGA_Invalid_Email__c>) trigger.new);     
        }
        
        // Method will be called to handle After Insert events
        if (Trigger.isAfter && Trigger.isInsert)
        {
        }
        
        // Method will be called to handle Before Update events
        if (Trigger.isBefore && Trigger.isUpdate)
        {
        }
        
        // Method will be called to handle After Update events
        if (Trigger.isAfter && Trigger.isUpdate)
        {
        }
    }
    
    // Method calls all the methods required to be executed before insert
    public void onBeforeInsert(List<VGA_Invalid_Email__c> lstTriggerNew)
    {
        tagDealerAccountandtagDealerCode(lstTriggerNew,null);
        setBrand(lstTriggerNew);
    }
    
    // This method is used for tag dealer Account according to dealer code.
    public void tagDealerAccountandtagDealerCode(list<VGA_Invalid_Email__c> lsttriggernew,map<id,VGA_Invalid_Email__c> triggeroldmap)
    {
        map<String,Id>mapofAccount =new map<String,Id>();
        Set<string> setOfVin = new Set<string>();
        
        Map<String,Asset> VIN_Product_Map=new Map<String,Asset>();
        set<Id> setofDealer =new set<Id>();
        
        for(VGA_Invalid_Email__c objInvalidEmail : lsttriggernew)
        {
            if(objInvalidEmail.VGA_Dealer_Code__c == Null && objInvalidEmail.VGA_Dealer_Name__c == null 
               && objInvalidEmail.VGA_Account__c != null)
            {
                setofDealer.add(objInvalidEmail.VGA_Account__c);
            }
        }
        
        if(setofDealer != null 
           && !setofDealer.isEmpty())
        {
            map<Id,VGA_Customer_Order_Details__c> mapofCOD = getMapOfCustomOrderDetails(setofDealer);
            map<Id,VGA_Ownership__c>        mapofOwnership = getOwnershipMap(setofDealer);
            
            for(VGA_Invalid_Email__c objInvalidEmail : lsttriggernew)
            {
                if(objInvalidEmail.VGA_Dealer_Code__c == Null 
                && objInvalidEmail.VGA_Dealer_Name__c == null 
                   && objInvalidEmail.VGA_Account__c != null)
                {
                    if(mapofCOD.containskey(objInvalidEmail.VGA_Account__c) 
                       && mapofCOD.get(objInvalidEmail.VGA_Account__c) != null 
                       && !mapofOwnership.containsKey(objInvalidEmail.VGA_Account__c))
                    {
                        objInvalidEmail.VGA_Dealer_Code__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__r.VGA_Dealer_Code__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__r.VGA_Dealer_Code__c:null;
                        objInvalidEmail.VGA_Dealer_Name__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__c:null;
                        objInvalidEmail.VGA_Type__c = 'Vehicle order';
                        if(objInvalidEmail.VGA_VIN__c==null)
                            objInvalidEmail.VGA_VIN__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_VIN__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_VIN__c:null;
                        if(objInvalidEmail.VGA_Commission_Number__c == null)
                            objInvalidEmail.VGA_Commission_Number__c =mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c:null;
                    }
                    else if(mapofOwnership.containsKey(objInvalidEmail.VGA_Account__c) 
                            && mapofOwnership.get(objInvalidEmail.VGA_Account__c) != null 
                            && !mapofCOD.containskey(objInvalidEmail.VGA_Account__c))
                    {
                        objInvalidEmail.VGA_Type__c = 'RDA’d';
                        objInvalidEmail.VGA_Dealer_Code__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r.VGA_Dealer_Code__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r.VGA_Dealer_Code__c:null;
                        objInvalidEmail.VGA_Dealer_Name__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__c:null;
                        if(objInvalidEmail.VGA_VIN__c==null)
                            objInvalidEmail.VGA_VIN__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_VIN_for_Mule__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_VIN_for_Mule__c:null;
                        if(objInvalidEmail.VGA_Commission_Number__c == null)
                            objInvalidEmail.VGA_Commission_Number__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c:null;
                    }
                    else if(mapofCOD.containskey(objInvalidEmail.VGA_Account__c) 
                            && mapofCOD.get(objInvalidEmail.VGA_Account__c) != null && 
                            mapofOwnership.containsKey(objInvalidEmail.VGA_Account__c)  
                            && mapofOwnership.get(objInvalidEmail.VGA_Account__c) != null)
                    {
                        if(mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Order_Date__c > mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Purchase_date__c)
                        {
                            objInvalidEmail.VGA_Type__c ='Vehicle order';
                            objInvalidEmail.VGA_Dealer_Code__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__r.VGA_Dealer_Code__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__r.VGA_Dealer_Code__c:null;
                            objInvalidEmail.VGA_Dealer_Name__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Dealer_Account__c:null;
                            if(objInvalidEmail.VGA_VIN__c==null)
                                objInvalidEmail.VGA_VIN__c = mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_VIN__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_VIN__c:null;
                            if(objInvalidEmail.VGA_Commission_Number__c == null)
                                objInvalidEmail.VGA_Commission_Number__c =mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c != null?mapofCOD.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c:null;
                        }
                        else
                        {
                            objInvalidEmail.VGA_Type__c = 'RDA’d';
                            objInvalidEmail.VGA_Dealer_Code__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r.VGA_Dealer_Code__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r.VGA_Dealer_Code__c:null;
                            objInvalidEmail.VGA_Dealer_Name__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__r != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Dealership__c:null;
                            if(objInvalidEmail.VGA_VIN__c==null)
                                objInvalidEmail.VGA_VIN__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_VIN_for_Mule__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_VIN_for_Mule__c:null;
                            if(objInvalidEmail.VGA_Commission_Number__c == null)
                                objInvalidEmail.VGA_Commission_Number__c = mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c != null?mapofOwnership.get(objInvalidEmail.VGA_Account__c).VGA_Commission_Number__c:null;
                        }
                    }
                }
                
            }
        }
    }

    private map<Id,VGA_Customer_Order_Details__c> getMapOfCustomOrderDetails(set<Id> setofDealer) {
        map<Id,VGA_Customer_Order_Details__c> mapofCOD = new map<Id,VGA_Customer_Order_Details__c>();

        for(VGA_Customer_Order_Details__c objCOD : [SELECT Id,VGA_Dealer_Account__c,VGA_Order_Date__c,VGA_VIN__c,VGA_Commission_Number__c,VGA_Dealer_Account__r.VGA_Dealer_Code__c,
                                                           VGA_Customer__c,VGA_Dealer_Code__c,VGA_Dealer_Code_f__c 
                                                    FROM VGA_Customer_Order_Details__c 
                                                    WHERE VGA_Customer__c in:setofDealer 
                                                    ORDER BY VGA_Order_Date__c desc])
        {
            if(objCOD.VGA_Order_Date__c != null && !mapofCOD.containskey(objCOD.VGA_Customer__c))
                mapofCOD.put(objCOD.VGA_Customer__c,objCOD);
        }

        return mapofCOD;
    }

    private map<Id,VGA_Ownership__c> getOwnershipMap(set<Id> setofDealer){
        Map<Id,VGA_Ownership__c> mapofOwnership = new Map<Id,VGA_Ownership__c>();
        
        for(VGA_Ownership__c objOwnership : [SELECT Id,VGA_Owner_Name__c,VGA_VIN_for_Mule__c,VGA_Purchase_date__c,VGA_Commission_Number__c,VGA_Dealership__c,
                                                    VGA_Dealer_Code__c,VGA_Dealership__r.VGA_Dealer_Code__c 
                                             FROM VGA_Ownership__c 
                                             WHERE VGA_Owner_Name__c IN:setofDealer 
                                             ORDER BY VGA_Purchase_date__c desc])
        {
            if(objOwnership.VGA_Purchase_date__c != null && !mapofOwnership.containskey(objOwnership.VGA_Owner_Name__c))
                mapofOwnership.put(objOwnership.VGA_Owner_Name__c,objOwnership);    
        }

        return mapofOwnership;
    }
    
    public class LabelBrandWrapper{
        public set<String> Condition;
        public String Value;
    }

    /*
    *@Update on 06/05/2019 By Hijith NS
    *@description   This Method populates the value of the brand field on the newly created Invalid Email records based on the
    *              brand and subbrand value of the model related to the vehicle that has the same VIN as the Invalid Email record that
    *              we are processing. We are using the VGA_InvalidEmail_Brand_Setting label
    *              to identify the values for each brand, sub-brand
    * @param lsttriggernew List of new Invalid Email records that we will process
    */        
    public void setBrand(list<VGA_Invalid_Email__c> lsttriggernew)
    {
        List<LabelBrandWrapper> LabelData = (List<LabelBrandWrapper>)JSON.deserialize(label.VGA_InvalidEmail_Brand_Setting,List<LabelBrandWrapper>.class);
        Set<string> SetOfVin = new Set<string>();        
        Map<String,Asset> VIN_Product_Map=new Map<String,Asset>();
        for(VGA_Invalid_Email__c objInvalidEmail : lsttriggernew)
        {
            if(objInvalidEmail.VGA_VIN__c != null)
            {
                if(!VIN_Product_Map.containsKey(objInvalidEmail.VGA_VIN__c))
                    VIN_Product_Map.put(objInvalidEmail.VGA_VIN__c,null);
            }
        }
        if(VIN_Product_Map.size()>0){
            List<Asset> VehicleList =[select name,id,Product2.id,product2.VGA_Brand__c,product2.VGA_Sub_brand__c,VGA_VIN__c from asset where VGA_VIN__c in:VIN_Product_Map.keySet()];
            for(Asset a:VehicleList){
                if(VIN_Product_Map.containsKey(a.VGA_VIN__c))
                {
                    VIN_Product_Map.put(a.VGA_VIN__c, a);
                }
            }
        }
        for(VGA_Invalid_Email__c objInvalidEmail : lsttriggernew)
        {
            if(VIN_Product_Map.containsKey(objInvalidEmail.VGA_VIN__c))
            {
                Asset a1 = VIN_Product_Map.get(objInvalidEmail.VGA_VIN__c);
                for(LabelBrandWrapper lwObj :LabelData )
                {
                    if(lwObj.Condition.contains(a1.Product2.VGA_Brand__c) 
                       && lwObj.Condition.contains(a1.Product2.VGA_Sub_brand__c)){
                       objInvalidEmail.VGA_Brand__c = lwObj.Value;
                       break;
                    }
                                            
                }
                
            }
        }
    }
}