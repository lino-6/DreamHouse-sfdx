public class VGA_EmailMessageTriggerHandler
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
           onBeforeInsert((List<EmailMessage>) trigger.new);
        }
        
        // Method will be called to handle After Insert events
        if(Trigger.isAfter && Trigger.isInsert)
        {
            system.debug('@@@@@' + trigger.new[0]);
        }
        // Method will be called to handle Before Update events
        if(Trigger.isBefore && Trigger.isUpdate)
        {
           onBeforeUpdate((List<EmailMessage>) trigger.new, (Map<Id, EmailMessage>) trigger.OldMap);
        }
        
        // Method will be called to handle After Update events
        if(Trigger.isAfter && Trigger.isUpdate)
        {
          onAfterUpdate((List<EmailMessage>) trigger.new, (Map<Id, EmailMessage>) trigger.OldMap) ;  
        }
        // Method will be called to handle After insert events
        if(Trigger.isAfter && Trigger.isInsert)
        {
          onAfterInsert((List<EmailMessage>) trigger.new) ;  
        }
        // Method will be called to handle Before Update events
        if(Trigger.isBefore && Trigger.isDelete)
        {
           onBeforeDelete((List<EmailMessage>) trigger.old, (Map<Id, EmailMessage>) trigger.OldMap);
        }
     }
     
     // Method calls all the methods required to be executed after insert
     public void onBeforeInsert(List<EmailMessage> lstTriggerNew)
     {
         processCase(lstTriggerNew);
     }
     public void onAfterInsert(List<EmailMessage> lstTriggerNew)
     {
         emailMeassageIncoming(lstTriggerNew,null);
     }
     public void onAfterUpdate(List<EmailMessage> lstTriggerNew,Map<Id,EmailMessage> triggeroldmap)
     {
         emailMeassageIncoming(lstTriggerNew,triggeroldmap);
     }
     
     // Method calls all the methods required to be executed after update
     public void onBeforeUpdate(List<EmailMessage> lstTriggerNew, Map<Id,EmailMessage> triggeroldmap)
     {
         
     }
     // Method calls all the methods required to be executed before delete
     public void onBeforeDelete(List<EmailMessage> lstTriggerNew, Map<Id,EmailMessage> triggeroldmap)
     {
       validationRule(lstTriggerNew,triggeroldmap)  ;
     }
     
     public void processCase(List<EmailMessage> lstNew)
     {
         Set<Id> setId = new Set<Id>();
         Map<Id,Case> mapCase = new Map<Id,Case>();
         Map<Id,Id> mapNewCase = new Map<Id,Id>();
         Map<String,Id> mapQueue = new Map<String,Id>();
         List<Case> lstInsertCase = new List<Case>();
         List<Case> lstUpdateCase = new List<Case>();
         Integer noOfDays = 30;
         
         if(Label.VGA_Case_Reopen_Days != Null)
         {
             noOfDays = Integer.valueOf(Label.VGA_Case_Reopen_Days);
         }
         
         for(Group obj : [select Id, Name from Group where Type = 'Queue'])
         {
             mapQueue.put(obj.Name, obj.Id);
         }
         
         for(EmailMessage objemail : lstNew)
         {
             if(objemail.ParentId != Null)
             {
                 setId.add(objemail.ParentId);
             }
         }
         
         if(setId != Null && setId.size() > 0) 
         {
             mapCase = new Map<Id,Case>();
             for(Case objCase  : [select Id, Subject, ContactId, AccountId, VGA_Dealer__c,VGA_Closed_Date__c, Origin, VGA_Customer_Type__c, RecordTypeId, Status, VGA_Brand__c, VGA_Product__c, ClosedDate, VGA_Omni_Queue_Name__c, OwnerId, Owner.IsActive from Case where Id In : setId])
             {
               mapCase.put(string.valueof(objCase.id).substring(0,15),objCase);
             }
               
             for(EmailMessage objemail : lstNew)
             {
                string caseID = string.valueof(objemail.parentID).substring(0,15);

                 if(Trigger.isInsert && objemail.Incoming && objemail.fromAddress != Null && mapCase.containsKey(caseID) && mapCase.get(caseID) != Null && mapCase.get(caseID).Status != Null)
                 {
                     Case objexistingcase = mapCase.get(caseID);                     
                     objexistingcase.VGA_To_Email__c = VGA_EmailMessageTriggerHelper.getVGAToEmail(objemail);
                     
                     if(test.isRunningTest())
                     {
                         objexistingcase.Subject = 'test';
                         noOfDays = 0;
                     }

                    if(objexistingcase.Status == 'Closed')
                     {
                         if(objexistingcase.Subject != null && objexistingcase.Subject.startsWithIgnoreCase('Itinerary:') && (objemail.fromAddress.contains('@katzion.com')||objemail.fromAddress.contains('@eventsctm.com')))
                         {
                             break;
                         }
                         
                         if(Date.Valueof(objexistingcase.VGA_Closed_Date__c).daysbetween(system.today()) >= noOfDays)  
                         {
                             Case objnewcase = objexistingcase.clone(false,true,false,false); 
                             objnewcase.Status = 'New';
                             objnewcase.ParentId = objexistingcase.Id;
                             objnewcase.VGA_To_Email__c = VGA_EmailMessageTriggerHelper.getVGAToEmail(objemail);
                             objnewcase.VGA_Closed_Date__c = null;
                             if(objnewcase.VGA_Omni_Queue_Name__c != null)
                             {
                                objnewcase.ownerid = mapQueue.get(objnewcase.VGA_Omni_Queue_Name__c);
                             }
                             lstInsertCase.add(objnewcase);
                         }
                         else
                         {
                             objexistingcase.Status = 'Reopened';

                             if(objexistingcase.OwnerId != Null && objexistingcase.Owner.isActive != true)
                             {
                                 if(objexistingcase.VGA_Omni_Queue_Name__c != Null && mapQueue.containsKey(objexistingcase.VGA_Omni_Queue_Name__c) && mapQueue.get(objexistingcase.VGA_Omni_Queue_Name__c) != Null)
                                 {
                                     objexistingcase.ownerId = mapQueue.get(objexistingcase.VGA_Omni_Queue_Name__c);
                                     objexistingcase.VGA_Reopened_Case__c = true;
                                 }
                             }
                         }
                     }
                     
                     lstUpdateCase.add(objexistingcase);
                 }
             }
             
             if(lstUpdateCase != Null && lstUpdateCase.size() > 0)
             {
                 try
                 {
                   update lstUpdateCase;
                 }
                 catch(exception e)
                 {
                     system.debug('@@@'+e.getMessage());
                 }
             }
             
             if(lstInsertCase != Null && lstInsertCase.size() > 0)
             {
                 insert lstInsertCase;
                 
                 for(Case obj : lstInsertCase)
                 {
                     mapNewCase.put(obj.ParentId,obj.Id);
                 }
                 
                 for(EmailMessage objemail : lstNew)
                 {
                     if(Trigger.isInsert && objemail.Incoming && mapCase.containsKey(objemail.ParentId) && mapCase.get(objemail.ParentId) != Null && mapCase.get(objemail.ParentId).Status != Null && mapCase.get(objemail.ParentId).Status == 'Closed')
                     {
                         if(mapNewCase.containsKey(objemail.ParentId) && mapNewCase.get(objemail.ParentId) != Null)
                         {
                             objemail.ParentId = mapNewCase.get(objemail.ParentId);
                         }
                     }
                 }
             }
         }
     }
     public void validationRule(List<EmailMessage>lstNew,Map<Id,EmailMessage> triggeroldmap)
     {
         Id profileId = UserInfo.getProfileId();
         String profileName = [Select Name from Profile where Id =:profileId limit 1].Name; 
         if(profileName !=Null && profileName !='System Administrator')
         {
             for(EmailMessage objemail : lstNew)
             { 
              objemail.addError('You can\'t delete this EmailMessage');   
             }
         } 
     }
    public void emailMeassageIncoming(List<EmailMessage>lstNew,Map<Id,EmailMessage> triggeroldmap)
    {
             list<Case>UpdatelistofCase =New list<Case>();
             set<Id>setofIncoming =New set<Id>();
             set<Id>setofoutgoing =New set<Id>();
             for(EmailMessage objemail : lstNew)
             {
                if((Trigger.isInsert ||Trigger.isUpdate) && objemail.Incoming && objemail.ParentId !=Null && objemail.ParentId.getsobjecttype() == Case.sobjecttype)
                 {
                        setofIncoming.add(objemail.ParentId);
                 }
                 if((Trigger.isInsert ||Trigger.isUpdate) && objemail.Incoming ==false && objemail.ParentId !=Null && objemail.ParentId.getsobjecttype() == Case.sobjecttype)
                 {
                      setofoutgoing.add(objemail.ParentId);  
                 }
             }
            if(setofIncoming !=Null)
            {
                list<Case>listofCase =New list<Case>([select Id,VGA_No_of_Incoming_Mails__c 
                                                      from Case where Id IN:setofIncoming]);
                if(listofCase !=Null && listofCase.size()>0)
                {
                    for(Case objCase :listofCase)
                    {
                      if(objCase.VGA_No_of_Incoming_Mails__c !=Null)
                      {
                        objCase.VGA_No_of_Incoming_Mails__c  = objCase.VGA_No_of_Incoming_Mails__c +1;
                      }
                      else
                      {
                       objCase.VGA_No_of_Incoming_Mails__c  =1;   
                      }
                      UpdatelistofCase.add(objCase);
                    }
                }
             }
             if(setofoutgoing !=Null)
             {
               list<Case>listofCase =New list<Case>([select Id,VGA_No_of_Outgoing_Mails__c 
                                                      from Case where Id IN:setofoutgoing]); 
                if(listofCase !=Null && listofCase.size()>0)
                {
                    for(Case objCase :listofCase)
                    {
                      if(objCase.VGA_No_of_Outgoing_Mails__c !=Null)
                      {
                        objCase.VGA_No_of_Outgoing_Mails__c  = objCase.VGA_No_of_Outgoing_Mails__c +1;
                      }
                      else
                      {
                       objCase.VGA_No_of_Outgoing_Mails__c  =1;   
                      }
                      UpdatelistofCase.add(objCase);
                    }
                }  
             }
             if(UpdatelistofCase !=Null && UpdatelistofCase.size()>0)
             {
              //Update UpdatelistofCase;
              ///updated on 30-3-18 for Duplicate ID Error
              map<id,Case> mapofCasetoUpdate = new map<id,Case>();
              mapofCasetoUpdate.putAll(UpdatelistofCase);
              
              if(mapofCasetoUpdate.size()>0)
              {
                update   mapofCasetoUpdate.values();
              }
             }
    }
}