public class VGA_LeadBatchHelper
{   
    /**
      * @author Ganesh M
      * @date 29/11/2018
      * @description This method reschedules the Lead Batch jobs for execution
      *              to run the number of minutes defined in the nextExecutionTime after the previous job.
      *              It will create a Error Log record in case something fails trying to 
      *              reschedule the job.
      * @param nextExecutionTime Number of minutes when the execution of this job will have to run
      * @param jobName Name of the job that we need to execute
      * @param manualJobId Id of the manual job record that we need to execute
    */   
    public static void rescheduleBatchJobs(Integer nextExecutionTime, String jobName, Id manualJobId){
        
        
        Datetime dt = system.now().addMinutes(nextExecutionTime);
            
        String day    = string.valueOf(dt.day());
        String month  = string.valueOf(dt.month());
        String hour   = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year   = string.valueOf(dt.year());
        
        String strJobName = getJobName(dt, jobName, manualJobId);
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        

        try
        {
            //Based on the job name we will be executing the right batch
            if(jobName == 'Lead Escalation Job'){
                System.schedule(strJobName, strSchedule, new VGA_scheduleLeadEscalation());
            }

            if(jobName == 'Lead Assignment Job'){
                System.schedule(strJobName, strSchedule, new VGA_scheduleLeadAssignment());
            }

            if(jobName == 'Pre Launch Lead Assignment Job'){
                System.schedule(strJobName, strSchedule, new VGA_scheduleManualLeadAssignment(manualJobId));
            }
            
        }
        catch(Exception e)
        {
            List<String> ExceptionList = new List<String>();
            ExceptionList.add(e.getMessage()+'\t Line Number : '+e.getLineNumber());
            manageExceptionInBatch(e, 'Exception in ' + jobName + ' Batch : Not able to schedule Job: ' + strJobName, jobName,  null, ExceptionList);
        }

    }

    /**
      * @author Lino Diaz Alonso
      * @date 03/07/2019
      * @description This method gets the Job name that we will use when we schecule this job for
      *              execution. For the prelaunch lead assigment jobs we need to include the tracking Id
      *              on the job name 
      * @param dt Date on when the job will be executed
      * @param jobName Name of the job that we need to execute
      * @param manualJobId Id of the manual job record that we need to execute
    */
    private static String getJobName(Datetime dt, String jobName, Id manualJobId){
        String name = jobName.replaceAll( '\\s+', '');

        if(jobName == 'Pre Launch Lead Assignment Job'){
            if(manualJobId != null){
                VGA_Manual_Assignment_Configuration__c objplc = [SELECT Id, VGA_Tracking_Id__c, VGA_Number_of_Leads_per_Distribution__c, 
                                                                 VGA_Time_Interval__c, VGA_Time_Interval_No_Leads__c 
                                                          FROM VGA_Manual_Assignment_Configuration__c 
                                                          WHERE Id =: manualJobId];
                if(objplc!=null){
                    return name + '-' + objplc.VGA_Tracking_Id__c + '-' + String.valueof(dt);
                }
            }
        }

        return name + '-' + String.valueof(dt);
    }

    /**
      * @author Lino Diaz Alonso
      * @date 04/02/2019
      * @description This method receives an exception and creates a Error Log record and also
      *              tries to reschedule the job.
      * @param e Exception that needs to be managed
      * @param process String with the information of the process that triggered the exception
      * @param BD Database context in the moment of the exception
      * @param ExceptionList List of exceptions
    */
    public static void manageExceptionInBatch(Exception e, String process, String batchName, Database.BatchableContext BC, List<String> ExceptionList){

        List<VGA_ErrorLog__c> errorLogList = [SELECT Id
                                              FROM VGA_ErrorLog__c
                                              WHERE Process__c LIKE: process];
        if(errorLogList.size() < 100){
            ErrorLogBuilder errorLogBuilder = new ErrorLogBuilder();
            errorLogBuilder.add(new Map<String, Object>{'RelatedObject__c' => 'Lead', 
                                                        'RelatedRecordID__c' => null, 
                                                        'Type__c' => 'Error', 
                                                        'Process__c' => process, 
                                                        'Message__c' => e.getMessage()+' at line number'+e.getLineNumber()}
                                                        , 1000);
            errorLogBuilder.save();
        }
        alertUser(BC, ExceptionList, batchName);

        rescheduleBatchJobs(5, batchName, null);
    }

    /**
      * @author Mohammed Ishaque
      * @date 24/05/2019
      * @description This method sends an email in case there is an issue with one of the batch jobs
      * @param BD Database context in the moment of the exception
      * @param ExceptionList List of exceptions
      * @param batchName Name of the job that we need to execute
    */
    public static void alertUser(Database.BatchableContext BC, List<String> ExceptionList, String batchName){
        
        String Message='Hi, <br/><br/> ' + batchName + ' Batch failed to run due to following error : <br/><br/>';
        Message+='<ul>';
        for(String l:ExceptionList){
            Message+='<li>'+l+'</li>';
        }
        Message+='</ul><br/>';
        if(BC!=null){
            AsyncApexJob Job=[SELECT Id, JobType, ApexClass.name, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, MethodName, CompletedDate, ExtendedStatus FROM AsyncApexJob where id=:BC.getJobId()];
            Message+='<h3>Batch Class Details</h3><br/><br/>';
            Message+='<table> <tr> <td><label>JOB ID</label></td> <td>'+Job.Id+'</td> </tr> <tr> <td><label>JOB TYPE</label></td> <td>'+Job.JobType+'</td> </tr> <tr> <td><label>APEX CLASS NAME</label></td> <td>'+Job.ApexClass.name+'</td> </tr> <tr> <td><label>STATUS</label></td> <td>'+Job.Status+'</td> </tr> <tr> <td><label>Total NO OF CHILD JOBs</label></td> <td>'+Job.TotalJobItems+'</td> </tr> <tr> <td><label>NO OF JOB PROCESSED</label></td> <td>'+Job.JobItemsProcessed+'</td> </tr> <tr> <td><label>COMPLETED DATE</label></td> <td>'+Job.CompletedDate+'</td> </tr> <tr> <td><label>EXTENDED STATUS</label></td> <td>'+Job.ExtendedStatus+'</td> </tr> </table>';
        }
        Message+='<br/><br/>Thanks<br/>Salesforce Support';
        VGA_Common.sendExceptionEmail('Exception in ' + batchName + ' Batch',Message);
    }
    
    /**
      * @author Lino Diaz Alonso
      * @date 02/07/2019
      * @description This method aborts the previous job before scheduling the same
      *              job for execution.
      * @param previousJobName Name of the job that we need to abort
      * @param recordId Id of the prelaucnh job that is related to the job that we need to abort
    */
    public static void abortPreviousJob(String previousJobName, String recordId){
        List<CronTrigger> lstCron = new List<CronTrigger>();

        if(previousJobName.contains('PreLaunchLeadAssignmentJob')){
            getListOfPreviousJobs(lstCron, previousJobName, recordId);
        }else {
            lstCron = [SELECT Id, CreatedDate, CronExpression, EndTime 
                       FROM CronTrigger 
                       WHERE CronJobDetail.Name LIKE: previousJobName
                       AND NextFireTime  = null];
        }

        if(lstCron != Null && !lstCron.isEmpty()){
            try{
                for(CronTrigger obj : lstCron){
                    system.abortjob(obj.Id);
                }
            }catch(Exception e){
                VGA_Common.sendExceptionEmail('Exception in ' + previousJobName, 'There was a problem aborting one of the previous jobs.');
            }
        } 
    }

    /**
      * @author  Lino Diaz Alonso
      * @date 02/07/2019
      * @description This method populates the list of jobs for the PreLaunchLeadAssignment jobs.
      * @param previousJobName Name of the job that we need to abort
      * @param recordId Id of the prelaucnh job that is related to the job that we need to abort
      * @param lstCron List of jobs
    */
    public static void getListOfPreviousJobs(List<CronTrigger> lstCron, String previousJobName, string recordId){
        List<VGA_Manual_Assignment_Configuration__c> objplcList = [SELECT Id, VGA_Tracking_Id__c, VGA_Number_of_Leads_per_Distribution__c, 
                                                                            VGA_Time_Interval__c, VGA_Time_Interval_No_Leads__c 
                                                                    FROM VGA_Manual_Assignment_Configuration__c 
                                                                    WHERE Id =: recordId];
        if(objplcList.size()!=0){
            String jobName = '%PreLaunchLeadAssignmentJob-' + objplcList[0].VGA_Tracking_Id__c +'%';

            for(CronTrigger crTrigger: [SELECT Id, CreatedDate, CronExpression, EndTime
                                        FROM CronTrigger 
                                        WHERE CronJobDetail.Name LIKE: jobName
                                        AND NextFireTime  = null]){
                lstCron.add(crTrigger);                
            }
        }
    }
}