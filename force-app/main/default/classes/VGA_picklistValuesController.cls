// ------------------------------------------------------------------------------------------------------
    // Version#     Date                Author                    
    // ------------------------------------------------------------------------------------------------------
    // 1.0          11-September-2017      Surabhi Ranjan         
    // ------------------------------------------------------------------------------------------------------
    //Description: This  class is used to get picklist value in all the component.
  public class VGA_picklistValuesController 
   {
    // -----------------------------------------------------------------------------------------------------------------
    // To get partent picklist value.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - string ,string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         11-September-2017          surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static list<string> PicklistValues(string objectAPIName, string fieldAPIName){
        return VGA_Common.getPickListValues(objectAPIName, fieldAPIName);
    }
     // -----------------------------------------------------------------------------------------------------------------
    // To get dependent picklist value.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - string ,string,string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         11-September-2017          surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static Map<String, List<String>> dependentPicklistValues(string objectAPIName, string fieldAPIName, string parentFieldAPI){
        
        return VGA_dependentPicklist.getFieldDependencies( objectAPIName, parentFieldAPI,fieldAPIName);
    }
 }