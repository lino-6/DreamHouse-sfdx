public class VGA_BBOService {
    
    private String org_id;
    private String auth_key;
    private String service_url;
    private VGA_BBOCustomer customer;
    private VGA_BBOLead lead;
    private Id account;
    
    public VGA_BBOService(VGA_BBOCustomer customer, VGA_BBOLead lead, Id account)
    {
        this.customer = customer;
        this.lead = lead;
        this.account = account;
        
        List<BBOSetting__mdt> record = [SELECT Enabled__c, Production_Authorization_Token__c, Production_Environment__c, Production_Organisation_ID__c, Production_Service_URL__c, Staging_Authorization_Token__c, Staging_Organisation_ID__c, Staging_Service_URL__c FROM BBOSetting__mdt WHERE DeveloperName ='BBO'];
        
        if(record.size() == 0)
        {
            throw new VGA_BBOException('Custom metadata not found');
        }
        
        BBOSetting__mdt settings = record.get(0);
        
        if(settings.Enabled__c == false)
        {
            throw new VGA_BBOException('Service not enabled in custom metadata');
        }
        
        if(settings.Production_Environment__c)
        {
            org_id = settings.Production_Organisation_ID__c;
            auth_key = settings.Production_Authorization_Token__c;
            //auth_key = 'eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIn0.TxRstvLwtIS9ASMoFYGIgiGW_fgFXAX2uR_S5gR3Em8xaPmTiF6mlg.OFr9TYyl8V-Af6PLz_O_MQ.-4U7RO5BhOicRdGZNgAUPWJAtnxUA-33B6hi0WkuTX_d-j_GmrkN-HL6CIMo9TrfiK2qIsZvXQP9k3KNOBPWroEzxYRQKWsX8nHbuFRA-hKNIvN5UCebka9RcsBqzGkakFSqISMQ-CjFFJtx6y1dg4f5eN4xRjx5rTpFveSsWDgz9aU-iLJ7KCYvv3zPhCQ7rOsBIFKHAAjkmfpV3u0-R_5ezwKt6P7ko60vKHoxTrgzPu8Yv3y6yaCGi64dge-exbHhkEMQIPcRFZh5ImnXNk79SbPplQOZrAIV6-j0wNw-ViQz89cDtl25T6D8BQmbE_8U_nXLFBrCk2sYAcnWoRset5xrzCkKZZiOKfVaDKhmhCI3I8gzhlUhaywztHoqy-mEthCTWYGenRz6musK_5OidktBU6wUk82Ya4azdOhNWWBxysgzNEXfcoEDM-Ux25H5XL89onBNguyj_rTH62ZMLwa_DQ_D73mslZ-ASGnl7PAdlhj7wTQDAaHQBmJh7O1MxA8yklcAtqn4psqNUq4ojGp77uvgK4W9wNuNAkk.q1lYVjSCob2d-cavYhj3KA';
            service_url = settings.Production_Service_URL__c;
            //service_url = 'https://smartsignals2.smart-digital-solutions.de/api/v1/CustomerEncryption';
        }
        else 
        {
            org_id = settings.Staging_Organisation_ID__c;
            auth_key = settings.Staging_Authorization_Token__c;   
            //auth_key = 'eyJhbGciOiJBMTI4S1ciLCJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwidHlwIjoiSldUIn0.sC-esT8Aww0EBaefC0h4cJQLxakPSjU_f-0DOCxMVVBpX18_QAF1vA.klUiYvbdA0tbrWFRh4KOSw.jbdGSt0oBUAcfhaTf2TsCWG4c69hrUTk9tHqtTwNO_g-rtuIuwltrR_4AFpeo6_0CdQSVV0BAQQ3e4yt8iFbto4D_CWLOHlwTQS5Q41Xvklt422Q5LsB-IKAdF8IEV2PJyw_L-Iewsn68IJeBYuzTS5GmNwRFgtnlHuw-eHp-KPP3-qy2_J_M_tLIXPZ9IoNcd6l6CetjxEdyekValD5KgPO3IVCy2maufaIy7P3O_pgiW47AR8yK_rpkqffGkFKocV_i7s1wdpL2OUsixBM0L_vJJoTiJ5bgsfCrYsh1u64Rch7xHDQXGGCS9Pyf3tMV6koFHFrV4QsnKOwEfQRpso4zb3SgA7fiOayqPNBo1Mr82lkgfYzrUzbDwxzGIrvKMxjIpn91sxDzuHGF23SGingwHgfFVtPOrid9pjV03kebwN0eDpd9oaLjVE70JgryMblrVTt9wJ_T0tP68L7ti1uazcxJ1BdZWzw4LPUYBoCz6-iqrF5MCd3opb16J41LgJpIVuBAnVdO5p3gZKYa1_zkHaH5Mudvc54DSoHGh0.JaV3Iiv9zUwXXFMpRha1mw'; 
            service_url = settings.Staging_Service_URL__c;
            //service_url = 'https://smasi2-stage.smart-digital-solutions.de/api/v1/CustomerEncryption';
        }
        
    }
    
    public VGA_BBOService(String payload)
    {
        this(null, null, null);
        
        Map<String, Object> obj = (Map<String, Object>)JSON.deserializeUntyped(payload);
        
        Map<String, Object> data = (Map<String, Object>)obj.get('cust');
        
        account = (Id)obj.get('account');
        
		if( data == null)
        {
            customer = null;
        }
        else
        {
            customer = new VGA_BBOCustomer();
            
            for(String key : data.keySet())
            {
                if(key == 'type')
                {
                    customer.customer_type =  customer.getEnumFromValue((Integer)data.get(key));
                }
                else if(key == 'camo')
                {
                    customer.camo = (String)data.get(key);
                }
                else if(key == 'pryr')
                {
                    customer.pryr = (Integer)data.get(key);
                }   
                else if(key == 'camp')
                {
                    customer.camp = (String)data.get(key);
                }  
                else if(key == 'puyr')
                {
                    customer.puyr = (Integer)data.get(key);
                } 
                else if(key == 'cusi')
                {
                    customer.cusi = (Integer)data.get(key);
                }     
                else if(key == 'nesd')
                {
                    customer.nesd = data.get(key) != null ? Date.valueOf((String)data.get(key)) : null;
                }                 

            }
        }
        
		data = (Map<String, Object>)obj.get('lead');
        
		if( data == null)
        {
            lead = null;
        }
        else
        {
            lead = new VGA_BBOLead();
            
            for(String key : data.keySet())
            {
                if(key == 'type')
                {
                    lead.lead_type =  lead.getEnumFromValue((Integer)data.get(key));
                }
                else if(key == 'camo')
                {
                    lead.camo = (String)data.get(key);
                }
                else if(key == 'pryr')
                {
                    lead.pryr = (Integer)data.get(key);
                }   
                else if(key == 'camp')
                {
                    lead.camp = (String)data.get(key);
                }       
                else if(key == 'lddt')
                {
                    lead.lddt = data.get(key) != null ? Date.valueOf((String)data.get(key)) : null;
                }                 

            }
        }        

    }
    

    public void setData(VGA_BBOCustomer customer, VGA_BBOLead lead, Id account)
    {
        this.customer = customer;
        this.lead = lead;
        this.account = account;
    }     
    
    public VGA_BBOCustomer getCustomer()
    {
        return customer;
    }
    
    public VGA_BBOLead getLead()
    {
        return lead;
    }
    
    public String getAccount()
    {
        return account;
    }
    
    public String getPayload()
    {
        return '{"cust": '+ (customer == null ? 'null' : customer.toString()) + ', "lead": ' + (lead == null ? 'null' : lead.toString()) + ', "account": "'+this.account+'"}';
    }
    
    public String encrypt()
    {
        
        String encrypted_string = '';
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();  
        
        req.setEndpoint(service_url + '?organizationId=' + org_id);
        req.setMethod('POST');
        req.setHeader('Content-type', 'text/plain;charset=UTF-8');
        req.setHeader('Authorization', 'Bearer ' + auth_key);
        req.setBody(getPayload());
        
		try 
		{            
            
            if(!Test.isRunningTest())
            {
                res = http.send(req);                       
                encrypted_string = res.getBody();
            }


		} catch(System.CalloutException e) 
		{
			System.debug('Callout error: '+ e);
			System.debug(res.toString());
		}     
        
        return encrypted_string;
    }
}