/**
* @author Lidiya Elizabeth
* @date 21/05/2019
* @description The class is used in VGA Retail service Claim project, the class gets all the data required for the lightning component.
*/
public without sharing  class VGA_ServicePlanClaimsController {
    public static  Integer Constant_KMS=Integer.valueOf(Label.VGA_ServiceClaim_Kilometres)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Kilometres);
    public static  Integer Constant_MONTH=Integer.valueOf(Label.VGA_ServiceClaim_Months)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Months);
    public static  Integer Constant_GST=Integer.valueOf(Label.VGA_ServiceClaim_GST)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_GST);
    public static  Integer Constant_ClaimLimit=Integer.valueOf(Label.VGA_ServiceClaim_Limit)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Limit);
    public static  String Constant_ComponentType='SVCPACK';
    public static  Set<String> BrandSet=new Set<String>{'PV Volkswagen','CV Volkswagen','Volkswagen'};

    
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to display data on Lightning component ,it collect all information need to display on Component
    */ 

    public class VINWrapperClass{
        @AuraEnabled public Asset Vehicle;
        @AuraEnabled public String ModelName,ModelYear,VIN,VehicleId,Description,Engine,FuelType,Transmission,RegNo,ModelCode;
        @AuraEnabled public Map<String,List<PolicyWrapperClass>> PolicyMap;
        @AuraEnabled public List<PolicyWrapperClass> Policies;
        @AuraEnabled public User LoggedInUser;

        public VINWrapperClass(){
            LoggedInUser=[select Id, Name,ContactId,Contact.Account.Name,Contact.AccountId from User Where Id=:UserInfo.getUserId()];
            Policies=new List<PolicyWrapperClass>();
            PolicyMap=new Map<String,List<PolicyWrapperClass>>();
            PolicyMap.put('PAST',new List<PolicyWrapperClass>());
            PolicyMap.put('PRESENT_SVC',new List<PolicyWrapperClass>());
            PolicyMap.put('PRESENT',new List<PolicyWrapperClass>());
            PolicyMap.put('FUTURE',new List<PolicyWrapperClass>());
        }
    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to Store Policy Information
    */ 

    public class PolicyWrapperClass{
        @AuraEnabled public String PolicyId,PolicyPlanType,PolicyStartDate,PolicyEndDate,PolicyMaxKMS,PolicyMaxEndDate,PolicyComponentCode,PolicyComponentType;
        @AuraEnabled public VGA_Vehicle_Policy__c Policy;
        @AuraEnabled public List<VGA_Service_Claim__c> ClaimData;
        @AuraEnabled public List<IntervalWrapperClass> MissedIntervalList;
        @AuraEnabled public List<IntervalWrapperClass> RemainingIntervalList;
        @AuraEnabled public List<IntervalWrapperClass> AllIntervalList;
        @AuraEnabled Public Boolean IntervalCheck;

        public PolicyWrapperClass(){
            ClaimData=new List<VGA_Service_Claim__c>();
            MissedIntervalList=new List<IntervalWrapperClass>();
            AllIntervalList=new List<IntervalWrapperClass>();
            RemainingIntervalList=new List<IntervalWrapperClass>();
            IntervalCheck=false;
        }
    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to Store VGA_Service_Interval__C Information
    */ 


    public class IntervalWrapperClass{
        @AuraEnabled public boolean Available;
        @AuraEnabled public String MaxDate,MaxKilometres;
        @AuraEnabled public VGA_Service_Interval__c ServiceInterval;
        public IntervalWrapperClass(VGA_Service_Interval__c interval,VGA_Vehicle_Policy__c  Policy){
            Available=true;
            ServiceInterval=interval;
            MaxDate=Policy.VGA_Policy_Start_Date__c==null?'':formatDate((Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(ServiceInterval.VGA_Months__c==null?0:ServiceInterval.VGA_Months__c)+Constant_MONTH))).format('dd-MM-yyyy');
            MaxKilometres=''+(Integer.valueOf(ServiceInterval.VGA_KMS__c==null?0:ServiceInterval.VGA_KMS__c)+Constant_KMS);
            
        }
    }

    
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to Store VGA_Service_Item__C Information
    */ 
    public class ItemWrapperClass{
        @AuraEnabled public String IntervalDescription;
        @AuraEnabled public Map<String,List<VGA_Service_Item__c>> ServiceItemMap;
        @AuraEnabled Public Decimal Parts,Sundries,Labour,Total,GST,TotalGST;
        public ItemWrapperClass(){
            ServiceItemMap = new Map<String,List<VGA_Service_Item__c>>();
            ServiceItemMap.put('Parts',new List<VGA_Service_Item__c>());
            ServiceItemMap.put('Labour',new List<VGA_Service_Item__c>());
            ServiceItemMap.put('Sundries',new List<VGA_Service_Item__c>());
            Parts=0;
            Sundries=0;
            Labour=0;
            Total=0;
            GST=0;
            TotalGST=0;
            IntervalDescription='';
        }
    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to Process Request params made by Component
    */ 
    public class ClaimWrapperClass{
        @AuraEnabled public String IntervalId,PolicyId,DealerId,ServiceDate,AssetId,RONumber,KM;
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description It accepts a vin number and gets all policy details with respect to VIN number 
    * @param VIN type:String
    * @return Asset object
    */ 
    @AuraEnabled 
    public static Response collectVINDetails(String  VIN){
        Response resp=new Response();
        try{
            VINWrapperClass VIN_Data=new VINWrapperClass();
            VIN_Data.Vehicle=collectVehicleDetails(VIN);
            if(VIN_Data.Vehicle.Vehicle_Policies__r.size()==0 || VIN_Data.Vehicle.Vehicle_Policies__r==null){
                resp.pass=false;
                resp.result='No Policies found!';
                return resp;
            }
            VIN_Data.Policies=collectPoliciesDetails(VIN_Data.Vehicle);
            Map<String,List<PolicyWrapperClass>> PolicyCodeMap=new Map<String,List<PolicyWrapperClass>>();
            for(PolicyWrapperClass temp:VIN_Data.Policies){
                if((temp.PolicyComponentCode!=null ||temp.PolicyComponentCode!='') && temp.PolicyComponentType==Constant_ComponentType){
                    if(!PolicyCodeMap.containsKey(temp.PolicyComponentCode))
                        PolicyCodeMap.put(temp.PolicyComponentCode,new List<PolicyWrapperClass>{});
                    PolicyCodeMap.get(temp.PolicyComponentCode).add(temp);
                }
            }
            if(PolicyCodeMap.size()>0 && VIN_Data.Vehicle.Product2Id!=null){
                collectServiceIntervals(VIN_Data.Vehicle.Product2Id,PolicyCodeMap);
            }

            for(PolicyWrapperClass temp:VIN_Data.Policies){
                if(PolicyCodeMap.containsKey(temp.PolicyComponentCode) && temp.PolicyComponentType==Constant_ComponentType ){
                    for(PolicyWrapperClass tmp:PolicyCodeMap.get(temp.PolicyComponentCode)){
                        if(tmp.PolicyId==temp.PolicyId){
                            temp=tmp;
                            temp=sortIntervals(temp);
                            temp=setAvailability(temp);
                            temp.IntervalCheck =temp.AllIntervalList.size()==0?false:true;  
                            break;
                        }
                    }
                }
            }
            tempMethod(VIN_Data);
            VIN_Data.ModelName=VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.VGA_model_card_name__c==null?'':VIN_Data.Vehicle.Product2.VGA_model_card_name__c);
            VIN_Data.ModelCode=VIN_Data.Vehicle.ProductCode==null?(VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.ProductCode==null?'':VIN_Data.Vehicle.Product2.ProductCode)):VIN_Data.Vehicle.ProductCode;
            VIN_Data.ModelYear=VIN_Data.Vehicle.VGA_Model_Year__c==null?(VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.VGA_Model_Year__c==null?'':VIN_Data.Vehicle.Product2.VGA_Model_Year__c)):VIN_Data.Vehicle.VGA_Model_Year__c;
            VIN_Data.VIN=VIN_Data.Vehicle.Name;
            VIN_Data.VehicleId=VIN_Data.Vehicle.Id;
            VIN_Data.Description=VIN_Data.Vehicle.ProductDescription==null?(VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.Description==null?'':VIN_Data.Vehicle.Product2.Description)):VIN_Data.Vehicle.ProductDescription;
            VIN_Data.Engine=VIN_Data.Vehicle.VGA_Engine__c==null?'':VIN_Data.Vehicle.VGA_Engine__c;
            VIN_Data.FuelType=VIN_Data.Vehicle.VGA_Fuel_Type__c==null?(VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.VGA_fuel_type_desc__c==null?'':VIN_Data.Vehicle.Product2.VGA_fuel_type_desc__c)):VIN_Data.Vehicle.VGA_Fuel_Type__c;
            VIN_Data.Transmission=VIN_Data.Vehicle.VGA_Transmission_Type__c==null?(VIN_Data.Vehicle.Product2==null?'':(VIN_Data.Vehicle.Product2.VGA_Transmission_Desc__c==null?'':VIN_Data.Vehicle.Product2.VGA_Transmission_Desc__c)):VIN_Data.Vehicle.VGA_Transmission_Type__c;
            VIN_Data.RegNo=VIN_Data.Vehicle.VGA_Registration_Number__c==null?'':VIN_Data.Vehicle.VGA_Registration_Number__c;

            resp.result=VIN_Data;
            resp.pass=true;
        }catch(Exception ex){
            resp.pass=false;
            resp.Error.add(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return resp;
    }
    
    /**
    * @author Lidiya Elizabeth
    * @date 21/05/2019
    * @description It accepts a vin number and gets all information required in the page from the following objects: Asset,Vehicle policy,Service Claim.
    * @param VIN type:String
    * @return Asset object
    */ 

    private static Asset collectVehicleDetails(String VIN){
        return [select id ,Name,VGA_Model_Name__c,Product2Id,VGA_Model_Year__c,VGA_Engine__c,VGA_Fuel_Type__c,VGA_Transmission_Type__c,ProductDescription,VGA_Registration_Number__c,Product2.Name,Product2.VGA_Model_Year__c,Product2.Description,Product2.VGA_model_card_name__c,Product2.VGA_Fuel_Type_Code__c,Product2.VGA_fuel_type_desc__c,Product2.VGA_Transmission_Code__c,Product2.VGA_Transmission_Desc__c,ProductCode,Product2.ProductCode,(select id,VGA_Component_Description__c,VGA_Policy_Start_Date__c,VGA_Policy_End_Date__c,VGA_Component_Code__c,Name,VGA_Component_Type__c from Vehicle_Policies__r ),(SELECT VGA_Claim_Date__c, VGA_GST__c, VGA_Total_Ex_GST__c, VGA_Vehicle_Policy__c, VGA_KMS__c, VGA_RO_Number__c, VGA_Service_Date__c, VGA_Dealer__c, VGA_Ownership__c, VGA_Asset__c, Name,VGA_Service_Interval__c ,VGA_Service_Interval__r.VGA_Service_Order__c,VGA_Dealer_Code__c,VGA_Service_Interval_Code__c,VGA_VIN__c,VGA_Service_Interval__r.VGA_Description__c,VGA_Dealer__r.Name, Id FROM Service_Claims__r where VGA_Is_Expired_Claim__c=false) from asset where name=:VIN and Product2.VGA_Brand__c in:BrandSet];
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description It gets all the vehicle policy for an asset,as well as the claim data,if the dealer has already claimed for a service
    *@param Vehicle which is the Asset object.
    * @return PolicyWrapperClass
    */ 

    private static List<PolicyWrapperClass> collectPoliciesDetails(Asset Vehicle){
        try{
            List<PolicyWrapperClass> tempPolicies=new List<PolicyWrapperClass>();
            for(VGA_Vehicle_Policy__c  temp:Vehicle.Vehicle_Policies__r){
                PolicyWrapperClass tempPolicy=new PolicyWrapperClass();
                tempPolicy.Policy=temp;
                tempPolicy.PolicyEndDate=temp.VGA_Policy_End_Date__c==null?'':formatDate(temp.VGA_Policy_End_Date__c).format('dd-MM-yyyy');/*ConvertDatetoString(''+temp.VGA_Policy_End_Date__c.format());*/
                tempPolicy.PolicyStartDate=temp.VGA_Policy_Start_Date__c==null?'':formatDate(temp.VGA_Policy_Start_Date__c).format('dd-MM-yyyy');/*ConvertDatetoString(''+temp.VGA_Policy_Start_Date__c.format());*/
                tempPolicy.PolicyId=temp.Id;
                //tempPolicy.PolicyPlanType=temp.Name;
                tempPolicy.PolicyPlanType=temp.VGA_Component_Description__c==null?'':temp.VGA_Component_Description__c;
                tempPolicy.PolicyComponentType=temp.VGA_Component_Type__c==null?'':temp.VGA_Component_Type__c;
                tempPolicy.PolicyComponentCode=temp.VGA_Component_Code__c==null?'':temp.VGA_Component_Code__c;
                tempPolicy.PolicyMaxEndDate=tempPolicy.PolicyComponentType==Constant_ComponentType?(temp.VGA_Policy_End_Date__c==null?'':formatDate(temp.VGA_Policy_End_Date__c.addMonths(Constant_MONTH)).format('dd-MM-yyyy')):'';
                tempPolicy.ClaimData=tempPolicy.PolicyComponentType==Constant_ComponentType?collectServiceClaims(temp.Id,Vehicle):null;
                tempPolicies.add(tempPolicy);
            }
            return tempPolicies;
        }catch(Exception ex){
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return new List<PolicyWrapperClass>();
    }
    
    /**
    * @author Lidiya Elizabeth
    * @date 21/05/2019
    * @description It gets all the service claims related to a vehicle policy for an asset.
    * @param PolicyID which is the vehicle policy Id
    *@param Vehicle which is the Asset object.
    * @return VGA_Service_Claim__c object
    */

    private static List<VGA_Service_Claim__c> collectServiceClaims(String PolicyID,Asset Vehicle){
        try{
            List<VGA_Service_Claim__c> tempClaims=new List<VGA_Service_Claim__c>();
            if(Vehicle.Service_Claims__r!=null){
                for(VGA_Service_Claim__c temp:Vehicle.Service_Claims__r){
                    if(PolicyID==temp.VGA_Vehicle_Policy__c){
                        tempClaims.add(temp);
                    }
                }
            }
            return tempClaims;
        }catch(Exception ex){
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return new List<VGA_Service_Claim__c>();
    }
    
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description is used to get all intervals from VGA_Servcie_Interval__c  Object, we use ModelId & Component code of Polict to retrieve all Intervals 
    * @param ModelId : Model(product) Id 
    * @param PolicyCodeMap : Map contains ComponentCode of Policy as KeySet against PolicyDetails 
    * 
    */ 

    private static void collectServiceIntervals(String ModelId,Map<String,List<PolicyWrapperClass>> PolicyCodeMap){
        try{
            
            String queryLike=generateQuery(PolicyCodeMap.keySet());
            System.debug('QUERYYY >>> '+queryLike);
            //System.debug(Database.query('SELECT VGA_Service_Order__c, VGA_Fixed_Price__c, VGA_Months__c, VGA_KMS__c, VGA_Description__c, VGA_Code__c, VGA_Price_Structure__c, VGA_Product2__c, VGA_Component_Code__c, Id FROM VGA_Service_Interval__c where VGA_Product2__c=:ModelId AND ( '+queryLike +')'));
            //Changed Field api name of VGA_Service_Interval__c Object  Added code to check component code is available in Service Interval
            List<VGA_Service_Interval__c> IntervalList=new List<VGA_Service_Interval__c>();
            IntervalList=Database.query('SELECT VGA_Service_Order__c, VGA_Fixed_Price__c, VGA_Months__c, VGA_KMS__c, VGA_Description__c, VGA_Code__c, VGA_Price_Structure__c, VGA_Product2__c, VGA_Component_Code__c, Id FROM VGA_Service_Interval__c where VGA_Product2__c=:ModelId AND ( '+queryLike +') order by VGA_Service_Order__c');
            
            
            
            //List<VGA_Service_Interval__c> IntervalList=[SELECT VGA_Service_Order__c, VGA_Fixed_Price__c, VGA_Months__c, VGA_KMS__c, VGA_Description__c, VGA_Code__c, VGA_Price_Structure__c, VGA_Product2__c, VGA_Component_Code__c, Id FROM VGA_Service_Interval__c where VGA_Component_Code__c in:PolicyCodeMap.keySet() and VGA_Product2__c=:ModelId];
            
            Boolean Bol1=false;
            for(VGA_Service_Interval__c temp:IntervalList){
                Set<String> dataSet=new Set<String>();
                dataSet.addall(temp.VGA_Component_Code__c.split(','));
                
                List<PolicyWrapperClass> tempPolicyList;
                for(String s:temp.VGA_Component_Code__c.split(',')){
                    tempPolicyList=PolicyCodeMap.containsKey(s)?PolicyCodeMap.get(s):null;
                if(tempPolicyList!=null){
                        break;
                    }
                }

                //List<PolicyWrapperClass> tempPolicyList=PolicyCodeMap.containsKey(temp.VGA_Component_Code__c)?PolicyCodeMap.get(temp.VGA_Component_Code__c):null;
                
                if(tempPolicyList!=null){
                    
                    Boolean Bol2=false;
                    for(PolicyWrapperClass tmp:tempPolicyList){
                        Bol2=false;
                        System.debug(tmp);
                    System.debug('==============');
                    System.debug('CHECk');
                    System.debug(tmp.Policy.VGA_Policy_End_Date__c);
                    System.debug(tmp.Policy.VGA_Policy_STart_Date__c);
                    
                    System.debug(tmp.Policy.VGA_Policy_End_Date__c.addMonths(Constant_MONTH));
                    System.debug(tmp.Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(temp.VGA_Months__c==null?0:temp.VGA_Months__c)+Constant_MONTH));
                    
                    
                    System.debug('**************');
                        
                        
                        if((tmp.Policy.VGA_Policy_End_Date__c.addMonths(Constant_MONTH))>=tmp.Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(temp.VGA_Months__c==null?0:temp.VGA_Months__c)+Constant_MONTH).addDays(-1)){
                        tmp.AllIntervalList.add(new IntervalWrapperClass(temp,tmp.Policy));
                            Bol1=true;
                        }else if(System.today()>=tmp.Policy.VGA_Policy_Start_Date__c && System.today()<=tmp.Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(temp.VGA_Months__c==null?0:temp.VGA_Months__c)+Constant_MONTH).addDays(-1)){
                            Bol2=true;
                        }
                        if(Bol2 && !Bol1){
                            tmp.AllIntervalList.add(new IntervalWrapperClass(temp,tmp.Policy));
                            Bol1=true;
                            break;
                    }
                    
                        //tmp.AllIntervalList.add(new IntervalWrapperClass(temp,tmp.Policy));
                    }
                    //tempPolicy.AllIntervalList.add(new IntervalWrapperClass(temp,tempPolicy.Policy));
                }
            }
        }catch(Exception ex){
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description : This is used to sort ServiceInterval by using VGA_Service_Order__c by passing Single SVC Policy 
    * @param Policy : Policy Information
    * @return PolicyWrapperClass : Policy Information
    */ 
    private static PolicyWrapperClass sortIntervals(PolicyWrapperClass Policy){
        PolicyWrapperClass temp =Policy;
        try{
            
            for(Integer i=0;i<temp.AllIntervalList.size();i++){
                for(Integer j=0;j<temp.AllIntervalList.size()-1;j++){
                    if(temp.AllIntervalList[j].ServiceInterval.VGA_Service_Order__c>temp.AllIntervalList[i].ServiceInterval.VGA_Service_Order__c){
                        IntervalWrapperClass tempz=temp.AllIntervalList[j];
                        temp.AllIntervalList[j]=temp.AllIntervalList[i];
                        temp.AllIntervalList[i]=tempz;
                    }
                }
            }
            if(temp.AllIntervalList.size()==1){
            
                temp.PolicyMaxKMS=temp.AllIntervalList[0].ServiceInterval.VGA_KMS__c==null?'0':''+(temp.AllIntervalList[0].ServiceInterval.VGA_KMS__c+Constant_KMS);
            }else if(temp.AllIntervalList.size()>1){
            temp.PolicyMaxKMS=temp.AllIntervalList[temp.AllIntervalList.size()-1].ServiceInterval.VGA_KMS__c==null?'0':''+(temp.AllIntervalList[temp.AllIntervalList.size()-1].ServiceInterval.VGA_KMS__c+Constant_KMS);
            }

            return temp;
        }catch(Exception ex) {
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return Policy;
    }
    
    
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description : This is used to set availablity of all Interval for Specified policy ;
    * @param Policy : Policy Information
    * @return PolicyWrapperClass : Policy Information
    */ 
    private static PolicyWrapperClass setAvailability(PolicyWrapperClass Policy){
        PolicyWrapperClass tempz =Policy;
        try{
            Map<String,VGA_Service_Claim__c> tempClaimMap=new Map<String,VGA_Service_Claim__c>();
            for(VGA_Service_Claim__c temp:tempz.ClaimData){
                if(temp.VGA_Service_Interval__r.VGA_Service_Order__c!=null){
                    tempClaimMap.put(''+temp.VGA_Service_Interval__r.VGA_Service_Order__c,temp);
                }
            }
            if(tempClaimMap.size()>0){
                List<String> tempList=new List<String>();
                tempList.addAll(tempClaimMap.keySet());
                tempList.sort();
                set<String> tempSet=new Set<String>();
                tempSet.addAll(tempList);
                
                List<VGA_Service_Claim__c> TempClaimList=new List<VGA_Service_Claim__c>();
                for(String tmp:tempList){
                    if(tempClaimMap.containsKey(tmp)){
                        TempClaimList.add(tempClaimMap.get(tmp));
                    }
                }
                tempz.ClaimData=TempClaimList;
                if(tempList.size()>0){
                    Integer LastNumber=Integer.valueOf(tempList[tempList.size()-1]);
                    for(IntervalWrapperClass INT_Temp:tempz.AllIntervalList){
                        if(INT_Temp.ServiceInterval.VGA_Service_Order__c<=LastNumber){
                            INT_Temp.Available=false;
                            if(!tempSet.contains(''+INT_Temp.ServiceInterval.VGA_Service_Order__c)){
                                tempz.MissedIntervalList.add(INT_Temp);
                            }
                        }else{
                            if(System.today()>tempz.Policy.VGA_Policy_Start_Date__c.addmonths(Integer.valueOf((INT_Temp.ServiceInterval.VGA_Months__c==null?0:INT_Temp.ServiceInterval.VGA_Months__c)+Constant_MONTH))){
                                INT_Temp.Available=false;
                                tempz.MissedIntervalList.add(INT_Temp);
                            }else{
                                INT_Temp.Available=true;
                                tempz.RemainingIntervalList.add(INT_Temp);
                            }
                        }
                    }
                }
            }else{
                for(IntervalWrapperClass INT_Temp:tempz.AllIntervalList){
                    if(System.today()>tempz.Policy.VGA_Policy_Start_Date__c.addmonths(Integer.valueOf((INT_Temp.ServiceInterval.VGA_Months__c==null?0:INT_Temp.ServiceInterval.VGA_Months__c)+Constant_MONTH))){
                        INT_Temp.Available=false;
                        tempz.MissedIntervalList.add(INT_Temp);
                    }else{
                        INT_Temp.Available=true;
                        tempz.RemainingIntervalList.add(INT_Temp);
                    }
                    
                }
            }  
            return tempz;
        }catch(Exception ex) {
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return Policy;
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description The method sorts the vehicle policies as present,present(SVCPACK),past and future policies.
    * @param  VINWrapperClass with all the policy,assetcorresponding to a vin
    * 
    */ 
    private static void tempMethod(VINWrapperClass VIN_DATA){
        for(PolicyWrapperClass temp:VIN_DATA.Policies){
            VGA_Vehicle_Policy__c tmp=temp.Policy;
            if(tmp.VGA_Policy_Start_Date__c!=null && tmp.VGA_Policy_End_Date__c!=null){
                if(System.today()<tmp.VGA_Policy_Start_Date__c && System.today()<tmp.VGA_Policy_End_Date__c.addMonths(Constant_MONTH)){
                    VIN_DATA.PolicyMap.get('FUTURE').add(temp);
                }else if(System.today()>=tmp.VGA_Policy_Start_Date__c && System.today()<=tmp.VGA_Policy_End_Date__c.addMonths(Constant_MONTH)){
                    if(temp.PolicyComponentType==Constant_ComponentType){
                        VIN_DATA.PolicyMap.get('PRESENT_SVC').add(temp);
                    }else{
                        VIN_DATA.PolicyMap.get('PRESENT').add(temp);
                    }
                }else if(System.today()>tmp.VGA_Policy_Start_Date__c && System.today()>tmp.VGA_Policy_End_Date__c.addMonths(Constant_MONTH)){
                    VIN_DATA.PolicyMap.get('PAST').add(temp);
                }
            }
        }
        VIN_DATA.Policies=null;
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description collect Items & calculate the Total Amount including GST & GST 
    * @param IntervalId: Service Interval Id 
    * @param ItemWrapperClass: Item Information
    * @return 
    */ 
    private static ItemWrapperClass collectItems(String IntervalId,ItemWrapperClass IWC){
        ItemWrapperClass temp_IWC=IWC;
        try{
            List<VGA_Service_Item__c> ItemList =[SELECT VGA_item_description__c, VGA_Total__c, VGA_Service_Plan__c, VGA_Price__c, VGA_Item_type__c, VGA_Active__c, VGA_Item_Number__c, VGA_HPG__c, VGA_Service_Type__c, VGA_Quantity__c, VGA_Brand_Code__c, Name, Id FROM VGA_Service_Item__c where VGA_Active__c=true and VGA_Service_Plan__c=:IntervalId];
            for(VGA_Service_Item__c temp :ItemList){
                if(temp.VGA_Item_type__c=='P'){
                    temp_IWC.Parts+=temp.VGA_Total__c;
                    temp_IWC.ServiceItemMap.get('Parts').add(temp);
                }else if(temp.VGA_Item_type__c=='L'){
                    temp_IWC.Labour+=temp.VGA_Total__c;
                    temp_IWC.ServiceItemMap.get('Labour').add(temp);
                }else if(temp.VGA_Item_type__c=='S'){
                    temp_IWC.Sundries+=temp.VGA_Total__c;
                    temp_IWC.ServiceItemMap.get('Sundries').add(temp);
                } 
                temp_IWC.Total+=temp.VGA_Total__c;
            }
            temp_IWC.GST=Constant_GST==0?0:(temp_IWC.Total*Constant_GST/100);
            //temp_IWC.TotalGST=Math.ceil(temp_IWC.Total+IWC.GST);
            return temp_IWC;
        }catch(Exception ex){
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }   
        return IWC; 
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 24/02019
    * @description Validates the data entered in the screen against the conditions.
    1.Service claim shouldbe made within 14 days of service.
    2.Kilometers shouldn't exceed the Kilometer specified for the particular service interval.
    *@param 
    * @return 
 
    @AuraEnabled
    public static Response ValidateClaim(String IntervalId,String PolicyId, Date ServiceDate, Integer KM){
        Response resp=new Response();
        try{
            VGA_Vehicle_Policy__c Policy= [ SELECT Id,VGA_Component_Code__c,VGA_Component_Type__c,VGA_Policy_End_Date__c,VGA_Policy_Start_Date__c FROM VGA_Vehicle_Policy__c WHERE Id=:PolicyId];
            VGA_Service_Interval__c Interval =[SELECT Id,VGA_Code__c,VGA_Description__c,VGA_KMS__c,VGA_Months__c,VGA_Fixed_Price__c,VGA_Service_Order__c,VGA_Component_Code__c FROM VGA_Service_Interval__c where Id =:IntervalId];
            if(system.today()>=ServiceDate && system.today() <=ServiceDate.addDays(Constant_ClaimLimit)){
                if(KM<=(interval.VGA_KMS__c+Constant_KMS) && ServiceDate <= Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(Interval.VGA_Months__c)+Constant_MONTH)){
                    ItemWrapperClass IWC=new ItemWrapperClass();
                    IWC.IntervalDescription=Interval.VGA_Code__c+'-'+Interval.VGA_Description__c;
                    IWC=collectItems(IntervalId, IWC);
                    resp.pass=true;
                    resp.result=IWC;
                }else{
                    resp.pass=false;
                    resp.Error.add('1-Service cannot be claimed,Please check Kilometre/Service Date ');
                }
            }else{
                resp.pass=false;
                resp.Error.add('2-Please contact your ABDM,Please check Service Date');
            }

        }catch(Exception ex){
            resp.pass=false;
            resp.Error.add('Unable to find Policy');
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return resp;
    }
    
    */
    /**
    * @author Lidiya Elizabeth
    * @date 23/05/2019
    * @description The method saves the claim data from page to the claim object in database
    *@param data :claim object data as a string
    * @return Response :returns the fields Name,VGA_Total_Ex_GST__cto be displayed in screen.
    */ 
    @AuraEnabled
    public  static Response saveClaim(String data){
        Response resp=new Response();
        try{
            ClaimWrapperClass CWC=(ClaimWrapperClass)JSON.deserialize(data,ClaimWrapperClass.class);
            VGA_Service_Claim__c objClaim = new VGA_Service_Claim__c();
            objClaim.VGA_RO_Number__c = CWC.RONumber;
            objClaim.VGA_KMS__c =Decimal.valueOf(CWC.KM);
            objClaim.VGA_Service_Interval__c = CWC.IntervalId;
            objClaim.VGA_Vehicle_Policy__c = CWC.PolicyId;
            objClaim.VGA_Dealer__c = CWC.DealerId;
            objClaim.VGA_Asset__c = CWC.AssetId;
            objClaim.VGA_Service_Date__c=Date.valueOf(CWC.ServiceDate);
            ItemWrapperClass IWC=new ItemWrapperClass();
            IWC=collectItems(CWC.IntervalId,IWC);
            objClaim.VGA_Total_Ex_GST__c=IWC.Total;
            objClaim.VGA_GST__c=IWC.GST;
            insert objClaim;
            objClaim=[select Id,Name,VGA_Total_Ex_GST__c from VGA_Service_Claim__c where Id=:objClaim.Id limit 1 ];
            resp.pass=true;
            resp.result=objClaim;
        }catch(Exception ex){
            resp.pass=false;
            resp.Error.add('Service cannot be claimed ');
        }
        return resp;
    }   
    
     /**
    * @author Mohammed Ishaque Shaikh
    * @date 24/02019
    * @description Validates the data entered in the screen against the conditions.
        1.Service claim shouldbe made within 14 days of service.
        2.Kilometers shouldn't exceed the Kilometer specified for the particular service interval.
    *@param IntervalId:  recordId  Used to Retrieve Master Record from VGA_Service_InterVal__c Object
    *@param PolicyId:  recordId  Used to Retrieve Master Record from VGA_Vehicle_Policy__c Object
    *@param ServiceDate:  Date of Service Information
    *@param KM:  Kilometere Information
    *@return  Reponse Wrapper with valid information in Result Attribute if it satisfy all condition, then this methond  will provide VGA_ServiceItem__c information(ItemWrapperClass) else it will provide Error Messages
    */
    @AuraEnabled
    public static Response ValidateClaim(String IntervalId,String PolicyId, Date ServiceDate, Integer KM,String VehicleId){
        Response resp=new Response();
        try{
            boolean KMS_Bol=false;
            boolean Date_Bol_1=false;
            boolean Date_Bol_2=false;
            boolean Other_Bol=false;
            boolean Claim_Bol=false;
            boolean Policy_Bol =false;

            VGA_Vehicle_Policy__c Policy= [ SELECT Id,VGA_Component_Code__c,VGA_Component_Type__c,VGA_Policy_End_Date__c,VGA_Policy_Start_Date__c FROM VGA_Vehicle_Policy__c WHERE Id=:PolicyId];
            VGA_Service_Interval__c Interval =[SELECT Id,VGA_Code__c,VGA_Description__c,VGA_KMS__c,VGA_Months__c,VGA_Fixed_Price__c,VGA_Service_Order__c,VGA_Component_Code__c FROM VGA_Service_Interval__c where Id =:IntervalId];
            List<AggregateResult> ARList=[select max(VGA_KMS__c) from VGA_Service_Claim__c where VGA_Asset__c = :VehicleId and VGA_Vehicle_Policy__c=:PolicyId ];
            Decimal x =(Decimal) (ARList[0].get( 'expr0' )==null?null:ARList[0].get( 'expr0' ));
            if(x!=null){
                if(KM<x){
                    Claim_Bol=true;
                }
            }
            System.debug('x>>'+x);
            Policy_Bol=Policy==null?true:(Policy.VGA_Policy_Start_Date__c==null?true:false);
            System.debug('Policyyyy>>'+Policy_Bol+'  policy>>'+ policy);
            if(!Policy_Bol){
                if(Policy.VGA_Policy_Start_Date__c.addmonths(Integer.valueOf((Interval.VGA_Months__c==null?0:Interval.VGA_Months__c)+Constant_MONTH)) < ServiceDate){
                    Policy_Bol=true;
                    
                }
                if(system.today()<ServiceDate && !Policy_Bol){
                    Date_Bol_1=true;
                }else if(ServiceDate<system.today().addDays(-Constant_ClaimLimit) && !Policy_Bol){
                    Date_Bol_2=true;
                }
                System.debug(Policy.VGA_Policy_Start_Date__c.addmonths(Integer.valueOf((Interval.VGA_Months__c==null?0:Interval.VGA_Months__c)+Constant_MONTH)));
                System.debug(ServiceDate);
                System.debug('Policyyyy>>'+Policy_Bol+'  policy>>'+ policy);
            }
            if(KM>(interval.VGA_KMS__c+Constant_KMS)){
                KMS_Bol=true; 
            }
            if(!KMS_Bol && !Date_Bol_1 && !Date_Bol_2  && Policy_Bol){
                resp.pass=false;
                //resp.Error.add('2-Service date is greater than the maximum posssible service date for the interval.');
                resp.Error.add('POLICY');
                resp.Error.add('Date is out of Max-End for the service selected.');
                resp.Error.add('KM is out of Max KM for the service selected.');

            }else if(KMS_Bol && !Date_Bol_1 && !Date_Bol_2  && !Policy_Bol){
                resp.pass=false;
                resp.Error.add('KM');
                resp.Error.add('KM is out of Max KM for the service selected.');
            }else if(KMS_Bol && !Date_Bol_1 && !Date_Bol_2  && Policy_Bol){
                resp.pass=false;
                resp.Error.add('POLICY');
                resp.Error.add('Date is out of Max-End for the service selected.');
                resp.Error.add('KM is out of Max KM for the service selected.');
            }else if(KMS_Bol && (Date_Bol_1 || Date_Bol_2) && !Policy_Bol){
                resp.pass=false;
                resp.Error.add('POLICY');
                if(Date_Bol_1){
                    resp.Error.add('Service Date cannot be claimed on Future date');
                }else{
                    resp.Error.add('Service date is greater than 14 days,Please contact your VGA Area Manager ');
                }
                resp.Error.add('KM is out of Max KM for the service selected.');
            }else if(!KMS_Bol && !Policy_Bol && (Date_Bol_1 || Date_Bol_2)){
                resp.pass=false;
                resp.Error.add('DATE');
                if(Date_Bol_1){
                    resp.Error.add('Service cannot be claimed on Future date ');
                }else if(Date_Bol_2){
                    resp.Error.add('Service date is greater than 14 days,Please contact your VGA Area Manager ');
                }
            }
            System.debug('Policy_Bol>>'+Policy_Bol);
            System.debug('Date_Bol_1>>'+Date_Bol_1);
            System.debug('Date_Bol_2>>'+Date_Bol_2);
            System.debug('KMS_Bol>>'+KMS_Bol);
            if(!KMS_Bol && !Date_Bol_1 && !Date_Bol_2 && !Policy_Bol){
                if(Claim_Bol){
                    resp.pass=false;
                    resp.Error.add('KM');
                    resp.Error.add('Service cannot be claimed,Please check Kilometre');
                    return resp;
                }else{
                    ItemWrapperClass IWC=new ItemWrapperClass();
                    IWC.IntervalDescription=Interval.VGA_Code__c+''+(Interval.VGA_Description__c==null?'':'-'+Interval.VGA_Description__c);
                    IWC=collectItems(IntervalId, IWC);
                    resp.pass=true;
                    resp.result=IWC;
                } 
            }
        }catch(Exception ex){
            resp.pass=false;
            resp.Error.add('Unable to find Policy '+ex.getMessage()+'>>'+ex.getLineNumber());
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return resp;
    }
    
   public static datetime formatDate(Date d) {
       datetime dateTimeValue = d;
       return dateTimeValue;
    }
    

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Wrapper classs used to send information to component in specific format.
    */ 
    public class Response{
        @AuraEnabled public boolean pass;
        @AuraEnabled public Object result;
        @AuraEnabled public List<String> Error;

        public Response(){
            pass=false;
            result=null;
            Error=new List<String>();
        }
    }
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 29/06/2019
    * @description Method is used to contruct Query String  
    *@param ComponentCodeSet:  contains component code set which  needs to be included in Query  String 
    */ 
    public Static String generateQuery(Set<String> ComponentCodeSet){
        try{
            List<String> dataQueryList=new List<String>();
            for(String s:ComponentCodeSet){
                dataQueryList.add(' VGA_Component_Code__c LIKE \'%'+s+'%\'');
            }
            System.debug(dataQueryList);
            
            return String.join(dataQueryList,' OR ');
        }catch(Exception ex){
            System.debug(ex.getMessage()+'>>'+ex.getLineNumber());
        }
        return '';
    }
}