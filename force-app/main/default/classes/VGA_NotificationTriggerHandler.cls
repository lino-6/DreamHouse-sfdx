public without sharing class VGA_NotificationTriggerHandler 
{
    public class jsonInput 
    {
        public String record_id;   
        public String from_id;   
        public String to;   
        public String textbody; 
    }
    
    public void onbeforeInsert(List<VGA_Dealer_Notification__c> triggerNew) 
    {
        
    }
    
    public void onafterInsert(List<VGA_Dealer_Notification__c> triggerNew) 
    {
        Id smsId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
        Id emailId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
        Set<Id> setId = new Set<Id>();
        Set<Id> setNot = new Set<Id>();
        
        for(VGA_Dealer_Notification__c objsub : triggerNew)
        {
            if(objsub.recordtypeId == smsId && !objsub.VGA_Inserted_From_Batch__c)
            {
                setId.add(objsub.Id);
            }
            else if(objsub.recordtypeId == emailId && !objsub.VGA_Inserted_From_Batch__c)
            {
                setNot.add(objsub.id);
            }
        }
        
        if(setId != Null && !setId.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
        {
            sendSMS(setId);
        }
        
        if(setNot != Null && !setNot.isEmpty() && System.IsBatch() == false && System.isFuture() == false)
        {
            sendEmail(setNot);
        }
    }
    
    //======================================================================================================================
    //Description : This Method is used to send email to dealer contacts from trigger
    //======================================================================================================================
    //Name:Pawan Mudgal         Date:04-12-17 
    //======================================================================================================================
    @future
    public static void sendEmail(Set<Id> setid)
    {
        Map<String,VGA_Notification_Setting__c> mapNotification = VGA_Notification_Setting__c.getAll();
        
        if(mapNotification.get('Email') != null && !mapNotification.get('Email').VGA_Process__c)
            return;
            
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        List<VGA_Dealer_Notification__c> lstUpdateEmail = new List<VGA_Dealer_Notification__c>();
        List<VGA_Dealer_Notification__c> lstEmails = [select Id, Dealer_Contact__c, VGA_Sent_From__c, VGA_Sent_To__c, VGA_Message_Body__c, VGA_Subscription_Type__c from VGA_Dealer_Notification__c where Id IN : setId];
        List<OrgWideEmailAddress> lstOrgEmailAddress = [select Id, DisplayName, Address from OrgWideEmailAddress where DisplayName = 'VGA Notifications'];
        
        try
        {
            for(VGA_Dealer_Notification__c objemail : lstEmails)
            {
                if(objemail.Dealer_Contact__c != Null && objemail.VGA_Message_Body__c != Null)
                {
                    Messaging.SingleEmailMessage  mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(objemail.Dealer_Contact__c);
                    if(objemail.VGA_Subscription_Type__c == 'New Lead Notification')
                    {
                        mail.setSubject('New Lead Notification');
                    }
                    if(lstOrgEmailAddress != null && !lstOrgEmailAddress.isEmpty())
                    {
                        mail.setOrgWideEmailAddressId(lstOrgEmailAddress[0].id);    
                    }  
                    mail.setPlainTextBody(objemail.VGA_Message_Body__c);
                    mail.setSaveAsActivity(false);
                    lstEmail.add(mail);
                    
                    objemail.VGA_Notification_Sent__c = true;
                    objemail.VGA_Sent_Date_Time__c = System.Now();
                    lstUpdateEmail.add(objemail);
                }
            }
        
            if(lstEmail != Null && !lstEmail.isEmpty())
            {
                Messaging.sendEmail(lstEmail);
                
                if(lstUpdateEmail != Null && !lstUpdateEmail.isEmpty())
                {
                    update lstUpdateEmail;
                }
            }
        }
        catch(Exception e)
        {
            VGA_Common.sendExceptionEmail('Exception in sending notification email', 'Hi, <br/><br/> Email Notification failed due to following error : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
        }
    }
    
    //======================================================================================================================
    //Description : This Method is used to send sms to dealer contacts from trigger
    //======================================================================================================================
    //Name:Pawan Mudgal         Date:04-12-17 
    //======================================================================================================================
    @future(callout=true)
    public static void sendSMS(Set<Id> setid)
    {
    	system.debug('got inside send SMS');
        Map<String,VGA_Notification_Setting__c> mapNotification = VGA_Notification_Setting__c.getAll();
        
        if(mapNotification.get('SMS') != null && !mapNotification.get('SMS').VGA_Process__c)
            return;
        
        List<jsonInput> lstjsonInput;
        VGA_SMSResponse objjsonOutput;
        List<VGA_Dealer_Notification__c> lstSms = [select Id, VGA_Sent_From__c, VGA_Sent_To__c, VGA_Message_Body__c from VGA_Dealer_Notification__c where Id IN : setId];
        List<VGA_Dealer_Notification__c> lstUpdateSms = new List<VGA_Dealer_Notification__c>();
        
        try
        {
            lstjsonInput = new List<jsonInput>();
            
            for(VGA_Dealer_Notification__c objsms : lstSms)
            {
                string strSentTo = '';
                
                if(objsms.VGA_Sent_To__c != Null && objsms.VGA_Message_Body__c != Null)
                {
                    if((objsms.VGA_Sent_To__c.substring(0,3) == '+61') || (objsms.VGA_Sent_To__c.substring(0,3) == '+91'))
                    {
                        strSentTo = objsms.VGA_Sent_To__c;
                    }
                    else if(objsms.VGA_Sent_To__c.substring(0,3) != '+61')
                    {
                        if(objsms.VGA_Sent_To__c.substring(0,1) == '0')
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c.substring(1);
                        }
                        else
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c;
                        }
                    }
                    else if(objsms.VGA_Sent_To__c.substring(0,2) != '61')
                    {
                        if(objsms.VGA_Sent_To__c.substring(0,1) == '0')
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c.substring(1);
                        }
                        else
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c;
                        }
                    }
                    
                    jsonInput obj = new jsonInput();
                    obj.record_id = objsms.Id;
                    obj.from_id = objsms.VGA_Sent_From__c;
                    obj.to = strSentTo;
                    obj.textbody = objsms.VGA_Message_Body__c;
                  
                    lstjsonInput.add(obj);
                }
            }
        
            String strBody = '';
            
            if(lstjsonInput != Null && !lstjsonInput.isEmpty())
            {
                strBody = JSON.serialize(lstjsonInput);
                system.debug('@@@@strBody@@@'+strBody);
            }
            
            if(strBody != Null)
            {
                Http objhttp = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint('callout:Mule_SMS_Credentials/sendsms');
                req.setMethod('POST');
                req.setTimeout(120000);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(strBody);
                HttpResponse res;
                
                if(!test.isRunningTest())
                {
                    res = objhttp.send(req);
                   
                    HttpResponse res1;
                    res1 = res;
                    system.debug('@@@@'+res1.getBody());
                    system.debug('@@@@code'+res1.getStatusCode());
                }
                else
                {
                    res = new HTTPResponse();
                    res.setBody('[{"record_id":"a040k000003o0ZjAAI","message_id":null,"send_at":null,"recipients":null,"cost":null,"sms":null,"delivery_stats":null,"error":{"code":"error"}},{"record_id":"a040k000003o0ZkAAI","message_id":98535840,"send_at":"2017-12-15 10:57:15","recipients":1,"cost":0.064,"sms":1,"delivery_stats":{"delivered":0,"pending":0,"bounced":0,"responses":0,"optouts":0},"error":{"code":"SUCCESS","description":"OK"}}]');
                }
                
                String apiResponse =  res.getBody();
                
                if(apiResponse.substring(0,1) == '{')
                {
                    apiResponse = '['+apiResponse+']';
                }
                
                system.debug('@@@@'+res.getBody()); 
                apiResponse = '{"vga":' + apiResponse + '}';
               
                objjsonOutput = VGA_SMSResponse.parse(apiResponse);
                system.debug('@@@@@objjsonOutput' + objjsonOutput);
                
                if(objjsonOutput != Null)
                {
                    List<VGA_SMSResponse.cls_vga> lstres = objjsonOutput.vga;
                    
                    for(VGA_SMSResponse.cls_vga objoutput : lstres)
                    {
                        if(objoutput.record_id != Null)
                        {
                            VGA_Dealer_Notification__c objnot = new VGA_Dealer_Notification__c(Id = objoutput.record_id);
                            objnot.VGA_Sent_Date_Time__c = System.Now();
                            objnot.VGA_Notification_Sent__c = true;
                            objnot.VGA_Notification_Response__c = String.valueof(JSON.serialize(objoutput));
                            lstUpdateSms.add(objnot);
                        }
                    }
                    
                    if(lstUpdateSms != Null && !lstUpdateSms.isEmpty())
                    {
                        update lstUpdateSms;
                    }
                }
            }
        }
        catch(Exception e)
        {
            VGA_Common.sendExceptionEmail('Exception in sending notification sms', 'Hi, <br/><br/> Sms Notification failed due to following error : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
        }
    }
    
    //======================================================================================================================
    //Description : This Method is used to send email to dealer contacts from batch
    //======================================================================================================================
    //Name:Pawan Mudgal         Date:04-12-17 
    //======================================================================================================================
    public static void sendEmailFromBatch(List<VGA_Dealer_Notification__c> lstEmails)
    {
        Map<String,VGA_Notification_Setting__c> mapNotification = VGA_Notification_Setting__c.getAll();
        
        if(mapNotification.get('Email') != null && !mapNotification.get('Email').VGA_Process__c)
            return;
            
        List<Messaging.SingleEmailMessage> lstEmail = new List<Messaging.SingleEmailMessage>();
        List<VGA_Dealer_Notification__c> lstUpdateEmail = new List<VGA_Dealer_Notification__c>();
        List<OrgWideEmailAddress> lstOrgEmailAddress = [select Id, DisplayName, Address from OrgWideEmailAddress where DisplayName = 'VGA Notifications'];
        
        try
        {
            for(VGA_Dealer_Notification__c objemail : lstEmails)
            {
                if(objemail.Dealer_Contact__c != Null && objemail.VGA_Message_Body__c != Null)
                {
                    Messaging.SingleEmailMessage  mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(objemail.Dealer_Contact__c);
                    
                    if(lstOrgEmailAddress != null && !lstOrgEmailAddress.isEmpty())
                    {
                        mail.setOrgWideEmailAddressId(lstOrgEmailAddress[0].id);    
                    }
                    
                    if(objemail.VGA_Subscription_Type__c == 'New Lead Notification')
                    {
                        mail.setSubject('New Lead Notification');
                    }
                    else if(objemail.VGA_Subscription_Type__c == 'All Escalations')
                    {
                        mail.setSubject('Lead Escalation Notification');
                    }
                    mail.setPlainTextBody(objemail.VGA_Message_Body__c);
                    mail.setSaveAsActivity(false);
                    lstEmail.add(mail);
                    
                    objemail.VGA_Notification_Sent__c = true;
                    objemail.VGA_Inserted_From_Batch__c = false;
                    objemail.VGA_Sent_Date_Time__c = System.Now();
                    lstUpdateEmail.add(objemail);
                }
            }
        
            if(lstEmail != Null && !lstEmail.isEmpty())
            {
                Messaging.sendEmail(lstEmail);
                
                if(lstUpdateEmail != Null && !lstUpdateEmail.isEmpty())
                {
                    update lstUpdateEmail;
                }
            }
        }
        catch(Exception e)
        {
            VGA_Common.sendExceptionEmail('Exception in sending notification email from batch processing', 'Hi, <br/><br/> Email Notification failed due to following error : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
        }
    }
        
    //======================================================================================================================
    //Description : This Method is used to send sms to dealer contacts from batch
    //======================================================================================================================
    //Name:Pawan Mudgal         Date:04-12-17 
    //======================================================================================================================
    public static void sendSMSFromBatch(List<VGA_Dealer_Notification__c> lstSms)
    {
        Map<String,VGA_Notification_Setting__c> mapNotification = VGA_Notification_Setting__c.getAll();
        
        if(mapNotification.get('SMS') != null && !mapNotification.get('SMS').VGA_Process__c)
            return;
        
        List<jsonInput> lstjsonInput;
        VGA_SMSResponse objjsonOutput;
        List<VGA_Dealer_Notification__c> lstUpdateSms = new List<VGA_Dealer_Notification__c>();
        String apiResponse='';
        
        try
        {
            lstjsonInput = new List<jsonInput>();
            
            for(VGA_Dealer_Notification__c objsms : lstsms)
            {
                string strSentTo = '';
                
                if(objsms.VGA_Sent_To__c != Null && objsms.VGA_Message_Body__c != Null)
                {
                    if((objsms.VGA_Sent_To__c.substring(0,3) == '+61') || (objsms.VGA_Sent_To__c.substring(0,3) == '+91'))
                    {
                        strSentTo = objsms.VGA_Sent_To__c;
                    }
                    else if(objsms.VGA_Sent_To__c.substring(0,3) != '+61')
                    {
                        if(objsms.VGA_Sent_To__c.substring(0,1) == '0')
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c.substring(1);
                        }
                        else if(objsms.VGA_Sent_To__c.substring(0,1) != '0')
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c;
                        }
                    }
                    else if(objsms.VGA_Sent_To__c.substring(0,2) != '61')
                    {
                        if(objsms.VGA_Sent_To__c.substring(0,1) == '0')
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c.substring(1);
                        }
                        else
                        {
                            strSentTo = '+61' + objsms.VGA_Sent_To__c;
                        }
                    }
                    jsonInput obj = new jsonInput();
                    obj.record_id = objsms.Id;
                    obj.from_id = objsms.VGA_Sent_From__c;
                    obj.to = strSentTo;
                    obj.textbody = objsms.VGA_Message_Body__c;
                
                    lstjsonInput.add(obj);
                }
            }
        
            String strBody = '';
            
            if(lstjsonInput != Null && !lstjsonInput.isEmpty())
            {
                strBody = JSON.serialize(lstjsonInput);
                system.debug('@@@@strBody@@@'+strBody);
            }
            
            if(strBody != Null)
            {
                Http objhttp = new Http();
                HttpRequest req = new HttpRequest();
                req.setEndpoint('callout:Mule_SMS_Credentials/sendsms');
                req.setMethod('POST');
                req.setTimeout(120000);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(strBody);
                HttpResponse res;
                system.debug('@@@ SMS Request Body: ' + strBody + '@@@');
                if(!test.isRunningTest())
                {
                    res = objhttp.send(req);                   
                    HttpResponse res1;
                    res1 = res;
                    system.debug('@@@@'+res1.getBody());
                    system.debug('@@@@code'+res1.getStatusCode());
                }
                else
                {
                    res = new HTTPResponse();
                    res.setBody('[{"record_id":"a040k000003o0ZjAAI","message_id":null,"send_at":null,"recipients":null,"cost":null,"sms":null,"delivery_stats":null,"error":{"code":"error"}},{"record_id":"a040k000003o0ZkAAI","message_id":98535840,"send_at":"2017-12-15 10:57:15","recipients":1,"cost":0.064,"sms":1,"delivery_stats":{"delivered":0,"pending":0,"bounced":0,"responses":0,"optouts":0},"error":{"code":"SUCCESS","description":"OK"}}]');
                }
                
                apiResponse =  res.getBody();
                system.debug('@@@@ Api Response: '+ apiResponse);
                if(apiResponse.substring(0,1) == '{')
                {
                    apiResponse = '['+apiResponse+']';
                }
                
                system.debug('@@@@'+ res.getBody()); 
                system.debug('@@@ API Response:' + apiResponse);
                apiResponse = '{"vga":' + apiResponse + '}';
               
                objjsonOutput = VGA_SMSResponse.parse(apiResponse);
                system.debug('@@@@@objjsonOutput: bulk sms:' + objjsonOutput);
                
                if(objjsonOutput != Null)
                {
                    List<VGA_SMSResponse.cls_vga> lstres = objjsonOutput.vga;
                    
                    for(VGA_SMSResponse.cls_vga objoutput : lstres)
                    {
                        if(objoutput.record_id != Null)
                        {
                            VGA_Dealer_Notification__c objnot = new VGA_Dealer_Notification__c(Id = objoutput.record_id);
                            objnot.VGA_Sent_Date_Time__c = System.Now();
                            objnot.VGA_Notification_Sent__c = true;
                            objnot.VGA_Notification_Response__c = String.valueof(JSON.serialize(objoutput));
                            lstUpdateSms.add(objnot);
                        }
                    }
                    
                    if(lstUpdateSms != Null && !lstUpdateSms.isEmpty())
                    {
                        update lstUpdateSms;
                    }
                }
                else
                {
                	//response not recieved, update the notification object with error
                	for(VGA_Dealer_Notification__c objsms : lstsms)
            		{
                		VGA_Dealer_Notification__c objnot = new VGA_Dealer_Notification__c(Id = objsms.Id);
                	    objnot.VGA_Sent_Date_Time__c = System.Now();
                        objnot.VGA_Notification_Sent__c = true;
                        objnot.VGA_Notification_Response__c = 'No response received from the SMS gateway';
                        lstUpdateSms.add(objnot);
                	}
                	//update
                	if(lstUpdateSms != Null && !lstUpdateSms.isEmpty())
                    {
                        update lstUpdateSms;
                    }
                }
            }
        }
        catch(Exception e)
        {
        	system.debug('Exception: ' + e.getMessage() +'at line number ' + e.getLineNumber());
            VGA_Common.sendExceptionEmail('Exception in sending notification batch sms', 'Hi, <br/><br/> Sms Notification failed due to following error : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
            
            //response not recieved, update the notification object with error
        	for(VGA_Dealer_Notification__c objsms : lstsms)
    		{
        		VGA_Dealer_Notification__c objnot = new VGA_Dealer_Notification__c(Id = objsms.Id);
        	    objnot.VGA_Sent_Date_Time__c = System.Now();
                objnot.VGA_Notification_Sent__c = true;
                objnot.VGA_Notification_Response__c = 'No response received from the SMS gateway';
                lstUpdateSms.add(objnot);
        	}
        	//update
        	if(lstUpdateSms != Null && !lstUpdateSms.isEmpty())
            {
                update lstUpdateSms;
            }
        }
    }
}