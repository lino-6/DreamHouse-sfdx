@RestResource(urlMapping='/idsdataprocessingforsales')
global with sharing class IdsDataProcessorForSales {

    @HttpPost   
    global static String processData(String seraializedAccount,String serializedVehicle,String serializedContact,
            String serializedPolicy,String serializedOwnership,String serializedPreviousOwners,String serializedCustomerOrder,
            String serializedCommonData, String serializedRoadsidePolicy) 
    {
        
        Account sAccount;
        Asset sVehicle;
        Contact sContact;
        VGA_Vehicle_Policy__c sPolicy;
        VGA_Vehicle_Policy__c sRoadsidePolicy;
        VGA_Ownership__c sOwnership;
        VGA_Ownership__c sPreviousOwnership;
        VGA_Customer_Order_Details__c sCustomerOrder;
        List<Lead> listLeads=null;
        CommonData cData;
        Product2 model;
        Asset relatedVehicle;
        Asset assetId;
        Account dealer;
        Schema.SObjectField extID;
        Account acc=null;
        Boolean bypassProcess=false;     
        
        Database.UpsertResult upsertAccount;
        Database.UpsertResult upsertVehicle;
        Database.SaveResult upsertContact;
        Database.UpsertResult upsertPolicy;
        Database.UpsertResult upsertOwnership;
        Database.UpsertResult upsertPreviousOwnership;
        Database.UpsertResult upsertCustomerOrder;
        Database.UpsertResult upsertRoadsidePolicy;

        Schema.SObjectField fieldVin;
        
        String sfResponse='';
        Boolean allSuccess=true;
        ApexResponseToMule sfResponseToMule=new ApexResponseToMule();
        sfResponseToMule.processFlag=1;
        Savepoint sp = Database.setSavepoint();
        try
        {
            
            if(serializedCommonData!=null){
                cData=(CommonData)JSON.deserialize(serializedCommonData,CommonData .class);
                if(cData.cModel!=null && cData.cModel!='')
                    model=new Product2(VGA_primary_Id__c=cData.cModel);
                if(cData.cVehicle!=null && cData.cVehicle!='')
                    relatedVehicle=new Asset(VGA_VIN__c=cData.cVehicle);
                if(cData.cDealer!=null && cData.cDealer!='')
                {
                    dealer=new Account(VGA_Dealer_Code__c=cData.cDealer);
                }
            }
            if(!(cData.cVehicleUse=='DD'))
            {
                    Id skodaDealerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Dealer Account').getRecordTypeId();
                    Id vwDealerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Dealer Account').getRecordTypeId();
                    if(seraializedAccount !=null)
                    {
                        sAccount=(Account)JSON.deserialize(seraializedAccount,Account.class);
                        if(sAccount.Id!=null)
                            acc=[select RecordTypeId from Account where Id = :sAccount.Id];
                        if(acc==null 
                           || (acc.RecordTypeId!=skodaDealerRecordTypeId
                                && acc.RecordTypeId!=vwDealerRecordTypeId) )
                        {
                                 upsertAccount = Database.upsert(sAccount,true);
                        }
                        else
                        {
                            bypassProcess=true;
                        }
                    }
            }           

            System.debug(' serializedVehicle ------->  ' + serializedVehicle );
            //vehicle can be null
            if(serializedVehicle!=null && !bypassProcess)
            {
                
                sVehicle=(Asset)JSON.deserializeStrict(serializedVehicle,Asset.class);
                System.debug(' sVehicle ------->  ' + sVehicle );
                
                sVehicle.VGA_Model__r=model;
                if(cData.cVehicleUse=='DD')
                {
                   sVehicle.Account=dealer;
                }
                else
                {
                    if(upsertAccount!=null)
                    {
                        sVehicle.AccountId=upsertAccount.getId();
                    }
                }
                //If there is no customer order we will use the vechicle external Id to upsert the vehicle record
                if(serializedCustomerOrder!=null ){
                    //get the externalid and use it for upsert - VGA_Asset_ExternalID__c
                    Schema.SObjectField vehicleExternalId = Asset.Fields.VGA_Asset_ExternalID__c;
                    sVehicle.AssetProvidedBy = dealer;

                    System.debug(' VEHICLE ------->  ' + sVehicle );
                    upsertVehicle = Database.upsert(sVehicle,vehicleExternalId,true);
                }
                else{
                    System.debug(' VEHICLE 1 ------->  ' + sVehicle );
                    fieldVin = Asset.Fields.VGA_VIN__c;
                    upsertVehicle = Database.upsert(sVehicle,fieldVin,true);
                }
            }

            if(serializedContact!=null && !bypassProcess)
            {
                sContact=(Contact)JSON.deserialize(serializedContact,Contact.class);
                if(cData.cVehicleUse=='DD')
                   sContact.Account=dealer;
                else
                {
                    if(upsertAccount!=null)
                    {
                        sContact.AccountId=upsertAccount.getId();
                    }
                }                    
                sContact.VGA_External_Id__c=null;
                
                //--- Bypassing Duplicate Rule ----//
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true; 
                
                if(sContact.Id == null) {
                    upsertContact = Database.insert(sContact, dml);
                }
                else {
                    upsertContact = Database.update(sContact, dml);
                }             
            }
            
            if(serializedPolicy!=null && !bypassProcess){
                sPolicy=(VGA_Vehicle_Policy__c)JSON.deserialize(serializedPolicy,VGA_Vehicle_Policy__c.class);
                extId=VGA_Vehicle_Policy__c.Fields.VGA_External_Id__c;               
                sPolicy.VGA_VIN__r=relatedVehicle;
                upsertPolicy = Database.upsert(sPolicy,extId,false);
            }
            
            //save standard roadside policy
            if(serializedRoadsidePolicy!=null && !bypassProcess){
                sRoadsidePolicy=(VGA_Vehicle_Policy__c)JSON.deserialize(serializedRoadsidePolicy,VGA_Vehicle_Policy__c.class);
                extId=VGA_Vehicle_Policy__c.Fields.VGA_External_Id__c;               
                sRoadsidePolicy.VGA_VIN__r=relatedVehicle;
                upsertRoadsidePolicy = Database.upsert(sRoadsidePolicy,extId,false);
            }
            /**
            * @author Archana Yerramilli
            * @date 14/01/2018
            * @description Ticket#731 - Created a lookup field VGA_Contact__c on Ownership Object with lookup to Contact Object.
            * @description Ticket#731 - Updated the below code to save contact ID for the new lookup field VGA_Contact__c on Ownership Object. 
            */
            System.debug(' Ownership ---->  ' + serializedOwnership );
            System.debug(' bypassProcess ---->  ' + bypassProcess );
            if(serializedOwnership!=null && !bypassProcess)
            {
 
                sOwnership=(VGA_Ownership__c)JSON.deserialize(serializedOwnership,VGA_Ownership__c.class);
                sOwnership.VGA_VIN__r=relatedVehicle;
                //Add Contact ID to the new lookup field on as per Ticket#731
                //01-Apr-2019 Added the condition to skip contact update when cVehicleUse is DD
                if(upsertContact !=null && upsertContact.getId() != null && cData.cVehicleUse!='DD' )
                {
                    sOwnership.VGA_Contact__c = upsertContact.getId();
                }
                         
                if(cData.cVehicleUse=='DD')
                {
                    if(sOwnership.VGA_Owner_Name__c==null)
                    {
                            sOwnership.VGA_Owner_Name__c=[Select Id from Account where VGA_Dealer_Code__c =: dealer.VGA_Dealer_Code__c][0].Id;
                          
                    }
                }
                else 
                {
                    if(upsertAccount!=null)
                    {
                        sOwnership.VGA_Owner_Name__c=upsertAccount.getId();  
                    }
                }
                
                sOwnership.VGA_Dealership__r=dealer;
                upsertOwnership = Database.upsert(sOwnership,true);
                
                if(upsertOwnership.isSuccess() && serializedPreviousOwners!=null)
                {
                    sPreviousOwnership=(VGA_Ownership__c)JSON.deserialize(serializedPreviousOwners,VGA_Ownership__c.class);
                    upsertPreviousOwnership=Database.upsert(sPreviousOwnership,true);
                }
            }
            
            if(serializedCustomerOrder!=null && !bypassProcess)
            {
                sCustomerOrder=(VGA_Customer_Order_Details__c)JSON.deserialize(serializedCustomerOrder,VGA_Customer_Order_Details__c.class);
                extId=VGA_Customer_Order_Details__c.Fields.VGA_External_Id__c;
                sCustomerOrder.VGA_Dealer_Account__r=dealer;
                sCustomerOrder.VGA_Model__r=model;
                //Below Line has been Added as a part of Ticket #9 VGA-10041--Vehicle-Stock-Data-into-Salesforce//
                //This will populate Asset value on Customer Order
                sCustomerOrder.VGA_Asset__r =relatedVehicle;
                if(upsertAccount!=null)
                {
                    sCustomerOrder.VGA_Customer__c=upsertAccount.getId();
                }
                //Add Contact ID to the new lookup field on as per Ticket#731
                //01-Apr-2019 Added the condition to skip contact update when cVehicleUse is DD
                if(upsertContact !=null && upsertContact.getId() != null && cData.cVehicleUse!='DD' )
                {
                    sCustomerOrder.VGA_Contact__c = upsertContact.getId();
                }

                upsertCustomerOrder = Database.upsert(sCustomerOrder,extId,true);
            }
             
            
            if(upsertAccount==null){
                sfResponseToMule.accountIdOrError='No action on Account';
            }
            else if(upsertAccount.isSuccess())
            {
                {
                    sfResponseToMule.accountIdOrError=upsertAccount.getId();
                    listLeads=new List<Lead>([SELECT id,Status FROM Lead where VGA_Customer_Account__c=:upsertAccount.getId() and Status!='Closed']);
                    for(Lead l:listLeads)
                        l.Status='Closed';
                    if(listLeads!=null && listLeads.size()>0)
                        update listLeads;
                }
            }
            else
            {
                    sfResponseToMule.accountIdOrError=upsertAccount.getErrors()[0].getMessage();
                    allSuccess=false;
            }

            if(upsertVehicle==null)
                sfResponseToMule.vehicleIdOrError='No action on Vehicle';
            else if(upsertVehicle.isSuccess())
                sfResponseToMule.vehicleIdOrError=upsertVehicle.getId();
            else{
                    sfResponseToMule.vehicleIdOrError=upsertVehicle.getErrors()[0].getMessage();
                    allSuccess=false;
                }


            if(upsertContact==null)
                sfResponseToMule.contactIdOrError='No action on Contact';
            else if(upsertContact.isSuccess())
                sfResponseToMule.contactIdOrError=upsertContact.getId();
            else{
                    sfResponseToMule.contactIdOrError=upsertContact.getErrors()[0].getMessage();
                    allSuccess=false;
                }


            if(upsertPolicy==null)
                sfResponseToMule.policyIdOrError='No action on Vehicle';
            else if(upsertPolicy.isSuccess())
                sfResponseToMule.policyIdOrError=upsertPolicy.getId();
            else{
                    allSuccess=false;
                    sfResponseToMule.policyIdOrError=upsertPolicy.getErrors()[0].getMessage();
                }

            if(upsertOwnership==null)
                sfResponseToMule.ownershipIdOrError='No action on Ownership';
            else if(upsertOwnership.isSuccess())
                sfResponseToMule.ownershipIdOrError=upsertOwnership.getId();
            else{
                    allSuccess=false;
                    sfResponseToMule.ownershipIdOrError=upsertOwnership.getErrors()[0].getMessage();
                }
             
             if(upsertPreviousOwnership==null)
                sfResponseToMule.previousOwnershipIdOrError='No action on Previous Ownerships';
            else if(upsertPreviousOwnership.isSuccess())
                sfResponseToMule.previousOwnershipIdOrError=upsertPreviousOwnership.getId();
            else{
                    allSuccess=false;
                    sfResponseToMule.previousOwnershipIdOrError=upsertPreviousOwnership.getErrors()[0].getMessage();
                }
                
            if(upsertCustomerOrder==null)
                sfResponseToMule.customerOrderIdOrError='No action on Customer Order';
            else if(upsertCustomerOrder.isSuccess())
                sfResponseToMule.customerOrderIdOrError=upsertCustomerOrder.getId();
            else{
                    allSuccess=false;
                    sfResponseToMule.customerOrderIdOrError=upsertCustomerOrder.getErrors()[0].getMessage();
                }

            sfResponseToMule.isSystemException=false;
            if(!allSuccess){
                    Database.rollback(sp);
                    sfResponseToMule.processFlag=3;
                    sfResponseToMule.systemExceptionMessage='All records rolled back.';
                }
            else{
                sfResponseToMule.systemExceptionMessage='Success';
            }

            return JSON.serialize(sfResponseToMule);


        }
        catch (Exception e){
            System.debug('---Exception Message:  '+e.getMessage()+' getLineNumber: '+e.getLineNumber());
            sfResponseToMule.processFlag=3;
            sfResponseToMule.isSystemException=true;
            sfResponseToMule.systemExceptionMessage=e.getMessage();
            Database.rollback(sp);
            return JSON.serialize(sfResponseToMule);
        }       
    }
    public class ApexResponseToMule{
        public Integer processFlag{set;get;}
        public String accountIdOrError{set;get;}
        public String vehicleIdOrError{set;get;}
        public String contactIdOrError{set;get;}
        public String policyIdOrError{set;get;}
        public String ownershipIdOrError{set;get;}
        public String previousOwnershipIdOrError{set;get;}
        public String customerOrderIdOrError{set;get;}
        public Boolean isSystemException{set;get;}
        public String systemExceptionMessage{set;get;}
    }
    public class CommonData{
        public String cVehicle{set;get;}
        public String cDealer{set;get;}
        public String cModel{set;get;}
        public String cVehicleUse{set;get;}
    }
}