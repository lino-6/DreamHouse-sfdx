/**
* @author Lidiya Elizabeth
* @date 24/05/2019
* @description The class is used to test VGA_ServicePlanClaimsController
*/
@isTest
public class VGA_ServicePlanClaimsControllerTest {
    /**
    * @author Lidiya Elizabeth
    * @date 24/05/2019
    * @description It is used to set up the data for test class.
    */
    @TestSetup
    static void buildConfig(){
        Account DealerAccount=VGA_CommonTracker.createDealerAccount();
        Contact DealerContact=VGA_CommonTracker.createDealercontact(DealerAccount.Id);
        User ContactUser=VGA_CommonTracker.CreateUser(DealerContact.Id);
        Product2 Model=VGA_CommonTracker.createProduct('MERCEDES');
        Model.VGA_Brand__c = 'PV Volkswagen';
        Model.VGA_Model_Year__c='2019'; 
        insert Model;
        Asset Vehicle=VGA_CommonTracker.buildVehicle('TESTVIN',DealerAccount.Id);
        Vehicle.Product2Id=Model.Id;
        insert Vehicle;
        Asset Vehicle1=VGA_CommonTracker.buildVehicle('TESTVIN1',DealerAccount.Id);
        Vehicle1.Product2Id=Model.Id;
        insert Vehicle1;
        VGA_Vehicle_Policy__c Policy=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy.VGA_Component_Type__c  = 'SVCPACK';
        Policy.VGA_Component_Code__c  = 'SP3YR_1';
        Policy.Is_Extended_Warranty__c= false;
        Policy.VGA_Policy_Start_Date__c=System.today();
        Policy.VGA_Policy_End_Date__c=System.today().addYears(3);
        insert Policy;

        VGA_Vehicle_Policy__c Policy1=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy1.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy1.VGA_Component_Type__c  = 'SVCPACK';
        Policy1.VGA_Component_Code__c  = 'SP3YR';
        Policy1.Is_Extended_Warranty__c= false;
        Policy1.VGA_Policy_Start_Date__c=System.today().addYears(2);
        Policy1.VGA_Policy_End_Date__c=System.today().addYears(5);
        insert Policy1;

        VGA_Vehicle_Policy__c Policy2=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy2.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy2.VGA_Component_Type__c  = 'SVCPACK';
        Policy2.VGA_Component_Code__c  = 'SP3YR';
        Policy2.Is_Extended_Warranty__c= false;
        Policy2.VGA_Policy_Start_Date__c=System.today().addYears(-5);
        Policy2.VGA_Policy_End_Date__c=System.today().addYears(-2);
        insert Policy2;

        
        List<VGA_Service_Interval__c> IntervalList=new List<VGA_Service_Interval__c>{
            buildServiceInterval('P015','15000kms or 12 months',15000,12,Model.Id,Policy.VGA_Component_Code__c,1),
            buildServiceInterval('P030','30000kms or 24 months',30000,24,Model.Id,Policy.VGA_Component_Code__c,2),
            buildServiceInterval('P045','45000kms or 36 months',45000,36,Model.Id,Policy.VGA_Component_Code__c,3)
        };
        insert IntervalList;
        List<VGA_Service_Item__c> ItemList=new List<VGA_Service_Item__c>{
            buildServiceItem(IntervalList[0].Id,'L',55.0,100.0),
            buildServiceItem(IntervalList[0].Id,'S',55.0,100.0),
            buildServiceItem(IntervalList[0].Id,'P',55.0,100.0),
            buildServiceItem(IntervalList[1].Id,'L',55.0,100.0),
            buildServiceItem(IntervalList[1].Id,'P',55.0,100.0),
            buildServiceItem(IntervalList[1].Id,'S',55.0,100.0),
            buildServiceItem(IntervalList[2].Id,'L',55.0,100.0),
            buildServiceItem(IntervalList[2].Id,'P',55.0,100.0),
            buildServiceItem(IntervalList[2].Id,'S',55.0,100.0)
        };
        insert ItemList;
        List<VGA_Service_Claim__c> ServiceClaimList=new List<VGA_Service_Claim__c>{
            buildServiceClaim(Vehicle.Id,5000,DealerAccount.Id,IntervalList[1].Id,Policy.Id)
        };
        insert ServiceClaimList;


    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 24/05/2019
    * @description It is used tom to genrate Service Interval
    * @param Code : Code for Interval
    * @param Description : Describe About Interval
    * @param KMS : Kilometre Info 
    * @param Month : Month Info
    * @param ModelId : Model(Product2) ID
    * @param PolicyCode : Policy Component Code Info
    * @param Order : Specify Order infor   From 1 2 3.....etc
    * @return VGA_Service_Item__c Object
    */ 
    //Change Field api name in Code Level VGA_Component_Code__c & VGA_Fixed_Price__c
    public static VGA_Service_Interval__c  buildServiceInterval(String Code,String Description,Integer KMS, Integer Month,Id ModelId,String PolicyCode,Integer Order){
        VGA_Service_Interval__c serviceInterval = new VGA_Service_Interval__c();
        serviceInterval.VGA_Code__c =Code;
        serviceInterval.VGA_Description__c	=Description;
        serviceInterval.VGA_External_Id__c=Code+Month+ModelId+'ssu';
        serviceInterval.VGA_KMS__c= KMS;
        serviceInterval.VGA_Product2__c = ModelId;
        serviceInterval.VGA_Months__c = Month;
        serviceInterval.VGA_Fixed_Price__c= 0; 
        serviceInterval.VGA_Price_Structure__c ='Assured';
        serviceInterval.VGA_Service_Order__c=Order;
        serviceInterval.VGA_Component_Code__c = PolicyCode;
        return serviceInterval;
    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 24/05/2019
    * @description It is used tom to genrate Service Item
    * @param ServiceIntervalId : Asset Id 
    * @param ItemType : type of Item  accept only P L S
    * @param Quantity : Quantity Details 
    * @param UnitPrice : Unit Price Detail
    * @return VGA_Service_Item__c Object
    */ 

    public static VGA_Service_Item__c  buildServiceItem(ID ServiceIntervalId,String ItemType,Decimal Quantity,decimal UnitPrice){
        VGA_Service_Item__c serviceItem = new VGA_Service_Item__c();
        serviceItem.VGA_Active__c   = true;
        serviceItem.VGA_Brand_Code__c   = 0;
        serviceItem.VGA_HPG__c  ='TEST';
        serviceItem.VGA_item_description__c ='TEST';
        serviceItem.VGA_Item_Number__c  ='TEST';
        serviceItem.VGA_Item_type__c    =ItemType;
        serviceItem.VGA_Price__c    =UnitPrice;
        serviceItem.VGA_Quantity__c =Quantity;
        serviceItem.VGA_Service_Plan__c = ServiceIntervalId;
        serviceItem.VGA_Service_Type__c ='Free';
        return serviceItem;
    }

    /**
    * @author Mohammed Ishaque Shaikh
    * @date 24/05/2019
    * @description It is used tom to genrate Service Claims
    * @param VehicleId : Asset Id 
    * @param Kilometre : Kilometre
    * @param DealerId : Dealer Account Id 
    * @param IntervalId : VGA_Service_Interval__c
    * @Param PolicyId : VGA_Service_Policy__c
    * @return VGA_Service_Claim__c Object 
    */ 
    public static VGA_Service_Claim__c buildServiceClaim(Id VehicleId,Integer Kilometre,Id DealerId,Id IntervalId,Id PolicyId){
        VGA_Service_Claim__c serviceClaim = new VGA_Service_Claim__c();
        serviceClaim.VGA_Asset__c = VehicleId;
        serviceClaim.VGA_KMS__c     = Kilometre;
        serviceClaim.VGA_Dealer__c  = DealerId;
        serviceClaim.VGA_GST__c = 12345;
        //serviceClaim.VGA_Ownership__c 
        serviceClaim.VGA_RO_Number__c  = '12345';
        serviceClaim.VGA_Service_Date__c =System.today();
        serviceClaim.VGA_Service_Interval__c =IntervalId; 
        serviceClaim.VGA_Total_Ex_GST__c=0;
        serviceClaim.VGA_Vehicle_Policy__c=PolicyId;
        return serviceClaim;
    }

    @isTest
    static void TestAllScenarios(){
        id serviceClaimId =[select Id from VGA_Service_Claim__C limit 1].id ;
        VGA_ServicePlanClaimsController.Response resp=VGA_ServicePlanClaimsController.collectVINDetails('TESTVIN');
        System.assertEquals(true, resp.pass);
        resp=VGA_ServicePlanClaimsController.collectVINDetails('TESTVIN1');
        System.assertEquals(false, resp.pass);
        VGA_ServicePlanClaimsController.ClaimWrapperClass temp=new VGA_ServicePlanClaimsController.ClaimWrapperClass();
        temp.AssetId=[select Id from Asset where Name='TESTVIN'].Id;
        temp.DealerId=[select id From Account limit 1].Id;
        temp.IntervalId=[select Id from VGA_Service_Interval__c where VGA_Service_Order__c=1].Id;
        temp.KM='0';
        temp.RONumber='sdsd';
        temp.ServiceDate=''+System.today();
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=1].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today(),5000,[select id from Asset where name='TESTVIN' limit 1].id);
        System.assertEquals(true, resp.pass);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=1].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today(),1000000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=1].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today().adddays(-100),5000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=1].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today().adddays(-100),500000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.saveClaim(Json.serialize(temp));
        VGA_Service_Claim__c  data=(VGA_Service_Claim__c ) resp.result;
		System.assertEquals([select Id from VGA_Service_Claim__C where id!=:serviceClaimId limit 1].id ,data.id);

    }
}