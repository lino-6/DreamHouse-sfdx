/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class IdsDataProcessorForSalesTest 
{
    public static Account objAccount;
    public static Asset objAsset;
    public static Contact objContact;
    public static VGA_Vehicle_Policy__c objVehicle;
    public static VGA_Vehicle_Policy__c objRoadsidePolicy;
    public static VGA_Ownership__c objOwnership;
    public static VGA_Customer_Order_Details__c objCustomerOrder;
     public static product2 objProduct;
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        LoadData();
        string expected='{"cVehicle" : "1234","cDealer" : "123432","cModel" :"12345","cVehicleUse" : "trestq" }'; 
        //string strAccount = JSON.serialize(objAccount);
        string strAccount = JSON.serialize(VGA_CommonTracker.createAccount());
        string strAsset = JSON.serialize(objAsset);
        string strContact = JSON.serialize(objContact);
        string strVP = JSON.serialize(objContact);
        string strOwnership = JSON.serialize(objOwnership);
        string strCustomerOrder = JSON.serialize(objCustomerOrder);
        string strRoadsidePolicy = JSON.serialize(objRoadsidePolicy);
       //  string strexpected = JSON.serialize(expected);
        
        //IdsDataProcessorForSales obj = new IdsDataProcessorForSales();
        IdsDataProcessorForSales.processData(strAccount,strAsset,strContact,strVP,strOwnership,strOwnership,strCustomerOrder,expected, strRoadsidePolicy);
        
    }
    public static void LoadData()
    {
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c='123432';
        update objAccount;
        
        objContact =  VGA_CommonTracker.createDealercontact(objAccount.id);
        
        objAsset = VGA_CommonTracker.createAsset('Test Asset');
        objAsset.accountid = objAccount.id;
        objAsset.VGA_VIN__c = '1234';
        insert objAsset;
        
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objVehicle.VGA_External_Id__c='12345';
        insert objVehicle;    
        
        objRoadsidePolicy= new VGA_Vehicle_Policy__c();
        objRoadsidePolicy.VGA_Brand__c = 'Volkswagen';
        objRoadsidePolicy.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objRoadsidePolicy.VGA_External_Id__c='67890';
        insert objRoadsidePolicy;
        
        objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount.id;
        objOwnership.VGA_Type__c = 'Current Owner';
        insert objOwnership;
        
        objProduct = new Product2();
        objProduct.Name = 'VGA';
        objProduct.ProductCode = '12345';
        objProduct.VGA_Primary_Id__c='12345';
        
        
        insert objProduct;
        objCustomerOrder =new VGA_Customer_Order_Details__c();
        objCustomerOrder.VGA_Customer_order_Id__c='test';
        objCustomerOrder.VGA_External_Id__c='323232';
        insert objCustomerOrder;
         
    }
}