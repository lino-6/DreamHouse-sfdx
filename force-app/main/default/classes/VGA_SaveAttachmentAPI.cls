/**
* @author Hijith NS
* @date 05/03/2019
* @description REST API to create/update attachment on Lead object for testdrive leads.
*              method will update the attachment if API request contains valid id 
*              else the method will create new attachment on passing valid lead id
*              on successfull execution method will return attachment id else return null object 
*/
@RestResource(urlMapping='/saveAttachment/*')
global with sharing class VGA_SaveAttachmentAPI {
    
    @HttpPost
    global static void saveFile() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        try{
            String  LeadPrimaryId	= req.params.get('pId');
            String  attachmentName	= req.params.get('name');
            String  attachmentType	= req.params.get('contentType');
            String  attachmentId 	= req.params.get('id');
            Boolean validAttachmentId = false;
            Boolean validAttachment = false;
            
            
            system.debug('body :'+ req.requestBody.toString());
            
            if((LeadPrimaryId!= null || LeadPrimaryId!='' ) && (attachmentName != null || attachmentName != '' ) ){
                Attachment attachmentToInsert=new Attachment();
                Lead objLead;
                if((attachmentName != null || attachmentName != '' )){
                    validAttachment=true;
                    objLead	=[Select id,Name,(select id  from Attachments where id=:attachmentId) from Lead where VGA_Primary_id__c=:LeadPrimaryId];
                }
                else{
                    validAttachment=false;
                    objLead	=[Select id,Name from Lead where VGA_Primary_id__c=:LeadPrimaryId];
                }
                if(objLead !=null ){
                    if(validAttachment){
                        if(objLead.Attachments.size()==1){
                            validAttachmentId=true;
                        }
                    }
                    
                    if(validAttachmentId){
                        attachmentToInsert.Id=objLead.Attachments[0].Id;
                    }
                    if(!validAttachmentId){
                        attachmentToInsert.ParentId=objLead.Id;
                    }
                    if(((attachmentId!='' || attachmentId!=null) && validAttachmentId)||((attachmentId==null || attachmentId =='') && !validAttachmentId))
                    {
                        attachmentToInsert.name 		= attachmentName;
                        attachmentToInsert.ContentType 	= attachmentType;
                        attachmentToInsert.Body 		= EncodingUtil.base64Decode(req.requestBody.toString());
                        Upsert attachmentToInsert;                 
                        res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"id":"'+attachmentToInsert.Id+'"}')));
                    }
                    else
                    {
                        res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{}')));
                    }
                }
                
            }else{
                res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{}')));
            }
            
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug('Line number'+ex.getLineNumber());
            res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{}')));
        }
    }
    
}