// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          11-September-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to get Lead Details and update lead details


public without sharing class VGA_LeadDetailsController 
{
    public static String DocusignAttachmentName='Consent_Form';
    public static String DocusignURL=Label.VGA_Consent_Form_URL;

    @AuraEnabled
    public static user getlogginguser()
    {
        return VGA_Common.getloggingAccountDetails();
    }
  
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to get list of Lead according to selected picklist value.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         11-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------

     @AuraEnabled
     public static list<Lead> getLeadlist(String Assignedvalue,String StatusValue ,string DealerId,string ConsultantId,
                                          String sortField, boolean isAsc)
     {   

          map<string,string>mapofstatus =new map<string,string>{'New'=>'New','Accepted'  =>'Accepted','Closed'    => 'Closed'};
          map<string,string>mapofdate     =new map<string,string>{'Last 30 days' => system.now().addDays(-30).format('yyyy-MM-dd'),
                                                          'Today'        =>system.now().format('yyyy-MM-dd'),
                                                          'Last 7 days'  =>system.now().adddays(-7).format('yyyy-MM-dd'),
                                                          'Last 90 days' => system.now().adddays(-90).format('yyyy-MM-dd')                          
                                                         };
         list<Lead>listofLead =new list<Lead>();
         string queryString;
         queryString = 'select Id,Name,Status,Owner.Name,VGA_Brand__c,VGA_Address__c,VGA_Color_Code__c,VGA_Sub_Brand__c,VGA_Request_Type_Image__c,VGA_Outcome__c,VGA_Assigned_to_Dealership__c,VGA_Request_Type__c,VGA_Escalation__c,VGA_User_2__c,VGA_User_3__c,VGA_User_4__c,VGA_User_5__c,VGA_User_6__c,VGA_Excites__c,VGA_Volkswagen_Fleet_Size__c,VGA_Lease_or_Purchase_Vehicles__c,VGA_Purchase_Timeframe__c,Title,Company';
         queryString += ' from Lead ';
         queryString += ' where PartnerAccountId =: DealerId';
         queryString += ' and VGA_Request_Type__c !=\'Brochure Request\'';
         if(ConsultantId !='All')
         {
           queryString += ' and OwnerId =: ConsultantId';
         }
         if(StatusValue !='All' && StatusValue !='New and Accepted')
         {
           queryString += ' and Status =\''+ mapofstatus.get(StatusValue)+'\'';
         }
         else if(StatusValue =='New and Accepted')
         {
            queryString +=  ' and (Status =\'New\' or Status = \'Accepted\' ) ';
         }
         if(Assignedvalue !='More than 90 days')
         {
            queryString += ' and VGA_Assign_Date__c <= ' + mapofdate.get('Today');
            queryString += ' and VGA_Assign_Date__c >= ' + mapofdate.get(Assignedvalue) ;
         }
         else
         {
             date morethan90Dates =date.valueof(mapofdate.get('Today')).adddays(-90);
             queryString += ' and VGA_Assign_Date__c <=:morethan90Dates'; 
         }
        
         if (sortField != '') 
         {
          queryString += ' order by ' + sortField;
          if (isAsc) 
          {
            queryString += ' asc';
          } 
          else 
          {
            queryString += ' desc';
          }
         }
         queryString += ' LIMIT 49999';
         
         system.debug('queryString>>>'+queryString);
         listofLead = Database.query(queryString);
         return listofLead;
     }
    
  /**
    * @author Ganesh M
    * @date 01/02/2019
    * @description getcampaignMap to get Campaign Name for Lead.
    * @param  LeadId
    * @return  String Campaign Name           
    */
    @AuraEnabled
    public static String getcampaignMap(string RecordId)
    {
         
        list<CampaignMember> campaignmemberlist = new list<campaignmember>();
        campaignmemberlist = [select id,campaign.name,leadid from campaignmember where leadid =: RecordId];
        map<id,string> campaignmap = new map<id,string>();
        if(!campaignmemberlist.isempty())
          {
             for(campaignmember cm: campaignmemberlist)
             {
                  campaignmap.put(cm.leadid,cm.campaign.name);
              }
         } 
       
         
        return campaignmap.get(RecordID);
    }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get lead detail.
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - string
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - lead
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         12-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     // 
     @AuraEnabled
     public static Lead getLeadDetails(string RecordId)
     {
        ID EventRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Test Drive Appointments').getRecordTypeId();
        Lead objLead =[select Id,Name,FirstName,LastName,Status,Owner.Name,VGA_User_Id__c,VGA_Outcome__c,
                        VGA_Model_of_Interest__c,VGA_Address__c,VGA_Status__c,VGA_Contact_Id__c,VGA_Dealer_Code__c,
                        VGA_Escalation__c,Address,
                        Email,Phone,MobilePhone,Country,city,street,postalcode,VGA_Sub_Brand__c,
                        VGA_Request_Type_Image__c,OwnerId,VGA_Lead_Id__c,VGA_Assignment_Done__c,
                        VGA_Brand__c,VGA_Request_Type__c,VGA_Volkswagen_Fleet_Size__c,VGA_Lease_or_Purchase_Vehicles__c,VGA_Purchase_Timeframe__c,Title,Company,
                        VGA_Configurator_URL__c,Description,LeadSource,createdDate,VGA_Owner_Name__c,VGA_Created_Date__c,Contact__c,VGA_VIN__c,
                       VGA_User_2__c,VGA_User_3__c,VGA_User_4__c,VGA_User_5__c,VGA_User_6__c,VGA_Excites__c,VGA_Comments__c,VGA_Test_Drive_Start_Date__c,(select id,OwnerId from Events where  RecordTypeId=:EventRecordTypeId  limit 1),VGA_Consent_Form_Attached__c,VGA_Dealer_Account__r.VGA_Pilot_Dealer__c from Lead where Id=: RecordId limit 1];
         
         return objLead;         
     }
    
     /**
    * @author Archana Y
    * @date 20/03/2019
    * @description The below method is used to get events based on a lead ID, which is considered as whoID. 
    * @param  leadID
    * @return  String Event          
    */
     @AuraEnabled
     public static Event getEvent(string leadID)
     {
         Event objEvent =	[SELECT ID, StartDateTime, EndDateTime, Owner.Name, OwnerId FROM Event Where WhoID =: leadID limit 1];          
         return objEvent;         
     }
    
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get all the contact related to logging dealership
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - None
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - map
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         12-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static map<string,string> getcontactDetails()
     {
          string Ids =UserInfo.getUserId() ;
          string logingName =UserInfo.getName();
          Map<String,string> mapofContactNameandId = new Map<String,string>();
          string AccountIds =VGA_Common.getloggingAccountId();
          mapofContactNameandId.put(logingName,Ids);
          mapofContactNameandId.put('All','All');
          list<user>listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId, Phone 
                                                from User where contact.AccountId =:AccountIds 
                                                and IsActive=true and Id !=:Ids limit 999]);
          
         if(listofUser !=Null && listofUser.size()>0)
          {
              for(user objuser :listofUser)
              {
                  mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
              }
               return mapofContactNameandId;
          }
        else
        {
            return mapofContactNameandId;
        }
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get logging dealership
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - None
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - map
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         12-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static map<string,Id> getAccountDetails()
     {
          Map<String,Id> mapofAccountNameandId = new Map<String,Id>();
          user objUser =VGA_Common.getloggingAccountDetails();
          if(objUser !=Null)
          {
                 mapofAccountNameandId.put(objUser.Contact.Account.Name,objUser.Contact.AccountId);
                 return mapofAccountNameandId;
          }  
         else
         {
            return mapofAccountNameandId;
         }
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to update Lead Information
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - Lead object
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - string
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         13-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
      @AuraEnabled
     public static string updateLeadDetails(Lead objLead,string ownerIds,String eventId,String EventOwnerId)
     {
         Savepoint sp = Database.setSavepoint(); 
         Event eve;
         
         if(eventId !=Null && EventOwnerId!=Null){
            eve=new Event(Id=eventId,OwnerId=EventOwnerId);
         }
        
         if(ownerIds !=Null && ownerIds!=objLead.OwnerId)
         {
            objLead.OwnerId   =ownerIds;
            objLead.Status    ='New';
            objlead.vga_status__c = '';          
         }
         if(objLead!= Null && objLead.Status !='New')
         {
           objLead.Status    =objLead.VGA_Status__c;
         }
            
         try
         {

             update objLead;             
             if(eve!=null){
                update eve;
                
             }             
             if(OwnerIds !=Null)
             {
                 list<Lead>listofLead =new list<Lead>();
                 listofLead.add(objLead);
                 if(listofLead !=Null && listofLead.size()>0)
                 {
                   VGA_LeadTriggerHandler.sendNotification(listofLead, false);
                 }
             }
             return objLead.Id;
         }
         catch(exception e)
         {
                Database.rollback(sp);
                System.debug('Error: ' + e.getMessage());
                return 'Error: ' + e.getMessage();
         }
     }
 
   

    
    @AuraEnabled
    public static list<LeadHistory>getLeadHistory(string selectLeadId)
    {
        list<LeadHistory>listofHistory =new list<LeadHistory>([select Id ,Field,LeadId,NewValue,createdDate,Lead.LastModifiedBy.Name,createdById,createdBy.Name,
                                                               OldValue from LeadHistory where LeadId =:selectLeadId ORDER BY createdDate DESC limit 999
                                                              ]);
       
        return listofHistory;
    }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to create Notes record on Lead
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - String
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - String
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         13-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static string createnotes(string titlevalue,string 
                                      commentvalue,string partentId)
     {
         Note objNotes =new Note ();
             objNotes.Title         =titlevalue;
             objNotes.Body          =commentvalue;
             objNotes.ParentId      =partentId;
         try
         {
             insert objNotes;
             return 'Comment saved successfully';
         }
         catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get list of Notes
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - string
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - list<Notes>
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         13-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static list<Note> getnotes(string partentId)
     {
         list<Note>listofNotes =new list<Note>([select Id,Body,owner.Name,createdDate 
                                                from Note where ParentId=:partentId ORDER BY createdDate DESC limit 999 ]);
         return listofNotes;
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get list of Notes
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - string
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - list<Notes>
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         13-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static string updateleadstatus(string leadId)
     {
         Lead objLead =new Lead();
         objLead.Status='Accepted';
         objLead.VGA_Status__c='Accepted';
         objLead.VGA_Lead_Accepted_Date__c=system.now();  
         objLead.Id=leadId;
         try
         {
             Update objLead;
             return objLead.Id;
             
         }
         catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to Role
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - None
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - string
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         15-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static string getuserInformation()
    {
        String dealerRole = '';
        List<string>ProfileName = VGA_Common.getProfileName();
        Map<String, Integer> rolesMap = new Map<String, Integer>{'Dealer Administrator'=> 1, 
                                                                'Lead Controller'      => 2, 
                                                                'Consultant'           => 3,
                                                                'Dealer Portal Group'  => 4,
                                                                'Dealer Portal'        => 5};


        for(string objRole :ProfileName)
        {
            if(objRole.contains('Dealer Administrator'))
            {
               dealerRole = objRole;
               break;
            }
            else if(dealerRole == '')
            {
                dealerRole = objRole;
            }
            else if(dealerRole != '' 
                    && rolesMap.containsKey(dealerRole)
                    && rolesMap.containsKey(objRole))
            {
                if(rolesMap.get(objRole) < rolesMap.get(dealerRole)){
                    dealerRole = objRole;
                }
            }
        }
       
        if(dealerRole == ''){
            dealerRole = 'Consultant';
        }

        return dealerRole; 
    }
         
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to get all the contact related to logging dealership
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - None
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - map
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         12-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static map<string,string> getresign(string RecordIds)
     {
          Map<String,string> mapofContactNameandId = new Map<String,string>();
          Lead objLead = getLeadDetails(RecordIds);
          string AccountIds =VGA_Common.getloggingAccountId();
          mapofContactNameandId.put(objLead.VGA_Owner_Name__c,objLead.OwnerId);
          list<user>listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId 
                                                from User where contact.AccountId =:AccountIds and IsActive=true  and Id !=: objLead.OwnerId limit 999]);
         
         if(listofUser !=Null && listofUser.size()>0)
          {
              for(user objuser :listofUser)
              {
                  mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
              }
               return mapofContactNameandId;
          }
        else
        {
            return mapofContactNameandId;
        }
     }
     // -----------------------------------------------------------------------------------------------------------------
     // This method is used to details
     // -----------------------------------------------------------------------------------------------------------------
     // INPUT PARAMETERS:    - None
     // -----------------------------------------------------------------------------------------------------------------                      
     // RETURNS:             - string
     // -----------------------------------------------------------------------------------------------------------------
     // Version#     Date                       Author                          Description
     // -----------------------------------------------------------------------------------------------------------------
     // 1.0         15-September-2017           surabhi Ranjan               Initial Version
     // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static string getuserInformationProfile()
    {
        user objuser  =VGA_Common.getloggingAccountDetails();
        return objuser.profile.Name;
    }
    @AuraEnabled
    public static list<USER> getPilotContactsUsers()
    {
        User LoggedInUser=[select Id,Name,ContactId,Contact.Name  from User where Id=:UserInfo.getUserId()];
        String AccountIds = VGA_Common.getloggingAccountId();
        Map<Id,User> UserMap=new Map<Id,User>([select id,Name,ContactId from User where isActive=true and Contact.AccountId=:AccountIds]);
        Map<Id,Id> ContactUserMap=new Map<Id,Id>();
        for(User u:UserMap.values()){
            ContactUserMap.put(u.ContactId,u.Id);
        }

        list<VGA_User_Profile__c> lstPilotUsers = new list<VGA_User_Profile__c>([SELECT Id,Name,VGA_Contact__c FROM VGA_User_Profile__c WHERE VGA_Available_TestDrive__c = true AND VGA_Contact__c=: ContactUserMap.keySet()]);
        Set<Id> AvailableContactIdSet=new Set<Id>();
        for(VGA_User_Profile__c temp: lstPilotUsers){
            if(temp.VGA_Contact__c!=null){
                AvailableContactIdSet.add(temp.VGA_Contact__c);
            }
        }
        Set<USER> uniquePilotContacts = new Set<USER>();
        if(LoggedInUser!=null){
            if(AvailableContactIdSet.contains(LoggedInUser.ContactId)){
               uniquePilotContacts.add(LoggedInUser); 
               AvailableContactIdSet.remove(LoggedInUser.ContactId);
            }
        }
        for(User con:UserMap.values()){
            if(AvailableContactIdSet.contains(con.ContactId)){
                uniquePilotContacts.add(con);
            }
        }
        List<User> uniqueContacts = new list<User>();
        uniqueContacts.addAll(uniquePilotContacts);
        return uniqueContacts;

            
    }

    /*
    *@author : MOHAMMED ISHAQUE SHAIKH
    *Description : Attachment Details Wrapper is used to populate data in Lightning Component
    */
    public class AttachmentDetailsWrapper{
        @AuraEnabled public List<Attachment> AttachmentList;
        @AuraEnabled public Boolean DocusignStage1Check;
        @AuraEnabled public Boolean DocusignStage2Check;
        @AuraEnabled public String DocusignLink;
        @AuraEnabled public Boolean LeadCheck; 
        public AttachmentDetailsWrapper(){
            AttachmentList=new List<Attachment>();
            DocusignStage1Check=true;
            DocusignStage2Check=true;
            LeadCheck=true;
            DocusignLink='';
        }

    }
    /*
    *@author : MOHAMMED ISHAQUE SHAIKH
    *Description : collect all attachment for provided LeadId  & also display Link for Consent form 
    *@Params : LeadId =>  type String
    *
    *
    */
    @AuraEnabled
    public static AttachmentDetailsWrapper collectAttachmentWithParentId(String LeadId){
        try{
            if(LeadId==null || LeadId==''){
                return null;
            }else{
                AttachmentDetailsWrapper ADW=new AttachmentDetailsWrapper();
                Lead l=[select id,VGA_Dealer_Code__c,VGA_Consent_Form_Attached__c,VGA_Primary_id__c from lead where id=:LeadId];
                if(ADW.LeadCheck){
                    ADW.LeadCheck=l.VGA_Primary_id__c!=null?(l.VGA_Primary_id__c!=''?true:false):false;
                }
                if(ADW.LeadCheck){
                    ADW.LeadCheck=l.VGA_Dealer_Code__c!=null?(l.VGA_Dealer_Code__c!=''?true:false):false;
                }
                try{
                    
                    ADW.AttachmentList=[select Id, Name, Description from Attachment where parentid=:LeadId Order by name Desc];
                    if(l.VGA_Consent_Form_Attached__c){
                       ADW.DocusignStage1Check=false;
                       ADW.DocusignStage2Check=false;
                    }
                }catch(Exception ex){
                    System.debug(ex);
                    System.debug(ex.getMessage());
                    System.debug(ex.getLineNumber());
                    ADW.AttachmentList=new List<Attachment>();
                }
                if(ADW.DocusignStage1Check){
                    if(ADW.LeadCheck){
                        ADW.DocusignLink=VGA_LeadDetailsController.DocusignURL+'?id='+l.VGA_Primary_id__c+'&dealerCode='+l.VGA_Dealer_Code__c+'&redirectURL=';  
                    }
                }
                return ADW;
            }
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return null;
    }

     /**
    * @author Archana Y
    * @date 22/03/2019
    * @description The below method is used to get all the Attachments for a particular lead
    * @param  LeadId
    * @return  List of Attachments          
    */
	@AuraEnabled   
    public static List<Attachment> getAttachment(string selectLeadId)
    {
        List<Attachment> attachLst;
        if(selectLeadId !='')
        {
         	attachLst= [select Id, Name, Description from Attachment where parentid=:selectLeadId];
        }        
        return attachLst;

    }
    
    /**
    * @author Archana Y
    * @date 20/03/2019
    * @description The below method is used to get all Pilot Contacts for a particular login. 
    * @param  ---
    * @return  list<Contact>         
    */
    @AuraEnabled
    public static list<Contact> getPilotContacts()
    {
        User LoggedInUser=[select Id,Name,ContactId,Contact.Name  from User where Id=:UserInfo.getUserId()];
        Contact defaultContact=new Contact(Id=LoggedInUser.ContactId);
        String AccountIds = VGA_Common.getloggingAccountId();
        Map<Id,Contact> ContactMap=new Map<Id,Contact>([SELECT Id,Name FROM Contact WHERE AccountId =: AccountIds]);
        list<VGA_User_Profile__c> lstPilotUsers = new list<VGA_User_Profile__c>([SELECT Id,Name,VGA_Contact__c FROM VGA_User_Profile__c WHERE VGA_Available_TestDrive__c = true AND VGA_Contact__c=: ContactMap.keySet()]);
        Set<Id> AvailableContactIdSet=new Set<Id>();
        for(VGA_User_Profile__c temp: lstPilotUsers){
            if(temp.VGA_Contact__c!=null){
                AvailableContactIdSet.add(temp.VGA_Contact__c);
            }
        }
        Set<Contact> uniquePilotContacts = new Set<Contact>();
        if(defaultContact!=null){
            if(AvailableContactIdSet.contains(defaultContact.Id)){
               uniquePilotContacts.add((ContactMap.containsKey(defaultContact.Id)?ContactMap.get(defaultContact.Id):defaultContact)); 
               AvailableContactIdSet.remove(defaultContact.Id);
            }
        }
        for(Contact con:ContactMap.values()){
            if(AvailableContactIdSet.contains(con.Id)){
                uniquePilotContacts.add(con);
            }
        }
        List<Contact> uniqueContacts = new list<Contact>();
        uniqueContacts.addAll(uniquePilotContacts);
        return uniqueContacts;
           
    }

    
    /**
    * @author Archana Y
    * @date 22/03/2019
    * @description The below method is used to delete an event based on event ID
    * @param  EventID
    * @return  True/False         
    */
    @AuraEnabled
    public static Boolean deleteEventDetails(Event EventId)
    {
    	try
        {
            if(EventId!=null)
            {
            	List<Event> iasisaid= [select id from Event where id=:EventId.Id];
            	Delete([select id from Event where id=:EventId.Id]);
            }
    		return true;
		}
        catch(Exception ex)
        {
			System.debug(ex+' '+ex.getMessage()+' '+ex.getLineNumber());
		}
		return false;
    }
    
    /**
    * @author Archana Y
    * @date 22/03/2019
    * @description The below method is used to save event details to event object
    * @param  Event, Lead and Event Owner ID
    * @return  List of Attachments          
    */
    @AuraEnabled
    public static String saveEventDetails(Event EventData, Lead lead, String OwnerID)
    {
    	try
        {
            if(lead != Null)
            {
                lead.Id	= lead.Id;
                lead.VGA_Comments__c = lead.VGA_Comments__c;
                update lead;  
            } 
			if(EventData != Null)
            {
                Event evt =new Event();
                evt.Id = EventData.Id;
                evt.StartDateTime = EventData.StartDateTime;
                evt.EndDateTime   = EventData.StartDateTime.addHours(1);
                if(OwnerID  != Null)
                {
                    List<User> UserId = [Select ID from User where ContactID =:  OwnerID];
                    evt.OwnerId = UserId[0].ID;
                 
                }
                update evt;  
                return 'The Event was successfully saved.';
            } 
		}
        catch(Exception ex)
        {
			return 'Error: ' + ex.getMessage();
		}
		
        return 'Error: Unable to save the Event';
    }

   /**
    * @author Ashok Chandra 
    ** @date 30/05/2019
    * @description The below method is used to get ownership details of lead account
    * @param   Lead ID
    * @return  List of ownerships          
    */
    @AuraEnabled
    public static list<VGA_Ownership__c> getOwnerships(String leadid)
    {
         try{
            string accountid=[select id, VGA_Customer_Account__c from Lead where id=:leadid].VGA_Customer_Account__c;
            System.debug(accountid);
            return [select Id ,VGA_Brand__c,VGA_Model_Card_Name__c,VGA_Used_Car__c,VGA_VIN__r.VGA_Model_Name__c,VGA_Model_Year__c,VGA_Sub_Brand__c,VGA_Purchase_Date__c,VGA_VIN__r.VGA_Vehicle_Use_Code__c from VGA_Ownership__c where VGA_Owner_Name__c =:accountid ORDER BY createdDate DESC limit 999];
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
        }
        return new list<VGA_Ownership__c>();
        
    }


}