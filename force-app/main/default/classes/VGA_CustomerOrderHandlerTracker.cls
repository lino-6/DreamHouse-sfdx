@isTest
public with sharing class VGA_CustomerOrderHandlerTracker 
{
    
    public static Account objAccount;
    
    static testmethod void unittest1()
    {
        LoadData();
        VGA_Customer_Order_Details__c  objCOD =new VGA_Customer_Order_Details__c();
        Test.startTest();
         objCOD.VGA_Dealer_Code__c = '1234';
        insert objCOD;
        objCOD.VGA_Customer__c=objAccount.Id;
        update objCOD;
        Test.stopTest();    
    }
    
    public static void LoadData()
    {
        VGA_Triggers__c obj = new VGA_Triggers__c();
        obj.Name = 'VGA_Customer_Order_Details__c';
        obj.VGA_Is_Active__c = true;
        insert obj;
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '1234';
        update objAccount;
         VGA_Customer_Order_Details__c  objCOD =new VGA_Customer_Order_Details__c();
        objCOD = VGA_CommonTracker.createCustomerOrderDetails();
    }
}