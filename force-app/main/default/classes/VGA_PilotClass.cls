/**
* @author Mohammed Ishaque Shaikh
* @date 05/Mar/2019
* @Ticket No  #1072 isPilotDealer Salesforce API
* @description Apex class used to send Dealer details to third party sites.
*/
@RestResource(urlMapping='/isPilotDealer/*')
global class VGA_PilotClass {
    //Attachement Consent File name Variable
    public static String AccountAttachmentFileName='consentform';
    
    /*
    * @author Mohammed Ishaque Shaikh
    * @description Wrapper class is used to send data in specified format 
    */
    public class PilotDealerWrapper{
        public String dealerName,dealerCode,templateId,settings;
        public Integer statusCode;
        public PilotDealerWrapper(){
            dealerName=null;
            dealerCode=null;
            templateId=null;
            settings=null;
            statusCode=500;
        }
    }
    
    /*
    * @author Mohammed Ishaque Shaikh
    * @description Method used to send Dealer details to third party sites.
    */    
    @HttpGet
    global static void isPilotDealer(){
        RestRequest req=RestContext.request;
        RestResponse res=RestContext.response;
        //res.addHeader('Content-Type', 'application/json');
        try{
            Account acc;
            String dealerCode= req.params.get('dealerCode');
            System.debug(dealerCode);
            if(dealerCode==null || dealerCode==''){
                res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Not pilot dealer"}')));
            }else{
            
              acc=[SELECT Id, Name, VGA_Pilot_Dealer__c, VGA_Dealer_Code__c,VGA_Consent_From_Settings__c,(select id,name from Attachments where name=:AccountAttachmentFileName limit 1) FROM Account where VGA_Dealer_Code__c=:dealerCode and  VGA_Pilot_Dealer__c=true limit 1];
                if(acc!=null){
                    PilotDealerWrapper pdw=new PilotDealerWrapper();
                    pdw.dealerName=acc.Name!=null?acc.Name:null;
                    pdw.dealerCode=acc.VGA_Dealer_Code__c!=null?acc.VGA_Dealer_Code__c:null;
                    pdw.templateId=acc.Attachments.size()>0?acc.Attachments[0].Id:null;
                    pdw.settings=acc.VGA_Consent_From_Settings__c!=null?acc.VGA_Consent_From_Settings__c:null;
                    pdw.statusCode=200;
                    res.responseBody=Blob.valueOf(JSON.serializePretty(pdw));
                    
                }else{
                    res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Not pilot dealer"}')));
                }
            }
            
        }catch(Exception ex){
            System.debug(ex+'\n'+ex.getLineNumber()+'\n'+ex.getMessage());
            res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Not pilot dealer"}')));
        }
    }

}