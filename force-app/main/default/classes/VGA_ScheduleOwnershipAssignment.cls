global class VGA_ScheduleOwnershipAssignment implements Schedulable
{
	global void execute(SchedulableContext sc)
    {
        VGA_OwnershipBatch objOwnershipBatch = new VGA_OwnershipBatch();
        Database.executebatch(objOwnershipBatch);
    }
}