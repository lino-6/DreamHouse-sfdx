public class VGA_DealerTrafficDetailController 
{
   
    @AuraEnabled
    public static user getlogginguser()
    {
        User objUser =VGA_Common.getloggingAccountDetails();
        return objUser;
    }
    @AuraEnabled
    public static list<VGA_WrapperofProduct>getproductDetails(string radiobutton)
    {
        string subbrand;
        if(radiobutton =='Passenger')
        {
          subbrand  = 'Passenger Vehicles (PV)'; 
        }
        else
        {
          subbrand  = 'Commercial Vehicles (CV)'; 
        }
        user objUser  =VGA_Common.getloggingAccountDetails();
        string AccountId =VGA_Common.getloggingAccountId(); 
        list<VGA_WrapperofProduct>wrapperlist =new list<VGA_WrapperofProduct>();
        list<VGA_Dealer_Traffic_Header__c> listofDealerTrafficheader=getdealerHeader(AccountId,radiobutton); 
        if(objUser.Contact.VGA_Brand__c =='Volkswagen')
        {
            if(listofDealerTrafficheader !=Null && listofDealerTrafficheader.size()>0)
            {
                list<VGA_Dealer_Traffic_Detail__c>listofDealerTrafficeDetail =new list<VGA_Dealer_Traffic_Detail__c>([select Id,VGA_Dealer_Traffic_Header__c,VGA_Phone_Ins__c,VGA_Internet__c,VGA_Test_Drives__c,VGA_Walk_Ins__c,VGA_Active_Brochure__r.VGA_Brochure_Label__c,VGA_Active_Brochure__c,VGA_Active_Brochure__r.Name
                                                                                                                      from VGA_Dealer_Traffic_Detail__c where VGA_Dealer_Traffic_Header__c=:listofDealerTrafficheader[0].Id and VGA_Active_Brochure__r.VGA_Sub_Brand__c includes (:subbrand) and VGA_Active_Brochure__r.VGA_Brand__c=:objUser.Contact.VGA_Brand__c ORDER BY VGA_Active_Brochure__r.VGA_Brochure_Label__c ASC  limit 49999]);
                if(listofDealerTrafficeDetail !=Null && listofDealerTrafficeDetail.size()>0)
                {
                    for(VGA_Dealer_Traffic_Detail__c objDealerTraffic :listofDealerTrafficeDetail)
                    {  
                        VGA_WrapperofProduct objwrap =new VGA_WrapperofProduct();
                        objwrap.WalkIns              =objDealerTraffic.VGA_Walk_Ins__c;
                        objwrap.PhoneIns             =objDealerTraffic.VGA_Phone_Ins__c; 
                        objwrap.TestDrives           =objDealerTraffic.VGA_Test_Drives__c;
                        objwrap.Internet             =objDealerTraffic.VGA_Internet__c;
                       // objwrap.Newones              =objDealerTraffic.VGA_New__c;
                      //  objwrap.Demo                 =objDealerTraffic.VGA_Demo__c;
                     //   objwrap.Fleet                =objDealerTraffic.VGA_Fleet__c;
                        objwrap.productName          =objDealerTraffic.VGA_Active_Brochure__r.VGA_Brochure_Label__c;
                        objwrap.productId            =objDealerTraffic.VGA_Active_Brochure__c;
                        objwrap.Id                   =objDealerTraffic.Id;
                        wrapperlist.add(objwrap); 
                    }
                }
            }
            else
            {
                list<VGA_Active_Brochure__c>listofProduct =new list<VGA_Active_Brochure__c>([select Id,Name,VGA_Brochure_Label__c,VGA_Dealer_Traffic_Active__c from VGA_Active_Brochure__c where VGA_Sub_Brand__c includes (:subbrand)  
                                                                                             and VGA_Brand__c=: objUser.Contact.VGA_Brand__c and VGA_Dealer_Traffic_Active__c=true ORDER BY VGA_Brochure_Label__c ASC limit 49999]);
                if(listofProduct !=Null && listofProduct.size()>0)
                {
                    for(VGA_Active_Brochure__c objProduct :listofProduct)
                    {
                        VGA_WrapperofProduct objwrap =new VGA_WrapperofProduct();
                        objwrap.WalkIns=0;
                        objwrap.PhoneIns=0; 
                        objwrap.TestDrives=0;
                        objwrap.Internet=0;
                        objwrap.Newones=0;
                        objwrap.Demo=0;
                        objwrap.Fleet=0;
                        objwrap.productName=objProduct.VGA_Brochure_Label__c;
                        objwrap.productId=objProduct.Id;
                        wrapperlist.add(objwrap);   
                    }
                }
            }
        }
        else
        {system.debug(listofDealerTrafficheader);
         system.debug(subbrand);
         system.debug(objUser.Contact.VGA_Brand__c);
            if(listofDealerTrafficheader !=Null && listofDealerTrafficheader.size()>0)
            {
                list<VGA_Dealer_Traffic_Detail__c>listofDealerTrafficeDetail =new list<VGA_Dealer_Traffic_Detail__c>([select Id,VGA_Dealer_Traffic_Header__c,VGA_Phone_Ins__c,VGA_Internet__c,VGA_Test_Drives__c,VGA_Walk_Ins__c,VGA_Active_Brochure__r.VGA_Brochure_Label__c,VGA_Active_Brochure__c,VGA_Active_Brochure__r.Name
                                                                                                                      from VGA_Dealer_Traffic_Detail__c where VGA_Dealer_Traffic_Header__c=:listofDealerTrafficheader[0].Id and VGA_Active_Brochure__r.VGA_Sub_Brand__c includes (:subbrand) and VGA_Active_Brochure__r.VGA_Brand__c=:objUser.Contact.VGA_Brand__c ORDER BY VGA_Active_Brochure__r.VGA_Brochure_Label__c ASC  limit 49999]);
                              
              if(listofDealerTrafficeDetail !=Null && listofDealerTrafficeDetail.size()>0)
               {
                for(VGA_Dealer_Traffic_Detail__c objDealerTraffic :listofDealerTrafficeDetail)
                {  
                    VGA_WrapperofProduct objwrap =new VGA_WrapperofProduct();
                    objwrap.WalkIns              =objDealerTraffic.VGA_Walk_Ins__c;
                    objwrap.PhoneIns             =objDealerTraffic.VGA_Phone_Ins__c; 
                    objwrap.TestDrives           =objDealerTraffic.VGA_Test_Drives__c;
                    objwrap.Internet             =objDealerTraffic.VGA_Internet__c;
                 /* objwrap.Newones              =objDealerTraffic.VGA_New__c;
                    objwrap.Fleet                =objDealerTraffic.VGA_Fleet__c;
                    objwrap.Demo                 =objDealerTraffic.VGA_Demo__c;*/
                    objwrap.productName          =objDealerTraffic.VGA_Active_Brochure__r.VGA_Brochure_Label__c;
                    objwrap.productId            =objDealerTraffic.VGA_Active_Brochure__c;
                    objwrap.Id                   =objDealerTraffic.Id;
                    wrapperlist.add(objwrap); 
                }
             }
            }
             else
             {
                 
                 list<VGA_Active_Brochure__c>listofProduct =new list<VGA_Active_Brochure__c>([select Id,Name,VGA_Brochure_Label__c,VGA_Dealer_Traffic_Active__c from VGA_Active_Brochure__c where VGA_Sub_Brand__c includes (:subbrand)  
                                                                                             and VGA_Brand__c=: objUser.Contact.VGA_Brand__c and VGA_Dealer_Traffic_Active__c=true ORDER BY VGA_Brochure_Label__c ASC limit 49999]);
                                                                                
                                                                  
                                                                  
                  if(listofProduct !=Null && listofProduct.size()>0)
                    {
                       for(VGA_Active_Brochure__c objProduct :listofProduct)
                        {
                            VGA_WrapperofProduct objwrap =new VGA_WrapperofProduct();
                            objwrap.WalkIns=0;
                            objwrap.PhoneIns=0; 
                            objwrap.TestDrives=0;
                            objwrap.Internet=0;
                            objwrap.Newones=0;
                            objwrap.Fleet=0;
                            objwrap.Demo=0;
                            objwrap.productName=objProduct.VGA_Brochure_Label__c;
                            objwrap.productId=objProduct.Id;
                            wrapperlist.add(objwrap);   
                        }
                    }
             }
            }
        if(wrapperlist !=Null && wrapperlist.size()>0)
        {
            return wrapperlist;
        }
        return wrapperlist;
    }
    @AuraEnabled
    public static string saveDealerDetails(string DealerDetails,string radiobutton)
    { 
        List<VGA_WrapperofProduct> listofDealerDetails = (List<VGA_WrapperofProduct>)JSON.deserialize(DealerDetails,List<VGA_WrapperofProduct>.class);
        Id DealerTrafficId; 
        user objUser  =VGA_Common.getloggingAccountDetails();
        VGA_Dealer_Traffic_Deadline__mdt objDealer = getDealerTrafficDeadline(objUser.Contact.Account.VGA_Brand__c);
        date startdate =VGA_Common.getWeekStartDate(system.today()).adddays(1);
        date Enddate   =startdate.adddays(6);
        date lastweekStartdate =VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        date lastweekEnddate =lastweekStartdate.adddays(6);
        string AccountId =VGA_Common.getloggingAccountId(); 
        list<VGA_Dealer_Traffic_Detail__c>listofDealerTrafficDetail =new list<VGA_Dealer_Traffic_Detail__c>();
        list<VGA_Dealer_Traffic_Header__c> listofDealerTrafficheader=getdealerHeader(AccountId,radiobutton);
        try
        {
        if(listofDealerTrafficheader !=Null && listofDealerTrafficheader.size()>0)
        {
            DealerTrafficId= listofDealerTrafficheader[0].Id;
        }
        else
        {
            VGA_Dealer_Traffic_Header__c objDealerTraffic =new VGA_Dealer_Traffic_Header__c();
            objDealerTraffic.VGA_Account__c  =  AccountId;
            objDealerTraffic.VGA_Brand__c       = objUser.Contact.Account.VGA_Brand__c;
            if(radiobutton =='Passenger')
            {
                objDealerTraffic.VGA_Sub_Brand__c  = 'Passenger Vehicles (PV)'; 
            }
            else
            {
                objDealerTraffic.VGA_Sub_Brand__c  = 'Commercial Vehicles (CV)'; 
            }
            if((objDealer.VGA_Day__c ==system.now().format('EEEE')) || test.isRunningTest())
            {
                Integer hours;
          Integer FirstEnddateTimevalue;
            Integer secondEnddateTimevalue;
            if(objDealer.VGA_Hours__c !=Null)
             {
                 FirstEnddateTimevalue=Integer.valueof(objDealer.VGA_Hours__c.substring(0,2));
                 secondEnddateTimevalue=Integer.valueof(objDealer.VGA_Hours__c.substring(3,5));
             }
            if(objDealer.VGA_Time__c =='AM')
            {
                if(objDealer.VGA_Hours__c =='12:00')
                {
                    hours =0;
                }
                else
                {
                    hours =FirstEnddateTimevalue;   
                }  
            }
            else if(objDealer.VGA_Time__c =='PM')
            {
                if(objDealer.VGA_Hours__c =='12:00')
                {
                    hours =12;
                }
                else
                {
                    hours =FirstEnddateTimevalue + 12;   
                }
            }
            datetime currentTime =getdate(Datetime.now());  
            datetime deadlinetime =getdate(datetime.newInstance(system.today().year(), system.today().month(), system.today().day(), hours, secondEnddateTimevalue, 0));
                if(currentTime >= deadlinetime)
                {
                    objDealerTraffic.VGA_Start_Date__c  =  startdate;
                    objDealerTraffic.VGA_End_Date__c    =  Enddate;   
                }
                else
                {
                    objDealerTraffic.VGA_Start_Date__c  =  lastweekStartdate;
                    objDealerTraffic.VGA_End_Date__c    =  lastweekEnddate; 
                }  
            } 
            else
            {
                    objDealerTraffic.VGA_Start_Date__c  =  startdate;
                    objDealerTraffic.VGA_End_Date__c    =  Enddate;   
            }
            Insert objDealerTraffic; 
            DealerTrafficId=objDealerTraffic.Id;
        }
        if(DealerTrafficId != Null)
        {
            for(VGA_WrapperofProduct objWrapper : listofDealerDetails)
            {
                VGA_Dealer_Traffic_Detail__c objDealerTraffic =createDealerTrafficHeader(objWrapper.WalkIns,objWrapper.Internet,
                                                                                         objWrapper.PhoneIns,objWrapper.TestDrives,DealerTrafficId,objWrapper.productId,objWrapper.Id,objUser.Contact.Account.VGA_Brand__c) ;
                listofDealerTrafficDetail.add(objDealerTraffic);
                
            }
        }
       
        if(listofDealerTrafficDetail !=Null && listofDealerTrafficDetail.size()>0)
        {
            upsert listofDealerTrafficDetail;
        }
            return 'Traffic data saved sucessfully'; 
        }
        catch(exception e)
            {
                     return 'Error: ' + e.getMessage(); 
            } 
    }
    @AuraEnabled
    public static VGA_Dealer_Traffic_Detail__c createDealerTrafficHeader(Decimal IntWalkIns,Decimal IntInternet,
                                                                         Decimal IntPhoneIns,Decimal IntTestDrives,
                                                                         string strDealerTrafficId,
                                                                         string strProductId,string recordId ,string Brand )
    {
        VGA_Dealer_Traffic_Detail__c objDealerDetail    = new VGA_Dealer_Traffic_Detail__c();
        objDealerDetail.VGA_Walk_Ins__c             =IntWalkIns;
        objDealerDetail.VGA_Internet__c             =IntInternet;
        objDealerDetail.VGA_Phone_Ins__c             =IntPhoneIns;
        objDealerDetail.VGA_Test_Drives__c            =IntTestDrives;
     /* objDealerDetail.VGA_New__c                   =IntNewones;
        objDealerDetail.VGA_Fleet__c                   =IntFleet;
        objDealerDetail.VGA_Demo__c                   =IntDemo;*/
        objDealerDetail.VGA_Dealer_Traffic_Header__c  =strDealerTrafficId;
        objDealerDetail.VGA_Active_Brochure__c        =strProductId; 
        objDealerDetail.Id                            =recordId;
        return objDealerDetail;
    }
    @AuraEnabled
    public static VGA_WrapperofProduct getTotalValue(string radiobutton )
    {
        string AccountId =VGA_Common.getloggingAccountId(); 
        list<VGA_Dealer_Traffic_Header__c> objDealerTraffic =getdealerHeader(AccountId,radiobutton);
        if(objDealerTraffic !=Null && objDealerTraffic.size()>0)
        {
            VGA_WrapperofProduct objWrapper =new VGA_WrapperofProduct();
            objWrapper.TotalTestDrives=objDealerTraffic[0].VGA_Total_Test_Drives__c;
            objWrapper.TotalInternet=objDealerTraffic[0].VGA_Total_Internet__c;
            objWrapper.TotalWalkIns=objDealerTraffic[0].VGA_Total_Walk_Ins__c;
            objWrapper.TotalPhoneIns=objDealerTraffic[0].VGA_Total_Phone__c;
         /*   objWrapper.TotalNewcount=objDealerTraffic[0].VGA_Total_New__c;
            objWrapper.TotalDemoCount=objDealerTraffic[0].VGA_Total_Demo__c;
            objWrapper.TotalFleetCount=objDealerTraffic[0].VGA_Total_Fleet__c;*/
            return objWrapper;
        }
        else
        {
            VGA_WrapperofProduct objWrapper =new VGA_WrapperofProduct();
            objWrapper.TotalTestDrives=0;
            objWrapper.TotalInternet=0;
            objWrapper.TotalWalkIns=0;
            objWrapper.TotalPhoneIns=0;
            objWrapper.TotalNewcount=0;
            objWrapper.TotalDemoCount=0;
            objWrapper.TotalFleetCount=0;
            return objWrapper; 
        }
    }
    @AuraEnabled
    public static list<VGA_Dealer_Traffic_Header__c> getdealerHeader(string dealershipId,
                                                                     string radiobuttonvalue)
    {
        list<VGA_Dealer_Traffic_Header__c>listofDealerTraffic =new list<VGA_Dealer_Traffic_Header__c>();
        user objUser  =VGA_Common.getloggingAccountDetails();
        VGA_Dealer_Traffic_Deadline__mdt objDealer = getDealerTrafficDeadline(objUser.Contact.Account.VGA_Brand__c);
        date startdate =VGA_Common.getWeekStartDate(system.today()).adddays(1);
        date Enddate   =startdate.adddays(6);
        date lastweekStartdate =VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        date lastweekEnddate =lastweekStartdate.adddays(6);
        if(objDealer.VGA_Day__c ==system.now().format('EEEE') || test.isRunningTest())
        {
            Integer hours;
            Integer FirstEnddateTimevalue;
            Integer secondEnddateTimevalue;
            if(objDealer.VGA_Hours__c !=Null)
             {
                 FirstEnddateTimevalue=Integer.valueof(objDealer.VGA_Hours__c.substring(0,2));
                 secondEnddateTimevalue=Integer.valueof(objDealer.VGA_Hours__c.substring(3,5));
             }
            if(objDealer.VGA_Time__c =='AM')
            {
                if(objDealer.VGA_Hours__c =='12:00')
                {
                    hours =0;
                }
                else
                {
                    hours =FirstEnddateTimevalue;   
                }  
            }
            else if(objDealer.VGA_Time__c =='PM')
            {
                if(objDealer.VGA_Hours__c =='12:00')
                {
                    hours =12;
                }
                else
                {
                    hours =FirstEnddateTimevalue + 12;   
                }
            }
            datetime currentTime =getdate(Datetime.now());  
            datetime deadlinetime =getdate(datetime.newInstance(system.today().year(), system.today().month(), system.today().day(), hours, secondEnddateTimevalue, 0));
            if(currentTime >= deadlinetime)
            { 
                listofDealerTraffic=[select Id,VGA_Type__c,VGA_Total_Internet__c,VGA_Total_Phone__c,
                                     VGA_Total_Test_Drives__c,VGA_Total_Walk_Ins__c from VGA_Dealer_Traffic_Header__c                                                                                               
                                     where (VGA_Start_Date__c=:startDate and VGA_End_Date__c =:EndDate) and VGA_Account__c=:dealershipId  
                                     and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%') limit 1 ];
            } 
            else
            {
                listofDealerTraffic=[select Id,VGA_Type__c,VGA_Total_Internet__c,VGA_Total_Phone__c,
                                     VGA_Total_Test_Drives__c,VGA_Total_Walk_Ins__c from VGA_Dealer_Traffic_Header__c                                                                                               
                                     where (VGA_Start_Date__c=:lastweekStartdate and VGA_End_Date__c =:lastweekEnddate) and VGA_Account__c=:dealershipId  
                                     and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%') limit 1 ];
            }
        }
        else
        {
            listofDealerTraffic=[select Id,VGA_Type__c,VGA_Total_Internet__c,VGA_Total_Phone__c,
                                 VGA_Total_Test_Drives__c,VGA_Total_Walk_Ins__c from VGA_Dealer_Traffic_Header__c                                                                                               
                                 where (VGA_Start_Date__c=:startDate and VGA_End_Date__c =:EndDate) and VGA_Account__c=:dealershipId  
                                 and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%') limit 1 ]; 
        }
        return listofDealerTraffic;
    }
    @AuraEnabled
    public static datetime getdate(datetime convertdate)
    {
        Integer offset = UserInfo.getTimezone().getOffset(convertdate);
        Datetime local = convertdate.addSeconds(offset/1000);
        return local;
    }
    @AuraEnabled
    public static VGA_Dealer_Traffic_Deadline__mdt getDealerTrafficDeadline(string brandtype)
    {
        VGA_Dealer_Traffic_Deadline__mdt objDealerTraffic =[select Id,VGA_Day__c,VGA_Hours__c,
                                                            VGA_Time__c,MasterLabel from 
                                                            VGA_Dealer_Traffic_Deadline__mdt where MasterLabel=:brandtype limit 1 ] ;
        return objDealerTraffic;
    }
    @AuraEnabled 
    public static string getendDate()
    {
        date startdate =VGA_Common.getWeekStartDate(system.today());
        date startdateone =VGA_Common.getWeekStartDate(system.today()).adddays(1);
        date Enddate;
        Datetime dt = DateTime.newInstance(startdateone, Time.newInstance(0, 0, 0, 0));
        String dayOfWeek=dt.format('EEEE');
        Datetime currentdate = System.now();
        String currentdayOfWeek=currentdate.format('EEEE');
       
        System.debug('startdate'+startdate.format());
        /* if(dayOfWeek==currentdayOfWeek)
             {
                 
                Enddate   = startdate;
             }
         else{   
                  Enddate   = startdate.adddays(7);
             }*/
         if(dayOfWeek==currentdayOfWeek && currentdate.time() < Time.newInstance(12, 30, 0, 0))
             {
                Enddate   = startdate.adddays(7);
             }
         else if(dayOfWeek==currentdayOfWeek && currentdate.time() > Time.newInstance(12, 30, 0, 0)) {   
                Enddate   =startdate.adddays(14);
             }
         else 
         {
             Enddate   = startdate.adddays(7);
         }
        
        //date Enddate   =startdate.adddays(7);
       
        string convertdate =DateTime.newInstance(Enddate.year(),Enddate.month(),Enddate.day()).format('EEEE, dd MMMM YYYY');
        return convertdate;
       
    }
    @AuraEnabled 
    public static string getendDatePlusone()
    {
        date startdate =VGA_Common.getWeekStartDate(system.today());
        date startdateone =VGA_Common.getWeekStartDate(system.today()).adddays(8);
        date Enddate;
       
        Datetime dt = DateTime.newInstance(startdateone, Time.newInstance(0, 0, 0, 0));
        String dayOfWeek=dt.format('EEEE');
        Datetime currentdate = System.now();
        system.debug('current date'+currentdate);
        String currentdayOfWeek=currentdate.format('EEEE');
        
        
        if(dayOfWeek!=currentdayOfWeek && currentdate <= dt)
        {
         Enddate = CalculateNextworkingdate(startdateone);          
        }
        if(dayOfWeek!=currentdayOfWeek && currentdate > dt)
        {
         Enddate = CalculateNextworkingdate(startdate.adddays(8));          
        }
       else if(dayOfWeek==currentdayOfWeek && currentdate.time() < Time.newInstance(12, 30, 0, 0))
             {
                Enddate   = null;
             }
         else if(dayOfWeek==currentdayOfWeek && currentdate.time() > Time.newInstance(12, 30, 0, 0)) {   
                Enddate   =CalculateNextworkingdate(startdate.adddays(15));
             }
        string convertdate;
      if(Enddate!=null)
      {
         convertdate=DateTime.newInstance(Enddate.year(),Enddate.month(),Enddate.day()).format('EEEE, dd MMMM YYYY');
      }
       else
       {
           convertdate='';
       }
        return convertdate;
    }
    
  Public static Date CalculateNextworkingdate(Date sdate)   
  {
        User objUser =VGA_Common.getloggingAccountDetails();
       Date tempStartDate;
      List<VGA_Public_Holiday__c> publicHolidays = [SELECT Id, Name, 
                                                  VGA_Holiday_Date__c, 
                                                  VGA_Holiday_Name__c, 
                                                  VGA_State_Region__c, 
                                                  VGA_Holiday_Type__c 
                                                      FROM VGA_Public_Holiday__c 
                                                      where (VGA_State_Region__c=:objUser.Contact.Account.VGA_Region_Code__c or VGA_State_Region__c='All')
                                                     order by VGA_Holiday_Date__c ];
        if(publicHolidays.size()>0)
        {
            //System.debug('startdate'+startdate.format());
           
            for(VGA_Public_Holiday__c holiday:publicHolidays)
            {
                System.debug('startdate'+sdate.format());
                System.debug('VGA_Holiday_Date__c'+holiday.VGA_Holiday_Date__c.format()); 
                if(sdate.format() == holiday.VGA_Holiday_Date__c.format())
                {
                    System.debug('IN IFFFF');
                    sdate = holiday.VGA_Holiday_Date__c.addDays(1);
                }
                tempStartDate = sdate;
                //System.debug('startdate'+startdate.format());
                //System.debug('VGA_Holiday_Date__c'+holiday.VGA_Holiday_Date__c.format()); 
            }
          
        }
      return tempStartDate;
  }
}