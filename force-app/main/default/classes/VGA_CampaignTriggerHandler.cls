public class VGA_CampaignTriggerHandler 
{
	public void runTrigger()
	{
		if (Trigger.isAfter && Trigger.isInsert)
        {
            onAfterInsert((List<Campaign>) trigger.new);  
        }
    } 
	public void onafterInsert(List<Campaign> triggerNew) 
    {
    	updateTrackingCode(triggerNew);
    }
    //=========================================================================================================
    //This Method is used to update tracking Code on Campaign if Tracking Code is not not entered manually
    //=========================================================================================================
    //Name:Rishi Kumar Patel      Date:11-1-2017
    //=========================================================================================================
    private static void updateTrackingCode(List<Campaign> triggerNew)
    {
    	if(triggerNew != null && !triggerNew.isEmpty())
    	{
    		set<ID> setCampaignID = new set<ID>();
    		for(Campaign objCampaign : triggerNew)
    		{
    			if(objCampaign.VGA_Tracking_ID__c == null)
    			{
    				setCampaignID.add(objCampaign.id);
    			}
    		}
    		if(setCampaignID != null && !setCampaignID.isEmpty())
    		{
    			list<Campaign> lstCampaigns = [select id,VGA_Tracking_ID__c,Campaign_Code__c from Campaign 
    											where id in:setCampaignID];
    			
    			if(lstCampaigns != null && !lstCampaigns.isEmpty())
    			{
    				for(Campaign objCampaign : lstCampaigns)
    				{
    					objCampaign.VGA_Tracking_ID__c = objCampaign.Campaign_Code__c;
    				}
    				update lstCampaigns;
    			}
    		}
    	}
    }    
}