@isTest
private class VGA_LeadEscalationBatchTest {
	
	@testSetup
    static void buildConfigList() 
    {
		Account objAcc = VGA_CommonTracker.createDealerAccount();
		objAcc.VGA_Timezone__c = 'Australia/Sydney';
		objAcc.VGA_Dealer_Code__c = '1234';
		objAcc.billingstate='ACT';
		update objAcc;

		VGA_Dealership_Profile__c objDealer = new VGA_Dealership_Profile__c();
		objDealer.Name = 'Passenger Vehicles (PV)';
		objDealer.VGA_Dealership__c = objAcc.id;
		objDealer.VGA_Escalate_Minutes__c = 20;
		insert objDealer;

		Contact objCon = VGA_CommonTracker.createDealercontact(objAcc.id);

		VGA_User_Profile__c objUserPro = VGA_CommonTracker.createUserProfile('test');
		objUserPro.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
		objUserPro.VGA_Brand__c = 'Volkswagen';
		objUserPro.VGA_Contact__c = objCon.id;
		objUserPro.VGA_Available__c = true;
		insert objUserPro;

		User objUser = VGA_CommonTracker.CreateUser(objCon.id);

		Id idgetDealerContact = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact'); 
		
		Contact objCon2 = new Contact();
		objCon2.FirstName 	='NewOwner';
		objCon2.LastName 	='TestDeal33333er';
		objCon2.RecordTypeId=idgetDealerContact;
		objCon2.VGA_Role__c ='Consultant';
		objCon2.VGA_Brand__c='Volkswagen';
		objCon2.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
		objCon2.AccountId=objAcc.id;
		objCon2.VGA_Available_to_Receive_Leads__c=true;
		objCon2.Email ='testt344444444432@live.com';
		objCon2.MobilePhone='21111111131111111';
		//objContact.VGA_Available__c = true;
		insert objCon2;  

		VGA_User_Profile__c objUserPro2 = VGA_CommonTracker.createUserProfile('test');
		objUserPro2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
		objUserPro2.VGA_Brand__c = 'Volkswagen';
		objUserPro2.VGA_Contact__c = objCon2.id;
		objUserPro2.VGA_Available__c = true;
		insert objUserPro2;

		VGA_Working_Hour__c objWork3 = VGA_CommonTracker.createWorkingHour('Tuesday','08:30 AM','01:30 PM');
		objWork3.VGA_User_Profile__c = objUserPro2.id;
		objWork3.Name = System.Now().format('EEEE');
		objWork3.VGA_Working__c = true;
		objWork3.VGA_End_Time__c = '11:00 PM';
		objWork3.VGA_Start_Time__c ='07:00 AM';
		insert objWork3;

		VGA_Working_Hour__c objWork4 = [select id, VGA_End_Time_Date_Time__c,VGA_Start_Date_Date_Time__c from 
									VGA_Working_Hour__c where VGA_User_Profile__c =:  objUserPro2.id];

		VGA_Working_Hour__c objWork = VGA_CommonTracker.createWorkingHour('Tuesday','08:30 AM','01:30 PM');
		objWork.VGA_User_Profile__c = objUserPro.id;
		objWork.Name = System.Now().format('EEEE');
		objWork.VGA_Working__c = true;
		objWork.VGA_End_Time__c = '11:00 PM';
		objWork.VGA_Start_Time__c ='07:00 AM';
		insert objWork;

		Profile p = [SELECT Id FROM Profile WHERE Name='Volkswagen Dealer Consultant Profile' LIMIT 1];
		
		User objUser2 = new User();
		objUser2.ContactId =objCon2.id;
		objUser2.ProfileId = p.Id;
		objUser2.username = 'tes2333t@test.com123';
		objUser2.FirstName = 'New';
		objUser2.LastName  = 'Owner';
		objUser2.email = 'test@test.com';
		objUser2.VGA_Brand1__c = 'Volkswagen';
		String alias = objUser2.username;
		if(alias.length() > 5){
		    alias = alias.substring(0,5);
		}
		objUser2.alias = alias;
		objUser2.languagelocalekey =' en_US';
		objUser2.localesidkey = 'en_US';
		objUser2.emailencodingkey = 'UTF-8';
		objUser2.TimezoneSidKey = 'America/Los_Angeles';

		insert objUser2;

		VGA_Notification_Template__c objCS = new VGA_Notification_Template__c();
        objCS.Name                      = 'All Escalations';
        objCS.VGA_Email_Body__c         = 'hi';
        objCS.VGA_Email_From_Address__c = 'rishi.patel@saasfocus.com';
        objCS.VGA_SMS_Body__c           = 'hi';
        objCS.VGA_SMS_From_Id__c        = 'rishi.patel@saasfocus.com';
        insert objCS;

        VGA_Notification_Template__c objCSNewLead = new VGA_Notification_Template__c();
        objCSNewLead.Name                         = 'New Lead Notification';
        objCSNewLead.VGA_Email_Body__c            = 'hi';
        objCSNewLead.VGA_Email_From_Address__c    = 'rishi.patel@saasfocus.com';
        objCSNewLead.VGA_SMS_Body__c              = 'hi';
        objCSNewLead.VGA_SMS_From_Id__c           = 'rishi.patel@saasfocus.com';
        insert objCSNewLead;
		
		VGA_Subscription__c objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c      = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'All Escalations';
        objSubscription.VGA_Delivery_Method__c   = 'Email and SMS';
        insert objSubscription;

        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c      = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c   = 'Email and SMS';
        insert objSubscription;

		List<Lead> leadsToInsertList = VGA_CommonTracker.buildLeads('escalationLead', 10);

		for(Lead objLead : leadsToInsertList){
			objLead.VGA_Brand__c 		   = 'Volkswagen';
			objLead.LastName 			   = 'Prasad';
			objLead.VGA_Sub_Brand__c 	   = 'Passenger Vehicles (PV)';
			objLead.VGA_Dealer_Code__c 	   = '1234';
			objLead.VGA_Dealer_Account__c  = objAcc.id;
			objLead.VGA_Assignment_Done__c = true;
			objLead.OwnerId 			   = objUser.id;	
			objLead.VGA_Assigned_to_Dealership__c = System.now().addDays(-10);
			objLead.VGA_Escalated_Date_Time__c    = System.now().addDays(-10);
					
		}

		insert leadsToInsertList;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 22/01/2019
    * @description When we execute the lead escalation batch job and it is not a public 
    *			   holiday and is during the working hours of dealership.
    *			   We will then be updating the leads owners so the leads are going 
    *			   from one owner to another until they are accepted.
    */    
    @isTest static void test01_LeadEscalationBatch_WhenEscalationIsExecuted_LeadOwnersWillBeUpdated()
    {
    	Map<Id,Lead> beforeUpdateLeadsMap = new Map<Id,Lead>([SELECT Id, OwnerID 
    														   FROM Lead]);
    	VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		
		Test.startTest();
		Database.executeBatch(obj);
    	Test.stopTest();

    	Contact cont = [SELECT Id
    					FROM Contact
    					WHERE FirstName = 'NewOwner'];
    	List<Lead> afterUpdateLeadsList = [SELECT Id, OwnerID, Owner.Name From Lead];
    	User usr = [SELECT Id, Name
    				FROM User
    				WHERE ContactId =: cont.Id ];
    	for(Lead afterLead : afterUpdateLeadsList){
    		//Verify that the lead owner is updated
    		if(beforeUpdateLeadsMap.containsKey(afterLead.Id)){
    			System.assertNotEquals(afterLead.OwnerId, beforeUpdateLeadsMap.get(afterLead.Id).OwnerId);
    			System.assertEquals(usr.Name, afterLead.Owner.Name);
    		}
    		
    	}
    }

    /**
    * @author Lino Diaz Alonso
    * @date 22/01/2019
    * @description When we execute the lead escalation batch job and it is not a public 
    *			   holiday and is during the working hours of dealership.
    *			   We will then be updating the leads owners so the leads are going 
    *			   from one owner to another until they are accepted.
    */    
    @isTest static void test02_LeadEscalationBatch_WhenEscalationIsExecutedAndIsREcurringHoliday_LeadsWontBeReassigned()
    {
    	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','All');   
       	insert objPH;

       	Map<Id,Lead> beforeUpdateLeadsMap = new Map<Id,Lead>([SELECT Id, OwnerID 
    														   FROM Lead]);
       	
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		
		Test.startTest();
		Database.executeBatch(obj);
    	Test.stopTest();


    	for(Lead afterLead : [SELECT Id, OwnerID, Owner.Name From Lead]){
    		//Verify that the lead owner has not been updated
    		if(beforeUpdateLeadsMap.containsKey(afterLead.Id)){
    			System.assertEquals(afterLead.OwnerId, beforeUpdateLeadsMap.get(afterLead.Id).OwnerId);
    		}
    		
    	}
    }

    /**
    * @author Lino Diaz Alonso
    * @date 22/01/2019
    * @description When we execute the lead escalation batch job and it is not a public 
    *			   holiday and is during the working hours of dealership.
    *			   We will then be updating the leads owners so the leads are going 
    *			   from one owner to another until they are accepted.
    */    
    @isTest static void test03_LeadEscalationBatch_WhenEscalationIsExecutedAndIsRecurringStateHoliday_LeadsWontBeReassigned()
    {
    	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','ACT');   
       	insert objPH;

       	Map<Id,Lead> beforeUpdateLeadsMap = new Map<Id,Lead>([SELECT Id, OwnerID 
    														   FROM Lead]);
       	
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		
		Test.startTest();
		Database.executeBatch(obj);
    	Test.stopTest();


    	for(Lead afterLead : [SELECT Id, OwnerID, Owner.Name From Lead]){
    		//Verify that the lead owner has not been updated
    		if(beforeUpdateLeadsMap.containsKey(afterLead.Id)){
    			System.assertEquals(afterLead.OwnerId, beforeUpdateLeadsMap.get(afterLead.Id).OwnerId);
    		}
    		
    	}
    }

    /**
    * @author Lino Diaz Alonso
    * @date 22/01/2019
    * @description When we execute the lead escalation batch job and it is not a public 
    *			   holiday and is during the working hours of dealership.
    *			   We will then be updating the leads owners so the leads are going 
    *			   from one owner to another until they are accepted.
    */    
    @isTest static void test04_LeadEscalationBatch_WhenEscalationIsExecutedAndIsSingleHoliday_LeadsWontBeReassigned()
    {
    	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','ACT');   
       	insert objPH;

       	Map<Id,Lead> beforeUpdateLeadsMap = new Map<Id,Lead>([SELECT Id, OwnerID 
    														   FROM Lead]);
       	
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		
		Test.startTest();
		Database.executeBatch(obj);
    	Test.stopTest();


    	for(Lead afterLead : [SELECT Id, OwnerID, Owner.Name From Lead]){
    		//Verify that the lead owner has not been updated
    		if(beforeUpdateLeadsMap.containsKey(afterLead.Id)){
    			System.assertEquals(afterLead.OwnerId, beforeUpdateLeadsMap.get(afterLead.Id).OwnerId);
    		}
    		
    	}
    }
	

    /**
    * @author Lino Diaz Alonso
    * @date 05/02/2019
    * @description When we execute the lead escalation batch job and an exception happens
    *              we will create a error log record and the job will be rescheduled.
    */    
    @isTest static void test05_LeadEscalationBatch_WhenExceptionHappens_ErrorLogBuilderWillBeCreated()
    {
        
        List<Lead> leadList = new List<Lead>();
        leadList.add(new Lead());
        leadList.add(null);

        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
        
        Test.startTest();
        obj.execute(null, leadList);
        Test.stopTest();


        for(VGA_ErrorLog__c errorLog : [SELECT Id, Process__c
                                        FROM VGA_ErrorLog__c]){
            //A new error log builder record will be created
            System.assert(errorLog.Process__c.contains('Exception in Lead Escalation Batch'));
            
        }
    }
}