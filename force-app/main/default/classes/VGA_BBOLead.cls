public class VGA_BBOLead {

    public enum types {NA, TEST_DRIVE_LEAD_FORM, OFFER_REQUEST_LEAD_FORM, CONTACT_LEAD_FORM
        , BROCHURE_LEAD_FORM, NEWSLETTER_LEAD_FORM, INFO_REQUEST_FORM, ONE_DAY_SPECIAL_TEST_DRIVE
        , COMPETITION_REGISTRATION, MULTIPLE_CTAS, VOUCHER_GENERATED_VIA_VOUCHER_ENGINE, VOUCHER_GENERATED_VIA_MARKET
        , TEST_DRIVE_PLUS_CONTACT, TEST_DRIVE_PLUS_OFFER, CONTACT_PLUS_OFFER, SERVICE_REQUEST_I,  PROSPECT_CAMPAIGN_VWCH
        , COMPLAINT, SUGGESTION, PROBLEMS_WITH_WEBSITE, ENVIRONMENTAL_QUESTION, OTHER, INFO_REQUEST_OR_AFTERSALES_SERVICE
        , MAGAZINE_SUBSCRIPTION, OFFER_RESERVATION, GENERAL_CONTACT_REQUEST, TEST_DRIVE_PLUS_BROCHURE, CONTACT_PLUS_BROCHURE
        , BUSINESS_LEADS, SERVICE_REQUEST_II, FINANCE_OFFER_FORM, CHAT, UNMAPPED_FORMTYPE}
    
    public types lead_type;
    public String camo;
    public Integer pryr;
    public String camp;
    public Date lddt;
    
    private static Map<String, Integer> type_map = new Map<String, Integer>
    {
        'NA' => 0,
        'TEST_DRIVE_LEAD_FORM' => 1,
        'OFFER_REQUEST_LEAD_FORM' => 2,
        'CONTACT_LEAD_FORM' => 3,
        'BROCHURE_LEAD_FORM' => 4,
        'NEWSLETTER_LEAD_FORM' => 5,
        'INFO_REQUEST_FORM' => 6,
        'ONE_DAY_SPECIAL_TEST_DRIVE' => 7,
        'COMPETITION_REGISTRATION' => 8,
        'MULTIPLE_CTAS' => 9,
        'VOUCHER_GENERATED_VIA_VOUCHER_ENGINE' => 11,
        'VOUCHER_GENERATED_VIA_MARKET' => 12,
        'TEST_DRIVE_PLUS_CONTACT' => 20,
        'TEST_DRIVE_PLUS_OFFER' => 21,
        'CONTACT_PLUS_OFFER' => 22,
        'SERVICE_REQUEST_I' => 23,
        'PROSPECT_CAMPAIGN_VWCH' => 24,
        'COMPLAINT' => 25,
        'SUGGESTION' => 26,
        'PROBLEMS_WITH_WEBSITE' => 27,
        'ENVIRONMENTAL_QUESTION' => 28,
        'OTHER' => 29,
        'INFO_REQUEST_OR_AFTERSALES_SERVICE' => 30,
        'MAGAZINE_SUBSCRIPTION' => 31,
        'OFFER_RESERVATION' => 32,
        'GENERAL_CONTACT_REQUEST' => 35,
        'TEST_DRIVE_PLUS_BROCHURE' => 40,
        'CONTACT_PLUS_BROCHURE' => 41,
        'BUSINESS_LEADS' => 42,
        'SERVICE_REQUEST_II' => 43,
        'FINANCE_OFFER_FORM' => 44,
        'CHAT' => 45,
        'UNMAPPED_FORMTYPE' => 99
    };    
    
    public VGA_BBOLead()
    {
        lead_type = null;
        camo = null;
        pryr = null;
        camp = null;
        lddt = null;
    }
    
    private Integer getTypeValue(String key)
    {
        return type_map.get(key);
    }   
    
    public types getEnumFromValue(Integer value)
    {
        for(String key : type_map.keySet())
        {
            if(type_map.get(key) == value)
            {
                for(types t : types.values())
                {
                    if(t.name() == key)
                    {
                        return t;
                    }
                }
            }
        }
        
        return null;
    }    
    
    public override String toString()
    {    
        return '{"type": ' + (lead_type == null ? 'null' : String.valueOf(getTypeValue(lead_type.name())))
            + ', "camo": ' + (camo == null ? 'null' : '"' + camo + '"') + ', "pryr": '+ (pryr == null ? 'null' : String.valueOf(pryr))
            + ', "camp": ' + (camp == null ? 'null' : '"' + camp + '"')  
            + ', "lddt": ' + (lddt == null ? 'null' : '"' + DateTime.newInstance(lddt.year(), lddt.month(), lddt.day()).format('yyyy-MM-dd') + '"') + '}';        
    }

}