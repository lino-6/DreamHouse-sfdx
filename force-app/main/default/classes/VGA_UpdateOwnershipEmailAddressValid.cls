/**
* @author Archana Yerramilli
* @date 23/10/2018
* @description 
* @param List<VGA_Ownership__c> is passed to the job 
*/

public class VGA_UpdateOwnershipEmailAddressValid implements Queueable
{
	public List<VGA_Ownership__c> lstOwnership; 
    
    public VGA_UpdateOwnershipEmailAddressValid(List<VGA_Ownership__c> results) 
    {
       System.debug('@@@ Entered VGA_UpdateOwnershipEmailAddressValid Constructor @@@' + results.size());
       this.lstOwnership = results;
    }

    public void execute(QueueableContext context)
    {
        System.debug('@@@ Entered VGA_UpdateOwnershipEmailAddressValid QueueableContext execute @@@');
        System.debug('lstOwnership in execute method are : ' + lstOwnership.size());
        update lstOwnership;
    }
}