/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_VehicleTriggerHandlerTracker 
{
	public static Account objAccount;
	public static Asset objAsset;
	public static product2 objProduct;
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        loadData();
        
      insert objAsset;
        objAsset.VGA_Fuel_Type__c = 'Petrol';
        update objAsset;
        
    }
    public static void loadData()
    {
    	VGA_Triggers__c obj = new VGA_Triggers__c();
    	obj.name = 'Asset';
    	obj.VGA_Is_Active__c = true;
    	
    	insert obj;
    	
    	objAccount = VGA_CommonTracker.createDealerAccount();
    	
    	objProduct = VGA_CommonTracker.createProduct('Test Product');
        objProduct.VGA_Brand__c = 'PV Volkswagen';
        insert objProduct;
    	
    	objAsset = new Asset();
    	objAsset.name = 'TEST';
    	objAsset.accountID = objAccount.id;
    	objAsset.Product2Id = objProduct.id;
        
    }
}