/*
   
    Change history:
   
    Date: 2018-05-15
    Modified By: Alfahad 
    Change: Disabled VGA_Unique_Customer_Number__c assignment. We decided to make it formula field  
   
    Date: 2018-05-11
    Modified By: Alfahad    
    Change: Added logic to populate VGA_Vehicle_Original_Sale_Date__c and VGA_Vehicle_Current_Sale_Date__c
    
    Date: 2018-05-28
    Modified By: Ganesh    
    Change: Added logic to populate VGA_Literature_Code__c on  Ownership object  when new policy created
*/
public with sharing class VGA_VehiclePolicyTriggerHandler  
{
   public void runTrigger()
    {
        if (Trigger.isAfter && Trigger.isInsert)
        {
            onAfterInsert((List<VGA_Vehicle_Policy__c>) trigger.new);  
        }
        
        if (Trigger.isAfter && Trigger.isUpdate)
        {
            onAfterUpdate((List<VGA_Vehicle_Policy__c>) trigger.new, (Map<Id, VGA_Vehicle_Policy__c>) trigger.OldMap);
        }
        
        if (Trigger.isAfter && Trigger.isDelete)
        {
            onAfterDelete((List<VGA_Vehicle_Policy__c>) trigger.old);
        }
    }
    
    public void onAfterInsert(List<VGA_Vehicle_Policy__c> lstTriggerNew) 
    {
        List<VGA_Allianz_Data_Mapping__c> obj_lst = createAllianzDataMapping(lstTriggerNew);
        obj_lst = populateAllianzDataMapping(lstTriggerNew, obj_lst);               

        INSERT obj_lst;
        
        updateLiteratureCode(lstTriggerNew); 
    }
   
    
    
    public void onAfterUpdate(List<VGA_Vehicle_Policy__c> lstTriggerNew, Map<Id,VGA_Vehicle_Policy__c> triggeroldmap)
    {
        updateLiteratureCode(lstTriggerNew);
    }
    
    /*
     * Author: Athira P R
     * Description : To update Allianze Policy record with Action 'D' if the Vehicle Policy is deleted.
     * Params: triggOld=> holds Vehilcle policy data in List 
     * Modified Date : 2019-07-08
    */
    public void onAfterDelete(List<VGA_Vehicle_Policy__c> triggerOld)
    {
        Set<Id> PolicyIdset = new Set<Id>();
        for(VGA_Vehicle_Policy__c policyobj: triggerOld) {
            PolicyIdset.add(policyobj.Id);
        }

        List<VGA_Allianz_Data_Mapping__c >  allianzToUpdate = new List<VGA_Allianz_Data_Mapping__c >();
        allianzToUpdate = [SELECT Id,VGA_Transaction_Code__c  FROM VGA_Allianz_Data_Mapping__c where VGA_Vehicle_Policy_ID__c IN: PolicyIdset ];
        if(allianzToUpdate.size() > 0){ 
            for(VGA_Allianz_Data_Mapping__c obj_allianz : allianzToUpdate ){
                obj_allianz.VGA_Transaction_Code__c = 'D';
            }      
            UPDATE allianzToUpdate;   
        }       
    }
    
    
    
    public static List<VGA_Allianz_Data_Mapping__c> getAllianzDataMapping(list<VGA_Vehicle_Policy__c> lsttriggernew)
    {       
        
        Set<Id> AssetId =  new Set<Id>();
        
        for(VGA_Vehicle_Policy__c obj : lsttriggernew)
        {
            AssetId.add(obj.id);
        }
        
        List<VGA_Allianz_Data_Mapping__c> obj_lst;
        
        if(AssetId != null && !AssetId.isEmpty())
        {
            obj_lst = [SELECT Id FROM VGA_Allianz_Data_Mapping__c WHERE VGA_VIN__c = :AssetId];
        }
        
        return obj_lst;
    }    
    
    public static List<VGA_Allianz_Data_Mapping__c> createAllianzDataMapping(list<VGA_Vehicle_Policy__c> lsttriggernew)
    {
        List<VGA_Allianz_Data_Mapping__c> obj_lst = new List<VGA_Allianz_Data_Mapping__c>();
        
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            for(VGA_Vehicle_Policy__c objVehiclePolicy : lsttriggernew)
            {    
                if(objVehiclePolicy.VGA_Component_Type__c != null && objVehiclePolicy.VGA_Component_Type__c == 'ROADSIDE')  
                {     
                    VGA_Allianz_Data_Mapping__c objADM = new VGA_Allianz_Data_Mapping__c();
                    objADM.VGA_Vehicle_Policy_ID__c = objVehiclePolicy.id;
                    objADM.VGA_Allianz_External_ID__c = objVehiclePolicy.VGA_External_Id__c; 
                    objADM.VGA_Transaction_Code__c = 'A';
                    objADM.VGA_Reference_Policy_ID__c = objVehiclePolicy.id;
                    obj_lst.add(objADM);
                }
            }
        }
        
        return obj_lst;     
    }
    
    /**
    * @author Lino Diaz
    * @date 28/05/2018
    * @description  Added logic to populate VGA_Literature_Code__c on Ownership object when new policy created
    */
    public static void updateLiteratureCode(list<VGA_Vehicle_Policy__c> lsttriggernew)
    {
        
        Set<Id> AssetId =  new Set<Id>();
        Map<Id, Map<string, string>> literature_code = new Map<Id, Map<string, string>>(); 
        Map<Id, Map<string, string>> literature_codeold = new Map<Id, Map<string, string>>();       
        
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {                           
            
            for(VGA_Vehicle_Policy__c objVehiclePolicy : lsttriggernew)
            {
                AssetId.Add(objVehiclePolicy.VGA_VIN__c);
                
                if(literature_code.get(objVehiclePolicy.VGA_VIN__c) == null)
                {
                    Map<string, string> code = new Map<string, string>();
                    Map<string, string> codeold = new Map<string, string>();
                    code.put('year', '1');
                    code.put('literature_code', 'SKOWEL001');
                    codeold.put('literature_code', 'SKOWEL004-A');
                    literature_code.put(objVehiclePolicy.VGA_VIN__c, code );
                    literature_codeold.put(objVehiclePolicy.VGA_VIN__c, codeold);
                }
               /* if(objVehiclePolicy.Is_Extended_Warranty__c == false){
                literature_code.get(objVehiclePolicy.VGA_VIN__c).put('literature_code', 'SKOWEL001');
                
                }*/
                if(objVehiclePolicy.VGA_Component_Code__c == 'SP5YR' && Integer.valueof(literature_code.get(objVehiclePolicy.VGA_VIN__c).get('year')) < 5){
                    literature_code.get(objVehiclePolicy.VGA_VIN__c).put('literature_code', 'SKOWEL002');
                    literature_code.get(objVehiclePolicy.VGA_VIN__c).put('year', '5');
                }
                else if(objVehiclePolicy.VGA_Component_Code__c == 'SP3YR' && Integer.valueof(literature_code.get(objVehiclePolicy.VGA_VIN__c).get('year')) < 3){
                    literature_code.get(objVehiclePolicy.VGA_VIN__c).put('literature_code', 'SKOWEL003');
                    literature_code.get(objVehiclePolicy.VGA_VIN__c).put('year', '3');
                }  
                
                 else  {
                     
                     literature_codeold.get(objVehiclePolicy.VGA_VIN__c).put('literature_code', 'SKOWEL004-A');
                   
                    
                }                
            }  
        }
        
        for(VGA_Vehicle_Policy__c obj_vehicle_policy : [SELECT VGA_Component_Code__c, VGA_VIN__c,Is_Extended_Warranty__c FROM VGA_Vehicle_Policy__c WHERE VGA_VIN__c = :AssetId])
        {
             if(obj_vehicle_policy.VGA_Component_Code__c == 'SP5YR' 
                && Integer.valueof(literature_code.get(obj_vehicle_policy.VGA_VIN__c).get('year')) < 5)
            {
                literature_code.get(obj_vehicle_policy.VGA_VIN__c).put('literature_code', 'SKOWEL002');
                literature_code.get(obj_vehicle_policy.VGA_VIN__c).put('year', '5');
            }
            else if(obj_vehicle_policy.VGA_Component_Code__c == 'SP3YR' 
                    && Integer.valueof(literature_code.get(obj_vehicle_policy.VGA_VIN__c).get('year')) < 3)
            {
                literature_code.get(obj_vehicle_policy.VGA_VIN__c).put('literature_code', 'SKOWEL003');
                literature_code.get(obj_vehicle_policy.VGA_VIN__c).put('year', '3');
            }   
            else 
            {
                literature_codeold.get(obj_vehicle_policy.VGA_VIN__c).put('literature_codeold', 'SKOWEL004-A'); 
            }      
        }
        
        List<VGA_Ownership__c> listVGO =  [SELECT Id, VGA_Literature_Code__c, VGA_VIN__c,VGA_Previous_Owner__c 
                                           FROM VGA_Ownership__c 
                                           WHERE VGA_VIN__c IN :AssetId AND VGA_Type__c = 'Current Owner'];
        for(VGA_Ownership__c obj_ownership : listVGO)
        {
            if(obj_ownership.VGA_Previous_Owner__c != null){
                obj_ownership.VGA_Literature_Code__c = literature_codeold.get(obj_ownership.VGA_VIN__c).get('literature_codeold');
            }
            else{  
                obj_ownership.VGA_Literature_Code__c = literature_code.get(obj_ownership.VGA_VIN__c).get('literature_code');
            }
        }
        
        update listVGO;
    }
    
    public static List<VGA_Allianz_Data_Mapping__c> populateAllianzDataMapping(list<VGA_Vehicle_Policy__c> lsttriggernew, List<VGA_Allianz_Data_Mapping__c> lst_allianz)
    {
        Id SkoFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
        Id VWFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
        Id SkoInd_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
        Id VWInD_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        
        
        Set<string> vin_ids = new Set<string>();
        Map<id, string> policy_vin_map = new Map<id, string>();
        
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {   
            for(VGA_Vehicle_Policy__c objVehiclePolicy : lsttriggernew)
            {
                for(VGA_Allianz_Data_Mapping__c objADM : lst_allianz)
                {
                    if(objADM.VGA_Vehicle_Policy_ID__c == objVehiclePolicy.id)
                    {
                        objADM.VGA_Brand__c =   objVehiclePolicy.VGA_Brand__c != null?objVehiclePolicy.VGA_Brand__c:null;
                        objADM.VGA_Sub_Brand__c = objVehiclePolicy.VGA_Sub_Brand__c != null?objVehiclePolicy.VGA_Sub_Brand__c:null;
                        
                        if(objADM.VGA_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Skoda'))
                        {
                            objADM.VGA_Client_Code__c = 'SKO';
                        }
                        else if(objADM.VGA_Sub_Brand__c != null && objADM.VGA_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Volkswagen') && objADM.VGA_Sub_Brand__c.equalsignorecase('Passenger Vehicles (PV)'))
                        {
                            objADM.VGA_Client_Code__c = 'VWP';
                        }
                        else if(objADM.VGA_Brand__c != null && objADM.VGA_Sub_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Volkswagen') && objADM.VGA_Sub_Brand__c.equalsignorecase('Commercial Vehicles (CV)'))
                        {
                            objADM.VGA_Client_Code__c = 'VWC';
                        }     
                        
                        objADM.VGA_Start_Date__c = objVehiclePolicy.VGA_Policy_Start_Date__c != null?objVehiclePolicy.VGA_Policy_Start_Date__c:null;
                        objADM.VGA_End_Date__c = objVehiclePolicy.VGA_Policy_End_Date__c != null?objVehiclePolicy.VGA_Policy_End_Date__c:null;
                        objADM.VGA_VIN__c = objVehiclePolicy.VGA_VIN__c !=null?objVehiclePolicy.VGA_VIN__c:null;
                        objADM.VGA_Policy_Reference_Number__c =  objVehiclePolicy.VGA_VIN__c !=null?objVehiclePolicy.VGA_VIN__c:null;
                        objADM.VGA_Manufacturer__c = objVehiclePolicy.VGA_Brand__c != null?objVehiclePolicy.VGA_Brand__c:null;
                        objADM.VGA_Last_Modified_Date_Time__c = system.now();
                        if(objVehiclePolicy.VGA_Is_Updated__c == true)
                        {
                            // objADM.VGA_Transaction_Code__c = 'C';
                        } 
                        else if(objVehiclePolicy.Created_Today__c == true)
                        {
                            objADM.VGA_Transaction_Code__c = 'A';
                        }
                        else if(objVehiclePolicy.VGA_Cancelled__c == true)
                        {
                            // objADM.VGA_Transaction_Code__c = 'D';       
                        }
                       // objADM.VGA_Unique_Customer_Number__c = objVehiclePolicy.VGA_VIN_No__c != null?objVehiclePolicy.VGA_VIN_No__c:null;
                        
                        if(objVehiclePolicy.VGA_Account_Record_Type_ID__c != null && (objVehiclePolicy.VGA_Account_Record_Type_ID__c == SkoFleet_RTId || objVehiclePolicy.VGA_Account_Record_Type_ID__c == VWFleet_RTId))
                        {
                            objADM.VGA_Company_Flag__c = 'Y';
                           // objADM.VGA_Address_Type__c = 'Work';
                        }
                        else if(objVehiclePolicy.VGA_Account_Record_Type_ID__c != null && (objVehiclePolicy.VGA_Account_Record_Type_ID__c == SkoInd_RTId ||objVehiclePolicy.VGA_Account_Record_Type_ID__c== VWInD_RTId ))
                        {
                            objADM.VGA_Company_Flag__c = 'N';
                          //  objADM.VGA_Address_Type__c = 'Work';
                        }   
                        
                        //Calculate product code 
                        objADM.VGA_Product_Code__c = VGA_AllianzDataMappingHelper.getAllianzProductCode(objVehiclePolicy);
                        
                        if(objVehiclePolicy.VGA_VIN_No__c != null) {                            
                            vin_ids.add(objVehiclePolicy.VGA_VIN_No__c);
                            policy_vin_map.put(objVehiclePolicy.id, objVehiclePolicy.VGA_VIN_No__c);
                        }
                                            
                                        
                    }
                }
            }
        }
        
        if(vin_ids != null && !vin_ids.isEmpty()) 
        {
            List<VGA_Ownership__c> lst_ownership = [SELECT VGA_Type__c, VGA_Purchase_Date__c, VGA_VIN_for_Mule__c FROM VGA_Ownership__c WHERE VGA_VIN_for_Mule__c IN :vin_ids];

            if(lst_ownership != null && !lst_ownership.isEmpty()) {
            
                for(VGA_Ownership__c obj_ownership : lst_ownership) {
                
                    for(VGA_Allianz_Data_Mapping__c objADM : lst_allianz)
                    {
                        if(obj_ownership.VGA_VIN_for_Mule__c == policy_vin_map.get(objADM.VGA_Vehicle_Policy_ID__c))
                        {               
                            if(objADM.VGA_Vehicle_Original_Sale_Date__c == null) {
                                objADM.VGA_Vehicle_Original_Sale_Date__c = obj_ownership.VGA_Purchase_Date__c;
                            }
                            
                            if(obj_ownership.VGA_Type__c == 'Current Owner') {
                                objADM.VGA_Vehicle_Current_Sale_Date__c = obj_ownership.VGA_Purchase_Date__c;
                            }
                            
                            if(obj_ownership.VGA_Purchase_Date__c < objADM.VGA_Vehicle_Original_Sale_Date__c) {
                                objADM.VGA_Vehicle_Original_Sale_Date__c = obj_ownership.VGA_Purchase_Date__c;
                            }
                            
                        }
                    }
                }
                
            }           
        }
        
        return lst_allianz;
    }
}