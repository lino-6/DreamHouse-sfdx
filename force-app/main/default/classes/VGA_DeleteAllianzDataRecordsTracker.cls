/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
//=======================================================================
//Test Class For VGA_DeleteAllianzDataRecords
//==============================================================
@isTest(seeAllData=false)
private class VGA_DeleteAllianzDataRecordsTracker 
{
	public static VGA_Allianz_Data_Mapping__c objAllianzDataMapping;
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        loadData();
        
        Test.setCreatedDate(objAllianzDataMapping.id,system.now().adddays(-31));
        
        test.startTest();
        	VGA_DeleteAllianzDataRecords objCls = new VGA_DeleteAllianzDataRecords();
        	Database.executeBatch(objCls);
        test.stopTest();
    }
    public static void loadData()
    {
    	objAllianzDataMapping = new VGA_Allianz_Data_Mapping__c();
    	insert objAllianzDataMapping;
    }
}