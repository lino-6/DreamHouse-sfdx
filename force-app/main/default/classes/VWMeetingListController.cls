public with sharing class VWMeetingListController {

    private ApexPages.StandardController ctrl;
    public List<punosmobile__Meeting__c> meetingsWithObject;
    Id objectId;
    private List<User> userList;

    public VWMeetingListController(ApexPages.StandardController ctrl) {
        queryRecords(ApexPages.currentPage().getParameters().get('Id'));
    }
    public void init(){
        queryRecords(ApexPages.currentPage().getParameters().get('Id'));
    }

    public void queryRecords(Id pageId) {
        objectId = pageId;
    }

    public String getBaseUrl() {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/';
    }

    public List<punosmobile__Meeting__c> getMeetingsWithObject() {
            if (objectId != null) {
                setMeetings(objectId);
            }
        return meetingsWithObject;
    }

    /// ******
    /// Parameter1  : Takes in Meetings Related to Object ID
    /// Action      : Sets Meetings that current user is owner or attendee of.
    ///             : Querys only Completed meetings with the Related Objects or Name fields Objects used in the Meeting.
    /// ******

    private void setMeetings(Id id) {
        if (Schema.SObjectType.punosmobile__Meeting__c.isAccessible()
            && Schema.SObjectType.punosmobile__Meeting__c.isQueryable()) {

            // Query meetings with the fields that the user has rights to see
            //This trimmed-down version relies on a Permission Set that provides all users with all rights related to Meeting objects
            meetingsWithObject = [SELECT Name,
                                Owner.Name,
                                punosmobile__Start_Time__c,
                                punosmobile__Meeting_Status__c
                                FROM punosmobile__Meeting__c
                                WHERE punosmobile__Meeting_Status__c = 'Completed'
                                AND (punosmobile__Related_To__c = :id)               
                                ORDER BY punosmobile__Start_Time__c DESC                          
                                ];
    	}
    }
}