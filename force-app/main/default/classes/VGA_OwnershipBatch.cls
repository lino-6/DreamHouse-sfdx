global class VGA_OwnershipBatch implements Database.Batchable<sObject>
{
	global Database.querylocator start(Database.BatchableContext bc)
    {
    	//string strOwnershipQuery = 'select id,VGA_Welcome_pack_Status__c,VGA_Welcome_Pack_Sent_Date_Time__c,VGA_Owner_Name__c,VGA_Previous_Owner__c	from VGA_Ownership__c where VGA_Welcome_pack_Status__c = null and VGA_Type__c =\'Current Owner\'';
    	
        set<String> setInvalidNames = new set<string>{'demo','fleet','dealer'};
		set<string> setInvalidCompanyNames = new set<string>{'demo','fleet','SKODA','ŠKODA'};
		
		string strOwnershipQuery = 'select id,VGA_Welcome_pack_Status__c,VGA_Welcome_Pack_Sent_Date_Time__c,VGA_Owner_Name__c,VGA_Previous_Owner__c';  
		strOwnershipQuery += ' from VGA_Ownership__c where VGA_Welcome_pack_Status__c=null';
		strOwnershipQuery += ' and VGA_Type__c='+'\'Current Owner\'';
		strOwnershipQuery += ' AND '+ 'VGA_First_Name__c NOT IN:setInvalidNames';
		strOwnershipQuery += ' and '+ 'VGA_Last_Name__c NOT IN:setInvalidNames';
		strOwnershipQuery += ' and ' + 'VGA_Company_Name__c NOT IN:setInvalidCompanyNames';
		strOwnershipQuery += ' and ' + 'VGA_Owner_Name__r.VGA_Address_Validation__c !='+'\'Invalid\''+' and '+'VGA_Owner_Name__r.VGA_Address_Validation__c !='+'\'Not Provided\'';
		strOwnershipQuery += ' and '+'(VGA_Owner_Name__r.RecordType.Name='+'\'Skoda Individual Customer\'';
		strOwnershipQuery += ' OR ' +'VGA_Owner_Name__r.RecordType.Name='+'\'Skoda Fleet Customer\''+')';
        
    	return Database.getQueryLocator(strOwnershipQuery);
    }
    global void execute(Database.BatchableContext BC, List<VGA_Ownership__c> lstOwnership)
    {
    	if(lstOwnership != null && !lstOwnership.isEmpty())
    	{
    		map<ID,Integer> mapofAllOwnership = new map<id,Integer>();
    		map<ID,Integer> mapofCurrentOwnership = new map<id,Integer>();
    		set<ID> setofAccountID = new set<ID>();
    		for(VGA_Ownership__c objOwnership : lstOwnership)
    		{
    			if(objOwnership.VGA_Owner_Name__c != null)
    			{
    				setofAccountID.add(objOwnership.VGA_Owner_Name__c);
    			}
    		}
    		if(setofAccountID != null && !setofAccountID.isEmpty())
    		{
    			AggregateResult[] lstOwnerShips = [select VGA_Owner_Name__c,count(id) from VGA_Ownership__c 
    													where VGA_Type__c =:'Current Owner' and VGA_Owner_Name__c in:setofAccountID GROUP BY VGA_Owner_Name__c];
    			
    			if(lstOwnerShips != null && !lstOwnerShips.isEmpty())
    			{
    				
    				for(AggregateResult ar : lstOwnerShips)
    				{
    					mapofAllOwnership.put(string.valueof(ar.get('VGA_Owner_Name__c')),integer.valueof(ar.get('expr0')));
    				}
    			}
    			
    			AggregateResult[] lstCurrentOwnerShips = [select VGA_Owner_Name__c,count(id) from VGA_Ownership__c 
    													where VGA_Type__c =:'Current Owner' and VGA_Owner_Name__c in:setofAccountID and VGA_Welcome_pack_Status__c = null GROUP BY VGA_Owner_Name__c];
    			
    			if(lstCurrentOwnerShips  != null && !lstCurrentOwnerShips .isEmpty())
    			{
    				for(AggregateResult ar : lstCurrentOwnerShips )
    				{
    					mapofCurrentOwnership.put(string.valueOf(ar.get('VGA_Owner_Name__c')),integer.valueof(ar.get('expr0')));
    				}
    			}
    		}
    		for(VGA_Ownership__c objOwnership : lstOwnership)
    		{
    			if(objOwnership.VGA_Owner_Name__c != null)
    			{
    				objOwnership.VGA_Welcome_pack_Status__c = 'Processed By Batch';
    				
    				if(mapofCurrentOwnership.containskey(objOwnership.VGA_Owner_Name__c) 
    						&& mapofCurrentOwnership.get(objOwnership.VGA_Owner_Name__c) != null && mapofCurrentOwnership.get(objOwnership.VGA_Owner_Name__c) >0)
					{
						objOwnership.VGA_No_of_Vehicles_in_this_File__c = mapofCurrentOwnership.get(objOwnership.VGA_Owner_Name__c);
					}
					
					if(mapofAllOwnership.containskey(objOwnership.VGA_Owner_Name__c) 
    						&& mapofAllOwnership.get(objOwnership.VGA_Owner_Name__c) != null && mapofAllOwnership.get(objOwnership.VGA_Owner_Name__c) >0)
					{
						objOwnership.VGA_No_of_Total_Vehicles__c = 	mapofAllOwnership.get(objOwnership.VGA_Owner_Name__c);
					}
					
    			}
    		}
    		update lstOwnership;
    	}
    }
    global void finish(Database.BatchableContext BC)
    {
    	 Map<String,VGA_Welcome_Data_Pack_Extract__c> mapofCS = VGA_Welcome_Data_Pack_Extract__c.getAll();
        
        if(mapofCS.get('Welcome Pack Data Extract') != null && mapofCS.get('Welcome Pack Data Extract').VGA_Process__c)
    	{
	    	string EndpointURL = 'callout:VGA_SkodaWelcomepack/initWPDataExtract';
	    	Http objhttp = new Http();
			HttpRequest req = new HttpRequest();
			req.setEndpoint(Endpointurl);
			req.setMethod('GET');
			req.setTimeout(120000);
			req.setHeader('Content-Type', 'application/json');
			HttpResponse res;
            
			if(!test.isRunningTest())
				res = objhttp.send(req);
			
		}
    }
}