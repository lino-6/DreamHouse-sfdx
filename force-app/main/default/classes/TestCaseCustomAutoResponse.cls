@isTest
public class TestCaseCustomAutoResponse
{
   //This method for valid Email testing
   @isTest public static void TestData_ValidEmail()
   {
Test.startTest();
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
      User u = new User(Alias = 'SysAdmin', Email='admin@bcinsourcing.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='admin@bcinsourcing.com');
        
      //  User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@camfed.org');
        insert u;
 
   
  // User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
   System.runAs(u) { 
   EmailTemplate et = new EmailTemplate (    developerName = 'test', 
                                             
                                             TemplateType= 'Text', 
                                             Name = 'test', 
                                             subject = 'test',
                                             body = 'test',
                                             FolderId = UserInfo.getUserId(),
                                             HtmlValue = 'test'
                                         );
   insert et;
   Email_To_Case__c etc = new Email_To_Case__c( name='academy@success.skoda.net.au;',
                                                Display_Name__c ='test',
                                                Service_Email__c='academy@success.skoda.net.au',
                                                Email_Template__c = 'test'
                                              );
   insert etc;
    
   Case c = new Case();
   c.Subject = 'Sample';
   c.Origin = 'Academy Email';
   c.SuppliedEmail = 'test@example.com';
   INSERT c; 
    
   EmailMessage objEmailMessage = new EmailMessage(
                                                       ToAddress = 'academy@success.skoda.net.au',
                                                       FromAddress = 'amhelp@succcess.volkswagen.net.au',
                                                       Subject = 'My First SF Task',
                                                       TextBody  = '1478',
                                                       ParentId  = c.id,
                                                       Incoming = true
                                                   );
   insert objEmailMessage;
   
      /* try{
           
           }
       catch(Exception e)  {
                           Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
                            System.AssertEquals(expectedExceptionThrown, false);
                           } */
   }
   }
       @isTest public static void TestData_TestUTFEmail()
   {
Test.startTest();
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
      User u = new User(Alias = 'SysAdmin', Email='admin@bcinsourcing.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='admin@bcinsourcing.com');
        
      //  User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@camfed.org');
        insert u;
 
   
  // User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
   System.runAs(u) { 
   EmailTemplate et = new EmailTemplate (    developerName = 'test', 
                                             TemplateType= 'Text', 
                                             Name = 'test', 
                                             subject = 'test',
                                             body = 'test',
                                             FolderId = UserInfo.getUserId(),
                                             HtmlValue = 'test'
                                         );
   insert et;
   Email_To_Case__c etc = new Email_To_Case__c( name='academy40%@success.skoda.net.au',
                                                Display_Name__c ='test case',
                                                Service_Email__c='academy@success.skoda.net.au',
                                                Email_Template__c = 'test'
                                              );
   insert etc;
    
   Case c = new Case();
   c.Subject = 'Sample';
   c.Origin = 'Academy Email';
   c.SuppliedEmail = 'test@example.com';
   INSERT c; 
    
   EmailMessage objEmailMessage = new EmailMessage(
                                                       ToAddress = 'academy40%@success.skoda.net.au;testcase@katzion.com',
                                                       FromAddress = 'amhelp@succcess.volkswagen.net.au',
                                                       Subject = 'My First SF Task',
                                                       TextBody  = '1478',
                                                       ParentId  = c.id,
                                                       Incoming = true
                                                   );
   
   
       try{
           insert objEmailMessage;
           }
       catch(Exception e)  {
                           Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
                            System.AssertEquals(expectedExceptionThrown, false);
                           } 
   }
   }
   @isTest public static void TestData_MultipleValidEmail()
   {
Test.startTest();
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
      User u = new User(Alias = 'SysAdmin', Email='admin@bcinsourcing.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='admin@bcinsourcing.com');
        
      //  User u1 = new User(Alias = 'standt1',Country='United Kingdom',Email='demo1@randomdemodomain.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US',ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', UserName='dprobertdemo1@camfed.org');
        insert u;
 
   
  // User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
   System.runAs(u) { 
   EmailTemplate et = new EmailTemplate (    developerName = 'test', 
                                             TemplateType= 'Text', 
                                             Name = 'test', 
                                             subject = 'test',
                                             body = 'test',
                                             FolderId = UserInfo.getUserId(),
                                             HtmlValue = 'test'
                                         );
   insert et;
   Email_To_Case__c etc = new Email_To_Case__c( name='academy@success.skoda.net.au',
                                                Display_Name__c ='test',
                                                Service_Email__c='academy@success.skoda.net.au',
                                                Email_Template__c = 'test'
                                              );
   insert etc;
    
   Case c = new Case();
   c.Subject = 'Sample';
   c.Origin = 'Academy Email';
   c.SuppliedEmail = 'test@example.com';
   INSERT c; 
    
   EmailMessage objEmailMessage = new EmailMessage(
                                                       ToAddress = 'academy@success.skoda.net.au;test@katzion.com',
                                                       FromAddress = 'amhelp@succcess.volkswagen.net.au',
                                                       Subject = 'My First SF Task',
                                                       TextBody  = '1478',
                                                       ParentId  = c.id,
                                                       Incoming = true
                                                   );
   
   
       try{
           insert objEmailMessage;
           }
       catch(Exception e)  {
                           Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
                            System.AssertEquals(expectedExceptionThrown, false);
                           } 
   }
   }
   //this method for Invalid Email testing
   @isTest public static void TestData_InvalidEmail()
   {
 Test.startTest();
   User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
   System.runAs(thisUser) { 
   EmailTemplate et = new EmailTemplate (    developerName = 'test', 
                                             TemplateType= 'Text', 
                                             Name = 'test', 
                                             subject = 'test',
                                             body = 'test',
                                             FolderId = UserInfo.getUserId(),
                                             HtmlValue = 'test'
                                         );
   insert et;
   Email_To_Case__c etc = new Email_To_Case__c( name='athira.p@katzion.com',
                                                Display_Name__c ='test',
                                                Service_Email__c= 'athira.p@katzion.com',
                                                Email_Template__c = 'test'
                                              );
   insert etc;
    
   Case c = new Case();
   c.Subject = 'Sample';
   c.Origin = 'Academy Email';
   c.SuppliedEmail = 'test@example.com';
   INSERT c; 
    
   EmailMessage objEmailMessage = new EmailMessage(    ToAddress = 'athira.p@katzion.com',
                                                       FromAddress = 'athira.p@katzion.com',
                                                       Subject = 'My First SF Task',
                                                       TextBody  = '1478',
                                                       ParentId  = c.id,
                                                       Incoming = true
                                                   );
   
   
       try{
           insert objEmailMessage;
           }
       catch(Exception e)  {
                           Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
                            System.AssertEquals(expectedExceptionThrown, true);
                           } 
   }
   }

   //This method for wrong case_id testing
  /* @isTest public static void TestData_Case()
   {
    Case c = new Case();
   c.Subject = 'Sample3';
   c.Origin = 'Academy Email3';
   c.SuppliedEmail = 'test3@example.com';
   INSERT c; 
   Case c2 = new Case();
   c2.Subject = 'Sample3';
   c2.Origin = 'Academy Email3';
   c2.SuppliedEmail = 'test3@example.com';
   INSERT c2; 
   
   EmailMessage objEmailMessage = new EmailMessage(    ToAddress = 'athira.p2@katzion.com',
                                                       FromAddress = 'athira.p2@katzion.com',
                                                       Subject = 'My First SF Task',
                                                       TextBody  = '1478',
                                                       ParentId  = null,
                                                       Incoming = true
                                                   );
   
   
       try{
           insert objEmailMessage;
          
           }
       catch(Exception e)  {
                           Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
                            System.AssertEquals(expectedExceptionThrown, false);
                           } 
  
   }*/
}