/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
private class VGA_PreLaunchAssignmentBatchTracker 
{
    public static VGA_Pre_Launch_Configuration__c objPrelaunchConfiguration;
    public static Lead objLead;
    public static VGA_Dealership_Profile__c objDealershipProfile;
    public static VGA_Trading_Hour__c objTradingHour;
    public static VGA_Working_Hour__c objWorkingHour;
    public static Account objAccount;
    public static Contact objContact;
    public static User objUser;
    public static VGA_User_Profile__c objUserPro;
    public static VGA_Working_Hour__c objWorking;    
    public static VGA_Working_Hour__c objWorking1;    
    public static VGA_Subscription__c objSubscription;
    
    static testmethod void unittest1()
    {
        LoadData();
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Nominated User';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        objDealershipProfile.VGA_Nominated_Contact__c = objcontact.id;
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            objPrelaunchConfiguration.VGA_Time_Interval__c = 20;
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);
            Database.executebatch(objBatch);    
        Test.stopTest();
    } 
     
    static testmethod void unittest2()
    {
        LoadData();
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);
            Database.executebatch(objBatch);    
        Test.stopTest();
    }
    
    static testmethod void unittest3()
    {
        LoadData();
        
        VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','All');   
        insert objPH;
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);  
            Database.executebatch(objBatch);    
        Test.stopTest();
    }
    
    static testmethod void unittest4()
    {
        LoadData();
        
        VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','All');   
        insert objPH;
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);  
            Database.executebatch(objBatch);    
        Test.stopTest();
    }
    
    static testmethod void unittest5()
    {
        LoadData();
        
        VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','ACT');   
        insert objPH;
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);  
            Database.executebatch(objBatch);    
        Test.stopTest();
    }
    
    static testmethod void unittest6()
    {
        LoadData();
        
        VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','ACT');   
        insert objPH;
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
        
        objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        Test.startTest();
            insert objPrelaunchConfiguration;
            VGA_PreLaunchAssignmentBatch objBatch = new VGA_PreLaunchAssignmentBatch(objPrelaunchConfiguration.id);  
            Database.executebatch(objBatch);    
        Test.stopTest();
    }
    
    public static void LoadData()
    {
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '20225';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;
        
        objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        objUser = VGA_CommonTracker.CreateUser(objContact.id);
        
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = objUserPro.id;
        objWorking.VGA_Working__c = true;
        objWorking.Name =  system.now().format('EEEE');
        insert objWorking;  
        
        objWorking1 = [select id,VGA_Unique_Key__c from VGA_Working_Hour__c where id=:objWorking.Id];
        
        objLead = VGA_CommonTracker.createLead();
        objLead.VGA_is_pre_launched_Vehicle__c = true;
        objLead.VGA_Assignment_Done__c = false;
        objLead.VGA_Follow_up_Lead__c = true;
        objLead.email = 'test@test.com';
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Dealer_Code__c = '20225';
        objLead.VGA_Tracking_Id__c = '12345';
        update objLead;
        
        objPrelaunchConfiguration = new VGA_Pre_Launch_Configuration__c();
        objPrelaunchConfiguration.VGA_Number_of_Leads_per_Distribution__c = 1;
        objPrelaunchConfiguration.VGA_Status__c = 'New';
        objPrelaunchConfiguration.VGA_Time_Interval__c = 10;
        objPrelaunchConfiguration.VGA_Start_Batch__c = true;
        objPrelaunchConfiguration.VGA_Tracking_Id__c = '12345';
     }
}