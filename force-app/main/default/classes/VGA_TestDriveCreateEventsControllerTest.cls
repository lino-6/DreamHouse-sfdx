/*
*@Author : MOHAMMED ISHAQUE SHAIKH
*@DATE   : 2019-03-19
*@Description : Test class for the class VGA_TestDriveCreateEventsController. 
*@Updated on : 2019-04-09 By HIJITH NS
*/
@isTest(SeeAllData=false)
public class VGA_TestDriveCreateEventsControllerTest {
    
    
    @testSetup 
    public static void buildConfigList()
    {  
        List<contact> conList =new List<Contact>();
        List<User> UserList=new List<User>();
        List<VGA_User_Profile__c> conUserProfileList = new List<VGA_User_Profile__c> ();
        Account acc;
        
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Lead',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Account',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Contact',true));
        insert CustomList;
        
         list<VGA_MuleUrl__c> customsettingdata =   VGA_CommonTracker.buildCustomSettingforMule();
        
        insert customsettingdata;
            
        acc=VGA_CommonTracker.buildDealerAccount('aa','qqq');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;        
        Account AccountCon=VGA_CommonTracker.createAccount();
        //Creating Contact
        Contact con=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt1');
        conList.add(con);
        Contact con1=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt2');
        con1.VGA_Role__c ='Dealer Administrator';
        conList.add(con1);
        insert conList;
        conUserProfileList=[SELECT Id,Name,VGA_Contact__c,VGA_Available_TestDrive__c FROM VGA_User_Profile__c];
        for(VGA_User_Profile__c t:conUserProfileList){
            t.VGA_Available_TestDrive__c=true;
        }
        update conUserProfileList;
        
        //Creating User 
        User PortalUser=VGA_CommonTracker.buildUser(conList[0].Id);
        PortalUser.IsActive  = True;
        UserList.add(PortalUser);
        User PortalUser1=VGA_CommonTracker.buildUser(conList[1].Id);
        PortalUser1.IsActive  = True;
        UserList.add(PortalUser1);
        Insert UserList; 
        
    }
    
    
  /**
  * @author HIjith NS
  * @date 09/04/2019
  * @description Method that test the getPilotContacts Method.
  *              Method returns list of pilot contacts from Parent Account of logged in User.
  */
    @isTest
    static  void Test01_getPilotContacts_whenValidLoggedInUser_ReturnListOfPilotContacts(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        
        Test.startTest();
        System.runAs(UserList[0]){
            noOfContacts = VGA_TestDriveCreateEventsController.getPilotContacts().size();
        }
        Test.stopTest();
        System.assertEquals(2,noOfContacts);
    }
  /**
  * @author Hijith NS
  * @date 09/04/2019
  * @description Method that tests the getActiveBrochures Methods.
  *              Methos return List of ActiveBrochures;
  */
  static testMethod void test02_getActiveBrochures_ReturnListOfActiveBrochures(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
        insert activeBrochuresList;
        Test.startTest();
        System.runAs(UserList[0]){
            
            noOfActviebrochers =VGA_TestDriveCreateEventsController.getActiveBrochures().size();
        }
        Test.stopTest();
        System.assertEquals(2,noOfActviebrochers);
    }
  /**
  * @author Hijith NS
  * @date 09/04/2019
  * @description test Method to test the CreateEvents Method.
  */
  static testMethod void test04_CreateEvents_CreatesEventOnValidOwnerID(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        
        Account AccountCon=VGA_CommonTracker.createAccount();
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        List<Contact>conList = [Select id,firstName,LastName from contact where firstName like '%TestCnt%'];
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        Test.startTest();
        System.runAs(UserList[0]){
            
           
            List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
            insert activeBrochuresList;    
            
            Event eve=new Event();
            eve.VGA_LeadFirstName_TestDrive__c='Test';
            eve.VGA_LeadLastName_TestDrive__c ='BAC';
            eve.VGA_Phone_TestDrive__c='1234567890';
            eve.VGA_Email_TestDrive__c='shaikhisaaq@gmail.com';
            eve.startDateTime=System.now();
            leadId = VGA_TestDriveCreateEventsController.createTestDriveLead(eve,'Test Drive Appointments',activeBrochuresList[0].id,true);
            MuleRequestMock fakeResponse1 = new MuleRequestMock(200, '{"primary_id" : "'+AccountCon.Id+'"}');
            
            Test.setMock(HttpCalloutMock.class, fakeResponse1);               
            result = VGA_TestDriveCreateEventsController.createEvents(eve,'Test Drive Appointments',activeBrochuresList[0].id,true,conList[0].Id);
            
        }
        Test.stopTest();
        System.assertEquals('Event created',result);
    }
 /**
  * @author Hijith NS
  * @date 09/04/2019
  * @description Method to test the CreateEvents Methods - for case if events already exists.
  */
  static testMethod void test06_CreateEvents_returnErrorIFEventAlreadyExists(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        Account AccountCon=VGA_CommonTracker.createAccount();
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
         List<Contact>conList = [Select id,firstName,LastName from contact where firstName like '%TestCnt%'];
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        Test.startTest();
        System.runAs(UserList[0]){
            
            List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
            insert activeBrochuresList;    
            
            Event eve=new Event();
            eve.VGA_LeadFirstName_TestDrive__c='Test';
            eve.VGA_LeadLastName_TestDrive__c ='BAC';
            eve.VGA_Phone_TestDrive__c='1234567890';
            eve.VGA_Email_TestDrive__c='shaikhisaaq@gmail.com';
            eve.startDateTime=System.now();
            MuleRequestMock fakeResponse1 = new MuleRequestMock(200, '{"primary_id" : "'+AccountCon.Id+'"}');            
            Test.setMock(HttpCalloutMock.class, fakeResponse1);       
            VGA_TestDriveCreateEventsController.createEvents(eve,'Test Drive Appointments',activeBrochuresList[0].id,true,conList[0].Id);
            result = VGA_TestDriveCreateEventsController.createEvents(eve,'Test Drive Appointments',activeBrochuresList[0].id,true,conList[0].Id);
            
        }
        Test.stopTest();
        System.assertEquals('Error: Appointment already exists on the selected time.',result);
    }
    
 /**
  * @author Hijith NS
  * @date 09/04/2019
  * @description Method to test the createTestDriveLead Method for eventtype equal to Test Drive Appointments.
  */
 static testMethod void test05_CreateTestDriveLead(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        List<Contact>conList = [Select id,firstName,LastName from contact where firstName like '%TestCnt%'];
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        Test.startTest();
        System.runAs(UserList[0]){
            
            List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
            insert activeBrochuresList;    
            
            Event eve=new Event();
            eve.VGA_LeadFirstName_TestDrive__c='New';
            eve.VGA_LeadLastName_TestDrive__c ='PAx';
            eve.VGA_Phone_TestDrive__c='5555555';
            eve.VGA_Email_TestDrive__c='test@test5.com';
            eve.startDateTime=System.now();
            leadId = VGA_TestDriveCreateEventsController.createTestDriveLead(eve,'Test Drive Appointments',activeBrochuresList[0].id,true);
            MuleRequestMock fakeResponse1 = new MuleRequestMock(200, '{"primary_id" : null}');
            Test.setMock(HttpCalloutMock.class, fakeResponse1); 
        }
        Test.stopTest();
                System.assertNotEquals(null,leadId);

    }

    /**
  * @author Hijith NS
  * @date 09/04/2019
  * @description Method tp test the createTestDriveLead Method for eventtype Not Equal to Test Drive Appointments.
  */
    static testMethod void test07_CreateTestDriveLead_ForNonTestDriveEvents(){
        
        Integer noOfContacts,noOfActviebrochers;
        String result,negResult,leadId;
        List<User> UserList =[select id,IsActive,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        List<Contact>conList = [Select id,firstName,LastName from contact where firstName like '%TestCnt%'];
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
        insert activeBrochuresList;    
        Test.startTest();
        System.runAs(UserList[0]){
            Event eve=new Event();
            eve.VGA_LeadFirstName_TestDrive__c='New';
            eve.VGA_LeadLastName_TestDrive__c ='PAx';
            eve.VGA_Phone_TestDrive__c='5555555';
            eve.VGA_Email_TestDrive__c='test@test5.com';
            eve.startDateTime=System.now();
            leadId = VGA_TestDriveCreateEventsController.createTestDriveLead(eve,'Non Test Drive',activeBrochuresList[0].id,true);
            MuleRequestMock fakeResponse1 = new MuleRequestMock(200, '{"primary_id" : null}');
            Test.setMock(HttpCalloutMock.class, fakeResponse1); 
        }
        Test.stopTest();
        System.assertEquals(null,leadId);
    }
    
}