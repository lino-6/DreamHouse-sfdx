/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_SMSResponseTracker 
{
	static testMethod void myUnitTest() 
    {
    	String json = '{\"sms\":1,\"send_at\":\"2017-12-06 17:45:49\",\"record_id\":\"a040k000003nDyeAAE\",\"recipients\":1,\"message_id\":97134794,\"error\":{\"description\":\"OK\",\"code\":\"SUCCESS\"},\"delivery_stats\":{\"responses\":0,\"pending\":0,\"optouts\":0,\"delivered\":0,\"bounced\":0},\"cost\":0.064}';
        VGA_SMSResponse obj = VGA_SMSResponse.parse(json);
        System.assert(obj != null);   
    }
}