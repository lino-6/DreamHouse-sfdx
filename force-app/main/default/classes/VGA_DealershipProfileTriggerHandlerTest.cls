/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_DealershipProfileTriggerHandlerTest
{
    @testsetup
 Static void buildDealershipProfiledetails()
 {    
        
     VGA_Triggers__c obj = new VGA_Triggers__c();
        obj.Name= 'VGA_Dealership_Profile__c';
        obj.VGA_Is_Active__c = true;
        insert obj;
        
       Account  objAccount = VGA_CommonTracker.createDealerAccount();
       Contact  objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
       Contact  objContact1 = VGA_CommonTracker.createDealercontact(objAccount.id);
       User     objUser =    VGA_CommonTracker.CreateUser(objContact.Id); 
      VGA_Dealership_Profile__c   objDP = VGA_CommonTracker.createDealershipProfile(objAccount.id);
      
       VGA_User_Profile__c       objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        objUserPro.VGA_Mobile__c = '1236598743';
        insert objUserPro;
        
  }  
     /**
    * @author Ganesh M
    * @date 07/12/2018
    * @description  This method is test to update Distribution Method on Account  Based on Distribution Method on Dealership Profile
    */
  
    static testMethod void myUnitTest01_update_DistributionMethod_on_Account_Based_on_Distribution_Method_on_Dealership_Profile() 
    {
        // TO DO: implement unit test
        Account acc = [select id,VGA_Distribution_Method_PV_Report__c from account where VGA_Brand__c='Volkswagen' limit 1 ];
        contact objcontact = [select id,VGA_Brand__c from contact where VGA_Brand__c='Volkswagen' limit 1];
        VGA_Dealership_Profile__c obdp = [select id,name from VGA_Dealership_Profile__c where VGA_Distribution_Method__c = 'Round Robin' limit 1];
        obdp.VGA_Nominated_Contact__c = objContact.id;
        obdp.VGA_Distribution_Method__c = ' Nominated User';
        
     try{
        update obdp;
        }
        catch(exception e){}
        
       test.starttest();
       VGA_Dealership_Profile__c obdp1 = [select id,name,VGA_Nominated_Contact__c,VGA_Distribution_Method__c,VGA_Dealership__c,VGA_Dealership__r.VGA_Distribution_Method_PV_Report__c from VGA_Dealership_Profile__c where VGA_Distribution_Method__c = 'Nominated User' limit 1];
       
       System.assertEquals(obdp1.VGA_Distribution_Method__c,obdp1.VGA_Dealership__r.VGA_Distribution_Method_PV_Report__c);
       test.stopTest();
        
    }
    
   
     /**
    * @author Ganesh M
    * @date 07/12/2018
    * @description  This method is test to update Nominated user on Dealership Profile Based on Nominated Contact
    */
  
    static testMethod void myUnitTest02_update_Nominateduser_Based_on_Nominated_Contact() 
    {
        // TO DO: implement unit test
        
        contact objcontact = [select id,VGA_Brand__c from contact where VGA_Brand__c='Volkswagen' limit 1];
        VGA_Dealership_Profile__c obdp = [select id,name from VGA_Dealership_Profile__c where VGA_Distribution_Method__c = 'Round Robin' limit 1];
        obdp.VGA_Nominated_Contact__c = objContact.id;
        obdp.VGA_Distribution_Method__c = ' Nominated User';
        
     try{
        update obdp;
        }
        catch(exception e){}
        
       test.starttest();
       VGA_Dealership_Profile__c obdp1 = [select id,name,VGA_Nominated_Contact__c,VGA_Nominated_User__c,VGA_Nominated_User__r.contactid from VGA_Dealership_Profile__c where VGA_Distribution_Method__c = 'Nominated User' limit 1];
       
       System.assertEquals(obdp1.VGA_Nominated_Contact__c,obdp1.VGA_Nominated_User__r.contactid);
       test.stopTest();
        
    }  
    
   /**
    * @author Ganesh M
    * @date 07/12/2018
    * @description  This method is test to Throw an error when we set Unavailble to receive Leads to Nominated user.
    */
  
    static testMethod void myUnitTest03_Throw_an_error_when_we_set_Unavailble_to_receive_Leads_to_Nominated_user() 
    {
        // TO DO: implement unit test
        
        contact objcontact = [select id,VGA_Brand__c from contact where VGA_Brand__c='Volkswagen' limit 1];
        VGA_Dealership_Profile__c obdp = [select id,name from VGA_Dealership_Profile__c where VGA_Distribution_Method__c = 'Round Robin' limit 1];
        obdp.VGA_Nominated_Contact__c = objContact.id;
        obdp.VGA_Distribution_Method__c = ' Nominated User';
        
     try{
        update obdp;
        }
        catch(exception e){
        
                Boolean expectedExceptionThrown =  e.getMessage().contains('A contact cannot be set as Nominated User if it is unavailable to receive leads.') ? true : false;
 
                System.AssertEquals(expectedExceptionThrown, true);
                }
        
      
        
    }    
   
}