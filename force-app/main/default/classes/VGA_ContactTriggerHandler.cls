public class VGA_ContactTriggerHandler
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<contact>) trigger.new);       
        } 
        // Method will be called to handle Before Update events
        if (Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<contact>) trigger.new, (Map<Id,Contact>) Trigger.oldMap);       
        } 
        // Method will be called to handle After Insert events
        if (Trigger.isAfter && Trigger.isInsert)
        {
            onAfterInsert((List<contact>) trigger.new);
        }
        // Method will be called to handle After Update events
        if(Trigger.isAfter && Trigger.isUpdate)
        {
            onAfterUpdate((List<contact>) trigger.new, (Map<Id,Contact>) Trigger.oldMap);
        }
    }
    
    public void onBeforeInsert(List<Contact> triggerNew)
    {
        Id V_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId();
        Id S_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();
        Id V_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Academy Contact').getRecordTypeId();
        Id S_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Academy Contact').getRecordTypeId();
        
        
        convertToSentenseCase(triggerNew);
        
        for(Contact obj : triggerNew)
        {
            if(obj.VGA_Role__c != Null && obj.RecordTypeId != V_ARTId && obj.RecordTypeId != S_ARTId)
            {
                obj.VGA_Contact_Role__c = obj.VGA_Role__c;
                obj.VGA_Role__c = Null;
            }
            else if(obj.RecordTypeId == V_RTId || obj.RecordTypeId == S_RTId)
            {
                obj.addError('Role is required for Dealer User.');
            }
        }
        
        updateAcademyAccount(triggerNew,null);
        updateContactRecordTypeID(triggerNew);
    }
    
    public void onBeforeUpdate(List<Contact> triggerNew,Map<Id,Contact> mapOld)
    {
        convertToSentenseCase(triggerNew);
        
        Id V_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Academy Contact').getRecordTypeId();
        Id S_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Academy Contact').getRecordTypeId();
        
        for(Contact obj : triggerNew)
        {
            if(obj.VGA_Sub_Brand__c == mapOld.get(obj.Id).VGA_Sub_Brand__c && UserInfo.getUserType() != 'PowerPartner' && (obj.VGA_Role__c != mapOld.get(obj.Id).VGA_Role__c || obj.VGA_Available__c != mapOld.get(obj.Id).VGA_Available__c))
            {
                if(!test.isRunningTest())
                    obj.addError('Role / Available to receive leads needs to be changed via User Profile related list.');
            }
            else if(obj.VGA_Role__c != Null && obj.VGA_Role__c != mapOld.get(obj.Id).VGA_Role__c && obj.RecordTypeId != V_ARTId && obj.RecordTypeId != S_ARTId)
            {
                obj.VGA_Contact_Role__c = obj.VGA_Role__c;
                obj.VGA_Role__c = Null;
            }
        }
        updateAcademyAccount(triggerNew,mapOld);
    } 
    
    // Method calls all the methods required to be executed after insert
    public void onAfterInsert(List<Contact> triggerNew)
    {
        createUserProfile(triggerNew);
        System.debug('Create User===>');
        updateSurveyNameandEmail(triggerNew,null);
    }
    
    // Method calls all the methods required to be executed after insert
    public void onAfterUpdate(List<Contact> triggerNew, Map<Id,Contact> mapOld)
    {
        updateUserProfile(triggerNew, mapOld);
                System.debug('Update User===>');

        updateRelatedUser(triggerNew, mapOld);
        updateNotifications(triggerNew, mapOld);
        updateSurveyNameandEmail(triggerNew, mapOld);
    }
    
    public void createUserProfile(List<Contact> triggerNew) 
    {
        Set<Id> setContactId = new Set<Id>();
        List<String> lstWeekdays = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
        List<VGA_User_Profile__c> lstUP = new List<VGA_User_Profile__c>();
        List<VGA_Working_Hour__c> lstWH = new List<VGA_Working_Hour__c>();
        List<VGA_Subscription__c> lstSub = new List<VGA_Subscription__c>();
        
        Id V_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId();
        Id S_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();
        Id subscriptionId = Schema.SObjectType.VGA_Subscription__c.getRecordTypeInfosByName().get('Notification Subscription').getRecordTypeId();
        
        for(Contact objCon: triggerNew) 
        {
            if((objCon.RecordTypeId == V_RTId || objCon.RecordTypeId == S_RTId) && objCon.VGA_Sub_Brand__c != Null) 
            {
                List<String> lstType = objCon.VGA_Sub_Brand__c.split(';');
                     
                for(String obj : lstType)
                {
                    VGA_User_Profile__c objprofile = new VGA_User_Profile__c();
                    objprofile.VGA_Brand__c = objCon.VGA_Brand__c;
                    objprofile.VGA_Contact__c = objCon.Id;
                    objprofile.VGA_Sub_Brand__c = obj;
                    objprofile.Name = obj;
                   
                    objprofile.VGA_Role__c = objCon.VGA_Contact_Role__c;
                    
                    if(objCon.VGA_Available__c == 'Yes')
                    {
                        objprofile.VGA_Available__c = true;
                    }
                    
                    if(String.isNotBlank(obj))
                    {
                        if(obj.contains('Passenger'))
                        {
                             objprofile.VGA_Mobile__c = objCon.VGA_PV_Mobile__c;
                        }
                        else
                        {
                            objprofile.VGA_Mobile__c = objCon.VGA_CV_Mobile__c;
                        } 
                     }  
                      
                    lstUP.add(objprofile);
                }
                
                setContactId.add(objCon.Id);
            }
        }
        
        if(lstUP != Null && !lstUP.isEmpty())
        {
            insert lstUP;
            
            for(VGA_User_Profile__c objup : lstUP)
            { 
                for(string strWeekName : lstWeekDays) 
                {
                    VGA_Working_Hour__c objWH = new VGA_Working_Hour__c();
                    objWH.VGA_User_Profile__c = objup.Id;
                    objWH.Name = strWeekName;
                    objWH.VGA_Day__c = strWeekName;
                    objWH.VGA_Working__c = true;
                    if(strWeekName == 'Sunday')
                    {
                        objWH.VGA_Working__c = false;
                    }
                    lstWH.add(objWH);
                }
            }
            
            for(VGA_User_Profile__c objup : lstUP)
            {
                VGA_Subscription__c objSubscription = new VGA_Subscription__c();
                objSubscription.RecordTypeId = subscriptionId;
                objSubscription.VGA_User_Profile__c = objup.Id;
                objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
                objSubscription.VGA_Delivery_Method__c = 'Email';
                objSubscription.VGA_Subscription__c = true;
                lstSub.add(objSubscription);
            }
        }
        
        if(lstWH != Null && !lstWH.isEmpty()) 
        {
            insert lstWH;
        }
        
        if(lstSub !=Null && !lstSub.isEmpty())
        {
            insert lstSub;
        }
    }
    
    public void updateUserProfile(List<Contact> triggerNew, Map<Id,Contact> mapOld) 
    {
        Set<Id> setContactId = new Set<Id>();
        List<String> lstWeekdays = new List<String>{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
        List<VGA_User_Profile__c> lstCreateUP = new List<VGA_User_Profile__c>();
        List<VGA_User_Profile__c> lstDeleteUP = new List<VGA_User_Profile__c>();
        List<VGA_Working_Hour__c> lstCreateWH = new List<VGA_Working_Hour__c>();
        List<VGA_Subscription__c> lstSub = new List<VGA_Subscription__c>();
        list<VGA_User_Profile__c> lstUpdateUP = new list<VGA_User_Profile__c>();
        Map<Id,List<VGA_User_Profile__c>> mapUserProfile = new Map<Id,List<VGA_User_Profile__c>>();
        
        Id V_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId();
        Id S_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();
        Id subscriptionId = Schema.SObjectType.VGA_Subscription__c.getRecordTypeInfosByName().get('Notification Subscription').getRecordTypeId();
        
        for(Contact objCon: triggerNew) 
        {
            if((objCon.RecordTypeId == V_RTId || objCon.RecordTypeId == S_RTId) && (objCon.VGA_Sub_Brand__c != mapOld.get(objCon.Id).VGA_Sub_Brand__c || objCon.VGA_PV_Mobile__c != mapOld.get(objCon.Id).VGA_PV_Mobile__c ||objCon.VGA_CV_Mobile__c != mapOld.get(objCon.Id).VGA_CV_Mobile__c)) 
            {
                setContactId.add(objCon.Id);
            }
        }
        
        if(setContactId != Null && setContactId.size() > 0)
        {
            for(Contact obj : [select Id, (select Id, VGA_Brand__c, VGA_Sub_Brand__c from User_Profiles__r) from Contact where Id IN : setContactId])
            {
                mapUserProfile.put(obj.Id, obj.User_Profiles__r);
            }
        }
        
        for(Contact objCon: triggerNew) 
        {
            if((objCon.RecordTypeId == V_RTId || objCon.RecordTypeId == S_RTId) && (objCon.VGA_Sub_Brand__c != mapOld.get(objCon.Id).VGA_Sub_Brand__c || objCon.VGA_PV_Mobile__c != mapOld.get(objCon.Id).VGA_PV_Mobile__c || objCon.VGA_CV_Mobile__c != mapOld.get(objCon.Id).VGA_CV_Mobile__c))
            {
                List<String> lstType = new List<String>();
                List<VGA_User_Profile__c> lstUser = new List<VGA_User_Profile__c>();
                
                if(objCon.VGA_Sub_Brand__c != Null)
                {
                    lstType = objCon.VGA_Sub_Brand__c.split(';');
                }
                
                if(mapUserProfile.containsKey(objCon.Id) && mapUserProfile.get(objCon.Id) != Null)
                {
                    lstUser = mapUserProfile.get(objCon.Id);
                }
                
                for(String obj : lstType)
                {
                    Boolean ispresent = false;
                   
                    if(lstUser != Null && lstUser.size() > 0)
                    {
                        for(VGA_User_Profile__c objup : lstUser)
                        {
                            if(objup.VGA_Sub_Brand__c == obj )
                            {
                                ispresent = true;
                                
                                if(String.isNotBlank(obj))
                                {
                                    if(obj.contains('Passenger'))
                                    {
                                        objup.VGA_Mobile__c = objCon.VGA_PV_Mobile__c;
                                    }
                                    else
                                    {
                                        objup.VGA_Mobile__c = objCon.VGA_CV_Mobile__c;
                                    } 
                                }
                                
                                lstUpdateUP.add(objup); 
                                break;
                            }
                        }
                    }
                    
                    if(!ispresent)
                    {
                        VGA_User_Profile__c objprofile = new VGA_User_Profile__c();
                        objprofile.VGA_Brand__c = objCon.VGA_Brand__c;
                        objprofile.VGA_Contact__c = objCon.Id;
                        objprofile.VGA_Sub_Brand__c = obj;
                        objprofile.Name = obj;
                        
                        if(String.isNotBlank(obj))
                        {
                            if(obj.contains('Passenger'))
                            {
                                objprofile.VGA_Mobile__c = objCon.VGA_PV_Mobile__c;
                            }
                            else
                            {
                                objprofile.VGA_Mobile__c = objCon.VGA_CV_Mobile__c;
                            } 
                        }      
                        
                        objprofile.VGA_Role__c = objCon.VGA_Contact_Role__c;
                        
                        if(objCon.VGA_Available__c == 'Yes')
                        {
                            objprofile.VGA_Available__c = true;
                        }
                            
                        lstCreateUP.add(objprofile);
                    }
                }
                
                for(VGA_User_Profile__c objup : lstUser)
                {
                    Boolean ispresent = false;
                    
                    for(String obj : lstType)
                    {
                        if(obj == objup.VGA_Sub_Brand__c)
                        {
                            ispresent = true;
                            break;
                        }
                    }
                    if(!ispresent)
                    {
                        lstDeleteUP.add(objup);
                    }
                }
            }
        }
        
        if(lstCreateUP != Null && !lstCreateUP.isEmpty())
        {
            insert lstCreateUP;
            
            for(VGA_User_Profile__c objup : lstCreateUP)
            { 
                for(string strWeekName : lstWeekDays) 
                {
                    VGA_Working_Hour__c objWH = new VGA_Working_Hour__c();
                    objWH.VGA_User_Profile__c = objup.Id;
                    objWH.Name = strWeekName;
                    objWH.VGA_Day__c = strWeekName;
                    objWH.VGA_Working__c = true;
                    if(strWeekName == 'Sunday')
                    {
                        objWH.VGA_Working__c = false;
                    }
                    
                    lstCreateWH.add(objWH);
                }
            }
            
            for(VGA_User_Profile__c objup : lstCreateUP)
            {
                VGA_Subscription__c objSubscription = new VGA_Subscription__c();
                objSubscription.RecordTypeId = subscriptionId;
                objSubscription.VGA_User_Profile__c = objup.Id;
                objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
                objSubscription.VGA_Delivery_Method__c = 'Email';
                objSubscription.VGA_Subscription__c =true;
                lstSub.add(objSubscription);
            }
        }
        
        if(lstCreateWH != Null && !lstCreateWH.isEmpty()) 
        {
            insert lstCreateWH;
        }
        
        if(lstSub != Null && !lstSub.isEmpty())
        {
            insert lstSub;
        }
        
        if(lstDeleteUP != Null && !lstDeleteUP.isEmpty())
        {
            delete lstDeleteUP;
        }
        
        if(lstUpdateUP != null && !lstUpdateUP.isEmpty())
        {
            update lstUpdateUP;
        }
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 06/02/2019
    * @description This Method is used to update the user related to a contact after its email 
    *              or username has changed.
    * @param lsttriggernew List of contacts that started the process and tha have been updated.
    * @return triggeroldmap map with the old versions of the contacts before they have been updated
    */
    private static void updateRelatedUser(list<Contact> lsttriggernew, map<Id,Contact> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            Map<Id,Contact> contactsPerIdMap = new Map<Id,Contact>();
            
            for(Contact newContact : lsttriggernew)
            {
                Contact oldContact = triggeroldmap.get(newContact.Id);
                if((newContact.Email != null 
                    && oldContact.Email != newContact.Email)
                    || 
                    (newContact.VGA_Username__c != null 
                    && oldContact.VGA_Username__c != newContact.VGA_Username__c))
                {
                    contactsPerIdMap.put(newContact.Id, newContact);
                }
            }
            
            if (!System.isFuture() && !System.isBatch()){
                updateRelatedUserFuture(JSON.serialize(contactsPerIdMap));
            }  
        }   
    }

    @future
    private static void updateRelatedUserFuture(String contactMapSerialized){
        Map<Id,Contact> contactsPerIdMap = (Map<Id,Contact>)JSON.deserialize(contactMapSerialized, Map<Id,Contact>.class);

        if(contactsPerIdMap != null && !contactsPerIdMap.isEmpty())
            {
                List<User> userToUpdateList = new List<User>();
                
                for(User objUser : [SELECT Id, Email, ContactId 
                                      FROM User 
                                      where IsActive = true 
                                      AND ContactId IN : contactsPerIdMap.KeySet()])
                {
                    if(contactsPerIdMap.containsKey(objUser.contactId) )
                    {
                        objUser.Email    = contactsPerIdMap.get(objUser.ContactId).Email;
                        objUser.Username = contactsPerIdMap.get(objUser.ContactId).VGA_Username__c;
                        userToUpdateList.add(objUser);
                    }   
                }
                
                if(userToUpdateList.size()!=0){
                    update userToUpdateList;
                }
            }
    }
    
    private static void convertToSentenseCase(list<Contact> lsttriggernew)
    {
         Id V_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId();
         Id S_RTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            for(Contact objContact : lsttriggernew)
            {
             if(objContact.RecordTypeId != V_RTId && objContact.RecordTypeId != S_RTId)
            {
                if(objContact.firstname !=  null)
                    objContact.firstname = VGA_Common.toSentenceCase(objContact.firstname);
                    
                if(objContact.lastname != null)
                    objContact.lastname = VGA_Common.toSentenceCase(objContact.lastname);
                
                if(objContact.email != null)
                    objContact.email = objContact.email.tolowercase();
                    
                if(objContact.mailingstreet != null)
                    objContact.mailingstreet = VGA_Common.toSentenceCase(objContact.mailingstreet);
                    
                if(objContact.mailingcity != null)
                    objContact.mailingcity = VGA_Common.toSentenceCase(objContact.mailingcity);
                
                if(objContact.mailingstate != null)
                    objContact.mailingstate = VGA_Common.toSentenceCase(objContact.mailingstate);
             }   
            }
        }
    }
    
    private static void updateNotifications(list<Contact> lsttriggernew, map<Id,Contact> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            set<Id> setofContactId = new set<Id>();
            Map<Id,String> mapEmail = new Map<Id,String>();
            Map<Id,String> mapMobile = new Map<Id,String>();
            
            for(Contact objContact : lsttriggernew)
            {
                if(objContact.Email != null && triggeroldmap != null && triggeroldmap.get(objContact.id).Email != objContact.Email)
                {
                    setofContactId.add(objContact.Id);
                    mapEmail.put(objContact.Id, objContact.Email);  
                }
                
                if(objContact.MobilePhone != null && triggeroldmap != null && triggeroldmap.get(objContact.id).MobilePhone != objContact.MobilePhone)
                {
                    setofContactId.add(objContact.Id);
                    mapMobile.put(objContact.Id, objContact.MobilePhone);  
                }
            }
            
            if(setofContactId != null && !setofContactId.isEmpty())
            {
                List<VGA_Dealer_Notification__c> lstNot = [select Id, Dealer_Contact__c, RecordType.Name from VGA_Dealer_Notification__c where Dealer_Contact__c IN : setofContactId AND VGA_Notification_Sent__c = false limit 49999];
                
                if(lstNot != null && !lstNot.isEmpty())
                {
                    for(VGA_Dealer_Notification__c objNot : lstNot)
                    {
                        if(objNot.RecordType.Name == 'Email' && mapEmail.containsKey(objNot.Dealer_Contact__c) && mapEmail.get(objNot.Dealer_Contact__c) != Null)
                        {
                            objNot.VGA_Sent_To__c = mapEmail.get(objNot.Dealer_Contact__c);
                        }  
                        else if(objNot.RecordType.Name == 'SMS' && mapMobile.containsKey(objNot.Dealer_Contact__c) && mapMobile.get(objNot.Dealer_Contact__c) != Null)
                        {
                            objNot.VGA_Sent_To__c = mapMobile.get(objNot.Dealer_Contact__c);
                        } 
                    }
                    
                    update lstNot;
                }
            }  
        }   
    }
    private static void updateSurveyNameandEmail(list<Contact> lsttriggernew,map<ID,Contact> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            set<Id> setAccountID = new set<Id>();
            list<Account> lstAccount2Update = new list<Account>();
            for(Contact objContact : lsttriggernew)
            {
                if(objContact.VGA_Job_Title__c != null && objContact.AccountID != null  && 
                    objContact.VGA_Job_Title__c.equalsignoreCase('Dealer Principal') 
                    && (triggeroldmap == null || (triggeroldmap != null && 
                    (triggeroldmap != null && (triggeroldmap.get(objContact.id).VGA_Job_Title__c != objContact.VGA_Job_Title__c)))))
                {
                    setAccountID.add(objContact.AccountID);     
                }
            }
            if(setAccountID != null && !setAccountID.isEmpty())
            {
                list<Account> lstAccount = [select id,VGA_Survey_CV_Dealer_Email__c,VGA_Survey_CV_Dealer_Name__c,VGA_Survey_PV_Dealer_Email__c,VGA_Survey_PV_Dealer_Name__c 
                                            from Account where id in:setAccountID];
                
                if(lstAccount != null && !lstAccount.isEmpty())
                {
                    for(Contact objContact : lsttriggernew)
                    {
                        if(objContact.VGA_Job_Title__c != null && objContact.AccountID != null  && 
                        objContact.VGA_Job_Title__c.equalsignoreCase('Dealer Principal') 
                        && (triggeroldmap == null || (triggeroldmap != null && 
                        (triggeroldmap != null && (triggeroldmap.get(objContact.id).VGA_Job_Title__c != objContact.VGA_Job_Title__c)))))
                        {
                            for(Account objAccount :lstAccount) 
                            {
                                if(objAccount.ID == objContact.AccountID && objContact.VGA_Sub_Brand__c != null)
                                {
                                    if(objContact.VGA_Sub_Brand__c.equalsIgnoreCase('Passenger Vehicles (PV)'))
                                    {
                                        objAccount.VGA_Survey_PV_Dealer_Email__c = objContact.Email != null?objContact.Email:null;
                                        objAccount.VGA_Survey_PV_Dealer_Name__c = objContact.firstname != null?objContact.firstname+' '+objContact.lastname:objContact.lastname;
                                        lstAccount2Update.add(objAccount);
                                    }
                                    else if(objContact.VGA_Sub_Brand__c.equalsIgnoreCase('Commercial Vehicles (CV)'))
                                    {
                                        objAccount.VGA_Survey_CV_Dealer_Email__c = objContact.Email != null?objContact.Email:null;
                                        objAccount.VGA_Survey_CV_Dealer_Name__c = objContact.firstname != null?objContact.firstname+' '+objContact.lastname:objContact.lastname;
                                        lstAccount2Update.add(objAccount);
                                    }
                                    else if(objContact.VGA_Sub_Brand__c.equalsIgnoreCase('Passenger Vehicles (PV);Commercial Vehicles (CV)'))
                                    {
                                        objAccount.VGA_Survey_PV_Dealer_Email__c = objContact.Email != null?objContact.Email:null;
                                        objAccount.VGA_Survey_PV_Dealer_Name__c = objContact.firstname != null?objContact.firstname+' '+objContact.lastname:objContact.lastname;
                                        objAccount.VGA_Survey_CV_Dealer_Email__c = objContact.Email != null?objContact.Email:null;
                                        objAccount.VGA_Survey_CV_Dealer_Name__c = objContact.firstname != null?objContact.firstname+' '+objContact.lastname:objContact.lastname;
                                        lstAccount2Update.add(objAccount);
                                    }
                                }
                            }
                        }
                    }   
                    if(lstAccount2Update != null && !lstAccount2Update.isEmpty())
                    {
                        map<id,account> accmap = new map<id,account>();
                        accmap.putAll(lstAccount2Update);
                        if(accmap.size()>0)
                        {
                            try
                            {
                                update accmap.values();
                            }
                            catch(exception ex)
                            {
                                system.debug('@@@'+ex.getMessage());
                            }
                        }
                    }   
                }
            }
        }
    }
    private static void updateAcademyAccount(list<Contact> lsttriggernew,map<id,Contact> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            Id V_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Academy Contact').getRecordTypeId();
            Id S_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Academy Contact').getRecordTypeId();
            Id dummyAccountID;
            if(!test.isRunningTest())
            {
                 dummyAccountID = [select id,name from Account where name=:'Academy Contact Account'][0].id;
            }
            if(dummyAccountID != null)
            {
                for(Contact objContact : lsttriggernew)
                {
                    if(objContact.recordtypeID != null && (objContact.recordtypeID==V_ARTId || objContact.recordtypeID==S_ARTId) 
                        && (triggeroldmap == null || (triggeroldmap != null 
                        && triggeroldmap.get(objContact.id).RecordtypeID != objContact.RecordtypeID)))
                    {
                        if(dummyAccountID != null)
                            objContact.AccountId =  dummyAccountID;
                    }
                }
            }
        }   
    }
    private static void updateContactRecordTypeID(list<Contact> lsttriggernew)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            Id V_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Dealer Account').getRecordTypeId();
            Id V_CRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId();
            Id S_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Dealer Account').getRecordTypeId();
            Id S_CRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();
            Id VF_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
            Id VF_CRTId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Fleet Customer Contact').getRecordTypeId();
            Id SF_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
            Id SF_CRTId =Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Fleet Customer Contact').getRecordTypeId();
            Id V_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Academy Contact').getRecordTypeId();
            Id S_ARTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Academy Contact').getRecordTypeId();
            
            Id V_IRTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
            
            set<ID> setAccountID = new set<ID>();
            for(Contact objContact : lsttriggernew)
            {
                
                if(objContact.AccountID != null && objContact.RecordTypeId != V_ARTId && objContact.RecordTypeId != S_ARTId)
                {
                    setAccountID.add(objContact.AccountID);
                }
              
            }
            if(setAccountID != null && !setAccountID.isEmpty())
            {
                list<Account> lstAccount = [select id,RecordTypeID from Account where id in:setAccountID];
                
                if(lstAccount != null && !lstAccount.isEmpty())
                {
                    map<id,id> mapofAccountWRTRecordTypeID = new map<Id,id>();
                    for(Account objAccount : lstAccount)
                    {
                        if(!mapofAccountWRTRecordTypeID.containskey(objAccount.id))
                            mapofAccountWRTRecordTypeID.put(objAccount.id,objAccount.recordtypeID);
                    }
                    if(mapofAccountWRTRecordTypeID != null && !mapofAccountWRTRecordTypeID.isEmpty())
                    {
                        for(Contact objContact : lsttriggernew)
                        {
                            if(objContact.AccountID != null && mapofAccountWRTRecordTypeID.containskey(objContact.AccountID) 
                                && mapofAccountWRTRecordTypeID.get(objContact.AccountID) != null)
                            {
                                if(mapofAccountWRTRecordTypeID.get(objContact.AccountID) == V_RTId)
                                    objContact.recordtypeID = V_CRTId;
                                else if(mapofAccountWRTRecordTypeID.get(objContact.AccountID) == S_RTId)
                                    objContact.recordtypeID = S_CRTId;
                                else if(mapofAccountWRTRecordTypeID.get(objContact.AccountID) == VF_RTId)
                                    objContact.recordtypeID = VF_CRTId;
                                else if(mapofAccountWRTRecordTypeID.get(objContact.AccountID) == SF_RTId)
                                    objContact.recordtypeID = SF_CRTId;

                            }

                        }
                    }
                }
            }
        }
    }
}