global class VGA_ResetDealerContactPassword 
{
    
 webservice static Boolean resetPassword(string contact_id) 
  { 
  	
  		if(contact_id == null) 
  		{
  			return false;
  		}

		List<User> user = [SELECT Id FROM  User WHERE ContactId = :contact_id AND isActive = true AND isPortalEnabled = true LIMIT 1];
		
		if(user != null && !user.isEmpty())
		{
			for(User obj_user : user) 
			{
				system.resetPassword(obj_user.Id, true);
			}
			
			return true;
			
		}
		
		return false;
		
    }
    

  @AuraEnabled	
  public static Boolean resetDealerContactPassword(string contact_id) 
  { 
  	
  		if(contact_id == null) 
  		{
  			return false;
  		}

      	PageReference visualforcePage = Page.DCPassReset;
        String content;
      
      	if(Test.isRunningTest())
        {
            content = 'Start_Of_Session_Id12345End_Of_Session_Id';
        }
      	else
        {
      		content = visualforcePage.getContent().toString();
        }
      
        Integer s = content.indexOf('Start_Of_Session_Id') + 'Start_Of_Session_Id'.length(),
                e = content.indexOf('End_Of_Session_Id');
        String sessionId = content.substring(s, e);

      	boolean result;
      	
      	if(Test.isRunningTest())
        {
            result = true;
        }
      	else
        {
            VGA_ResetDealerContactPasswordSOAP.VGA_ResetDealerContactPassword stub = new VGA_ResetDealerContactPasswordSOAP.VGA_ResetDealerContactPassword();
            result = stub.resetPassword(contact_id, sessionId);	
        }
		return result;
		
    }
    
    @AuraEnabled
    public static contact getcontactDetail(string record_id)
    {
      contact objContact = [select Id, Name FROM Contact WHERE Id = :record_id LIMIT 1 ];
      return objContact;
    }    
    	
    
}