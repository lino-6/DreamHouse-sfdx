global class VGA_SendBatchSMS implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts
{
    List<VGA_Dealer_Notification__c> lstUpdateSms = new List<VGA_Dealer_Notification__c>();
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strQuery = 'select id,VGA_Lead__c,VGA_Lead__r.VGA_Brand__c,Dealer_Contact__c,VGA_Sent_From__c,VGA_Sent_To__c,VGA_Subscription_Type__c,VGA_Sent_Date_Time__c,VGA_Notification_Sent__c,VGA_Inserted_From_Batch__c,VGA_Message_Body__c,VGA_Notification_Response__c from VGA_Dealer_Notification__c';
        strQuery += ' where VGA_Inserted_From_Batch__c=true';
        strQuery += ' and VGA_Notification_Sent__c=false';
        strQuery += ' and VGA_Is_SMS__c = true'; 
        
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<VGA_Dealer_Notification__c> lstsms)
    {
        if(lstsms != null && !lstsms.isEmpty())
        {
            system.debug('@@@@@lstsms@@@@@@'+lstsms);
            Map<String,VGA_Notification_Setting__c> mapNotification = VGA_Notification_Setting__c.getAll();
            if(mapNotification.get('SMS') != null && !mapNotification.get('SMS').VGA_Process__c)
                return;
            
            List<VGA_NotificationTriggerHandler.jsonInput> lstSkodajsonInput;
            List<VGA_NotificationTriggerHandler.jsonInput> lstVolkeswagenjsonInput;
            
            List<VGA_Dealer_Notification__c> lstUpdateSms = new List<VGA_Dealer_Notification__c>();
            
            try
            {
                lstSkodajsonInput = new List<VGA_NotificationTriggerHandler.jsonInput>();
                lstVolkeswagenjsonInput = new List<VGA_NotificationTriggerHandler.jsonInput>();
                for(VGA_Dealer_Notification__c objsms : lstsms)
                {
                    string strSentTo = '';
                    if(objsms.VGA_Sent_To__c != Null && objsms.VGA_Message_Body__c != Null)
                    {
                        if((objsms.VGA_Sent_To__c.substring(0,3) == '+61') || (objsms.VGA_Sent_To__c.substring(0,3) == '+91'))
                        {
                            strSentTo = objsms.VGA_Sent_To__c;
                        }
                        else if(objsms.VGA_Sent_To__c.substring(0,3) != '+61')
                        {
                            if(objsms.VGA_Sent_To__c.substring(0,1) == '0')
                            {
                                strSentTo = '+61' + objsms.VGA_Sent_To__c.substring(1);
                            }
                            else if(objsms.VGA_Sent_To__c.substring(0,1) != '0')
                            {
                                strSentTo = '+61' + objsms.VGA_Sent_To__c;
                            }
                        }
                        
                        if(objsms.VGA_Lead__r.VGA_Brand__c=='Skoda')
                        {
                            VGA_NotificationTriggerHandler.jsonInput obj = new VGA_NotificationTriggerHandler.jsonInput();
                            obj.record_id = objsms.Id;
                            obj.from_id = objsms.VGA_Sent_From__c;
                            obj.to = strSentTo;
                            obj.textbody = objsms.VGA_Message_Body__c;
                            
                            lstSkodajsonInput.add(obj);
                        }
                        else{
                            
                            VGA_NotificationTriggerHandler.jsonInput obj = new VGA_NotificationTriggerHandler.jsonInput();
                            obj.record_id = objsms.Id;
                            obj.from_id = objsms.VGA_Sent_From__c;
                            obj.to = strSentTo;
                            obj.textbody = objsms.VGA_Message_Body__c;
                            
                            lstVolkeswagenjsonInput.add(obj);
                        }
                    }
                }
                String strBody = '';
                String VstrBody = '';
                
                if(lstSkodajsonInput != Null && !lstSkodajsonInput.isEmpty())
                {
                    strBody = JSON.serialize(lstSkodajsonInput);
                    system.debug('@@@'+strBody);
                }
                if(lstVolkeswagenjsonInput != Null && !lstVolkeswagenjsonInput.isEmpty())
                {
                    vstrBody = JSON.serialize(lstVolkeswagenjsonInput);
                    system.debug('@@@'+vstrBody);
                }
                
                if(strBody!='')
                {
                    lstUpdateSms=CallMuleAPi(strBody,'Skoda');
                }
                if(vstrBody!='')
                {
                    lstUpdateSms=CallMuleAPi(vstrBody,'Volkswagen');
                }
                
                
                if(lstUpdateSms != Null && !lstUpdateSms.isEmpty())
                {
                    update lstUpdateSms;
                }
            }
            catch(exception e)
            {
                VGA_Common.sendExceptionEmail('Exception in sending notification sms', 'Hi, <br/><br/> Sms Notification failed due to following error : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        Integer intFreq = integer.valueOf(label.VGA_SMS_Batch_Frequency);
        Datetime dt = system.now().addMinutes(intFreq);
        
        String day = string.valueOf(dt.day());
        String month = string.valueOf(dt.month());
        String hour = string.valueOf(dt.hour());
        String minute = string.valueOf(dt.minute());
        String second = string.valueOf(dt.second());
        String year = string.valueOf(dt.year());
        
        String strJobName = 'Send SMS Batch-' + String.valueof(dt);
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        
        if(!test.isRunningTest())
            system.schedule(strJobName, strSchedule, new VGA_ScheduleSendBatchSMS());
        
        List<CronTrigger> lstCron = [select Id, State, NextFireTime, CronJobDetailId from CronTrigger where NextFireTime = Null AND State = 'DELETED'];
        
        if(lstCron != Null && !lstCron.isEmpty())
        {
            try
            {
                for(CronTrigger obj : lstCron)  
                { 
                    system.abortjob(obj.id);
                }
            }
            catch(Exception e)
            {
                system.debug('@@Exception in deleting job : ' + e.getMessage());
            }
        }
    }
    
    global List<VGA_Dealer_Notification__c> CallMuleAPi(string strBody, String brand)
    {
        VGA_SMSResponse objjsonOutput;
        Http objhttp = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:Mule_SMS_Credentials');
        //req.setEndpoint('https://vga-services-katz-v1.au-s1.cloudhub.io/sendsms');
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setHeader('Content-Type', 'application/json');
        
        if(brand=='Volkswagen'&& VGA_MuleUrl__c.getValues('Volkswagen')!=null)
            
        {
            
            req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
            req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);  
        }
        else if(brand=='Skoda'&& VGA_MuleUrl__c.getValues('Skoda')!=null)
        {
            req.setHeader('client_id', VGA_MuleUrl__c.getValues('Skoda').Client_ID__c);
            req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Skoda').Secret_Id__c);  
        }
        
        
        
        req.setBody(strBody);
        HttpResponse res;
        if(!test.isRunningTest())
        {
            res = objhttp.send(req);
        }
        else
        {
            res = new HTTPResponse();
            res.setBody('[{"record_id":"a040k000003o0ZjAAI","message_id":null,"send_at":null,"recipients":null,"cost":null,"sms":null,"delivery_stats":null,"error":{"code":"error"}},{"record_id":"a040k000003o0ZkAAI","message_id":98535840,"send_at":"2017-12-15 10:57:15","recipients":1,"cost":0.064,"sms":1,"delivery_stats":{"delivered":0,"pending":0,"bounced":0,"responses":0,"optouts":0},"error":{"code":"SUCCESS","description":"OK"}}]');
        }
        String apiResponse =  res.getBody();
        system.debug('@@@@sapiResponse@@@@'+apiResponse);
        if(apiResponse.contains('Invalid Expression  : '))
        {
            apiResponse = apiResponse.replace('Invalid Expression  : ','');
        }
        if(apiResponse.substring(0,1) == '{')
        {
            apiResponse = '['+apiResponse+']';
        }
        apiResponse = '{"vga":' + apiResponse + '}';
        objjsonOutput = VGA_SMSResponse.parse(apiResponse);
        
        if(objjsonOutput != Null)
        {
            List<VGA_SMSResponse.cls_vga> lstres = objjsonOutput.vga;    
            
            for(VGA_SMSResponse.cls_vga objoutput : lstres)
            {
                if(objoutput.record_id != Null)
                {
                    VGA_Dealer_Notification__c objnot = new VGA_Dealer_Notification__c(Id = objoutput.record_id);
                    objnot.VGA_Sent_Date_Time__c = System.Now();
                    objnot.VGA_Notification_Sent__c = true;
                    objnot.VGA_Inserted_From_Batch__c = false;
                    objnot.VGA_Notification_Response__c = String.valueof(JSON.serialize(objoutput));
                    lstUpdateSms.add(objnot);
                }
            }
            
        }
        return lstUpdateSms;
    }
}