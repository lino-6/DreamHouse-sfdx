public with sharing class VGA_AllianzDataMappingHelper 
{
    public static String getAllianzProductCode(VGA_Vehicle_Policy__c objVehiclePolicy)
    {
        if(objVehiclePolicy.Is_Extended_Warranty__c == false)
        {
            return objVehiclePolicy.VGA_Policy_Code__c != null?objVehiclePolicy.VGA_Policy_Code__c:null;
        }

        if(objVehiclePolicy.VGA_Component_Type__c != null
            && (objVehiclePolicy.VGA_Component_Type__c.equalsignorecase('Roadside')
                || objVehiclePolicy.VGA_Component_Type__c.equalsignorecase('ROADSIDEAD')))
            {

                if(objVehiclePolicy.VGA_Program_Code__c != null
                    && (objVehiclePolicy.VGA_Program_Code__c.equalsignorecase('SIRA'))
                  )
                {
                    return 'SIRA'; 
                }
                else if(objVehiclePolicy.VGA_Component_Code__c != null
                        && (objVehiclePolicy.VGA_Component_Code__c.equalsignorecase('DW12')
                            || objVehiclePolicy.VGA_Component_Code__c.equalsignorecase('DW00')))
                {
                    return objVehiclePolicy.VGA_Policy_Code__c +'D'; 
                }
                else if(objVehiclePolicy.VGA_Component_Code__c != null
                        && (objVehiclePolicy.VGA_Component_Code__c.equalsignorecase('ADRD12')))
                {
                    return objVehiclePolicy.VGA_Policy_Code__c + 'G';
                }
                else if(objVehiclePolicy.VGA_Component_Code__c != null
                        && ((objVehiclePolicy.VGA_Component_Code__c.equalsignorecase('FW24'))
                            || (objVehiclePolicy.VGA_Component_Code__c.equalsignorecase('RD2YR'))))

                {
                    return objVehiclePolicy.VGA_Policy_Code__c + 'E';
                }
            }
        
        return '';
    }
}