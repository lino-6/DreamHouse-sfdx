global class VGA_ScheduleSendBatchSMS implements Schedulable
{
	global void execute(SchedulableContext sc) 
    {
        VGA_SendBatchSMS obj = new VGA_SendBatchSMS();
        Database.executeBatch(obj,200);
    }
}