public class VGA_ActiveBrochureJSON implements Comparable {
    
    public String name;
    public String brand;
    public String brochureCode;
    public String brochureFileURL;
    public String brochureLabel;
    public Boolean brochureRequestActive;
    public Boolean dealerCallbackActive;
    public String modelCarConfiguratorURL;
    public String imageURL;
    public String subBrand;
    public String thumbnailURL;
    /*public String carlineId;
public String carlineName;
public String carConfiguratorURL;*/
    public Decimal sortOrder;
    public Boolean testDriveActive;
    public List<Preference> preference;
    public List<Excites> excites;
    public class Excites {
        public string name;
    } 
    public String label;
    public class Preference implements Comparable{
        public Boolean default_x;
        public String label;
        public String value;
        public String imageURL;
        public String salesGroupId;
        public String salesGroupName;
        public String trimId;
        public String trimName;
        public Decimal sortOrder;
        public String carlineId;
        public String carlineName;
        public String carConfiguratorURL;
        
        public Integer compareTo(Object instance)
        {
            Preference that = (Preference)instance;
            
            // nulls last
            if (that.sortOrder == null) return 1;
            if (this.sortOrder == null) return -1;
            
            // descending
            return (this.sortOrder < that.sortOrder) ? -1 : 1;
        }
    }
    
    
    public VGA_ActiveBrochureJSON(VGA_Active_Brochure__c activeBrochure, List<VGA_Model_Attribute__c> modelAttibuteList,List<VGA_Excites_Options__c> modelExcitesList){
        this.name           = activeBrochure.Name;
        this.brand          = activeBrochure.VGA_Brand__c;
        this.brochureCode   = activeBrochure.VGA_Brochure_Code_Formula__c;
        this.brochureFileURL= activeBrochure.VGA_Brochure_File_URL__c;
        this.brochureLabel  = activeBrochure.VGA_Brochure_Label__c;
        this.imageURL       = activeBrochure.VGA_Brochure_Banner_Image_URL__c;
        this.subBrand       = activeBrochure.VGA_Sub_Brand__c;
        this.testDriveActive= activeBrochure.VGA_Test_Drive_Active__c;
        this.thumbnailURL   = activeBrochure.VGA_Thumbnail_Url__c ;
        // this.carlineId      = activeBrochure.VGA_Carline_Id__c;
        // this.carlineName    = activeBrochure.VGA_Carline_Name__c ;
        this.sortOrder      = activeBrochure.VGA_Sort_Order__c;
        // this.carConfiguratorURL   = activeBrochure.VGA_Car_Configurator_Url__c ;
        this.brochureRequestActive= activeBrochure.VGA_Brochure_Request_Active__c;
        this.dealerCallbackActive= activeBrochure.VGA_Dealer_Callback_Active__c;
        this.modelCarConfiguratorURL= activeBrochure.VGA_ModelCar_Configurator_URL__c;
        this.preference = new List<Preference>();
        this.excites = new List<Excites>();
        
        boolean isDefaultNeeded = modelAttibuteList.size() == 1 ? false : true;
        Integer lastSortOrder = 1;
        string defaultValue = 'I don\'t mind';
        Map<Integer, String> imagesPerSortOrderMap = new Map<Integer, String>();
        
        for(VGA_Model_Attribute__c modelAtribute : modelAttibuteList){
            if(modelAtribute.VGA_Preference__c == defaultValue)
            {
                isDefaultNeeded = false;
            }
            
            if(modelAtribute.VGA_Sort_Order__c != null
               && !imagesPerSortOrderMap.containsKey(modelAtribute.VGA_Sort_Order__c.intValue())){
                   imagesPerSortOrderMap.put(modelAtribute.VGA_Sort_Order__c.intValue(), modelAtribute.VGA_Model_image_URL__c);
               }
            
            lastSortOrder = getLastSortOrder(lastSortOrder, modelAtribute.VGA_Sort_Order__c);
            
            Preference newPreference = new Preference();
            newPreference.sortOrder = modelAtribute.VGA_Sort_Order__c ;
            // We need to show the VGA_Preference__c as it is given in the data [ as per client feedback]
            newPreference.label     = modelAtribute.VGA_Preference__c ; 
            newPreference.default_x = modelAttibuteList.size() == 1 ? true : modelAtribute.VGA_Is_Default_Preference__c;
            newPreference.value     = modelAtribute.VGA_Preference__c ;
            newPreference.imageURL  = modelAtribute.VGA_Model_image_URL__c ;
            newPreference.trimId    = modelAtribute.VGA_Trim_Id__c  ;
            newPreference.trimName  = modelAtribute.VGA_Trim_Name__c  ;
            newPreference.carlineId  = modelAtribute.VGA_Carline_Id__c;
            newPreference.carlineName  = modelAtribute.VGA_Carline_Name__c;
            newPreference.carConfiguratorURL  = modelAtribute.VGA_Car_Configurator_Url__c;
            newPreference.salesGroupId  = modelAtribute.VGA_Version_Sales_Group_Id__c  ;
            newPreference.salesGroupName= modelAtribute.VGA_Version_Sales_Group_Name__c  ;
            this.preference.add(newPreference);
        }
        
        if(isDefaultNeeded)
        {
            Preference newPreference = new Preference();            
            newPreference.label     = defaultValue;
            newPreference.default_x =  false;
            newPreference.value     = defaultValue;
            newPreference.imageURL  = imagesPerSortOrderMap.get(lastSortOrder);
            newPreference.sortOrder = lastSortOrder + 1;
            this.preference.add(newPreference);
        }
        
        this.preference.sort();
        for(VGA_Excites_Options__c modelExcite : modelExcitesList){
            Excites newexcite = new Excites();
            newexcite.name = modelExcite.name;
            this.excites.add(newexcite);
        } 
        
    }
    
    public Integer getLastSortOrder(Integer lastSortOrder, Decimal currentValue)
    {
        if(currentValue > lastSortOrder) {
            return currentValue.intValue();
        }
        
        return lastSortOrder;
    }
    
    public Integer compareTo(Object instance)
    {
        VGA_ActiveBrochureJSON that = (VGA_ActiveBrochureJSON)instance;
        
        // nulls last
        if (that.sortOrder == null) return 1;
        if (this.sortOrder == null) return -1;
        
        // descending
        return (this.sortOrder < that.sortOrder) ? -1 : 1;
    }
    
    public static String serializeSorted(List<VGA_ActiveBrochureJSON> input)
    {
        // you could add a convenience method which combines steps
        input.sort();
        return JSON.serialize(input);
    }
}