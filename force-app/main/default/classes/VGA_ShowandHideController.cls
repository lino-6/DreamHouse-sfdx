// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          13-oct-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to for Show and Hide Tab component.
public  Without Sharing class VGA_ShowandHideController 
{

    /**
    * @author Surabhi Ranjan
    * @date 13/10/2017
    * @description Method being used by the VGA_ShowandHideTab component where we return the 
    *              name of the role with the highest access that this contact has.
    *              We are meeting the following AC with this method:
    *              In case a contact has more than one related user profile, 
    *              we should consider always the highest role for access to Dealer CLMS.
    * @return String Name of the highest role that this contact has in his list of user profiles.
    */
    @AuraEnabled
    public static String getuserInformation()
    {
        String dealerRole = '';
        List<string>ProfileName = VGA_Common.getProfileName();
        Map<String, Integer> rolesMap = new Map<String, Integer>{'Dealer Administrator'=> 1, 
                                                                'Lead Controller'      => 2, 
                                                                'Consultant'           => 3,
                                                                'Dealer Portal Group'  => 4,
                                                                'Dealer Portal'        => 5};


        for(string objRole :ProfileName)
        {
            if(objRole.contains('Dealer Administrator'))
            {
               dealerRole = objRole;
               break;
            }
            else if(dealerRole == '')
            {
                dealerRole = objRole;
            }
            else if(dealerRole != '' 
                    && rolesMap.containsKey(dealerRole)
                    && rolesMap.containsKey(objRole))
            {
                if(rolesMap.get(objRole) < rolesMap.get(dealerRole)){
                    dealerRole = objRole;
                }
            }
        }
       
        if(dealerRole == ''){
            dealerRole = 'Consultant';
        }

        return dealerRole;   
    }

    /**
    * @author Surabhi Ranjan
    * @date 13/10/2017
    * @description Method being used by the VGA_ShowandHideTab component where we return 
    *              a Boolean that let us know if the current use Is a Loan Car dealer
    *              or not.
    * @return Boolean that will be true if the current user is a Loan Car Dealer and false if not.
    */
    @AuraEnabled
    public static boolean getloancar()
    {
        User ObjUser =[select Id,ContactId,Contact.AccountId,Contact.Account.Name,
                              Contact.Account.VGA_Brand__c,profile.Name,VGA_Account_Id__c,
                              VGA_Role__c,Contact.VGA_Brand__c, Contact.Account.VGA_Sub_Brand__c,VGA_Brand__c,
                              Contact.Account.VGA_Dealer_Code_Text__c,VGA_Dealer_Code__c,VGA_Is_Loan_Car_Dealer__c,
                              VGA_Account_Name__c 
                              FROM User 
                              WHERE Id=:UserInfo.getUserId() 
                              LIMIT 1];
        return ObjUser.VGA_Is_Loan_Car_Dealer__c;
    }
    
    /**
    * @author Surabhi Ranjan
    * @date 13/10/2017
    * @description Method being used by the VGA_ShowandHideTab component where we return 
    *              a String with the url that is being used in this dealer portal.
    *              It will be different if we are calling the brand is Skoda or
    *              Volkswagen.
    * @return String Url that will be used for this user.
    */
    @AuraEnabled
    public static String fetchit()
    {
        string urlValue ='';
        string urler = URL.getSalesforceBaseUrl().toExternalForm();
        
        User ObjUser =[SELECT Id,Contact.VGA_Brand__c,
                              Contact.Account.VGA_Sub_Brand__c,VGA_Brand__c
                       FROM User 
                       WHERE Id=:UserInfo.getUserId() 
                       LIMIT 1];
        
        if(ObjUser.VGA_Brand__c =='volkswagen')
        {
            urlValue =urler +'/volkswagen/s/task';
        }
        else
        {
            urlValue =urler +'/skoda/s/task';
        }
         
        return (urlValue);
    }
    
}