public class VGA_LeadTriggerHelper
{   
    /**
    * @author Lino Diaz Alonso
    * @date 27/11/2018
    * @description This Method is used to return the map of users group by
    *              its Id. The users Ids will be the owners of the leads 
    *              that started the process.
    * @param lstLead List of leads that started the process
    * @return Map of users group by Id
    */
    public static Map<Id,User> getUsersPerOwnerId(List<Lead> leadsList) {
        
        Set<Id> leadOwnersIdSet = new Set<Id>();
        Map<Id,User> usersPerOwnerIdMap = new Map<Id,User>();

        for(Lead newLead : leadsList){
            leadOwnersIdSet.add(newLead.OwnerId);
        }

        return new Map<Id, User>([SELECT Id, ContactId 
                                  FROM User 
                                  WHERE Id IN: leadOwnersIdSet]);
    }

    /**
    * @date 10/10/2018
    * @description This Method is used to return leads that do not contain 
    *              Profanity Words
    * @param lstLead List of leads that started the process
    * @return List of leads that don't contain profanity words.
    */
    public static List<Lead> checkProfanity(List<Lead> lstLead)
    {
        List<Lead> lstFilteredLeads = new List<Lead>();
        ID vwRTId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        ID skRTId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
        
        //Get profanity queue record that will be the owner of the new leads with profanity words.
        Group  profanityQueue = [SELECT Id, Name 
                                 FROM Group 
                                 WHERE  Type = 'Queue' 
                                 AND Name = : Label.VGA_Profanity_Queue];
        
        if(lstLead != null && !lstLead.isEmpty())
        {
            Boolean isprofinity;
            Map<String,VGA_Profanity__c> mapProfanity = new Map<String,VGA_Profanity__c>();
            
            //Build map with all the profanity records group by the profanity word
            for(VGA_Profanity__c objProfanity : [SELECT Id, VGA_Keyword__c,VGA_Profanity_Exact_Match__c 
                                                 FROM VGA_Profanity__c 
                                                 WHERE VGA_Is_Active__c = true 
                                                 AND VGA_Keyword__c != NULL])
            {
                mapProfanity.put(objProfanity.VGA_Keyword__c.trim().tolowercase(), objProfanity);
            }
            
            for(Lead objLead : lstLead)
            {
                if(objLead.VGA_Brand__c != Null)
                {
                    if(objLead.VGA_Brand__c == 'Volkswagen' && objLead.recordtypeid == null)
                    {
                        objLead.RecordTypeId = vwRTId;
                    }
                    else if(objLead.VGA_Brand__c == 'Skoda'  && objLead.recordtypeid == null)
                    {
                        objLead.RecordTypeId = skRTId;
                    }
                }
                
                if(objLead.LeadSource != Null && objLead.LeadSource == 'PV Website')
                {
                    objLead.LeadSource = 'Volkswagen Website';
                }
                
                if(mapProfanity != null && !mapProfanity.isEmpty())
                {
                    isprofinity = false;
                    
                    for(string eachProfanityKeyWord : mapProfanity.keySet())
                    {
                        // If the profanity keyword requires an exact match
                        if( mapProfanity.containsKey(eachProfanityKeyWord) && mapProfanity.get(eachProfanityKeyWord).VGA_Profanity_Exact_Match__c)
                        {
                            //We check that the first name, last name or the email is exactly equal to the profanity keyword
                            if((objLead.firstname != null && objLead.firstname.trim().tolowercase() == eachProfanityKeyWord)
                                || (objLead.lastname != null && objLead.lastname.trim().tolowercase() == eachProfanityKeyWord)
                                || (objLead.email != null && objLead.Email.split('@').get(0) == eachProfanityKeyWord)){
                                objLead.VGA_Profanity__c = true;
                                objLead.Status = 'Closed';
                                objLead.ownerID = profanityQueue.Id;
                                objLead.VGA_Profanity_Record__c = mapProfanity.get(eachProfanityKeyWord).Id;
                                isprofinity = true;
                                break;
                            }
                        //If the profanity word doesn't require an exact match we can just check if 
                        //the lead first name/last name or email contains the profanity word.
                        }else if((objLead.firstname != null && objLead.firstname.trim().tolowercase().contains(eachProfanityKeyWord))
                                || (objLead.lastname != null && objLead.lastname.trim().tolowercase().contains(eachProfanityKeyWord))
                                || (objLead.email != null && objLead.email.trim().tolowercase().contains(eachProfanityKeyWord)))
                        {
                            objLead.VGA_Profanity__c = true;
                            objLead.Status = 'Closed';
                            objLead.ownerID = profanityQueue.Id;
                            objLead.VGA_Profanity_Record__c = mapProfanity.get(eachProfanityKeyWord).Id;
                            isprofinity = true;
                            break;
                        }
                    }
                    
                    if(!isprofinity)
                    {
                        lstFilteredLeads.add(objLead);
                    }
                }
                else
                {
                    lstFilteredLeads.add(objLead);
                }
            }
        }
        
        return lstFilteredLeads;
    }


    /**
    * @author Ganesh M
    * @date 06/02/2019
    * @description This Method is used to return the true or false. 
    *              Will check which Leads should be assigned to Dealership Queue.
    *              only individuals,Testdrive/Dealer call back and status New Leads should be 
    *              assigned to Dealership queue, since Followup_lead__c = true is checking 
    *              in checkRequestType method we are not checking here.
    * @#881
    * @param Lead that we are processing
    * @return Boolean it will be true if we need to assign the dealer to a queue and false if we dont
    */
    public static Boolean validateIfLeadNeedsToBeAssignedToQueue(Lead newLead, ID vwRTId, ID skRTId) {
        
        if(newLead.VGA_Brand__c != null 
            && newLead.VGA_Sub_Brand__c != null 
            && (newLead.recordtypeid == vwRTId || newLead.recordtypeid == skRTId) 
            && newLead.status== 'New' 
            && (newLead.VGA_Request_Type__c == 'Callback Request' 
                || newLead.VGA_Request_Type__c == 'Test Drive'
                ||  (newLead.VGA_Request_Type__c == 'Fleet Lead' && newLead.VGA_Volkswagen_Fleet_Size__c=='0-29'))
            && newLead.VGA_Tracking_ID__c != '790' )
        {  
            return true;
        }

        return false;    
    } 
    
    /**
      * @author MOHAMMED ISHAQUE SHAIKH
      * @date 27/11/2018
      * @description This Method is used to calculate Datetime  value  
      *              from User5__C and User6__C fields for Lead record
      * @param obj -type lead  
      * @param strTimeZone -type String  It contains Dealer timezone.
      */
    public static void validateStartDateTime(Lead obj,String strTimeZone){
        try{
            System.debug('CHEKC ONE');
            Boolean DateCheck=false;
            List<String> TimeSplit_meridiem=obj.VGA_User_6__c.split(' ');
            List<String> Timesplit;
            if(TimeSplit_meridiem.size()==2 && TimeSplit_meridiem!=null){
                Timesplit=TimeSplit_meridiem[0].split(':');
                if(Timesplit.size()==2){
                    if(TimeSplit_meridiem[1]=='AM' || TimeSplit_meridiem[1]=='PM'){
                        if(Integer.valueOf(Timesplit[0])>=1 && Integer.valueOf(Timesplit[0])<=12){
                            if(TimeSplit_meridiem[1]=='PM'){  
                                if(Integer.valueOf(Timesplit[0])==12){
                                    Timesplit[0]='12';
                                }else{
                                    Timesplit[0]=''+(12+Integer.valueOf(Timesplit[0])); 
                                } 
                            }else{
                               if(Integer.valueOf(Timesplit[0])==12){
                                    Timesplit[0]='00';
                               } 
                            }
                        }else{
                            DateCheck=true;
                        }
                       
                    }else{
                        DateCheck=true;
                    }
                }else{
                    DateCheck=true;
                }
            }else if(TimeSplit_meridiem.size()==1){
               Timesplit=TimeSplit_meridiem[0].split(':');
               if(Timesplit.size()==2){
                   if(Integer.valueOf(Timesplit[0])>=0 && Integer.valueOf(Timesplit[1])>=0 && Integer.valueOf(Timesplit[0])<=23 && Integer.valueOf(Timesplit[1])<=59 ){
                       
                   }else{
                       DateCheck=true;
                   }
               }else{
                   DateCheck=true;
               }
            }

            if(!DateCheck){
               
               List<String> DateArr=obj.VGA_User_5__c.split('/');
               Datetime temp=Datetime.newInstance(Integer.Valueof(DateArr[0]),Integer.Valueof(DateArr[1]),Integer.Valueof(DateArr[2]),Integer.Valueof(Timesplit[0]),Integer.Valueof(Timesplit[1]),00);
               
               Timezone DealerTimezone=Timezone.getTimeZone(strTimeZone);
               Timezone UserTimeZone=UserInfo.getTimeZone();
               
               Long DateTimeDiffernce=UserTimeZone.getOffset(temp)-DealerTimezone.getOffset(temp);
               
               obj.VGA_Test_Drive_Start_Date__c=Datetime.newInstance(temp.getTime()+DateTimeDiffernce); 
                System.debug(obj.VGA_Test_Drive_Start_Date__c);
           
            }else{
                obj.addError('please specify Test Drive date correctly!');
            }
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            obj.addError('please specify Test Drive date correctly!');
        }
    }

    /**
    * @author Lino Diaz Alonso
    * @date 10/04/2019
    * @description Method being used to populate the duration fields after the Lead
    *              Accepted date is updated or clear the values after the lead is
    *              reassigned.
    * @param triggerNew List of new Leads that we are processing.
    * @param mapOld Map with the list of leads with the old values.
    */
    public static void populateDurationFields(List<Lead> triggerNew, Map<Id,Lead> mapOld){

        List<Lead> leadsList = new List<Lead>();

        for(Lead newLead : triggerNew){
            Lead oldLead = mapOld.get(newLead.Id);

            if((newLead.VGA_Status__c == null ||
                newLead.VGA_Status__c == '')
               && oldLead.VGA_Status__c == 'Accepted'){
                  //If the lead has been reassigned then all the duration fields will be cleared
                  newLead.Status = 'New';
                  
                  newLead.VGA_Lead_Accepted_Date__c = null; 
                  newLead.VGA_Duration_Hours1__c   = null;
                  newLead.VGA_Duration_Minutes1__c = null; 
            }else if(oldLead.VGA_Lead_Accepted_Date__c != null
               && newLead.VGA_Lead_Accepted_Date__c == null){
                newLead.VGA_Duration_Hours1__c   = null;
                newLead.VGA_Duration_Minutes1__c = null;          
            } else if(newLead.VGA_Lead_Accepted_Date__c != null &&
                      newLead.VGA_Lead_Accepted_Date__c != oldLead.VGA_Lead_Accepted_Date__c){
                newLead.VGA_First_Accepted_Date__c = newLead.VGA_First_Accepted_Date__c == null ? newLead.VGA_Lead_Accepted_Date__c : newLead.VGA_First_Accepted_Date__c;
                leadsList.add(newLead);
            } 
        }

        if(leadsList.size() !=0){
            VGA_CalculateLeadDurationHours.updateLeadDurationField(leadsList);
        }
    }
}