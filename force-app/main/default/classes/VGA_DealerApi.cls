@RestResource(urlMapping='/getDealerDetails/*')
global with sharing class VGA_DealerApi {
 
    @HttpGet
    global static Void  GetDealerDetails() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        list<VGA_Trading_Hour__c> trandinghours=new list<VGA_Trading_Hour__c>();
        list<VGA_Public_Holiday__c> publicholiday=new list<VGA_Public_Holiday__c>();
        list<VGA_Dealership_Profile__c> dealerprofile=new list<VGA_Dealership_Profile__c>();
        String accountId='';
          accountId  = req.params.get('dealercode');
        string subbrand= '';
           subbrand= req.params.get('subbrand');
        if(subbrand=='PV')
        {
            subbrand='Passenger Vehicles (PV)';
        }
        else
        {
            subbrand='Commercial Vehicles (CV)';
        }
        list<Account> result = [SELECT Id, Name,VGA_Dealer_Code__c,BillingAddress,BillingCity,Billingstreet,Billingstate,ShippingAddress,VGA_Map_Location__c FROM Account WHERE VGA_Dealer_Code__c = :accountId AND (recordtype.developerName='Volkswagen_Dealer_Account' or recordtype.developerName='Skoda_Dealer_Account') limit 1];
        
        if(result.size()>0 && accountId!=null && subbrand!=null)
        {
         dealerprofile=[select id from VGA_Dealership_Profile__c where VGA_Dealership__c=:result[0].id and VGA_Sub_Brand__c=:subbrand limit 1];
         publicholiday=[select VGA_Holiday_Date__c from VGA_Public_Holiday__c where VGA_State_Region__c=:result[0].Billingstate or VGA_State_Region__c='All'];
        }
        if(dealerprofile.size()>0)
        {
            trandinghours=[select VGA_Day__c,VGA_Start_Time__c,VGA_End_Time__c,VGA_Working__c from VGA_Trading_Hour__c where VGA_Dealership_Profile__c=:dealerprofile[0].id];
        
        }
        system.debug('dealercodeeee'+accountid+'  subbrand '+subbrand);
         JSONGenerator gen = JSON.createGenerator(true);
       if(!result.isEmpty() && accountid!=null && accountid!='' && subbrand!=null && subbrand!='' && dealerprofile.size()>0)
        {
            //instantiate the generator
            
            gen.writeStartObject();
            gen.writeStringField('dealerName', result[0].Name);
            gen.writeStringField('dealerCode', result[0].VGA_Dealer_Code__c);
            gen.writeStringField('dealerAddress', result[0].BillingStreet+', '+result[0].BillingCity);
            gen.writeFieldName('tradingHours');
            if(!trandinghours.isEmpty())
              {
                 gen.writeStartArray();
                for(VGA_Trading_Hour__c th: trandinghours)
                {
                    gen.writeStartObject();
                    gen.writeStringField('name',th.VGA_Day__c);
                    if(th.VGA_Working__c==true)
                    gen.writeBooleanField('isclosed', false);
                    else
                     gen.writeBooleanField('isclosed', true);   
                    
                    gen.writeStringField('startTime', th.VGA_Start_Time__c);
                    gen.writeStringField('endTime', th.VGA_End_Time__c);
                    gen.writeEndObject();
                 }
                gen.writeEndArray();
              }  
             else
             {
                 gen.writeStartObject();
                 gen.writeEndObject();
                 
             }
           gen.writeFieldName('publicHolidays');
        gen.writeStartArray();
        for(VGA_Public_Holiday__c ph: publicholiday)
        {
           gen.writeDate(ph.VGA_Holiday_Date__c);
        }
        gen.writeEndArray();
        if(result[0].VGA_Map_Location__c!=null)
        {
        gen.writeStringField('mapLocation', result[0].VGA_Map_Location__c);
        }
        else
        {
         gen.writeStringField('mapLocation', '');
        }
        gen.writeEndObject();
        
       
        }
       else
       {
           gen.writeStartObject();
           gen.writeEndObject();
       }
        
     res.responseBody = Blob.valueOf(gen.getAsString());
  
    }
}