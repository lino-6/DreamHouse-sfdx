global class VGA_scheduleAccountSubscription implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        String batchSize= Label.VGA_Batch_Size;
        VGA_updateAccountSubscriptions b = new VGA_updateAccountSubscriptions(); 
        database.executebatch(b,Integer.valueof(batchSize));
    }
}