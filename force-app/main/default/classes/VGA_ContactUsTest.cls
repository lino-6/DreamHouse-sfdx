@isTest
private class VGA_ContactUsTest 
{
    
    static contactUsController controller;    
    static Attachment attach;
    
    /**
    * @author Archana Yerramilli
    * @date  29/01/2019
    * @description This method contains the test data for form entry/submission. This is reused in every test method. 
    */
    private static void init() 
    {
        list<VGA_MuleUrl__c> customsettingdata =   VGA_CommonTracker.buildCustomSettingforMule();
        insert customsettingdata;
        
        controller = new contactUsController();                 
        controller.selectedTitleType = 'Ms';
        controller.rego = '12312';
        controller.firstName = 'First1Name';
        controller.lastName = 'Last2Name';
        controller.email = 'archana.yerramilli@katzion.com';
        controller.mobile = '0424473730';
        controller.homeNumber = '04012123456';
        controller.selectedContactMeViaList = 'Email';
        controller.postalAddress = 'Level 5 37 Bligh St';
        controller.postCode = '2000';
        controller.suburb = 'SYDNEY';
        controller.State = 'NSW';
        controller.receiveEmail = true;
        controller.iConsent = 'true';
        controller.abn = '123142526557';     
        controller.licenseNumber = '12345';
        controller.licenseState = 'QLD';
        controller.dateOfBirth = '05-05-2017';
        controller.kilometer = '1000';
        controller.dateOfSale ='05-05-2017';
        controller.companyName = 'ContactUsTestCompany';
        controller.dateOfChange = '10-05-2017';
        controller.selectedSkodaModelList = 'Octavia';
        controller.homeNumber = '0424473700';
        controller.workNumber = '0424473700';
        controller.phoneNumber = '0424473700';
        controller.mobile = '0424473700';
        controller.message = 'message';
        attach = new Attachment(); 
        attach.Name='Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Test Attachment Body'); 
        attach.body=bodyBlob;         
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);   
    }
    
    /**
    * @author Archana Yerramilli
    * @date  29/01/2019
    * @description The below test method is created to test Ownership Amendment - Sold Vehicle for Skoda.
    */
    @isTest static void Test01_ContactUsForSkoda_whenOwnershipAmendmentIsVehicleSold_thenNewOwnerFieldsAreUpdated()
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.skoda_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.brand = 'Skoda';
        controller.selectedAmendmentType = 'Sold Vehicle';     
        controller.DateSold = '09-01-2019';
        controller.NewOwnerFirstName = 'Test Method First Name';
        controller.NewOwnerLastName  = 'New Owner Last Name';
        controller.NewOwnerMobile    = '0469382076';
        controller.NewOwnerEmail     = 'testmethod@gmail.com';
        controller.selectedBrandType = 'Skoda';
        controller.attachment_ppsr = attach;              
        controller.submit();
        Test.stopTest();
        
        List<Case> objUpdatedCase = [SELECT ID,
                                            VGA_New_Owner_First_Name__c,
                                            VGA_New_Owner_Last_Name__c,
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Web_Form_VIN__c,
                                            VGA_Web_Form_State__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Web_Form_Postal_Address__c,
                                            VGA_Web_Form_Suburb__c
                                            from Case where VGA_New_Owner_Email__c =: 'testmethod@gmail.com'];
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        //System.AssertEquals(controller.NewOwnerFirstName,   objUpdatedCase[0].VGA_New_Owner_First_Name__c );
   }

    /**
    * @author Archana Yerramilli
    * @date  29/01/2019
    * @description The below test method is created to test Ownership Amendment - Sold Vehicle for VW. 
    */    
    @isTest static void Test02_ContactUsForVW_whenOwnershipAmendmentIsVehicleSold_thenNewOwnerFieldsAreUpdated()
    {
        init();
        Test.startTest();
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);         
       
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);     
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Sold Vehicle';      
        controller.NewOwnerFirstName = 'PV Test Method New Owner First Name';
        controller.NewOwnerLastName  = 'PV Test Method New Owner Last Name';
        controller.NewOwnerMobile    = '0469382076';
        controller.NewOwnerEmail     = 'testmethod@gmail.com';
        controller.DateSold = '09-01-2018';                     
        controller.attachment_ppsr = attach;              
        controller.submit();
        Test.stopTest();
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
   
   }
    
    /**
    * @author Archana Yerramilli
    * @date  29/01/2019
    * @description The below test method is to test the Ownership amendment type vehicle written off. 
    * @description This should create a new case with Ownership Amendment Purpose as "Vehicle written off and completely destroyed"
    */
    @isTest static void Test03_ContactUsForVW_whenOwnershipAmendmentIsVehicleWrittenOff_thenCreateCase() {
        init();
        Test.startTest(); 
 
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);            
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Vehicle written off and completely destroyed';
        controller.selectedBrandType = 'volkswagen';
        controller.dateWrittenOff = '05-05-2017';         
        controller.attachment_ppsr = attach;  
        controller.rego = '15185';
        controller.lastName = 'Written Off Test';
        
        controller.submit();
        Test.stopTest();
        
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());        
        List<Case> objUpdatedCase03 = [SELECT ID,
                                            VGA_Date_Written_Off__c,
                                            VGA_New_Owner_Last_Name__c,
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Web_Form_VIN__c,
                                            VGA_Web_Form_State__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Web_Form_Postal_Address__c,
                                            VGA_Web_Form_Suburb__c
                                            from Case where VGA_Web_Form_REGO__c =: '15185'];
          System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());    
          System.AssertEquals(controller.rego , objUpdatedCase03[0].VGA_Web_Form_REGO__c); 
         
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method will test the new owner amendment when vehicle is Purchase Used Vehicle for Skoda, it should save the fields and create an interaction.  
    */
    @isTest static void Test04_ContactUsForSkoda_whenOwnershipAmendmentIsPurchaseUsedVehicle_thenCreateCase() 
    {
        init();
        Test.startTest(); 
        PageReference pageRef = Page.Skoda_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Purchased used vehicle';
          controller.vin = 'vwvin';
        controller.selectedBrandType = 'skoda';
        controller.submit();
                
        Test.stopTest();
        List<Case> objPurchaseVehicleCase = [SELECT ID,                                         
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Date_of_Sale__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Drivers_License_Number__c,
                                            VGA_Drivers_Licence_State__c,VGA_Licence_State__c
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertEquals(controller.licenseState,   objPurchaseVehicleCase[0].VGA_Licence_State__c );
       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method is created to test the new owner amendment when vehicle is Purchase Used Vehicle for VW, it should save the fields and create an interaction.  
    */
    @isTest static void Test05_ContactUsForVW_whenOwnershipAmendmentIsPurchaseUsedVehicle_thenCreateCase() 
    {
        init();
        Test.startTest(); 
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Purchased used vehicle';
        controller.vin = 'vwvin';
        controller.selectedBrandType = 'volkswagen';
        controller.submit();
                
        Test.stopTest();
        List<Case> objPurchaseVWVehicleCase = [SELECT ID,                                           
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Date_of_Sale__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Drivers_License_Number__c,
                                            VGA_Drivers_Licence_State__c,VGA_Licence_State__c
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertEquals(controller.licenseState,   objPurchaseVWVehicleCase[0].VGA_Licence_State__c );
       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method is created to test the request type is General Enquiry for Skoda. This should create a case and submit the form, redirecting to thank you page.
    */
    @isTest static void Test06_ContactUsForSkoda_whenRequestTypeIsGeneralEnquiry_thenCreateCase() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Skoda_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'General Enquiry';
        controller.vin = 'vwwvin';
        controller.description = 'Skoda test for General Enquiry Request Type';
        controller.submit();
        String nextPage = controller.submit().getUrl(); 
        Test.stopTest();
        List<Case> objGenEnquiryCase = [SELECT ID,VGA_Description__c,VGA_Brand__c from Case where VGA_Web_Form_VIN__c =: 'vwwvin'];
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertEquals(controller.description,objGenEnquiryCase[0].VGA_Description__c );
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method is created to test the request type is General Enquiry for VW.  
    * @description This should submit the form details and create an interaction along with attachments.
    */    
    @isTest static void Test07_ContactUsForVW_whenRequestTypeIsGeneralEnquiry_thenCreateCaseWithAttachments() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'General Enquiry';
        controller.vin = 'vwwvin';
        controller.description = 'volkswagen_Contact_Us test for General Enquiry Request Type';
        controller.attachment_insurance = attach;        
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;         
        controller.attachment_ppsr = attach1;
        controller.submit();
        Test.stopTest();
        List<Case> objGenEnquiryCase = [SELECT ID,VGA_Description__c,VGA_Brand__c from Case where VGA_Web_Form_VIN__c =: 'vwwvin'];
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertEquals(controller.description,objGenEnquiryCase[0].VGA_Description__c );
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method will test the request type is Ownership Amendment and Amendment Type "Vehicle reported stolen and not recovered"  for VW.  
    * @description This should submit the form details and create an interaction.
    */
    @isTest static void Test08_ContactUsForVW_whenRequestTypeIsOwnershipAmendmentVehicleReportedStolen_thenCreateCase() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Vehicle reported stolen and not recovered';
        controller.selectedBrandType = 'volkswagen';
        controller.vin = 'vwvin';
        controller.rego = 'Test';
        controller.datestolen = '05-05-2017';
        controller.attachment_insurance = attach;        
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;         
        controller.attachment_ppsr = attach1;  
        controller.submit();
        Test.stopTest();
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        Case objVehicleStolenCase = [SELECT ID,Ownership_Amendment_Purpose__c from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        System.assertEquals(controller.selectedAmendmentType, objVehicleStolenCase.Ownership_Amendment_Purpose__c);
       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  30/01/2019
    * @description The below method will test the request type is General Enquiry for with theadId for sendEmail check.  
    */
    @isTest static void Test09_ContactUs_whenCaseIsCreated_thenSendEmail() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        
        controller.selectedRequestType = 'General Enquiry';
        controller.vin = 'vinVW';
        controller.description = 'Skoda test for General Enquiry Request Type';
        controller.submit();
        Case objCase = [SELECT VGA_Registration_No__c, Status, VGA_Web_Case_Origin__c, VGA_Web_Form_Email__c, VGA_Web_Form_Mobile__c, VGA_Web_Form_Postal_Address__c,VGA_Web_Form_Postcode__c,
                              VGA_Web_Form_REGO__c,VGA_Web_Form_State__c, VGA_Web_Form_Suburb__c, VGA_Web_Form_VIN__c from Case where VGA_Web_Form_VIN__c =: 'vinVW'];
        controller.sendEmail(objCase,'vgasupport@katzion.com', 'Volkswagen');
        string threadId = '';
        threadId = controller.getThreadId(objCase.Id);
        Test.stopTest();
        List<Case> objCaseCreated = [SELECT VGA_Registration_No__c, Status, VGA_Web_Case_Origin__c, VGA_Web_Form_Email__c, VGA_Web_Form_Mobile__c, VGA_Web_Form_Postal_Address__c,VGA_Web_Form_Postcode__c,
                              VGA_Web_Form_REGO__c,VGA_Web_Form_State__c, VGA_Web_Form_Suburb__c, VGA_Web_Form_VIN__c, Ownership_Amendment_Purpose__c from Case where VGA_Web_Form_VIN__c =: 'vinVW'];
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertNotEquals(controller.selectedRequestType,objCaseCreated[0].Ownership_Amendment_Purpose__c);
        
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Ownership Amendment and Amendment Type is Purchased Used Vehicle for Skoda. 
    * Assert to enure a case is created by checking Case ID is not Null. 
    */
    @isTest static void Test10_ContactUsForVW_whenAmendmentTypeIsPurchaseUsedVehicle_thenCreateCaseWithAttachments() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        
        controller.selectedRequestType = 'Ownership Amendment';
        controller.vin = 'vwvin';
        controller.description = 'Purchased Used Vehicle';
        controller.selectedBrandType = 'volkswagen';    
        controller.attachment_insurance = attach;
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;  
        controller.attachment_ppsr = attach1;
        controller.submit();        
       
        Test.stopTest();
        List<Case> lstPurchaseVehicleCase = [SELECT ID,                                         
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Date_of_Sale__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Drivers_License_Number__c,
                                            VGA_Drivers_Licence_State__c,VGA_Licence_State__c
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertNotEquals(null,lstPurchaseVehicleCase[0].ID );
        
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Ownership Amendment and Amendment Type is Vehicle exported overseas for VW. 
    * Assert to enure a case is created with Amendment Type Vehicle exported overseas
    */
    @isTest static void Test11_ContactUsForVW_whenAmendmentTypeIsVehicleExportedOverseas_thenCreateCaseWithAttachments() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);        
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Vehicle exported overseas';
        controller.selectedBrandType = 'volkswagen';    
        controller.attachment_ppsr = attach;           
        controller.dateExported ='05-05-2017';  
        String nextPage = controller.submit().getUrl();         
        controller.submit();
        Test.stopTest();

        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());   
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Ownership Amendment and Amendment Type is Change Address. 
    * Assert to ensure a case is created with Amendment Type address change
    */
    @isTest static void Test12_ContactUsForSkoda_whenAmendmentTypeIsChangeAddress_thenCreateCaseWithAttachments()
    {
        init();
        Test.startTest();           
        PageReference pageRef = Page.skoda_Contact_Us;
        Test.setCurrentPage(pageRef); 
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Change Address / Contact Details / Registration Details';
        controller.brand = 'Skoda';
        controller.vin = 'vwvin';
        controller.rego = 'Test123';
        controller.dateOfChange ='05-05-2017';
        controller.selectedTitleType = 'Mrs';
        controller.firstName = 'ChangeAddressFirstName';
        controller.lastName = 'ChangeAddressLastName';
        controller.companyName = 'ChangeAddressTest';
        controller.postalAddress = 'abc';
        controller.suburb = 'def';
        controller.state = 'QLD';
        controller.postCode = '2222';
        controller.mobile ='';    
        Attachment attach = new Attachment(); 
        attach.Name='Unit Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob;         
        controller.attachment_insurance = attach;        
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;         
        controller.attachment_ppsr = attach1;
        controller.submit();
        Test.stopTest();
        
        List<Case> lstChangeAddCase = [SELECT ID, VGA_Date_of_Change__c, VGA_Web_Form_Email__c, VGA_Web_Form_Mobile__c                                        
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertNotEquals(null,lstChangeAddCase[0].VGA_Date_of_Change__c );       
    }
        
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test date formatting. 
    */
    @isTest static void Test13_ContactUs_FixDate_ConvertFromStringToDateTime()
    {
        Test.startTest();
        init();
        PageReference pageRef = Page.skoda_Contact_Us;
        Test.setCurrentPage(pageRef); 
        string incorrectDate = '15-01-2019';        
        List<Attachment> attach = new List<Attachment>();
        controller.fixDateFormat(incorrectDate);     
        Test.stopTest();
        System.AssertNotEquals(incorrectDate,controller.fixDateFormat(incorrectDate));
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method should test case creation and form submission when amendment type is Change address for VW
    */
    @isTest static void Test14_ContactUsForVW_whenAmendmentTypeIsChangeAddress_thenCreateCaseWithAttachments()
    {
        init();
        Test.startTest();           
        PageReference pageRef = Page.volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);      
        controller.attachment_insurance = attach;        
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;         
        controller.attachment_ppsr = attach1;
        controller.vin = 'winvin';
        controller.submit();        
        String nextPage = controller.submit().getUrl();            
        Test.stopTest();
        List<Case> lstChangeAddCase = [SELECT ID, VGA_Date_of_Change__c, VGA_Web_Form_Email__c, VGA_Web_Form_Mobile__c                                        
                                            from Case where VGA_Web_Form_VIN__c =: 'winvin'];
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        System.AssertEquals(controller.mobile,lstChangeAddCase[0].VGA_Web_Form_Mobile__c );       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Enquiry for VW. This should create an interaction and submit the data.   
    */
    @isTest static void Test15_ContactUsForVW_whenRequestTypeIsGeneralEnquiry_thenCreateCase() 
    {
       
        Test.startTest(); 
        init();
        PageReference pageRef = Page.volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);   
        controller.selectedRequestType  = 'General Enquiry';
        controller.selectedBrandType    = 'volkswagen';
        controller.selectedEnquiryAbout = 'Parts';
        controller.receiveEmail = false;
        controller.vwOwner = true;
        controller.selectedVWModelList = 'Amarok';
        controller.vin = 'winvin';
        controller.submit();
        List<Case> lstGenEnquiryCase = [SELECT ID, Vehicle_Model_General__c,VGA_Model_Year__c,VGA_Product__c, VGA_Web_Form_Email__c, VGA_Web_Form_Mobile__c,
                                              VGA_Are_you_a_Volkswagen_Owner__c ,VGA_Enquiry_Type__c                                       
                                              from Case where VGA_Web_Form_VIN__c =: 'winvin'];
        Test.stopTest();       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Enquiry for VW.  
    */
    @isTest static void Test16_ContactUsForSkoda_submitAndGetEnquiryModelTitleBrandStateOwnershipPurpose()
    {
        init();
        Test.startTest();
        controller.brand = 'Skoda';
        controller.validateVIN();        
        controller.getVWModel();
        controller.getContactMeVia();
        controller.getEnquiryAbout();
        controller.getSkodaModel();
        controller.getTitleType();
        controller.getStateList();
        controller.getBrandType();
        controller.getOwnershipAmendmentPurpose();
        controller.getRequestType();
        Test.StopTest(); 

    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Enquiry for VW PV.  
    */
    @isTest static void Test17_ContactUsVWPV_submitAndGetEnquiryModelTitleBrandStateOwnershipPurpose()
    {
        init();
        Test.startTest();
        controller.brand = 'PV Volkswagen';
        controller.vin = 'vwvin';
        controller.validateVIN();        
        controller.getVWModel();
        controller.getContactMeVia();
        controller.getEnquiryAbout();
        controller.getSkodaModel();
        controller.getTitleType();
        controller.getStateList();
        controller.getBrandType();
        controller.getOwnershipAmendmentPurpose();
        controller.getRequestType();
        Test.StopTest();       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is Enquiry for CV Volkswagen.  
    */
    @isTest static void Test18_ContactUsVWCV_submitAndGetEnquiryModelTitleBrandStateOwnershipPurpose()
    {
        init();
        Test.startTest();
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);  
        controller.brand = 'CV Volkswagen';
        controller.vin = 'vwvin';
        controller.validateVIN();        
        controller.getVWModel();
        controller.getContactMeVia();
        controller.getEnquiryAbout();
        controller.getSkodaModel();
        controller.getTitleType();
        controller.getStateList();
        controller.getBrandType();
        controller.getOwnershipAmendmentPurpose();
        controller.getRequestType();
        Test.StopTest();       
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type Enquiry for CV Volkswagen. This should create an interaction, submit the form and redirect to thankyou page. 
    */
    @isTest static void Test19_ContactUsForVW_whenRequestTypeIsVehicleExported_thenCreateCase() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.Volkswagen_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Vehicle exported overseas';
        controller.selectedBrandType = 'volkswagen';
        controller.vin = 'vwvin';
        controller.rego = 'Test';
        controller.dateExported = '05-05-2017';
        controller.attachment_insurance = attach;        
        Attachment attach1 = new Attachment(); 
        attach1.Name='Unit Test Attachment'; 
        Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
        attach1.body=bodyBlob1;         
        controller.attachment_ppsr = attach1;  
        controller.submit();
        String nextPage = controller.submit().getUrl();
        Test.stopTest();
        PageReference expectedPage  = new PageReference('/apex/volkswagen_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        List<Case> objVehicleExporCase = [SELECT ID, Ownership_Amendment_Purpose__c from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        System.assertEquals(controller.selectedAmendmentType, objVehicleExporCase[0].Ownership_Amendment_Purpose__c);
    }
    
    /**
    * @author Archana Yerramilli
    * @date  31/01/2019
    * @description The below method will test the request type is VehicleExported for Skoda. This should create an interaction, submit the form and redirect to thankyou page. 
    */
    @isTest static void Test20_ContactUsForSkoda_whenRequestTypeIsVehicleExported_thenCreateCase() 
    {
        init();
        Test.startTest();
        PageReference pageRef = Page.skoda_contact_us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Vehicle exported overseas';
        controller.selectedBrandType = 'skoda';
        controller.vin = 'vwvin';
        controller.rego = 'Test';
        controller.dateExported = '05-05-2017';
        controller.attachment_insurance = attach;    
        controller.submit();
        String nextPage = controller.submit().getUrl();      
        Test.stopTest();
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
        List<Case> objSkodaExportCase = [SELECT ID,Ownership_Amendment_Purpose__c from Case where VGA_Web_Form_VIN__c =: 'vwvin'];        
        System.assertEquals(controller.selectedAmendmentType, objSkodaExportCase[0].Ownership_Amendment_Purpose__c);
    }
    
    @isTest static void Test23_whenthereisException_thenSendEmailException() 
    {
            String subject  = 'Test Method';
            String htmlBody ='<b>Testing</b>'; 
            try
            {
                controller.dateExported = '@#non';
                Attachment attach1 = new Attachment(); 
                attach1.Name='Unit Test Attachment'; 
                Blob bodyBlob1 = Blob.valueOf('1 Unit Test Attachment Body'); 
                attach1.body=bodyBlob1;         
                controller.attachment_ppsr = attach1;      
            }
            catch(Exception ex)
            { 
                contactUsController.sendExceptionEmail(subject,ex.getMessage(),null);  
            }             
        
    }
    
    @isTest static void Test21_ContactUsForSkoda_whenVINDoesNotExist_thenCaseNotCreated() 
    {
        init();
        Test.startTest(); 
        PageReference pageRef = Page.Skoda_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Purchased used vehicle';
        controller.vin = 'ABCDVIN';
        controller.selectedBrandType = 'skoda';
        controller.workNumber = '';
        controller.submit();
                
        Test.stopTest();
        List<Case> objPurchaseVehicleCase = [SELECT ID,                                         
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Date_of_Sale__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Drivers_License_Number__c,
                                            VGA_Drivers_Licence_State__c,VGA_Licence_State__c
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
      
    }
    
    @isTest static void Test22_ContactUsForSkoda_whenCompAndABNDoesNotExist_thenfindAccount() 
    {
        init();
        Test.startTest(); 
        PageReference pageRef = Page.Skoda_Contact_Us;
        Test.setCurrentPage(pageRef);
        controller.selectedRequestType = 'Ownership Amendment';
        controller.selectedAmendmentType = 'Purchased used vehicle';
        controller.vin = 'ABCDVIN';
        controller.selectedBrandType = 'skoda';
        controller.brand = 'Skoda';
        controller.companyName = '';
        controller.abn = '';
        controller.submit();
                
        Test.stopTest();
        List<Case> objPurchaseVehicleCase = [SELECT ID,                                         
                                            Ownership_Amendment_Purpose__c,
                                            VGA_Brand__c,
                                            VGA_Date_of_Sale__c,
                                            VGA_Web_Form_REGO__c,
                                            VGA_Web_Form_Postcode__c,
                                            VGA_Drivers_License_Number__c,
                                            VGA_Drivers_Licence_State__c,VGA_Licence_State__c
                                            from Case where VGA_Web_Form_VIN__c =: 'vwvin'];
        
        PageReference expectedPage  = new PageReference('/apex/skoda_contact_us');
        System.AssertEquals(expectedPage.getUrl(),pageRef.getUrl());
      
    }
    

    
}