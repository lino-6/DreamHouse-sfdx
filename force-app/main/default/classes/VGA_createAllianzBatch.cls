global class VGA_createAllianzBatch implements Database.Batchable<sObject>
{
    list<VGA_Allianz_Data_Mapping__c> lstAllianzDataMapping = new list<VGA_Allianz_Data_Mapping__c>(); 
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strVehiclePolicyQuery = 'select id,VGA_Brand__c,VGA_Sub_Brand__c,VGA_Start_Date__c,VGA_End_Date__c,VGA_VIN__c,VGA_Is_Updated__c,Created_Today__c,VGA_Cancelled__c,VGA_VIN_No__c,VGA_Account_Record_Type_ID__c,Is_Extended_Warranty__c,VGA_Policy_Code__c,VGA_Component_Type__c,VGA_Component_Code__c from VGA_Vehicle_Policy__c';
        
        return Database.getQueryLocator(strVehiclePolicyQuery);
    }
    global void execute(Database.BatchableContext BC, List<VGA_Vehicle_Policy__c> lstVP)
    {
        if(lstVP != null && !lstVP.isEmpty())
        {
            try
            {
            Id SkoFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
            Id VWFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
            Id SkoInd_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
            Id VWInD_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
            
            for(VGA_Vehicle_Policy__c objVehiclePolicy : lstVP)
            {
                VGA_Allianz_Data_Mapping__c objADM = new VGA_Allianz_Data_Mapping__c();
                
                objADM.VGA_Brand__c =   objVehiclePolicy.VGA_Brand__c != null?objVehiclePolicy.VGA_Brand__c:null;
                objADM.VGA_Sub_Brand__c = objVehiclePolicy.VGA_Sub_Brand__c != null?objVehiclePolicy.VGA_Sub_Brand__c:null;
                
                if(objADM.VGA_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Skoda'))
                {
                    objADM.VGA_Client_Code__c = 'SKO';
                }
                else if(objADM.VGA_Sub_Brand__c != null && objADM.VGA_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Volkswagen') && objADM.VGA_Sub_Brand__c.equalsignorecase('Passenger Vehicles (PV)'))
                {
                    objADM.VGA_Client_Code__c = 'VWP';
                }
                else if(objADM.VGA_Brand__c != null && objADM.VGA_Sub_Brand__c != null && objADM.VGA_Brand__c.equalsignoreCase('Volkswagen') && objADM.VGA_Sub_Brand__c.equalsignorecase('Commercial Vehicles (CV)'))
                {
                    objADM.VGA_Client_Code__c = 'VWC';
                }
                
                objADM.VGA_Start_Date__c = objVehiclePolicy.VGA_Start_Date__c != null?objVehiclePolicy.VGA_Start_Date__c:null;
                objADM.VGA_End_Date__c = objVehiclePolicy.VGA_End_Date__c != null?objVehiclePolicy.VGA_End_Date__c:null;
                objADM.VGA_VIN__c = objVehiclePolicy.VGA_VIN__c !=null?objVehiclePolicy.VGA_VIN__c:null;
                objADM.VGA_Policy_Reference_Number__c =  objVehiclePolicy.VGA_VIN__c !=null?objVehiclePolicy.VGA_VIN__c:null;
                objADM.VGA_Manufacturer__c = objVehiclePolicy.VGA_Brand__c != null?objVehiclePolicy.VGA_Brand__c:null;
                if(objVehiclePolicy.VGA_Is_Updated__c == true)
                {
                    // objADM.VGA_Transaction_Code__c = 'C';
                }
                else if(objVehiclePolicy.Created_Today__c == true)
                {
                    objADM.VGA_Transaction_Code__c = 'A';
                }
                else if(objVehiclePolicy.VGA_Cancelled__c == true)
                {
                    objADM.VGA_Transaction_Code__c = 'D';       
                }
                objADM.VGA_Unique_Customer_Number__c = objVehiclePolicy.VGA_VIN_No__c != null?objVehiclePolicy.VGA_VIN_No__c:null;
                
                if(objVehiclePolicy.VGA_Account_Record_Type_ID__c != null && (objVehiclePolicy.VGA_Account_Record_Type_ID__c == SkoFleet_RTId || objVehiclePolicy.VGA_Account_Record_Type_ID__c == VWFleet_RTId))
                {
                    objADM.VGA_Company_Flag__c = 'Y';
                   // objADM.VGA_Address_Type__c = 'Work';
                }
                else if(objVehiclePolicy.VGA_Account_Record_Type_ID__c != null && (objVehiclePolicy.VGA_Account_Record_Type_ID__c == SkoInd_RTId ||objVehiclePolicy.VGA_Account_Record_Type_ID__c== VWInD_RTId ))
                {
                    objADM.VGA_Company_Flag__c = 'N';
                   // objADM.VGA_Address_Type__c = 'Work';
                }
                
                //Calculate product code 
                objADM.VGA_Product_Code__c = VGA_AllianzDataMappingHelper.getAllianzProductCode(objVehiclePolicy);

                objADM.VGA_Vehicle_Policy_ID__c = objVehiclePolicy.id;
                
                lstAllianzDataMapping.add(objADM);
            }
            if(lstAllianzDataMapping != null && !lstAllianzDataMapping.isEmpty())
            {
                insert lstAllianzDataMapping;
            }
        }
            catch(exception e)
            {
                system.debug('@@@@'+e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
}