@isTest(seeAllData=false)
public class VGA_BatchUpdateAllianzDataMappingTracker 
{
    public static VGA_Vehicle_Policy__c objVehicle;
    public static VGA_Ownership__c objOwnership;
    public static VGA_Allianz_Data_Mapping__c objADM;
    public static Asset objAsset;
    public static Account objAccount;
    
 	static testmethod void unittest1()
    {
        loadData();
        
        Test.startTest();
        	VGA_BatchUpdateAllianzDataMapping obj = new VGA_BatchUpdateAllianzDataMapping();
        	Database.executeBatch(obj);
        Test.stopTest();
    }
    static testmethod void unittest2Schedule()
    {
        VGA_scheduleBatchupdateADM sh1 = new VGA_scheduleBatchupdateADM();
        
        Test.startTest();
        	String sch = '0 0 23 * * ?'; 
        	System.schedule('Test Territory Check', sch, sh1);
        Test.stopTest(); 
    }
    public static void loadData()
    {
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'FW24';
        insert objVehicle; 
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        
        objAsset = new Asset();
       	objAsset.name = 'TEST';
    	objAsset.accountID = objAccount.id;
       
        
        insert objAsset;
        
        objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_VIN__c = objAsset.Id;
        objOwnership.VGA_Policy_Code__c = objVehicle.id;
        objOwnership.VGA_Type__c = 'Current Owner';
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership; 
        
        objADM = new VGA_Allianz_Data_Mapping__c();
        objADM.VGA_Vehicle_Policy_ID__c = objVehicle.id;
        objADM.VGA_VIN__c = objAsset.Id;
        insert objADM;

    }
}