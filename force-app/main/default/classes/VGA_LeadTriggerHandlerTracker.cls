/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
private class VGA_LeadTriggerHandlerTracker
{
  public static Lead objLead;
  public static VGA_Active_Brochure__c objActive;
  public static VGA_Post_Code_Mapping__c objPostCodeMapping;
  public static Lead objLead2;
  public static Account objAccount1;
  public static Account objAccount;
  public static VGA_Trading_Hour__c objTradingHour;
  public static VGA_Dealership_Profile__c objDealershipProfile;
  public static Contact objContact;
    public static User objUser;
    public static VGA_User_Profile__c objUserPro;
  public static VGA_Working_Hour__c objWorking;   
  public static VGA_Subscription__c objSubscription;
  public static VGA_Profanity__c objProfanity;
  public static Campaign objCampaign;
    
  static testmethod void myUnittest1()
  {
    LoadData();
    
   objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '20225';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;

        
        objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        
        objUser = VGA_CommonTracker.CreateUser(objContact.id);
    
    objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Nominated User';
        objDealershipProfile.VGA_Nominated_Contact__c = objContact.id;
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
    
    objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = objUserPro.id;
        objWorking.VGA_Working__c = true;
        objWorking.Name =  system.now().format('EEEE');
        insert objWorking; 
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'All New Leads';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
    
    objLead = new Lead();
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        objLead.VGA_Tracking_ID__c = '12345';
          objlead.VGA_Color_Code__c= '12345';
        objLead.VGA_Marketing_opt_in__c = true;
        insert objLead;  
        
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Duplicate_Customer_Id__c = objAccount1.id;
        insert objLead2; 
  }
  static testmethod void myUnittest1_RoundRobin()
  {
    LoadData();
    
    objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '20225';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;
        
        objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        
        objUser = VGA_CommonTracker.CreateUser(objContact.id);
    
    objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
    
    objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = objUserPro.id;
        objWorking.VGA_Working__c = true;
        objWorking.Name =  system.now().format('EEEE');
        insert objWorking; 
        
        objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
    
    objLead = new Lead();
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        insert objLead;  
        
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Duplicate_Customer_Id__c = objAccount1.id;
        insert objLead2; 
  }
  static testmethod void unittest4()
  {
    VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','All');   
         insert objPH;
    myUnittest1_RoundRobin();
  }
  static testmethod void unittest5()
  {
    VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','All');   
         insert objPH;
    myUnittest1_RoundRobin();
  }
  static testmethod void unittest6()
  {
    VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','ACT');    
         insert objPH;
    myUnittest1_RoundRobin();
  }
  static testmethod void unittest7()
  {
    VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','ACT');    
         insert objPH;
    myUnittest1_RoundRobin();
  }
  static testmethod void unittest8()
  {
    objAccount1 = new Account();
        objAccount1.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        objAccount1.lastname = 'Test';
        insert objAccount1;
    
    objLead = new Lead();
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        insert objLead;  
        
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Duplicate_Customer_Id__c = objAccount1.id;
        objLead2.VGA_Related_Lead__c = objLead.id;
        objLead2.VGA_Customer_Account__c = objAccount1.id;
        
        insert objLead2;   
        objLead2.VGA_Outcome__c ='Spam';
        update objLead2;
        
        list<Lead> lstLead = new list<Lead>();    
        lstLead.add(objLead2);
        
        
        
        VGA_LeadTriggerHandler.updateCustomerAccount(lstLead);
    
  }
  static testmethod void unittest9()
  {
     loadData();
     
    
     
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.Email ='rishi@test.com';
        objLead2.postalcode = '1234';
        objLead2.MobilePhone ='7053315180';
        insert objLead2;   
        objLead2.VGA_Outcome__c ='Spam';
        update objLead2;
    }
    
    static testmethod void unittestlead()
  {
     
    objAccount1 = new Account();
        objAccount1.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
        objAccount1.lastname = 'Test';
        insert objAccount1;
    
    objLead = new Lead();
        objLead.VGA_Brand__c = 'Skoda';
     //   objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        insert objLead;  
        
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Skoda';
     //   objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Duplicate_Customer_Id__c = objAccount1.id;
        objLead2.VGA_Related_Lead__c = objLead.id;
        objLead2.VGA_Customer_Account__c = objAccount1.id;
        
        insert objLead2;   
        objLead2.VGA_Outcome__c ='Spam';
        update objLead2;
        
        list<Lead> lstLead = new list<Lead>();    
        lstLead.add(objLead2);
        
        
        
        VGA_LeadTriggerHandler.updateCustomerAccount(lstLead);
        VGA_LeadTriggerHandler.updateRelatedLead(lstLead);
      
    
    }
    
    static testmethod void unittest10()
  {
     loadData();
     
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '20225';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;
      
        objAccount1 = new Account();
        objAccount1.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        objAccount1.lastname = 'Test';
        insert objAccount1;      
        
     
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLead';
        objLead2.LastName = 'TestLead';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.Email ='rishitest@test.com';
        objLead2.postalcode = '0000';
        objLead2.MobilePhone ='7053315188';
        objLead2.VGA_Email_Validation__c= 'Valid';
        objLead2.VGA_Marketing_opt_in__c= true;
       objLead2.VGA_Duplicate_Customer_Id__c=objAccount1.Id;
      
        objLead2.VGA_Customer_Account__c = objAccount1.id;
        insert objLead2;   

      Account objAccount = new Account();
        objAccount.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        objAccount.FirstName  = 'TestLead';
        objAccount.lastname = 'TestLead';
        objAccount.personEmail= 'test@gmail.com';
        objAccount.BillingPostalCode='0000';
        objAccount.VGA_Brand__c= 'Volkswagen';
        objAccount.VGA_Email_Validation__c= 'Valid';
        objAccount.Owner__c= 'Non_Owner';
        objAccount.VGA_News_Innovations_Flag__c= true;
            

        insert objAccount;
    }
  static testmethod void myUnittest3()
  {
    
    loadData();
    
    objProfanity=VGA_CommonTracker.createProfanity('TestLeadFirst');
        insert objProfanity; 
    
    objLead = new Lead();
        objLead.VGA_Brand__c = 'Skoda';  
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.VGA_Primary_id__c = '1234';
        objLead.postalcode = '1234';
        insert objLead; 
        
        Lead objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Skoda';  
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Spam__c = true;
        objLead2.VGA_Related_Request_ID__c = 'BFA7CE35-D8F7-42B5-BC7E-AE2A8985DA63';   
        insert objLead2; 
        
        list<Lead> lstLead = new list<Lead>();
        lstLead.add(objLead);
        
        map<string,Lead> mapofLead = new map<String,Lead>();
        mapofLead.put('1234',objLead);
        
        objLead.VGA_Request_Type__c = 'Brochure Request';
        update objLead;
        
        
        VGA_LeadTriggerHandler.checkRequestType(lstLead);
        
         objLead.VGA_Request_Type__c = 'Registration of Interest';
        update objLead;
         VGA_LeadTriggerHandler.checkRequestType(lstLead);  
         
        objProfanity=VGA_CommonTracker.createProfanity('TestLeadFirst');
        insert objProfanity;  
        
        VGA_LeadTriggerHandler.checkSpam(lstLead);
        
        objPostCodeMapping.VGA_SKP_Dealer_Code__c = '20225';
        update objPostCodeMapping;
        VGA_LeadTriggerHandler.getDealerCode(mapofLead);
        
         objLead.VGA_Brand__c = 'Volkswagen';  
         objLead.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';  
         update objLead;
         objPostCodeMapping.VGA_VWC_Dealer_Code__c = '20225';
        update objPostCodeMapping;
         VGA_LeadTriggerHandler.getDealerCode(mapofLead);
         
        VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','All');   
         insert objPH;
         
         objLead.VGA_Brand__c = 'Volkswagen';  
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        update objLead;
        
        VGA_LeadTriggerHandler.assignUser(lstLead);
      
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Skoda';  
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'TestLeadFirst';
        objLead2.LastName = 'TestLeadLastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Spam__c = true;
        objLead2.VGA_Related_Request_ID__c = 'BFA7CE35-D8F7-42B5-BC7E-AE2A8985DA63';  
        objLead2.VGA_Request_Type__c = 'Car Finder';
        insert objLead2;       
      
  }
  
  public static void LoadData()
  {
    VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Lead';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        VGA_Notification_Template__c obj = new VGA_Notification_Template__c();
      obj.name = 'New Lead Notification';
      obj.VGA_Email_Body__c = 'Hi';
      obj.VGA_Email_From_Address__c = 'pawan.mudgal@saasfocus.com123';
      obj.VGA_SMS_Body__c = 'Hi,';
      obj.VGA_SMS_From_Id__c = '1234';
      insert obj;
        
        objAccount1 = new Account();
        objAccount1.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        objAccount1.lastname = 'Test';
        insert objAccount1;
        
    
    VGA_Lead_High_Light_Rule__c objCS = new VGA_Lead_High_Light_Rule__c();
        objCS.Name = 'Web Site';
        objCS.VGA_Color_Code__c = '#EEE8AA';
        insert objCS;
        
        VGA_Active_Brochure__c objActive = VGA_CommonTracker.createActiveBrochure();
        objActive.Name = 'abc';
        objActive.VGA_Brand__c = 'Volkswagen';
        objActive.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        insert objActive;
        
        objPostCodeMapping = new VGA_Post_Code_Mapping__c();
        objPostCodeMapping.VGA_Post_Code__c = '1234';
        objPostCodeMapping.VGA_VWP_Dealer_Code__c= '20225';
        
        insert objPostCodeMapping;
        
        objCampaign = new Campaign();
        objCampaign.Name = 'Test';
        objCampaign.VGA_Tracking_ID__c = '12345';
        
        insert objCampaign;
  }
   /**
    * @author Ganesh M
    * @date 14/11/2018
    * @description Method that tests Updated Account when lead is created.
    */
static testmethod void test01_UpdateAccount_whenLeadCreated()
  {
    
    
     list<lead> leadlist = new list<lead>();
    Account objacc = VGA_CommonTracker.buildPersonAccount('Volkswagen Individual Customer','test');
         
            objacc.VGA_Brand__c = 'Volkswagen'; 
            objacc.VGA_Promotions_Flag__c  = true;
           insert objacc;  
    
     objLead = new Lead();
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'First';
        objLead.LastName = 'LastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        objLead.VGA_Tracking_ID__c = '12345';
          objlead.VGA_Color_Code__c= '12345';
        objLead.VGA_Marketing_opt_in__c = true;
        objLead.VGA_Customer_Account__c  = objacc.Id;
        insert objLead;  
        
        objLead2 = new Lead();
        objLead2.VGA_Brand__c = 'Volkswagen';
        objLead2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead2.VGA_Model_of_Interest__c = 'abc';
        objLead2.FirstName  = 'First';
        objLead2.LastName = 'LastName';
        objLead2.Company = 'TestCompany';
        objLead2.Status = 'New';
        objLead2.VGA_Campaign_Source__c = 'Web Site';
        objLead2.postalcode = '1234';
        objLead2.VGA_Customer_Account__c  = objacc.id;
        insert objLead2; 
        
  
   
       test.starttest();
         account acc = [select id,name,VGA_BBO_Tag_ID__c,VGA_BBO_Tag_Payload__c,lastname from account where id=:objLead.VGA_Customer_Account__c]; 
         
       test.stoptest();
       system.assertEquals(acc.name,'test');  
      
  }  
}