@isTest(seeAllData=false)
public class VGA_CreateCampaignMembersTracker 
{
    public static Lead objLead;
    public static Campaign objCampaign;
    //public static CampaignMember objCampaignMember;
    
   static testmethod void unittest1()
    {
        loadData();
        
        Test.startTest();
        VGA_CreateCampaignMembers obj = new VGA_CreateCampaignMembers();
        Database.executeBatch(obj);
        Test.stopTest();
    }
    
    public static void loadData()
    {
        objLead = new Lead();
        objLead.LastName = 'Test Lead';
        objLead.VGA_Tracking_ID__c = '647';
        objLead.Email = 'test@katzion.com';
        objLead.VGA_Brand__c = 'Volkswagen';
        insert objLead; 
        
        objLead = new Lead();
        objLead.LastName = 'Test Lead 1';
        objLead.VGA_Tracking_ID__c = null;
        objLead.Email = 'test1@katzion.com';
        objLead.VGA_Brand__c = 'Volkswagen';
        insert objLead; 
        
        objCampaign = new Campaign();
        objcampaign.name='test';
        objCampaign.Description = 'Test Campaign';
        objCampaign.VGA_Tracking_ID__c = '647';
        insert objCampaign;
        
        objCampaign = new Campaign();
        objcampaign.name='test1';
        objCampaign.Description = 'Test Campaign 2';
        objCampaign.VGA_Tracking_ID__c = '648';
        insert objCampaign;

    }
}