/**
* @author HIJITH NS
* @date 04/04/2019
* @description API Class to return lead details upon receiving lead id.
*/
@RestResource(urlMapping='/getLeadDetails/*')
global class VGA_LeadDetailAPI {
    public class LeadResponseWrapper{
        String firstname,lastname,email,mobile,model,startDate,startTime;
        public Integer statusCode;
        public LeadResponseWrapper(){
            firstname=null;
            lastname=null;
            email=null;
            mobile=null;
            model=null;
            startDate=null;
            startTime=null;
            statusCode=500;
        }
    }
    
    @HttpGet
    global static void getLeadByID() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        try{
            
            String LeadID = req.params.get('id');
            if (LeadID ==null || LeadID =='')
            {
                res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Lead not found"}'))); 
            }
            else
            { 
                Lead objLead  = new Lead();
                objLead = [select id,FirstName,LastName,Email,MobilePhone,VGA_Model_of_Interest__c,VGA_Test_Drive_Start_Date__c,VGA_Dealer_Account__c,VGA_Dealer_Account__r.VGA_Timezone__c from Lead where id=:LeadID or VGA_Primary_id__c=:LeadID];
                LeadResponseWrapper LeadData = new LeadResponseWrapper();
                LeadData.firstname = objLead.FirstName;
                LeadData.lastname = objLead.LastName;
                LeadData.email = objLead.Email;
                LeadData.mobile = objLead.MobilePhone;
                LeadData.model = objLead.VGA_Model_of_Interest__c;
                if(objLead.VGA_Test_Drive_Start_Date__c!=null){  //&& objLead.VGA_Dealer_Account__r.VGA_Timezone__c!=null){
                    //Timezone tm=Timezone.getTimeZone(objLead.VGA_Dealer_Account__r.VGA_Timezone__c);
                    //String data=objLead.VGA_Test_Drive_Start_Date__c.format('yyyy-MM-dd h:mm a',tm.getDisplayName());
                    //DateTime dd=DateTime.newInstanceGMT(objLead.VGA_Test_Drive_Start_Date__c.Year(),objLead.VGA_Test_Drive_Start_Date__c.month(),objLead.VGA_Test_Drive_Start_Date__c.Day(),objLead.VGA_Test_Drive_Start_Date__c.hour(),objLead.VGA_Test_Drive_Start_Date__c.minute(),objLead.VGA_Test_Drive_Start_Date__c.second());
                    //String data=dd.format('yyyy-MM-dd h:mm a');
                    
                    String data=objLead.VGA_Test_Drive_Start_Date__c.format('yyyy-MM-dd h:mm a',objLead.VGA_Dealer_Account__r.VGA_Timezone__c);
                    LeadData.startDate=(data.split(' ',2))[0];
                    LeadData.startTime=(data.split(' ',2))[1];
                }

                LeadData.statusCode = 200;
                res.responseBody = Blob.valueOf(JSON.serializePretty(LeadData));
            }
        }
        catch(Exception ex)  
        {
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Lead not found"}'))); 
        }
    }
}