/*
*Author         : MOHAMMED ISHAQUE SHAIKH
*Description    : Class is used to  calculate Escalation Time for Lead
*Created Date   : 2019-04-12
*
*/
public class VGA_CalculateEscalationTime{
    /*
    *Author         : MOHAMMED ISHAQUE SHAIKH
    *Description    : Method to calcutlate Escalation time
    *Params         : objLead    -> Lead 
                      mapofDistributionMethod-> contain Minutes info to escate lead
                      mapofPublicHoliday->contain holiday info
                      mapofDealerCodeWRTBillingState->contains Postal code information with respect to dealer
                      mapofTH-> Contains Trading hour information
    */
     public static DateTime calculateEscalationTime(Lead objLead,Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod,
                                                    Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday,
                                                    Map<String,String> mapofDealerCodeWRTBillingState,
                                                    Map<string,SObject> mapofTH){
        
        try{
            Integer escalateMinutes = getEscalateMinutes(objLead, mapofDistributionMethod);
            String dealerTimeZone   = getDealerTimeZone(objLead, mapofTH);
            Datetime nextDay        = getNextDayToProcessDateTime(dealerTimeZone);
            return getEscalationTime(objLead, nextDay, dealerTimeZone,escalateMinutes, mapofTH,mapofPublicHoliday,mapofDealerCodeWRTBillingState);
            
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return null;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method that gets the Escalation Date/Time for a particular
    *              lead.
    */
    private static Datetime getEscalationTime(Lead objLead, Datetime nextDay, String dealerTimeZone,
                                              Integer escalateMinutes, Map<string,SObject> mapofTH,
                                              Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday,
                                              Map<String,String> mapofDealerCodeWRTBillingState){
        String dealerState = VGA_CalculateHelperClass.getDealerState(objLead, mapofDealerCodeWRTBillingState);
        
        for(Integer i=0;;i++){
            
            if(!VGA_CalculateHelperClass.isPublicHoliday(mapofPublicHoliday,nextDay.dateGMT(), dealerState)){
                String tradingHoursKey  = (objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + nextDay.formatGmt('EEEE')).trim().tolowercase();
                
                String startTime = '';
                String endTime   = '';

                if(mapofTH.containskey(tradingHoursKey)){
                    SObject sObj = mapofTH.get(tradingHoursKey);

                    if(sObj instanceof VGA_Trading_Hour__c){
                        VGA_Trading_Hour__c tradHour = (VGA_Trading_Hour__c) sObj;
                        System.debug(' tradHour ' + tradHour);
                        if(tradHour == null
                           || !tradHour.VGA_Working__c){
                            nextDay=nextDay.addDays(1);
                            continue;
                        }
                        startTime = tradHour.VGA_Start_Time__c;
                        endTime   = tradHour.VGA_End_Time__c;
                    
                    } else if(sObj instanceof VGA_Working_Hour__c){
                        VGA_Working_Hour__c workHour = (VGA_Working_Hour__c) sObj;
                        System.debug(' workHour ' + workHour);
                        if(workHour == null
                           || !workHour.VGA_Working__c){
                            nextDay=nextDay.addDays(1);
                            continue;
                        }
                        startTime = workHour.VGA_Start_Time__c;
                        endTime   = workHour.VGA_End_Time__c;

                    }

                    Datetime dealerStartDt = VGA_CalculateHelperClass.convertToUserTimeZone(DateTime.parse(nextDay.formatGmt('dd/MM/yyyy') + ' ' + startTime));
                    Datetime dealerEndDt   = VGA_CalculateHelperClass.convertToUserTimeZone(DateTime.parse(nextDay.formatGmt('dd/MM/yyyy') + ' ' + endTime));

                    if(i!=0){
                        nextDay = dealerStartDt;
                    }

                    if(nextDay.getTime() < dealerStartDt.getTime() 
                        && nextDay.getTime() < dealerEndDt.getTime()){
                        nextDay = dealerStartDt;
                    }
                    
                    DateTime tempNextDate = nextDay;
                    nextDay = nextDay.addMinutes(escalateMinutes);

                    if(dealerStartDt.getTime()  <= nextDay.getTime() 
                        && dealerEndDt.getTime() >= nextDay.getTime()){
                
                        return transformDateToGTM(nextDay, dealerTimeZone);
                    }else{
                        escalateMinutes -= VGA_CalculateHelperClass.getMinutesDifference(tempNextDate, dealerEndDt);
                    }
                    
                    }
            }

            //Get the next date
            nextDay=nextDay.addDays(1);
            
        }

        return null;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description Methods that transforms a given date into a GTM format.
    * @param nextDay Date that needs to be converted into GTM
    * @param dealerTimeZone Time zone in which the date is
    * @return Datetime date in GTM format
    */
    private static Datetime transformDateToGTM(Datetime nextDay, String dealerTimeZone){
        string dateTimeStr = nextDay.format('yyyy-MM-dd HH:mm:ss',  dealerTimeZone);
        string dateGmtStr  = nextDay.formatGMT('yyyy-MM-dd HH:mm:ss');
        Datetime localDateTime = DateTime.valueOf(dateTimeStr);
        Datetime baseGMTTime   = DateTime.valueOf(dateGMTStr);
        Long milliSecDiff =  baseGMTTime.getTime() - localDateTime.getTime();
        Long minDiff = milliSecDiff / 1000 / 60;
        
        return nextDay.addMinutes(minDiff.intValue());
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description Methods that gets the next day that we need to process.
    * @param dealerTimeZone Time zone in which the date is
    * @return Datetime date of the next day
    */
    private static Datetime getNextDayToProcessDateTime(String dealerTZone){
        return Datetime.valueofGMT(System.now().format('yyyy-MM-dd HH:mm:ss', dealerTZone));
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description Methods that gets the dealer Time Zone. It will get this information
    *              from the Dealer Trading Hours.
    * @param objLead Lead from which we need the dealer time zone
    * @param mapofTH Map of trading Hours
    * @return String with the Dealer Time Zone
    */
    private static String getDealerTimeZone(Lead objLead, Map<string,SObject> mapofTH){
        //Key that is used to retrieve the trading hours
        String tradingHoursKey  = (objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase();
        
        if(mapofTH.containsKey(tradingHoursKey)){
            SObject sObj = mapofTH.get(tradingHoursKey);

            if(sObj instanceof VGA_Trading_Hour__c){
                VGA_Trading_Hour__c tradHour = (VGA_Trading_Hour__c) sObj;
                return tradHour.VGA_Timezone__c;
            } else if(sObj instanceof VGA_Working_Hour__c){
                VGA_Working_Hour__c workHour = (VGA_Working_Hour__c) sObj;
                return workHour.VGA_Timezone__c;
            }
        }
        
        return null;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description Methods that gets the escalate minutes for a particular
    *              dealer and.
    * @param objLead Lead from which we need the escalate minutes
    * @param mapofDistributionMethod Dealership trading hours per escalate minutes key
    * @return Integer Escalate minutes for this dealer
    */
    private static Integer getEscalateMinutes(Lead objLead, Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod){
        //Key that will be used to retrieve the escalate minutes for the current Lead
        String escalateMinutesKey = (objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase();
        return integer.valueof(mapofDistributionMethod.get(escalateMinutesKey).VGA_Escalate_Minutes__c);
    }
}