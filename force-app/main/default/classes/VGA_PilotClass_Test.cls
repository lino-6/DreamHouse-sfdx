/**
* @author Mohammed Ishaque Shaikh
* @date 05/Mar/2019
* @description Test Class for the VGA_TDAttachment.
*              It tests the isPilotDealer  method that returns information about Dealers.
*/
@isTest
public class VGA_PilotClass_Test {
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we pass valid DealerCode for testing
     * 
     */
	@isTest
    static void testWithValidDealerCode(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Consent_From_Settings__c='Terms and condition';
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	req.addParameter('dealerCode', '0007');
       		req.requestURI = '/services/apexrest/isPilotDealer';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_PilotClass.isPilotDealer();
        	System.assertEquals(200,((VGA_PilotClass.PilotDealerWrapper)JSON.deserialize(res.responseBody.toString(),VGA_PilotClass.PilotDealerWrapper.class)).statuscode);
        Test.stopTest();
    }
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we pass Invalid DealerCode for testing
     * 
     */
    @isTest
    static void testWithInvalidDealerCode(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Consent_From_Settings__c='Terms and condition';
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	req.addParameter('dealerCode', '0005');
       		req.requestURI = '/services/apexrest/isPilotDealer';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_PilotClass.isPilotDealer();
        	System.assertEquals(500,((VGA_PilotClass.PilotDealerWrapper)JSON.deserialize(res.responseBody.toString(),VGA_PilotClass.PilotDealerWrapper.class)).statuscode);
        Test.stopTest();
    }
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we wont pass DealerCode for testing
     * 
     */
    @isTest
    static void testWithoutDealerCode(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Consent_From_Settings__c='Terms and condition';
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	//req.addParameter('dealerCode', '0005');
       		req.requestURI = '/services/apexrest/isPilotDealer';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_PilotClass.isPilotDealer();
        	System.assertEquals(500,((VGA_PilotClass.PilotDealerWrapper)JSON.deserialize(res.responseBody.toString(),VGA_PilotClass.PilotDealerWrapper.class)).statuscode);
        Test.stopTest();
    }
}