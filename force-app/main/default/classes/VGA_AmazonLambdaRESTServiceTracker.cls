@isTest
public class VGA_AmazonLambdaRESTServiceTracker 
{
	@isTest(SeeAllData=true)
    public static void test1()
    {
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/findDealer'; 
        req.params.put('model', 'Golf');
        req.params.put('postcode', '2000');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        String str = VGA_AmazonLambdaRESTService.doGetDealer();
   		Test.stopTest(); 
    }
    
	@isTest(SeeAllData=true)
    public static void test2()
    {
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/findDealer'; 
        req.params.put('model', 'amarok');
        req.params.put('postcode', '2000');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        String str = VGA_AmazonLambdaRESTService.doGetDealer();
   		Test.stopTest(); 
    }    
    
	@isTest(SeeAllData=true)
    public static void test3()
    {
        Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/findDealer'; 
        req.params.put('model', 'fabia');
        req.params.put('postcode', '2000');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        String str = VGA_AmazonLambdaRESTService.doGetDealer();
   		Test.stopTest(); 
    }    
}