public without sharing class VGA_RecallDetailsController 
{
    
    @AuraEnabled
        public static List<VGA_Service_Campaigns__c> lst_service_campaign{get;set;}

    @AuraEnabled
     public static list<VGA_Service_Campaigns__c> getRecalllist(String requestParams, boolean isschedule)
                                          
     {   
         
         QueryFieldsWrapper queryCmp =(QueryFieldsWrapper) JSON.deserializeStrict(requestParams,QueryFieldsWrapper.class);
         system.debug('*****'+queryCmp);
        // string brand = '%' + queryCmp.brand + '%' ;
         string brand      = '\''+queryCmp.brand+'\'';
         string modelAlias = queryCmp.ModelAlias;
         string years      = queryCmp.ModalYear;
         string criteria   = queryCmp.Criteria;
         string cmptype   = queryCmp.Cmptype;
         string cmpcode   = queryCmp.Cmpcode;
         string states     = queryCmp.States;
         //string sch = 'Future';
         string sch = '\'future\'';
         string queryString;
         //criteria = '\'' + criteria + '\',';
        // system.debug('criteria'+criteria);
         queryString = 'SELECT VGA_SC_Vehicle__r.Product2.VGA_Model_Alias__c, VGA_SC_Vehicle__r.VGA_VIN__c,VGA_SC_Vehicle__r.VGA_Model__r.name,';
         queryString += ' VGA_SC_Vehicle__r.VGA_Model_Name__c,VGA_SC_Vehicle__r.VGA_Model__c,VGA_SC_Vehicle__r.VGA_Model_Year__c,VGA_SC_Vehicle__r.Account.BillingState,';
         queryString += ' VGA_SC_Vehicle__r.Product2.VGA_Brand__c,VGA_SC_Brand__c,Filter_criteria__c,VGA_SC_Status__c, VGA_SC_Vehicle__r.Nevdis_Owner_State__c,VGA_SC_Campaign_Type__c,Campaign_Code__c ';
         queryString += 'FROM VGA_Service_Campaigns__c';
         queryString += ' where  VGA_SC_Brand__c =  ' +brand;
         
         if(modelAlias != ''&& modelAlias != null)
         {
            queryString += ' and VGA_SC_Vehicle__r.Product2.VGA_Model_Alias__c in(' + PrepareINString(modelAlias) + ')'; 
         }
         
         if(years != '' && years != null)
         {
            queryString += ' and VGA_SC_Vehicle__r.VGA_Model_Year__c in(' + PrepareINString(years) + ')';
         }
          if(criteria != '')
         {
            //queryString += ' and Filter_criteria__c INCLUDES (:criteria)';
            queryString += ' and Filter_criteria__c INCLUDES (' + PrepareINString(criteria) + ')';
            //in (' + PrepareINString(criteria) + ')'; 
         }  
          if(cmptype != '')
         {
           
            queryString += ' and VGA_SC_Campaign_Type__c IN (' + PrepareINString(cmptype) + ')';
            //in (' + PrepareINString(criteria) + ')'; 
         }  
          if(cmpcode != '' && cmpcode != '--- None ---' && cmpcode != null)
         {
            
            queryString += ' and Campaign_Code__c INCLUDES (' + PrepareINString(cmpcode) + ')';
            //in (' + PrepareINString(criteria) + ')'; 
         }  
          if(states != ''&& states != null)
         {
           if(cmptype == 'Takata Airbag Recall'){
            queryString += ' and VGA_SC_Vehicle__r.Nevdis_Owner_State__c  in (' + PrepareINString(states) + ')'; 
             }
            else
            {
                queryString += ' and VGA_SC_Vehicle__r.Account.BillingState  in (' + PrepareINString(states) + ')';
            }
          } 
            queryString += ' and VGA_SC_Status__c = ' +sch;
            queryString += ' LIMIT 49999';
          system.debug('queryString****'+queryString);
          
          if(isschedule==true)
                {
                   VGA_RecallSearchBatch batchable = new VGA_RecallSearchBatch(queryString);
                        Database.executeBatch(batchable);
                }
                
           lst_service_campaign = Database.query(queryString);
       
       
  

         return lst_service_campaign; 
   } 
   
    @TestVisible 
    private static string PrepareINString(string input)
    {
        string[] values = input.split(';');
        string output = '';
        for(string value:values)
        {
            output += '\'' + value + '\',';
        }
        if(output.length()>0)
        {
            output = output.left(output.length() - 1);
        }
        return output;
    }
   
  @AuraEnabled
    public static boolean UpdateStatus(List <string>  lstrecordid)
       {
       
      List <VGA_Service_Campaigns__c> servicelist = [select Id from VGA_Service_Campaigns__c where id IN: lstrecordid];
    
    
     try
     {
  
        for(VGA_Service_Campaigns__c SC:servicelist)
              {
  
                    sc.VGA_SC_Status__c = 'Scheduled';
  
               } 
         Database.SaveResult[] srList = Database.update(servicelist, false);
               for (Database.SaveResult sr : srList) 
               {
                     if (sr.isSuccess())
                      {
                          // Operation was successful, so get the ID of the record that was processed
                             // System.debug('Successfully Updated');
                        }
                  else {
                           // Operation failed, so get all errors                
                         for(Database.Error err : sr.getErrors())
                          {
                           System.debug('The following error has occurred.');                    
                         System.debug(err.getStatusCode() + ': ' + err.getMessage());
                         //System.debug('Account fields that affected this error: ' + err.getFields());
                         }
                      }
                }
                        // update lst_service_campaign;
          return true; 
     }
     catch(exception e)
     {
         return false;
     }
 }  
    
@AuraEnabled
    public static Map<string, string> getModelAliasList(string brand)
    {
        
       brand = '%' + brand + '%' ;
       system.debug('Brand: ' + brand);
        Map<string, string> result = new Map<string, string>();        
      List<AggregateResult> models = [select VGA_Model_Alias__c from product2 WHERE VGA_Brand__c LIKE  :brand  group by VGA_Model_Alias__c ];
        for(AggregateResult model:models)
        {
            string model_alias = (string) model.get('VGA_Model_Alias__c');
            result.put(model_alias, model_alias);
        }
     
        
        return result;
    }
   @AuraEnabled
    public static Map<string, string> getStates()
    {
       
        Map<string, string> result = new Map<string, string>();        
        
        for(AggregateResult postalcode:[SELECT Nevdis_Owner_State__c  FROM Asset WHERE Nevdis_Owner_State__c   != null GROUP BY Nevdis_Owner_State__c])
     
        {
            //string postal_code = (string) postalcode.get('VGA_Region_Code__c');
            string postal_code = (string) postalcode.get('Nevdis_Owner_State__c');
            result.put(postal_code, postal_code);
        }
      
        return result;
    } 
     @AuraEnabled
    public static List<string> getyears()
    {
       
       List<string> result = new List<string>();
        result.add('--- None ---');        
         result.add('2007');
        result.add('2008');
        result.add('2009');
        result.add('2010');
        result.add('2011');
        result.add('2012');
        result.add('2013');
        result.add('2014');
        result.add('2015');
        result.add('2016');
        result.add('2017');
        
      
        return result;
    } 
  /* @AuraEnabled
 public static List <String> getselectOptions(sObject objObject, string fld) {
 
          List <String> allOpts = new list <String> ();
  
        Schema.sObjectType objType = objObject.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        map <String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
       list <Schema.PicklistEntry> values = fieldMap.get(fld).getDescribe().getPickListValues();
     
       for (Schema.PicklistEntry a: values) {
                 allOpts.add(a.getValue());
           }
  
            allOpts.sort();
   return allOpts;
 }*/
 @AuraEnabled  
    public static Map<String,List<String>> getDependentOptionsImpl(string objApiName , string contrfieldApiName , string depfieldApiName){
        system.debug(objApiName + '##' + contrfieldApiName + '###' + depfieldApiName);
           
        String objectName = objApiName.toLowerCase();
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
            //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
         
        if (!Schema.getGlobalDescribe().containsKey(objectName)){
            System.debug('OBJNAME NOT FOUND --.> ' + objectName);
            return null;
         }
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType==null){
            return objResults;
        }
        Bitset bitSetObj = new Bitset();
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        //Check if picklist values exist
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            System.debug('FIELD NOT FOUND --.> ' + controllingField + ' OR ' + dependentField);
            return objResults;     
        }
        
        List<Schema.PicklistEntry> contrEntries = objFieldMap.get(controllingField).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> depEntries = objFieldMap.get(dependentField).getDescribe().getPicklistValues();
         objFieldMap = null;
        List<Integer> controllingIndexes = new List<Integer>();
        for(Integer contrIndex=0; contrIndex<contrEntries.size(); contrIndex++){            
            Schema.PicklistEntry ctrlentry = contrEntries[contrIndex];
            String label = ctrlentry.getLabel();
            objResults.put(label,new List<String>());
            controllingIndexes.add(contrIndex);
        }
        List<Schema.PicklistEntry> objEntries = new List<Schema.PicklistEntry>();
        List<PicklistEntryWrapper> objJsonEntries = new List<PicklistEntryWrapper>();
        for(Integer dependentIndex=0; dependentIndex<depEntries.size(); dependentIndex++){            
               Schema.PicklistEntry depentry = depEntries[dependentIndex];
               objEntries.add(depentry);
        } 
        objJsonEntries = (List<PicklistEntryWrapper>)JSON.deserialize(JSON.serialize(objEntries), List<PicklistEntryWrapper>.class);
        List<Integer> indexes;
        for (PicklistEntryWrapper objJson : objJsonEntries){
            if (objJson.validFor==null || objJson.validFor==''){
                continue;
            }
            indexes = bitSetObj.testBits(objJson.validFor,controllingIndexes);
            for (Integer idx : indexes){                
                String contrLabel = contrEntries[idx].getLabel();
                objResults.get(contrLabel).add(objJson.label);
            }
        }
        objEntries = null;
        objJsonEntries = null;
        system.debug('objResults--->' + objResults);
        return objResults;
    }
    
public class QueryFieldsWrapper{
 @AuraEnabled   
  public String Brand{get;set;}
 @AuraEnabled   
  public String Criteria{get;set;}
  @AuraEnabled   
  public String Cmptype{get;set;}
  @AuraEnabled   
  public String Cmpcode{get;set;}
 @AuraEnabled   
  public String ModalYear {get;set;}
 @AuraEnabled   
  public string ModelAlias{get;set;}
  @AuraEnabled   
  public string States{get;set;}
        
}   
public class PicklistEntryWrapper{
public PicklistEntryWrapper(){            
        }
   public String active {get;set;}
   public String defaultValue {get;set;}
   public String label {get;set;}
   public String value {get;set;}
   public String validFor {get;set;}
} 
 }