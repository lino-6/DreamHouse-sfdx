//////////////////////////////////////////////////////////////////////////////
//Description:Batch Class To Delete ADM Records which are created 30 days before
//============================================================================
//Name:Rishi Kumar patel    Date:1 Feb'18    email:rishi.patel@saasfocus.com
//============================================================================
//////////////////////////////////////////////////////////////////////////////
global class VGA_DeleteAllianzDataRecords implements Database.Batchable<sObject> 
{
	//Fetch All Records which have been created 30 days Before
	global Database.querylocator start(Database.BatchableContext bc)
	{
		Datetime lastDate  = system.today().adddays(-30);
		string strADM = 'select id,createdDate from VGA_Allianz_Data_Mapping__c where CreatedDate <:lastDate';
		
		return Database.getQueryLocator(strADM);
	}
	//Delete All Fetched Records
	global void execute(Database.BatchableContext BC, List<VGA_Allianz_Data_Mapping__c> lstADM)
	{
		if(lstADM != null && !lstADM.isEmpty())
		{
			delete lstADM;
		}			
	}
	global void finish(Database.BatchableContext BC)
	{
		
	} 
}