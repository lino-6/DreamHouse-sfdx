/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_CommonTestClass 
{
    public static Account objAccount;
    public static Contact objContact;
    public static User objUser;
    static testMethod void myUnitTest() 
    {
        LoadData();
        // TO DO: implement unit test
        list<string> lstFields = VGA_Common.getPickListValues('Lead','status');    
        Date dtStartDate = VGA_Common.getWeekStartDate(system.today());
        
        string strloggingAccountId = VGA_Common.getloggingAccountId();
        
        list<contact> lstContact = VGA_Common.getAllContact(objAccount.id);
        
        User objUser = VGA_Common.getloggingAccountDetails();
        
        string FieldList = VGA_Common.getSobjectFields('Lead');
        
        Id recordtypeID = VGA_Common.GetRecordTypeId('Lead','Skoda Fleet Customer');
        
        Boolean Check = VGA_Common.IsNullOrEmptyString('test');
        
        Datetime dt = VGA_Common.converttoGMT(system.today());
        
        Datetime dt1= VGA_Common.convertstrtoGMT(string.valueof(system.now()));
        
        Id id1 = VGA_Common.getSubscriptionRecordTypeId();
        
        Id id2 = VGA_Common.getReportSubscriptionRecordTypeId();
        
        string strrecordtypeid = VGA_Common.recordTypeId('Skoda Dealer Contact');
        
        VGA_Common.sendExceptionEmail('test','test');
        set<String> myArray = new set<String>();
        myArray.add('Dealer Administrator');
        set<String> myArray2 = new set<String>();
        myArray2.add('Lead Controller');
        set<String> myArray3 = new set<String>();
        myArray3.add('Consultant');
         set<String> myArray4 = new set<String>();
        myArray4.add('Dealer Portal Group');
         set<String> myArray5 = new set<String>();
        myArray5.add('Dealer Portal');
       	VGA_Common.getTopMostRole(myArray);
         VGA_Common.getTopMostRole(myArray2);
        VGA_Common.getTopMostRole(myArray3);
        VGA_Common.getTopMostRole(myArray4);
        VGA_Common.getTopMostRole(myArray5);
        VGA_Common.toSentenceCase('VGA');
        VGA_Common.toSentenceCase('VGA,VGA');
        VGA_Common.toSentenceCase('VGA.VGA');
        VGA_Common.toSentenceCase('VGA VGA');
        system.runAs(objUser)
        {      
            list<String> lstProfileName = VGA_Common.getProfileName();  
            //VGA_Common.getTopMostRole(myArray);
            VGA_Common.getuserId(objUser.ContactiD);
           //VGA_Common.toSentenceCase('VGA');
            //VGA_Common.getTopMostRole(myArray2);
           // VGA_Common.getTopMostRole(myArray3);
            //VGA_Common.getTopMostRole(myArray4);
             //VGA_Common.getTopMostRole(myArray5);
        }
    }
    public static void LoadData()
    {
        objAccount = VGA_CommonTracker.createDealerAccount();
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        objUser = VGA_CommonTracker.CreateUser(objContact.id);
    }
}