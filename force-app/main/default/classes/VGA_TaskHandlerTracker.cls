/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_TaskHandlerTracker 
{
	public static Account objAccount;
	public static Contact objContact;
	public static Case objCase;
	public static Task objTask;
	public static Task objTask1;
	public static Task objTask2;
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        loadData();
        
        insert objTask;
        
    	insert	objCase;
    	
    	objTask1 = new Task();
    	objTask1.whatid = objCase.id;
    	objTask1.subject = 'New Interaction Assigned';
    	objTask1.status = 'open';
    	objTask1.priority = 'High';
    	insert objTask1;  
    	
    	objTask2 = new Task();
    	objTask2.whatid = objCase.id;
    	objTask2.subject = 'New Interaction Assigned';
    	objTask2.status = 'open';
    	objTask2.priority = 'High';
    	insert objTask2;     
    }
    public static void loadData()
    {
    	VGA_Triggers__c objTrigger = new VGA_Triggers__c();
    	objTrigger.name = 'Task';
    	objTrigger.VGA_Is_Active__c = true;
    	
    	insert objTrigger;
    	
    	objAccount = VGA_CommonTracker.createDealerAccount();
    	
    	objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
    	
    	 objTask = new Task();
         objTask.whoid = objContact	.id;
         objTask.status = 'open';
         objTask.priority = 'High';
         
         objCase = VGA_CommonTracker.createCase();  
    }
}