/**
* @author Mohammed Ishaque Shaikh
* @date 2019-03-20
* @description Class is used to List all Recordtype  and access throughout Organization
* 
*/
public class VGA_GlobalRecordType{
    public static Boolean GlobalCheck;
    public static String Global_Account_VWRTId,Global_Account_SKRTId,Global_DealerNotification_SMSRTId,Global_DealerNotification_EmailRTId,Global_Lead_VWRTId,Global_Lead_SKRTId;
    static{
        try{
            //Account RecordType
            Global_Account_VWRTId                   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
            Global_Account_SKRTId                   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
            
            //VGA_Dealer_Notification__c RecordType
            Global_DealerNotification_SMSRTId       = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
            Global_DealerNotification_EmailRTId     = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();

            //Lead RecordType
            Global_Lead_VWRTId                      = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
            Global_Lead_SKRTId                      = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId(); 
            
            //GLobal Check If Needed
            GlobalCheck=true;
        }catch(Exception ex){
            GlobalCheck=false;
            System.debug('Error in VGA_GlobalRecordType Apex Class >> Unable to Load  RecordTypeDetails');
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
}