/*
*author : MOHAMMED ISHAQUE SHAIKH
*description : to schecule VGA_ServiceClaimBatch Class
*Created Date : 2019-07-10
*/
global class VGA_ScheduleServiceClaim implements Schedulable {
    global void execute(SchedulableContext sc){
        Database.executeBatch(new VGA_ServiceClaimBatch());
    }
}