public class VGA_ExportLeadController 
{
    public string Assignedvalue {get;set;}
    public string StatusValue{get;set;}
    public string DealerId {get;set;}
    public string ConsultantId{get;set;}
    public list<Lead>listofLead {get;set;}
    public String queryString{get;set;}
    public List<Account> accList{get;set;}

    public VGA_ExportLeadController(ApexPages.StandardController controller) 
    {
         Assignedvalue =ApexPages.currentPage().getParameters().get('assign');
         StatusValue    =ApexPages.currentPage().getParameters().get('status');
         DealerId =ApexPages.currentPage().getParameters().get('accId');
         ConsultantId=ApexPages.currentPage().getParameters().get('consltId');
         listofLead     =New list<Lead>();
        
         getLeadList();
        
    }
    public void getLeadList()
    {
       map<string,string>mapofstatus =new map<string,string>{'New'=>'New','Accepted'  =>'Accepted','Closed'    => 'Closed'};
       map<string,string>mapofdate     =new map<string,string>{'Last 30 days' => system.now().addDays(-30).format('yyyy-MM-dd'),
                                                              'Today'        =>system.now().format('yyyy-MM-dd'),
                                                              'Last 7 days'  =>system.now().adddays(-7).format('yyyy-MM-dd'),
                                                              'Last 90 days' => system.now().adddays(-90).format('yyyy-MM-dd')                          
                                                             };
        queryString = 'select id,PartnerAccountId,OwnerId,VGA_Created_Date_Formula__c';
        for(Schema.FieldSetMember fld :SObjectType.Lead.FieldSets.VGA_Lead_Field_Set.getFields()) 
        {
         queryString += ', ' + fld.getFieldPath();
        }
        queryString += ' from Lead ';
        queryString += ' where PartnerAccountId =: DealerId';
             if(ConsultantId !='All')
             {
               queryString += ' and OwnerId =: ConsultantId';
             }
             if(StatusValue !='All' && StatusValue !='New and Accepted')
             {
               queryString += ' and Status =\''+ mapofstatus.get(StatusValue)+'\'';
             }
             else if(StatusValue =='New and Accepted')
             {
                queryString +=  ' and (Status =\'New\' or Status = \'Accepted\' ) ';
             }
             if(Assignedvalue !='More than 90 days')
             {
                queryString += ' and VGA_Assign_Date__c<= ' + mapofdate.get('Today');
                queryString += ' and VGA_Assign_Date__c>= ' + mapofdate.get(Assignedvalue) ;
             }
             else
             {
               date morethan90Dates =date.valueof(mapofdate.get('Today')).adddays(-90);
               queryString += ' and VGA_Assign_Date__c <=:morethan90Dates'; 
             }
             listofLead = Database.query(queryString);
            
    }
   

}