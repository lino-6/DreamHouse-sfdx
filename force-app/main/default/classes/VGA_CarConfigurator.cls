/**
* @author Ashok Chandra
* @date 17/10/2018
* @description Class to create the car configurator lead based on the selection like options,color,modal.
*/
Public class VGA_CarConfigurator{
     
     public String postcode1          {get;set;}
     public String modelname          {get;set;}
     public String firstname          {get;set;}
     public String lastname           {get;set;}
     public String phonenumber        {get;set;}
     public String EmailAddress       {get;set;}
     public integer marketingoptn     {get;set;}
     public Boolean checkbox          {get;set;}
     public Boolean Dealerbackcheckbox{get;set;}
      
     public string colorcode          {get;set;}
     public string options            {get;set;}
     public String brand = '';
     public String source = '';
     public integer Lead_type =104046;
     public integer callbackLead_type =104014;
     public integer site_id =1;
     public integer follow_up_lead =1;
     public integer brand_did =101783;
     public string DDBURL;
     Public string Modalcode;
     Public string Modalyear;
     Public string variant;
 
 
    /**
    * @author Ashok Chandra
    * @date 1710/2018
    * @description constructor
    */
     Public  VGA_CarConfigurator()
     {
         DDBURL    = ApexPages.currentPage().getParameters().get('bbo1');
         modelname = ApexPages.currentPage().getParameters().get('bbo2');
     
        if(ApexPages.currentPage().getParameters().get('bbo3')!=null){
            postcode1 = ApexPages.currentPage().getParameters().get('bbo3');
        }
        
        
        if(ApexPages.currentPage().getParameters().get('postcode')!=null && postcode1==null){
            postcode1 = ApexPages.currentPage().getParameters().get('postcode');
        }
            
            
        if(DDBURL!=null )
        {
            parseDDBurl(DDBURL);
        }
            
        system.debug('checkboxxxxx'+ ApexPages.currentPage().getParameters().get('checkbox1'));
    }
    
    
    /**
    * @author Ashok Chandra
    * @date 17/10/2018
    * @description It will create the Json based on selections i.e., marketing,brand, source,
    *              sub brand,vechicle interest and call the Mule Api to create the lead.
    *              We will get the active broucher code by using modelname in custom metadata type
    */
    public PageReference submit()
    {
        if(checkbox == true)
        {
            marketingoptn = 1;
        }
        else{
            marketingoptn = 0;
        }
          
        if(Modalcode!=null)
        {
        list<product2> produtcode = [select id,VGA_Active_Brochure__c,VGA_Active_Brochure__r.name,VGA_Active_Brochure__r.VGA_Brand__c,VGA_Active_Brochure__r.VGA_Sub_Brand__c from Product2 where ProductCode =:Modalcode];
             if(produtcode != Null && produtcode.size() > 0)
              {
                  modelname=produtcode[0].VGA_Active_Brochure__r.name;
              }
            
              if(!produtcode.isEmpty())
              { 
                  if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c =='Volkswagen' && produtcode[0].VGA_Active_Brochure__r.VGA_Sub_Brand__c=='Commercial Vehicles (CV)')
                  {
                       source='PV Website';
                      brand_did=101784;
                      
                  }
                  if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c =='Volkswagen' && produtcode[0].VGA_Active_Brochure__r.VGA_Sub_Brand__c =='Passenger Vehicles (PV)')
                  {
                       source='PV Website';
                      brand_did=101783;
                      
                  }
                  
                  if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c=='Skoda')
                  {
                       source='Skoda Website';
                       brand_did=101785;
                      
                  }
                  
              }
           } 
         
     
         //  The endpoint information is coming from the VGA_Configurator named credential
      
     
         string setbody  = '{"email_address":"'+this.EmailAddress+ '", '
                    + '"lead_type_did":"'+ this.Lead_type + '", '
                    + '"site_id":"'+ this.site_id+ '", '
                    + '"source": "'+ this.source+ '", '
                    + '"brand_did":"'+ this.brand_did+ '", '
                    + '"post_code":"'+ this.postcode1 + '", '
                    + '"car_configurator_url":"'+ this.DDBURL + '", '
                    + '"marketing_opt_in":"'+this.marketingoptn+ '", '
                    + '"vehicle_interested_in":"'+this.modelname+ '", '
                    + '"user1":"'+this.colorcode+ '", '
                    + '"user3":"'+this.Modalcode+ '", '
                    + '"user4":"'+this.Modalyear+ '", '
                    + '"user5":"'+this.variant+ '", '
                    + '"brand":"'+ this.brand + '" , }';
        HttpRequest req = new HttpRequest();
        Http http = new Http();
        req.setHeader('Accept', 'application/json');
        req.setHeader('Content-type', 'application/json');
        req.setEndpoint('callout:VGA_CarConfigurator');
        req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
        req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c); 
        req.setMethod('POST');
        system.debug('Dealerbackcheckbox   ' +this.Dealerbackcheckbox );
        if(Dealerbackcheckbox == false && options!='+')
        {
            setbody=setbody.remove('}');
            setbody+=     '"user2":"'+this.options+ '" ,'
                         +'"last_name":"'+ this.EmailAddress+ '" }' ;
            req.setBody(setbody);
        }
        else if(Dealerbackcheckbox == false && options=='+')
        {
            setbody=setbody.remove('}');
            setbody+=     '"user2":"",'
                         +'"last_name":"'+ this.EmailAddress+ '"  }' ;
            req.setBody(setbody);
        }
        else if(Dealerbackcheckbox == true && options!='+')
        {
            setbody=setbody.remove('}');
            setbody+=     '"user2":"'+this.options+ '",'
                        + '"follow_up_lead":"'+this.follow_up_lead+ '", '  
                        + '"first_name":"'+ this.firstname+ '", '
                        + '"last_name":"'+ this.lastname+ '", '
                        + '"mobile_phone":"'+this.phonenumber+ '" }' ;
            req.setBody(setbody);
        }
        else if(Dealerbackcheckbox == true && options=='+')
        {
            setbody=setbody.remove('}');
            setbody+=   '"follow_up_lead":"'+this.follow_up_lead+ '", ' 
                     + '"first_name":"'+ this.firstname+ '", '
                     + '"last_name":"'+ this.lastname+ '", '
                     + '"mobile_phone":"'+this.phonenumber+ '" }';
            
            req.setBody(setbody);
        }
        system.debug('request**********'+ req.getBody());
        try
        {
            HttpResponse response = http.send(req);
            String body = response.getBody();
            System.debug(body);
            if (response.getStatusCode() != 201)
            {
                System.debug('The status code returned was not expected: ' +
                response.getStatusCode() + ' ' + response.getStatus());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Cannot process the request'));
            }
            else
            {
                System.debug(response.getBody());
            }
        }
        catch(System.CalloutException e)
        {}
         
         return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenCarConfiguratorThankyou');
   
    }
    
    /**
     * @author Ashok Chandra
     * @date 17/10/2018
     * @description It accepts one Url and will spilt the URL to get modelname, 
                    modelyear,modelcode,Colorcode and options.
     * @param ddurl URl parameter called bbol. 
     */
    @TestVisible
    private void parseDDBurl(string ddurl)
    {
        findUrlElements(ddurl);
        DDBURL  = getDDBURL(ddurl);
        System.debug(' DDBURL ---> ' + DDBURL);
        options=getOptions(ddurl);
        System.debug(' options ---> ' + options);
   }

   /**
     * @author Lino Diaz Alonso
     * @date 08/02/2019
     * @description Method used to iterate through the received url and find all the elements that
     *              we will use to update the lead fields.
     * @param ddurl URl parameter called bbol. 
     */
   private void findUrlElements(string ddurl){
        
        Integer position = 0;
        // We are assuming the url will always have this format: 
        //https://www.volkswagen.com.au/app/configurator/vw-au/en/golf/30316/37625/r/BQ1RMH-GPB3PB3-GPDDPDD-GPI8PI8-GPLAPLA-GPRDPRD-GPZ3PZ3-MSNRS7X-GWL5WL5-GWZDWZD-GW4BW4B-GZMDZMD/2019/0/F14%20G2G2/F56%20%20%20%20%20TW/GPS1PS1?page=summary&postcode=2000
        
        for(String urlparameter : ddurl.split('/')){
            System.debug(' ------> ' + urlparameter);
            //We will find first the language that in our case will be en as we know that after
            //language the model will be provided
            if(urlparameter == 'en'
                && position == 0){
                position = 1;
            } else if(position == 1){
                System.debug(' Modelname ----> ' + urlparameter);
                modelname = urlparameter;
                position++;
            } else if(position ==2
                      && !urlparameter.isNumeric()){
                //After the model we will be receiveing few numbers and the next no numeric parameter will be the variant
                System.debug(' Sub-Model name ----> ' + urlparameter);
                variant = urlparameter;
                position++;
            } else if(position ==3){
                //After the variant we will be receiving the model code.
                Modalcode = urlparameter.substring(0,urlparameter.indexOf('-'))+'/';
                position++;
            } else if(position ==4){
                //After the model code we will be getting the year.
                //We will attach the two last digits of the year to the model code
                System.debug(' Year ----> ' + urlparameter);
                Modalyear = urlparameter;
                Modalcode += urlparameter.substring(2,Modalyear.length());
                position++;
            } else if(position ==5
                      && !urlparameter.isNumeric()){
                //The last parameter that we will be getting is the color code
                System.debug(' Color Code ---->' + urlparameter.substring(urlparameter.indexOf(' '), urlparameter.length()).trim());
                colorcode= urlparameter.substring(urlparameter.indexOf(' '), urlparameter.length()).trim();
                position++;
            }
        }
   }

   /**
     * @author Lino Diaz Alonso
     * @date 08/02/2019
     * @description Method that returns the ddbURl after replacing the first space for its UTF character
     *              and adding the plus in the options section. The rest of the url should not contain
     *              any space
     * @param ddurl URl parameter called bbol. 
     */
   private String getDDBURL(string ddurl){
        
        Integer position = 0;
        System.debug('URL before ---> ' + ddurl);
        String ddbURL =ddurl.replaceFirst(' ', '%20') ;
        
        if(ddbURL.contains('/+')){
            return ddbURL.substringBefore('?') + '+?' +  ddbURL.substringAfter('?');
        }

        ddbURL =ddbURL.replace('/ ', '/+') ;
        ddbURL = ddbURL.replaceAll(' ', '');

        return ddbURL;
        
    }

    /**
     * @author Lino Diaz Alonso
     * @date 08/02/2019
     * @description Method that returns the options specified in the url. Options are defined just before
     *              the ? and if they are empty we will return a +.
     * @param ddurl URl parameter called bbol. 
     */
    private String getOptions(string ddurl){
        System.debug(' ddurl ---> ' + ddurl);
        //String optionsValue = ddurl.substringBetween('/ ', '?');
        String optionsValue = ddurl.substring(ddurl.lastIndexOf('/')+1,ddurl.lastIndexOf('?'));
        System.debug(' options ---> ' + optionsValue);

        if(optionsValue ==null ||
            optionsValue == ''||
            optionsValue == ' '){
            return '+';
        }


        return optionsValue;
    }
   
}