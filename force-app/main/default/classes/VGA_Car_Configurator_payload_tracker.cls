@isTest
private class VGA_Car_Configurator_payload_tracker {


 @testSetup
    static void buildConfigList() {
        
       VGA_Car_configurator_Config__c con=new VGA_Car_configurator_Config__c();
       
      con.Name='golf-r';
      con.VGA_Api_Name__c='glof-r';
      con.VGA_Expiry_Date__c=null;
      
      insert con;
      
      Attachment a=new attachment();
      a.Name='test';
      a.body=blob.valueof('test');
      a.parentid=con.id;
      
      insert a;
        
        
    }
    
    /**
    * @author Ashok Chandra
    * @date 11/07/2019
    * @description Method that tests the VGA_Car_Configurator_payload class.
    *       To Make sure Payload API is returing the Json of attached file of  VGA_Car_configurator_Config__c record      
    *        
    */

static testMethod void testDoGet() {
     Test.starttest();
     VGA_Car_configurator_Config__c conf=[select Name,VGA_Api_Name__c from VGA_Car_configurator_Config__c limit 1];
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
    
    req.requestURI = '/services/apexrest/getCarConfiguratorConfig';  
    req.httpMethod = 'GET';
    RestContext.request = req;
    req.params.put('apiName',conf.VGA_Api_Name__c);
    RestContext.request = req;
    RestContext.response = res;
    VGA_Car_Configurator_payload.Getconfig();
    Test.stoptest();
     system.assertEquals(res.statuscode, 200);

}
 /**
    * @author Ashok Chandra
    * @date 11/07/2019
    * @description Method that tests the VGA_Car_Configurator_payload class.
    *       To Make sure Payload API is returing the empty payload when the record is not found or request parameter is empty      
    *        
    */

static testMethod void VGA_Car_Configurator_payload_testDoGet_norecord() {
        test.starttest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/getCarConfiguratorConfig';  
        req.httpMethod = 'GET';
        req.params.put('apiName','golf-r22');
        RestContext.request = req;
        RestContext.response = res;
        VGA_Car_Configurator_payload.Getconfig();
        
        
        
        req.httpMethod = 'GET';
        req.params.put('apiName','');
        RestContext.request = req;
        RestContext.response = res;
        VGA_Car_Configurator_payload.Getconfig();
        Test.stoptest();
}

}