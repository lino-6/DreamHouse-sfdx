public with sharing class contactUsController {
    @TestVisible
private String getThreadId(String caseId){
 return '[ ref:_' 
         + UserInfo.getOrganizationId().left(5) 
         + UserInfo.getOrganizationId().mid(11,4) + '._' 
         + caseId.left(5)
         + caseId.mid(10,5) + ':ref ]';
 }  
  @TestVisible   
private void sendEmail(Case c, String email, String brand) 
{
        Web_To_Case__c wt = Web_To_Case__c.getInstance(brand);        
        if(wt == Null) {
            return;
        }        
        Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        EmailTemplate template = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = :wt.Email_Template__c];
        String threadId = getThreadId(c.id);        
        String subject = template.Subject;    
        subject = subject.replace('{!Case.CaseNumber}', c.CaseNumber);
        if(c.Subject != Null) {
            subject = subject.replace('{!Case.Subject}', c.Subject);
        }
        subject = subject.replace('{!Case.Thread_Id}', threadId);        
        String htmlBody = template.HtmlValue;        
        if(htmlBody != Null)   {
            htmlBody = htmlBody.replace('{!Case.Thread_Id}', threadId);  
            htmlBody = htmlBody.replace('{!Case.CaseNumber}', c.CaseNumber);
        }        
        String plainBody = template.Body;
        plainBody = plainBody.replace('{!Case.Thread_Id}', threadId);  
        plainBody = plainBody.replace('{!Case.CaseNumber}', c.CaseNumber);   
        String[] toAddresses = new String[] {email}; 
        mail.setToAddresses(toAddresses);
        mail.setReplyTo(wt.Service_Email__c);      
        OrgWideEmailAddress[] owea = [select Id,address from OrgWideEmailAddress where Address = :wt.Service_Email__c];        
        if ( owea.size() > 0 && !owea.isempty()) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        else {    
            mail.setSenderDisplayName(wt.Display_Name__c);    
        }        
        mail.setSubject(subject);        
        mail.setBccSender(false);        
        mail.setUseSignature(false);        
        mail.setPlainTextBody(plainBody);        
        mail.setHtmlBody(htmlBody);        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });     
        EmailMessage emailMsg = new EmailMessage(); // Created a EmailMessage and copy all details from above.
        emailMsg.ToAddress = (mail.getToAddresses())[0];
        emailMsg.FromAddress = wt.Service_Email__c;
        emailMsg.Subject = mail.getSubject();
        emailMsg.HtmlBody = mail.getHtmlBody();
        emailMsg.TextBody = mail.getPlainTextBody();
        emailMsg.ParentId = c.Id; //Attach with the case
        emailMsg.MessageDate = system.now();
        emailMsg.Status = '0';  
        insert emailMsg;    
}

public List<SelectOption> getRequestType() {
  List<SelectOption> requestTypeList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Case.Interaction_Type__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  for( Schema.PicklistEntry f : ple) {
    requestTypeList.add(new SelectOption(f.getLabel(), f.getValue()));
  } 
 return requestTypeList;
}

public List<SelectOption> getOwnershipAmendmentPurpose() {
  List<SelectOption> purposeList = new List<SelectOption>();
  List<SelectOption> mainPurposeList = new List<SelectOption>(); 
  mainPurposeList.add(new SelectOption('--None--', '--None--'));
    
  Schema.DescribeFieldResult fieldResult = Case.Ownership_Amendment_Purpose__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  //purposeList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) {
    purposeList.add(new SelectOption(f.getLabel(), f.getValue()));
  }
  purposeList.sort();
  mainPurposeList.addAll(purposeList);
  return mainPurposeList;
} 


public List<SelectOption> getBrandType() {
  List<SelectOption> brandList = new List<SelectOption>();
  brandList.add(new SelectOption('Volkswagen PV', 'Passenger'));
  brandList.add(new SelectOption('Volkswagen CV', 'Commercial'));     
  return brandList;
}


public List<SelectOption> getStateList() {
  List<SelectOption> stateList = new List<SelectOption>();
  stateList.add(new SelectOption('--None--', '--None--'));
  stateList.add(new SelectOption('ACT', 'ACT'));     
  stateList.add(new SelectOption('NSW', 'NSW'));
  stateList.add(new SelectOption('NT', 'NT'));
  stateList.add(new SelectOption('QLD', 'QLD'));
  stateList.add(new SelectOption('SA', 'SA'));
  stateList.add(new SelectOption('TAS', 'TAS'));
  stateList.add(new SelectOption('VIC', 'VIC'));
  stateList.add(new SelectOption('WA', 'WA'));
  return stateList;
}

public List<SelectOption> getTitleType() {
  List<SelectOption> titleList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Account.Salutation.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  titleList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) {
    titleList.add(new SelectOption(f.getLabel(), f.getValue()));
  } 
  return titleList;
}    

public List<SelectOption> getEnquiryAbout() {
  List<SelectOption> enquiryList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Case.VGA_Product__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  enquiryList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) {
    enquiryList.add(new SelectOption(f.getLabel(), f.getValue()));
  }    
  return enquiryList;
}  


public List<SelectOption> getContactMeVia() {
  List<SelectOption> contactMeViaList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Case.Contact_Me_Via__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  contactMeViaList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) {
    contactMeViaList.add(new SelectOption(f.getLabel(), f.getValue()));
  }   
  return contactMeViaList;
}

public List<SelectOption> getVWModel() {
  List<SelectOption> VWModelList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Case.VW_Model__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  VWModelList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) {
    VWModelList.add(new SelectOption(f.getLabel(), f.getValue()));
  }    
  return VWModelList;
}

public List<SelectOption> getSkodaModel() {
  List<SelectOption> SkodaModelList = new List<SelectOption>();
  Schema.DescribeFieldResult fieldResult = Case.Skoda_Model__c.getDescribe();
  List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
  SkodaModelList.add(new SelectOption('--None--', '--None--'));
  for( Schema.PicklistEntry f : ple) 
  {
    SkodaModelList.add(new SelectOption(f.getLabel(), f.getValue()));
  }    
  return SkodaModelList;
}

public String selectedSkodaModelList {set; get;}
List<Attachment> allAttachment = new List<Attachment>();
public String selectedVWModelList {set; get;}
public String selectedContactMeViaList { set; get;} 
public String selectedEnquiryAbout { set; get;}  
public String selectedRequestType { set; get;}
public String selectedTitleType {set; get;}    
public String selectedAmendmentType {set; get;}
public String firstName {set; get;}
public String lastName {set; get;}  
public String email {set; get;}   
public String phoneNumber {set; get;} 
public Boolean vwOwner {set; get;}
public Boolean skodaOwner {set; get;}    
public String vin { set; get;}
public String rego {set; get;}
public String description {set; get;}   
public String iConsent {set; get;}
public Boolean receiveEmail {set; get;} 
public String selectedBrandType {set; get;} 
public String brand { set; get;}    
public String dateWrittenOff { set; get;} 
public String datestolen {set; get;} 
public String dateExported { set; get;}
public String dateOfSale { set; get;}
public String dateOfBirth { set;  get;}
public String dateOfChange { set; get;}
public String kilometer { set; get;}
public String licenseNumber {set; get;}
public String licenseState { set; get;}
public String companyName { set; get;}
public String companyPosition {set; get;}
public String abn {set; get;}
public String postalAddress { set; get;}
public String suburb {set; get;}
public String state {set; get;}
public String postCode { set;  get;}    
public String homeNumber { set; get;}
public String workNumber { set; get;}   
public String mobile { set; get;}
public String message { set; get;}
public String dpid { set; get;}
public Boolean vinValid { set; get;}

/**
* @author Archana Yerramilli
* @date  22/01/2018
* @description The below fields have been added as per ticket#633.
* @description As per ticket#633, a new Amendment type is added and the below fields are to be saved to case object. 
* @param Case List
*/
public String NewOwnerFirstName {set; get;}
public String NewOwnerLastName {set; get;}
public String NewOwnerEmail {set; get;}
public String NewOwnerMobile {set; get;}
public String DateSold {set; get;}

public Attachment attachment_ppsr {  get {
    if (attachment_ppsr == null)
      attachment_ppsr = new Attachment();
    return attachment_ppsr;
  }  
  set;
}
public Attachment attachment_insurance {  get {
    if (attachment_insurance == null)
      attachment_insurance = new Attachment();
    return attachment_insurance;
  }
   set;
} 
public Object validateVIN() {
  List<Asset> vehicleList;
  vehicleList = [SELECT Name FROM Asset WHERE VGA_VIN__c = :vin ];

  if(vehicleList.size() <= 0) {
    vinValid = False;
  }
  else {
      vinValid = True;
  }

  return null;
}  
public Account findBAccount(String companyName, String abn, String email, String mobile, Boolean isOwner, String brand, string state, string postCode)
{
        Muleapi api = new Muleapi();       
        api.email_address = email;
        api.region_code = state;
        api.post_code = postCode;
        api.brand = brand;
        api.company_name = companyName;
        api.phone = mobile;
        api.ABN = abn;
        String bid;
        Account ba= null;       
        if(brand == 'Skoda') 
        {
            bid= api.businessAccountSearch();
            if(bid!=null && bid != '')
               {
                ba= [select id from account where id=:bid];
               }
        }
        else if(brand == 'Volkswagen')
        {
  			bid= api.businessAccountSearch();
            if(bid!=null && bid != '')
            {
                ba= [select id from account where id=:bid];
            }
        }
      return ba;
}
public Contact findBContact(String companyName, String abn, String email, String mobile, String brand, String firstName, String lastName)
{
     
        Muleapi api = new Muleapi();       
        api.email_address = email;
        api.first_name =firstName;
        api.last_name =lastName;
        api.brand = brand;
        api.company_name = companyName;
        api.phone = mobile;
        api.ABN = abn;
        String bcid;
        Contact bc= null;    
        if(brand== 'Skoda') 
        {
            bcid= api.businessContactSearch();
            if(bcid!=null && bcid != '')
            {                         
                bc= [select id, firstName, lastName, MobilePhone, Email from contact where id=:bcid];
            }
        }
        else if(brand== 'Volkswagen')
        {
        	bcid= api.businessContactSearch();
        	if(bcid!=null && bcid != '')
        	{
            	bc= [select id, firstName, lastName, MobilePhone, Email from contact where id=:bcid];
        	}
      	}      
        return bc;
}  
public Account findAccount(String firstName, String lastName, String email, String mobile, Boolean isOwner, String brand, string state, string postCode)
{
    List<Account> accountList = new List<Account>(); 
    Muleapi api = new Muleapi();
    api.first_name =firstName;
    api.last_name =lastName;
    api.mobile_phone = mobile;
    api.email_address = email;
    api.region_code =state ;
    api.post_code = postCode;
    api.brand = brand;
   
    String pid;
    String bid;
    Account a= null;
    if(brand== 'Skoda') {
   
       pid= api.personAccountSearch();
        if(pid!=null && pid != '')
        {
            a= [select id,VGA_Owner__c from account where id=:pid];
        }
    }       
    if(brand== 'Volkswagen'){
  
      pid= api.personAccountSearch();
      if(pid!=null && pid != '')
      {
         a= [select id,VGA_Owner__c from account where id=:pid];
      }
    }
    return a; 
   
}
public String fixDateFormat(String inDate) 
{
  //This will convert dd-mm-yyyy to yyyy-mm-dd
  String outDate = '';
  List<String> parts = inDate.split('-');
  if(parts.size() != 3) {
    return Null;
  }
  outDate = parts[2] + '-' + parts[1] + '-' + parts[0];
  if(outDate.length() != 10) {
    return Null;
  }
  return outDate;
}
    
/**
* @author Archana Yerramilli
* @date  22/01/2018
* @description As per ticket#633, a new Amendment type is added and new fields are to be saved to case object. 
* @description Newly added fields will be saved for amendment type "I have sold my vehicle" for both Skoda and VW. 
* @param Case List
*/
public PageReference submit() 
{
    string message = '';
    string queueName = '';
    try
    {
        if(attachment_ppsr != null)
        {
        	allAttachment.add(attachment_ppsr);
        }
        if(attachment_insurance != null)
        {
        	allAttachment.add(attachment_insurance);
        }  
        //check if home or work numbers were given, if they are provided, move to mobile
        if(mobile==null || mobile.replace(' ', '') =='' )
        {
            //check if home number was provided
            if (homeNumber!=null && homeNumber.replace(' ', '')!='')
            {
                mobile = homeNumber;
            }
            else if (workNumber!=null && workNumber.replace(' ', '')!='')
            {
                mobile = workNumber;
            }
        }
        //get recordTypes
        //person Accounts
        Id skodaPersonAccountRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
        Id vwPersonAccountRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();  
        //Fleet Accounts
        Id skodaFleetCustomerRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();                   
        Id vwFleetCustomerRType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();  
        //Fleet Contacts
        Id skodaFleetContactRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Fleet Customer Contact').getRecordTypeId();   
        Id vwFleetContactRType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Fleet Customer Contact').getRecordTypeId();  
        //case types
        Id enquiryRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enquiry').getRecordTypeId();                 
        Id ownershipRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Owner Amendment').getRecordTypeId();                      
        Account a, ba;
        Contact bc;   
        //flags
        Boolean ownerFlag = false; //owner or not
        Boolean companyFlag = false; //company infomation provided or not
        //if not general enquiry, set ownerflag to true
        if(selectedRequestType != 'General Enquiry') {  
           ownerFlag = true;
        }
        //if company name and ABN provided, set companyFlag
        if(companyName!= null && abn!= null && abn!='' && companyName!='')
        {
           companyFlag = true;
        }     
        //if company, search for business account and business contact
        if(companyFlag)
        {
            ba= findBAccount(companyName, abn, email, mobile.replace(' ', ''), ownerFlag, brand, state, postCode);
            bc= findBContact(companyName, abn, email, mobile.replace(' ', ''), brand, firstName, lastName); 
        }
        else
        {
            //search for personAccount
           a = findAccount(firstName, lastName, email, mobile.replace(' ', ''), ownerFlag, brand, state, postCode);
        }
        //businessAccount
        if(companyFlag)
        {
            if(ba == null ) 
            {
                System.debug('Business Account doesnt exist');
                ba = new Account();                
                if(brand == 'Skoda') {
                    ba.RecordTypeId = skodaFleetCustomerRType;      
                }    
                else {        
                    ba.RecordTypeId = vwFleetCustomerRType;
                }       
                //set company Name details
                ba.Name                 = companyName;
                ba.VGA_ABN__c           = abn;       
                ba.VGA_Brand__c         = brand;                
                //call insert
                Database.SaveResult SR  = Database.insert(ba, true);
                //check status
                if(!SR.isSuccess()) {              
                    System.debug(LoggingLevel.Error, 'ERROR: ' + SR);
                }
                System.debug('Account inserted ' + ba.id);    
            }
            
            //business contact
            if (bc==null)
            {
                //business contact exists
                System.debug('Business Contact dont exist');
                bc = new Contact();
                //set record type
                if(brand == 'Skoda') {
                    bc.RecordTypeId = skodaFleetContactRType;
                }
                else {
                    bc.RecordTypeId = vwFleetContactRType;  
                }      
                //map other fields 
                bc.AccountId        = ba.id;
                bc.FirstName        = firstName;
                bc.LastName         = lastName;
                bc.Salutation       = selectedTitleType;
                bc.Email            = email;
                bc.MobilePhone      = mobile.replace(' ', '');
                bc.VGA_Brand__c     = brand;
                
                //call insert
                Database.SaveResult SR = Database.insert(bc, true);
                //check status
                if(!SR.isSuccess()) 
                {              
                  System.debug(LoggingLevel.Error, 'ERROR: ' + SR);
                }       
                System.debug('Business Contact inserted ' + bc.id);
            }
            else
            {
                //business contact exists           
                //first name, last name and email matches   
                if(bc.FirstName  == firstName && 
                   bc.LastName   == lastName      && 
                   bc.Email      == email
                   ) {
                    //update mobile
                    bc.MobilePhone      = mobile.replace(' ', '');  
                }      
                //first name, last name and email matches
                if(bc.FirstName   == firstName && 
                   bc.LastName    == lastName  && 
                   bc.MobilePhone == mobile.replace(' ', '')
                   ) { 
                    //update email
                    bc.Email            = email;
                }      
                //call update
                Database.SaveResult SR = Database.update(bc, False);
                //check status
                if(!SR.isSuccess()) 
                {
                    System.debug(LoggingLevel.Error, 'ERROR: ' + SR);
                }           
            System.debug('Business Contact updated');                       
            }        
        }
        else{
            //person account
            if (a==null)
            {
                System.debug('Person Account dont exist');
                a = new Account();
                if(brand == 'Skoda') 
                {
                    //skoda record types
                    a.RecordTypeId = skodaPersonAccountRType;
                }
                else
                {
                    a.RecordTypeId = vwPersonAccountRType;
                }
                //demographic details
                a.Salutation                            = selectedTitleType;
                a.FirstName                             = firstName;
                a.LastName                              = lastName;     
                a.PersonEmail                           = email;
                a.VGA_Email_Address_Valid__c            = True;
                a.PersonMobilePhone                     = mobile.replace(' ', '');
                a.VGA_Phone_Valid__c                    = True;
                a.VGA_Marketing_Opt_In__pc              = Boolean.valueOf(receiveEmail);
                a.VGA_News_Innovations_Flag__c          = Boolean.valueOf(receiveEmail);
                a.VGA_Vehicle_Launches_Flag__c          = Boolean.valueOf(receiveEmail);
                a.VGA_Promotions_Flag__c                = Boolean.valueOf(receiveEmail);
                a.VGA_Sponsorship_out__c                = Boolean.valueOf(receiveEmail);
                a.VGA_Events_opt_out__c                 = Boolean.valueOf(receiveEmail);
                a.VGA_Welcome_experience_On_boarding__c = Boolean.valueOf(receiveEmail);
                a.VGA_Brand__pc                         = brand;
                a.VGA_Brand__c                          = brand;
                //if owner, set addresss details    
                if(selectedRequestType != 'General Enquiry') {
                    a.PersonMailingStreet               = postalAddress;
                    a.PersonMailingCity                 = suburb;
                    //       a.PersonMailingStateCode = state;
                    a.PersonMailingPostalCode           = postcode;
                    a.BillingStreet                     = postalAddress;
                    a.BillingCity                       = suburb;
                    a.BillingPostalCode                 = postcode;
                    a.BillingState                      = state;
                    a.VGA_DPID__c                       = dpid;           
                }   
                Database.SaveResult SR = Database.insert(a, true);        
                if(!SR.isSuccess()) 
                {              
                  System.debug(LoggingLevel.Error, 'ERROR: ' + SR);
                }
                System.debug('Account inserted ' + a.id);
            }
           else{
                //person account exists
                a.Salutation                                = selectedTitleType;
                a.FirstName                                 = firstName;
                a.LastName                                  = lastName;
                a.PersonMobilePhone                         = mobile.replace(' ', '');  
                a.PersonEmail                               = email;
                a.BillingStreet                             = postalAddress;
                a.BillingCity                               = suburb;
                a.BillingPostalCode                         = postcode;   
                a.BillingState                              = state;      
                a.VGA_Email_Address_Valid__c                = True;
                a.VGA_Phone_Valid__c                        = True;             
                //if opted into email, set all flags - **why are we setting all flags here?
                if(Boolean.valueOf(receiveEmail)== True){
                    a.VGA_Marketing_Opt_In__c               = Boolean.valueOf(receiveEmail);      
                    a.VGA_News_Innovations_Flag__c          = Boolean.valueOf(receiveEmail);
                    a.VGA_Vehicle_Launches_Flag__c          = Boolean.valueOf(receiveEmail);
                    a.VGA_Promotions_Flag__c                = Boolean.valueOf(receiveEmail);
                    a.VGA_Sponsorship_out__c                = Boolean.valueOf(receiveEmail);
                    a.VGA_Events_opt_out__c                 = Boolean.valueOf(receiveEmail);
                    a.VGA_Welcome_experience_On_boarding__c = Boolean.valueOf(receiveEmail);
                }
                
                //update record if non owner
                if (a.VGA_Owner__c !='Yes')
                {
                    Database.SaveResult SR = Database.update(a, true);        
                    if(!SR.isSuccess()) {              
                      System.debug(LoggingLevel.Error, 'ERROR: ' + SR);
                    }
                    System.debug('Account updated ' + a.id);
                }
           }//person account exists
        }//person account
        
        //insert case    
        Case c = new Case();    
        if(companyFlag)
        { 
        // if business contact exists and person account doesnt exist
            c.AccountId = ba.Id;
            c.ContactId = bc.Id;
        }
        else
        {
            c.AccountId = a.Id; 
        }                   
        
        c.VGA_Web_Form_VIN__c           = vin;
        c.VGA_Web_Form_REGO__c          = rego;
        c.SuppliedEmail                 = email;
        c.Origin                        = 'VW Website';
        c.VGA_Web_Form_Postcode__c      = postCode;
        c.VGA_Web_Form_Postal_Address__c= postalAddress;
        c.VGA_Web_Form_Suburb__c        = suburb;
        c.VGA_Web_Form_State__c         = state;
        c.VGA_Web_Form_Email__c         = email;
        c.Contact_Me_Via__c             = selectedContactMeViaList;
        c.VGA_Web_Form_Mobile__c        = mobile.replace(' ', '');
        c.VGA_Marketing_Opt_In__c       = Boolean.valueOf(receiveEmail);   
        //check if VIN exists    
        if(vin==null|| vin.equals('')){
            c.AssetId   = null;
        }
        else {
           
            //get a list of VINs
            List<Asset> vinId;
            vinId       = [SELECT Id FROM Asset WHERE VGA_VIN__c = :vin];
            //if vin matches, map VIN
            if(vinId.size() > 0) {    
                c.AssetId = vinId[0].Id; 
            }  
        }   
        //set orgin and queue
        if(brand == 'Skoda') {
            queueName           = 'Web - Skoda';
            c.Origin            = 'Skoda Website';
        }
        else{
            queueName    = 'Web - VW';
            c.Origin            = 'VW Website';
        }
        //set owner flag
        if(brand == 'Skoda') {
            c.VGA_Are_you_a_Skoda_Owner__c = ownerFlag;  
        } 
        else {        
            c.VGA_Are_you_a_Volkswagen_Owner__c = ownerFlag;
        }  
        //set case fields
        if(selectedRequestType == 'General Enquiry') 
        {    
            //general
            c.VGA_Description__c        = description;
            c.VGA_Product__c            = selectedEnquiryAbout;
            c.Contact_Me_Via__c         = selectedContactMeViaList;
            c.recordTypeId              = enquiryRecordType; // Enquiry type
            if(brand == 'Skoda') {
                if(selectedSkodaModelList != '--None--' && selectedSkodaModelList != Null) {
                    c.Skoda_Model__c    = selectedSkodaModelList;
                }          
            }
            else {
                if(selectedVWModelList != '--None--' && selectedVWModelList != Null) {
                    c.VW_Model__c       = selectedVWModelList;
                }
            }
            c.Interaction_Type__c = 'Enquiry';                                 
        }//general enquiry
        else 
        {
        //Ownership amendment   
          if(brand == 'Skoda') {
                c.VGA_Brand__c              = 'Skoda';
          }
          else {
                //for VW vehicles, check if it's PV or CV
                List<Product2> vehicleList = [SELECT VGA_Brand__c,VGA_Sub_brand__c 
                                                FROM Product2 
                                               WHERE VGA_Brand__c = :brand];
                //vehicle exists
                if(vehicleList.size() > 0) {
                    if(vehicleList[0].VGA_Sub_brand__c == 'Passenger') {
                        c.VGA_Brand__c = 'Volkswagen PV';
                    }
                    else {
                        c.VGA_Brand__c = 'Volkswagen CV';
                    }
                }
          }
          c.recordTypeId                    = ownershipRecordType; 
          c.Interaction_Type__c             = 'Ownership Amendment';
          c.Ownership_Amendment_Purpose__c  = selectedAmendmentType;         
          c.VGA_Migration_Details__c        = companyName;
          if(selectedAmendmentType == 'Vehicle written off and completely destroyed') 
          {               
                c.VGA_Date_Written_Off__c   = Date.valueOf(fixDateFormat(dateWrittenOff));
                c.VGA_Web_Form_Email__c     = email;
                c.VGA_Web_Form_Mobile__c    = mobile.replace(' ', '');
          }
          else if (selectedAmendmentType == 'Vehicle reported stolen and not recovered') 
          {
                c.VGA_Date_Stolen__c        = Date.valueOf(fixDateFormat(datestolen));
                c.VGA_Web_Form_Email__c     = email;
                c.VGA_Web_Form_Mobile__c    = mobile.replace(' ', '');
          }
          else if (selectedAmendmentType == 'Vehicle Exported Overseas') 
          {
                c.VGA_Date_Exported__c      = Date.valueOf(fixDateFormat(dateExported));
                c.VGA_Web_Form_Email__c     = email;
                c.VGA_Web_Form_Mobile__c    = mobile.replace(' ', '');
          }
          else if (selectedAmendmentType == 'Purchased Used Vehicle') 
          {
                c.VGA_Date_of_Sale__c       = Date.valueOf(fixDateFormat(dateOfSale));
                c.VGA_Kilometers__c         = Decimal.valueOf(kilometer);
                c.VGA_Drivers_License_Number__c = licenseNumber;
                c.VGA_Licence_State__c      = licenseState;
                c.VGA_Date_of_Birth__c      = Date.valueOf(fixDateFormat(dateOfBirth));
                c.VGA_Web_Form_Email__c     = email;
                c.VGA_Web_Form_Mobile__c    = mobile.replace(' ', '');        
          }  
          else if (selectedAmendmentType == 'Change Address / Contact Details / Registration Details') 
          {
                c.VGA_Date_of_Change__c     = Date.valueOf(fixDateFormat(dateOfChange));
                c.VGA_Web_Form_Email__c     = email;
                c.VGA_Web_Form_Mobile__c    = mobile.replace(' ', '');
          }
          else if (selectedAmendmentType == 'Sold Vehicle') 
          {
                c.VGA_Date_Sold__c            = Date.valueOf(fixDateFormat(dateSold));
                c.VGA_New_Owner_First_Name__c = NewOwnerFirstName;
                c.VGA_New_Owner_Last_Name__c  = NewOwnerLastName;
                c.VGA_New_Owner_Mobile__c     = NewOwnerMobile.replace(' ', '');
                c.VGA_New_Owner_Email__c      = NewOwnerEmail;
          }            
        }//ownership amendment
        //insert case
        Database.SaveResult SR = Database.insert(c, true);
        if(!SR.isSuccess()) 
        {
            System.debug('CASE_ERROR: ' + SR);
        }                 
        System.debug('Case inserted '+ c.Id);    
        //add attachments
        if(attachment_ppsr.name != null) {               
            attachment_ppsr.OwnerId     = UserInfo.getUserId();
            attachment_ppsr.ParentId    = c.Id; // the record the file is attached to
            attachment_ppsr.IsPrivate   = false;            
            Database.SaveResult SR0     = Database.insert(attachment_ppsr, true);
        }  
        if(attachment_insurance.name != null) {               
            attachment_insurance.OwnerId    = UserInfo.getUserId();
            attachment_insurance.ParentId   = c.Id; // the record the file is attached to
            attachment_insurance.IsPrivate  = false;
            Database.SaveResult SR1         = Database.insert(attachment_insurance, true);
        } 
        //reset attachment object
        attachment_ppsr                     = null;
        attachment_insurance                = null;  
        System.debug('Attachment(s) inserted');
        
        List<QueueSobject> queue = [SELECT id, queueid FROM queuesobject WHERE queue.name = :queueName];
        if(queue.size() > 0) 
        {
            c.OwnerId           = queue[0].queueid;            
        }         
        update c;        
        //redirect based on brand 
        if(brand == 'Skoda') 
        {
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaThankYou');
        }
        else 
        {
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenThankYou');
        } 
        //end of account(s) and case processing
    }
    catch(Exception e)
    {
        system.debug('Exception: ' + e.getMessage() +'at line number ' + e.getLineNumber());
        //redirect to thank you page
        sendExceptionEmail('Exception in contact us form', 'Hi, <br/><br/> Error in Contact Us form : <br/><br/>' + e.getMessage() +'at line number ' + e.getLineNumber() + '<br/><br/>Form content was: '+this.prepareMessage()+'Thanks<br/>Salesforce Support',allAttachment);
        if(brand == 'Skoda') {
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaThankYou');
        }
        else {
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenThankYou');
        }        
    }
 }  
public string prepareMessage() 
{
        string str = '<br>';        
        for(string key: ApexPages.currentPage().getParameters().keySet())
        {
            str = str + ' ' + key + ' : ' + ApexPages.currentPage().getParameters().get(key) + ' <br>';
        }        
        return str;
}     
public static void sendExceptionEmail(string strSubject,String strHtmlBody,List<Attachment> allAttachments)
{
        if(Label.VGA_Exception_Email_Address != Null)  
        {
             List<String> lstaddress = Label.VGA_Exception_Email_Address.split(',');             
             list<OrgWideEmailAddress> lstOrgEmailAddress = [select Id,DisplayName,Address from OrgWideEmailAddress where DisplayName =:'VGA Notifications'];
             if(lstaddress != null && !lstaddress.isEmpty())
             {
                 Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                 List<Messaging.EmailFileAttachment> lstEmailAttachments = new List<Messaging.EmailFileAttachment>();
                 message.toAddresses = lstaddress;
                 if(lstOrgEmailAddress != null && !lstOrgEmailAddress.isEmpty())
                 {
                    message.setOrgWideEmailAddressId(lstOrgEmailAddress[0].id); 
                 }
                 
                 message.subject = strSubject;
                 message.HtmlBody = strHtmlBody;
                 List<Messaging.Emailfileattachment> efaList = new List<Messaging.Emailfileattachment>();
                 if(allAttachments !=null)
                 {
                    
                       	for (Integer i = 0; i < allAttachments.size(); i++)
                        {
                            if(allAttachments[i].Name != null)
                            {
                                system.debug('Name of the attachment: '+allAttachments[i].Name);
                                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                                efa.setFileName(allAttachments[i].Name);
                                efa.setBody(allAttachments[i].Body);
                                efa.setContentType(allAttachments[i].ContentType);
                                efaList.add( efa );
                            }
                        }
                 }
                 if(efaList !=null)
                 {
                      message.setFileAttachments(efaList );
                 }
                                
                 Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                 try
                 {
                 if(!test.isRunningTest()) 
                    Messaging.sendEmail(messages);
                }
                catch(exception e)
                {
                    system.debug('@@@@'+e.getmessage());
                }
             }
        }
    }
}