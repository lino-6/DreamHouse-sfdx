@isTest
global with sharing class VWExtensionControllerUnitTest {
    
    static testMethod void VWExtensionControllerTest() {
        BigTestData td = createTestData();

        punosmobile__Meeting__c m = td.meetings[0];
        ApexPages.StandardController sc = new ApexPages.StandardController(m);
        VWExtensionController ctrl = new VWExtensionController(sc);
        
        system.assertNotEquals(null, ctrl);
    }
    
    static testMethod void VWOpenTasksUnitTest() {
        
        BigTestData td = createTestData();

        punosmobile__Meeting__c m = td.meetings[0];
        
        PageReference openTasks = Page.PreparationExtensionSlotOne;
        ApexPages.StandardController sc = new ApexPages.StandardController(m);
        
        openTasks.getParameters().put('id', m.Id);
        openTasks.getParameters().put('showmore', 'true');
        openTasks.getParameters().put('agendaTemplate', 'true');
        Test.setCurrentPage(openTasks);
        
        
        VWExtensionController ctrl = new VWExtensionController(sc);
        
        system.assertEquals(0, ctrl.rowsLimit);
        system.assertEquals(true, ctrl.useAgendaTemplate);
    	
    }
    
    // Tests end here
    // Create test data
    private static BigTestData createTestData() {
        Account account = new Account(Name = 'Test account');
        
        List<RecordType> recordTypeList = [SELECT Id From RecordType WHERE DeveloperName=:'Volkswagen_Dealer_Account' AND SobjectType=:'Account' LIMIT 1];
        
        String recordTypeID;
        
        if(!recordTypeList.isEmpty()){
            recordTypeID = recordTypeList[0].Id;
            account.put('RecordTypeId', recordTypeID);
        }    
            
        insert account;

        Contact contact = new Contact(AccountId = account.ID, LastName = 'last name');
        insert contact;

        punosmobile__Meeting_Assistant_Agenda__c agenda = new punosmobile__Meeting_Assistant_Agenda__c(punosmobile__Status__c ='Active');
        insert agenda;

        List<punosmobile__Agenda_Item__c> agendaPoints = new List<punosmobile__Agenda_Item__c>{
            new punosmobile__Agenda_Item__c(punosmobile__Agenda__c = agenda.Id, punosmobile__Related_Object__c = 'Account', punosmobile__Object_Relation_Status__c = 'Answers will have linkage to object'),
            new punosmobile__Agenda_Item__c(punosmobile__Agenda__c = agenda.Id),
            new punosmobile__Agenda_Item__c(punosmobile__Agenda__c = agenda.Id)
        };
        insert agendaPoints;

        map<String,ID> profiles = new Map<String,ID>();
            List<Profile> ps = [select id, name from Profile where name =
               'Standard User' or name = 'System Administrator'];
            for(Profile p : ps){profiles.put(p.name, p.id);}

        User newUser = new User(alias = 'NewTest',
            email='standassrduser@testorg.com',
            emailencodingkey='UTF-8',
            profileid = profiles.get('Standard User'),
            lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US',
            timezonesidkey='America/Los_Angeles',
            username='standassrduser@TestPunos.com');
            insert newUser;
        
        Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact');
        insert testContact;
        
        String contactID = String.valueOf(testContact.Id).substring(0, 15);
        
        Event testEvent = new Event(Subject = 'Test event', WhoId = contactID, StartDateTime = Date.today(), EndDateTime = Date.today() + 1);
        insert testEvent;
        
        Account testAccount = new Account(Name = 'test account');
        if(!recordTypeList.isEmpty()){
			recordTypeID = recordTypeList[0].Id;
            testAccount.put('RecordTypeId', recordTypeID);
		}
        insert testAccount;

        List<punosmobile__Meeting__c> meetings = new List<punosmobile__Meeting__c>{
            new punosmobile__Meeting__c(Name = 'meeting1',
                OwnerId = newUser.Id,
                punosmobile__Description__c = 'Descriptionsdasada',
                punosmobile__Meeting_Status__c = 'Agreed',
                punosmobile__Agenda_Template__c = agenda.Id,
                punosmobile__Event__c = testEvent.Id,
                punosmobile__Related_To__c = testAccount.Id),
            new punosmobile__Meeting__c(Name = 'meeting2'),
            new punosmobile__Meeting__c(Name = 'meeting3')
        };
        insert meetings;
		
        List<Task> tasks = new List<Task>{
            new Task(OwnerId = newUser.Id, Subject = 'Test task 1', Priority = 'Normal', WhoId = contactID, Status = 'Completed', punosmobile__Meeting_ID__c=meetings[0].Id),
            new Task(OwnerId = newUser.Id, Subject = 'Test task 2', Priority = 'Normal', WhoId = contactID, Status = 'Open', WhatId = meetings[0].punosmobile__Related_To__c)
        };
        List<RecordType> taskRecordTypeList = [SELECT Id From RecordType WHERE DeveloperName=:'New_Tasks' AND SobjectType=:'Task' LIMIT 1];
        
        String taskRecordTypeID;
        
        if(!taskRecordTypeList.isEmpty()){
           taskRecordTypeID = taskRecordTypeList[0].Id;
        //    account.put('RecordTypeId', recordTypeID);
        //    
        }    
        for(Task task : tasks){
            task.put('RecordTypeId', taskRecordTypeID);
        }
        insert tasks;

        List<punosmobile__Meeting_Agenda_Item__c > meetingPoints = new List<punosmobile__Meeting_Agenda_Item__c>{
            new punosmobile__Meeting_Agenda_Item__c(punosmobile__Meeting__c = meetings[0].Id, punosmobile__Related_Record_ID__c = account.Id, punosmobile__Agenda_Item_Number__c=1, punosmobile__Agenda_Item__c = agendaPoints[0].Id),
            new punosmobile__Meeting_Agenda_Item__c(punosmobile__Meeting__c = meetings[0].Id, punosmobile__Related_Record_ID__c = account.Id, punosmobile__Agenda_Item_Number__c=2, punosmobile__Agenda_Item__c = agendaPoints[0].Id),
            new punosmobile__Meeting_Agenda_Item__c(punosmobile__Meeting__c = meetings[0].Id, punosmobile__Related_Record_ID__c = account.Id, punosmobile__Agenda_Item_Number__c=3, punosmobile__Agenda_Item__c = agendaPoints[0].Id)
        };
        insert meetingPoints;

        List<punosmobile__Meeting_Resources__c> meetingResources = new List<punosmobile__Meeting_Resources__c>{
            new punosmobile__Meeting_Resources__c(punosmobile__Meeting__c = meetings[0].Id, punosmobile__Status__c = 'Owner'),
            new punosmobile__Meeting_Resources__c(punosmobile__Meeting__c = meetings[0].Id, punosmobile__Status__c = 'Invited')
        };
        insert meetingResources;

        /*
        List<punosmobile__Agenda_Item_Question__c> aiq = new List<punosmobile__Agenda_Item_Question__c>{
                   new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Dynamic Value',
                        punosmobile__Field_Name__c = 'Name',
                        punosmobile__Input_Type__c = 'Text Input',
                        punosmobile__Question__c = 'DIS IS QUESTION',
                        punosmobile__Related_Object__c = account.Id),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Dynamic Value',
                        punosmobile__Field_Name__c = 'Name',
                        punosmobile__Input_Type__c = 'Number Input',
                        punosmobile__Question__c = 'DIS IS QUESTION',
                        punosmobile__Related_Object__c = account.Id),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Checkbox',
                        punosmobile__Input_Type__c = 'Checkbox',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Multi Select',
                        punosmobile__Input_Type__c = 'Multi Select',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Dropdown List',
                        punosmobile__Input_Type__c = 'Dropdown List',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Slider',
                        punosmobile__Input_Type__c = 'Date',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Slider',
                        punosmobile__Input_Type__c = 'Percent Input',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                    new punosmobile__Agenda_Item_Question__c(
                        punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                        punosmobile__Answer_Type__c = 'Slider',
                        punosmobile__Input_Type__c = 'Slider',
                        punosmobile__Question__c = 'DIS IS QUESTION'
                       ),
                       new punosmobile__Agenda_Item_Question__c(
                           punosmobile__Agenda_Item__c = agendaPoints[0].Id,
                           punosmobile__Answer_Type__c = 'Field From Account',
                           punosmobile__Input_Type__c = 'Text Input',
                           punosmobile__Question__c = 'DIS IS QUESTION'
                       )
        };
        insert aiq;

       punosmobile__Meeting_Question__c SQ = new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id);
       insert SQ;

       List<punosmobile__Meeting_Question__c> SQs = new List<punosmobile__Meeting_Question__c>{
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[0].Id, punosmobile__Meeting_Input_Type__c = 'Text Input', punosmobile__Answer_Text__c ='text input text test'),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[1].Id, punosmobile__Meeting_Input_Type__c = 'Number Input', punosmobile__Answer_Number__c =2),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[2].Id, punosmobile__Meeting_Input_Type__c = 'Checkbox', punosmobile__Answer_Checkbox__c = true),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[3].Id, punosmobile__Meeting_Input_Type__c = 'Multi Select', punosmobile__Answer_Options__c = 'true'),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[4].Id, punosmobile__Meeting_Input_Type__c = 'Dropdown List', punosmobile__Answer_Selected_Options__c = 'Option1'),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[5].Id, punosmobile__Meeting_Input_Type__c = 'Slider', punosmobile__Answer_Slider__c = 12),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[6].Id, punosmobile__Meeting_Input_Type__c = 'Date', punosmobile__Answer_Slider__c = 12),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_Item_Question__c = aiq[7].Id, punosmobile__Meeting_Input_Type__c = 'Percent Input', punosmobile__Answer_Slider__c = 12),
            new punosmobile__Meeting_Question__c(punosmobile__Meeting_Agenda_Item__c = meetingPoints[0].Id, punosmobile__Agenda_item_Question__c = aiq[8].Id, punosmobile__Meeting_Input_type__c = 'Text Input', punosmobile__Answer_Text__c ='testertext')
        };
        insert SQs;
*/
        //return new BigTestData(account, contact, meetings, meetingPoints, SQs, meetingResources, tasks);
        return new BigTestData(account, contact, meetings, meetingPoints, meetingResources, tasks);
    }

    public class BigTestData {
        public Account account {get;set;}
        public Contact contact {get;set;}
        public List<punosmobile__Meeting__c> meetings  {get;set;}
        public List<punosmobile__Meeting_Agenda_Item__c> meetingPoints { get; set; }
        //public List<punosmobile__Meeting_Question__c> SQs {get;set;}
        public List<punosmobile__Meeting_Resources__c> meetingResources {get;set;}
        public List<Task> tasks {get;set;}

        //public BigTestData(Account account, Contact contact, List<punosmobile__Meeting__c> meetings, List<punosmobile__Meeting_Agenda_Item__c> meetingPoints, List<punosmobile__Meeting_Question__c> SQs, List<punosmobile__Meeting_Resources__c> meetingResources, List<Task> tasks) {

        public BigTestData(Account account, Contact contact, List<punosmobile__Meeting__c> meetings, List<punosmobile__Meeting_Agenda_Item__c> meetingPoints, List<punosmobile__Meeting_Resources__c> meetingResources, List<Task> tasks) {
                       
            this.account = account;
            this.contact = contact;
            this.meetings = meetings;
            this.meetingPoints = meetingPoints;
            //this.SQs = SQs;
            this.meetingResources = meetingResources;
            this.tasks = tasks;
        }
    }
    
}