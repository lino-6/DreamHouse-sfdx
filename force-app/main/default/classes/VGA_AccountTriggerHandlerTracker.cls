@isTest
public class VGA_AccountTriggerHandlerTracker 
{
   public static Account objAccount;
     public static Account objAccount2;
     public static VGA_Customer_Order_Details__c objCOD;
     public static VGA_Ownership__c objOwnership; 
   public static testMethod void Test1()
   {
    LoadData();
    
    insert objAccount;
        
         insert objAccount2;
         
        objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount2.Id;
        objCOD.VGA_Order_Date__c = system.today();
        insert objCOD;
         
        objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount2.Id;
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership;
         
        objAccount2.VGA_Email_Validation__c = 'Not Provided';
        update objAccount2; 
   }
    public static testMethod void Test2()
   {
    LoadData();
    
    insert objAccount2;
         
        objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount2.Id;
        objCOD.VGA_Order_Date__c = system.today();
        insert objCOD;
         
        objAccount2.VGA_Email_Validation__c = 'Not Provided';
        update objAccount2; 
   }
    public static testMethod void Test3()
   {
    LoadData();
    
    insert objAccount2;
         
         objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount2.Id;
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership;
         
        objAccount2.VGA_Email_Validation__c = 'Not Provided';
        update objAccount2; 
         objAccount2.VGA_Email_Validation__c = 'valid';
        update objAccount2;
   }
    public static testMethod void Test3_1()
   {
    LoadData();
    
    insert objAccount2;
         
         objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount2.Id;
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership;
         
         objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount2.Id;
        objCOD.VGA_Order_Date__c = system.today();
        insert objCOD;
         
        objAccount2.VGA_Email_Validation__c = 'Not Provided';
        update objAccount2; 
         objAccount2.VGA_News_Innovations_Flag__c = true;
         objAccount2.VGA_Email_Validation__c = 'valid';
        update objAccount2;
   }
    public static testMethod void Test4()
   {
    LoadData();
    
    insert objAccount;
         objAccount.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
         update objAccount;
        
   }
    
   public static void LoadData()
   {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Account';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom; 
         
    objAccount = new Account();
    objAccount.name  = 'Test';
    objAccount.VGA_Brand__c = 'Volkswagen';
    objAccount.VGA_Sub_Brand__c = 'Passenger Vehicles (PV);Commercial Vehicles (CV)';
        objAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Dealer Account').getRecordTypeId(); 
         
        objAccount2 = new Account();
        objAccount2.LastName = 'Test';
        objAccount2.PersonEmail = 'Test@test.com';
        objAccount2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        objAccount2.VGA_Owner__c = 'Yes';
    
   }
}