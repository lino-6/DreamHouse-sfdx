global class VGA_updateAccountSubscriptions implements Database.Batchable<sObject>
{
           
            
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strAccountQuery = 'select Id,VGA_Subscription_Batch__c,personEmail,VGA_News_Innovations_Flag__c,VGA_Brand__c,VGA_Vehicle_Launches_Flag__c,VGA_Welcome_experience_On_boarding__c,VGA_Sponsorship_out__c,VGA_Promotions_Flag__c,VGA_Events_opt_out__c,VGA_Vehicle_Preference_Flag__c,Unsubscription_Reason__c from Account';
        strAccountQuery += ' where VGA_Subscription_Batch__c=true and VGA_Batch__c=false and personEmail != null and VGA_Brand__c != null order by lastmodifiedDate desc'; 
        return Database.getQueryLocator(strAccountQuery);
    }
    global void execute(Database.BatchableContext BC, List<Account> lstAccount)
    {
            set<string> setofEmail = new set<String>();
            set<string> setofBrand = new set<String>();
            set<ID> setofAccountID = new set<ID>();
            list<Account> lstAccount2Update = new list<Account>();
        if(lstAccount != null && !lstAccount.isEmpty())
        {       
            map<string,list<Account>> mapofUniqueAccount = new map<string,list<Account>>();
            
            for(Account objAccount : lstAccount)
            {
                    string strkey = '';
                    strkey = objAccount.personEmail+'@'+objAccount.VGA_Brand__c;
                    if(!mapofUniqueAccount.containskey(strkey))
                        mapofUniqueAccount.put(strkey,new list<Account>());
                        mapofUniqueAccount.get(strkey).add(objAccount);
                       
            }
            if(mapofUniqueAccount != null && !mapofUniqueAccount.isEmpty())
            {
                map<string,Account> mapofAccount = new map<string,Account>();
                for(list<Account> outerlist : mapofUniqueAccount.values())
                {
                    for(Account objAccount : outerlist)
                    {
                        setofAccountID.add(objAccount.id);
                        setofEmail.add(objAccount.personEmail);
                        setofBrand.add(objAccount.VGA_Brand__c);
                        
                        string strkey = '';
                        strkey = objAccount.personEmail+'@'+objAccount.VGA_Brand__c;
                        
                        if(strkey != null)
                        {
                            if(!mapofAccount.containskey(strkey))
                                mapofAccount.put(strkey,objAccount);
                        }
                    }
                }
              //  setofEmailExclution = Subscription_Exclusion_List__c.getInstance();
                  
               
                if(setofAccountID != null && !setofAccountID.isEmpty() && setofEmail != null && !setofEmail.isEmpty() && setofBrand != null && !setofBrand.isEmpty() && !setofEmail.contains('noemail') )
                {
                    map<string,list<Account>> mapofAccounttoprocess = new map<string,list<Account>>();
                 /*   Map<id,account> aMap = new Map<id,account>([select Id,VGA_News_Innovations_Flag__c,PersonEmail,VGA_Brand__c,
                                                VGA_Vehicle_Launches_Flag__c,VGA_Welcome_experience_On_boarding__c,VGA_Sponsorship_out__c,
                                                VGA_Promotions_Flag__c,VGA_Events_opt_out__c,VGA_Vehicle_Preference_Flag__c,Unsubscription_Reason__c
                                                from Account where VGA_Brand__c  IN: setofBrand and PersonEmail IN: setofEmail and ID NOT IN: setofAccountID ]);*/
                    
                    for(Account objAcc : [select Id,VGA_News_Innovations_Flag__c,PersonEmail,VGA_Brand__c,
                                                VGA_Vehicle_Launches_Flag__c,VGA_Welcome_experience_On_boarding__c,VGA_Sponsorship_out__c,
                                                VGA_Promotions_Flag__c,VGA_Events_opt_out__c,VGA_Vehicle_Preference_Flag__c,Unsubscription_Reason__c
                                                from Account where VGA_Brand__c  IN: setofBrand and PersonEmail IN: setofEmail and ID NOT IN: setofAccountID 
                                                ])
                       // As per best practise
                       
                       
                        // for(account objAcc : aMap.values())                       
                                                
                                                
                    {
                        string strKey = '';
                        strKey = objAcc.personEmail+'@'+objAcc.VGA_Brand__c;
                        if(strKey != '')
                        {
                            if(mapofAccount.containskey(strKey) && mapofAccount.get(strKey) != null)
                            {
                                objAcc.VGA_News_Innovations_Flag__c=mapofAccount.get(strKey).VGA_News_Innovations_Flag__c;
                                objAcc.VGA_Vehicle_Launches_Flag__c=mapofAccount.get(strKey).VGA_Vehicle_Launches_Flag__c;
                                objAcc.VGA_Welcome_experience_On_boarding__c=mapofAccount.get(strKey).VGA_Welcome_experience_On_boarding__c;
                                objAcc.VGA_Sponsorship_out__c=mapofAccount.get(strKey).VGA_Sponsorship_out__c;
                                objAcc.VGA_Promotions_Flag__c= mapofAccount.get(strKey).VGA_Promotions_Flag__c;
                                objAcc.VGA_Events_opt_out__c= mapofAccount.get(strKey).VGA_Events_opt_out__c;
                                objAcc.Unsubscription_Reason__c= mapofAccount.get(strKey).Unsubscription_Reason__c;
                                objAcc.VGA_Vehicle_Preference_Flag__c= mapofAccount.get(strKey).VGA_Vehicle_Preference_Flag__c;
                               // objAcc.VGA_Batch__c =true;
                               
                               /* added line for another batch doing same */
                              objAcc.VGA_Subscription_Batch__c=false;
                               objAcc.VGA_Batch__c =false;
                               
                                lstAccount2Update.add(objAcc);
                            }
                        }
                    }
                }
                if(lstAccount2Update != null && !lstAccount2Update.isEmpty())
                 {
                      try
                      {
                        //update lstAccount2Update;
                        database.update(lstAccount2Update); 
                      }
                      catch(exception e)
                       {
                         system.debug('@@@'+e.getmessage());
                       }
                        
                 }
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
      try
        {
        // String batchSize= Label.VGA_Batch_Size;
        //  VGA_ChainingBatchProcess objChain =New VGA_ChainingBatchProcess();
        //  dataBase.executeBatch(objChain,Integer.valueof(batchSize)) ;
        }
         catch(Exception e)
        {
          system.debug('============'+e.getmessage());  
        }
        
    }
}