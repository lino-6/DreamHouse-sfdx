public without sharing class VGA_TestDriveShowEventDetailsController {

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 2019-02-22
    * @description  get information of Specified Event 
    * @param Event ID
    * @return Event List
    */
    @AuraEnabled
    public static Event getLeadDetails(string RecordId)
    {
        List<Event> eventList = [SELECT Id, EndDateTime, StartDateTime,RecordType.Id,RecordType.Name, 
										Subject 
							     FROM Event 
								 WHERE Id=:RecordId
								 LIMIT 1];
		if(eventList.size()!= 0){
			return eventList[0];
		}
        return null;
     }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 2019-02-22
    * @description  update information of Specified Event 
    * @param EventData : Event information to be updated in  String format 
    * @return Event  : return Updated Event 
    */
    @AuraEnabled
    public static Event updateEvent(String EventData){
    	try
        {
    		Event data=(Event)JSON.deserialize(EventData, Event.class);
    		update data;
    		return data;
		}
        catch(Exception ex)
        {
			System.debug(ex+' '+ex.getMessage()+' '+ex.getLineNumber());
		}
		return null;
    }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 2019-02-22
    * @description  delete information of Specified EventId 
    * @param EventId : EventId  information to be deleted
    * @return Boolean : to notify if it is deleted or not
    */
    @AuraEnabled
    public static Boolean deleteEvent(String EventId){
    	try{
            Event ev =[select id from Event where id=:EventId];
    		delete ev;
    		return true;
		}catch(Exception ex){
			System.debug(ex+' '+ex.getMessage()+' '+ex.getLineNumber());
		}
		return false;
    }
}