public class VGA_TestDriveShowLeadDetailsController {

    @AuraEnabled
    public static user getlogginguser()
    {
        return VGA_Common.getloggingAccountDetails();
    }
  
     @AuraEnabled
     public static list<Lead> getLeadlist(String Assignedvalue,String StatusValue ,string DealerId,string ConsultantId,
                                          String sortField, boolean isAsc)
     {   
          map<string,string>mapofstatus =new map<string,string>{'New'=>'New','Accepted'  =>'Accepted','Closed'    => 'Closed'};
          map<string,string>mapofdate     =new map<string,string>{'Last 30 days' => system.now().addDays(-30).format('yyyy-MM-dd'),
                                                          'Today'        =>system.now().format('yyyy-MM-dd'),
                                                          'Last 7 days'  =>system.now().adddays(-7).format('yyyy-MM-dd'),
                                                          'Last 90 days' => system.now().adddays(-90).format('yyyy-MM-dd')                          
                                                         };
         list<Lead>listofLead =new list<Lead>();
         string queryString;
         queryString = 'select Id,Name,Status,Owner.Name,VGA_Brand__c,VGA_Address__c,VGA_Color_Code__c,VGA_Sub_Brand__c,VGA_Request_Type_Image__c,VGA_Outcome__c,VGA_Assigned_to_Dealership__c,VGA_Request_Type__c,VGA_Escalation__c';
         queryString += ' from Lead ';
         queryString += ' where PartnerAccountId =: DealerId';
         queryString += ' and VGA_Request_Type__c !=\'Brochure Request\'';
         if(ConsultantId !='All')
         {
           queryString += ' and OwnerId =: ConsultantId';
         }
         if(StatusValue !='All' && StatusValue !='New and Accepted')
         {
           queryString += ' and Status =\''+ mapofstatus.get(StatusValue)+'\'';
         }
         else if(StatusValue =='New and Accepted')
         {
            queryString +=  ' and (Status =\'New\' or Status = \'Accepted\' ) ';
         }
         if(Assignedvalue !='More than 90 days')
         {
            queryString += ' and VGA_Assign_Date__c <= ' + mapofdate.get('Today');
            queryString += ' and VGA_Assign_Date__c >= ' + mapofdate.get(Assignedvalue) ;
         }
         else
         {
             date morethan90Dates =date.valueof(mapofdate.get('Today')).adddays(-90);
             queryString += ' and VGA_Assign_Date__c <=:morethan90Dates'; 
         }
        
         if (sortField != '') 
         {
          queryString += ' order by ' + sortField;
          if (isAsc) 
          {
            queryString += ' asc';
          } 
          else 
          {
            queryString += ' desc';
          }
         }
         queryString += ' LIMIT 49999';
         listofLead = Database.query(queryString);
         return listofLead;
     }

     @AuraEnabled
     public static Lead getLeadDetails(string RecordId)
     {
         Lead objLead =[select Id,Name,FirstName,LastName,Status,Owner.Name,VGA_User_Id__c,VGA_Outcome__c,
                        VGA_Model_of_Interest__c,VGA_Address__c,VGA_Status__c,VGA_Contact_Id__c,VGA_Dealer_Code__c,
                        VGA_Escalation__c,Address,
                        Email,Phone,MobilePhone,Country,city,street,postalcode,VGA_Sub_Brand__c,
                        VGA_Request_Type_Image__c,OwnerId,VGA_Lead_Id__c,VGA_Assignment_Done__c,
                        VGA_Brand__c,VGA_Request_Type__c,
                        VGA_Configurator_URL__c,Description,LeadSource,createdDate,VGA_Owner_Name__c,VGA_Created_Date__c,Contact__c,VGA_VIN__c 
                        from Lead where Id=: RecordId limit 1];
         return objLead;

         
     }
    
     public static map<string,string> getcontactDetails()
     {
          string Ids =UserInfo.getUserId() ;
          string logingName =UserInfo.getName();
          Map<String,string> mapofContactNameandId = new Map<String,string>();
          string AccountIds =VGA_Common.getloggingAccountId();
          mapofContactNameandId.put(logingName,Ids);
          mapofContactNameandId.put('All','All');
          list<user>listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId 
                                                from User where contact.AccountId =:AccountIds 
                                                and IsActive=true and Id !=:Ids limit 999]);
          
         if(listofUser !=Null && listofUser.size()>0)
          {
              for(user objuser :listofUser)
              {
                  mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
              }
               return mapofContactNameandId;
          }
        else
        {
            return mapofContactNameandId;
        }
     }

     @AuraEnabled
     public static map<string,Id> getAccountDetails()
     {
          Map<String,Id> mapofAccountNameandId = new Map<String,Id>();
          user objUser =VGA_Common.getloggingAccountDetails();
          if(objUser !=Null)
          {
                 mapofAccountNameandId.put(objUser.Contact.Account.Name,objUser.Contact.AccountId);
                 return mapofAccountNameandId;
          }  
         else
         {
            return mapofAccountNameandId;
         }
     }

     @AuraEnabled
     public static string updateLeadDetails(Lead objLead,string ownerIds)
     {
         
         
     system.debug('enter into method');
         if(ownerIds !=Null)
         {
          system.debug('ownerIds' +ownerIds);
          objLead.OwnerId   =ownerIds;
          objLead.Status    ='New';
          objlead.vga_status__c = '';
        
            // system.debug('ownerIds' +objLead.OwnerId);
           // system.debug('objLead.VGA_Contact_Id__c' +objLead.VGA_Contact_Id__c);
             //system.debug('objuser.ContactId ' +objuser.ContactId );
          
         }
         if(objLead.Status !='New')
         {
          objLead.Status    =objLead.VGA_Status__c;
         }
         try
         {
             update objLead;
             if(OwnerIds !=Null)
             {
                 list<Lead>listofLead =new list<Lead>();
                 listofLead.add(objLead);
                 if(listofLead !=Null && listofLead.size()>0)
                 {
                   VGA_LeadTriggerHandler.sendNotification(listofLead, false);
                 }
             }
             return objLead.Id;
         }
            catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            }
         
     }
  
    @AuraEnabled
    public static list<LeadHistory>getLeadHistory(string selectLeadId)
    {
        list<LeadHistory>listofHistory =new list<LeadHistory>([select Id ,Field,LeadId,NewValue,createdDate,Lead.LastModifiedBy.Name,
                                                               OldValue from LeadHistory where LeadId =:selectLeadId ORDER BY createdDate DESC limit 999
                                                              ]);
       
        return listofHistory;
    }
     @AuraEnabled
     public static string createnotes(string titlevalue,string 
                                      commentvalue,string partentId)
     {
         Note objNotes =new Note ();
             objNotes.Title         =titlevalue;
             objNotes.Body          =commentvalue;
             objNotes.ParentId      =partentId;
         try
         {
             insert objNotes;
             return 'Comment saved successfully';
         }
         catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
     }

     @AuraEnabled
     public static list<Note> getnotes(string partentId)
     {
         list<Note>listofNotes =new list<Note>([select Id,Body,owner.Name,createdDate 
                                                from Note where ParentId=:partentId ORDER BY createdDate DESC limit 999 ]);
         return listofNotes;
     }

     @AuraEnabled
     public static string updateleadstatus(string leadId)
     {
         Lead objLead =new Lead();
         objLead.Status='Accepted';
         objLead.VGA_Status__c='Accepted';
         objLead.VGA_Lead_Accepted_Date__c=system.now();  
         objLead.Id=leadId;
         try
         {
             Update objLead;
             return objLead.Id;
             
         }
         catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
     }

    @AuraEnabled
    public static string getuserInformation()
    {
        string Administratorprofile;
        string Consultantprofile ='Consultant' ;
        list<string>ProfileName=VGA_Common.getProfileName();
        if(test.isRunningTest())
        {
            ProfileName.add('Dealer Administrator');
        }
        for(string objRole :ProfileName)
        {
            if(objRole.contains('Dealer Administrator'))
            {
               Administratorprofile=objRole;
               break;
            }
            else if(!objRole.contains('Dealer Administrator') && objRole.contains('Lead Controller') 
                     && !objRole.contains('Consultant') && !objRole.contains('Dealer Portal Group') && !objRole.contains('Dealer Portal')  )
            {
                Administratorprofile=objRole;
                break;
            }
            else if(!objRole.contains('Dealer Administrator') && !objRole.contains('Lead Controller') 
                     && objRole.contains('Consultant') && !objRole.contains('Dealer Portal Group') && !objRole.contains('Dealer Portal') )
            {
                Administratorprofile=objRole;
                break;
            }
            else if(objRole.contains('Dealer Portal Group') )
            {
                Administratorprofile=objRole;
                break;
            }
             else if(!objRole.contains('Dealer Administrator') && !objRole.contains('Lead Controller') 
                     && !objRole.contains('Consultant') && !objRole.contains('Dealer Portal Group') && objRole.contains('Dealer Portal'))
            {
                Administratorprofile=objRole;
                break;
            }

            else
            {
                Administratorprofile='Consultant';
            }
        }
       
            return Administratorprofile;   
         
    }
         
    @AuraEnabled
    public static map<string,string> getresign(string RecordIds)
    {
          Map<String,string> mapofContactNameandId = new Map<String,string>();
          Lead objLead = getLeadDetails(RecordIds);
          string AccountIds =VGA_Common.getloggingAccountId();
          mapofContactNameandId.put(objLead.VGA_Owner_Name__c,objLead.OwnerId);
          list<user>listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId 
                                                from User where contact.AccountId =:AccountIds and IsActive=true  and Id !=: objLead.OwnerId limit 999]);
         
         if(listofUser !=Null && listofUser.size()>0)
          {
              for(user objuser :listofUser)
              {
                  mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
              }
               return mapofContactNameandId;
          }
        else
        {
            return mapofContactNameandId;
        }
     }

    @AuraEnabled
    public static string getuserInformationProfile()
    {
        user objuser  =VGA_Common.getloggingAccountDetails();
        return objuser.profile.Name;
    }
}