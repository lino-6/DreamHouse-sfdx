global class VGA_BatchUpdateAllianzDataMapping implements Database.Batchable<sObject>
{
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strQuery='select id,VGA_Brand__c,VGA_Sub_Brand__c,VGA_Client_Code__c,VGA_Product_Code__c,VGA_Start_Date__c,VGA_End_Date__c,VGA_VIN__c,VGA_Vehicle_Policy_ID__c,VGA_Policy_Reference_Number__c,VGA_Manufacturer__c from VGA_Allianz_Data_Mapping__c where VGA_Vehicle_Policy_ID__c !=null';       
        
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC, List<VGA_Allianz_Data_Mapping__c> lstADM)
    {
        if(lstADM != null && !lstADM.isEmpty())
        {
            try
            {
                Id SkoFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
                Id VWFleet_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
                Id SkoInd_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
                Id VWInD_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
                set<Id> setofVPID = new set<ID>();
                set<Id> setofVehicleID = new set<ID>();
                for(VGA_Allianz_Data_Mapping__c objADM : lstADM)
                {
                    if(objADM.VGA_Vehicle_Policy_ID__c != null)
                    {
                        setofVPID.add(objADM.VGA_Vehicle_Policy_ID__c);
                    }
                    if(objADM.VGA_VIN__c != null)
                    {
                        setofVehicleID.add(objADM.VGA_VIN__c);
                    }
                }
                if(setofVPID != null && !setofVPID.isEmpty())
                {
                    map<ID,Date> mapofOwnership = new map<Id,Date>();
                    map<Id,Date> mapofCurrentOwnership = new map<Id,Date>();
                    if(setofVehicleID != null && !setofVehicleID.isEmpty())
                    {
                        for(VGA_Ownership__c objOwnership : [select id,VGA_Purchase_Date__c,VGA_VIN__c from VGA_Ownership__c where 
                                                                VGA_VIN__c in:setofVehicleID and VGA_Purchase_Date__c != null 
                                                                order by VGA_Purchase_Date__c asc])
                        {
                            if(!mapofOwnership.containskey(objOwnership.VGA_VIN__c))
                                mapofOwnership.put(objOwnership.VGA_VIN__c,objOwnership.VGA_Purchase_Date__c);
                                system.debug('@@@'+mapofOwnership);
                        }
                        for(VGA_Ownership__c objOwnership : [select id,VGA_Purchase_Date__c,VGA_VIN__c,VGA_Type__c from VGA_Ownership__c where VGA_VIN__c in:setofVehicleID 
                                                                and VGA_Type__c ='Current Owner' and VGA_Purchase_Date__c != null])
                        {
                            if(!mapofCurrentOwnership.containskey(objOwnership.VGA_VIN__c))
                                mapofCurrentOwnership.put(objOwnership.VGA_VIN__c,objOwnership.VGA_Purchase_Date__c);
                            system.debug('@@@'+mapofCurrentOwnership);
                        }
                    }
                    ////
                    map<Id,VGA_Vehicle_Policy__c> mapofVehiclePolicy = new map<ID,VGA_Vehicle_Policy__c>();
                    for(VGA_Vehicle_Policy__c objVP : [select id,VGA_Brand__c,VGA_Sub_Brand__c,VGA_Start_Date__c,VGA_End_Date__c,VGA_VIN__c,
                                                       VGA_Is_Updated__c,Created_Today__c,VGA_Cancelled__c,VGA_VIN_No__c,
                                                       VGA_Account_Record_Type_ID__c,Is_Extended_Warranty__c,VGA_Policy_Code__c,
                                                       VGA_Component_Type__c,VGA_Component_Code__c,VGA_Policy_End_Date__c,VGA_Policy_Start_Date__c from 
                                                       VGA_Vehicle_Policy__c where id in:setofVPID])
                    {
                        if(!mapofVehiclePolicy.containsKey(objVP.id))
                            mapofVehiclePolicy.put(objVP.id, objVP);
                    }
                    if(mapofVehiclePolicy != null && !mapofVehiclePolicy.isEmpty())
                    {
                        list<VGA_Allianz_Data_Mapping__c> lstAllianzDataMapping2Update = new list<VGA_Allianz_Data_Mapping__c>();
                        for(VGA_Allianz_Data_Mapping__c objADM : lstADM)
                        {
                            if(objADM.VGA_Vehicle_Policy_ID__c != null && mapofVehiclePolicy.containsKey(objADM.VGA_Vehicle_Policy_ID__c) 
                               && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c) != null)
                            {
                                if(mapofOwnership.containskey(objADM.VGA_VIN__c) && mapofOwnership.get(objADM.VGA_VIN__c) != null)
                                {
                                    objADM.VGA_Vehicle_Original_Sale_Date__c = mapofOwnership.get(objADM.VGA_VIN__c);
                                    objADM.VGA_Vehicle_Current_Sale_Date__c = mapofOwnership.get(objADM.VGA_VIN__c);
                                }
                                if(mapofCurrentOwnership.containskey(objADM.VGA_VIN__c) && mapofCurrentOwnership.get(objADM.VGA_VIN__c) != null)
                                {
                                    objADM.VGA_Vehicle_Current_Sale_Date__c = mapofCurrentOwnership.get(objADM.VGA_VIN__c);
                                }
                                
                                objADM.VGA_Brand__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c:null;
                                objADM.VGA_Sub_Brand__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c:null;
                                objADM.VGA_Start_Date__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Policy_Start_Date__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Policy_Start_Date__c:null;
                                objADM.VGA_End_Date__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Policy_End_Date__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Policy_End_Date__c:null;
                                objADM.VGA_VIN__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_VIN__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_VIN__c:null;
                                objADM.VGA_Policy_Reference_Number__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_VIN__c;
                                objADM.VGA_Manufacturer__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c:null;
                                
                                objADM.VGA_Unique_Customer_Number__c = mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_VIN_No__c != null?mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_VIN_No__c:null;
                                
                                
                                if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Is_Updated__c == true)
                                {
                                   // objADM.VGA_Transaction_Code__c = 'C';
                                }
                                else if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).Created_Today__c == true)
                                {
                                    objADM.VGA_Transaction_Code__c = 'A';
                                }
                                else if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Cancelled__c == true)
                                {
                                    objADM.VGA_Transaction_Code__c = 'D';       
                                }
                                
                                if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c != null && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c.equalsignoreCase('Skoda'))
                                {
                                    objADM.VGA_Client_Code__c = 'SKO';
                                }
                                else if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c != null && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c != null && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c.equalsignoreCase('Volkswagen') && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c.equalsignoreCase('Passenger Vehicles (PV)'))
                                {
                                    objADM.VGA_Client_Code__c = 'VWP';
                                }
                                else if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c != null && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c != null && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Brand__c.equalsignoreCase('Volkswagen') && mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Sub_Brand__c.equalsignoreCase('Commercial Vehicles (CV)'))
                                {
                                    objADM.VGA_Client_Code__c = 'VWC';
                                }
                                
                                
                                if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c != null && (mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c == SkoFleet_RTId || mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c == VWFleet_RTId))
                                {
                                    objADM.VGA_Company_Flag__c = 'Y';
                                //    objADM.VGA_Address_Type__c = 'Work';
                                }
                                else if(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c != null && (mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c == SkoInd_RTId || mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c).VGA_Account_Record_Type_ID__c == VWInD_RTId ))
                                {
                                    objADM.VGA_Company_Flag__c = 'N';
                                //    objADM.VGA_Address_Type__c = 'Work';
                                }

                                //Calculate product code 
                                objADM.VGA_Product_Code__c = VGA_AllianzDataMappingHelper.getAllianzProductCode(mapofVehiclePolicy.get(objADM.VGA_Vehicle_Policy_ID__c));
                                lstAllianzDataMapping2Update.add(objADM);
                            }    
                        }
                        if(lstAllianzDataMapping2Update != null && !lstAllianzDataMapping2Update.isEmpty())
                        {
                            update lstAllianzDataMapping2Update;
                        }
                    }
                }
            }
            catch(exception ex)
            {
                system.debug('@@@'+ex.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        
    }
}