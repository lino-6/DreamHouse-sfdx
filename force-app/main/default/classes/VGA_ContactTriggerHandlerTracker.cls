/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

//======================================================
//Test Class For VGA_ContactTriggerHandler
//=====================================================
//Name:Rishi Patel    Date:8-11-17       Created
//     Nitisha Prasad   21-11-17         Updated
//=====================================================

@isTest(seeAllData=false)
private class VGA_ContactTriggerHandlerTracker 
{
    public static Account objAccount;
    public static Account objAccount1;
    public static Contact objContact;
    public static VGA_Dealer_Notification__c objDealerNotification;
    public static User objUser;
    public static VGA_User_Profile__c objUserPro;
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        loadData();  
        
        
        Test.startTest();
        
        
        
        objDealerNotification = new VGA_Dealer_Notification__c();
        objDealerNotification.Dealer_Contact__c = objContact.id;
        objDealerNotification.VGA_Notification_Sent__c=false;
        
        insert objDealerNotification;
        
        objContact.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objContact.Email = 'rishi.patel@saasfocus.com';
        objContact.MobilePhone = '7053315190';
        objContact.mailingstreet = 'Test';
        objContact.mailingcity = 'Delhi';
        objContact.mailingstate = 'Up';
        update objContact;
        Test.stopTest();
        
        
        
        system.debug('@@@@objContact@@@'+objContact);
        
    }
    static testMethod void myUnitTest2() 
    {
        // TO DO: implement unit test
        loadData();  
        
        
        Test.startTest();
        
        objContact.VGA_Job_Title__c = 'Dealer Principal';
        update objContact; 
        
        objDealerNotification = new VGA_Dealer_Notification__c();
        objDealerNotification.Dealer_Contact__c = objContact.id;
        objDealerNotification.VGA_Notification_Sent__c=false;
        
        insert objDealerNotification;
        
        
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objContact.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objContact.Email = 'rishi.patel@saasfocus.com';
        objContact.MobilePhone = '7053315190';
        objContact.mailingstreet = 'Test';
        objContact.mailingcity = 'Delhi';
        objContact.mailingstate = 'Up';
        update objContact;
        Test.stopTest();
        
        
        
        system.debug('@@@@objContact@@@'+objContact);
        
    }
    
    public static void loadData()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Contact';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        objContact.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        update objContact;
        
        
        
    }
}