@RestResource(urlMapping='/findDealer/*')
global with sharing class VGA_AmazonLambdaRESTService {

    //URL format: https://instance.salesforce.com/services/apexrest/findDealer?model=<Model>&postcode=<postcode>
     /**
    * @author Alfahad PM
    * @date 05/06/2019
    * @description This class is called by AWS lambda called 'getDealer' to get dealer details. 
    *              The 'getDealer' lambda function is invoked by Amazon connect. 
    *              When a user calls the hotline number and enter his postcode, 
    *              Amazon connect will call lambda function and lambda will call this Salesforce API and pass postcode. 
    *              This method will return dealer details by checking the PMA mapping the lambda function is called getDealerDetails
    */
     @HttpGet
     global static string doGetDealer()
     {
         String model = RestContext.request.params.get('model');
		 String post_code = RestContext.request.params.get('postcode');
         
         //Added Added logic to update Postal Code Length
         //If Postal code length is <=3 then Prefixed 0 in Postal Code
         if(post_code!=null){
            if(post_code.trim().length()<=3){
                String temp='';
                for(Integer i=0;i<(4-post_code.trim().length());i++){
                    temp+='0';
                }
                post_code=temp+post_code.trim();
            }
         }
         //*****************//
         
         if(String.isBlank(model))
             return '{1}';
         
         List<Product2> prod_lst = [SELECT VGA_Brand__c FROM Product2 WHERE VGA_Model_Alias__c =: model LIMIT 1];
         if(prod_lst.size() == 0)
             return '{2}';
   
         List<VGA_Post_Code_Mapping__c> pma = [SELECT VGA_VWP_Dealer__c, VGA_VWC_Dealer__c, VGA_SKP_Dealer__c FROM VGA_Post_Code_Mapping__c WHERE VGA_Post_Code__c =: post_code];
         if(pma.size() == 0)
             return '{3}';   
         
         String name = '';
         String dealer_code = '';
         String email = '';
         String phone = '';
         Id dealer_id = null;
         
         if(prod_lst.get(0).VGA_Brand__c == 'PV Volkswagen')
         {  
             dealer_id = pma.get(0).VGA_VWP_Dealer__c;
         }
         else if(prod_lst.get(0).VGA_Brand__c == 'CV Volkswagen')
         {
			dealer_id = pma.get(0).VGA_VWC_Dealer__c;            
         }
         else 
         {
			dealer_id = pma.get(0).VGA_SKP_Dealer__c;	             
         }
         
         List<Account> act_lst = [SELECT Name, VGA_Dealer_Code__c, VGA_Email__c, phone FROM Account WHERE id =: dealer_id];
         
         if(act_lst.size() == 0)
             return '{4}' + dealer_id;
         
         name = act_lst.get(0).Name != null ? act_lst.get(0).Name : '';
         dealer_code = act_lst.get(0).VGA_Dealer_Code__c != null ? act_lst.get(0).VGA_Dealer_Code__c : '';
         email = act_lst.get(0).VGA_Email__c != null ? act_lst.get(0).VGA_Email__c : '';
         phone = act_lst.get(0).phone != null ? act_lst.get(0).phone : '';         
         
         return '{"dealer_name": "' +name + '", "dealer_code":"'+dealer_code+'", "dealer_email": "'+email+'", "dealer_phone": "'+phone+'"}';                                                       
     }
}