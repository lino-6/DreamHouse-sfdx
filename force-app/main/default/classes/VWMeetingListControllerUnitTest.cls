// ******
// PVWMeetingListControllerUnitTest
// Copyright Punos Mobile 2017
// Test class target: VWMeetingListController
// Test logic:
// 1. Create a test account.
// 2. Create two test users.
// 3. Create test events related to the account using one of the test users, consequently triggering the creation of meetings.
// 4. Using the second test user, try to populate the meeting list. The number of the meetings should match the number of the events created.
// ******
// 
@isTest
public class VWMeetingListControllerUnitTest {

    private static string AccountID;
    private static Account testAccountParameter;
    private static User UserTwo;
    
    static testMethod void testPopulateMeetingList(){
        
        //Create test meetings via the test events
        List<Event> testEventsForMeetingList = createTestEvents();
        setTestMeetingsAsCompleted();
        
        //Creates a test user that can be used to query the meetings for the list
        UserTwo = createTestUserTwo();
          
			PageReference AccountMeetingListPage = Page.VWMeetingListAccount;
       		AccountMeetingListPage.getParameters().put('id',AccountID);
       		Test.setCurrentPage(AccountMeetingListPage);
            
            ApexPages.StandardController sc = new ApexPages.StandardController(testAccountParameter);          
        	VWMeetingListController mlc = new VWMeetingListController(sc);
         	mlc.init();
        	mlc.getMeetingsWithObject();
      
        	//Run actual test code as the test user number two
        	System.runAs(UserTwo){          
        	System.assertEquals(10, mlc.meetingsWithObject.size());       
        	}
    }
    
        
    //Creates one new test account. Returns the account ID.
    private static String createTestAccount(){
 
        Account account = new Account(Name = 'Test account');
        
        List<RecordType> recordTypeList = [SELECT Id From RecordType WHERE DeveloperName=:'Volkswagen_Dealer_Account' AND SobjectType=:'Account' LIMIT 1];
        
        String recordTypeID;
        
		if(!recordTypeList.isEmpty()){
			recordTypeID = recordTypeList[0].Id;
            account.put('RecordTypeId', recordTypeID);
		}	
            
        insert account;
        //Ensure that the account was created successfully and fetch the Id
        List<Account> NewAccountlist = [SELECT id From Account WHERE Name = 'Test account' LIMIT 1];
        
        testAccountParameter = NewAccountlist[0];
        
        return NewAccountlist[0].Id;       
    }
    
    //Creates the first test user
    private static String createTestUserOne(){

		//First we check that does the user already exist.
        List<User> userlist = [SELECT id From User WHERE Username = 'testStandardUserOne@punosmobile.com' LIMIT 1];
        if(!userlist.isEmpty()){
            return userlist[0].ID;
        }
        else //The user does not exist. We'll create a new standard user.
        	 //Please note that for different scenarios, you might need to change, e.g., the profile, locale and time zone.
        {
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

            User user = new User(alias = 'testOne',
                                 firstname='TestOne',
                                 lastname='Testing',
                                 username='testStandardUserOne@punosmobile.com',
                                 email='standarduser.one@punosmobile.com',
                                 emailencodingkey='UTF-8',
                                 languagelocalekey='en_US',
                                 localesidkey='en_US',
                                 profileid = profileId.id,
                                 timezonesidkey='America/Los_Angeles');

            insert user;
			
			//Ensure that the user was created successfully and fetch the Id
            List<User> NewUserlist = [SELECT id From User WHERE Username = 'testStandardUserOne@punosmobile.com' LIMIT 1];

            return NewUserlist[0].Id;
        }
    }
    
    //Creates the second test user
    private static User createTestUserTwo(){

		//First we check that does the user already exist.
        List<User> userlist = [SELECT id From User WHERE Username = 'testStandardUserTwo@punosmobile.com' LIMIT 1];
        if(!userlist.isEmpty()){
            return userlist[0];
        }
        else //The user does not exist. We'll create a new standard user.
        	 //Please note that for different scenarios, you might need to change, e.g., the profile, locale and time zone.
        {
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

            User user = new User(alias = 'testTwo',
                                 firstname='TestTwo',
                                 lastname='Testing',
                                 username='testStandardUserTwo@punosmobile.com',
                                 email='standarduser.two@punosmobile.com',
                                 emailencodingkey='UTF-8',
                                 languagelocalekey='en_US',
                                 localesidkey='en_US',
                                 profileid = profileId.id,
                                 timezonesidkey='America/Los_Angeles');

            insert user;
			
			//Ensure that the user was created successfully and fetch the Id
            List<User> NewUserlist = [SELECT id From User WHERE Username = 'testStandardUserTwo@punosmobile.com' LIMIT 1];

            return NewUserlist[0];
        }
    }
  
    
    //Creates a collection of events using the first test user and the account
    private static List<Event> createTestEvents(){
    	
        //Creates a test account that will be related to the event
        AccountID = createTestAccount();
        
    	//Creates a test user that can be used as an event owner
        String OwnerID = createTestUserOne();
        
        List<Event> events = new List<Event>();
		
		//Test event ActivityDateTime is the current moment
        DateTime dt = DateTime.now();

		//Create 10 new Events
        for(Integer i=0; i < 10; i++) {
            Event event = new Event();
            event.OwnerId = ownerID;
            event.Subject ='Meeting List Test '+i;
            event.DurationInMinutes = 60;
            event.ActivityDateTime = dt;
            event.WhatId = AccountID;
            events.add(event);
        }

		insert events;

        return events;
    }
    
    //As the meeting list only displays meetings that have been completed, we need to mark all test meetings as such.
    private static void setTestMeetingsAsCompleted(){
        List<punosmobile__Meeting__c> newMeetingsList = [SELECT Id, punosmobile__Meeting_Status__c FROM punosmobile__Meeting__c];
        
        for(punosmobile__Meeting__c meeting : newMeetingsList){
        	meeting.punosmobile__Meeting_Status__c = 'Completed';    
        }
        
        update newMeetingsList;
    }
}