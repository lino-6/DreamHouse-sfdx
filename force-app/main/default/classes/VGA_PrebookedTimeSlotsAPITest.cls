@isTest
public class VGA_PrebookedTimeSlotsAPITest {
    
    @testSetup
    static void buildConfigList() {
        Account dealerAcc = VGA_CommonTracker.buildDealerAccount('DealerAccount', '1234');
        dealerAcc.VGA_Pilot_Dealer__c = true;
        dealerAcc.VGA_Timezone__c     = 'Australia/Sydney';
        insert dealerAcc;

        VGA_Dealership_Profile__c dealershipProfile = VGA_CommonTracker.createDealershipProfile(dealerAcc.Id);

        VGA_Trading_Hour__c tradHours = VGA_CommonTracker.createTrading_Hour(dealershipProfile.Id);

        Contact cont = VGA_CommonTracker.buildDealerContact(dealerAcc.Id, 'testContact');
        insert cont;

        User relatedUser =  VGA_CommonTracker.buildUser(cont.Id);
        insert relatedUser;

        VGA_User_Profile__c usrProfile = VGA_CommonTracker.buildUserProfile('Dealer Administrator', cont.Id, 'Volkswagen', 'Passenger Vehicles (PV)');
        usrProfile.VGA_Available_TestDrive__c = true;
        insert usrProfile;

        VGA_Working_Hour__c objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = usrProfile.id;
        objWorking.VGA_Working__c      = true;
        objWorking.Name                =  system.now().format('EEEE');
        insert objWorking;

        Event evt = new Event();
        evt.StartDateTime = system.now();
        evt.EndDateTime   = system.now().addHours(1);
        evt.OwnerId       = relatedUser.Id;
        insert evt;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 09/04/2018
    * @description Test method for the VGA_PrebookedTimeSlotsAPI.
    *              It tests the scenario when we request the available time slots for a 
    *              dealer that is not a pilot. In this scenario will return a list with
    *              all the time slots based on the dealer trading hours
    */
    @isTest
    public static void test01_GetPrebookedTimeSlots_WhenDealerIsNotPilot_WeShouldReturnAllTimeSlots()
    {
        Account acc = [SELECT Id, VGA_Pilot_Dealer__c FROM Account WHERE VGA_Dealer_Code__c = '1234' LIMIT 1 ];
        acc.VGA_Pilot_Dealer__c = false;
        update acc;

        RestRequest req = new RestRequest();
        req.params.put('subbrand', 'Passenger Vehicles (PV)');
        req.params.put('dealerCode', '1234');
        req.params.put('date', system.now().format('yyyy-MM-dd'));
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        VGA_PrebookedTimeSlotsAPI.getPreBookedTimeSlots();

        Test.stopTest();

        
        System.debug(EncodingUtil.base64Encode(res.responseBody));
        //System.assert(..., ..., '...');
    }

    /**
    * @author Lino Diaz Alonso
    * @date 09/04/2018
    * @description Test method for the VGA_PrebookedTimeSlotsAPI.
    *              It tests the scenario when we request the available time slots for a 
    *              dealer that is a pilot. In this scenario will return a list with
    *              all the time slots based on the dealer trading hours and the contacts availability
    */
    @isTest
    public static void test02_GetPrebookedTimeSlots_WhenDealerIsNotPilot_WeShouldReturnAllTimeSlots()
    {
        RestRequest req = new RestRequest();
        req.params.put('subbrand', 'Passenger Vehicles (PV)');
        req.params.put('dealerCode', '1234');
        req.params.put('date', system.now().format('yyyy-MM-dd'));
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        VGA_PrebookedTimeSlotsAPI.getPreBookedTimeSlots();

        Test.stopTest();

        
        System.debug(EncodingUtil.base64Encode(res.responseBody));
        //System.assert(..., ..., '...');
    }
}