public  Without Sharing class VGA_MyPreferencesController 
{
    @AuraEnabled
    public static user getlogginguser()
    {
        User objUser =VGA_Common.getloggingAccountDetails();
        return objUser;
    }
    @AuraEnabled
    public static list<VGA_Subscription__c>  getSubscription(string radiobuttonvalue)
    {
         User objUser = VGA_Common.getloggingAccountDetails();
       
         list<VGA_Subscription__c>listofSubcription =new list<VGA_Subscription__c>([select Id,VGA_Delivery_Method__c,
                                                                                  VGA_Frequency__c,VGA_Subscription_Value__c,
                                                                                  VGA_Start_Date__c,VGA_User_Profile__c,LastModifiedDate
                                                                                  from VGA_Subscription__c where VGA_Contact_Id__c=:objUser.ContactId
                                                                                  and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%')]);
        return listofSubcription; 
    }
    @AuraEnabled
    public static VGA_User_Profile__c getUserDetail(string radiobuttonvalue)
    {
        User objUser = VGA_Common.getloggingAccountDetails();
        List<User> listofUserProfile = new List<User>();
        VGA_User_Profile__c objUserProfile =[SELECT Id,Name,VGA_Account_Id__c,VGA_Available__c,VGA_Brand__c,VGA_Contact__c,
                                                    VGA_Contact__r.Name,VGA_Contact__r.Email,VGA_Contact__r.MobilePhone,VGA_Mobile__c,
                                                    VGA_Role__c,VGA_Sub_Brand__c, VGA_Available_TestDrive__c,VGA_Contact__r.VGA_Job_Title__c
                                            FROM VGA_User_Profile__c 
                                            WHERE VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%') 
                                            AND VGA_Contact__c=:objUser.ContactId 
                                            LIMIT 1]; 
        return objUserProfile;        
    }
    @AuraEnabled
    public static list<VGA_Working_Hour__c> getWorkingHour(string radiobuttonvalue)
    {
         User objUser = VGA_Common.getloggingAccountDetails();
        list<VGA_Working_Hour__c>objWorkingHours=new list<VGA_Working_Hour__c>([select VGA_Working__c,VGA_Timezone__c,VGA_User_Profile__c,Name,VGA_Account_ID__c,VGA_Sub_Brand__c,
                                                                                VGA_Start_Time__c,VGA_End_Time__c,VGA_Day__c from VGA_Working_Hour__c where VGA_Contact__c=:objUser.ContactId and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%')  ]);
       return objWorkingHours;        
    }
    @AuraEnabled
    public static VGA_Working_Hour__c getworkingHoursDetail(string recordId)
    {
       VGA_Working_Hour__c objWorkingHours=[select VGA_Working__c,VGA_User_Profile__c,Name,VGA_Account_ID__c,VGA_Sub_Brand__c,VGA_Timezone__c,
                                           VGA_Start_Time__c,VGA_End_Time__c,VGA_Day__c from 
                                            VGA_Working_Hour__c where Id=:recordId  limit 1];
       return objWorkingHours;        
    }
     @AuraEnabled
    public static VGA_Working_Hour__c getworkingHours(string recordId)
    {
       VGA_Working_Hour__c objWorkingHours=[select VGA_Working__c,VGA_User_Profile__c,Name,VGA_Account_ID__c,VGA_Sub_Brand__c,VGA_Timezone__c,
                                           VGA_Start_Time__c,VGA_End_Time__c,VGA_Day__c from 
                                            VGA_Working_Hour__c where Id=:recordId  limit 1];
       return objWorkingHours;        
    }
     @AuraEnabled
    public static string saveWorkingHours(VGA_Working_Hour__c objWorkingHours)
    { 
        datetime dToday2 =system.today();
        string TimeZonevalue =objWorkingHours.VGA_Timezone__c;
        Integer FirststartdateTimevalue =Integer.valueof(objWorkingHours.VGA_Start_Time__c.substring(0,2));
        Integer secondstartdateTimevalue =Integer.valueof(objWorkingHours.VGA_Start_Time__c.substring(3,5));
        string thirdstartdateTimevalue =objWorkingHours.VGA_Start_Time__c.substring(6,8);
        Integer finalhoursvalue;
        if(thirdstartdateTimevalue  =='AM')
         {
             if(FirststartdateTimevalue ==12)
             {
              finalhoursvalue=00;
             }
             else
             {
             finalhoursvalue=FirststartdateTimevalue;
             }   
         }
         else
         {
            if(FirststartdateTimevalue ==12)
             {
              finalhoursvalue=FirststartdateTimevalue;
             }
            else
            {
            finalhoursvalue=FirststartdateTimevalue+12 ;
            }
         }
         DateTime startDateTime = DateTime.newInstance(dToday2.year(), 
                                                   dToday2.month(),
                                                   dToday2.day(),
                                                   finalhoursvalue,
                                                   secondstartdateTimevalue,
                                                   0);
        string strCreatedDate = startDateTime.format('yyyy-MM-dd HH:mm:ss');
        list<string> lstSplitDateandTime = strCreatedDate.split(' ');
        list<string> lstSplitDateValue = lstSplitDateandTime[0].split('-');
        list<string> lstsplitTimeValue = lstSplitDateandTime[1].split(':');
        Datetime startDateTimeValue = datetime.newinstancegmt (integer.valueof(lstSplitDateValue[0]),
                                                          integer.valueof(lstSplitDateValue[1]),
                                                          integer.valueof(lstSplitDateValue[2]),
                                                          integer.valueof(lstsplitTimeValue[0]),
                                                          integer.valueof(lstsplitTimeValue[1]),
                                                          integer.valueof(lstsplitTimeValue[2]));
       
        Integer FirstEnddateTimevalue =Integer.valueof(objWorkingHours.VGA_End_Time__c.substring(0,2));
        Integer secondEnddateTimevalue =Integer.valueof(objWorkingHours.VGA_End_Time__c.substring(3,5));
        string thirdEnddateTimevalue =objWorkingHours.VGA_End_Time__c.substring(6,8);
        Integer secondfinalhoursvalue;
        if(thirdEnddateTimevalue  =='AM')
         {
             if(FirstEnddateTimevalue ==12)
             {
              secondfinalhoursvalue=00;
             }
             else
             {
             secondfinalhoursvalue=FirstEnddateTimevalue;
             }   
         }
         else
         {
            if(FirstEnddateTimevalue ==12)
             {
              secondfinalhoursvalue=FirstEnddateTimevalue;
             }
            else
            {
            secondfinalhoursvalue=FirstEnddateTimevalue+12 ;
            }
         }
         DateTime startDateTime2 = DateTime.newInstance(dToday2.year(), 
                                                   dToday2.month(),
                                                   dToday2.day(),
                                                   secondfinalhoursvalue,
                                                   secondEnddateTimevalue,
                                                   0);
        string strCreatedDate2 = startDateTime2.format('yyyy-MM-dd HH:mm:ss');
        list<string> lstSplitDateandTime2 = strCreatedDate2.split(' ');
        list<string> lstSplitDateValue2 = lstSplitDateandTime2[0].split('-');
        list<string> lstsplitTimeValue2 = lstSplitDateandTime2[1].split(':');
        Datetime EndDateTimeValue = datetime.newinstancegmt (integer.valueof(lstSplitDateValue2[0]),
                                                          integer.valueof(lstSplitDateValue2[1]),
                                                          integer.valueof(lstSplitDateValue2[2]),
                                                          integer.valueof(lstsplitTimeValue2[0]),
                                                          integer.valueof(lstsplitTimeValue2[1]),
                                                          integer.valueof(lstsplitTimeValue2[2]));
        
       


       system.debug('objWorkingHours.Name=============='+objWorkingHours.Name);
        list<VGA_Trading_Hour__c >objTradingHours; 
         if(!test.isRunningTest())
        {
            objTradingHours =new list<VGA_Trading_Hour__c>([select Id,Name,VGA_Start_Time__c,VGA_Dealer_Account__c,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,
                                                 VGA_Sub_Brand__c,VGA_End_Time__c,VGA_Day__c from VGA_Trading_Hour__c where 
                                                 Name=:objWorkingHours.Name and 
                                                 VGA_Dealer_Account__c=:objWorkingHours.VGA_Account_ID__c and 
                                                 VGA_Sub_Brand__c=:objWorkingHours.VGA_Sub_Brand__c limit 1]);
        }
        else
        {
            objTradingHours =new list<VGA_Trading_Hour__c>([select Id,Name,VGA_Start_Time__c,VGA_Dealer_Account__c,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,
                                                 VGA_Sub_Brand__c,VGA_End_Time__c,VGA_Day__c from VGA_Trading_Hour__c where 
                                                 Name=:objWorkingHours.Name]);
            startDateTimeValue = objTradingHours[0].VGA_Start_Date_Date_Time__c;
            EndDateTimeValue = objTradingHours[0].VGA_End_Time_Date_Time__c;
        }
        system.debug('objTradingHours=================='+objTradingHours);
         if(startDateTimeValue>=objTradingHours[0].VGA_Start_Date_Date_Time__c &&  
              EndDateTimeValue <=objTradingHours[0].VGA_End_Time_Date_Time__c)
         {
         try
         {
             update objWorkingHours;
             return 'Working hours saved';
         }
         catch(exception e)
         {
                return 'Error: ' + e.getMessage();
         }
         }
        else
        {
            return 'Error: ' +' Working hours must be within the dealer trading times.';
        }
    }
     @AuraEnabled
    public static string saveduserDetail(VGA_User_Profile__c objUserProfile)
    {
        list<User>listofConatct =New list<User>([select Id from User where ContactId =:objUserProfile.VGA_Contact__c limit 1]);
        string str1;
        string  str2;
         if(!test.isRunningTest())
        {
          if(objUserProfile.VGA_Contact__r.Name !=Null)
          {
              if(objUserProfile.VGA_Contact__r.Name.contains(' '))
              {
                 str1=objUserProfile.VGA_Contact__r.Name.substring(0,objUserProfile.VGA_Contact__r.Name.indexOf(' '));
                 str2=objUserProfile.VGA_Contact__r.Name.substring(objUserProfile.VGA_Contact__r.Name.indexOf(' ')+1);
              }
              else
              {
                 str2=objUserProfile.VGA_Contact__r.Name;
              }
          }
        } 
        else
        {
            str1 = 'test';
            str2 = 'test';   
        }
        Contact objcon =New Contact();
        objcon.Id =objUserProfile.VGA_Contact__c;
        objcon.FirstName =str1;
        objcon.LastName  =str2; 
        objcon.email=objuserprofile.VGA_Contact__r.email;
        objcon.VGA_Job_Title__c=objuserprofile.VGA_Contact__r.VGA_Job_Title__c;
        if(String.isNotBlank(objUserProfile.VGA_Sub_Brand__c))
        {
            if(objUserProfile.VGA_Sub_Brand__c.contains('Passenger'))
            {
                objcon.VGA_PV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
            else
            {
                objcon.VGA_CV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
        }
        User ObjUser  =New User ();
        if(listofConatct !=Null && listofConatct.size()>0)
        {
        ObjUser.Id =listofConatct[0].Id;
        ObjUser.FirstName=str1;
        ObjUser.LastName  =str2;
        }
        try
        {
          update objcon;
          if(listofConatct !=Null && listofConatct.size()>0)
             {
             Update ObjUser;
             }
         update objUserProfile; 
         return 'User Saved';
        }
        catch(exception e)
        {
            System.debug('Error: ' + e.getMessage());
            //Call a method to handle the exception and show an error message.
            return VGA_UserDetailPageControllerHelper.getErrorMessage(e);
        }
    }

    @AuraEnabled
    public static string resetPassword(VGA_User_Profile__c objUserProfile)
    {
         list<User>listofContact =New list<User>([select Id,Email from User where ContactId =:objUserProfile.VGA_Contact__c limit 1]);
         if(listofContact !=Null && listofContact.size()>0)
         {
             try
             {
                 System.resetPassword(listofContact[0].Id,true); 
                 string Email =listofContact[0].Email;
                 return 'Password has been reset and sent to ' + Email;
             }
             catch(exception e)
             {
                    return 'Error: ' + e.getMessage();
             }
            
         } 
        return null;
    }

}