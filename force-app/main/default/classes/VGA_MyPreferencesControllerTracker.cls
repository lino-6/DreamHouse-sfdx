/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_MyPreferencesControllerTracker 
{
    public static Account objAccount;
    public static Contact objContact;
    public static User objUser;
    public static VGA_Subscription__c objSubscription;
    public static VGA_User_Profile__c objUserProfile;
    public static VGA_Working_Hour__c objWH;
    public static VGA_Trading_Hour__c objTrading;
    public static VGA_Trading_Hour__c objTH;
    public static VGA_Dealership_Profile__c objDp;
    static testmethod void unittest1()
    {
        loadData(); 
        objUserProfile = VGA_CommonTracker.createUserProfile('test');
        objUserProfile.VGA_Contact__c = objContact.id;
        objUserProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        insert objUserProfile;
        
        objSubscription = VGA_CommonTracker.createSubscription(objUserProfile.id);
        insert objSubscription;
        objWH.VGA_User_Profile__c = objUserProfile.id;
        objWH.Name = system.now().format('EEEE');
        insert  objWH; 
        objTH = VGA_CommonTracker.createTrading_Hour(objDp.Id);
        
        system.debug('@@@'+objWH.Name +'@@@'+objTH.Name);
        system.runas(objUser)
        {
            user objUser = VGA_MyPreferencesController.getlogginguser();
            
            list<VGA_Subscription__c> lstsubscription = VGA_MyPreferencesController.getSubscription('Passenger Vehicles (PV)');
            
            VGA_User_Profile__c objUP = VGA_MyPreferencesController.getUserDetail('Passenger Vehicles (PV)');
            
            list<VGA_Working_Hour__c> lstWH= VGA_MyPreferencesController.getWorkingHour('Passenger Vehicles (PV)');
            
            VGA_Working_Hour__c objWH = VGA_MyPreferencesController.getworkingHoursDetail(objWH.Id);
            
            VGA_Working_Hour__c objWH1 = VGA_MyPreferencesController.getworkingHours(objWH.Id);
            
            string str1 = VGA_MyPreferencesController.saveWorkingHours(objWH);
            
            string str2 = VGA_MyPreferencesController.saveduserDetail(objUserProfile);
            
            VGA_MyPreferencesController.resetPassword(objUserProfile);
        }
    }
    public static void loadData()
    {
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        update objAccount;
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        objUser =   VGA_CommonTracker.CreateUser(objContact.id);
        objWH = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'07:00 AM','07:00 PM');
        objDp = VGA_CommonTracker.createDealershipProfile(objAccount.Id, objAccount.VGA_Sub_Brand__c, 'Nominated User');
        objDp.VGA_Brand__c ='Volkswagen';
        objDp.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDp.VGA_Nominated_User__c = userinfo.getUserId();
        objDp.VGA_Nominated_Contact__c = objContact.Id;
        insert objDp;
        objTrading=VGA_CommonTracker.createTrading_Hour(objDp.Id);
        
        
        
    }
}