public without sharing class VGA_MapDetailsController 
{
    public string LeadId{get;set;}
    public Lead objLead{get;set;}

    public VGA_MapDetailsController(ApexPages.StandardController controller) 
    {
            LeadId=ApexPages.currentPage().getParameters().get('Ids');
            getLeadDetails();
    }
    public void getLeadDetails()
    {
     objLead=[select Id,Name,Street,City,State,PostalCode from lead where Id=:LeadId limit 1];
    }

}