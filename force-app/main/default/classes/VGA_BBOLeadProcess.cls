/**
      * @author Ganesh M
      * @date 14/11/2018
      * @description Class Updating BBO Tag of Account when Lead is created 
      */
Public class VGA_BBOLeadProcess {


 /**
      * @author Ganesh M
      * @date 14/11/2018
      * @description It updating Updating BBO Tag of Account when Lead is created 
      * @param  lsttriggernew is a collection of new Lead records
      * @return  Null
      */
   public void tagBBO(List<Lead> triggerNew)
   {
    
     Set<Id> account_id = new Set<Id>();
     Set<Id> Lead_id = new Set<Id>();
     map<id,string> campaignLead = new map<id,string>();
     
      for(Lead l : triggerNew)
       {
          account_id.add(l.VGA_Customer_Account__c);
          Lead_id.add(l.id);
        }
        if(Lead_id.size()>0)
         {
           for(campaignmember cm : [select id,leadid,description,Campaign.Name,Campaign.description from campaignmember where leadid in :Lead_id])
           {
               
               campaignLead.put(cm.leadid,cm.Campaign.description);
               
            }
          }
        List<Account> account_lst = [SELECT Id, VGA_BBO_Tag_Payload__c FROM Account WHERE isPersonAccount = true AND VGA_Brand__c != 'Skoda' AND VGA_Promotions_Flag__c  = true AND Id = :account_id];
        for(Account obj_act : account_lst)
        {
        
            for(Lead obj : triggerNew)
            {
                if(obj.VGA_Customer_Account__c == obj_act.id)
                {
               
                    VGA_BBOService bbo = new VGA_BBOService(obj_act.VGA_BBO_Tag_Payload__c == null ? '{}' : obj_act.VGA_BBO_Tag_Payload__c);
                    VGA_BBOCustomer bbo_customer = bbo.getCustomer();
                    VGA_BBOLead bbo_lead = new VGA_BBOLead();
                    if(obj.VGA_Request_Type__c == 'Brochure Request')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.BROCHURE_LEAD_FORM;
                    }
                    else if(obj.VGA_Request_Type__c == 'Test Drive')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.TEST_DRIVE_LEAD_FORM;
                    }
                    else if(obj.VGA_Request_Type__c == 'Callback Request')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.CONTACT_LEAD_FORM;
                    }
                    else if(obj.VGA_Request_Type__c == 'Registration of Interest')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.OTHER;
                    }
                    else if(obj.VGA_Request_Type__c == 'Request for Information')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.INFO_REQUEST_FORM;
                    }
                    else if(obj.VGA_Request_Type__c == 'Competition Entry')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.COMPETITION_REGISTRATION;
                    }
                    else if(obj.VGA_Request_Type__c == 'VWFS Parity Lead')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.OTHER;
                    }
                    else if(obj.VGA_Request_Type__c == 'Car Finder')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.OTHER;
                    }
                    else if(obj.VGA_Request_Type__c == 'stock locator')
                    {
                        bbo_lead.lead_type = VGA_BBOLead.types.OTHER;
                    }
                    else {
                        bbo_lead.lead_type = VGA_BBOLead.types.OTHER;
                    }
                    if(!string.isBlank(obj.VGA_Model_of_Interest__c))
                       {
                           List<String> models = obj.VGA_Model_of_Interest__c.split(',');
                           if(models.size() >= 1)
                               bbo_lead.camo = models.get(0);
                       }
                    bbo_lead.pryr = obj.CreatedDate.date().year();
                    //bbo_lead.camp = obj.VGA_Campaign_Source__c;
                    bbo_lead.camp =   campaignLead.get(obj.id); 
                    bbo_lead.lddt = obj.CreatedDate.date();
                    bbo.setData(bbo_customer, bbo_lead, obj_act.id);
                    BBOEncrypt(bbo.getPayload());
                }
            }
        }
    }
    
    @future(callout=true)
    public static void BBOEncrypt(String payload)
    {
   
        VGA_BBOService bbo = new VGA_BBOService(payload);
        String encrypted_tag = bbo.encrypt();
        Account act = new Account(id = bbo.getAccount(), VGA_BBO_Tag_ID__c = encrypted_tag, VGA_BBO_Tag_Payload__c = payload);
        
        UPDATE act;
        
    }

}