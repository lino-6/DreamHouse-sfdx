public class VGA_BBOCustomer {

    public enum types {UNKNOWN, FACTORY_CAR_ORDER, STOCK_CAR, USED_CAR}
    public types customer_type;
    public String camo;
    public Integer pryr;
    public String camp;
    public Integer puyr;
    public Integer cusi;
    public Date nesd;
    
    private static Map<String, Integer> type_map = new Map<String, Integer>
    {
        'UNKNOWN' => 0,
        'FACTORY_CAR_ORDER' => 1,
        'STOCK_CAR' => 2,
        'USED_CAR' => 3
    };
    
    public VGA_BBOCustomer()
    {
        customer_type = null;
        camo = null;
        pryr = null;
        camp = null;
        puyr = null;
        cusi = null;
        nesd = null;
    }
    
    private Integer getTypeValue(String key)
    {
        return type_map.get(key);
    }
    
    public types getEnumFromValue(Integer value)
    {
        for(String key : type_map.keySet())
        {
            if(type_map.get(key) == value)
            {
                for(types t : types.values())
                {
                    if(t.name() == key)
                    {
                        return t;
                    }
                }
            }
        }
        
        return null;
    }
    
    public override String toString()
    {        
        return '{"type": ' + (customer_type == null ? 'null' : String.valueOf(getTypeValue(customer_type.name())))
            + ', "camo": ' + (camo == null ? 'null' : '"' + camo + '"') + ', "pryr": '+ (pryr == null ? 'null' : String.valueOf(pryr))
            + ', "camp": ' + (camp == null ? 'null' : '"' + camo + '"') + ', "puyr": ' + (puyr == null ? 'null' : String.valueOf(puyr))
            + ', "cusi": ' + (cusi == null ? 'null' : String.valueOf(cusi)) 
            + ', "nesd": ' + (nesd == null ? 'null' : '"' + DateTime.newInstance(nesd.year(), nesd.month(), nesd.day()).format('yyyy-MM-dd') + '"') + '}';        
    }
    
}