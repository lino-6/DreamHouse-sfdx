// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          31-August-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class will be used in all class.

public class VGA_Common 
{
    // -----------------------------------------------------------------------------------------------------------------
    // Get list of picklist values .
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - String - objectAPIName,String - fieldAPIName
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - list<string> 
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                    Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         31-August-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
       public static list<string> getPickListValues(string objectAPIName, string fieldAPIName) 
       {
        list<string> lstPickvals = new list<string>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectAPIName);//From the Object Api name retrieving the SObject
        Sobject Object_name = targetType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(fieldAPIName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values)  //for all values in the picklist list
        {
          lstPickvals.add(a.getValue());//add the value  to our final list
        }
        return lstPickvals;
    }
    // -----------------------------------------------------------------------------------------------------------------
    // Get current Network Id .
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                    Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         01-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
   
    /*public static string getNetworkId() 
    {
        return Network.getNetworkId();
    }*/
    // -----------------------------------------------------------------------------------------------------------------
    // get current Network Name.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - Id
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         01-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    /*public static string getNetworkName(String NetworkId)
    {
        Network objNetwork =[SELECT id, name FROM Network where Id =: NetworkId];
        return objNetwork.Name;
    }*/
    // -----------------------------------------------------------------------------------------------------------------
    // get product record Type Id.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - String -RecordName
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         02-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------

    /*public static string getproductRecordTypeId(string recordName)
    {
        RecordType objRecordType   =[SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'product2' and DeveloperName LIKE :('%' + recordName + '%')];
        return objRecordType.Id;
    }*/
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to get start date of current week.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - date -today
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - date
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         02-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    public static date getWeekStartDate(date today)
    {
        Date startOfWeek = today.toStartOfWeek();
        return startOfWeek;
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to get current logging Account Id.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - string
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         02-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    public static string getloggingAccountId()
    {
         User ObjUser =[select Id,ContactId,Contact.AccountId from User where Id=:UserInfo.getUserId() limit 1];
         return ObjUser.Contact.AccountId;
    }
     // -----------------------------------------------------------------------------------------------------------------
    // This method is used to get all contact related to logging dealer.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - String -Account Id
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - list<Contact>
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         11-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
     public static list<contact> getAllContact(string AccountIds)
     {
         list<contact>objContact =new list<contact>([select Id,Name,VGA_Role__c,VGA_Available_to_Receive_Leads__c,MobilePhone,
                                                     Email from contact where AccountId=:AccountIds]);
         return objContact;  
     }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to get current logging Account Id.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - User
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         11-September-2017           surabhi Ranjan               Initial Version
    // -----------------------------------------------------------------------------------------------------------------
        public static User getloggingAccountDetails()
        {
             User ObjUser =[select Id,ContactId,Contact.AccountId,Contact.Account.Name,
                            Contact.Account.VGA_Brand__c,profile.Name,VGA_Account_Id__c,
                            VGA_Role__c,Contact.VGA_Brand__c,Contact.VGA_Sub_Brand__c,
                            Contact.Account.VGA_Sub_Brand__c,VGA_Brand__c,
                            Contact.Account.VGA_Dealer_Code_Text__c,VGA_Dealer_Code__c,
                            VGA_Account_Name__c,Contact.Account.VGA_Region_Code__c from User where Id=:UserInfo.getUserId() limit 1];
             return ObjUser;
        }

    // -----------------------------------------------------------------------------------------------------------------
    // This Static method returns the comma separated fields for given object API Name
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - String - objectAPIName
    // -----------------------------------------------------------------------------------------------------------------
    //                      
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - Id - recordTypeId
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                    Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         06-Sept-2017           Priyank Rajvanshi                Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    public static string getSobjectFields(String sobjectName) {
        
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(sobjectName).getDescribe().fields.getMap();
        
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()) {
           if(fieldMap.get(fieldName).getDescribe().isAccessible()) {
                    
               if(commaSepratedFields == null || commaSepratedFields == '') {
                    commaSepratedFields = 'select '+fieldName;
                }
                else {
                    commaSepratedFields = commaSepratedFields + ', ' + fieldName;
                }
            }
           
        }
        
        return commaSepratedFields;
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This Static method returns the record type Id for given object API Name & RecordType Name 
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - String - objectAPIName
    // -----------------------------------------------------------------------------------------------------------------
    //                      - String - recordTypeName
    // ----------------------------------------------------------------------------------------------------------------- 
    // RETURNS:             - Id - recordTypeId
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                    Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         06-Sept-2017           Priyank Rajvanshi                Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    public static id GetRecordTypeId(string objectAPIName, string recordTypeName){
        if(IsNullOrEmptyString(objectAPIName) || IsNullOrEmptyString(recordTypeName)){
            return null;
        }
        Map<String, Schema.SObjectType> sobjectSchemaMap;
        if(sobjectSchemaMap == null)
        {
            sobjectSchemaMap = Schema.getGlobalDescribe();
        }
        Schema.SObjectType sObjType = sobjectSchemaMap.get(objectAPIName) ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
        Id recordTypeId = RecordTypeInfo.get(recordTypeName).getRecordTypeId();
        return recordTypeId;
    }
    
    // -----------------------------------------------------------------------------------------------------------------
    // This Static method is used to validate null or Empty String 
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - string    - strInput
    // -----------------------------------------------------------------------------------------------------------------
    // RETURNS:             - boolean   - true/false
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                    Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         06-Sept-2017           Priyank Rajvanshi                Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    public static boolean IsNullOrEmptyString(string strInput){
        if(strInput == null || strInput.equals('')){
            return true;
        }
        return false;
    }
    //===============================================================================
    //Description:This method is used to Convert a DateTime value to its GMT time  
    //===============================================================================
    //Name: Rishi Patel               Date:25-9-17
    //===============================================================================
    public static Datetime converttoGMT(Datetime dt)
    {
        if(dt != null)  
        {
            string GMTTime = dt.format('yyyy-MM-dd HH:mm:ss','GMT');  
            return datetime.valueof(GMTTime);    
        }
        else
        {
            return null;
        }   
    }
    //=========================================================================================
    //Description:This method is used to Convert a DateTime in string formatt to its GMT time  
    //===========================================================================================
    //Name: Rishi Patel               Date:25-9-17
    //===========================================================================================
    public static Datetime convertstrtoGMT(string DtTime)
    {
        if(DtTime != null)  
        {
            Datetime GMTTime = Datetime.valueof(DtTime);
            string strGMTTIME = GMTTime.format('yyyy-MM-dd HH:mm:ss z','GMT');
            return datetime.valueof(GMTTime);    
        }
        else
        {
            return null;
        }   
    }
    //=========================================================================================
    //Description:This method is used get logging user Profile .
    //===========================================================================================
    //Name: Surabhi Ranjan               Date:09-11-17
    //===========================================================================================
    public static list<String> getProfileName()
    {
        list<string>profileName =new list<string>();
        Map<String,string>mapofProfile =new map<String,string>{'Dealer Administrator'    => 'Dealer Administrator'};
        Map<String,string>mapofProfilerole =new map<String,string>{'Consultant'    => 'Consultant',
                                                                  'Lead Controller' =>'Lead Controller',
                                                                  'Dealer Portal Group' =>'Dealer Portal Group',
                                                                  'Dealer Portal' =>'Dealer Portal'
                                                        };
        User objUser = getloggingAccountDetails();
        if(objUser.ContactId !=Null)
        {
            list<VGA_User_Profile__c>objUserProfile =new list<VGA_User_Profile__c>([select Id,VGA_Role__c 
                                                                                    from VGA_User_Profile__c 
                                                                                    where VGA_Contact__c=: objUser.ContactId ]);
            system.debug('objUserProfile=========='+objUserProfile);
            if(objUserProfile !=Null && objUserProfile.size()>0)
            {
                for(VGA_User_Profile__c objectUser  :objUserProfile)
                {
                  if(objectUser.VGA_Role__c != mapofProfilerole.get(objectUser.VGA_Role__c) )
                  {
                     profileName.add('Dealer Administrator');
                  }
                  else if(mapofProfilerole.get(objectUser.VGA_Role__c) =='Consultant')
                  {
                      profileName.add('Consultant');
                  }
                  else if(mapofProfilerole.get(objectUser.VGA_Role__c) =='Lead Controller')
                  {
                    profileName.add('Lead Controller');   
                  }
                  else if(mapofProfilerole.get(objectUser.VGA_Role__c) =='Dealer Portal Group')
                  {
                      
                    profileName.add('Dealer Portal Group');
                      
                  }
                  else if(mapofProfilerole.get(objectUser.VGA_Role__c) =='Dealer Portal')
                  {
                      
                    profileName.add('Dealer Portal');
                      
                  }
                }
               
            }   
        }
        return profileName;
    }
    public static string getSubscriptionRecordTypeId()
    {
        Id devRecordTypeId = Schema.SObjectType.VGA_Subscription__c.getRecordTypeInfosByName().get('Notification Subscription').getRecordTypeId();
        return devRecordTypeId;
    }
    public static string getReportSubscriptionRecordTypeId()
    {
        Id devRecordTypeId = Schema.SObjectType.VGA_Subscription__c.getRecordTypeInfosByName().get('Report Subscription').getRecordTypeId();
        return devRecordTypeId;
    }
    public static void sendExceptionEmail(string strSubject,String strHtmlBody)
    {
        if(Label.VGA_Exception_Email_Address != Null)  
        {
             List<String> lstaddress = Label.VGA_Exception_Email_Address.split(',');
             
             ///////////////////
             list<OrgWideEmailAddress> lstOrgEmailAddress = [select Id,DisplayName,Address from OrgWideEmailAddress where DisplayName =:'VGA Notifications'];
            ///////////////////////
             if(lstaddress != null && !lstaddress.isEmpty())
             {
                 Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                 message.toAddresses = lstaddress;
                 if(lstOrgEmailAddress != null && !lstOrgEmailAddress.isEmpty())
                 {
                    message.setOrgWideEmailAddressId(lstOrgEmailAddress[0].id); 
                 }
                 
                 message.subject = strSubject;
                 message.HtmlBody = strHtmlBody;
                 Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                 try
                 {
                 if(!test.isRunningTest()) 
                    Messaging.sendEmail(messages);
                }
                catch(exception e)
                {
                    system.debug('@@@@'+e.getmessage());
                }
             }
        }
    }
     public static string recordTypeId(string DevelopmentName)
     {
        
         string devRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(DevelopmentName).getRecordTypeId() ;
         
         return devRecordTypeId;
     }
    public static string getuserId(string contactId)
    {
        user objUser =[select Id from User where ContactId=:contactId limit 1];
        return objUser.Id;
    }
    
    public static string toSentenceCase(string str)
    {
        if(str != '')
        {
            //str = str.trim().tolowercase().capitalize();
            
            if(!str.contains(' ') && !str.contains(',') && !str.contains('.'))
            {
                str = str.trim().tolowercase().capitalize();
            }
            else if(!str.contains(' ') && str.contains(','))
            {
                List<String> names = str.split(',');
                
                for(integer i = 0;i<names.size();i++)
                    names[i] = names[i].trim().tolowercase().capitalize();
                str = string.join(names,',');   
            }
            else if(!str.contains(' ') && str.contains('.'))
            {
                List<String> names = str.split('\\.');
                system.debug('@@@names'+names.size());  
                for(integer i = 0;i<names.size();i++)
                    names[i] = names[i].trim().tolowercase().capitalize();
                str = string.join(names,'.');   
            }
            else if(str.contains(' ') && (!str.contains('.') || !str.contains(','))) 
            {
                List<String> names = str.split(' ');
                
                for(integer i = 0;i<names.size();i++)
                    names[i] = names[i].trim().tolowercase().capitalize();
                str = string.join(names,' ');   
            }
                
        }
        return str;     
    }  
    
    public static string getTopMostRole(set<String> lstRoles) 
    {
        string topMostRole = '';
        if(lstRoles != null && !lstRoles.isEmpty())
        {
            if(lstRoles.contains('Dealer Administrator'))
            {
                topMostRole = 'Dealer Administrator';
            }
            else if(!lstRoles.contains('Dealer Administrator') && lstRoles.contains('Lead Controller'))
            {
                topMostRole='Lead Controller';
            }
            else if(!lstRoles.contains('Dealer Administrator') && !lstRoles.contains('Lead Controller') 
                     && lstRoles.contains('Consultant'))
            {
                topMostRole='Consultant';
            }
            else if(!lstRoles.contains('Dealer Administrator') && !lstRoles.contains('Lead Controller') 
                     && !lstRoles.contains('Consultant') && lstRoles.contains('Dealer Portal Group'))
            {
                topMostRole='Dealer Portal Group';
            }
            else if(!lstRoles.contains('Dealer Administrator') && !lstRoles.contains('Lead Controller') 
                     && !lstRoles.contains('Consultant') && !lstRoles.contains('Dealer Portal Group') && lstRoles.contains('Dealer Portal'))
            {
                topMostRole='Dealer Portal';
            }
        }
        return topMostRole; 
    }   
}