@istest
public class VGA_DealerApiTest {
    
    @testsetup
    Static void builddetails()
     {
     
       Account objAccount = VGA_CommonTracker.CreateDealerAccount();
       VGA_Dealership_Profile__c dealership=VGA_CommonTracker.createDealershipProfile(objAccount.id);
       VGA_Trading_Hour__c tradinghours  =VGA_CommonTracker.createTrading_Hour(dealership.id);
         tradinghours.VGA_Day__c='Monday';
         update tradinghours;
       VGA_Public_Holiday__c publicholiday=VGA_CommonTracker.createPublicHoliday(date.today(),'test','daily','');  
      }
    
       /**
    * @author Ashok Chandra
    * @date 21/11/2018
    * @description Method that tests the GetDealerDetails in  DealerApi class
    * RestApi will expose the dealer details 
    * Parameter is dealercode             
    */
    
    static testmethod void dealerapitest(){
      Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getDealerDetails'; 
        req.params.put('dealercode', '123432');
        req.params.put('subbrand', 'PV');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_DealerApi.GetDealerDetails();
    Test.stopTest();   
    }
}