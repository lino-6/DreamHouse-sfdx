// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          25-oct-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to for Invalid Email component.
public class VGA_InvalidEmailController 
{
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to login user Details
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         25-oct-2017             surabhi Ranjan                     Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static user getlogginguser()
    {
        
        return VGA_Common.getloggingAccountDetails();
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to List of Invalid Email 
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         25-oct-2017             surabhi Ranjan                     Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static list<VGA_WrapperofInvalidEmail> getlistofInvalidEmail(String sortField, boolean isAsc)
    {
        list<VGA_WrapperofInvalidEmail>wrapperlist =new list<VGA_WrapperofInvalidEmail>();
        string AccountId =VGA_Common.getloggingAccountId();
        list<VGA_Invalid_Email__c>listofInvalidEmail =new list<VGA_Invalid_Email__c>();
        string queryString;
        queryString = 'select Id,VGA_Brand__c,VGA_Commission_Number__c,VGA_Dealer_Name__c,VGA_Email_Address__c,VGA_First_Name__c,VGA_Last_Name__c,VGA_Type__c,VGA_VIN__c ';
        queryString += ' from VGA_Invalid_Email__c ';
        queryString += ' where VGA_Dealer_Name__c=:AccountId and VGA_Invalid_Email__c = true and VGA_Ignore__c=false';
        
        if (sortField != '') 
        {
            queryString += ' order by ' + sortField;
            if (isAsc) 
            {
                queryString += ' asc';
            } 
            else 
            {
                queryString += ' desc';
            }
        }
        queryString += ' LIMIT 49999';
        listofInvalidEmail = Database.query(queryString);
        if(listofInvalidEmail !=Null && !listofInvalidEmail.isEmpty())  
        {
            for(VGA_Invalid_Email__c objInvalidEmail :listofInvalidEmail)
            {
                VGA_WrapperofInvalidEmail objwrap =new VGA_WrapperofInvalidEmail();
                objwrap.objInvalidEmail     =objInvalidEmail;
                wrapperlist.add(objwrap);
                
            }
        }   
        return wrapperlist;  
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to update Email.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - None
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         25-oct-2017             surabhi Ranjan                     Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static string updateInvalidEmail(string EmailId,string RecordId)
    {
    List<VGA_Invalid_Email__c> invalidemail =new list<VGA_Invalid_Email__c>();
        if(RecordId != null){
            invalidemail = [select Id,VGA_Brand__c  from VGA_Invalid_Email__c where id =: RecordId];
            
        }
        
        list<VGA_Invalid_EmailCS__c>listofInvalidEmail =new list<VGA_Invalid_EmailCS__c>([select Id,VGA_Active__c from VGA_Invalid_EmailCS__c limit 1]);
        if(listofInvalidEmail !=Null && listofInvalidEmail[0].VGA_Active__c==true && listofInvalidEmail.size()>0)
        {
          
            
         
            string StatusValue='';
         
            Http objhttp = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:VGA_InvalidEmail'+'?email='+EmailId);
            req.setMethod('GET');
            req.setTimeout(120000);
            req.setHeader('Content-Type', 'application/json');
             if(invalidemail != null && invalidemail[0].VGA_Brand__c =='Volkswagen' && VGA_MuleUrl__c.getValues('Volkswagen')!=null)
            {
        
        
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);  
        }
        else if(invalidemail[0].VGA_Brand__c =='Skoda' && VGA_MuleUrl__c.getValues('Skoda')!=null)
        {
         req.setHeader('client_id', VGA_MuleUrl__c.getValues('Skoda').Client_ID__c);
         req.setHeader('client_secret', VGA_MuleUrl__c.getValues('Skoda').Secret_Id__c);  
        }
           
            
            HttpResponse res;
            if(!test.isRunningTest())
            {
                res = objhttp.send(req);
            }
            else
            {
                res = new HTTPResponse();
                res.setBody('[{"StatusCode": "0","StatusDescription": "Valid: The email represents a real account / inbox available at the given domain.","EmailAccount": "surabhi.ranjan222","EmailDomain": "gmail.com","Connected": "","Disposable": "False","RoleAddress": "False","ErrorMessage": ""}]');
            }
            JSONParser parser = JSON.createParser(res.getBody());
            while (parser.nextToken() != null)
            {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME))
                {
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if(fieldName == 'StatusCode') 
                    {
                        StatusValue = parser.getText();
                    }
                }
            } 
            
            VGA_Invalid_Email__c objInvalidEmail = New VGA_Invalid_Email__c();
           
           // if(StatusValue.contains('3') || StatusValue.contains('0') || StatusValue.contains('1') )
           // BY Ganesh 08/05/2019
           // Removing contains Check, since Mule return status code is '-1' for Invalid Emails
           if(Integer.valueof(StatusValue) == 3 || Integer.valueof(StatusValue) == 0 || Integer.valueof(StatusValue) == 1 )
            {
                
                objInvalidEmail.VGA_Email_Address__c = EmailId ;
                objInvalidEmail.Id                   = RecordId;
                objInvalidEmail.VGA_Invalid_Email__c =false;
                try
                {
                    Update objInvalidEmail;
                    return 'Email Updated Successfully.';
                }
                catch(exception e)
                {
                    return 'Error: ' + e.getMessage();
                } 
            }
            else
            {
                return 'Error: ' +'Invalid Email Found.';
            }   
        }
        else
        {
            VGA_Invalid_Email__c objInvalidEmail = New VGA_Invalid_Email__c();
            objInvalidEmail.VGA_Email_Address__c = EmailId ;
            objInvalidEmail.Id                   = RecordId;
            objInvalidEmail.VGA_Invalid_Email__c =false; 
            try
            {
                Update objInvalidEmail;
                return 'Email Updated Successfully.';
            }
            catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
        }
        
        
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to Ignore Invalid Email.
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - Record Id
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         13-Nov-2017             surabhi Ranjan                     Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    @AuraEnabled
    public static string updateIgnore(string recordId)
    {
        VGA_Invalid_Email__c objInvalidEmail = New VGA_Invalid_Email__c();
        objInvalidEmail.VGA_Ignore__c        = true ;
        objInvalidEmail.Id                   = recordId;
        try
        {
            Update objInvalidEmail;
            return 'The email has been ignored successfully.';
        }
        catch(exception e)
        {
            return 'Error: ' + e.getMessage();
        } 
    }
    // -----------------------------------------------------------------------------------------------------------------
    // This method is used to loggingUser Details
    // -----------------------------------------------------------------------------------------------------------------
    // INPUT PARAMETERS:    - Record Id
    // -----------------------------------------------------------------------------------------------------------------
    // Version#     Date                       Author                          Description
    // -----------------------------------------------------------------------------------------------------------------
    // 1.0         13-Nov-2017             surabhi Ranjan                     Initial Version
    // -----------------------------------------------------------------------------------------------------------------
    
    @AuraEnabled
    public static string getuserInformation()
    {
        string Administratorprofile;
        string Consultantprofile ='Consultant' ;
        list<string>ProfileName=VGA_Common.getProfileName();
        if(test.isRunningTest())
        {
            ProfileName.add('Dealer Administrator');
        }
        for(string objRole :ProfileName)
        {
            if(objRole.contains('Dealer Administrator'))
            {
                Administratorprofile='Dealer Administrator' ;
            }
        }
        if(Administratorprofile != Null)
        {
            return Administratorprofile;
        }
        else
        {
            return Consultantprofile;   
        }   
    } 
    
    
    
}