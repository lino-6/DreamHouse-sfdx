/**
* @author Lidiya Elizabeth
* @date 24/05/2019
* @description The class is used to test VGA_ServiceIntervalTriggerHandler
*/
@isTest
public class VGA_ServiceIntervalTriggerHandlerTest {
    @isTest
    static void TestAllScenarios(){
        VGA_Triggers__c serviceTrigger =VGA_CommonTracker.buildCustomSettingTrigger('VGA_Service_Interval__c',true);
        insert serviceTrigger;
        Product2 Model=VGA_CommonTracker.createProduct('MERCEDES');
        Model.VGA_Brand__c = 'PV Volkswagen';
        Model.VGA_Model_Year__c='2019'; 
        insert Model;
        List<VGA_Service_Interval__c> IntervalList=new List<VGA_Service_Interval__c>{
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P015','15000kms or 12 months',15000,12,Model.Id,'abc',1),
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P030','30000kms or 24 months',30000,24,Model.Id,'abc',1),
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P045','45000kms or 36 months',45000,36,Model.Id,'abc',3)
        };
        insert IntervalList[0];
        System.assertEquals(IntervalList[0].Id,[select Id from VGA_Service_Interval__c limit 1 ].Id);
        try{
            insert IntervalList[1];
        }catch(exception ex){
            System.assertEquals('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Duplicate Service Order cannot be Accepted.: []',ex.getMessage());
        }

    }
    
}