//=================================================================================
// Tracker class for VGA_scheduleManualLeadAssignment class.
// =================================================================================
// Created by Nitisha Prasad on 27-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_scheduleManualLeadAssignTest
{
    public static VGA_scheduleManualLeadAssignment sh1;
    public static VGA_Manual_Assignment_Configuration__c objPreLaunch;
    public static Product2 objPro;
    
    static testMethod void myUnitTest() 
    {
        LoadData();
        
        sh1 = new VGA_scheduleManualLeadAssignment(objPreLaunch.id);
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
    } 
    
    public static void LoadData() 
    {
        objPro = new Product2();
        objPro.Name = 'test';
        insert objPro;
        
        objPreLaunch = new VGA_Manual_Assignment_Configuration__c();
        insert objPreLaunch;
    }
}