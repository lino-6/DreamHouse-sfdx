/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class VGA_NotificationTriggerHandlerTracker 
{
    public static VGA_Dealer_Notification__c objDealerNotification;
    public static Account objAccount;
    public static Contact objContact;
    
    static testMethod void myUnitTest() 
    {
        loadData();
        Test.startTest();
            objDealerNotification.recordtypeid = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
            objDealerNotification.VGA_Sent_To__c = '+918249714488';
            objDealerNotification.VGA_Message_Body__c ='TEST';
            insert objDealerNotification;
        Test.stopTest();
    }
    static testMethod void myUnitTest2() 
    {
        loadData();
        Test.startTest();
            objDealerNotification.recordtypeid = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            objDealerNotification.Dealer_Contact__c  = objContact.id;
            objDealerNotification.VGA_Message_Body__c = 'test';
            objDealerNotification.VGA_Subscription_Type__c = 'New Lead Notification';
            insert objDealerNotification;
        Test.stopTest();
    }
    static testMethod void myUnitTest3() 
    {
        loadData();
        
        list<VGA_Dealer_Notification__c> lstDealerNotification = new list<VGA_Dealer_Notification__c>();
        
        Test.startTest();
            objDealerNotification.recordtypeid = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            objDealerNotification.Dealer_Contact__c  = objContact.id;
            objDealerNotification.VGA_Message_Body__c = 'test';
            objDealerNotification.VGA_Subscription_Type__c = 'New Lead Notification';
            insert objDealerNotification;
            
            lstDealerNotification.add(objDealerNotification);
            
            VGA_NotificationTriggerHandler.sendEmailFromBatch(lstDealerNotification);
            VGA_NotificationTriggerHandler.sendSMSFromBatch(lstDealerNotification);
        Test.stopTest();
        
    }
    static testMethod void myUnitTest3_1() 
    {
        loadData();
        
        list<VGA_Dealer_Notification__c> lstDealerNotification = new list<VGA_Dealer_Notification__c>();
        
        Test.startTest();
            objDealerNotification.recordtypeid = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            objDealerNotification.Dealer_Contact__c  = objContact.id;
            objDealerNotification.VGA_Message_Body__c = 'test';
            objDealerNotification.VGA_Subscription_Type__c = 'All Escalations';
            objDealerNotification.VGA_Sent_To__c='+918249714488';

            insert objDealerNotification;
            
            lstDealerNotification.add(objDealerNotification);
            
            VGA_NotificationTriggerHandler.sendEmailFromBatch(lstDealerNotification);
            VGA_NotificationTriggerHandler.sendSMSFromBatch(lstDealerNotification);
        Test.stopTest();
        
    }
    
    public static void loadData()
    {
        VGA_Triggers__c obj = new VGA_Triggers__c();
        obj.Name = 'VGA_Dealer_Notification__c';
        obj.VGA_Is_Active__c = true;
        insert obj;
        
        VGA_Notification_Setting__c objSMS = new VGA_Notification_Setting__c();
        objSMS.Name = 'SMS';
        objSMS.VGA_Process__c = true;
        insert objSMS;
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        
        objDealerNotification = new VGA_Dealer_Notification__c();
        
    }
}