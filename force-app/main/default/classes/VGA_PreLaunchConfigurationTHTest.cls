//=================================================================================
// Tracker class for VGA_PreLaunchConfigurationTriggerHandler class.
// =================================================================================
// Created by Nitisha Prasad on 21-Nov-2017.
// =================================================================================
@isTest
private class VGA_PreLaunchConfigurationTHTest
{
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'VGA_Pre_Launch_Configuration__c';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        Product2 objProduct = VGA_CommonTracker.createProduct('test');
        insert objProduct;

        VGA_Pre_Launch_Configuration__c objPre = VGA_CommonTracker.createPreLaunchConfig();
        insert objPre;
    }
    
    /**
    * @method VGA_PreLaunchConfigurationTriggerHandler class
    * @case   When a Pre Launch configuration record is updated 
    * @result then the status is updated to In Progress and the 
    *         VGA_PreLaunchAssignmentBatch starts
    */
    static testMethod void test01_VGA_PreLaunchConfigurationTriggerHandlerUpdate_whenRecordIsUpdated_BatchShouldStartAndStatusShouldBeInProgress() 
    {
        VGA_Pre_Launch_Configuration__c objPre = [SELECT Id, VGA_Status__c, VGA_Start_Batch__c FROM VGA_Pre_Launch_Configuration__c LIMIT 1];
        
        Test.startTest();
          objPre.VGA_Status__c = 'New';
          objPre.VGA_Start_Batch__c = true;
          update objPre;
        Test.stopTest();

        //Make sure the pre launch configuration record status is updated to Closed 
        //after the batch job runs
        VGA_Pre_Launch_Configuration__c updatedPre = [SELECT Id, VGA_Status__c, VGA_Start_Batch__c FROM VGA_Pre_Launch_Configuration__c WHERE Id =: objPre.Id];
        System.assertEquals('Closed', updatedPre.VGA_Status__c);
    }
}