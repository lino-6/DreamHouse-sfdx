public with sharing class RecallController {
       
    public String vin 
    {
      set;
      get;
    }
    
    public String brand 
    {
      set;
      get;
    }    
    
    public String returnURL
    {
      set;
      get;        
    }    
  /*  
    public string sessionVin;
    
    public string getSessionVin() 
    {
        Cache.SessionPartition session = Cache.Session.getPartition('local.myPartitionSession');
        string result = (string)session.get('vga--migration.checkVinStatusResult.checkVinStatusResult_Vin');
        return result;
    }
    
    public void setSessionVin(string value)
    {
        Cache.SessionPartition session = Cache.Session.getPartition('local.myPartitionSession');
        session.put('vga--migration.checkVinStatusResult.Vin', value);
    }
*/
    public List<Asset> validateVIN(string vin, string brand) 
    {
        List<Asset> vehicleList = new List<Asset>();
        
        if(vin == null) 
        {
            return vehicleList;
        }
        
        List<string> list_brand = new List<string>();
        
        if(brand == 'Volkswagen')
        {
            list_brand.add('PV Volkswagen');
            list_brand.add('CV Volkswagen');
        }
        else
        {
            list_brand.add('Skoda');
        }
        
        vehicleList = [SELECT Name, VGA_Cat1_Description__c, VGA_Cat2_Description__c FROM Asset WHERE VGA_VIN__c = :vin AND VGA_Model__r.VGA_Brand__c IN :list_brand LIMIT 1];
        
        return vehicleList;
    }  

    
    public VGA_Service_Campaigns__c getServiceCampaignStatus(string vin) 
    {
                     
        if(vin == null)
        {
            return null;
        }
                        
        List<VGA_Service_Campaigns__c> serviceList = [SELECT Id, VGA_SC_Status__c,passenger_airbag_recall_pra_number__c,driver_airbag_recall_pra_number__c, 
                                                      VGA_SC_Vehicle__r.VGA_Model__r.VGA_model_card_name__c, VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c,driver_airbag_Recall_Scheduled_Commencem__c FROM VGA_Service_Campaigns__c WHERE VGA_SC_Vin__c = :vin AND VGA_SC_Campaign_Type__c = 'Takata Airbag Recall'];
        
        if(serviceList != null && !serviceList.isEmpty())
        {
            return serviceList[0];
        }
        
        return null;
    }

     /**
* @author Ganesh M
* @date 13/06/2019
* @description  As per Service campaign Status we are showing  whether Enter VIN (vehicle) is part of the Takata Airbag Safety Recall on Website
                Added  New values  Stolen,Exported,Completed – AR/AD Values to Service Campaign Status as per new Requirement 
                we need to show active/completed status on website according to Brand for these newly added values Service campaign status vehicles.
*/
    public void checkVinStatus()
    {
        List<Asset> vehicle = validateVIN(vin, brand);
        VGA_Service_Campaigns__c serviceCampaign = getServiceCampaignStatus(vin); 
        
        system.debug(vin);
        system.debug(brand);
        system.debug(vehicle);
        system.debug(serviceCampaign);
        
        String modelName = '';
        string PRANo = '';
        string sdatefinal='';
        if(serviceCampaign!=null){
        if(serviceCampaign.passenger_airbag_recall_pra_number__c != null && serviceCampaign.driver_airbag_recall_pra_number__c == null )
        {
        
            PRANo = serviceCampaign.passenger_airbag_recall_pra_number__c;
           }
           
             else if(serviceCampaign.driver_airbag_recall_pra_number__c != null && serviceCampaign.passenger_airbag_recall_pra_number__c == null){
              
                  PRANo = serviceCampaign.driver_airbag_recall_pra_number__c;
             }
             else if(serviceCampaign.passenger_airbag_recall_pra_number__c != null && serviceCampaign.driver_airbag_recall_pra_number__c != null){
                      PRANo = serviceCampaign.passenger_airbag_recall_pra_number__c + ' and '  + serviceCampaign.driver_airbag_recall_pra_number__c ; 
                  }
           if(serviceCampaign.driver_airbag_Recall_Scheduled_Commencem__c!=null)
           {
              datetime sdate=serviceCampaign.driver_airbag_Recall_Scheduled_Commencem__c;
               sdatefinal=sdate.format('dd/MM/yyyy');
           } 
          }        
        if(!vehicle.isEmpty()) 
        {
            modelName = vehicle[0].VGA_Cat1_Description__c;
            if(vehicle[0].VGA_Cat2_Description__c != null && vehicle[0].VGA_Cat2_Description__c != ' ')
            {
                modelName += ' ' + vehicle[0].VGA_Cat2_Description__c;
            }
        }
      
        
     //   sessionVin = vin;
  
        if(brand == 'Volkswagen')
        {
            if(!vehicle.isEmpty() && serviceCampaign == null)
            {
                returnURL =  URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=1';  
                return;
            }
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=2&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }  
             if(!vehicle.isEmpty() && (serviceCampaign.VGA_SC_Status__c == 'Scheduled' || serviceCampaign.VGA_SC_Status__c == 'Initiated' || serviceCampaign.VGA_SC_Status__c == 'statutory write-off'  || serviceCampaign.VGA_SC_Status__c == 'Stolen' || serviceCampaign.VGA_SC_Status__c == 'Exported' ))
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=3&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }   
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Future' )
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=4&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo+'&Sdate='+sdatefinal;  
                return;
            }  
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed - AR/AD')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=5&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }    
            
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Airbag Removed')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=6&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            } 
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Airbag Deployed' )
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=8&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            } 
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Refused')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=7&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }          
            
            returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=0';
        }
        else 
        {
            if(!vehicle.isEmpty() && serviceCampaign == null)
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=1';  
                return;
            }
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=2&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }  

             if(!vehicle.isEmpty() && (serviceCampaign.VGA_SC_Status__c == 'Scheduled' || serviceCampaign.VGA_SC_Status__c == 'Initiated' || serviceCampaign.VGA_SC_Status__c == 'statutory write-off' || serviceCampaign.VGA_SC_Status__c == 'Stolen' || serviceCampaign.VGA_SC_Status__c == 'Exported'))
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=3&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }    
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Future')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=4&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo+'&Sdate='+sdatefinal;  
                return;
            }  
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed - AR/AD')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=5&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }  
             if(!vehicle.isEmpty() &&  serviceCampaign.VGA_SC_Status__c == 'Airbag Removed') 
             { 
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=6&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Airbag Deployed')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=8&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo+'&Sdate='+sdatefinal;  
                return;
            }    
             if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Refused')
            {
                returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=7&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo;  
                return;
            }           
            
            returnURL = URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=0';            
        }
        System.debug(returnURL);        
    }

    public PageReference submit() {
    
        List<Asset> vehicle = validateVIN(vin, brand);
        VGA_Service_Campaigns__c serviceCampaign = getServiceCampaignStatus(vin); 
        
        system.debug(vin);
        system.debug(brand);
        system.debug(vehicle);
        system.debug(serviceCampaign);
        
        String modelName = '';
        string PRANo = '';
        string sdatefinal='';
        if(serviceCampaign!=null){
        if(serviceCampaign.passenger_airbag_recall_pra_number__c != null && serviceCampaign.driver_airbag_recall_pra_number__c == null )
        {
        
            PRANo = serviceCampaign.passenger_airbag_recall_pra_number__c;
           }
           
             else if(serviceCampaign.driver_airbag_recall_pra_number__c != null && serviceCampaign.passenger_airbag_recall_pra_number__c == null){
              
                  PRANo = serviceCampaign.driver_airbag_recall_pra_number__c;
             }
             else if(serviceCampaign.passenger_airbag_recall_pra_number__c != null && serviceCampaign.driver_airbag_recall_pra_number__c != null){
                      PRANo = serviceCampaign.passenger_airbag_recall_pra_number__c + ' and '  + serviceCampaign.driver_airbag_recall_pra_number__c ; 
                  }
            if(serviceCampaign.driver_airbag_Recall_Scheduled_Commencem__c!=null)
           {
              datetime sdate=serviceCampaign.driver_airbag_Recall_Scheduled_Commencem__c;
               sdatefinal=sdate.format('dd/MM/yyyy');
           } 
          }        
       
        if(!vehicle.isEmpty()) 
        {
            modelName = vehicle[0].VGA_Cat1_Description__c;
            if(vehicle[0].VGA_Cat2_Description__c != null && vehicle[0].VGA_Cat2_Description__c != ' ')
            {
                modelName += ' ' + vehicle[0].VGA_Cat2_Description__c;
            }
        }        
        
        if(brand == 'Volkswagen')
        {
            if(!vehicle.isEmpty() && serviceCampaign == null)
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=1');  
            }
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=2&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo);  
            }       
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Scheduled')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=3&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo);  
            }    
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Future')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=4&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo+'&Sdate='+sdatefinal);  
            }           
            
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/VolkswagenRecallResult?vin='+vin+'&result=0');
        }
        else 
        {
            if(!vehicle.isEmpty() && serviceCampaign == null)
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=1');  
            }
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Completed')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=2&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo);  
            }       
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Scheduled')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=3&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo);  
            }    
            
            if(!vehicle.isEmpty() && serviceCampaign.VGA_SC_Status__c == 'Future')
            {
                return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=4&modelYear='+serviceCampaign.VGA_SC_Vehicle__r.VGA_Model__r.VGA_Year__c+'&modelName='+modelName+'&PRANo='+PRANo+'&Sdate='+sdatefinal);  
            }           
            
            return new PageReference(URL.getSalesforceBaseUrl().toExternalForm() + '/SkodaRecallResult?vin='+vin+'&result=0');            
        }
    }
}