//Tracker class for VGA_picklistValuesController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          7-Nov-2017 
//===============================================================================================
//Coverage for picklistValuesController on 				7-Nov-2017		90%
//===============================================================================================
@isTest
public class VGA_picklistValuesControllerTracker 
{
    public static testmethod void unittest1()
    {
        list<string> lstPicklistValue = VGA_picklistValuesController.PicklistValues('Account','VGA_Brand__c');
        VGA_picklistValuesController.dependentPicklistValues('Account','VGA_Sub_Brand__c','VGA_Brand__c');
    }

}