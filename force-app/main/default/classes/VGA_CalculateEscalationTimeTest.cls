@isTest
private class VGA_CalculateEscalationTimeTest
{
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Lead';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;

        VGA_Profanity__c exactMatchProf = VGA_CommonTracker.buildProfanity('ass');
        exactMatchProf.VGA_Profanity_Exact_Match__c = true;
        insert exactMatchProf;

        VGA_Profanity__c partialMatchProf = VGA_CommonTracker.buildProfanity('stupid');
        partialMatchProf.VGA_Profanity_Exact_Match__c = false;
        insert partialMatchProf;

        Account dealerAcc = VGA_Commontracker.buildDealerAccount('test Dealer', '2505');
        dealerAcc.BillingState = 'NSW';
        dealerAcc.VGA_Timezone__c = 'Australia/Sydney';
        dealerAcc.VGA_Dealer_Code__c = '999';
        insert dealerAcc;

        Contact newContact = VGA_Commontracker.buildDealerContact(dealerAcc.Id, 'testcontact');
        newContact.VGA_Sub_Brand__c   ='Passenger Vehicles (PV)';
        insert newContact;

        VGA_User_Profile__c userProfile = VGA_Commontracker.buildUserProfile('Dealer Administrator', newContact.Id,'Volkswagen', 'Passenger Vehicles (PV)');
        insert userProfile;

        VGA_Commontracker.createWeekWorkingHours(userProfile.Id);

        VGA_Dealership_Profile__c dealershipProfile = VGA_Commontracker.createDealershipProfile(dealerAcc.Id);

        VGA_Commontracker.createWeekTradingHours(dealershipProfile.Id);

        VGA_Public_Holiday__c statePublicHoliday = VGA_Commontracker.createPublicHoliday(Date.today(),'test holiday','Single','NSW');
        insert statePublicHoliday;

        VGA_Public_Holiday__c statePublicHoliday1 = VGA_Commontracker.createPublicHoliday(Date.today().addDays(10),'test holiday1','Recurring','NSW');
        insert statePublicHoliday1;

        VGA_Public_Holiday__c allPublicHoliday = VGA_Commontracker.createPublicHoliday(Date.today().addDays(1),'test holiday all','Single','All');
        insert allPublicHoliday;

        VGA_Public_Holiday__c allPublicHoliday1 = VGA_Commontracker.createPublicHoliday(Date.today().addDays(15),'test holiday1 all','Recurring','All');
        insert allPublicHoliday1;

        List<Lead> dealerLeadsList = VGA_CommonTracker.buildLeads('dealerLeads', 2);
        
        for(Lead newLead : dealerLeadsList){
            newLead.VGA_Dealer_Account__c = dealerAcc.Id;
            newLead.VGA_Assigned_to_Dealership__c = System.Now().addDays(-3);
            newLead.VGA_Brand__c='Volkswagen';
            newLead.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
            newLead.VGA_Dealer_Code__c = '999';
        }
        
        insert dealerLeadsList;
    }
    
        /**
    * @author Lino Diaz
    * @date 16/04/2019
    * @description Method that tests the calculation of the lead duration
    *              that will be the difference between the lead accepted date and
    *              the date when the dealer got assigned to the dealership.
    *              The calculation will happen on the Lead update
    */
    static testMethod void test01_VGA_CalculateLeadDuration_whenLeadIsAccepted_CalculateLeadDuration() 
    {
        List<Lead> lstLead = [SELECT Id, FirstName, LastName, VGA_Brand__c, VGA_Sub_Brand__c, VGA_Escalation__c, VGA_Escalated_Date_Time__c, 
                                    VGA_Dealer_Code__c, VGA_Dealer_Account__c, OwnerId, VGA_Owner_Name__c, VGA_Contact_ID__c, VGA_Current_Owner_Name__c, VGA_Previous_Owner_Name__c, 
                                    VGA_Dealer_Name_Survey__c,VGA_User_2__c,VGA_Outcome__c 
                              FROM Lead];

        Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod = new Map<string,VGA_Dealership_Profile__c>();
        Map<string,list<VGA_Working_Hour__c>> mapofWH = new Map<string,list<VGA_Working_Hour__c>>();
        Map<id,User> mapOfUser = new Map<id,User>();
        Map<Id,User> mapofUserIdwrtaccountid = new Map<Id,User>();
        Map<Id,Contact> mapContact = new Map<Id,Contact>();
        
        Map<String,String> mapofDealerCodeWRTBillingState = new Map<String,String>();
        Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday = new Map<String,List<VGA_Public_Holiday__c>>(); 
        Map<string,VGA_Trading_Hour__c> mapofTH = new Map<string,VGA_Trading_Hour__c>();
        Map<Id, List<VGA_Working_Hour__c>> workHourPerContactIdMap = new Map<Id, List<VGA_Working_Hour__c>>();
        
        
        
        populateMapsWithData(mapofDistributionMethod,
                             mapofPublicHoliday,
                             mapofDealerCodeWRTBillingState,
                             mapofTH,mapofWH,lstLead,workHourPerContactIdMap);
        Contact newCont = [SELECT Id FROM Contact LIMIT 1];

        Datetime escalateDt;
        Datetime escalateDt1;
        Test.startTest();
        for(Lead newLead : lstLead){
            escalateDt = VGA_CalculateEscalationTime.calculateEscalationTime(newLead,mapofDistributionMethod,
                                    mapofPublicHoliday, mapofDealerCodeWRTBillingState,
                                    mapofTH);

            escalateDt1 = VGA_CalculateEscalationTime.calculateEscalationTime(newLead,mapofDistributionMethod,
                                    mapofPublicHoliday, mapofDealerCodeWRTBillingState,
                                    VGA_CalculateHelperClass.getWorkingHoursPerUniqueKey(workHourPerContactIdMap.get(newCont.Id)));
            
        }
        Test.stopTest();
        
        System.assertNotEquals(escalateDt, null); 
        System.assertNotEquals(escalateDt1, null); 
    }

    private static void populateMapsWithData(Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod,
                                             Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday,
                                             Map<String,String> mapofDealerCodeWRTBillingState,
                                             Map<string,VGA_Trading_Hour__c> mapofTH,
                                             Map<string,list<VGA_Working_Hour__c>> mapofWH,
                                             List<Lead> lstLead,
                                             Map<Id, List<VGA_Working_Hour__c>> workHourPerContactIdMap){
        Set<string> setofUniqueKey = new Set<string>();  
        Set<string> setofUniquekey2 = new Set<string>();
        set<String> setofUniqueKey3 = new set<String>();
        Set<String> setDealerCode = new Set<String>();

        for(VGA_Public_Holiday__c objph : [select Id, VGA_Holiday_Date__c, VGA_Holiday_Name__c, VGA_Holiday_Type__c, VGA_State_Region__c from VGA_Public_Holiday__c limit 49999])
        {
            if(!mapofPublicHoliday.containskey(objph.VGA_State_Region__c.tolowercase()))
                mapofPublicHoliday.put(objph.VGA_State_Region__c.tolowercase(),new List<VGA_Public_Holiday__c>());
            
            mapofPublicHoliday.get(objph.VGA_State_Region__c.tolowercase()).add(objph);
        }

        for(Lead objLead : lstLead)
        {
            if(objLead.VGA_Dealer_Code__c != null && objLead.VGA_Brand__c != null && objLead.VGA_Sub_Brand__c != null)
            {   
                setofUniqueKey.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase());    
                setofUniquekey2.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                setofUniqueKey3.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                setDealerCode.add(objLead.VGA_Dealer_Code__c);  
            }
        }

        if(setDealerCode != null && !setDealerCode.isEmpty())
        {
            for(Account objAccount : [select Id, BillingState, VGA_Dealer_Code__c from Account where VGA_Dealer_Code__c IN : setDealerCode AND BillingState != null])
            {
                mapofDealerCodeWRTBillingState.put(objAccount.VGA_Dealer_Code__c,objAccount.BillingState.tolowercase());
            }
            for(VGA_Trading_Hour__c objTH : [select id,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,VGA_End_Time__c,VGA_Dealer_Code__c, VGA_Start_Time__c, VGA_Unique_Key__c, VGA_Working__c,VGA_Timezone__c from VGA_Trading_Hour__c where VGA_Dealer_Code__c in:setDealerCode]){
                if(!mapofTH.containskey(objTH.VGA_Unique_Key__c))
                    mapofTH.put(objTH.VGA_Unique_Key__c.trim().tolowercase(),objTH); 
                
            }
        }

        if(setofUniquekey2 != null && !setofUniquekey2.isEmpty())
        {
            for(VGA_Dealership_Profile__c objDP : [select id, VGA_Brand__c, VGA_Escalate_Minutes__c, VGA_Nominated_User__c, VGA_Distribution_Method__c, VGA_Unique_Key_2__c, Name from VGA_Dealership_Profile__c
                                                   where VGA_Unique_Key_2__c IN : setofUniquekey2
                                                ])
            {
                if(!mapofDistributionMethod.containskey(objDP.VGA_Unique_Key_2__c))
                    mapofDistributionMethod.put(objDP.VGA_Unique_Key_2__c.trim().tolowercase(),objDP);    
            }
        }

        VGA_CalculateHelperClass.getWorkingHoursMap(setofUniqueKey, mapofWH, workHourPerContactIdMap);

    }
}