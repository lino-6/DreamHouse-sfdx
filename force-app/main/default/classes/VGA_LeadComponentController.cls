/**
* @author Hijith NS
* @date 05/Feb/2019
* @description:  getloggedInUserDetails method used to return loggedin user info.
*/

public without sharing class VGA_LeadComponentController{
    
    /**
    * @author Hijith NS
    * @date 05/Feb/2019
    * @description:  Inner class is used to send information to Lightning Compononent.it contains Role ,user details and Ispilot check informations
    */
    Public class VGA_LeadUserInfo{
        @AuraEnabled
        public User LoggedInUser;
        @AuraEnabled
        public String Role;
        @AuraEnabled 
        public Boolean isPilot;
        public VGA_LeadUserInfo(){
            isPilot=false;
            LoggedInUser=null;
            Role=null;
        }
    }
    
	
    /**
	* @author Mohammed Ishaque Shaikh
	* @date 06/Feb/2019 
	* @Ticket #908
	* @description It returns Current role of community user.it is used in lightning component to populate Consultant dropbox
	* @return roles info as String
	*/
    
    public static string getuserInformation(){
        String dealerRole = '';
        List<string>ProfileName = VGA_Common.getProfileName();
        Map<String, Integer> rolesMap = new Map<String, Integer>{'Dealer Administrator'=> 1,'Lead Controller'=> 2,'Consultant'=> 3,'Dealer Portal Group'=> 4,'Dealer Portal'=> 5};
       	for(string objRole :ProfileName){
            if(objRole.contains('Dealer Administrator')) {
                dealerRole = objRole;
                break;
            }
            else if(dealerRole == ''){
                dealerRole = objRole;
            }
            else if(dealerRole != '' && rolesMap.containsKey(dealerRole) && rolesMap.containsKey(objRole)){
                if(rolesMap.get(objRole) < rolesMap.get(dealerRole)){
                    dealerRole = objRole;
                }
            }
        }
        if(dealerRole == ''){
            dealerRole = 'Consultant';
        }
        return dealerRole; 
    }
    
    
    /**
	* @author Hijith NS
	* @date 05/Feb/2019 
	* @Ticket #905
	* @description It returns loggedIn user information like Role ,user details and Ispilot check 
	* @return wrappper class - VGA_LeadUserInfo 
	*/
    @AuraEnabled
    public static VGA_LeadUserInfo getloggedInUserDetails(){
        try{
            VGA_LeadUserInfo Info=new VGA_LeadUserInfo();
            Info.LoggedInUser=[select id,contactId,contact.Account.VGA_Pilot_Dealer__c  from User where id=:UserInfo.getUserId()];
            Info.isPilot=Info.LoggedInUser.contact.Account.VGA_Pilot_Dealer__c;
            Info.Role=VGA_LeadComponentController.getuserInformation();
            return Info;
        }catch(Exception ex){
            System.debug(ex+'==>'+ex.getLineNumber()+'==>'+ex.getMessage());
        }
        return null;
    }
    
    
}