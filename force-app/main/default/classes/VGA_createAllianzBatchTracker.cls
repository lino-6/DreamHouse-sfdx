@isTest
public class VGA_createAllianzBatchTracker
{
    public static VGA_Vehicle_Policy__c objVehicle;
	static testmethod void unitTest1()
    {
        loadData();
        
        Test.startTest();
        	VGA_createAllianzBatch obj = new VGA_createAllianzBatch();
        	Database.executeBatch(obj);
        Test.stopTest();
    }
    public static void loadData()
    {
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'FW24';
        insert objVehicle;  
    }
}