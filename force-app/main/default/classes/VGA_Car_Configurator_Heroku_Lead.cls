@RestResource(urlMapping='/postCarConfiguratorLead/*')
global class VGA_Car_Configurator_Heroku_Lead{
    
    static String Lead_type ='104046';
    static String site_id='1';
    
    
    static String source='';
    static integer brand_did;
    static String modelname='';
    
    /**
* @author Alfahad P M   
* @date 12/07/2019
* @description REST API to post the lead to Salesforce.
*              by using mule api callout
Sample payload
{
"brand": "Volkswagen",
"car_configurator_url": "https://aus.volkswagen.com.au",
"post_code": 2000,
"marketing_opt_in": 1,
"vehicle_interested_in": "Golf R",
"exterior_colour_name": "Pure White",
"exterior_colour_mrrp": 0,
"interior_colour_name": "Titan black",
"interior_colour_mrrp": 100,
"options":  [
{
"name": "R Line",
"mrrp": 2000
},
{
"name": "Sound and Vision",
"mrrp": 3000
}

],    
"model_code": "BQ14NZ/19",
"model_year": 2019.
"model_mrrp": 54322,
"variant": "R",
"image_url": "https://media.volkswagen.com/vw-BQ1-my2019/iris?COSY-EU-100-1713VV%25lXm0kYNazzSU9mu0fO7AkkyyJ1vTvsd2MzppEKhjTtKSK8Cq71MM8H2Lvyr0Q%25UOqggAa5hP4KYwqeQeOOnabyyJ8H3WDZ4HvCJii8CxnOoo0gZrnZlO4Jkoc744q3lMM0kYiD9FFkJ9hO0onq2Tuw99Q1mEiCrZ0ssRdGbQ0aoD77XxMjRMxFvvjkuhb5AZc11TBQ58osULxKPOqqf98SNgtNK9HKswQLAH17w4aqL170%25qY6CF9BvRR5QYVdzYBKXXYXPrttOkUT55P5P999dJp7ZZsoH9iiIE24ffQXTi22UFFsnn4hwDuugUgc00zHjyFF3dzSxxciGHBBhVVsTTklhfWWHnkVGGKuvJJJMXFGllvFeYEELsfjVVbIJyXXYzLBttONtJ55P%25JR99dKcAZZsZ8OiiIz5QffQfQK22U2FCnn4nfRuug1vp00zPZQFF3yy9xxckfHBBh2cnTTkpcSWWH4n3GGKcBgJJMFTYllvK8LEELdEFVVbTbPXXYheBttOior55P%25%25T99dg19ZZsEhGiiIu54ffQ%25xP22UagYnn4Cq%25uuga4c00zp26FF3KEexxciROBBhvmoTTkA6MWWHEkiGGKevLJJMN18llv3T8EELR4TVVbjB3XXYWDkttOobp55PJtl99d9gUZZskEqiiIxnRffQcui22UtMynn4x09uugPgq00zKCkFF3wqfxxcfVIBBhZhiTTkTsyWWHux8GGK0TpJJMvL1llvKdYEEL6sjVVb6myXXYrp6ttOUtv55PdSy99dJ51ZZsMXUiiI1HqffQFTr22UaF5nn4xvOuug97u00zKKiFF3wE%25xxc7yOBBhl9sTTkRbmWWH7N0GGKGvgJJMOX7llvCKYEELEiyVVbNLmXXYXJ8ttO3tw55PJfw99dlDMZZsMzSiiIEHwffQDX722UjDdnn4yzDuugOwr00z1BAFF3ZRUxxciTjBBhLSoTTkmIXWWHwkvGGKO9rJJMRG8llvzWLEELAW4VVbIby&width=1678",
"callback_required": 1,
"email": "alfahad.p@katzion.com",
"first_name": "Alfahad",
"last_name": "P M",
"phone": "9995802118",
"total_drive_price": 60000
}
*/
    
    private static Void getsourceandbrand(String ModelCode)
    {
        if(ModelCode!=null)
        {
            list<product2> produtcode = [select id,VGA_Active_Brochure__c,VGA_Active_Brochure__r.name,VGA_Active_Brochure__r.VGA_Brand__c,VGA_Active_Brochure__r.VGA_Sub_Brand__c from Product2 where ProductCode =:ModelCode];
            if(produtcode != Null && produtcode.size() > 0)
            {
                modelname=produtcode[0].VGA_Active_Brochure__r.name;
            }
            
            if(!produtcode.isEmpty())
            { 
                if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c =='Volkswagen' && produtcode[0].VGA_Active_Brochure__r.VGA_Sub_Brand__c=='Commercial Vehicles (CV)')
                {
                    source='Website';
                    brand_did=101784;
                    
                }
                if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c =='Volkswagen' && produtcode[0].VGA_Active_Brochure__r.VGA_Sub_Brand__c =='Passenger Vehicles (PV)')
                {
                    source='Website';
                    brand_did=101783;
                    
                }
                
                if(produtcode[0].VGA_Active_Brochure__r.VGA_Brand__c=='Skoda')
                {
                    source='Website';
                    brand_did=101785;
                    
                }
                
            }
        }
    } 
    
    private static Void callmuleapi(VGA_CarConfigurator_Lead_Payload data)
    {
        RestResponse res = RestContext.response;
        
        string setbody  = '{"email_address":"'+data.email+ '", '
            + '"lead_type_did":"'+ Lead_type + '", '
            + '"site_id":"'+ site_id+ '", '
            + '"source": "'+ source+ '", '
            + '"brand_did":"'+ brand_did+ '", '
            + '"post_code":"'+ data.post_code+ '", '
            + '"car_configurator_url":"'+ data.car_configurator_url+ '", '
            + '"marketing_opt_in":"'+data.marketing_opt_in + '", '
            + '"vehicle_interested_in":"'+modelname+ '", '
            + '"user1":"'+data.exterior_colour_name+ '", '
            + '"user2":"'+data.interior_colour_name+ '", '
            + '"user3":"'+data.model_code+ '", '
            + '"user4":"'+data.model_year+ '", '
            + '"user5":"'+data.variant+ '", '
            + '"user6":"'+data.model_mrrp+ '", '
            + '"user7":"'+data.exterior_colour_mrrp+ '", '
            + '"car_configurator_image_url":"'+data.image_url+ '", '
            + '"tracking_code_did":"'+data.tracking_code_did+ '", '
            + '"user9":"'+data.interior_colour_mrrp+ '", ';
        
        if(data.options.size() >= 1)
        {
            setbody += '"user10":"'+data.options[0].name+ '", '
                + '"user11":"'+data.options[0].mrrp+ '", ';
        }      
        
        if(data.options.size() >= 2)
        {
            setbody += '"user12":"'+data.options[1].name+ '", '
                + '"user13":"'+data.options[1].mrrp+ '", ';
        }
        
        setbody += '"user14":"'+data.total_drive_price+ '", '
            	+ '"brand":"'+ data.brand+'"';
        
        
        if(data.callback_required == 1)
        {
            setbody+=   +',"follow_up_lead":'+data.callback_required
                + ',"dealer_code":"'+data.dealer_code+'"';
            
        }
        else{
            setbody+=   +',"follow_up_lead": 0';
        }
        
        setbody+=   + ',"first_name":"'+ data.first_name + '", '
            + '"last_name":"'+ data.last_name + '", '
            + '"mobile_phone":"'+ data.phone + '" }' ;      
        
        HttpRequest req1 = new HttpRequest();
        Http http = new Http();
        req1.setHeader('Accept', 'application/json');
        req1.setHeader('Content-type', 'application/json');
        req1.setHeader('client_id', VGA_MuleUrl__c.getValues('Volkswagen').Client_ID__c);
        req1.setHeader('client_secret', VGA_MuleUrl__c.getValues('Volkswagen').Secret_Id__c);
        req1.setEndpoint('callout:VGA_Lead_ExperienceAPI');
        req1.setMethod('POST');
        
        
        req1.setBody(setbody);
        system.debug('request**********'+ req1.getBody());
        JSONGenerator gen = JSON.createGenerator(true); 
        try
        {
            HttpResponse response = http.send(req1);
            String body = response.getBody();
            System.debug(body);
            if (response.getStatusCode() != 200)
            {
                System.debug('The status code returned was not expected: ' +
                             response.getStatusCode() + ' ' + response.getStatus());
                gen.writeStartObject();
                gen.writeStringField('Statusvalue', 'Failure');
                gen.writeNumberField('Statuscode', 404);
                gen.writeEndObject();
                
            }
            else if(response.getStatusCode() == 200)
            {
                System.debug(response.getBody());
                
                gen.writeStartObject();
                gen.writeStringField('Statusvalue', 'Success');
                gen.writeNumberField('Statuscode', 200);
                
                gen.writeEndObject();
                
            }
            else
            {
                gen.writeStringField('Status', 'Fail');
                
            }
        }
        catch(System.CalloutException e)
        {}
        
        res.responseBody = Blob.valueOf(gen.getAsString());
        
        
    }    
    
    @HttpPost
    global Static void PostCarConfiguratorLead() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String jsonString = req.requestBody.toString();
        System.debug(jsonString);
        VGA_CarConfigurator_Lead_Payload data = VGA_CarConfigurator_Lead_Payload.parse(jsonString);
        getsourceandbrand(data.model_code);
        callmuleapi(data);
        System.debug(brand_did);
    }
    
    
}