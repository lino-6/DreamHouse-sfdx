/*
 * Copyright (c) 2017 Punos Mobile Ltd
 */

global with sharing class VWExtensionController {
    
    public List<Task> tasks { get; private set; }
    public List<Task> PDFTasks { get; private set; }
    public List<Task> PDFTasksCompleted { get; private set; }
    public String baseURL   { get; private set; }
    public punosmobile__Meeting__c meeting { get; private set; }
    public Integer rowsLimit   { get; private set; }
    public String PDFBranding { get; private set; }
    public Boolean useAgendaTemplate { get; private set; }
    public String dealerName { get; private set; }
    
    // List of specific attachments
    public List<ContentVersion> FileList { get; private set; }
    
    private ApexPages.StandardController stdController;
    global VWExtensionController(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        
        meeting = (punosmobile__Meeting__c) stdController.getRecord();
        rowsLimit = 10;
        useAgendaTemplate = false;
        
        if (Schema.sObjectType.punosmobile__Meeting__c.isQueryable() && Schema.SObjectType.punosmobile__Meeting__c.isAccessible()){
            meeting = [SELECT 
                        Name,
                        OwnerId,
                        punosmobile__Related_To__c,
                        punosmobile__Event__c
                        FROM punosmobile__Meeting__c
                        WHERE Id = :meeting.Id];
        } else {
            system.debug('User has no rights to query Meeting Assistants Meeting__c object');
            return;
        }
        
        Id currentUserId = UserInfo.getUserId();
        
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        
		// Get dealer name for display on PDF
		List<Account> dealerNameList = [SELECT Id,
			Name
			FROM Account
			WHERE
			Id = :meeting.punosmobile__Related_To__c LIMIT 1 ]; 

        if(!dealerNameList.isEmpty()){
      		dealerName = dealerNameList[0].Name;
			//System.Debug(dealerName);    
        }

        
        tasks = [SELECT 
                    Id, 
                    Subject, 
                    ActivityDate
                FROM Task
                WHERE 
                    // Status != 'Completed'
                    IsClosed = false
                 	AND OwnerId = :meeting.OwnerId
                    AND WhatId != NULL
                    AND WhatId = :meeting.punosmobile__Related_To__c
                ORDER BY ActivityDate ASC NULLS LAST LIMIT 1000];
                
         PDFTasks = [SELECT 
                    Id, 
                    Subject, 
                    ActivityDate,
                    Status,
                    IsClosed,
                    punosmobile__Meeting_ID__c,
                    Who.Name
                FROM Task
                WHERE 
                    punosmobile__Meeting_ID__c != :meeting.Id
                    AND IsClosed = False
					// AND Status != 'Completed'
                    AND OwnerId = :meeting.OwnerId
                    AND WhoID != NULL
                    AND WhatId != NULL
                    AND WhatId = :meeting.punosmobile__Related_To__c
                    // If Status = Completed get tasks when due date is inside 1 month time frame. If no due date get tasks that are Completed by CreatedDate 1 month in past.
                    // Get all tasks that are not completed regardles of when they were created or due date.
                    AND ((((ActivityDate = LAST_N_DAYS:30 OR ActivityDate = NEXT_N_DAYS:30) OR (ActivityDate = NULL AND CreatedDate = LAST_N_DAYS:30)) AND Status = 'Completed') OR Status != 'Completed')
                ORDER BY ActivityDate ASC NULLS LAST];
                
        PDFTasksCompleted = [SELECT 
                            Id, 
                            Subject, 
                            ActivityDate,
                            IsClosed,
                            Status,
                            punosmobile__Meeting_ID__c,
                            Who.Name
                        FROM Task
                        WHERE 
                            punosmobile__Meeting_ID__c != :meeting.Id
                            // AND Status = 'Completed'
                            AND IsClosed = true
                            AND OwnerId = :meeting.OwnerId
                            AND WhoID != NULL
                            AND WhatId != NULL
                            AND WhatId = :meeting.punosmobile__Related_To__c
                            // If Status = Completed get tasks when due date is inside 1 month time frame. If no due date get tasks that are Completed by CreatedDate 1 month in past.
                            // Get all tasks that are not completed regardles of when they were created or due date.
                            AND ((((ActivityDate = LAST_N_DAYS:30 OR ActivityDate = NEXT_N_DAYS:30) OR (ActivityDate = NULL AND CreatedDate = LAST_N_DAYS:30)) AND Status = 'Completed') OR Status != 'Completed')
                        ORDER BY ActivityDate ASC NULLS LAST];
        
        // Show all tasks when opening in separate window
        String showMore = Apexpages.currentpage().getparameters().get('showmore');
        
        if (showMore != null && showMore.equalsIgnoreCase('true')) {
            rowsLimit = tasks.size();
        }
        
        // Use agenda template when creating PDF from Preparation
        String template = Apexpages.currentpage().getparameters().get('agendaTemplate');
        
        if (template != null && template.equalsIgnoreCase('true')) {
            useAgendaTemplate = true;
        }

            
        // Define what branding should be used        
        String userRoleId = String.valueOf(UserInfo.getProfileId().substring(0, 15));
        MA_PDF_Branding__c[] branding = [SELECT Used_Branding__c FROM MA_PDF_Branding__c WHERE User_Profile_Id__c = :userRoleId LIMIT 1];
        
        if (branding.size() > 0) {
            PDFBranding = branding[0].Used_Branding__c; 
        }
        
                //List all files related to the Event and with sharing permission
               	//Get all Files and Attachments from the Event
            //Associate all Files and Attachments with the Task
            //Files
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
        ID eventID = meeting.punosmobile__Event__c;
        contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :eventID];
        if(!contentDocumentLinks.isEmpty()){
        	
        List<Id> CDLIds = new List<Id>();

        for (ContentDocumentLink cdl : contentDocumentLinks ){
            CDLIds.add(cdl.ContentDocumentId);
        }

		List<ContentVersion> contentVersionsShared = [SELECT Id, Title FROM ContentVersion WHERE ContentDocumentId IN :CDLIds AND Share_with_public_notes__c = true];
        
        if(!contentVersionsShared.isEmpty()){          
            Filelist = contentVersionsShared;                
        }
        
        }
		
        
    }

}