@isTest
public class VGA_ActiveBrochureAPITest {
    
    @testSetup
    static void buildConfigList() {
        Set<String> namesSet = new Set<String>{'Polo', 'Arteon'};
        List<VGA_Active_Brochure__c> activeBrochuresList = VGA_TestDataFactory.buildActiveBrochures(namesSet);
        insert activeBrochuresList;

        Set<String> prefNamesSet = new Set<String>{'Trendline', 'I don\'t mind'};
        List<VGA_Model_Attribute__c> modelAttributeList = VGA_TestDataFactory.buildModelAttributes(prefNamesSet, activeBrochuresList[0].Id);
        
        prefNamesSet = new Set<String>{'Trendline', 'Comfortline'};
        modelAttributeList.addAll(VGA_TestDataFactory.buildModelAttributes(prefNamesSet, activeBrochuresList[1].Id));
        insert modelAttributeList;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 10/12/2018
    * @description Test method for the VGA_ActiveBrochureAPI.
    *              It tests the getActiveBrochures method that returns the list of active
    *              brochures and its related model attibutes.
    */
    @isTest
    public static void test01_GetActiveBrochures_WhenCallingActiveBrochuresAPI_WeShouldGetRightData()
    {
        RestRequest req = new RestRequest();
        req.params.put('brand', 'Volkswagen');
        RestResponse res = new RestResponse();

        RestContext.request = req;
        RestContext.response = res;

        Test.startTest();

        VGA_ActiveBrochureAPI.getActiveBrochures();

        Test.stopTest();

        
        System.debug(EncodingUtil.base64Encode(res.responseBody));
        //System.assert(..., ..., '...');
    }
}