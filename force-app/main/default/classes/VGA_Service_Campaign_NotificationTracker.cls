@isTest
private class VGA_Service_Campaign_NotificationTracker
{
    
   @testsetup
 Static void buildServiceCampaigndetails()
 {    
        VGA_Triggers__c CustomTrigger =new VGA_Triggers__c();
        CustomTrigger = VGA_CommonTracker.buildCustomSettingTrigger('VGA_Service_Campaign_Notification__c',true);
        insert CustomTrigger;  
        
         Account objAcc = VGA_CommonTracker.createDealerAccount();
         Asset objAsset = VGA_CommonTracker.createAsset('test');
               objAsset.VGA_VIN__c = 'WVWZZZAUZJP002582';
               objAsset.AccountId = objAcc.id;
        
        insert objAsset;
        
       Id SC_RTId = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();
       VGA_Service_Campaigns__c  objservicecampaign  = VGA_CommonTracker.buildTakataServiceCampaing();
                                 objservicecampaign.RecordTypeId = SC_RTId;
                                 objservicecampaign.driver_airbag_inflator_affected__c = 'y';
                                 objservicecampaign.VGA_SC_Vehicle__c = objAsset.Id;
                                 objservicecampaign.VGA_SC_Status__c  = 'Scheduled';
       
        Insert objservicecampaign;
  }     
  
     /**
    * @author Ganesh M
    * @date 31/10/2018
    * @description Method that tests updated Service Campaign Status When Notification is Created.
    */
   static testMethod void test01_UpdateServiceCampaigns_whenNotificationsCreated() 
    { 
       
     Test.startTest();
            VGA_Service_Campaigns__c sc=[select id, VGA_SC_Vehicle__c,VGA_SC_Status__c,VGA_SC_Vin__c,driver_airbag_date_replaced__c,driver_airbag_inflator_affected__c   from VGA_Service_Campaigns__c  where  VGA_SC_Vin__c  ='12345'];
            
             VGA_Service_Campaign_Notification__c servicenotificationobj = new VGA_Service_Campaign_Notification__c();
                
            servicenotificationobj.Name = 'Test';
            servicenotificationobj.VGA_SC_ID__c = sc.Id;
            servicenotificationobj.VGA_SC_Code__c = '69Q7';
            servicenotificationobj.VGA_SC_Notification_Method__c = 'Email';
            servicenotificationobj.VGA_SC_Notification_Date__c = System.today();
            servicenotificationobj.VGA_SC_Notification_Status__c = 'Sent';
            servicenotificationobj.VGA_SC_Notification_Type__c = 'First';
        
              Insert servicenotificationobj;
        
        Test.stopTest();
        
        
           VGA_Service_Campaigns__c sc1=[select id,VGA_SC_Status__c, VGA_SC_Vehicle__c,VGA_SC_Vin__c,driver_airbag_date_replaced__c,Driver_Airbag_Recall_Initiated__c  from VGA_Service_Campaigns__c  where  Id = :sc.Id];
           
         // System.assertEquals(sc1.Driver_Airbag_Recall_Initiated__c ,system.today());
          //System.assertEquals(sc1.VGA_SC_Status__c ,'Initiated');
        
        
    }
}