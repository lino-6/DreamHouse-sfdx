/**
* @author Ganesh M
* @date 25/10/2018
* @description Class Updating Passengers,Driver details for Takata Service Campaigns 
*/
public with sharing class VGA_Service_CampaignHandler
{
    public void runTrigger()
    {
        if(trigger.isBefore && Trigger.isInsert)
        {
            populateVechile((List<VGA_Service_Campaigns__c>) trigger.new,null);
            insertMandatoryFields((List<VGA_Service_Campaigns__c>) trigger.new);
        }
        
        if (Trigger.isBefore && Trigger.isUpdate)
        {
            updateTakataRecallDetails((List<VGA_Service_Campaigns__c>) trigger.new, (Map<Id, VGA_Service_Campaigns__c>) trigger.OldMap); 
            populateVechile((List<VGA_Service_Campaigns__c>) trigger.new, (Map<Id, VGA_Service_Campaigns__c>) trigger.OldMap);
        }
        
    } 
    
    /**
    * @author Ganesh M
    * @date 25/10/2018
    * @description It updating Driver_air_bag_replaced,Date and Passenger_air_bag_replaced,Date when status got updated 
    * @param  lsttriggernew is a collection of new service campaigns records
    * @param   triggeroldmap is a collection of old service campaigns records which needs to be updated 
    * @return  Null
    */
    Private static void updateTakataRecallDetails(list<VGA_Service_Campaigns__c> lsttriggernew,map<Id,VGA_Service_Campaigns__c> triggeroldmap)
    {
        
        Id SC_RTId = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId(); 
        
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        { 
            
            for(VGA_Service_Campaigns__c SC : lsttriggernew )
                
            {
                
                if(SC.RecordTypeId == SC_RTId && SC.VGA_SC_Status__c == 'Completed')
                {
                    
                    if(SC.Passenger_airbag_inflator_affected__c == 'Y' && SC.VGA_SC_Completetion_Date__c != null)
                        
                    {
                        SC.Passenger_airbag_replaced__c = 'Y'; 
                        SC.Passenger_airbag_date_replaced__c = SC.VGA_SC_Completetion_Date__c;
                    }  
                    
                    SC.Driver_airbag_replaced__c = 'Y';
                    SC.Driver_airbag_date_replaced__c = SC.VGA_SC_Completetion_Date__c;
                    
                }
                if(SC.RecordTypeId == SC_RTId && SC.VGA_SC_Status__c == 'Scheduled')
                {  
                    SC.VGA_Scheduled_Date__c = date.today();
                }  
                
            }
        }
    }
    
    /**
    * @author Ashok Chandra
    * @date 30/11/2018
    * @description It will update the vechile on service campaign based on VIN.
    * @param  lsttriggernew is a collection of new service campaigns records

    */
    Private static void populateVechile(list<VGA_Service_Campaigns__c> lsttriggernew, map<Id,VGA_Service_Campaigns__c> triggeroldmap)
    {
        set<string> vinset=new set<string>();
        Map<string,id> vinVechilemap=new Map<string,id>();
        Map<id,string> vinVechilesubbrandmap=new Map<id,string>();
        
        for(VGA_Service_Campaigns__c Vsc: lsttriggernew)
        {
            if(Vsc.VGA_SC_Vin__c!=null || (Vsc.VGA_SC_Vin__c!=null && Vsc.VGA_SC_Vin__c!=triggeroldmap.get(Vsc.id).VGA_SC_Vin__c))
                vinset.add(Vsc.VGA_SC_Vin__c);        
        }
        
        for(Asset Vechile:[select id,VGA_VIN__c,product2.VGA_Sub_brand__c from asset where VGA_VIN__c in :vinset])
        {
            vinVechilemap.put(Vechile.VGA_VIN__c,Vechile.id);
            vinVechilesubbrandmap.put(Vechile.id,Vechile.product2.VGA_Sub_brand__c);
        }
        
        for(VGA_Service_Campaigns__c Vsc: lsttriggernew)
        {
            Vsc.VGA_SC_Vehicle__c  = vinVechilemap.get(Vsc.VGA_SC_Vin__c);       
            vsc.VGA_SC_Sub_Brand__c= vinVechilesubbrandmap.get(vinVechilemap.get(Vsc.VGA_SC_Vin__c));   
        }
        
        
        
    } 
    
    /**
    * @author Lidiya
    * @date 11/02/2019
    * @description Updates all other mandatory fields whenever mule insert a 
    *              record to service campagin object. If we are inserting a service campaign that 
    *              already exits in Salesforce we will need to find it and update it.
    *              We will use the external Id field to find the related service campaigns that we
    *              need to update.
    * @param  lsttriggernew is a collection of new service campaigns records
    */
    private static void insertMandatoryFields(list<VGA_Service_Campaigns__c> lsttriggernew)
    {
        Map<String, VGA_Service_Campaigns__c> serviceCampaignsPerVINMap = getServiceCampaignsPerVINMap(lsttriggernew);
        Map<Id, VGA_Service_Campaigns__c> serviceCampaignsToUpdateMap  = new Map<Id, VGA_Service_Campaigns__c>();
        

        Set<Id> servCampaignIdSet = new Set<Id>();
        Set<String> takataServCode    = getTakataServiceCampignCodesSet();
        Set<String> emissionsServCode = getEmissionsServiceCampignCodesSet();
        Map<String, List<String>> dependenciesMap = VGA_dependentPicklist.getFieldDependencies('VGA_Service_Campaigns__c','VGA_SC_Campaign_Type__c','Campaign_Code__c');
        
        for(VGA_Service_Campaigns__c serviceCampaign : lsttriggernew)
        {
            System.debug(serviceCampaignsPerVINMap);
            if(serviceCampaignsPerVINMap.containsKey(serviceCampaign.VGA_SC_Vin__c)){
                updateRelatedServiceCampaign(serviceCampaignsPerVINMap.get(serviceCampaign.VGA_SC_Vin__c),serviceCampaign, serviceCampaignsToUpdateMap);
            }
            
            //Map the campaign code name with the VIN value from mule
            serviceCampaign.Name = serviceCampaign.VGA_SC_Vin__c;
            
            if( serviceCampaign.VGA_SC_Code__c!=null)
            {
                serviceCampaign.VGA_SC_Campaign_Type__c = getSCCampaignType(serviceCampaign.VGA_SC_Code__c, dependenciesMap);
                serviceCampaign.Campaign_Code__c = serviceCampaign.VGA_SC_Code__c;

                //Check if the campaign code to decide the record type and campaign type values
                if(takataServCode.contains(serviceCampaign.VGA_SC_Code__c))
                {
                    serviceCampaign.recordtypeID = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();
                }
                else if(emissionsServCode.contains(serviceCampaign.VGA_SC_Code__c))
                {
                    serviceCampaign.recordtypeID = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Emissions').getRecordTypeId();
                }
                else
                {
                    serviceCampaign.recordtypeID = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Standard Service Campaign').getRecordTypeId();
                }
            } 
        }
        
        if(serviceCampaignsToUpdateMap.size()!=0){
            update serviceCampaignsToUpdateMap.values();
        }
    }

    /**
    * @author Lidiya
    * @date 28/03/2019
    * @description Method that returns the Campaign Type for an specific campaign code.
    *              We are getting the value for each code from the dependent picklist defined in 
    *              Salesforce between the VGA_SC_Campaign_Type__c and Campaign_Code__c fields
    * @param sCCode Service Campaign code for which we need to find the Campaign Type
    * @return  String with the value of the campaign type related to the sc code
    */
    private static String getSCCampaignType(String sCCode, Map<String, List<String>> servCampDependenciesMap){
        List<String> serviceCampaignCodeList = sCCode.split(';');

        if(serviceCampaignCodeList.size()!=0){
            String serviceCampaignCode = serviceCampaignCodeList[0];
            for(String data : servCampDependenciesMap.keyset()){
                List<String> myvaluesList = servCampDependenciesMap.get(data);
                for(String campagincode : myvaluesList){
                    if(campagincode == serviceCampaignCode
                       && data != null){
                        return data;
                    }
                    
                }
            } 
        }

        return Label.VGA_Standard_Campaign_Type;  
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 21/03/2019
    * @description Method that returns a Set with the codes beign used for Takata campaigns.
    *              The list of codes is being stored in a custom label called VGA_Service_Campaigns_Codes_Takata
    *              that will need to be updated every time a new campaign code is added.
    * @return  Set<String> Set with the Service Campaign Codes that are user for Takata campaigns
    */
    private static Set<String> getTakataServiceCampignCodesSet(){
        Set<String> takataCodeSet = new Set<String>();
        
        takataCodeSet = new Set<String> (Label.VGA_Service_Campaigns_Codes_Takata.split('\\|'));
        return takataCodeSet;
        
        
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 21/03/2019
    * @description Method that returns a Set with the codes beign used for Emissions campaigns.
    *              The list of codes is being stored in a custom label called VGA_Service_Campaigns_Codes_Takata
    *              that will need to be updated every time a new campaign code is added.
    * @return  Set<String> Set with the Service Campaign Codes that are user for Emissions campaigns
    */
    private static Set<String> getEmissionsServiceCampignCodesSet(){
        Set<String> emissionsCodeSet = new Set<String>();
        
        emissionsCodeSet = new Set<String> (Label.VGA_Service_Campaign_Codes_Emissions.split('\\|'));
        return emissionsCodeSet;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 21/03/2019
    * @description Method that returns a Map with Service Campaigns group by VIN.
    *              We will find the service campaigns with the same VIN as the one that we are inserting
    *              and if the External Id is contained in a Service Campaign that exists in Salesforce we
    *              will update it with the values coming in the new created one.
    * @return  Map<String, VGA_Service_Campaigns__c> Map of Service Campaigns group by VIN.
    */
    private static Map<String, VGA_Service_Campaigns__c> getServiceCampaignsPerVINMap(List<VGA_Service_Campaigns__c> serviceCampaignList){
        Map<String, VGA_Service_Campaigns__c> serviceCampaignsPerVINMap = new Map<String, VGA_Service_Campaigns__c>();
        Map<String, List<VGA_Service_Campaigns__c>> serviceCampaignListPerVINMap = new Map<String, List<VGA_Service_Campaigns__c>>();
        
        for(VGA_Service_Campaigns__c serviceCampaign : serviceCampaignList){
            if(serviceCampaignListPerVINMap.containsKey(serviceCampaign.VGA_SC_Vin__c)){
                serviceCampaignListPerVINMap.get(serviceCampaign.VGA_SC_Vin__c).add(serviceCampaign);
            } else {
                List<VGA_Service_Campaigns__c> tempList = new List<VGA_Service_Campaigns__c>();
                tempList.add(serviceCampaign);
                serviceCampaignListPerVINMap.put(serviceCampaign.VGA_SC_Vin__c, tempList);
            }
        }
        
        if(serviceCampaignListPerVINMap.size()!=0){
            for(VGA_Service_Campaigns__c oldServCampaign : [SELECT Id, VGA_External_ID__c,VGA_SC_Vin__c
                                                            FROM VGA_Service_Campaigns__c
                                                            WHERE VGA_SC_Vin__c IN: serviceCampaignListPerVINMap.KeySet()
                                                            AND VGA_External_ID__c LIKE '%;%']){
                                                            
                if(serviceCampaignListPerVINMap.containsKey(oldServCampaign.VGA_SC_Vin__c)){
                    for(VGA_Service_Campaigns__c newServCampaign : serviceCampaignListPerVINMap.get(oldServCampaign.VGA_SC_Vin__c)){
                        if(oldServCampaign.VGA_External_ID__c != null
                            && newServCampaign.VGA_External_ID__c != null
                            && oldServCampaign.VGA_External_ID__c.contains(newServCampaign.VGA_External_ID__c)
                            && !serviceCampaignsPerVINMap.containsKey(oldServCampaign.VGA_SC_Vin__c)){
                                serviceCampaignsPerVINMap.put(oldServCampaign.VGA_SC_Vin__c, oldServCampaign);
                            }
                    }
                }
            }
        }
        
        return serviceCampaignsPerVINMap;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 21/03/2019
    * @description Method that updates a Service Campaign in Salesforce with the values of a new created
    one that has been completed.
    * @param  oldServiceCampaign Service Campaign that already exists in Salesforce and that needs to be updated.
    * @param  newServiceCampaign New created Service Campaign which matches with one existing in Salesforce
    * @param  serviceCampaignsToUpdateMap Map where we will be adding all the service campaigns that needs to be updated
    *         We should use a Map to avoid having duplicate values on the list that we
    *         are trying to update.
    */
    private static void updateRelatedServiceCampaign(VGA_Service_Campaigns__c oldServiceCampaign,
                                                     VGA_Service_Campaigns__c newServiceCampaign, 
                                                     Map<Id, VGA_Service_Campaigns__c> serviceCampaignsToUpdateMap){
        
        if(!serviceCampaignsToUpdateMap.containsKey(oldServiceCampaign.Id)
           || (serviceCampaignsToUpdateMap.containsKey(oldServiceCampaign.Id)
               && newServiceCampaign.VGA_SC_Status__c == 'Completed')
           ){
            oldServiceCampaign.VGA_SC_Completetion_Date__c = newServiceCampaign.VGA_SC_Completetion_Date__c;
            oldServiceCampaign.VGA_SC_Status__c            = newServiceCampaign.VGA_SC_Status__c;
            
            serviceCampaignsToUpdateMap.put(oldServiceCampaign.Id, oldServiceCampaign);
        }   
    }        
}