global class VGA_LeadAssignmentBatch implements Database.Batchable<sObject>, Database.AllowsCallouts,Database.Stateful
{
    global Boolean BatchCheck;
    global String  Query;
    global List<String> ExceptionList;


    global VGA_LeadAssignmentBatch(){
        ExceptionList=new List<String>();
        try{
            //Abort the previous job with same name that the one that we are going to reschedule
            VGA_LeadBatchHelper.abortPreviousJob('LeadAssignmentJob%', '');

            Id queueId = [select Id from Group where Type = 'Queue' AND Name = 'Dealership Queue'].Id;
            Query = 'Select Id, FirstName, LastName, VGA_Brand__c, VGA_Sub_Brand__c, VGA_Dealer_Code__c, VGA_Dealer_Account__c, OwnerId, VGA_Assignment_Done__c, VGA_Contact_Id__c, VGA_Owner_Name__c, VGA_Contact_Mobile__c, VGA_Contact_Email__c from Lead where Status = \'New\' AND OwnerId =\''+queueId+'\''; 
            BatchCheck=false;
            System.debug('QUERY IS \n'+Query);
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        
    }


    global Database.querylocator start(Database.BatchableContext bc){   
        try{
            return Database.getQueryLocator(Query);
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            return Database.getQueryLocator('select id from Lead where id=\'\'');
        }    
           
    }

    
    global void execute(Database.BatchableContext BC, List<Lead> lstLead)
    {
        if(lstLead != null && !lstLead.isEmpty())
        {
            try
            {
                Set<string> setofUniqueKey = new Set<string>();  
                Set<string> setofUniquekey2 = new Set<string>();
                set<String> setofUniqueKey3 = new set<String>();
                
                Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod = new Map<string,VGA_Dealership_Profile__c>();
                Map<string,list<VGA_Working_Hour__c>> mapofWH = new Map<string,list<VGA_Working_Hour__c>>();
                Map<id,User> mapOfUser = new Map<id,User>();
                Map<Id,User> mapofUserIdwrtaccountid = new Map<Id,User>();
                Map<Id,Contact> mapContact = new Map<Id,Contact>();
                Set<String> setDealerCode = new Set<String>();
                Map<String,String> mapofDealerCodeWRTBillingState = new Map<String,String>();
                Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday = new Map<String,List<VGA_Public_Holiday__c>>(); 
                
                Map<string,VGA_Trading_Hour__c> mapofTH=new Map<string,VGA_Trading_Hour__c>(); 
                
                
                List<Lead> lstUpdateLead = new List<Lead>();
                
                for(User objUser : [select Id, ContactId, AccountID from User where ContactId != Null AND isActive = true limit 49999])       
                {    
                    mapOfUser.put(objUser.contactId,objUser);   
                    mapofUserIdwrtaccountid.put(objUser.id,objUser);      
                }
                
                for(VGA_Public_Holiday__c objph : [select Id, VGA_Holiday_Date__c, VGA_Holiday_Name__c, VGA_Holiday_Type__c, VGA_State_Region__c from VGA_Public_Holiday__c limit 49999])
                {
                    if(!mapofPublicHoliday.containskey(objph.VGA_State_Region__c.tolowercase()))
                        mapofPublicHoliday.put(objph.VGA_State_Region__c.tolowercase(),new List<VGA_Public_Holiday__c>());
                    
                    mapofPublicHoliday.get(objph.VGA_State_Region__c.tolowercase()).add(objph);
                }

            
            
                for(Lead objLead : lstLead)
                {
                     if(objLead.VGA_Dealer_Code__c != null && objLead.VGA_Brand__c != null && objLead.VGA_Sub_Brand__c != null)
                     {   
                         setofUniqueKey.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase());    
                         setofUniquekey2.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                         setofUniqueKey3.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                         setDealerCode.add(objLead.VGA_Dealer_Code__c);  
                     }
                }
                
                if(setDealerCode != null && !setDealerCode.isEmpty())
                {
                    for(Account objAccount : [select Id, BillingState, VGA_Dealer_Code__c from Account where VGA_Dealer_Code__c IN : setDealerCode AND BillingState != null])
                    {
                        mapofDealerCodeWRTBillingState.put(objAccount.VGA_Dealer_Code__c,objAccount.BillingState.tolowercase());
                    }
                    for(VGA_Trading_Hour__c objTH : [select id,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,VGA_End_Time__c,VGA_Dealer_Code__c, VGA_Start_Time__c, VGA_Unique_Key__c, VGA_Working__c,VGA_Timezone__c from VGA_Trading_Hour__c where VGA_Dealer_Code__c in:setDealerCode]){
                        if(!mapofTH.containskey(objTH.VGA_Unique_Key__c))
                            mapofTH.put(objTH.VGA_Unique_Key__c.trim().tolowercase(),objTH); 
                        
                    }
                }
                
                if(setofUniquekey2 != null && !setofUniquekey2.isEmpty())
                {
                    for(VGA_Dealership_Profile__c objDP : [select id, VGA_Brand__c, VGA_Escalate_Minutes__c, VGA_Nominated_User__c, VGA_Distribution_Method__c, VGA_Unique_Key_2__c, Name from VGA_Dealership_Profile__c
                                                           where VGA_Unique_Key_2__c IN : setofUniquekey2])
                    {
                        if(!mapofDistributionMethod.containskey(objDP.VGA_Unique_Key_2__c))
                            mapofDistributionMethod.put(objDP.VGA_Unique_Key_2__c.trim().tolowercase(),objDP);    
                    }
                }
                
                if(setofUniqueKey != null && !setofUniqueKey.isEmpty())
                {
                    for(VGA_Working_Hour__c objWH :  [select Id, VGA_Brand__c, VGA_Day__c, VGA_Dealer_Code__c, VGA_Contact_ID__c, VGA_End_Time__c,
                                                      VGA_Start_Time__c, VGA_Last_Assigned_Date_Time__c, VGA_Sub_Brand__c, VGA_Unique_Key__c, VGA_Working__c,
                                                      VGA_Available__c, VGA_Timezone__c, VGA_Start_Date_Date_Time__c, VGA_End_Time_Date_Time__c from VGA_Working_Hour__c 
                                                      where VGA_Unique_Key__c IN : setofUniqueKey and VGA_Working__c = true 
                                                      AND VGA_Available__c = true order by VGA_Last_Assigned_Date_Time__c asc NULLS FIRST])
                    {
                        if(!mapofWH.containskey(objWH.VGA_Unique_Key__c))
                            mapofWH.put(objWH.VGA_Unique_Key__c.trim().tolowercase(),new List<VGA_Working_Hour__c>());
                        
                        mapofWH.get(objWH.VGA_Unique_Key__c.trim().tolowercase()).add(objWH);
                    }
                }
                
                for(Lead objLead : lstLead)
                {
                    String strkey = '';
                    String strkey1 = '';  
                    
                    if(objLead.VGA_Dealer_Code__c != null && objLead.VGA_Brand__c != null && objLead.VGA_Sub_Brand__c != null)
                    {
                        strkey  = (objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase(); 
                        strkey1 = (objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase();
                        Boolean isPublicHoliday = false;
                        
                        if(mapofDealerCodeWRTBillingState != null && mapofDealerCodeWRTBillingState.containskey(objLead.VGA_Dealer_Code__c) && mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c) != null)
                        {
                            if(mapofPublicHoliday != null && !mapofPublicHoliday.isEmpty())
                            {
                                if(mapofPublicHoliday.containsKey('all') && mapofPublicHoliday.get('all') != Null)
                                {
                                    for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get('all'))
                                    {
                                        if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                                        {
                                            if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c.day() == system.today().day() && objph.VGA_Holiday_Date__c.month() == system.today().month())
                                            {
                                                isPublicHoliday = true;
                                                break;
                                            }
                                        }
                                        else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                                        {
                                            if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c == system.today())
                                            {
                                                isPublicHoliday = true;
                                                break; 
                                            }
                                        }
                                    }
                                }
                                
                                if(mapofPublicHoliday.containskey(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)) && mapofPublicHoliday.get(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)) != null)
                                {
                                    for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)))
                                    {
                                        if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                                        {
                                            if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c.day() == system.today().day() && objph.VGA_Holiday_Date__c.month() == system.today().month())
                                            {
                                                isPublicHoliday = true;
                                                break;
                                            }
                                        }
                                        else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                                        {
                                            if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c == system.today())
                                            {
                                                isPublicHoliday = true;
                                                break; 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        if(!isPublicHoliday)
                        {
                            if(mapofDistributionMethod != null && !mapofDistributionMethod.isEmpty() && mapofDistributionMethod.containskey(strkey1))
                            {
                                if(mapofDistributionMethod.get(strkey1).VGA_Distribution_Method__c != Null && mapofDistributionMethod.get(strkey1).VGA_Distribution_Method__c.equalsignoreCase('Nominated User'))
                                {
                                    if(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c != null && mapofUserIdwrtaccountid.containsKey(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c) && mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c).AccountId != null)
                                    {
                                        if(mapofWH != null && mapofWH.containskey(strkey) && !mapofWH.get(strkey).isEmpty())
                                        {
                                            String ContactId = String.valueof(mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c).contactId).substring(0,15);
                                            
                                            for(VGA_Working_Hour__c obj : mapofWH.get(strkey))
                                            {
                                            
                                                if(obj.VGA_Contact_ID__c != Null && String.valueof(obj.VGA_Contact_ID__c).substring(0,15) == ContactId)
                                                {
                                                    String Timezone = obj.VGA_Timezone__c;
                                            
                                                    Datetime dtStartDate1 = obj.VGA_Start_Date_Date_Time__c;  
                                                    Datetime dtEndDate1 = obj.VGA_End_Time_Date_Time__c;
                                                    String strCreatedDate1 = system.now().format('yyyy-MM-dd HH:mm:ss',Timezone);
                                                    
                                                    List<string> lstSplitDateandTime1 = strCreatedDate1.split(' ');
                                                    List<string> lstSplitDateValue1 = lstSplitDateandTime1[0].split('-');
                                                    List<string> lstsplitTimeValue1 = lstSplitDateandTime1[1].split(':');
                                                
                                                    Datetime dtCreatedDate1 = datetime.newinstancegmt(integer.valueof(lstSplitDateValue1[0]),integer.valueof(lstSplitDateValue1[1]),integer.valueof(lstSplitDateValue1[2]),integer.valueof(lstsplitTimeValue1[0]),integer.valueof(lstsplitTimeValue1[1]),integer.valueof(lstsplitTimeValue1[2]));
                                                    
                                                    if(dtCreatedDate1 >= dtStartDate1 && dtCreatedDate1 <= dtEndDate1)
                                                    {
                                                        objLead.ownerID = mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c;  
                                                        objLead.VGA_Assignment_Done__c = true;  
                                                        objLead.VGA_Assigned_to_Dealership__c = system.now(); 
                                                        objLead.VGA_Dealer_Account__c = mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c).AccountId;
                                                        
                                                        if(integer.valueof(mapofDistributionMethod.get(strkey1).VGA_Escalate_Minutes__c) > 0)         
                                                        {      
                                                            objLead.VGA_Escalated_Date_Time__c = VGA_CalculateEscalationTime.calculateEscalationTime(objLead,mapofDistributionMethod,mapofPublicHoliday,mapofDealerCodeWRTBillingState,mapofTH);
                                                        }
                                                        lstUpdateLead.add(objLead);
                                                        
                                                        Contact objcon = new Contact(id = mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey1).VGA_Nominated_User__c).ContactId);
                                                        objcon.VGA_Last_Assigned_Date_Time__c = system.now();
                                                        mapContact.put(objcon.Id, objcon);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else if(mapofDistributionMethod.get(strkey1).VGA_Distribution_Method__c.equalsignoreCase('Round Robin'))
                                {
                                    if(mapofWH != null && mapofWH.containskey(strkey) && !mapofWH.get(strkey).isEmpty())
                                    {
                                        for(VGA_Working_Hour__c obj : mapofWH.get(strkey))
                                        {
                                            String Timezone = obj.VGA_Timezone__c;
                                            
                                            Datetime dtStartDate = obj.VGA_Start_Date_Date_Time__c;  
                                            Datetime dtEndDate = obj.VGA_End_Time_Date_Time__c;
                                            string strCreatedDate = system.now().format('yyyy-MM-dd HH:mm:ss',Timezone);
                                            
                                            list<string> lstSplitDateandTime = strCreatedDate.split(' ');
                                            list<string> lstSplitDateValue = lstSplitDateandTime[0].split('-');
                                            list<string> lstsplitTimeValue = lstSplitDateandTime[1].split(':');
                                        
                                            Datetime dtCreatedDate = datetime.newinstancegmt(integer.valueof(lstSplitDateValue[0]),integer.valueof(lstSplitDateValue[1]),integer.valueof(lstSplitDateValue[2]),integer.valueof(lstsplitTimeValue[0]),integer.valueof(lstsplitTimeValue[1]),integer.valueof(lstsplitTimeValue[2]));
                                            
                                            if(dtCreatedDate >= dtStartDate && dtCreatedDate <= dtEndDate)
                                            {
                                                if(mapOfUser.containskey(obj.VGA_Contact_ID__c) && mapOfUser.get(obj.VGA_Contact_ID__c).Id != null && mapOfUser.get(obj.VGA_Contact_ID__c).accountid != null)
                                                {
                                                    objLead.ownerID = mapOfUser.get(obj.VGA_Contact_ID__c).Id;
                                                    objLead.VGA_Assignment_Done__c = true;
                                                    objLead.VGA_Dealer_Account__c = mapOfUser.get(obj.VGA_Contact_ID__c).AccountId;
                                                    objLead.VGA_Assigned_to_Dealership__c = system.now();
                                                    
                                                    if(integer.valueof(mapofDistributionMethod.get(strkey1).VGA_Escalate_Minutes__c) > 0)  
                                                    {
                                                        objLead.VGA_Escalated_Date_Time__c = VGA_CalculateEscalationTime.calculateEscalationTime(objLead,mapofDistributionMethod,mapofPublicHoliday,mapofDealerCodeWRTBillingState,mapofTH);
                                                    }
                                                    lstUpdateLead.add(objLead);
                                                    
                                                    Contact objcon = new Contact(id = obj.VGA_Contact_ID__c);
                                                    objcon.VGA_Last_Assigned_Date_Time__c = system.now();
                                                    mapContact.put(objcon.Id, objcon);break;
                                                } 
                                            }
                                        }
                                    }
                                }
                            }
                        }      
                    }
                }
                
                if(lstUpdateLead != null && !lstUpdateLead.isEmpty())
                {
                    update lstUpdateLead;
                    
                    VGA_LeadTriggerHandler.sendNotification(lstUpdateLead, true);
                }
                
                if(mapContact != null && !mapContact.isEmpty())
                {
                    update mapContact.values();
                }
            }   
            catch(Exception e)
            {
                BatchCheck=true;
                ExceptionList.add(e.getMessage()+'\t Line Number : '+e.getLineNumber());
                VGA_LeadBatchHelper.manageExceptionInBatch(e, 'Exception in Lead Assignment Batch : ', 'Lead Assignment Job', BC, ExceptionList);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        try
        {
            Id smsId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
            Id emailId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            List<VGA_Dealer_Notification__c> lstId = new List<VGA_Dealer_Notification__c>();
            List<VGA_Dealer_Notification__c> lstNot = new List<VGA_Dealer_Notification__c>();
            for(VGA_Dealer_Notification__c objsub : [select id, recordtypeId, Dealer_Contact__c, VGA_Message_Body__c, VGA_Subscription_Type__c, VGA_Sent_To__c, VGA_Sent_From__c from VGA_Dealer_Notification__c where VGA_Inserted_From_Batch__c = true AND VGA_Notification_Sent__c = false])
            {
                if(objsub.recordtypeId == smsId)
                {
                    lstId.add(objsub);
                }
                else if(objsub.recordtypeId == emailId)
                {
                    lstNot.add(objsub);
                }
            }
            if(lstId != Null && !lstId.isEmpty())               
            {
                system.debug('Calling SMS Batch process');
                VGA_NotificationTriggerHandler.sendSmsFromBatch(lstId);
            }
            if(lstNot != Null && !lstNot.isEmpty())
            {
                VGA_NotificationTriggerHandler.sendEmailFromBatch(lstNot);
            } 
            
            VGA_LeadBatchHelper.rescheduleBatchJobs(5, 'Lead Assignment Job', null);
            
        }
        catch(Exception e)
        {
            BatchCheck=true;
            ExceptionList.add(e.getMessage()+'\t Line Number : '+e.getLineNumber()); 
            VGA_LeadBatchHelper.manageExceptionInBatch(e, 'Exception in Lead Assignment Batch : ', 'Lead Assignment Job', BC, ExceptionList);
        }
        
        if(Test.isRunningTest()){
            BatchCheck=true;
            ExceptionList.add('TestData');
        }
    }    
}