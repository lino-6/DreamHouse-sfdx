@isTest
private class VGA_ModelHandlerTest
{
    // public static VGA_Service_Campaigns__c objData;
     
  @testsetup
 Static void buildModeldetails()
 {    
        
        VGA_Triggers__c CustomTrigger =new VGA_Triggers__c();
        CustomTrigger = VGA_CommonTracker.buildCustomSettingTrigger('Product2',true);
        insert CustomTrigger; 
        
         VGA_Active_Brochure__c objActiveBrochure =  new VGA_Active_Brochure__c();
              objActiveBrochure.name = 'Golf';
              objActiveBrochure.VGA_Brochure_Label__c= 'Golf';
              objActiveBrochure.VGA_Brand__c = 'Volkswagen';
              objActiveBrochure.VGA_Sub_Brand__c= 'Passenger Vehicles (PV)';
              
        
        insert objActiveBrochure;
        
       Product2 objModel = new product2();
       objModel.name='Test';
       objModel.VGA_Model_Alias__c = 'Golf';
        Insert objModel;
  }  
     /**
    * @author Ganesh M
    * @date 06/12/2018
    * @description Method that tests Tagging correct Active Brochure to Model for Inserting.
    */
    static testMethod void test01_UpdatingActivebrochure_field_BeforeInserting() 
    {      
        
        Test.startTest();
        
      product2 p=[select id, VGA_Model_Alias__c,VGA_Active_Brochure__c  from Product2 where  VGA_Model_Alias__c ='Golf'];
      VGA_Active_Brochure__c AB = [select id,name from VGA_Active_Brochure__c where name = 'Golf'];
           
   
        Test.stopTest();
         
         System.assertEquals(p.VGA_Active_Brochure__c,AB.id);
    } 
  /**
    * @author Ganesh M
    * @date 06/12/2018
    * @description Method that tests Tagging correct Active Brochure to Model for updating.
    */
    static testMethod void test02_UpdatingActivebrochure_field_BeforeUpdating() 
    {      
        
        Test.startTest();
        
         List<product2> p1=[select id,name, VGA_Model_Alias__c,VGA_Active_Brochure__c  from Product2 where  VGA_Model_Alias__c  ='Golf']; 
       
       p1[0].name='Test1';
        update p1;
        
       Test.stopTest(); 
       
      List<product2> p=[select id, VGA_Model_Alias__c,VGA_Active_Brochure__c  from Product2 where  id = :p1[0].id];
      VGA_Active_Brochure__c AB = [select id,name from VGA_Active_Brochure__c where name = 'Golf'];
           
   
        
         
         System.assertEquals(p[0].VGA_Active_Brochure__c,AB.id);
    }   
   
   }