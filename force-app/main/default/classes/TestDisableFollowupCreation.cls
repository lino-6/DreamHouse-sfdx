@isTest
public class TestDisableFollowupCreation {

    @isTest public static void Test1() {
        
        Case c = new Case();
        c.Subject = 'Sample';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'test@example.com';
        INSERT c;
        
        EmailMessage em = new EmailMessage(FromAddress = 'academy@volkswagen.com.au'
                                           , Incoming = True
                                           , ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com'
                                           , Subject = 'Test email'
                                           , TextBody = '23456 '
                                           , ParentId = c.Id);
        try {
            INSERT em;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }
    
    @isTest public static void Test2() {
        
        Case c = new Case();
        c.Subject = 'Sample';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'test@example.com';
        INSERT c;
        
        EmailMessage em = new EmailMessage(FromAddress = 'noreply@salesforce.com'
                                           , Incoming = True
                                           , ToAddress= 'hello@670ocglw7xhomi4oyr5yw2zvf.8kp7yeag.8.case.salesforce.com'
                                           , Subject = 'Test email'
                                           , TextBody = '23456 '
                                           , ParentId = c.Id);
        try {
            INSERT em;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    } 
    
    @isTest public static void Test3() {
        
        Case c = new Case();
        c.Subject = 'Sample: your message has been received';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }  

    @isTest public static void Test4() {
        
        Case c = new Case();
        c.Subject = 'Sample: Automatic Reply';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }   
    
    @isTest public static void Test5() {
        
        Case c = new Case();
        c.Subject = 'Sample: Delivery Status Notification (Failure)';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    } 
    
    @isTest public static void Test6() {
        
        Case c = new Case();
        c.Subject = 'Sample: Mail System Error';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }
    
    @isTest public static void Test7() {
        
        Case c = new Case();
        c.Subject = 'Undeliverable: Error';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }    
    
    @isTest public static void Test8() {
        
        Case c = new Case();
        c.Subject = 'Undelivered Mail: Error';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }
    
    @isTest public static void Test9() {
        
        Case c = new Case();
        c.Subject = 'Error: failure notice';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }   
    
    @isTest public static void Test10() {
        
        Case c = new Case();
        c.Subject = 'Error: out of office';
        c.Origin = 'Email';
        c.SuppliedEmail = 'test@ncat.nsw.gov.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    } 
    
    @isTest public static void Test11() {
        
        Case c = new Case();
        c.Subject = 'Test';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'academy@volkswagen.com.au';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    } 
/*
    @isTest public static void Test12() {
        
        Case c = new Case();
        c.Subject = 'Itinerary: Test';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'vgagroups@travelctm.com';        
        
        try {
            
            Account ac = new Account();
            ac.Name = 'Test account';
            Insert ac;
            
            Academy_Itinerary__c a = new Academy_Itinerary__c();
            a.Name = 'Id';
            a.Account_Id__c = ac.Id;
            Insert a;
            
            INSERT c;
        } catch(Exception e) {
            System.assert(FALSE, e.getMessage());
        } 
    }  
 */   
    @isTest public static void Test13() {
        
        Case c = new Case();
        c.Subject = 'Itinerary: Test';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'noreply@salesforce.com';        
        
        try {            
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    } 
    
    @isTest public static void Test14() {
        
        Case c = new Case();
        c.Subject = 'Auto Reply';
        c.Origin = 'Academy Email';
        c.SuppliedEmail = 'noreply@salesforce.com';        
        
        try {
            INSERT c;
        } catch(Exception e) {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Rejecting autoresponder email') ? TRUE : FALSE;
            System.AssertEquals(expectedExceptionThrown, TRUE);
        } 
    }     
    
}