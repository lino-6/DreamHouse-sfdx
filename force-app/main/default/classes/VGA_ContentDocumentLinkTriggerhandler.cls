public class VGA_ContentDocumentLinkTriggerhandler  
{
    public void runTrigger()
    {
        if(Trigger.isBefore && Trigger.isInsert)
        {
        	onbeforeInsert((List<ContentDocumentLink>) trigger.new);        
        }
    }
    public void onbeforeInsert(List<ContentDocumentLink> lstTriggerNew)
    {
    	updateVisibility(lstTriggerNew);	    
    }
    //===================================================================================
    //This Method is Used to update Visibility of Files to "All Users"
    //===================================================================================
    //Name: Rishi     Date:9 jan'17
    //===================================================================================
    private static void updateVisibility(List<ContentDocumentLink> lstTriggerNew)
    {
        if(lstTriggerNew != null && !lstTriggerNew.isEmpty())
        {
        	for(ContentDocumentLink objFile : lstTriggerNew)
            {
                objFile.Visibility = 'AllUsers';
            }
        }
    }
}