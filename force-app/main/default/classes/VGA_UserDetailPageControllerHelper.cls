public without Sharing  class VGA_UserDetailPageControllerHelper
{
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that creates a new user for a contact that is passed as a
      *              a parameter.
      * @param objContact Contact that will be used to create a new user
      * @return User new user created from the contact
    */
    public static User buildUserFromContact(Contact objContact){
        User objuser = new User();
        objuser.Username = objContact.VGA_Username__c;
        objuser.ContactId = objContact.Id;
        Map<String,Id> mapProfileId = getMapProfileId();
        String topMostRole          = getTopMostRole(objContact);
        Id VWRTId = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact');
        Id SKRTId = VGA_Common.GetRecordTypeId('Contact','Skoda Dealer Contact');
        if(topMostRole.equalsignorecase('Dealer Administrator'))
        {
            objuser.ProfileId = objContact.RecordTypeId == VWRTId ? mapProfileId.get('Volkswagen Dealer Principal Profile') : mapProfileId.get('Skoda Dealer Principal Profile');
        }
        else if(topMostRole.equalsignorecase('Lead Controller'))
        {
            if(objContact.RecordTypeId == VWRTId)
            {
                objuser.ProfileId = mapProfileId.get('Volkswagen Lead Controller Profile');
            }
            else
            {
                objuser.ProfileId = mapProfileId.get('Skoda Lead Controller Profile');
            }
        }
        else if(topMostRole.equalsignorecase('Consultant') || topMostRole.equalsignorecase('Dealer Portal Group') || topMostRole.equalsignorecase('Dealer Portal'))
        {
            if(objContact.RecordTypeId == VWRTId)
            {
                objuser.ProfileId = mapProfileId.get('Volkswagen Dealer Consultant Profile');
            }
            else
            {
                objuser.ProfileId = mapProfileId.get('Skoda Dealer Consultant Profile');
            }
        }
        else if(topMostRole == ''){
            //If the top role is empty it means that a sub-brand has not been provided.
            return null;
        }
        objuser.isActive = true;
        if(objContact.FirstName != Null)
        {
            objuser.FirstName = objContact.FirstName;
            objuser.Alias = string.valueof(objContact.FirstName.substring(0,1) + objContact.LastName.substring(0,1));
        }
        else
        {
            objuser.Alias = string.valueof(objContact.LastName.substring(0,1));
        }
        objuser.CompanyName = objContact.Account.Name;
        objuser.Title = objContact.VGA_Role__c;
        objuser.Email = objContact.Email;
        objuser.EmailEncodingKey = 'UTF-8';
        objuser.LastName = objContact.LastName;
        objuser.CommunityNickname = objContact.lastname.substring(0,1) + String.valueof(System.Now());
        objuser.TimeZoneSidKey = 'Australia/Sydney';
        objuser.LocaleSidKey = 'en_AU';
        objuser.LanguageLocaleKey = 'en_US';
        objuser.MobilePhone = objContact.MobilePhone;
        objuser.VGA_Brand1__c = objContact.VGA_Brand__c;
        return objuser;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that returns a Map with the portal profiles group by Id.
      * @return Map<String,Id> Map of profiles group by its Id
    */
    public static Map<String,Id> getMapProfileId() {
        Map<String,Id> mapProfileId = new Map<String,Id>();
        for(Profile obj : [SELECT Id, Name
                           FROM Profile
                           WHERE UserType = 'PowerPartner'])
        {
            mapProfileId.put(obj.Name,obj.Id);
        }
        return mapProfileId;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that given a contact, it will retrieve the list of related
      *              user profile and find the top most role.
      * @param objContact Contact for what we will get the list of related user profiles
      * @return String Name of the top most role.
     */
    public static String getTopMostRole(Contact objContact){
        String topMostRole = '';
        Set<String> lstRoles = new Set<string>();
        for(VGA_User_Profile__c objuserProfile : [SELECT Id, VGA_Role__c, VGA_Contact__c
                                                  FROM VGA_User_Profile__c
                                                  WHERE VGA_Contact__c =: objContact.Id])
        {
            if(objuserProfile.VGA_Role__c != null)
                lstRoles.add(objuserProfile.VGA_Role__c);
        }
        if(lstRoles != null && !lstRoles.isEmpty())
        {
            topMostRole = VGA_Common.getTopMostRole(lstRoles);
        }
        return topMostRole;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that given a a list of users it addes them into a group
      *              based on their brand.
      * @param lstUser List of users that will be added to a group.
     */
    public static void addUsertoGroup(list<User> lstUser)
    {
        if(lstUser != null && !lstUser.isEmpty())
        {
            list<groupMember> groupList = new list<groupMember>();
            list<Group> VWgrp = [select id from Group where name = : 'VW Portal User Group'];
            list<Group> SKOgrp = [select id from Group where name = : 'SKO Portal User Group'];
            for(User objUser : lstUser)
            {
                if(objUser.VGA_Brand1__c != null)
                {
                    if(objUser.VGA_Brand1__c.equalsignoreCase('Volkswagen'))
                    {
                        for(group objVWgrp : VWgrp)
                        {
                             groupMember gp = new groupMember(groupId = objVWgrp.Id);
                             gp.UserOrGroupId = objUser.Id;
                             groupList.add(gp);
                        }
                    }
                    else if(objUser.VGA_Brand1__c.equalsignoreCase('Skoda'))
                    {
                        for(Group objSKOgrp : SKOgrp)
                        {
                            groupMember gp = new groupMember(groupId = objSKOgrp.Id);
                            gp.UserOrGroupId = objUser.Id;
                            groupList.add(gp);
                        }
                    }
                }
                if(groupList != null && !groupList.isEmpty())
                {
                    if(!test.isRunningTest())
                    insert groupList;
                }
            }
        }
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that given a user and a subbrand checks on the existing
      *              Dealership Profile records to check if the user is a Nominated User.
      *              It returns true if it is and false in case is not.
      * @param usrId User that needs to be checked to see if it is a nominated user
      * @param subbrand Subbrand for which this user will/won't be nominated user
      * @return Boolean Either This user is nominated user or not (true/false)
     */
    public static Boolean isNominatedUser(Id usrId, String  subbrand){
        for(VGA_Dealership_Profile__c dealershipProfile : [SELECT Id,VGA_Distribution_Method__c, VGA_Sub_Brand__c
                                                           FROM VGA_Dealership_Profile__c
                                                           WHERE VGA_Nominated_User__c=: usrId
                                                           AND VGA_Distribution_Method__c = 'Nominated User'] ){
            if(subbrand == null){
                return true;
            }else if(subbrand.contains(dealershipProfile.VGA_Sub_Brand__c)){
                return true;
            }
        }
        return false;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Method that given a user will update all the contacts that its own
      *              so they are now owned by the default owner that is stored in
      *              a custom label in Salesforce.
      * @param usrId User that is being disabled and whose contacts need to be updated.
     */
    public static void updateContactsOwnedByDisableUser(Id usrId)
    {
        //Get username of the contact owner that we will use as the new owner for the contacts.
        String myLabel = System.Label.VGA_Contact_Owner;
        List<User> automationlstUser = [SELECT Id,isActive,VGA_Brand__c,ContactId
                                        FROM User
                                        WHERE userName =: myLabel
                                        AND isActive=true
                                        LIMIT 1];
        if(automationlstUser !=Null
            && automationlstUser.size()>0)
        {
            updatecontactOwner(usrId,automationlstUser[0].Id);
        }
    }
    /**
      * @author Lino Diaz Alonso
      * @date 28/12/2018
      * @description Future method that updates the owners of the contacts previously owned
      *              by a user that has been disabled..
      * @param usrId User that is being disabled and whose contacts need to be updated.
      * @param defaultOwnerId User Id of the default owner in Salesforce and that will now
      *                       own the affected contacts.
     */
    @future
    public static void updatecontactOwner(Id usrId,Id defaultOwnerId)
    {
        List<Contact> contactsToUpdateList = new List<Contact>();
        for(Contact objCon :[SELECT Id, OwnerId
                             FROM Contact
                             WHERE OwnerId=:usrId])
        {
            objCon.OwnerId = defaultOwnerId;
            contactsToUpdateList.add(objCon);
        }
        update contactsToUpdateList;
    }
    /**
      * @author Ashok Chandra
      * @date 18/09/2018
      * @description Method that creates a new contact if it doesn't exist any contact
      *              with the same email.
      * @param objContact Contact that will be inserted.
      * @param recordTypeName Record Type name of the contact that will be created.
      * @param selectedValue Value of the subbrand for which we will create a new contact.
      * @param objUser User current user that is trying to insert a new contact
      * @return Contact that has been created
     */
    public static Contact createNewContact(Contact objContact, String recordTypeName,
                                           String selectedValue, User objUser){
        String ContactRecordId =VGA_Common.recordTypeId(recordTypeName);
        objContact.AccountId                 =objUser.VGA_Account_Id__c;
        objContact.VGA_Partner_User_Status__c= 'Enabled';
        objContact.VGA_Available__c = objContact.VGA_Available_to_Receive_Leads__c==true ? 'Yes' : 'No';
        if(ContactRecordId !=Null)
        {
            objContact.RecordTypeId=ContactRecordId;
        }
        objContact.VGA_Sub_Brand__c = selectedValue !='Commercial' ? 'Passenger Vehicles (PV)' : 'Commercial Vehicles (CV)';
        insert objContact;
        return objContact;
    }
    /**
      * @author Ashok Chandra
      * @date 18/09/2018
      * @description Method that updats the existing contact if the user tries to create
      *              a contact that already exists in the system.
      * @param objContact Contact that will be updated.
      * @param selectedValue Value of the subbrand for which we will update the existing contact.
      * @return Contact that has been updated
     */
    public static Contact updateExistingContact(Contact objContact, String selectedValue){
        String subbrand = objContact.VGA_Sub_Brand__c!=null ? objContact.VGA_Sub_Brand__c : null;
        if(subbrand!=null)
        {
            //Duplicate user
            if(objContact!=null
                && subbrand=='Passenger Vehicles (PV);Commercial Vehicles (CV)')
            {
                return null;
            }
            //Duplicate user
            if(objContact!=null
                && objContact.VGA_Brand__c=='Skoda'
                && subbrand=='Passenger Vehicles (PV)')
            {
                return null;
            }
            if(selectedValue !='Commercial'
                && !subbrand.contains('Passenger Vehicles (PV)'))
            {
                objContact.VGA_Sub_Brand__c ='Passenger Vehicles (PV);Commercial Vehicles (CV)';
            }
            else if(selectedValue =='Commercial'
                && !subbrand.contains('Commercial Vehicles (CV)'))
            {
                objContact.VGA_Sub_Brand__c ='Passenger Vehicles (PV);Commercial Vehicles (CV)';
            }
        }
        else
        {
            objContact.VGA_Sub_Brand__c = selectedValue=='Commercial' ? 'Commercial Vehicles (CV)' : 'Passenger Vehicles (PV)';
        }
        //The contact will be always enabled
        objcontact.VGA_Partner_User_Status__c='Enabled';
        return objContact;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 31/12/2018
      * @description Method that receives an exception and generates an error message that
      *              that will be displayed in the dealer portal.
      * @param ex Exception that was generated.
      * @return String Error message that we will be displying in the dealer porta.
     */
    public static String getErrorMessage(Exception ex){
        String errorMessage =  'Error: Sorry! Exception occured. Please contact the administrator.';
        if(ex.getMessage().contains('DUPLICATE_USERNAME'))
        {
            errorMessage = 'Error: Duplicate Username';
        }
        if(ex.getMessage().contains('LICENSE_LIMIT_EXCEEDED'))
        {
            errorMessage = 'Error: License Limit Exceeded';
        }

        if(ex.getMessage().contains('Cannot change available to receive leads as user is set to nominated user'))
        {
            return 'Error: Cannot change available to receive leads as user is set to nominated user';
        }

        if(ex.getMessage().contains('The dealer should be a pilot user to be able to receive test drive leads'))
        {
            return 'Error: The dealer should be a pilot user to be able to receive test drive leads';
        }

        System.debug(ex.getMessage());
        return errorMessage;
    }
    /**
      * @author Lino Diaz Alonso
      * @date 03/01/2018
      * @description Method to disable a contact and its related user if the contact
      *              doesn't have any subrand.
      * @param objCon Contact that will be disabled.
      * @param subbrand Sub-brand for which we are disabling the contact
      * @param objUserProfile User profile related to the contact that we are disabling.
      * @param listofUserProfile List of user profiles related to the contact that we are disabling.
      * @return String Error/Success message that we will be displying in the dealer porta.
     */
    public static String disableContact(Contact objCon, String subbrand,
                                        VGA_User_Profile__c objUserProfile,
                                        list<VGA_User_Profile__c>listofUserProfile){
        Contact objContact = new Contact();
        if(subbrand.contains(objUserProfile.VGA_Sub_Brand__c)){
            objContact.VGA_Sub_Brand__c = subbrand.remove(objUserProfile.VGA_Sub_Brand__c);
        }
        //If the contact doesn't have any other user profile related to it
        if(listofUserProfile.size() == 1){
            //We will be disabling the contact
            objContact.VGA_Partner_User_Status__c = 'Disabled';
            objContact.VGA_Active__c = false;
        }
        objContact.Id=objUserProfile.VGA_Contact__c;
        if(String.isNotBlank(objUserProfile.VGA_Sub_Brand__c))
        {
            if(objUserProfile.VGA_Sub_Brand__c.contains('Passenger'))
            {
                objcon.VGA_PV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
            else
            {
                objcon.VGA_CV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
        }
        try
        {
            update objContact;
            if(listofUserProfile.size() == 1){
                //Update related user so it is inactive
                updateusercontact(objContact.Id);
            }
            return 'User Deleted';
        }
        catch(exception e)
        {
            return 'Error: Sorry! Exception occured. Please contact the administrator.';
        }
    }
    /**
      * @author Lino Diaz Alonso
      * @date 03/12/2018
      * @description Method that receives a contact id and updates its related user.
      * @param ContactId Id of the contact for which we will be updating its related user.
     */
    @future
    public static void updateusercontact(Id ContactId)
    {
         string strStatus;
         list<user>lstUser =new list<user>();
         lstUser = [select Id, isActive,VGA_Brand__c,ContactId from User where ContactId =: ContactId limit 1];
         lstUser[0].IsActive = False;
         Database.upsert(lstUser,true);
    }
    /**
      * @author Lino Diaz Alonso
      * @date 03/01/2018
      * @description Method to disable a contact and its related user if the contact
      *              doesn't have any subrand. It will also reassign all the leads that
      *              the related user owns. New owner will be specified in the
      *              selecteduserId field.
      * @param updateListofLead List of leads related to the contact that we are disabling and
      *                         that needs to be updated.
      * @param subbrand Sub-brand for which we are disabling the contact
      * @param objUserProfile User profile related to the contact that we are disabling.
      * @param listofUserProfile List of user profiles related to the contact that we are disabling.
      * @param selecteduserId Id of the user that will be the new owner of the leads.
      * @return String Error/Success message that we will be displying in the dealer porta.
     */
    public static String updateContactAndReassignLeads(String subbrand, list<Lead> updateListofLead,
                                                       Set<Id> leadIdSet, VGA_User_Profile__c objUserProfile,
                                                       list<VGA_User_Profile__c>listofUserProfile,
                                                       String selecteduserId){
        String strmessage = 'Error: Sorry! Exception occured. Please contact the administrator.';
        Contact objContact = new Contact();
        if(subbrand.contains(objUserProfile.VGA_Sub_Brand__c)){
            objContact.VGA_Sub_Brand__c=subbrand.remove(objUserProfile.VGA_Sub_Brand__c);
        }
        objContact.Id=objUserProfile.VGA_Contact__c;
        if(listofUserProfile.size() == 1){
            objContact.VGA_Partner_User_Status__c = 'Disabled';
        }
        try
        {
            if(updateListofLead !=Null
                && updateListofLead.size()>0)
            {
                if(updateListofLead.size() > 100){
                    updateRelatedLeads(leadIdSet, selecteduserId);
                } else {
                    update updateListofLead;
                }
            }
            update objContact;
            if(listofUserProfile.size() == 1){
                updateusercontact(objContact.Id);
            }
            strmessage='User Deleted';
            return strmessage;
        }
        catch(exception e)
        {
                strmessage='Error: Sorry! Exception occured. Please contact the administrator.';
                return strmessage;
        }
    }
    /**
      * @author Lino Diaz Alonso
      * @date 03/01/2018
      * @description Method to  reassign all the leads that the related user owns. New owner will be specified in the
      *              selecteduserId field.
      * @param leadIdSet List of leads related to the contact that we are disabling and
      *                   that needs to be updated.
      * @param selecteduserId Id of the user that will be the new owner of the leads.
     */
    @future
    public static void updateRelatedLeads(Set<Id> leadIdSet, Id selectedUserId){
      List<Lead> leadsToUpdateList = new List<Lead>([select Id,VGA_Brand__c,VGA_Sub_Brand__c FROM Lead where Id IN:leadIdSet]);
      for(Lead leadToProcess : leadsToUpdateList){
        leadToProcess.OwnerId = selectedUserId;
      }
      // DML statement
        Database.SaveResult[] srList = Database.update(leadsToUpdateList, false);
        // Iterate through each returned result
        for (Integer i = 0; i < leadsToUpdateList.size(); i++) {
            Database.SaveResult s = srList[i];
            SObject origRecord = leadsToUpdateList[i];
            if (s.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully updated lead. Lead ID: ' + s.getId());
            } else {
              // Operation failed, so get all errors
                for(Database.Error err : s.getErrors()) {
                    System.debug('The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                }
            }
        }
    }

    /**
      * @author Lino Diaz Alonso
      * @date 29/01/2018
      * @description Method that gets the contact Ids of all the contacts related to an account
      *              and with a certain subbrand that are available to receive leads.         
      * @param accountId Account Id related to all the contacts that we will be retrieving.
      * @param subbrand Name of the sub brand that we will use to retrieve the contacts
      * @return Set<Id> with all the contacts that meet all the conditions.
    */
    public static Set<Id> getContactsAvailableToReceiveSet(Id accountId, String subbrand){
        Set<Id> contactIdSet = new Set<Id>();

        for(VGA_User_Profile__c usrProfile : [SELECT Id, VGA_Available__c, VGA_Contact__c
                                              FROM VGA_User_Profile__c
                                              WHERE (VGA_Account_Id__c =: accountId
                                              OR VGA_Account_Id__c =: String.valueOf(accountId).substring(0, 15))
                                              AND VGA_Available__c = true
                                              AND VGA_Sub_Brand__c =: subbrand]){
          contactIdSet.add(usrProfile.VGA_Contact__c);
        }

        return contactIdSet;
    }
}