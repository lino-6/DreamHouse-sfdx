/**
* @author Archana Yerramilli
* @date 09/01/2018
* @description This is a test class for case trigger helper
* @description Case Trigger Helper is a class which takes care of creation/updation of tasks. 
*/


@isTest
private class VGA_CaseTriggerHandlerTest 
{
public static Case objCase;
    ID recordTypeId;
    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test data setup
    */
    @testSetup
    static void buildConfigList() 
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Case';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
    }
    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to create a new task for an Interaction
    */    
    @isTest static void test01_CreateTask_whenWeCallHelperForCaseTrigger_TaskShouldBeCreated()
    {
        Test.startTest(); 
        ID recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Task Academy').getRecordTypeId();
        Case objCreateCase = VGA_CommonTracker.createCase(); 
        Task objTask = VGA_CaseTriggerHelperForTask.createNewTask(recordTypeId,'Test', 'New Interaction Assigned','Open', 'Medium', objCreateCase,system.now());
        Test.stopTest();
        System.AssertEquals(objTask.VGA_Action_Required__c,'New Interaction Assigned');

        //Assert to Test New Task is Created Successfully
        List<Task> updatedTask = [select Id, VGA_Action_Required__c from Task where Id = : objTask.Id];
        for(Task task : updatedTask){
			System.AssertEquals('New Interaction Assigned',task.VGA_Action_Required__c);  
        }
    }
    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Insert a new Interaction of Academy type and go through the task creation flow
    * @description This should not insert a task because there is no incoming email and its not an Update for Trigger
    */    
    @isTest static void test02_CreateTask_whenWeCreateTask_CaseShouldNotBeCreated() 
    {
               Test.startTest(); 
               Set<String> caseSubjectSet = new Set<String>{'Academy Test 1', 'Academy Test 2', 'Academy Test 21', 'Academy Test IAS12123'};
               List<Case> createCaseList = VGA_CommonTracker.buildCaseRecords(caseSubjectSet); 
        	   INSERT createCaseList;                              
               Test.stopTest();

               //Assert to check a Task is not created for the case createCaseList[0] and catch exception
               try
               {
                 List<Task> lstTask = [SELECT ID FROM Task where WhatId =: createCaseList[0].ID AND What.type = 'Case'];
               }
               catch(Exception e)
               {
                 Boolean expectedExceptionThrown =  e.getMessage().contains('Attempt to de-reference a null object') ? true : false;
                 System.AssertEquals(expectedExceptionThrown, true);
               } 
    }

    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Update an Interaction of Academy type and go through the task creation flow
    * @description This should not create a task because there is no incoming email, omni channel assignment
    */ 
    @isTest static void test03_UpdateCase_whenWeUpdateCaseWithoutOmniChannel_TaskShouldNotBeCreated() 
    {
               Test.startTest(); 
               Set<String> caseSubjectSet = new Set<String>{'Academy Test 12', 'Academy Test 212', 'Academy Test 201', 'Academy Test IAS1212356'};
               List<Case> createCaseList = VGA_CommonTracker.buildCaseRecordsVWEmail(caseSubjectSet); 
        	   INSERT createCaseList;
               Group  myQueue = [select Id, Name from Group where  Type = 'Queue' AND Name = : 'Email VW'];
               createCaseList[0].OwnerID= myQueue.ID;

               UPDATE createCaseList[0];               
               Test.stopTest();
               //Assert to test Case Created and Updated. 
               List<Case> updatedCaseList = [SELECT Id, Status, Owner.Name, Subject FROM CASE WHERE Id IN: createCaseList];
               System.AssertEquals(updatedCaseList[0].OwnerID, myQueue.ID, 'Queue Successfully Assigned to Case');
               System.AssertEquals('Academy Test 212',createCaseList[1].Subject);
    }
   /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Update an Interaction of Academy type and go through the task creation flow
    * @description This should create a task because we have created data for VGA_Omni_Accepted_Date_Time__c and VGA_Omni_Channel_Status__c
    */ 
   @isTest static void test04_CreateCase_whenWeAcceptedAcademyCase_newTaskAcademyShouldBeCreated() 
   {
            Case objCase        = VGA_CommonTracker.createCase();  
            Account  objAccount = VGA_CommonTracker.createDealerAccount();
            ID recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('New Task Academy').getRecordTypeId();
			Test.startTest();             
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.Name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 1440;
            insert objCS;
            objCase.VGA_Omni_Queue_Name__c = 'Academy Email';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';             

            update objCase;            
            Test.stopTest();
            Task objTask = VGA_CaseTriggerHelperForTask.createNewTask(recordTypeId,'Test', 'New Interaction Assigned','Open', 'Medium', objCase,System.Now().adddays(1));
            System.AssertEquals('Academy Email', objCS.Name);       
            System.AssertEquals('New Interaction Assigned', objTask.VGA_Action_Required__c); 
            System.AssertEquals(recordTypeID , objTask.RecordTypeID);  
    }

    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Update an Interaction of VW Email type and go through the task creation flow when there is an incoming email and omni channel case acceptance. 
    * @description This should create a task because we have created data for VGA_Omni_Accepted_Date_Time__c and VGA_Omni_Channel_Status__c
    */ 
   @isTest static void test05_UpdateCase_whenThereIsIncomingEmailForVWEmail_TaskShouldBeCreated() 
   {
             
			Test.startTest();  
            Case objCase        = VGA_CommonTracker.createCase();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.Name = 'VW Email';
            objCS.VGA_Priority_Ranking__c = 2;
            objCS.VGA_SLA_Mins__c = 120;
            insert objCS;     
       
            objCase.VGA_Omni_Queue_Name__c = 'VW Email';
            objCase.Origin = 'VW Email';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now(); 
            objCase.VGA_No_of_Incoming_Mails__c = 1;       
            insert objCase;            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';    
            objCase.VGA_No_of_Incoming_Mails__c = 2;
            update objCase;            
            Test.stopTest();
            List<Task> lstTask = [SELECT ID FROM Task where WhatId =: objCase.ID AND What.type = 'Case'];

            System.assertNotEquals(NULL, lstTask);
        
    }
    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Update an Interaction of Academy Email when there are more than one incoming emails.
    * @description This should create a task because we have created data for incoming email, VGA_Omni_Accepted_Date_Time__c and VGA_Omni_Channel_Status__c
    */   
    @isTest static void test06_UpdateCase_whenAcademyEmailCaseAcceptedAndIncomingEmailsExist_TaskShouldBeCreated() 
   {
             
			Test.startTest();  
            Case objCase        = VGA_CommonTracker.createCase();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.Name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 2;
            objCS.VGA_SLA_Mins__c = 120;
            insert objCS;     
       
            objCase.VGA_Omni_Queue_Name__c = 'Academy Email';
            objCase.Origin = 'Academy Email';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now(); 
            objCase.VGA_No_of_Incoming_Mails__c = 1;       
            insert objCase;            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';    
            objCase.VGA_No_of_Incoming_Mails__c = 2;

            update objCase;            
        	Test.stopTest();
            List<Task> lstTask = [SELECT ID FROM Task where WhatId =: objCase.ID AND What.type = 'Case'];
            System.assertNotEquals(NULL, lstTask);        
    }
    
    /**
    * @author Archana Yerramilli
    * @date 09/01/2018
    * @description This is a test to Update an Interaction of Academy Email When there is an Inbound email , VGA_To_Email__c
    * @description This should create a task because we have created data for VGA_Omni_Accepted_Date_Time__c and VGA_Omni_Channel_Status__c
    */ 
    @isTest static void test08_UpdateCase_whenAcademyEmailCaseAcceptedThroughOmniChannel_TaskShouldBeCreated() 
   {
             
			Test.startTest();  
            Id ARTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
            Case objCase        = VGA_CommonTracker.createCase();  
            objCase.Origin = 'Academy Email';
            objCase.RecordTypeID =  ARTId;
            Account objAccount = VGA_CommonTracker.createDealerAccount();
            Contact objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
            objCase.suppliedEmail = objContact.email;
            objCase.VGA_Reopened_Case__c = true;
            insert objCase;
            objCase.Status = 'Assigned';
            objCase.VGA_To_Email__c = 'academy@success.volkswagen.net.au';

            update objCase;           
        	Test.stopTest();
            List<Task> lstTask = [SELECT ID FROM Task where WhatId =: objCase.ID AND What.type = 'Case'];

            System.assertNotEquals(NULL, lstTask);
        
    }
    

}