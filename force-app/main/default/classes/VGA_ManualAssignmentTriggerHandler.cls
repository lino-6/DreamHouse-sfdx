public with sharing class VGA_ManualAssignmentTriggerHandler 
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onbeforeInsert((list<VGA_Manual_Assignment_Configuration__c>) trigger.new);
        }
        
        // Method will be called to handle Before Update events
        if (Trigger.isBefore && Trigger.isUpdate)
        {
           onbeforeUpdate((list<VGA_Manual_Assignment_Configuration__c>) trigger.new,(map<id, VGA_Manual_Assignment_Configuration__c>) trigger.OldMap);
        }
        
        // Method will be called to handle After Insert events
        if (Trigger.isAfter && Trigger.isInsert)
        {
            onafterInsert((list<VGA_Manual_Assignment_Configuration__c>) trigger.new);
        }
        
        // Method will be called to handle After Update events
        if(Trigger.isAfter && Trigger.isUpdate)
        {
            
        }
    }
    
    private void onbeforeInsert(list<VGA_Manual_Assignment_Configuration__c> lsttriggernew)
    {
        updateStatusonInsert((list<VGA_Manual_Assignment_Configuration__c>) trigger.new);
    }

    private void onafterInsert(list<VGA_Manual_Assignment_Configuration__c> lsttriggernew)
    {
        startBatch(lsttriggerNew, null);
    }

    private void onbeforeUpdate(list<VGA_Manual_Assignment_Configuration__c> lsttriggernew,map<id,VGA_Manual_Assignment_Configuration__c> triggeroldmap)
    {
        startBatch(lsttriggerNew,triggeroldmap);   
    }

    /**
      * @author Lino Diaz Alonso
      * @date 22/03/2019
      * @description This method updates the status of the manual assignments configuration object
      *              to In Progress when the start batch checkbox is equals to true.
      *              This is the pre condition before scheduling the VGA_ManualAssignmentBatch job.
      * @param lsttriggernew List of Manual Assignment Configuration records that we will process
    */
    private static void updateStatusonInsert(list<VGA_Manual_Assignment_Configuration__c> lsttriggernew){
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
			for(VGA_Manual_Assignment_Configuration__c objPLC : lsttriggernew)
            {
                
                if(objPLC.VGA_Start_Batch__c 
                    && objPLC.VGA_Status__c == 'New'
                    )
                {
                    objPLC.VGA_Status__c = 'In Progress';                          
                }
            }
        }
    }

    /**
      * @author Lino Diaz Alonso
      * @date 22/03/2019
      * @description This method schedules the VGA_ManualAssignmentBatch job for execution after
      *              a new record is created or after the Start Batch is updated to true 
      * @param lsttriggernew List of Manual Assignment Configuration records that we will process
      * @param triggeroldmap Map with the old values of the record being used on the update trigger.
    */
    public static void startBatch(list<VGA_Manual_Assignment_Configuration__c> lsttriggernew, map<id,VGA_Manual_Assignment_Configuration__c> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
			for(VGA_Manual_Assignment_Configuration__c objPLC : lsttriggernew)
            {
                
                if(objPLC.VGA_Start_Batch__c 
                    && (trigger.isInsert 
                        || (trigger.isUpdate 
                            && triggeroldmap.get(objPLC.id).VGA_Start_Batch__c == false)
                        )
                    )
                {
                    //We will only update the job to In Progress when it is an update operation
                    if(trigger.isUpdate){
                        objPLC.VGA_Status__c = 'In Progress';
                    }
                                         
                    VGA_ManualAssignmentBatch objCls = new VGA_ManualAssignmentBatch(objPLC.id); 
                    Database.executeBatch(objCls);       
                }
                // In case the status changes to closed we will stop the job
                if(objPLC.VGA_Status__c == 'Closed') 
                {
              		stopJob(objPLC);       
                }
            }
        }
    }

    /**
      * @author Lino Diaz Alonso
      * @date 22/03/2019
      * @description This method updates the status of the manual assignments configuration object
      *              to In Progress when the start batch checkbox is equals to true.
      *              This is the pre condition before scheduling the VGA_ManualAssignmentBatch job.
      * @param lsttriggernew List of Manual Assignment Configuration records that we will process
    */
 	public static void stopJob(VGA_Manual_Assignment_Configuration__c objPLC)
    {
        System.Debug('@@@@Status is NOT In Prog,batch will NOT start---'+ objPLC.VGA_Status__c);
        String trackingID = objPLC.VGA_Tracking_Id__c;
        String preAssignmentJobName = 'PreLaunchLeadAssignmentJob-';
        String jobName = preAssignmentJobName + trackingID;
        
        List<CronTrigger> lstCron = [SELECT Id, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType,State FROM CronTrigger where CronJobDetail.JobType = '7'];
        if(lstCron != Null && !lstCron.isEmpty())
        {
            for(CronTrigger obj : lstCron) 
            {
                //We'll find the job related to the record that we are updating and we will close it.
                if(obj.CronjobDetail.Name.contains(jobName))
                {
                    objplc.VGA_Status__c = 'Closed';
                    objPLC.VGA_Start_Batch__c = false;
                    objPLC.VGA_Immediate__c   = false;
                    system.abortjob(obj.Id);
                    break;
                }
                                            
            }
        }
    }
}