@isTest
private class VGA_Car_Configurator_Heroku_Lead_Tracker {
    
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c CustomTrigger =new VGA_Triggers__c();
        CustomTrigger = VGA_CommonTracker.buildCustomSettingTrigger('Product2',true);
        insert CustomTrigger; 
        
        list<VGA_Active_Brochure__c> aclist=new list<VGA_Active_Brochure__c>();
        
        VGA_Active_Brochure__c actb=VGA_CommonTracker.createActiveBrochure();
        actb.Name='GOLF';
        actb.VGA_Brand__c='Volkswagen';
        actb.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
        actb.VGA_Brochure_Label__c = 'Golf';
        
        
        VGA_Active_Brochure__c actb1=VGA_CommonTracker.createActiveBrochure();
        actb1.Name='tiguan';
        actb1.VGA_Brand__c='Volkswagen';
        actb1.VGA_Sub_Brand__c='Commercial Vehicles (CV)';
        actb1.VGA_Brochure_Label__c='tiguan';
        aclist.add(actb);
        aclist.add(actb1); 
        insert aclist;
        Product2 objModel = new product2();
        objModel.name='Test';
        objModel.VGA_Model_Alias__c = 'Golf';
        objModel.ProductCode = 'BQ14NZ/18';
        Insert objModel;
        
        Product2 objModel1 = new product2();
        objModel1.name='Test1';
        objModel1.VGA_Model_Alias__c = 'tiguan';
        objModel1.ProductCode = 'AD148T/18';
        Insert objModel1;
        
        VGA_MuleUrl__c vwset=new VGA_MuleUrl__c();
        vwset.Name='Volkswagen';
        vwset.Client_ID__c='12121212121';
        vwset.Secret_Id__c='1212';
        insert vwset;
        
        
    }
    
    /**
* @author Ashok Chandra
* @date 17/10/2018
* @description Method that tests the VGA_CarConfigurator class.
*              It makes sure a new Lead is created with brand Volkswagen
*       and Sub brand Passenger Vehicles (PV).
*               

*/
    
/*
    static testmethod void pmaapitest1(){
        Test.startTest();
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/postCarConfiguratorLead'; 
        
        req.params.put('brand', 'Volkswagen');
        req.params.put('post_code', '2000');
        req.params.put('car_configurator_url', 'test');
        req.params.put('marketing_opt_in', '1');
        req.params.put('vehicle_interested_in', '1');
        req.params.put('colorcode', '2000');
        req.params.put('ModelCode', '2000');
        req.params.put('variant', '2000');
        req.params.put('modelyear', '2000');
        req.params.put('image_url', '2000');
        req.params.put('Emailaddress', '2000');        
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_Car_Configurator_Heroku_Lead.PostCarConfiguratorLead();
        
        
        //  System.assertEquals(config.modelname,'GOLF');
    }
    */
    
/**
* @author Hijith NS 
* @date 15/07/2019
* @description Method that tests the VGA_Car_Configurator_Heroku_Lead.
*              It makes sure a new Lead is created with brand Volkswagen
*              and Sub brand Passenger Vehicles (PV) from heroku page using rest API.
* @ModifiedBy Hijith NS 
*/
    
    @istest
    static void Test2_PostCarConfiguratorLead() {
        
        Test.startTest();
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : null}');
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/postCarConfiguratorLead'; 
        
        req.requestBody = BLOB.valueOf('{ "brand": "Volkswagen", "car_configurator_url": "https://aus.volkswagen.com.au", "post_code": 2000, "marketing_opt_in": 1, "vehicle_interested_in": "Golf R", "exterior_colour_name": "Pure White", "exterior_colour_mrrp": 0, "interior_colour_name": "Titan black", "interior_colour_mrrp": 100, "options": [ { "name": "R Line", "mrrp": 2000 }, { "name": "Sound and Vision", "mrrp": 3000 } ], "model_code": "BQ14NZ/19", "model_year": 2019, "model_mrrp": 54322, "variant": "R", "image_url": "https://media.volkswagen.com/vw-BQ1-my2019/iris?COSY-EU-100-1713VV%25lXm0kYNazzSU9mu0fO7AkkyyJ1vTvsd2MzppEKhjTtKSK8Cq71MM8H2Lvyr0Q%25UOqggAa5hP4KYwqeQeOOnabyyJ8H3WDZ4HvCJii8CxnOoo0gZrnZlO4Jkoc744q3lMM0kYiD9FFkJ9hO0onq2Tuw99Q1mEiCrZ0ssRdGbQ0aoD77XxMjRMxFvvjkuhb5AZc11TBQ58osULxKPOqqf98SNgtNK9HKswQLAH17w4aqL170%25qY6CF9BvRR5QYVdzYBKXXYXPrttOkUT55P5P999dJp7ZZsoH9iiIE24ffQXTi22UFFsnn4hwDuugUgc00zHjyFF3dzSxxciGHBBhVVsTTklhfWWHnkVGGKuvJJJMXFGllvFeYEELsfjVVbIJyXXYzLBttONtJ55P%25JR99dKcAZZsZ8OiiIz5QffQfQK22U2FCnn4nfRuug1vp00zPZQFF3yy9xxckfHBBh2cnTTkpcSWWH4n3GGKcBgJJMFTYllvK8LEELdEFVVbTbPXXYheBttOior55P%25%25T99dg19ZZsEhGiiIu54ffQ%25xP22UagYnn4Cq%25uuga4c00zp26FF3KEexxciROBBhvmoTTkA6MWWHEkiGGKevLJJMN18llv3T8EELR4TVVbjB3XXYWDkttOobp55PJtl99d9gUZZskEqiiIxnRffQcui22UtMynn4x09uugPgq00zKCkFF3wqfxxcfVIBBhZhiTTkTsyWWHux8GGK0TpJJMvL1llvKdYEEL6sjVVb6myXXYrp6ttOUtv55PdSy99dJ51ZZsMXUiiI1HqffQFTr22UaF5nn4xvOuug97u00zKKiFF3wE%25xxc7yOBBhl9sTTkRbmWWH7N0GGKGvgJJMOX7llvCKYEELEiyVVbNLmXXYXJ8ttO3tw55PJfw99dlDMZZsMzSiiIEHwffQDX722UjDdnn4yzDuugOwr00z1BAFF3ZRUxxciTjBBhLSoTTkmIXWWHwkvGGKO9rJJMRG8llvzWLEELAW4VVbIby&width=1678", "callback_required": 1, "email": "alfahad.p@katzion.com", "first_name": "Alfahad", "last_name": "P M", "phone": "9995802118", "total_drive_price": 60000}' );  
        
        req.httpMethod = 'POST';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_Car_Configurator_Heroku_Lead.PostCarConfiguratorLead();  
    }  
    
}