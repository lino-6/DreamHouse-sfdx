public class VGA_CalculateHelperClass
{
    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns the time difference between two dates in 
    *              minutes
    * @param startDt Starting date time
    * @param entDt End Date time
    * @return Double with the value of the difference between these two dates
    */
    public static Double getTimeDifference(Datetime startDt, Datetime endDt){
        Long dt1Long = startDt.getTime();
        Long dt2Long = endDt.getTime();
        Long milliseconds = dt2Long - dt1Long;
        Double seconds = milliseconds / 1000;
        Double minutes = seconds / 60;

        return minutes;
    }
    
    public static Integer getMinutesDifference(Datetime startDt, Datetime endDt){
        Long dt1Long = startDt.getTime();
        Long dt2Long = endDt.getTime();
        Long milliseconds = dt2Long - dt1Long;
        Long seconds = milliseconds / 1000;
        Long minutes = seconds / 60;

        return minutes.intValue();
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the user timezone
    * @param dt Datetime that we are trying to convert
    * @return Datetime in the current user time zone
    */
    public static Datetime convertToUserTimeZone(Datetime dt){
        String userTimeZone = UserInfo.getTimeZone() + '';
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', userTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the dealer timezone
    * @param dt Datetime that we are trying to convert
    * @param dealerTimeZone String with the dealer time zone value
    * @return Datetime in the dealer time zone
    */
    public static Datetime convertDateTimeZone(Datetime dt, String dealerTimeZone){
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', dealerTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }
    
    public static String getDealerState(Lead objLead, Map<String,String> mapofDealerCodeWRTBillingState){
        if(mapofDealerCodeWRTBillingState != null 
               && mapofDealerCodeWRTBillingState.containskey(objLead.VGA_Dealer_Code__c) 
               && mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c) != null){
            return mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c);
        }

        return null;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method that given a date returns true if the date is a holiday
    *              in a particular state and false if it is not.
    * @param mapofPublicHoliday Map of public holidays group by state
    * @param currentDay Day for which we will be checking if it is a public holiday or not
    * @param dealerState Dealer state where the holiday will take place
    * @return Boolean true if the date is a public holiday and false if it is not
    */
    public static Boolean isPublicHoliday(Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday, 
                                           Date currentDay, String dealerState){
        Boolean isPublicHoliday = false; 

        if(mapofPublicHoliday != null && !mapofPublicHoliday.isEmpty())
        {
            if(mapofPublicHoliday.get('all') != null 
               && mapofPublicHoliday.get('all').size() > 0)
            {
                for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get('all'))
                {
                    if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                        && objph.VGA_Holiday_Date__c.day() == currentDay.day() 
                        && objph.VGA_Holiday_Date__c.month() == currentDay.month())
                        {
                            isPublicHoliday = true;
                            break;
                        }
                    }
                    else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c == currentDay)
                        {
                            isPublicHoliday = true;
                            break; 
                        }
                    }
                }
            }
            if(mapofPublicHoliday.containskey(dealerState) 
               && mapofPublicHoliday.get(dealerState) != null)
            {
                for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get(dealerState))
                {
                    if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c.day() == currentDay.day() 
                           && objph.VGA_Holiday_Date__c.month() == currentDay.month())
                        {
                            isPublicHoliday = true;
                            break;
                        }
                    }
                    else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c == currentDay)
                        {
                            isPublicHoliday = true;
                            break; 
                        }
                    }
                }
            }
        }

        return isPublicHoliday;
    }

    public static void getWorkingHoursMap(Set<string> setofUniqueKey, 
                                          Map<string,list<VGA_Working_Hour__c>> mapofWH,
                                          Map<Id, List<VGA_Working_Hour__c>> workHourPerContactIdMap){
            
        if(setofUniqueKey != null && !setofUniqueKey.isEmpty())
        {
            for(VGA_Working_Hour__c objWH :  [select Id, VGA_Brand__c, VGA_Day__c, VGA_Dealer_Code__c, VGA_Contact_ID__c, VGA_End_Time__c,
                                                VGA_Start_Time__c, VGA_Last_Assigned_Date_Time__c, VGA_Sub_Brand__c, VGA_Unique_Key__c, VGA_Working__c,
                                                VGA_Available__c, VGA_Timezone__c, VGA_Start_Date_Date_Time__c, VGA_End_Time_Date_Time__c 
                                                from VGA_Working_Hour__c 
                                                where VGA_Working__c = true 
                                                AND VGA_Available__c = true 
                                                order by VGA_Last_Assigned_Date_Time__c asc NULLS FIRST])
            {
                if(setofUniqueKey.contains(objWH.VGA_Unique_Key__c)){
                     if(!mapofWH.containskey(objWH.VGA_Unique_Key__c))
                        mapofWH.put(objWH.VGA_Unique_Key__c.trim().tolowercase(),new List<VGA_Working_Hour__c>());
                    
                    mapofWH.get(objWH.VGA_Unique_Key__c.trim().tolowercase()).add(objWH);
                }
               

                if(!workHourPerContactIdMap.containsKey(objWH.VGA_Contact_ID__c)){
                     workHourPerContactIdMap.put(objWH.VGA_Contact_ID__c,new List<VGA_Working_Hour__c>());
                }

                workHourPerContactIdMap.get(objWH.VGA_Contact_ID__c).add(objWH);

            }
        }
    }

    public static void populateTradingHoursMap(Map<string,VGA_Trading_Hour__c> mapofTH, Set<String> setDealerCode) {
        
        for(VGA_Trading_Hour__c objTH : [SELECT Id,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,
                                                VGA_End_Time__c,VGA_Dealer_Code__c, VGA_Start_Time__c, 
                                                VGA_Unique_Key__c, VGA_Working__c,VGA_Timezone__c 
                                         FROM VGA_Trading_Hour__c 
                                         WHERE VGA_Dealer_Code__c IN: setDealerCode]){
            if(!mapofTH.containskey(objTH.VGA_Unique_Key__c))
                mapofTH.put(objTH.VGA_Unique_Key__c.trim().tolowercase(),objTH); 
        
        }
    }    

    public static Map<string,VGA_Working_Hour__c> getWorkingHoursPerUniqueKey(List<VGA_Working_Hour__c> workHoursList)  {
        
        Map<string,VGA_Working_Hour__c> mapofWH = new Map<string,VGA_Working_Hour__c>(); 
        
        for(VGA_Working_Hour__c objWH : workHoursList){
            if(!mapofWH.containskey(objWH.VGA_Unique_Key__c))
                mapofWH.put(objWH.VGA_Unique_Key__c.trim().tolowercase(),objWH); 
            
        }

        return mapofWH;
    }
}