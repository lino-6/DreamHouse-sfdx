/*
*author : MOHAMMED ISHAQUE SHAIKH
*description : Batch Class used to create Expired Claim Record for Vehicle Policy (VGA_Vehicle_Policy__c) whose Component Type is 'SVCPACK'
*Created Date : 2019-07-10
*/
global class VGA_ServiceClaimBatch  implements Database.Batchable<SObject>{
    //bellow variable used to Send notification  when batch has any error while Executing 
    global Boolean BatchCheck;
    global String  Query;
    global List<String> ExceptionList;

    //bellow variable used to setup data from Label 
    global static  Integer Constant_KMS=Integer.valueOf(Label.VGA_ServiceClaim_Kilometres)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Kilometres);
    global static  Integer Constant_MONTH=Integer.valueOf(Label.VGA_ServiceClaim_Months)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Months);
    global static  Integer Constant_GST=Integer.valueOf(Label.VGA_ServiceClaim_GST)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_GST);
    global static  Integer Constant_ClaimLimit=Integer.valueOf(Label.VGA_ServiceClaim_Limit)==null?0:Integer.valueOf(Label.VGA_ServiceClaim_Limit);
    global static  String Constant_ComponentType='SVCPACK';
    global static  Set<String> BrandSet=new Set<String>{'PV Volkswagen','CV Volkswagen','Volkswagen'};

    //Used to Process Vehicle Policy
    global Map<Id,VGA_Vehicle_Policy__c> GlobalPolicyMap;
    global Map<Id,ModelServiceIntervalClass> GlobalModelMap;
    global Map<Id,VINWrapperClass> GlobalVehicleMap;

    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *description :Constructor to initialize our Batch 
    */
    global VGA_ServiceClaimBatch(){
        
        ExceptionList=new List<String>();
        BatchCheck=false;
        DateTime CurrentDate= System.now();
        try{
            List<CronTrigger> lstCron = [select Id from CronTrigger where  CronJobDetail.Name LIKE '%ServiceClaimJob%']; //AND NextFireTime  = null];
            if(lstCron != Null && !lstCron.isEmpty()){
                try{
                    for(CronTrigger obj : lstCron){
                        system.abortjob(obj.Id);
                    }
                }catch(Exception e){
                    system.debug('@@Exception in deleting job : ' + e.getMessage());
                }
            } 
            Query = 'SELECT Id, Name, VGA_Policy_End_Date__c, VGA_Policy_Start_Date__c, VGA_VIN__c, VGA_VIN__r.Product2Id, VGA_VIN_No__c, VGA_Component_Code__c, VGA_Component_Type__c ,VGA_Service_Batch_Stamp__c,VGA_Max_Policy_End_Date__c FROM VGA_Vehicle_Policy__c where VGA_VIN__r.Product2Id!=null and VGA_Component_Type__c=\''+Constant_ComponentType+'\' and  VGA_Component_Code__c!=null and VGA_Policy_Start_Date__c<='+CurrentDate.format('YYYY-MM-dd')+' and VGA_Max_Policy_End_Date__c>='+CurrentDate.format('YYYY-MM-dd')+' and VGA_Service_Batch_Stamp__c!='+CurrentDate.format('YYYY-MM-dd');//+' and id=\'a0h2N000000JP7W\'';
            BatchCheck=false;
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            alertUser(null);
        }
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name :rescheduleServiceClaim
    *Description : used to reschudle Batch VGA_ServiceClaimBatch Class
    */
    private  void rescheduleServiceClaim(){
        try{
            /*Datetime dt = system.now().addMinutes(5);
            String day = string.valueOf(dt.day());
            String month = string.valueOf(dt.month());
            String hour = string.valueOf(dt.hour());
            String minute = string.valueOf(dt.minute());
            String second = string.valueOf(dt.second());
            String year = string.valueOf(dt.year());
            */
            
            String strJobName = 'ServiceClaimJob-' + String.valueof(System.now());
            String strSchedule = '0 0 22 * * ? *';
            
            if(!test.isRunningTest()) 
                System.schedule(strJobName, strSchedule, new VGA_ScheduleServiceClaim());
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name :alertUser
    *Description : used to send Notification Email, if 'Batch Check' is true
    *Params :
    *Database.BatchableContext BC=> it is used to capture Batch class details in Email 
    */
    private  void alertUser(Database.BatchableContext BC){
        if(BatchCheck){
            String Message='Hi, <br/><br/> Service Claim Batch failed to run due to following error : <br/><br/>';
            Message+='<ul>';
            for(String l:ExceptionList){
                Message+='<li>'+l+'</li>';
            }
            Message+='</ul><br/>';
            if(BC !=null){
                AsyncApexJob Job=[SELECT Id, JobType, ApexClass.name, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, MethodName, CompletedDate, ExtendedStatus FROM AsyncApexJob where id=:BC.getJobId()];
                Message+='<h3>Batch Class Details</h3><br/><br/>';
                Message+='<table> <tr> <td><label>JOB ID</label></td> <td>'+Job.Id+'</td> </tr> <tr> <td><label>JOB TYPE</label></td> <td>'+Job.JobType+'</td> </tr> <tr> <td><label>APEX CLASS NAME</label></td> <td>'+Job.ApexClass.name+'</td> </tr> <tr> <td><label>STATUS</label></td> <td>'+Job.Status+'</td> </tr> <tr> <td><label>Total NO OF CHILD JOBs</label></td> <td>'+Job.TotalJobItems+'</td> </tr> <tr> <td><label>NO OF JOB PROCESSED</label></td> <td>'+Job.JobItemsProcessed+'</td> </tr> <tr> <td><label>COMPLETED DATE</label></td> <td>'+Job.CompletedDate+'</td> </tr> <tr> <td><label>EXTENDED STATUS</label></td> <td>'+Job.ExtendedStatus+'</td> </tr> </table>';
            }
            Message+='<br/><br/>Thanks<br/>Salesforce Support';
            VGA_Common.sendExceptionEmail('Exception in Service Claim  Batch',Message);
        }
        rescheduleServiceClaim();
    }

    
    global Database.querylocator start(Database.BatchableContext BC){
        try{
            return Database.getQueryLocator(Query);
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            alertUser(BC);
            return Database.getQueryLocator('select id from VGA_Vehicle_Policy__c where id=\'\'');
        }
    }

    global void execute(Database.BatchableContext BC,List<VGA_Vehicle_Policy__c> PolicyList){
        GlobalPolicyMap=new Map<Id,VGA_Vehicle_Policy__c>();
        GlobalModelMap=new Map<Id,ModelServiceIntervalClass>();
        GlobalVehicleMap=new Map<Id,VINWrapperClass>();
        try{

            System.debug('>>>'+PolicyList);
//SELECT Id, Name, VGA_Policy_End_Date__c, VGA_Policy_Start_Date__c, VGA_VIN__c, VGA_VIN__r.Product2Id, VGA_VIN_No__c, VGA_Component_Code__c, VGA_Component_Type__c ,VGA_Service_Batch_Stamp__c FROM VGA_Vehicle_Policy__c
            Map<Id,ModelServiceIntervalClass> LocalModelMap=new Map<Id,ModelServiceIntervalClass>();
            Map<Id,VINWrapperClass> LocalVehicleMap=new Map<Id,VINWrapperClass>();
            Set<Id> VINSet=new Set<Id>();
            
            for(VGA_Vehicle_Policy__c temp:PolicyList){
                GlobalPolicyMap.put(temp.Id,temp);
                System.debug(temp.VGA_VIN__c);
                if(temp.VGA_VIN__c!=null){
                    if(!GlobalVehicleMap.containsKey(temp.VGA_VIN__c)){
                        LocalVehicleMap.put(temp.VGA_VIN__c,null);
                    }
                    if(temp.VGA_VIN__r.Product2Id!=null){
                        if(!GlobalModelMap.containsKey(temp.VGA_VIN__r.Product2Id)){
                            LocalModelMap.put(temp.VGA_VIN__r.Product2Id,null);
                        }
                    }
                }
            }  
            Map<Id,Asset> AssetMap;
            Map<Id,Product2> ProductMap;
            if(LocalVehicleMap.size()>0){
                AssetMap=collectVINDetailswithClaims(LocalVehicleMap.keySet());
            }
            if(LocalModelMap.size()>0){
                ProductMap=collectModelDetailswithIntervals(LocalModelMap.keySet());
            }
            for(Id tmp:LocalVehicleMap.keySet()){
                if(AssetMap.containsKey(tmp)){
                    VINWrapperClass temp=new VINWrapperClass(AssetMap.get(tmp));
                    LocalVehicleMap.put(tmp,temp);
                }
            }
            for(Id tmp:LocalModelMap.keySet()){
                if(ProductMap.containsKey(tmp)){
                    ModelServiceIntervalClass temp=new ModelServiceIntervalClass(ProductMap.get(tmp));
                    LocalModelMap.put(tmp,temp);
                }
            }
            GlobalVehicleMap.putAll(LocalVehicleMap);
            GlobalModelMap.putAll(LocalModelMap);
            doCalculation();

        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }


    global void finish(Database.BatchableContext BC){
        try{
            /*
            Map<Id,List<VGA_Service_Claim__c>> CreateNewServiceClaimMap=new Map<Id,List<VGA_Service_Claim__c>>();
            Date currentDate=System.today();
            for(VGA_Vehicle_Policy__c temp:GlobalPolicyMap.values()){
                List<VGA_Service_Claim__c> tmpExpiredClaimList=collectNewExpiredClaims(temp.Id);
                System.debug('COUNT>>>'+tmpExpiredClaimList);
                if(tmpExpiredClaimList.size()>0){
                    CreateNewServiceClaimMap.put(temp.Id,tmpExpiredClaimList);
                }

                temp.VGA_Service_Batch_Stamp__c=currentDate;
            }
            generateExpiredCLaim(CreateNewServiceClaimMap);
            updatePolicies();
            */
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
        }
        alertUser(BC);
        if(Test.isRunningTest()){
            BatchCheck=true;
            ExceptionList.add('TestData');
            alertUser(BC);
        }
    }

    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name :doCalculation
    *Description : 1)it has logic to identify Expired Claim for Vehicle Policy.
    *              2)create new Service Claim for Expired Policy.
    *              3)Updating VGA_Service_Batch_Stamp__c field with Current date in Vehicle Policy.
    */
    private void doCalculation(){
        try{
            Map<Id,List<VGA_Service_Claim__c>> CreateNewServiceClaimMap=new Map<Id,List<VGA_Service_Claim__c>>();
            Date currentDate=System.today();
            for(VGA_Vehicle_Policy__c temp:GlobalPolicyMap.values()){
                List<VGA_Service_Claim__c> tmpExpiredClaimList=collectNewExpiredClaims(temp.Id);
                System.debug('COUNT>>>'+tmpExpiredClaimList);
                if(tmpExpiredClaimList.size()>0){
                    CreateNewServiceClaimMap.put(temp.Id,tmpExpiredClaimList);
                }

                temp.VGA_Service_Batch_Stamp__c=currentDate;
            }
            generateExpiredCLaim(CreateNewServiceClaimMap);
            updatePolicies();
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }

    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name :generateExpiredCLaim
    *Description : 2)create new Service Claim for Expired Interval in Policy.
    *           
    */
    private void generateExpiredCLaim(Map<Id,List<VGA_Service_Claim__c>> NewServiceClaimMap){
        try{
            List<VGA_Service_Claim__c> newList=new List<VGA_Service_Claim__c>();
            for(Id tmp:NewServiceClaimMap.keySet()){
                newList.addAll(NewServiceClaimMap.get(tmp));
            }
            if(newList.size()>0)
                insert newList;
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name :collectNewExpiredClaims
    *Description : Identify Missed/Expired Claim for policy
    *return  list of new Expired Claim which need to be created.       
    */
    private List<VGA_Service_Claim__c> collectNewExpiredClaims(Id PolicyId){
        try{
            List<VGA_Service_Claim__c> ServiceClaimExpiredList=new List<VGA_Service_Claim__c>();
            VGA_Vehicle_Policy__c Policy=GlobalPolicyMap.get(PolicyId);
            if(Policy.VGA_Policy_Start_Date__c!=null  && Policy.VGA_Policy_End_Date__c!=null && Policy.VGA_Component_Code__c!=''){
                ModelServiceIntervalClass ModelData=GlobalModelMap.get(Policy.VGA_VIN__r.Product2Id);
                VINWrapperClass VehicleData=GlobalVehicleMap.get(Policy.VGA_VIN__c);
                for(VGA_Service_Interval__c temp:ModelData.IntervalMap.values()){
                   if(!((VehicleData.ClaimIntervalSet).contains(''+temp.Id))){
                        if(temp.VGA_Component_Code__c.contains(Policy.VGA_Component_Code__c)){
                            if(System.today()>Policy.VGA_Policy_Start_Date__c.addMonths(Integer.valueOf(temp.VGA_Months__c==null?0:temp.VGA_Months__c)+Constant_MONTH) || temp.VGA_Service_Order__c<VehicleData.MaxServiceOrder){
                                VGA_Service_Claim__c tempClaim=new VGA_Service_Claim__c();
                                tempClaim.VGA_Asset__c=Policy.VGA_VIN__c;
                                tempClaim.VGA_Service_Interval__c=temp.Id;
                                tempClaim.VGA_Vehicle_Policy__c=Policy.Id;
                                tempClaim.VGA_Is_Expired_Claim__c=true;
                                ServiceClaimExpiredList.add(tempClaim);
                            }
                        }
                    } 
                }
            }
            System.debug('>>>'+ServiceClaimExpiredList);
            return ServiceClaimExpiredList;
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return new List<VGA_Service_Claim__c>();
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name : updatePolicies
    *Description : 3)Updating VGA_Service_Batch_Stamp__c field with Current date in Vehicle Policy.
    *       
    */
    private void updatePolicies(){
        try{
            if(GlobalPolicyMap.size()>0)
                update GlobalPolicyMap.values();
        }catch(Exception ex){
            BatchCheck=true;
            ExceptionList.add(ex.getMessage()+'\t Line Number : '+ex.getLineNumber());
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }    
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name : collectVINDetailswithClaims
    *Description : to collect VIN Information  with Service Claim  
    * Params:
    *VINIdSet => contains list of Vehicle Record Ids      
    */
    @RemoteAction@ReadOnly
    public static Map<Id,Asset> collectVINDetailswithClaims(Set<Id> VINIdSet){
        try{
            return new Map<Id,Asset>([SELECT Id,Product2Id,Product2.ProductCode,(SELECT VGA_Vehicle_Policy__c, VGA_Dealer__c, VGA_Asset__c, Name,VGA_Service_Interval__c ,VGA_Service_Interval__r.VGA_Service_Order__c,VGA_Service_Interval_Code__c,VGA_VIN__c, Id, VGA_Is_Expired_Claim__c from Service_Claims__r) from Asset where Id in:VINIdSet]);
        }catch(Exception ex){
            
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return new Map<Id,Asset>();
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name : collectModelDetailswithIntervals
    *Description : to collect Product Information  with Service Claim  
    * Params:
    *VINIdSet => contains list of Vehicle Record Ids      
    */
    @RemoteAction@ReadOnly
    public static Map<Id,Product2> collectModelDetailswithIntervals(Set<Id> ModelIdSet){
        try{
            return new Map<Id,Product2>([select Id, Productcode ,(SELECT VGA_Service_Order__c, VGA_Months__c, VGA_KMS__c, VGA_Code__c, VGA_Product2__c, VGA_Component_Code__c, Id FROM Service_Intervals__r order by VGA_Service_Order__c) from Product2 where Id in:ModelIdSet]);
        }catch(Exception ex){
            
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return new Map<Id,Product2>();
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Wrapper Class Name : ModelServiceIntervalClass
    *Description : to store Product information in particular format  
    *    
    */

    global class ModelServiceIntervalClass{
        global Product2 Model;
        global Map<Id,VGA_Service_Interval__c> IntervalMap;
        global ModelServiceIntervalClass(Product2 P){
            Model=P;
            IntervalMap=new Map<Id,VGA_Service_Interval__c>();
            for(VGA_Service_Interval__c inter:P.Service_Intervals__r){
                IntervalMap.put(inter.Id,inter);
            }
        }
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Wrapper Class Name : VINWrapperClass
    *Description : to store Vehicle(Asset) information in particular format  
    *    
    */
    global class VINWrapperClass{
        global String VehiclePolicy;
        global Asset Vehicle;
        global List<VGA_Service_Claim__c>  AvailableClaimedService;
        global List<VGA_Service_Claim__c>  AvailableExpiredService;
        global List<VGA_Service_Claim__c>  NotAvailableExpiredService;
        global Set<Id> ClaimIntervalSet;
        global Integer MaxServiceOrder;
        global VINWrapperClass(Asset a){
            Vehicle=a;
            AvailableClaimedService=new List<VGA_Service_Claim__c>();
            AvailableExpiredService=new List<VGA_Service_Claim__c>();
            ClaimIntervalSet=new Set<Id>();
            MaxServiceOrder=0;
            
            NotAvailableExpiredService=new List<VGA_Service_Claim__c>();
            for(VGA_Service_Claim__c clm:a.Service_Claims__r){
                if(clm.VGA_Service_Interval__c!=null){
                    ClaimIntervalSet.add(clm.VGA_Service_Interval__c);
                }
                if(clm.VGA_Is_Expired_Claim__c){
                    AvailableExpiredService.add(clm);
                }else{
                    if(clm.VGA_Service_Interval__r.VGA_Service_Order__c!=null){
                        if(clm.VGA_Service_Interval__r.VGA_Service_Order__c>MaxServiceOrder){
                            MaxServiceOrder=(Integer)clm.VGA_Service_Interval__r.VGA_Service_Order__c;
                        }
                    }
                    AvailableClaimedService.add(clm);
                }
            }
        }
    }
}