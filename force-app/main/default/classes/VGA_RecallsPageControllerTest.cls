@isTest
public class VGA_RecallsPageControllerTest {

    @testSetup
    static void buildConfigList() {
        Asset vehicle = VGA_CommonTracker.buildVehicle('WV1ZZZ7HZ9H044254', VGA_CommonTracker.createAccount().Id);
        insert vehicle;

        VGA_Ownership__c ownership = VGA_CommonTracker.buildOwnership(vehicle.Id, UserInfo.getUserId());
        ownership.VGA_Type__c = 'Current Owner';
        insert ownership;

        VGA_Service_Campaigns__c takataServCampaign = VGA_CommonTracker.buildTakataServiceCampaing(vehicle.Id);
        insert takataServCampaign;

        Recall_DocuSign_Settings__c recallDocuSett = VGA_CommonTracker.buildRecallDocusignSettings();
        insert recallDocuSett;
    }

    /**
    * @author Lino Diaz
    * @date 25/09/2018
    * @description Method that tests the updateCase method on the VGA_RecallsPageController class
    *              When the Update Case button is clicked
    *              then a the case will be updated adding some new values to the docusign fields
    */
    @isTest static void test01_UpdateCase_whenWeClickUpdateCaseButton_CaseShouldBeUpdate() {
        VGA_Service_Campaigns__c service_obj = [SELECT Id, VGA_SC_Vehicle__c,VGA_SC_Vehicle__r.VGA_VIN__c,
                                                       Campaign_Code__c 
                                                FROM VGA_Service_Campaigns__c 
                                                WHERE VGA_SC_Vin__c = 'WV1ZZZ7HZ9H044254' 
                                                LIMIT 1];

        Test.startTest();
            ID recallRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Recall Refused').getRecordTypeId();
        
            Case vw_case_obj = new Case();
            vw_case_obj.Subject = 'Test';
            vw_case_obj.RecordTypeId = recallRTID;
            vw_case_obj.VW_Service_Campaign_VIN__c = service_obj.id;
                
            INSERT vw_case_obj;

            VGA_RecallsPageController.updateCase(vw_case_obj.Id);
        Test.stopTest();

        //Make sure that the docusign fields have been updated with the right values
        Case updatedCase = [SELECT Id, VGA_VIN__c, VGA_DocuSign_Recall_VIN_No__c, VGA_DocuSign_Recall_Campaign_No__c, VGA_DocuSign_Recall_Date_Of_Service__c FROM Case WHERE Id =: vw_case_obj.Id];
        System.assertEquals(service_obj.VGA_SC_Vehicle__c, updatedCase.VGA_VIN__c);
        System.assertEquals(service_obj.VGA_SC_Vehicle__r.VGA_VIN__c, updatedCase.VGA_DocuSign_Recall_VIN_No__c);
        System.assertEquals(service_obj.Campaign_Code__c, updatedCase.VGA_DocuSign_Recall_Campaign_No__c);
        System.assertEquals(system.now().format('dd-MM-YYYY'), updatedCase.VGA_DocuSign_Recall_Date_Of_Service__c);
    }

    /**
    * @author Lino Diaz
    * @date 25/09/2018
    * @description Method that tests the updateCase method on the VGA_RecallsPageController class
    *               When the Update Case button is clicked and the conditions are not met
    *               for the logic to run
    *               then a the case won't be updated and the docusign fields will keep the
    *               same values that they had
    */
    @isTest static void test02_UpdateCaseNegative_whenWeClickUpdateCaseButton_CaseShouldBeUpdate() {
        VGA_Service_Campaigns__c service_obj = [SELECT Id, VGA_SC_Vehicle__c,VGA_SC_Vehicle__r.VGA_VIN__c,
                                                       Campaign_Code__c 
                                                FROM VGA_Service_Campaigns__c 
                                                WHERE VGA_SC_Vin__c = 'WV1ZZZ7HZ9H044254' 
                                                LIMIT 1];

        Test.startTest();
            ID complaintTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
        
            Case vw_case_obj = new Case();
            vw_case_obj.Subject = 'Test';
            vw_case_obj.RecordTypeId = complaintTID;
            vw_case_obj.VW_Service_Campaign_VIN__c = service_obj.id;
                
            INSERT vw_case_obj;

            VGA_RecallsPageController.updateCase(vw_case_obj.Id);
        Test.stopTest();

        //Make sure that the docusign fields were not updated
        Case updatedCase = [SELECT Id, VGA_VIN__c, VGA_DocuSign_Recall_VIN_No__c, VGA_DocuSign_Recall_Campaign_No__c, VGA_DocuSign_Recall_Date_Of_Service__c FROM Case WHERE Id =: vw_case_obj.Id];
        System.assertNotEquals(service_obj.VGA_SC_Vehicle__c, updatedCase.VGA_VIN__c);
        System.assertNotEquals(service_obj.VGA_SC_Vehicle__r.VGA_VIN__c, updatedCase.VGA_DocuSign_Recall_VIN_No__c);
        System.assertNotEquals(service_obj.Campaign_Code__c, updatedCase.VGA_DocuSign_Recall_Campaign_No__c);
        System.assertNotEquals(system.now().format('dd-MM-YYYY'), updatedCase.VGA_DocuSign_Recall_Date_Of_Service__c);
    }


    /**
    * @author Lino Diaz
    * @date 25/09/2018
    * @description Method that tests the sendDocuSignContract method on the VGA_RecallsPageController class
    *              When the Send DocuSign Contract is clicked
    *              then we will be calling the Docusign Webservice
    */
    @isTest static void test03_SendDocuSignContract_whenWeClickSendDocuSignContract_DocusignAPIShouldBeCalled() {
        VGA_Service_Campaigns__c service_obj = [SELECT Id, VGA_SC_Vehicle__c,VGA_SC_Vehicle__r.VGA_VIN__c,
                                                       Campaign_Code__c 
                                                FROM VGA_Service_Campaigns__c 
                                                WHERE VGA_SC_Vin__c = 'WV1ZZZ7HZ9H044254' 
                                                LIMIT 1];
        VGA_Ownership__c ownership = [SELECT Id, VGA_VIN_for_Mule__c FROM VGA_Ownership__c LIMIT 1 ];       
        Asset vehicle = [SELECT Id, VGA_VIN__c FROM Asset WHERE VGA_VIN__c = 'WV1ZZZ7HZ9H044254'];

        Test.startTest();
            ID recallRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Recall Refused').getRecordTypeId();
        
            Case vw_case_obj = new Case();
            vw_case_obj.Subject = 'Test';
            vw_case_obj.RecordTypeId = recallRTID;
            vw_case_obj.VW_Service_Campaign_VIN__c = service_obj.id;
            vw_case_obj.VGA_VIN__c = vehicle.Id; 

            INSERT vw_case_obj;

            String token = VGA_RecallsPageController.sendDocuSignContract(vw_case_obj.Id);
        Test.stopTest();

        //If the process is completed token should be equals to test
        //As the process is running synchronously class is sending a mock response
        //if the method is called from a test method
        System.assertEquals('test', token);
    }

    /**
    * @author Lino Diaz
    * @date 25/09/2018
    * @description When the Send DocuSign Contract is clicked and the case doesn't meet
    *              the criteria
    *              then we will receive an error instead of the toeken
    */
    @isTest static void test04_SendDocuSignContractNegative_whenWeClickSendDocuSignContract_CriteriaNotMet_DocusignAPIShouldNotBeCalled() {
        VGA_Service_Campaigns__c service_obj = [SELECT Id, VGA_SC_Vehicle__c,VGA_SC_Vehicle__r.VGA_VIN__c,
                                                       Campaign_Code__c 
                                                FROM VGA_Service_Campaigns__c 
                                                WHERE VGA_SC_Vin__c = 'WV1ZZZ7HZ9H044254' 
                                                LIMIT 1];
        VGA_Ownership__c ownership = [SELECT Id, VGA_VIN_for_Mule__c FROM VGA_Ownership__c LIMIT 1 ];       
        Asset vehicle = [SELECT Id, VGA_VIN__c FROM Asset WHERE VGA_VIN__c = 'WV1ZZZ7HZ9H044254'];

        Test.startTest();
            ID complaintTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();
            
            Case vw_case_obj = new Case();
            vw_case_obj.Subject = 'Test';
            vw_case_obj.RecordTypeId = complaintTID;
            vw_case_obj.VW_Service_Campaign_VIN__c = service_obj.id;
            vw_case_obj.VGA_VIN__c = vehicle.Id; 

            INSERT vw_case_obj;

            String error = VGA_RecallsPageController.sendDocuSignContract(vw_case_obj.Id);
        Test.stopTest();

        //If the case doesn't meet the criteria we will be getting an error instead of the token
        System.assertEquals('ERROR: DocuSign signing is only allowed for Takata Airbag Recall.', error);
    }

    /**
    * @author Lino Diaz
    * @date 25/09/2018
    * @description Test the getCaseRecordType method
    *              When the the getCaseRecordType method is called
    *              then we will be getting the case record type name
    */
    @isTest static void test04_GetCaseRecordType_getCaseRecordTypeMethod_whenCallingTheMethod_GetRecordTypeName() {
        VGA_Service_Campaigns__c service_obj = [SELECT Id, VGA_SC_Vehicle__c,VGA_SC_Vehicle__r.VGA_VIN__c,
                                                       Campaign_Code__c 
                                                FROM VGA_Service_Campaigns__c 
                                                WHERE VGA_SC_Vin__c = 'WV1ZZZ7HZ9H044254' 
                                                LIMIT 1];

        Test.startTest();
            ID recallRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Recall Refused').getRecordTypeId();
        
            Case vw_case_obj = new Case();
            vw_case_obj.Subject = 'Test';
            vw_case_obj.RecordTypeId = recallRTID;
            vw_case_obj.VW_Service_Campaign_VIN__c = service_obj.id;
                
            INSERT vw_case_obj;
            //Call getCaseRecordType method
            String recordTypeName = VGA_RecallsPageController.getCaseRecordType(vw_case_obj.Id);
        Test.stopTest();

        //Test that the method is returning the correct record type name.
        System.assertEquals('Recall Refused', recordTypeName);
    }
}