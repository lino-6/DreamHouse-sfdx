global with sharing class VWMeetingCompletedController {
    @InvocableMethod(label='VWMeetingCompletedController' description='Creates an new Task and public PDF link and attaches it to Task field Meeting_PDF_Link')
    public static String[] statusChangedToCompleted(List<String> ids) {

        if (ids.isEmpty()) {
            system.debug('No Meeting ID provided');
            return null;
        }
        
        String meetingId = String.valueOf(ids[0]).substring(0, 15);
        
        MEWrapper meetingEventWrapper = queryMeetingAndEvent(meetingId);
        
        if (meetingEventWrapper == null) {
            system.debug('Either query for meeting or event was not successful OR its a Task meeting OR Event has no WhoID; AKA Name field');
            return null;
        }
        
        Id fileId = createPDF(meetingId, meetingEventWrapper.meeting.Name);
        
        List<ContentDocument> documents = [SELECT Id FROM ContentDocument WHERE Id in (SELECT ContentDocumentId FROM ContentVersion WHERE Id = :fileId AND IsLatest = true AND Share_with_public_notes__c = true)];
        if (documents.isEmpty()) {
            system.debug('No Document was created or found');
            return null;
        }
        
        Task newTask;
        if (meetingEventWrapper.taskRelation.isEmpty()) {
            newTask = new Task(     Subject = meetingEventWrapper.meeting.Name, 
                                    WhoId = meetingEventWrapper.event.WhoId,
                               		status = 'Open',
									// If task Record type needs to be changed, change this value
									// SANDBOX 
									// RecordTypeId = '0120k000000D2K0AAK',   
                                    // Production
									RecordTypeId = '0127F000000YuZJQA0',  
                               		OwnerId = meetingEventWrapper.meeting.OwnerId,
                                    ActivityDate = Date.today() + 5); 
            insert newTask;
        } else {
            
            String PDFUrl = '';
            
            for (sObject relation : meetingEventWrapper.taskRelation) {
				
                Datetime startTime = meetingEventWrapper.meeting.punosmobile__Start_Time__c;
				
                date myDate = date.newInstance(startTime.year(), startTime.month(), startTime.day());
                
				String dateString = myDate.format();
                
           		newTask = new Task(     Subject = 'Accept minutes: ' + meetingEventWrapper.meeting.Name + ', ' + dateString,
                						OwnerId = meetingEventWrapper.meeting.OwnerId,
										status = 'Open',
										// If task Record type needs to be changed, change this value
                          	         	// RecordTypeId = '0120k000000D2K0AAK',                        		
                    					// Production
										 RecordTypeId = '0127F000000YuZJQA0', 
                                   		ActivityDate = Date.today() + 5); 
                                    
            	insert newTask;
                
                List<sObject> insertableList = new List<sObject>();

                relation.put('TaskId', newTask.Id); 
                insertableList.add(relation);

            	insert insertableList;
                
                // PDF from the task Files leaved away 1.2.2018 VM
                // createContentDocumentLink(documents[0].Id, newTask.Id);
                
                List<ContentVersion> cv = [SELECT Id From ContentVersion WHERE Id = :fileId AND IsLatest = true AND Share_with_public_notes__c = true];
        		if (cv.isEmpty()) {
            		system.debug('No ContentVersion was found for ID: ' + fileId);
            		return null;
        		}
                
                String CDName = meetingEventWrapper.meeting.Name;
                
        		// Add a link to the created document to the task and update it.
                if (PDFUrl.length() == 0){
                	PDFUrl = createContentDistribution(cv[0].id, CDName);	    
                }
        		newTask.Meeting_PDF_Link__c = PDFUrl;
        		update newTask;
            }
            
        
        }
        
        	//Get all Files and Attachments from the Event
            //Associate all Files and Attachments with the Task
            //Files
        List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
        
        if (meetingEventWrapper.Event != null){
           	contentDocumentLinks = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :meetingEventWrapper.Event.Id];
        }
        else
        {
            return null;
        }
        
        List<Id> CDLIds = new List<Id>();

        for (ContentDocumentLink cdl : contentDocumentLinks ){
            CDLIds.add(cdl.ContentDocumentId);
        }

		List<ContentVersion> contentVersionsShared = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN :CDLIds AND Share_with_public_notes__c = true];
        if(contentVersionsShared.isEmpty()){
            return null;
        }
        
        List<ContentDocumentLink> contentDocumentLinksToBeUpdated = new List<ContentDocumentLink>();
        
        for (ContentVersion cv : contentVersionsShared){
        	ContentDocumentLink testContentDocumentLink = new ContentDocumentLink();
			testContentDocumentLink.LinkedEntityId = newTask.Id;
			testContentDocumentLink.ShareType = 'V'; 
			testContentDocumentLink.ContentDocumentId = cv.ContentDocumentId;
			testContentDocumentLink.Visibility = 'AllUsers'; 
			contentDocumentLinksToBeUpdated.add(testContentDocumentLink);
        }        
        
        insert contentDocumentLinksToBeUpdated;
        
        //Attachments
        //NOT NEEDED ANY LONGER
        /*
        List<Attachment> attachments = new List<Attachment>();                
        if (meetingEventWrapper.Event != null){
            attachments = [SELECT Id, Name, Body, OwnerId, ParentId FROM Attachment WHERE ParentId = :meetingEventWrapper.Event.Id];
        }
        else {
            return null;
        }
        
        List<Attachment> attachmentsToBeUpdated = new List<Attachment>();      
        
        for (Attachment att : attachments){
        
                Attachment testAttachment = new Attachment(contentType = '.png',
                                                   body = att.body,
                                                   name = att.Name,
                                                   OwnerId = att.OwnerID,
                                                   ParentId = newTask.Id);
            
         attachmentsToBeUpdated.add(testAttachment);   
            
        }
        
        insert attachmentsToBeUpdated;
        */
        
        List<String> successList = new List<String>();
        successList.add('success');
        return successList;
    }
    
    //Creates a new ContentVersion. This is later associated with a ContentDocumentLink.
    @TestVisible
    private static Id createPDF(Id meetingId, String name){
        PageReference pdfPage = Page.CustomPDFAttachment;
    	pdfPage.getParameters().put('id',meetingId);
    	pdfPage.getParameters().put('pdf','true');
    	pdfPage.getParameters().put('ext','1');
        
        Blob body;
        
        try {         
            if(Test.isRunningTest()){
            	body = blob.valueOf('DummyDataForPdf');
        	}else{
            	body = pdfPage.getContent();
        	}
        } catch (VisualforceException e) {
            system.debug(e);
            body = Blob.valueOf('e');
        }
        
        //Create tbe content version
        SObject contentVersion = Schema.getGlobalDescribe().get('ContentVersion').newSObject();
        contentVersion.put('ContentLocation', 'S');
        contentVersion.put('VersionData', body);
        contentVersion.put('Title', name);
        contentVersion.put('PathOnClient', 'Testpdf.pdf');
        
        insert contentVersion; 
         
        //Return the above created content version id. This is needed to create the content document link associating the content version with the sObject.        
        return contentVersion.id;
    }
    
    //Creates a ContentDocumentLink associating a ContentDocument with an Event.
    private static void createContentDocumentLink(Id cdId, Id taskId){
    	ContentDocumentLink contentDocumentLink = new ContentDocumentLink();
    	contentDocumentLink.LinkedEntityId = taskId;
		contentDocumentLink.ShareType = 'V'; 
		contentDocumentLink.ContentDocumentId = cdId;
		contentDocumentLink.Visibility = 'AllUsers'; 
		insert contentDocumentLink;
    }
    
    private static Task createTask() {
        
        return null;
    }
    
    private static String createContentDistribution(Id cvId, String name) {
        
        SObject contentDistribution = Schema.getGlobalDescribe().get('ContentDistribution').newSObject();
        System.debug('ContName ' + name);
        contentDistribution.put('Name', name);
        System.debug('ContentVersionId ' + cvId);
        contentDistribution.put('ContentVersionId', cvId);
        
        insert contentDistribution;
        
        //List<sObject> cd = Database.query('SELECT DistributionPublicUrl FROM ContentDistribution WHERE ContentVersionId = '' + cvId + ''');
        List<sObject> cd = [SELECT DistributionPublicUrl FROM ContentDistribution WHERE ContentVersionId = :cvId];
        
        if (cd.isEmpty()){
            system.debug('No ContentDistribution found!');
            return null;
        }
        
        String externalPDFUrl = (String)cd[0].get('DistributionPublicUrl');
        
        return externalPDFUrl;
    }
    
    private class MEWrapper{
        punosmobile__Meeting__c meeting;
        Event event;
        List<Id> eventWhoIds = new List<Id>();
        List<sObject> taskRelation = new List<sObject>();
    }

    private static MEWrapper queryMeetingAndEvent(Id meetingId) {
        MEWrapper wrapper = new MEWrapper();
        
        List<punosmobile__Meeting__c> meetings = [SELECT 
                                                Id, 
                                                Name,
                                                OwnerId,
                                                punosmobile__Start_Time__c,
                                                punosmobile__Event__c
                                                FROM punosmobile__Meeting__c
                                                WHERE Id = :meetingId];
                                        
        if (!meetings.isEmpty() && meetings[0].punosmobile__Event__c != null) {
            wrapper.meeting = meetings[0];
        } else {
            return null;
        }
        
        List<Event> events = [SELECT Id, WhoId FROM Event WHERE Id = :wrapper.meeting.punosmobile__Event__c];
        
        if (!events.isEmpty() && events[0].WhoId != null) {
            wrapper.event = events[0];
        } else {
            return null;
        }
        
        // We need to handle WhoId's differently when Shared Activity is on as Event Name field can have multiple objects in it.
        Map<String, Schema.SObjectField> M = Schema.SObjectType.EventRelation.fields.getMap();
        
        if (M.containsKey('IsParent')) {
            //List<sObject> eventWhoObjects = Database.query('SELECT Id, Relation.Name, RelationId FROM EventWhoRelation WHERE EventId = '' + wrapper.event.Id + ''' );
            List<sObject> eventWhoObjects = [SELECT Id, Relation.Name, RelationId FROM EventWhoRelation WHERE EventId = :wrapper.event.Id];
            
            if (!eventWhoObjects.isEmpty()) {
                
                for (SObject obj : eventWhoObjects) {
                    Id id = (String)obj.get('RelationId');
                    
                    wrapper.eventWhoIds.add(id);
                    
                    sObject tskRelation = Schema.getGlobalDescribe().get('TaskRelation').newSObject();
                    tskRelation.put('RelationId', id);
                    wrapper.taskRelation.add(tskRelation);
                }
                
            } else {
                return null;
            }
        }
        
        return wrapper;   
    }

}