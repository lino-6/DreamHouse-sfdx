// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          15-September-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This class is used in Dealer Traffic and Campaign Activity Component
public class VGA_WrapperofProduct 
{
@AuraEnabled
public string productName{get;set;}          //To get productName in Dealer Traffic and Campaign Activity Component.
@AuraEnabled
public string productId{get;set;}            //To get productId in Dealer Traffic and Campaign Activity Component.
@AuraEnabled
public Decimal WalkIns{get;set;}             //To get walkin in Dealer Traffic Component.
@AuraEnabled
public Decimal Internet{get;set;}            //To get Internet in Dealer Traffic Component.
@AuraEnabled
public Decimal PhoneIns{get;set;}            //To get PhoneIns in Dealer Traffic Component.
@AuraEnabled
public Decimal Newones{get;set;}            //To get New in Dealer Traffic Component.
@AuraEnabled
public Decimal Demo{get;set;}            //To get Demo in Dealer Traffic Component.
@AuraEnabled
public Decimal Fleet{get;set;}            //To get Fleet in Dealer Traffic Component.
@AuraEnabled
public string Id{get;set;}                   //To get Record Id in Dealer Traffic Component and Campaign Activity Component.
@AuraEnabled
public Decimal TestDrives{get;set;}          //To get TestDrives in Dealer Traffic Component.
@AuraEnabled
public Decimal TotalTestDrives{get;set;}    //To get TotalTestDrives in Dealer Traffic Component.
@AuraEnabled
public Decimal TotalInternet{get;set;}      //To get TotalInternet in Dealer Traffic Component.
@AuraEnabled
public Decimal TotalWalkIns{get;set;}       //To get TotalWalkIns in Dealer Traffic Component.
@AuraEnabled
public Decimal TotalPhoneIns{get;set;}     //To get TotalPhoneIns in Dealer Traffic Component.
@AuraEnabled
public Decimal Newcount{get;set;} 
@AuraEnabled
public Decimal DemoCount{get;set;}
@AuraEnabled
public Decimal TotalNewcount{get;set;}
@AuraEnabled
public Decimal TotalDemoCount{get;set;}
@AuraEnabled
public Decimal TotalFleetCount{get;set;}
@AuraEnabled
public Decimal CarConfiguratorDownloads{get;set;} //To get CarConfiguratorDownloads in Campaign Activity Component.
@AuraEnabled
public Decimal SearchImpressions{get;set;}       //To get SearchImpressions in Campaign Activity Component.
@AuraEnabled
public Decimal WebTraffic{get;set;}             //To get WebTraffic in Campaign Activity Component.
@AuraEnabled
public string Display{get;set;}                //To get Display in Campaign Activity Component.
@AuraEnabled
public string Social{get;set;}                //To get Social in Campaign Activity Component.
@AuraEnabled
public VGA_Campaign_Activity_Header__c objCampaignActivity{get;set;}                //To get Social in Campaign Activity Component.
public VGA_WrapperofProduct()
{
    
}

}