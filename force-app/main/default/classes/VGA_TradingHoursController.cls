// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          20-September-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to get Dealership Profile and Details.
public without  sharing class VGA_TradingHoursController 
{
// -----------------------------------------------------------------------------------------------------------------
// This method is used to login user Details
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - None
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static user getlogginguser()
{
    User objUser =VGA_Common.getloggingAccountDetails();//get user Information from Common class
    return objUser;
}
// -----------------------------------------------------------------------------------------------------------------
// This method is used to login trading Hours according to selected radio button .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
 @AuraEnabled
 public static list<VGA_Trading_Hour__c> listofTradingHours(string radiobuttonvalue)
 {    list<VGA_Trading_Hour__c>listofTrading =new list<VGA_Trading_Hour__c>(); //list of Trading Hours.
      User ObjUser =[select Id,ContactId,Contact.AccountId from User where Id=:UserInfo.getUserId() limit 1]; //To get user Account Id.
      if(ObjUser !=Null && ObjUser.Contact.AccountId !=Null)
      {
        VGA_Dealership_Profile__c objDealership=[select Id,VGA_Brand__c,VGA_Dealership__c,Name,
                                                    VGA_Distribution_Method__c,VGA_Escalate_Minutes__c,
                                                    VGA_Nominated_User__c,VGA_Sub_Brand__c from VGA_Dealership_Profile__c where 
                                                    VGA_Dealership__c=:ObjUser.Contact.AccountId and 
                                                    VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%')limit 1 ]; // To get Dealership Profile Details according to dealership and Sub Brand.
           if(objDealership !=Null)
           {
             listofTrading=[select Id,VGA_Day__c,VGA_Dealership_Profile__c,VGA_End_Time__c,Name,
                          VGA_Start_Time__c,VGA_Working__c from VGA_Trading_Hour__c 
                         where VGA_Dealership_Profile__c=:objDealership.Id  order by VGA_Day__c limit 999]; // To get list of Trading Hoours related to Dealership profile.
              if(listofTrading !=Null && listofTrading.size()>0)
               {
                   return listofTrading;
               }
           }
      }
    return listofTrading;
 }
   // -----------------------------------------------------------------------------------------------------------------
  // This method is used to Dealership Profile according selected radio Button.
 // -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
 @AuraEnabled
 public static VGA_Dealership_Profile__c getDealershipDetails(string radiobuttonvalue)
 {
     VGA_Dealership_Profile__c ObjDealership;
     User ObjUser =[select Id,ContactId,Contact.AccountId from User where Id=:UserInfo.getUserId() limit 1];//To get user Account Id.
     
    if(ObjUser !=Null && ObjUser.Contact.AccountId !=Null)
     {
      objDealership=[select Id,VGA_Brand__c,VGA_Dealership__c,VGA_Dealership__r.VGA_Dealer_Code__c,VGA_Dealership__r.Name,VGA_Dealership__r.VGA_Sub_Brand__c,
                             VGA_Distribution_Method__c,VGA_Escalate_Minutes__c,VGA_Dealership__r.VGA_Active__c,VGA_Last_User_Assigned__c,
                             VGA_Nominated_User__c,VGA_Nominated_User__r.Name,VGA_Sub_Brand__c,lastmodifieddate,VGA_Last_Modified_Email__c from VGA_Dealership_Profile__c where 
                             VGA_Dealership__c=:ObjUser.Contact.AccountId and VGA_Sub_Brand__c LIKE :('%' + radiobuttonvalue + '%') limit 1 ]; // To get Dealership Profile Details according to dealership and Sub Brand.
     }
     return ObjDealership;
 }
   // -----------------------------------------------------------------------------------------------------------------
  // This method is used to get Trading Hours details  .
 // -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
 @AuraEnabled
 public static VGA_Trading_Hour__c getTradingDetails(string RecordId)
 {
   VGA_Trading_Hour__c ObjTrading = new VGA_Trading_Hour__c();
   
   objTrading = [select Id,VGA_Day__c,VGA_End_Time__c,VGA_Start_Time__c,Name,
                                    VGA_Working__c from VGA_Trading_Hour__c where Id=:RecordId limit 1]; // To get Trading Hours Details.
   
   return ObjTrading;
 }
  // -----------------------------------------------------------------------------------------------------------------
  // This method is used to update Trading Information .
 // -----------------------------------------------------------------------------------------------------------------
 // INPUT PARAMETERS:    - VGA_Trading_Hour__c 
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
 @AuraEnabled
 public static string saveTradingDetails(VGA_Trading_Hour__c objTradinglst)
 {
      try
      {
          upsert objTradinglst;
          return 'Working hours saved';
      }
      catch(exception e)
      {
         return 'Error: ' + e.getMessage();
      }
 }
// -----------------------------------------------------------------------------------------------------------------
// This method is used to update Dealership Details .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - VGA_Dealership_Profile__c ,String
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
 @AuraEnabled
 public static string saveDealershipDetails(VGA_Dealership_Profile__c objDealershiplst,string Ids)
 {
    
    if(objDealershiplst.VGA_Distribution_Method__c == 'Nominated User'
      && Ids == '--None--'){
      return 'Error: Please select Nominate User.';
      
    }

    if(objDealershiplst.VGA_Distribution_Method__c != 'Round Robin')
    {
        objDealershiplst.VGA_Nominated_User__c    =Ids;
        if(Ids !=Null)
        {
            list<User>objUser =New list<User>([select Id,ContactId from User where Id=:Ids limit 1]);
            if(objUser !=Null && objUser.size()>0)
            {
                if( objUser[0].ContactId !=Null)
                {
                    objDealershiplst.VGA_Nominated_Contact__c = objUser[0].ContactId;
                }
            }
        } 
    }else if(objDealershiplst.VGA_Distribution_Method__c == 'Round Robin'){
        objDealershiplst.VGA_Nominated_User__c    = null;
        objDealershiplst.VGA_Nominated_Contact__c = null;
    }
    
    try
    {
        upsert objDealershiplst;
        return 'Dealer Saved';    
    }
    catch(exception e)
    {
        if(e.getMessage().contains('A contact cannot be set as Nominated User if it is unavailable to receive leads.'))
        {
            return 'Error: A contact cannot be set as Nominated User if it is unavailable to receive leads.';
        }

        return 'Error: ' + e.getMessage();
    }
          
     
 }
// -----------------------------------------------------------------------------------------------------------------
// This method is used to get list of user related to loging Dealership.
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - None
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         20-September-2017           surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
     @AuraEnabled
     public static map<string,string> listofuser(VGA_Dealership_Profile__c objdealer,string selected)
     {
            string subbrands;
            if(selected =='Passenger')
            {
              subbrands  = 'Passenger Vehicles (PV)'; 
            }
            else
            {
              subbrands  = 'Commercial Vehicles (CV)'; 
            }
           list<VGA_Dealership_Profile__c> objuserlist =New list<VGA_Dealership_Profile__c>();
           Map<String,string> mapofContactNameandId = new Map<String,string>();
             if(objdealer !=Null)
             {
               objuserlist =[select Id, VGA_Nominated_User__c,VGA_Nominated_User__r.Name
                             from VGA_Dealership_Profile__c where Id=:objdealer.Id limit 1];
             }
              if(objuserlist !=Null && objuserlist[0].VGA_Nominated_User__c !=Null)
              {
               mapofContactNameandId.put(objuserlist[0].VGA_Nominated_User__r.Name,objuserlist[0].VGA_Nominated_User__c);   
              }
             else
             {
                mapofContactNameandId.put('--None--','--None--') ;
             }
            string AccountIds =VGA_Common.getloggingAccountId();
             // To get List of User related to logging Dealership .
            list<user>listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId 
                                                from User where contact.AccountId =:AccountIds and IsActive=true limit 999]);
             if(listofUser !=Null && listofUser.size()>0)
              {
                  for(user objuser :listofUser)
                  {
                      mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
                  }
                   return mapofContactNameandId;
              }
            else
            {
                return mapofContactNameandId;
            }
     } 
}