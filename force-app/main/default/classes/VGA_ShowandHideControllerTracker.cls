//Tracker class for VGA_ShowandHideController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for VGA_ShowandHideController on                 8-Nov-2017      100%
//===============================================================================================
@isTest(seeAllData = false) 
public class VGA_ShowandHideControllerTracker 
{

    /**
    * @author Lino Diaz
    * @date 31/12/2018
    * @description Method that tests the getUserInformation method for this scenario:
    *              When one of the user profiles related to a contact is Dealer Admninistrator
    *              The contact role will be always Dealer Administrator as it is the highest
    *              role.
    */
    static testMethod void test01_GetuserInformation_whenWeHaveTwoUserPfiles_ContactRoleWillBeHighest() 
    {
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        Contact objCon = VGA_CommonTracker.createDealercontact(objAcc.id);
        User objUser = VGA_CommonTracker.CreateUser(objCon.id);
        
        VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
        objProfile.VGA_Role__c = 'Consultant';
        objProfile.VGA_Contact__c = objUser.ContactId;
        insert objProfile;

        objProfile = new  VGA_User_Profile__c();
        objProfile.VGA_Role__c = 'Dealer Administrator';
        objProfile.VGA_Contact__c = objUser.ContactId;
        insert objProfile;

        Test.startTest();
        system.runAs(objUser)
        {
            String contactRole = VGA_ShowandHideController.getuserInformation();
            System.assertEquals('Dealer Administrator', contactRole);
        }
        
        Test.stopTest();
    }

    /**
    * @author Lino Diaz
    * @date 31/12/2018
    * @description Method that tests the getUserInformation method for this scenario:
    *              When we have two user profiles related to a contact will get always the
    *              one with the highest access. In this case we have two Consultant and
    *              Dealer Portal Group so we will return Consultant as it is the highest.
    */
    static testMethod void test02_GetuserInformation_whenOneOfRolesIsAdmin_ContactRoleWillBeAdmin() 
    {
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        Contact objCon = VGA_CommonTracker.createDealercontact(objAcc.id);
        User objUser = VGA_CommonTracker.CreateUser(objCon.id);
        
        VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
        objProfile.VGA_Role__c = 'Dealer Portal Group';
        objProfile.VGA_Contact__c = objUser.ContactId;
        insert objProfile;

        objProfile = new  VGA_User_Profile__c();
        objProfile.VGA_Role__c = 'Consultant';
        objProfile.VGA_Contact__c = objUser.ContactId;
        insert objProfile;

        Test.startTest();
        system.runAs(objUser)
        {
            String contactRole = VGA_ShowandHideController.getuserInformation();
            System.assertEquals('Consultant', contactRole);
        }
        
        Test.stopTest();
    }

   /**
    * @author Lino Diaz
    * @date 31/12/2018
    * @description Method that tests the getloancar method that will return the value of
    *              the user field VGA_Is_Loan_Car_Dealer__c that is a Boolean field.
    */
    static testMethod void test03_getloancar_whenWeCallGetLoanCarMethod_WeWillGetValueOnAccRelatedToUser()
    {
       
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        objAcc.VGA_Is_Loan_Car_Dealer__c = true;
        update objAcc;

        Contact objCon = VGA_CommonTracker.createDealercontact(objAcc.id);
        
        User objUser = VGA_CommonTracker.CreateUser(objCon.id);

        system.runAs(objUser)
        {
            Boolean isLoanDealer = VGA_ShowandHideController.getloancar();
            System.assert(isLoanDealer);
        }
    }

    /**
    * @author Lino Diaz
    * @date 31/12/2018
    * @description Method that tests the fetchit method that will return the value of
    *              the url that is going to be used by the loggin user..
    */
    static testMethod void test04_fetchit_whenWeCallFetchItMethod_WeWillGetPortalURLBasedOnBrand()
    {
       
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        objAcc.VGA_Is_Loan_Car_Dealer__c = true;
        update objAcc;

        Contact objCon = VGA_CommonTracker.createDealercontact(objAcc.id);
        objCon.VGA_Brand__c = 'volkswagen';
        update objCon;

        User objUser = VGA_CommonTracker.CreateUser(objCon.id);
        
        system.runAs(objUser)
        {
            String portalURL = VGA_ShowandHideController.fetchit();
            System.assert(portalURL.contains('volkswagen'));
        }

        objCon.VGA_Brand__c = 'skoda';
        update objCon;

        system.runAs(objUser)
        {
            String portalURL = VGA_ShowandHideController.fetchit();
            System.assert(portalURL.contains('skoda'));
        }
    }
}