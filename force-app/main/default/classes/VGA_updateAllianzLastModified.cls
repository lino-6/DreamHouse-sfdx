/**
* @author Archana Yerramilli
* @date 19/10/2018
* @description This is a Queueable class which is triggered from Account object only when there are more than 9000 records to be updated.
* @description Its an Asynchronous Apex processes by using the Queueable interface which allows jobs to queue.
* @description This class updates two fields in Allianz Data Mapping : VGA_Last_Modified_Date_Time__c and VGA_Transaction_Code__c
* @param List<VGA_Allianz_Data_Mapping__c> is passed to the job 
*/

public class VGA_updateAllianzLastModified implements Queueable
{
public List<VGA_Allianz_Data_Mapping__c> lstAllianzMappings; 
    public VGA_updateAllianzLastModified(List<VGA_Allianz_Data_Mapping__c> results) 
    {
              this.lstAllianzMappings = results;
              System.debug('@@@ results in Const method are : ' + results.size());
    }

    public void execute(QueueableContext context)
    {
       for(VGA_Allianz_Data_Mapping__c objADM : lstAllianzMappings)
       {
            objADM.VGA_Last_Modified_Date_Time__c = system.now();                          
       }
       try 
       {
            update lstAllianzMappings;     
       } 
       catch (System.DmlException dmlEx) 
       {
            if (!dmlEx.getMessage().contains('UNABLE_TO_LOCK_ROW')) 
            {
                throw dmlEx;  
            }
       }
    }
}