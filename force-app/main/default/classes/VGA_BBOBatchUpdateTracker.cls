@isTest
public class VGA_BBOBatchUpdateTracker {

    public static void loadData()
    {
        
        Account act = new Account();
        act.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        act.LastName = 'test';
        act.VGA_Brand__c = 'Volkswagen';
        act.VGA_Email_Validation__c = 'Trusted Source';
        act.VGA_Email_Address_Valid__c = true;
        act.VGA_Promotions_Flag__c = true;
        act.VGA_BBO_Batch_Visited__c = null;
        INSERT act;
        
        Product2 prod = new Product2();
        prod.name = 'test';
        prod.VGA_Model_Alias__c = 'test';
        INSERT prod;
        
        Asset ast = new Asset();
        ast.name = 'test';
        ast.VGA_Model_Year__c = '2017';
        ast.product2id = prod.id;
        ast.AccountId = act.id;
        INSERT ast;
        
        VGA_Ownership__c own = new VGA_Ownership__c();
        own.VGA_VIN__c = ast.id;
        own.VGA_Purchase_Date__c = System.now().Date();
        own.vga_owner_name__c = act.id;
        INSERT own;
        
        Lead l = new Lead();
        l.VGA_Request_Type__c = 'Brochure Request';
        l.VGA_Customer_Account__c = act.id;
        l.VGA_Model_of_Interest__c = 'test1, test2';
        l.VGA_Campaign_Source__c = 'test';
        l.LastName = 'test';
        INSERT l;
        
		l = new Lead();
        l.VGA_Request_Type__c = 'Test Drive';
        l.VGA_Customer_Account__c = act.id;
        l.VGA_Model_of_Interest__c = 'test1, test2';
        l.VGA_Campaign_Source__c = 'test';
        l.LastName = 'test';
        INSERT l;     
        
		l = new Lead();
        l.VGA_Request_Type__c = 'Callback Request';
        l.VGA_Customer_Account__c = act.id;
        l.VGA_Model_of_Interest__c = 'test1, test2';
        l.VGA_Campaign_Source__c = 'test';
        l.LastName = 'test';
        INSERT l;  

		l = new Lead();
        l.VGA_Request_Type__c = 'Registration of Interest';
        l.VGA_Customer_Account__c = act.id;
        l.VGA_Model_of_Interest__c = 'test1, test2';
        l.VGA_Campaign_Source__c = 'test';
        l.LastName = 'test';
        INSERT l;    
        
		l = new Lead();
        l.VGA_Request_Type__c = 'Request for Information';
        l.VGA_Customer_Account__c = act.id;
        l.VGA_Model_of_Interest__c = 'test1, test2';
        l.VGA_Campaign_Source__c = 'test';
        l.LastName = 'test';
        INSERT l;           
    }    
    
    @isTest
    public static void test1()
    {
        loadData();
		VGA_BBOBatchUpdate b = new VGA_BBOBatchUpdate();     
        database.executebatch(b,100);
    }
    
}