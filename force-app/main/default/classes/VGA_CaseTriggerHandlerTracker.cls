@isTest(seealldata=false)
private class VGA_CaseTriggerHandlerTracker 
{
    public static Case objCase;
    public static Asset objAsset;
    public static Account objAccount;
    public static Product2 objProduct;
    public static Contact objContact;
    public static Account objPersonAccount;
    String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
    public static User objUser;
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            /*Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;*/
            objAsset.accountid = objAccount.id;
            insert objAsset;
            
            objCase.VGA_Social_Page_Name__c = 'VGA Unoff';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest2() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest3() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'Facebook-VW';
            objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = 'rishi.patel@saasfocus.com';
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
     static testMethod void myUnitTest4() 
     {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'Facebook-VW';
            //objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = 'rishi.patel@saasfocus.com';
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest5() 
     {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'VW Email';
            //objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = Label.VGA_VW_Emails_Academy;
            objCase.VGA_To_Email__c = Label.VGA_VW_Emails_Academy;
            insert objCase;
            
            
        Test.stopTest();
        
    }
    static testMethod void myUnitTest5_1() 
     {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'VW Email';
            //objCase.ContactId = objContact.id;
            //objCase.SuppliedEmail = Label.VGA_VW_Emails_Academy;
            objCase.VGA_To_Email__c = Label.VGA_SKO_Emails_hotalerts;
            insert objCase;
            
            
        Test.stopTest();
        
    }
    static testMethod void myUnitTest6() 
     {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            List<BusinessHours> bhs=[select id from BusinessHours where IsActive=true and name='Customer Support Team'];
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'Skoda Email';
            //objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = Label.VGA_VW_Emails_Academy;
            objCase.VGA_To_Email__c = Label.VGA_SKO_Emails_Academy;
         objCase.VGA_Reopened_Case__c = TRUE;
            insert objCase;
            
            
        Test.stopTest();
        
    }
    static testMethod void myUnitTest7() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 1440;
            insert objCS;
            
            List<BusinessHours> bhs=[select id from BusinessHours where IsActive=true and name='Customer Support Team'];
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'Academy Email';
            //objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = Label.VGA_VW_Emails_Academy;
            objCase.VGA_To_Email__c = Label.VGA_SKO_Emails_Academy;
            insert objCase;
            
            
        Test.stopTest();
    }
    static testMethod void myUnitTest8() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 140;
            insert objCS;
            
            List<BusinessHours> bhs=[select id from BusinessHours where IsActive=true and name='Customer Support Team'];
            objAsset.accountid = objAccount.id;
            objAsset.product2Id = objProduct.id;
            insert objAsset;
            
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.assetID = objAsset.id;
            objCase.Origin = 'Academy Email';
            //objCase.ContactId = objContact.id;
            objCase.SuppliedEmail = Label.VGA_VW_Emails_Academy;
            objCase.VGA_To_Email__c = Label.VGA_SKO_Emails_Academy;
            insert objCase;
            
            
        Test.stopTest();
    }
    static testMethod void myUnitTest9() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 1440;
            insert objCS;
            
            /*Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;*/
            objAsset.accountid = objAccount.id;
            insert objAsset;
            
            objCase.VGA_Omni_Queue_Name__c = 'Academy Email';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest10() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 144;
            insert objCS;
            
            /*Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;*/
            objAsset.accountid = objAccount.id;
            insert objAsset;
            
            objCase.VGA_Omni_Queue_Name__c = 'Academy Email';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
     static testMethod void myUnitTest11() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            VGA_Case_SLA__c objCS = new VGA_Case_SLA__c();
            objCS.name = 'Academy Email';
            objCS.VGA_Priority_Ranking__c = 12;
            objCS.VGA_SLA_Mins__c = 144;
            insert objCS;
            
            /*Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;*/
            objAsset.accountid = objAccount.id;
            insert objAsset;
            
            objCase.VGA_Omni_Queue_Name__c = 'Academy Email';
            //objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            objCase.contactid = objContact.id;
            insert objCase;
            
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
        Test.stopTest();
        
    }
    static testMethod void myUnitTest12() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            objAsset.accountid = objAccount.id;
            insert objAsset;
            
            objCase.VGA_Social_Page_Name__c = 'CV Unofficial';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;
            objCase.VGA_Social_Page_Name__c = 'SK Unofficial';
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
            objCase.VGA_Social_Page_Name__c = 'enquiryvga';
            update objCase;
            
        Test.stopTest();
        
    }
    static testMethod void myUnitTest13() 
    {
        // TO DO: implement unit test
        LoadData();
        
        Test.startTest();  
            
            /*Group g1 = new Group(Name='group name', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
            insert q1;*/
            objAsset.accountid = objAccount.id;
            insert objAsset;
            objCase.status = 'Closed';
            objCase.VGA_Survey_Email_Sent__c =true;
            objCase.VGA_Survey_Email_Sent_Date_Time__c = system.now();
            objCase.contactID = objContact.id;
            objCase.VGA_Social_Page_Name__c = 'Amarok Australia';
            objCase.VGA_Omni_Accepted_Date_Time__c = system.now();
            insert objCase;
            objCase.VGA_Social_Page_Name__c = 'ŠKODA Australia';
            objCase.VGA_Omni_Channel_Status__c  = 'Opened';
            
            update objCase;
            objCase.VGA_Social_Page_Name__c = 'Volkswagen (AU)';
            update objCase;
            
        Test.stopTest();
        
    }
    static testmethod void myUnittest14()
    {
        LoadData();
        
        objUser = VGA_CommonTracker.CreateUser(objContact.id);
        
        system.runAs(objUser)
        {
            objCase.VGA_Created_via_Portal__c = true; 
            insert objCase;
        }
    }
    static testmethod void myUnittest15()
    {
        LoadData();
        
        
        insert objCase;
        objCase.Subject = 'TEST';
        objCase.SuppliedEmail ='rishi.patel@saasfocus.com';
        objCase.VGA_Created_via_Portal__c = true;
        objCase.VGA_Dealer_Id__c = objAccount.id;
        
        update objCase;
    }
    static testmethod void myUnittest16()
    {
        LoadData();
        
        Id ARTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
        
        objCase.Origin = 'Academy Email';
        objCase.RecordTypeID =  ARTId;
        objCase.suppliedEmail = objContact.email;
        
        insert objCase;
        objCase.VGA_No_of_Incoming_Mails__c = 2;
        update objCase;
    } 
    static testmethod void myUnittest17()
    {
        LoadData();
        
        Id ARTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
        
        objCase.Origin = 'Academy Email';
        objCase.RecordTypeID =  ARTId;
        objCase.suppliedEmail = objContact.email;
        objCase.VGA_Reopened_Case__c = true;
        insert objCase;
        objCase.Status = 'Assigned';
        objCase.VGA_To_Email__c = Label.VGA_VW_Emails_Academy;
        update objCase;
    } 

    /**
    * @author Ashok Chandra
    * @date  08/05/2019
    * @description This is a test to update a Interaction to close and check any open tasks related to interaction 
    */    
    static testmethod void test1_Closestatusupdate_checkopentasks()
    {
        LoadData();
        Id ARTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
        case c = new Case();        
       // c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = ARTId;
        c.Origin = 'Dealer CRM VW';  
        c.DocuSign_Recall_Last_Name__c = 'Tester';    
        Insert c;    
        Task ct=new Task (whatid=c.id, subject='test');
        insert ct;
        case updatecase=[select id,status from case where id=:c.id];
        updatecase.Status = 'closed';
         DmlException expectedException;
         Test.startTest();
            try
            {
                 update updatecase;
            }
            catch (DmlException d)
            {
                expectedException = d;
            }
        Test.stopTest();
        System.assertEquals(true, expectedException.getMessage().contains('Interaction cannot be closed if there are open tasks.'));
       } 
    
    
    /**
    * @author Ashok Chandra
    * @date  08/05/2019
    * @description This is a test to update a Interaction to close and check any open tasks related to interaction 
    */    
    static testmethod void tes2_Closestatusupdate_checkopentasks()
    {
        LoadData();
        Id ARTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
        case c = new Case();        
       // c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = ARTId;
        c.Origin = 'Dealer CRM VW';  
        c.DocuSign_Recall_Last_Name__c = 'Tester';    
        Insert c;    
        Task ct=new Task (whatid=c.id, subject='test');
        insert ct;
        
        Task tt=[select id,status from task where id=:ct.id];
        tt.Status='Closed';
        update tt;
        
        case updatecase=[select id,status from case where id=:c.id];
        updatecase.Status = 'closed';
         DmlException expectedException;
         Test.startTest();
            try
            {
                 update updatecase;
            }
            catch (DmlException d)
            {
                expectedException = d;
            }
        Test.stopTest();
        system.assertEquals(null, expectedException, 'The record should be saved');
       } 
   
   
    public static void LoadData()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Case';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        String RecTypeId= [select Id from RecordType where (Name='Volkswagen Individual Customer') and (SobjectType='Account')].Id;
         
        objCase = VGA_CommonTracker.createCase();  
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        
        objAsset = VGA_CommonTracker.createAsset('Test Asset');
        
        objProduct = VGA_CommonTracker.createProduct('Test Product');
        objProduct.VGA_Brand__c = 'PV Volkswagen';
        insert objProduct;
        
        
        
        objPersonAccount = new Account(
          RecordTypeID=RecTypeId,
          FirstName='Test FName',
          LastName='Test LName',
          PersonMailingStreet='test@yahoo.com',
          PersonMailingPostalCode='12345',
          PersonMailingCity='SFO',
          PersonEmail='rishi.patel@saasfocus.com',
          PersonHomePhone='1234567',
          PersonMobilePhone='12345678' 
        );
 
        insert objPersonAccount;
        
        
    }
    
    @istest
    public static void test1()
    {
        LoadData();
        VGA_Service_Campaigns__c s = new VGA_Service_Campaigns__c();
        s.VGA_SC_Status__c = 'Future';
        s.VGA_SC_Code__c = '69Q7';
        s.Name__c = 'Takata';
        s.VGA_SC_Vin__c = '12345';
        s.VGA_SC_Sub_Brand__c = 'PV';
        s.VGA_SC_Brand__c = 'Volkswagen';
        s.VGA_SC_Description__c = 'Test';
        s.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
        
      //  Insert s;
        
        ID recallRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Recall Refused').getRecordTypeId();
        Case c = new Case();        
        c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = recallRTID;
        c.Origin = 'Dealer CRM VW';
        c.DocuSign_Recall_First_Name__c = 'Tester';   
        c.DocuSign_Recall_Last_Name__c = 'Tester';
        c.VGA_DocuSign_Recall_Email__c = 'a@a.com';
        c.VGA_DocuSign_Recall_Contact_No__c = 'dddd';
        
        Insert c;
        VGA_CaseTriggerHandler.takataRecallAccountSearch(c.id, 'test', 'test', null, null, null, null, null, null, null, null);
        c.DocuSign_Recall_First_Name__c = 'Testing';
        c.DocuSign_Recall_Last_Name__c = 'Testing';
        Update c;
        
    s = new VGA_Service_Campaigns__c();
        s.VGA_SC_Status__c = 'Future';
        s.VGA_SC_Code__c = '69Q7';
        s.Name__c = 'Takata';
        s.VGA_SC_Vin__c = '12345fdf';
        s.VGA_SC_Sub_Brand__c = 'PV';
        s.VGA_SC_Brand__c = 'Volkswagen';
        s.VGA_SC_Description__c = 'Test';
        s.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
        
        Insert s;        
        
    c = new Case();        
        c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = recallRTID;
        c.Origin = 'Dealer CRM VW';  
        c.DocuSign_Recall_Last_Name__c = 'Tester';    
        
        Insert c;    
        c.DocuSign_Recall_Last_Name__c = 'Tested'; 
        Update c;
        
    s = new VGA_Service_Campaigns__c();
        s.VGA_SC_Status__c = 'Future';
        s.VGA_SC_Code__c = '69Q7';
        s.Name__c = 'Takata';
        s.VGA_SC_Vin__c = '12345weeee';
        s.VGA_SC_Sub_Brand__c = 'PV';
        s.VGA_SC_Brand__c = 'Volkswagen';
        s.VGA_SC_Description__c = 'Test';
        s.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
        
        Insert s;        
        
    c = new Case();        
        c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = recallRTID;
        c.Origin = 'Dealer CRM VW';  
        c.VGA_DocuSign_Recall_Email__c = 'a@a.com';
        c.DocuSign_Recall_Last_Name__c = 'Tested';
        Insert c;    
        c.VGA_DocuSign_Recall_Email__c = 'av@a.com';
        c.DocuSign_Recall_Last_Name__c = 'Tested';
        Update c;
        
        
    s = new VGA_Service_Campaigns__c();
        s.VGA_SC_Status__c = 'Future';
        s.VGA_SC_Code__c = '69Q7';
        s.Name__c = 'Takata';
        s.VGA_SC_Vin__c = '12345weeee';
        s.VGA_SC_Sub_Brand__c = 'PV';
        s.VGA_SC_Brand__c = 'Volkswagen';
        s.VGA_SC_Description__c = 'Test';
        s.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
        
        Insert s;        
        
    c = new Case();        
        c.VW_Service_Campaign_VIN__c = s.id;
        c.RecordTypeId = recallRTID;
        c.Origin = 'Dealer CRM VW';  
        c.DocuSign_Recall_Last_Name__c = 'Tested';
        c.VGA_DocuSign_Recall_Contact_No__c = 'dfsdf';
        Insert c;    
        c.DocuSign_Recall_Last_Name__c = 'Tested';
        c.VGA_DocuSign_Recall_Contact_No__c = 'erwrwer';
        Update c;       
        
        try {
            c = new Case();        
            c.VW_Service_Campaign_VIN__c = s.id;
            c.RecordTypeId = recallRTID;
            c.Origin = 'Dealer CRM VW';  
            c.DocuSign_Recall_Last_Name__c = 'Tested';
            c.VGA_DocuSign_Recall_Contact_No__c = 'dfsdf';
            Insert c;    
            c.DocuSign_Recall_Last_Name__c = 'Tested';
            c.VGA_DocuSign_Recall_Contact_No__c = 'erwrwer';
            Update c;  
        } catch (Exception e)
        {
            Boolean expectedExceptionThrown =  (e.getMessage().contains('Following one or more service campaign cases exist for this VIN:')) ? true : false;
            
            System.AssertEquals(expectedExceptionThrown, true);            
        }
                
        
    }
}