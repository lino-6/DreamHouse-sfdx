global class VGA_RecallSearchBatch implements Database.Batchable<sObject> ,Database.stateful
{
    
    
    global List<VGA_Service_Campaigns__c> campList = new List<VGA_Service_Campaigns__c>();
    global string query;
    
    
    
    global VGA_RecallSearchBatch(string query )
    {
        this.query = query;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       // return DataBase.Query(query);
       return Database.getQueryLocator(query); 
        //return MainAcounts;
    }
    
    global void execute(Database.BatchableContext BC,List<VGA_Service_Campaigns__c> scope)
    {
        
      
        
         for(VGA_Service_Campaigns__c c: scope)
        {
            c.VGA_SC_Status__c='Scheduled';
            system.debug(c);
        }
        update scope;
    }
    
    
    
    
    
    
    
    global void finish(Database.BatchableContext bc)
    {
        
    }
}