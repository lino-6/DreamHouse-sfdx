@isTest
public class VGA_ResetDealerContactPasswordTracker 
{
    
    public static Contact c;
    public static User u;
    
    public static void loadData()
    {
        Recordtype rec = [SELECT Id FROM RecordType where NAME = 'Volkswagen Dealer Account'];
            
        Account portalAccount = new Account(name = 'portalAccount', recordtypeid = rec.id);//create a portal account first
        insert portalAccount;
        
        c = new contact(LastName = 'portalContact', AccountId = portalAccount.Id); //create a portal contact
        insert c;
        
        Profile portalProfileId = [SELECT Id FROM Profile WHERE Name='Volkswagen Dealer Consultant Profile' LIMIT 1];        
        
        u = new User( email='test@test.com',
                    profileid = portalProfileId.Id, 
                    UserName='testjl-ehsfjkjsfdjqad@test-lsfjlakfsdkjasfd.com',
                    Alias = 'GDS',
                    TimeZoneSidKey='America/New_York',
                    EmailEncodingKey='ISO-8859-1',
                    LocaleSidKey='en_US', 
                    LanguageLocaleKey='en_US',
                    ContactId = c.Id,
                    PortalRole = 'Manager',
                    FirstName = 'test',
                    LastName = 'test');
        
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            insert u;
        }        
        
    }

    @isTest
    public static void test1()
    {
        
        loadData();
        Boolean result = VGA_ResetDealerContactPassword.resetPassword(c.id);
        system.assertEquals(true, result, 'Password reset failed!.');

    }
    
    @isTest
    public static void test2()
    {
        loadData();
        Contact con = VGA_ResetDealerContactPassword.getcontactDetail(c.id);
        system.assertEquals(c.id, con.id, 'Contact id not matching!.');
    }
    
    @isTest(SeeAllData=true)
    public static void test3()
    {
        loadData();
        VGA_ResetDealerContactPassword.resetDealerContactPassword(c.id);
    }    
    
}