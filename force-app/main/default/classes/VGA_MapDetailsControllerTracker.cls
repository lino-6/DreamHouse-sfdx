//Tracker class for VGA_MapDetailsController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Dec-2017 
//===============================================================================================
//Coverage for VGA_MapDetailsController on              8-Dec-2017       100%
//===============================================================================================
@isTest(seeAllData = false)    
public class VGA_MapDetailsControllerTracker 
{
    public static testMethod void Test1()
    {
       Lead objLead       =VGA_CommonTracker.createLead();
       ApexPages.currentpage().getparameters().put('Ids',objLead.Id); 
       ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objLead);
       VGA_MapDetailsController objMap =new VGA_MapDetailsController(sc);
    }
}