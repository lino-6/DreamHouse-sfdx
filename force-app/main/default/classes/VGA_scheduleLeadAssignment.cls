global class VGA_scheduleLeadAssignment implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        VGA_LeadAssignmentBatch b = new VGA_LeadAssignmentBatch(); 
        database.executebatch(b,200);
    }
}