@isTest
public with sharing class VGA_CampaignActivityControllerTracker
{
	public static VGA_Campaign_Activity_Header__c objCampaignActivityHeader;
	public static VGA_Campaign_Activity_Detail__c objCampaignActivityDetail;
	public static product2 objProduct;
	public static VGA_Active_Brochure__c objActiveBrochure;
    
	static testmethod void unittest1()
	{
        LoadData();
        
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        //objCampaignActivityHeader.VGA_Week_Start_Date__c = VGA_CampaignActivityController.getweekstartdate();
        //objCampaignActivityHeader.VGA_Week_Ending_Date__c = VGA_CampaignActivityController.getweekEnddate();
        insert objCampaignActivityHeader;
        
        objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        insert objCampaignActivityDetail;
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Skoda','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		
       
    }
    static testmethod void unittest1_1()
	{
        LoadData();
        
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        //objCampaignActivityHeader.VGA_Week_Start_Date__c = VGA_CampaignActivityController.getweekstartdate();
        //objCampaignActivityHeader.VGA_Week_Ending_Date__c = VGA_CampaignActivityController.getweekEnddate();
        insert objCampaignActivityHeader;
        
        
        objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        insert objCampaignActivityDetail;
        
        /*objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.VGA_Brand__c = 'Skoda';
		insert objProduct;*/
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Skoda','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		
        
    }
    static testmethod void unittest1_2()
	{
        LoadData();
        
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        //objCampaignActivityHeader.VGA_Week_Start_Date__c = VGA_CampaignActivityController.getweekstartdate();
        //objCampaignActivityHeader.VGA_Week_Ending_Date__c = VGA_CampaignActivityController.getweekEnddate();
        insert objCampaignActivityHeader;
        
        
        //objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        //insert objCampaignActivityDetail;
        
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.VGA_Brand__c = 'Skoda';
		insert objProduct;
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Skoda','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		

    }
    static testmethod void unittest1_2_1()
	{
        LoadData();
        
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        //objCampaignActivityHeader.VGA_Week_Start_Date__c = VGA_CampaignActivityController.getweekstartdate();
        //objCampaignActivityHeader.VGA_Week_Ending_Date__c = VGA_CampaignActivityController.getweekEnddate();
        insert objCampaignActivityHeader;
        
        
        objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        insert objCampaignActivityDetail;
        
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.VGA_Brand__c = 'Skoda';
		insert objProduct;
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Skoda','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		
         VGA_Campaign_Activity_Detail__c objCampaignActivityDetail = VGA_CampaignActivityController.createCampaign(12.0,21.0,12.0,'test','test',objCampaignActivityHeader.Id,objProduct.Id,objCampaignActivityDetail.id,'Skoda');
        VGA_Campaign_Activity_Detail__c objCampaignActivityDetail1 = VGA_CampaignActivityController.createCampaign(12.0,21.0,12.0,'test','test',objCampaignActivityHeader.Id,objProduct.Id,objCampaignActivityDetail.id,'Volkswagen');
    }
    static testmethod void unittest2()
	{
		LoadData();
		date objstartDate = VGA_CampaignActivityController.getWeekStartDate();
		date objEndDate = VGA_CampaignActivityController.getWeekEndDate();
		
		objCampaignActivityHeader.VGA_Brand__c = 'Volkswagen';
		objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
		
		insert objCampaignActivityHeader;
		
		objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
		insert objCampaignActivityDetail; 
		
		list<VGA_WrapperofProduct> lstProductDetails =VGA_CampaignActivityController.getProductDetails('Volkswagen','Passenger Vehicles (PV)',string.valueof(objstartDate),string.valueof(objEndDate));
		
		string str123 = JSON.serialize(objCampaignActivityDetail);
		system.debug('@@str123'+str123);
		string str = VGA_CampaignActivityController.saveCampaignInformation('['+str123+']','Volkswagen','Passenger Vehicles (PV)');
	}
    static testmethod void unittest2_1()
	{
		LoadData();
		date objstartDate = VGA_CampaignActivityController.getWeekStartDate();
		date objEndDate = VGA_CampaignActivityController.getWeekEndDate();
		
		objCampaignActivityHeader.VGA_Brand__c = 'Volkswagen';
		objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
		
		insert objCampaignActivityHeader;
		
		//objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
		//insert objCampaignActivityDetail; 
		
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        objActiveBrochure.VGA_Dealer_Traffic_Active__c=true;
        insert objActiveBrochure;
        
		list<VGA_WrapperofProduct> lstProductDetails =VGA_CampaignActivityController.getProductDetails('Volkswagen','Passenger Vehicles (PV)',string.valueof(objstartDate),string.valueof(objEndDate));
		
		string str123 = JSON.serialize(objCampaignActivityDetail);
		system.debug('@@str123'+str123);
		//string str = VGA_CampaignActivityController.saveCampaignInformation('['+str123+']','Volkswagen','Passenger Vehicles (PV)');
	}
    static testmethod void unittest3()
	{
        LoadData();
        /*
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
       
        insert objCampaignActivityHeader;
        
        objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        insert objCampaignActivityDetail;
        */
        
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.VGA_Brand__c = 'Skoda';
		insert objProduct;
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Skoda','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		
        
    }
    static testmethod void unittest3_1()
	{
        LoadData();
        /*
        objCampaignActivityHeader.VGA_Brand__c = 'Skoda';
        objCampaignActivityHeader.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
       
        insert objCampaignActivityHeader;
        
        objCampaignActivityDetail = VGA_CommonTracker.createCampaignActivityDetail(objCampaignActivityHeader.id);
        insert objCampaignActivityDetail;
        */
        
        /*objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.VGA_Brand__c = 'Skoda';
		insert objProduct;
        */
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        objActiveBrochure.VGA_Dealer_Traffic_Active__c=true;
        insert objActiveBrochure;
        
		date startDate = VGA_CampaignActivityController.getweekstartdate();
		date endDate = 	 VGA_CampaignActivityController.getweekEnddate();
		
		list<VGA_WrapperofProduct> lstWrapper = VGA_CampaignActivityController.getproductDetails('Volkswagen','Passenger Vehicles (PV)',string.valueof(startDate),string.valueof(endDate));
		
        
    }
	public static void LoadData()
	{
		objCampaignActivityHeader = VGA_CommonTracker.createCampaignActivityHeader();
        
        objProduct =VGA_CommonTracker.createProduct('strProductName'); 
        
        objActiveBrochure = VGA_CommonTracker.createActiveBrochure();
	}    
}