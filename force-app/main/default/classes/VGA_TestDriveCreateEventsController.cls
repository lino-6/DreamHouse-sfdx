public without sharing class VGA_TestDriveCreateEventsController 
{
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that create a new event in Salesforce from the dealer CLMS calendar.
    * @param newEvent Event details coming from the lightning component that we will be trying to insert.
    * @param eventType Type of the event that we will be inserting: 'Test Drive Appointment', 'Unavailable'
    * @param selectedModel Model of Interest that we will be booking for the Test Drive
    * @param marketingOptIn Marketing Opt In selection
    * @param eventOwnerId Id of owner of the event
    * @return string Either Success or error message
    */
    @AuraEnabled
    public static string createEvents(Event newEvent, String eventType, 
                                      String selectedModel, Boolean marketingOptIn, 
                                      String eventOwnerId)
    {
        Id ownerId = getEventOwnerId(eventOwnerId);
        
        //If we are not able to find any owner
        if(ownerId == null){
            return 'Error: Event Owner has not been found.';
        }
        
        try
        {
            Id devRecordTypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get(eventType).getRecordTypeId();            
            newEvent.RecordTypeId  = devRecordTypeId;
            newEvent.StartDateTime = newEvent.StartDateTime;
            newEvent.EndDatetime   = getEventDatetime(newEvent, eventType);            
            
            //Check if there is already an event on the selected time slot
            if(validateEventDate(newEvent, ownerId))
            {
                return 'Error: Appointment already exists on the selected time.';
            }
            newEvent.Subject       = getEventSubject(newEvent, eventType);
            newEvent.WhoId         = createTestDriveLead(newEvent, eventType, selectedModel,marketingOptIn);
            newEvent.OwnerId       = ownerId; 
            
            insert newEvent;
        }
        catch(exception e)
        {
            return 'Error: ' + e.getMessage(); 
        }
        
        return 'Event created';
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that returns the event owner. If nothing is coming on the parameters
    *              then the owner will be the current user
    * @param eventOwnerId Id of owner of the event
    * @return Id Of the user that will be the owner of the event
    */
    private static Id getEventOwnerId(String eventOwnerId){
        
        if(eventOwnerId  != Null)
        {
            List<User> userList = [Select ID from User where ContactID =:  eventOwnerId];
            if(userList.size()==0){
                return null;
            }
            return userList[0].Id; 
        }
        
        return UserInfo.getUserId();
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that validates if there is already an event on the selected time slot.
    * @param ownerId Id of owner of the event
    * @param newEvent Event details coming from the lightning component that we will be trying to insert.
    * @return Boolean True if there is already an event on the same time slot and false if 
    *                 there is not.
    */
    private static Boolean validateEventDate(Event newEvent, Id ownerId){
        
        Datetime startDt   = newEvent.StartDateTime;
        Datetime endSlotDt = newEvent.EndDateTime;
        
        for(Event evt : [SELECT Id, OwnerId, StartDateTime, EndDatetime
                         FROM Event
                         WHERE OwnerId =: ownerId]){
            
            //Make sure there is no event in Salesforce with that overlaps with the one
            //that we are trying to create
            if((startDt >= evt.StartDateTime && endSlotDt <= evt.EndDateTime)
            || (startDt <= evt.StartDateTime && endSlotDt >= evt.EndDateTime)
            || (startDt > evt.StartDateTime && startDt < evt.EndDateTime)
            || (endSlotDt > evt.StartDateTime && endSlotDt < evt.EndDateTime)){
                return true; 
            }            
        }
        
        return false;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that gets the event subject based on the event type value.
    * @param eventType Type of the event that we will be inserting: 'Test Drive Appointment', 'Unavailable'
    * @param newEvent Event details coming from the lightning component that we will be trying to insert.
    * @return String with the event's subject
    */   
    private static String getEventSubject(Event newEvent, String eventType){
        
        if(eventType == 'Test Drive Appointments')
        {
            return 'Test Drive Appointment - ' + newEvent.VGA_LeadFirstName_TestDrive__c + ' ' + newEvent.VGA_LeadLastName_TestDrive__c;
        }
        
        return newEvent.Subject;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that gets the event end date time based on the event type
    * @param eventType Type of the event that we will be inserting: 'Test Drive Appointment', 'Unavailable'
    * @param newEvent Event details coming from the lightning component that we will be trying to insert.
    * @return Datetime when the event will be ending.
    */
    private static Datetime getEventDatetime(Event newEvent, String eventType){
        
        if(eventType == 'Test Drive Appointments'){
            return newEvent.StartDatetime.addHours(1);
        }
        
        return newEvent.EndDatetime;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that create a new test drive lead with the data coming from the form.
    *              This lead will be related to the test drive event.
    * @param newEvent Event details coming from the lightning component that we will be trying to insert.
    * @param eventType Type of the event that we will be inserting: 'Test Drive Appointment', 'Unavailable'
    * @param selectedModel Model of Interest that we will be booking for the Test Drive
    * @param marketingOptIn Marketing Opt In selection
    * @return Id of the newly create lead
    */
    public static Id createTestDriveLead(Event newEvent, String eventType, 
                                         String selectedModel, Boolean marketingOptIn)
    {
        //Current user Id that will be the lead owner
        Id currentUserId = UserInfo.getUserId();
        VGA_Active_Brochure__c selectedActiveBrochure = getActiveBrochureRecord(selectedModel);
        
        if(selectedActiveBrochure != null && eventType == 'Test Drive Appointments')
        {
            User usr = getCurrentUserDetails(currentUserId);
            Lead newLead = new Lead();
            newLead.FirstName               = newEvent.VGA_LeadFirstName_TestDrive__c;
            newLead.LastName                = newEvent.VGA_LeadLastName_TestDrive__c;
            newLead.VGA_Sub_Brand__c        = selectedActiveBrochure.VGA_Sub_Brand__c;
            newLead.MobilePhone             = newEvent.VGA_Phone_TestDrive__c;
            newLead.Email					= newEvent.VGA_Email_TestDrive__c;
            newLead.VGA_Model_of_Interest__c= selectedActiveBrochure.Name;
            newLead.VGA_Request_Type__c     = 'Test Drive';
            newLead.LeadSource              = 'Dealer CLMS';
            newLead.VGA_Brand__c            = 'Volkswagen';
            newLead.VGA_Dealer_Code__c      = usr.Account.VGA_Dealer_Code__c;
            newLead.PostalCode              = usr.Account.VGA_Dealer_Post_Code__c;
            newLead.OwnerId                 = currentUserId;
            newLead.VGA_Dealer_Account__c  	= usr.AccountId;
            newLead.Status                  = 'Accepted';
            newLead.VGA_Status__c           = 'Accepted';
            newLead.VGA_Assigned_to_Dealership__c = System.now();
            newLead.VGA_Test_Drive_Start_Date__c  = newEvent.StartDateTime;
            newLead.VGA_Marketing_opt_in__c = newLead.VGA_Marketing_opt_in__c == true ? marketingOptIn : newLead.VGA_Marketing_opt_in__c;
            newLead.VGA_Primary_id__c=generatePrimaryIdonLead();
            insert newLead;
            
            getMatchScore( newLead.Id ,newLead.FirstName, newLead.LastName, newLead.Email , newLead.Phone, False , 'Volkswagen', '', '',marketingOptIn,newLead.VGA_Sub_Brand__c); 
            
            return newLead.Id;
        }
        
        return null;
    }
   
    /**
    * @author Archana Yerramilli
    * @date 08/04/2018
    * @description Method checks match score logic, a future call to Mule API.If account exists, it will populate CustomerAccount with existing account.
    * @description If Account does not exist, a new account is created. 
    * @param Lead details like LeadID, firstname, lastname, email, phone, isOwner, brand , state, postcode, subbrand, marketingOptIn
    */
    @future (callout = true)
    public static void getMatchScore(String ID, String fname, String lname, String email, String Phone, Boolean isOwner, String Brand, String State, String PostCode, Boolean marketingOptIn, String subBrand)
    {
        Account customerAccount ;
        customerAccount = VGA_CheckMatchScore.personAccountSearch(fname,lname,email,Phone,isOwner,Brand,State,PostCode);         
        if(customerAccount !=null )
        {
            List<Lead> leadToUpdate = [Select ID,VGA_Customer_Account__c,Name FROM Lead where Id =: ID LIMIT 1];
       		leadToUpdate[0].VGA_Customer_Account__c = customerAccount.ID;        
          	update leadToUpdate;  
        }
        else // Create New Account if Acc does not exist
        {
            Account objAccount = new Account();
            objAccount.firstname = fname;
            objAccount.lastname = lname;
            objAccount.PersonEmail = email;
            objAccount.PersonmobilePhone = Phone;
            objAccount.personmailingPostalCode = PostCode;    
            objAccount.VGA_Brand__c = 'Volkswagen';
            objAccount.recordtypeID = VGA_GlobalRecordType.Global_Account_VWRTId;
            objAccount.VGA_Sub_Brand__c = subBrand;
            if(marketingOptIn == true)
            {
                objAccount.VGA_News_Innovations_Flag__c = true;
                objAccount.VGA_Vehicle_Launches_Flag__c = true;
                objAccount.VGA_Welcome_experience_On_boarding__c = true;
                objAccount.VGA_Events_opt_out__c = true;
                objAccount.VGA_Promotions_Flag__c = true;
                objAccount.VGA_Sponsorship_out__c = true;
            }
            INSERT objAccount;
            List<Account> acctoUpdate = [Select ID, Name FROM Account where Id =: objAccount.Id LIMIT 1];
            if(acctoUpdate !=null)
            {	
                    List<Lead> leadToUpdate = [Select ID,VGA_Customer_Account__c,Name FROM Lead where Id =: ID LIMIT 1];
                    leadToUpdate[0].VGA_Customer_Account__c = objAccount.Id;        
          	        update leadToUpdate;                  
            }
        }         
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that given a model Id it returns the related active brochure record.
    * @param selectedModel Model Id
    * @return VGA_Active_Brochure__c Active brochure record related to the given model Id
    */
    private static VGA_Active_Brochure__c getActiveBrochureRecord(String selectedModel){
        if(selectedModel != null){
            List<VGA_Active_Brochure__c> activeBrochureRecord = [SELECT Id, Name, VGA_Brand__c, VGA_Sub_Brand__c, 
                                                                 VGA_Test_Drive_Active__c
                                                                 FROM VGA_Active_Brochure__c
                                                                 WHERE VGA_Test_Drive_Active__c = true
                                                                 AND Id =: selectedModel];
            if(activeBrochureRecord.size()!=0){
                return activeBrochureRecord[0];
            } 
        }
        return null;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that return the logged in user record from where we will be getting
    *              the related account details
    * @param currentUserId Id of the logged in user
    * @return User current user record
    */
    private static User getCurrentUserDetails(Id currentUserId)
    {
        List<User> currentUser = [SELECT Id, AccountId,Account.VGA_Dealer_Code__c, Account.VGA_Dealer_Post_Code__c
                                  FROM User
                                  WHERE AccountId != null
                                  AND Id =: currentUserId];
        
        if(currentUser.size()!=0){
            return currentUser[0];
        }
        
        return null;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that return the list of active brochures wich subbrand is available
    *              for the logged in dealer contact.
    * @return List<VGA_Active_Brochure__c> List of active brochures that will be available while 
    *         creating a new Lead
    */
    @AuraEnabled
    public static List<VGA_Active_Brochure__c> getActiveBrochures()
    {
        List<VGA_Active_Brochure__c> activeBrochureNamesList = new List<VGA_Active_Brochure__c>();
        String contactSubBrands = getContactSubBrands();
        
        for(VGA_Active_Brochure__c activeBrochure : [SELECT Id, Name, VGA_Brand__c, VGA_Sub_Brand__c, VGA_Test_Drive_Active__c
                                                     FROM VGA_Active_Brochure__c
                                                     WHERE VGA_Test_Drive_Active__c = true
                                                     AND VGA_Brand__c = 'Volkswagen'
                                                     ORDER BY Name]){
            if(contactSubBrands.contains(activeBrochure.VGA_Sub_Brand__c)){
                activeBrochureNamesList.add(activeBrochure);
            }   
        }        
        return activeBrochureNamesList;
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that returns the Sub-brands available for the logged in contact
    * @return String Name of the sub-brands for which the logged in contact has access.
    */
    private static String getContactSubBrands()
    {
        Id currentUserId = UserInfo.getUserId();        
        List<User> currentUser = [SELECT Id, Contact.VGA_Sub_Brand__c
                                  FROM User
                                  WHERE ContactId != null
                                  AND Id =: currentUserId];        
        if(currentUser.size()!=0){
            return currentUser[0].Contact.VGA_Sub_Brand__c;
        }
        
        return null;
    } 
    
    /**
    * @author Lino Diaz Alonso
    * @date 08/04/2018
    * @description Method that returns the list of pilot contacts.
    * @return List<Contact> List of contacts
    */
    @AuraEnabled
    public static List<Contact> getPilotContacts()
    {
        User LoggedInUser=[select Id,Name,ContactId,Contact.Name  from User where Id=:UserInfo.getUserId()];
        Contact defaultContact=new Contact(Id=LoggedInUser.ContactId);
        String AccountIds = VGA_Common.getloggingAccountId();
        Map<Id,Contact> ContactMap=new Map<Id,Contact>([SELECT Id,Name FROM Contact WHERE AccountId =: AccountIds]);
        list<VGA_User_Profile__c> lstPilotUsers = new list<VGA_User_Profile__c>([SELECT Id,Name,VGA_Contact__c FROM VGA_User_Profile__c WHERE VGA_Available_TestDrive__c = true AND VGA_Contact__c=: ContactMap.keySet()]);
        Set<Id> AvailableContactIdSet=new Set<Id>();
        for(VGA_User_Profile__c temp: lstPilotUsers){
            if(temp.VGA_Contact__c!=null){
                AvailableContactIdSet.add(temp.VGA_Contact__c);
            }
        }
        Set<Contact> uniquePilotContacts = new Set<Contact>();
        if(defaultContact!=null){
            if(AvailableContactIdSet.contains(defaultContact.Id)){
               uniquePilotContacts.add((ContactMap.containsKey(defaultContact.Id)?ContactMap.get(defaultContact.Id):defaultContact)); 
               AvailableContactIdSet.remove(defaultContact.Id);
            }
        }
        for(Contact con:ContactMap.values()){
            if(AvailableContactIdSet.contains(con.Id)){
                uniquePilotContacts.add(con);
            }
        }
        List<Contact> uniqueContacts = new list<Contact>();
        uniqueContacts.addAll(uniquePilotContacts);
        return uniqueContacts;
        
    }
    
    public static String generatePrimaryIdonLead(){
        try{
            Blob b = Crypto.GenerateAESKey(128);
            String h = EncodingUtil.ConvertTohex(b);
            String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
            return guid;
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            return null;
        }
    }
}