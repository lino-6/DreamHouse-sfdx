/**
* @author Archana Yerramilli
* @date  18/03/2019
* @description This is a common class used to check mule match score logic and returns a boolean TRUE or FALSE
* @param Case List
*/


public class VGA_CheckMatchScore {
    
    public static Account personAccountSearch(String firstName, String lastName, String email, String mobile, Boolean isOwner, String brand, string state, string postCode)
    {
        List<Account> accountList = new List<Account>(); 
        Muleapi api = new Muleapi();
        api.first_name =firstName;
        api.last_name =lastName;
        api.mobile_phone = mobile;
        api.email_address = email;
        api.region_code =state ;
        api.post_code = postCode;
        api.brand = brand;        
        String pid;
        Account account= null;
        pid= api.personAccountSearch();
        if(pid!=null && pid != '')
        {
                account = [select id,VGA_Owner__c from account where id=:pid];
        }
        return account ; 
        
    }
    
}