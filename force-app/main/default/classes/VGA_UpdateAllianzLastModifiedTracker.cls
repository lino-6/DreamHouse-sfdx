/**
* @author Archana Yerramilli
* @date 22/10/2018
* @description This is a test class for Queueable Job VGA_updateAllianzLastModified
* @param List<VGA_Allianz_Data_Mapping__c> is passed to the job 
*/


@isTest
public class VGA_UpdateAllianzLastModifiedTracker {
 @isTest static void TestQueuableJob()
     {
         List<VGA_Allianz_Data_Mapping__c> lstAllianz = VGA_CommonTracker.buildAllianzMapping();
         VGA_updateAllianzLastModified updateAllianz = new VGA_updateAllianzLastModified(lstAllianz);
         Test.startTest();
            System.enqueueJob(updateAllianz);
         Test.stopTest();        
      }
}