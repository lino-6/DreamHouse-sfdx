/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_FileTriggerHandlerTracker 
{
	public static Lead objLead;
	Public static Task objTask;
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        LoadData();
        
        test.startTest();
        
         string before = 'Testing base 64 encode';            
         Blob beforeblob = Blob.valueOf(before);
         //Insert contentdocument data
         ContentVersion cv = new ContentVersion();
         cv.title = 'test content trigger';      
         cv.PathOnClient ='test';           
         cv.VersionData =beforeblob;          
         insert cv;         
                                                
         ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];

        ContentDocumentLink newFileShare = new ContentDocumentLink();
        newFileShare.contentdocumentid = testcontent.contentdocumentid;
        newFileShare.LinkedEntityId = objTask.id;
        newFileShare.ShareType= 'V';
        insert newFileShare;
        
        test.stopTest();
        
    }
    public static void LoadData()
    {
    	VGA_Triggers__c obj = new VGA_Triggers__c();
        obj.Name = 'ContentDocumentLink';
        obj.VGA_Is_Active__c = true;
        insert obj;
        
        objLead = new Lead();
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Model_of_Interest__c = 'abc';
        objLead.FirstName  = 'TestLeadFirst';
        objLead.LastName = 'TestLeadLastName';
        objLead.Company = 'TestCompany';
        objLead.Status = 'New';
        objLead.VGA_Campaign_Source__c = 'Web Site';
        objLead.postalcode = '1234';
        
        insert objLead;  
        
         objTask = new Task();
         objTask.whoid=objLead.id;
         objTask.status = 'open';
         objTask.priority = 'High';
         
         insert objTask;
         
         system.debug('@@@objTask'+objTask);
    }
}