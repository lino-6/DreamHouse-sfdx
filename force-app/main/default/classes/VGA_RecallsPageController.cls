public Without Sharing class VGA_RecallsPageController {

    
    @AuraEnabled
    public static string updateCase(Id case_id)
    {
        
        List<Case> lst_case = [SELECT Id, VW_Service_Campaign_VIN__c, Skoda_Service_Campaign_VIN__c, RecordType.Name FROM Case WHERE Id = :case_id];
        
        if(lst_case.isEmpty())
        {
            return 'ERROR: Invalid Case.';
        }
        
        Case obj_case = lst_case.get(0);
        
        if(obj_case.RecordType.Name != 'Recall Refused')
        {
        	return 'ERROR: DocuSign signing is only allowed for Takata Airbag Recall.';
        }
        
        Id service_campaign_obj_id = obj_case.VW_Service_Campaign_VIN__c != null ? obj_case.VW_Service_Campaign_VIN__c : obj_case.Skoda_Service_Campaign_VIN__c;
        
        if(service_campaign_obj_id == null) 
        {
            return 'ERROR: Cannot find service campaign record.';
        }
        
        List<VGA_Service_Campaigns__c> lst_service_campaign = [SELECT Campaign_Code__c, VGA_SC_Vehicle__c, VGA_SC_Status__c, VGA_SC_Vehicle__r.VGA_VIN__c
                                                               , VGA_SC_Vehicle__r.Product2.VGA_Brand__c
                                                               FROM VGA_Service_Campaigns__c WHERE Id = :service_campaign_obj_id];
        
        if(lst_service_campaign.isEmpty())
        {
            return 'ERROR: Cannot find service campaign record.';
        }    
        
        VGA_Service_Campaigns__c obj_service_campaign = lst_service_campaign.get(0);
        
        if(obj_service_campaign.VGA_SC_Status__c == 'Refused')
        {
            return 'ERROR: Service campaign of this VIN is already in Refused status.';
        }        
        
        List<VGA_Ownership__c> lst_ownership = [SELECT Id, Is_owner_a_dealer__c, VGA_Owner_Name__r.FirstName, VGA_Owner_Name__r.LastName, VGA_Owner_Name__r.PersonMobilePhone,
                                                VGA_Owner_Name__r.PersonEmail, VGA_Owner_Name__r.BillingStreet, VGA_Owner_Name__r.BillingCity,
                                                VGA_Owner_Name__r.BillingPostalCode, VGA_Owner_Name__r.BillingState,
                                                VGA_VIN__c FROM VGA_Ownership__c 
                                                WHERE VGA_Type__c = 'Current Owner' AND VGA_VIN_for_Mule__c = :obj_service_campaign.VGA_SC_Vehicle__r.VGA_VIN__c];        
        
        //Either no owner or multiple current owners
        if(lst_ownership.size() != 1)
        {
            return 'ERROR: Cannot find Vehicle owner.';
        }
        
        VGA_Ownership__c obj_ownership = lst_ownership.get(0);
        
        /*
        if(obj_ownership.Is_owner_a_dealer__c != null && obj_ownership.Is_owner_a_dealer__c == true)
        {
            return 'ERROR: Vehicle belongs to a fleet.';
        }
*/
           
        User obj_user = [SELECT id, Contact.Account.Name FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        
        obj_case.VGA_VIN__c = obj_service_campaign.VGA_SC_Vehicle__c;
        obj_case.VGA_DocuSign_Recall_VIN_No__c = obj_service_campaign.VGA_SC_Vehicle__r.VGA_VIN__c;
        obj_case.VGA_DocuSign_Recall_Campaign_No__c = obj_service_campaign.Campaign_Code__c;
        obj_case.VGA_DocuSign_Recall_Date_Of_Service__c = system.now().format('dd-MM-YYYY');
        obj_case.VGA_DocuSign_Recall_Dealership_Name__c = obj_user.Contact.Account.Name;
        obj_case.VGA_Brand__c = obj_service_campaign.VGA_SC_Vehicle__r.Product2.VGA_Brand__c == 'skoda' ? 'skoda' : 'volkswagen';
        
        
/*
        bj_case.DocuSign_Recall_First_Name__c = obj_ownership.VGA_Owner_Name__r.FirstName;
        obj_case.DocuSign_Recall_Last_Name__c = obj_ownership.VGA_Owner_Name__r.LastName;
        obj_case.VGA_DocuSign_Recall_Contact_No__c = obj_ownership.VGA_Owner_Name__r.PersonMobilePhone;
        obj_case.VGA_DocuSign_Recall_Email__c = obj_ownership.VGA_Owner_Name__r.PersonEmail;
        obj_case.VGA_DocuSign_Recall_Address__c = obj_ownership.VGA_Owner_Name__r.BillingStreet;
        obj_case.DocuSign_Recall_City__c = obj_ownership.VGA_Owner_Name__r.BillingCity;
        obj_case.DocuSign_Recall_Post_Code__c = obj_ownership.VGA_Owner_Name__r.BillingPostalCode;
        obj_case.DocuSign_Recall_State__c = obj_ownership.VGA_Owner_Name__r.BillingState;
*/
        
        //obj_case.AssetId = obj_ownership.VGA_VIN__c;
            
        update obj_case;
        
        //obj_service_campaign.VGA_SC_Status__c='Refused';        
        //update obj_service_campaign;
        
        List<String> lst_campaign_count = obj_service_campaign.Campaign_Code__c.split(';');
            
        return String.valueOf(lst_campaign_count.size());
    }    
    
    @AuraEnabled
    public static string sendDocuSignContract(Id case_id)
    {
        
        List<Case> lst_case = [SELECT RecordType.Name, VGA_VIN__r.Product2.VGA_Brand__c, VGA_VIN__r.VGA_VIN__c FROM Case WHERE Id = :case_id];
        
        if(lst_case.isEmpty())
        {
            return 'ERROR: Invalid Case.';
        }
        
        Case obj_case = lst_case.get(0);
        
        if(obj_case.RecordType.Name != 'Recall Refused')
        {
        	return 'ERROR: DocuSign signing is only allowed for Takata Airbag Recall.';
        }
            
        User obj_user = [SELECT id, contactid, Contact.Email, Contact.Firstname, Contact.Lastname, Contact.Account.Name FROM User WHERE id =: UserInfo.getUserId() LIMIT 1];
        
        String dealer_name = obj_user.Contact.Firstname != null ? (obj_user.Contact.Firstname + ' ') : '';
        dealer_name += obj_user.Contact.Lastname;
        
        String dealer_email = obj_user.Contact.Email;
        
        List<VGA_Ownership__c> lst_ownership = [SELECT Id, VGA_VIN__r.Account.Firstname, VGA_VIN__r.Account.Lastname FROM VGA_Ownership__c WHERE VGA_Type__c = 'Current Owner' AND VGA_VIN_for_Mule__c = :obj_case.VGA_VIN__r.VGA_VIN__c];        
        
        //Either no owner or multiple current owners
        if(lst_ownership.size() != 1)
        {
            return 'ERROR: Cannot find Vehicle owner.';
        } 
        
        VGA_Ownership__c obj_ownership = lst_ownership.get(0);
        
        String customer_name = obj_ownership.VGA_VIN__r.Account.Firstname != null ? (obj_ownership.VGA_VIN__r.Account.Firstname+' ') : '';
        customer_name += obj_ownership.VGA_VIN__r.Account.Lastname;        
        
        String brand = obj_case.VGA_VIN__r.Product2.VGA_Brand__c == 'skoda' ? 'skoda' : 'volkswagen';        
                       
        customer_name = 'Customer';

		string url = inPersonSign(case_id, dealer_name, dealer_email, customer_name, brand);   
        
        return url;
    }
    
    @AuraEnabled
    public static string getCaseRecordType(Id case_id)
    {
      if(case_id == null)
          return '';
        
       List<Case> case_lst = [SELECT RecordType.Name FROM Case WHERE Id = : case_id LIMIT 1];
       
       if(case_lst.isEmpty())
           return '';
        
        return case_lst.get(0).RecordType.Name;
    }
    
       
	//@future(callout=true)
	public Static string inPersonSign(Id objID, string dealer_name, String dealer_email, string customer_name, String brand)
	{ 
        
        Recall_DocuSign_Settings__c settings = Recall_DocuSign_Settings__c.getValues('Takata');
        if(settings == null)
        {
            return 'ERROR: Invalid DocuSign settings.';
        }
        
        String template_id = brand == 'skoda' ? settings.Skoda_Template_Id__c : settings.Volkswagen_Template_Id__c;
        String username = brand == 'skoda' ? settings.Skoda_Username__c : settings.User_Name__c;
        String password = brand == 'skoda' ? settings.Skoda_Password__c : settings.Password__c;
        String integrator_key = brand == 'skoda' ? settings.Skoda_Integrator_Key__c : settings.Integrator_Key__c;
        String account_id = brand == 'skoda' ? settings.Skoda_Account_Id__c : settings.Account_Id__c;
        String production_soap_url = settings.Production_SOAP_URL__c;
        String sandbox_soap_url = settings.Sandbox_SOAP_URL__c;
        
        if(string.isBlank(template_id) || string.isBlank(username) || string.isBlank(password) || string.isBlank(integrator_key)
          || string.isBlank(account_id) || string.isBlank(production_soap_url) || string.isBlank(sandbox_soap_url)) {
            return 'ERROR: Invalid DocuSign settings.';
        }
        
        String soap_url = settings.Enable_Production__c == true ? production_soap_url : sandbox_soap_url;

		
		String auth =   '<DocuSignCredentials>' + 
						'<Username>'+username+'</Username>' + 
						'<Password>'+password+'</Password>' +
						'<IntegratorKey>'+integrator_key+'</IntegratorKey>' + 
					'</DocuSignCredentials>';
	
	//init api request, you can see that we use properties from our custom metadata type to populate the webservice url
	DocuSignAPI.APIServiceSoap dsApiSend = new DocuSignAPI.APIServiceSoap();
	dsApiSend.endpoint_x = soap_url;            
	dsApiSend.inputHttpHeaders_x = new Map<String, String>();
	dsApiSend.inputHttpHeaders_x.put('X-DocuSign-Authentication', auth);
			
			
	DocuSignAPI.EnvelopeInformation envelope = new DocuSignAPI.EnvelopeInformation();
	envelope.Subject = 'Takata Airbag Safety Recall - Reporting Form';
	envelope.EmailBlurb = 'Please verify the details and sign the Takata Airbag Safety Recall - Reporting Form';
	envelope.AccountId  = account_id; 		
	
	DocuSignAPI.Recipient recipient_dealer = new DocuSignAPI.Recipient();
	recipient_dealer.ID = 1;
	recipient_dealer.Type_x = 'InPersonSigner';
	recipient_dealer.RoutingOrder = 1;
	recipient_dealer.Email = dealer_email;
	recipient_dealer.UserName = dealer_name;        
	recipient_dealer.RequireIDLookup = false;
	recipient_dealer.RoleName = 'Customer'; 
    recipient_dealer.SignerName = customer_name;


	// make recipient captive for embedded experience
	recipient_dealer.CaptiveInfo = new DocuSignAPI.RecipientCaptiveInfo();
	recipient_dealer.CaptiveInfo.ClientUserId = '1';
	
	// Role assignments
	DocuSignAPI.TemplateReferenceRoleAssignment roleAssignment_dealer = new DocuSignAPI.TemplateReferenceRoleAssignment();
	roleAssignment_dealer.RoleName = recipient_dealer.RoleName;
	roleAssignment_dealer.RecipientID = recipient_dealer.ID;            
		
        
        //////////////////////
        /*
        DocuSignAPI.Recipient recipient2 = new DocuSignAPI.Recipient();
        recipient2.ID = 2;
        recipient2.Type_x = 'CarbonCopy';
        recipient2.RoutingOrder = 2;
        recipient2.Email = dealer_email;
        recipient2.UserName = customer_name; 
        recipient2.RoleName = 'Customer';
        */
        ////////////////////////        
	
	DocuSignAPI.ArrayOfRecipient1 recipients = new DocuSignAPI.ArrayOfRecipient1();
	recipients.Recipient = new DocuSignAPI.Recipient[1];
	recipients.Recipient[0] = recipient_dealer; 
	//recipients.Recipient[1] = recipient2; 
        

	
	// Create object for the NDA server-side template
	DocuSignAPI.TemplateReference ndaTemplate = new DocuSignAPI.TemplateReference();
	ndaTemplate.Template = template_id;
	ndaTemplate.TemplateLocation = 'Server';	
	
	// Add role assignment
	ndaTemplate.RoleAssignments = new DocuSignAPI.ArrayOfTemplateReferenceRoleAssignment();
	ndaTemplate.RoleAssignments.RoleAssignment = new DocuSignAPI.TemplateReferenceRoleAssignment[1];
	ndaTemplate.RoleAssignments.RoleAssignment[0] = roleAssignment_dealer;	

	// create array of template references
	DocuSignAPI.ArrayOfTemplateReference templateReferences = new DocuSignAPI.ArrayOfTemplateReference();
	templateReferences.TemplateReference = new DocuSignAPI.TemplateReference[1];
	templateReferences.TemplateReference[0] = ndaTemplate;	
	
	envelope.CustomFields = new DocuSignAPI.ArrayOfCustomField();
	envelope.CustomFields.CustomField = new DocuSignAPI.CustomField[2];
	DocuSignAPI.CustomField myCustomField = new DocuSignAPI.CustomField();
	myCustomField.Name = 'DSFSSourceObjectId'; //this is what walter and mpaler talked about
	myCustomField.Value = objID; //for example, pass the id of current record via VF page, and write this line as myCustomField.Value = my_custom_Object.Id;
	myCustomField.Show = 'False';
	myCustomField.Required = 'False';
	myCustomField.CustomFieldType = 'Text';
	envelope.CustomFields.CustomField[0] = myCustomField;    
	
	
	DocuSignAPI.CustomField myCustomField1 = new DocuSignAPI.CustomField();
	myCustomField1.Name = '##SFCase'; //this is what walter and mpaler talked about
	myCustomField1.Value = objID; //for example, pass the id of current record via VF page, and write this line as myCustomField.Value = my_custom_Object.Id;
	myCustomField1.Show = 'False';
	myCustomField1.Required = 'False';
	myCustomField1.CustomFieldType = 'Text';
	envelope.CustomFields.CustomField[1] = myCustomField1;          
        
	
	String envelopeId;
	String token;
        
	try {
        if(Test.isRunningTest())
        {            
            envelopeId= 'test';
        }
        else 
        {
            DocuSignAPI.EnvelopeStatus es = dsApiSend.CreateEnvelopeFromTemplates(templateReferences, recipients, envelope, true);
            system.debug(es);
            envelopeId = es.EnvelopeID;            
        }
	} catch ( CalloutException e) {
		System.debug('Exception - ' + e );
        return 'ERROR: Webservice error.';
	}     

       
     
        
        DocuSignAPI.RequestRecipientTokenAuthenticationAssertion assert = new DocuSignAPI.RequestRecipientTokenAuthenticationAssertion();
        Blob blobKey = crypto.generateAesKey(128);
        assert.AssertionID = EncodingUtil.convertToHex(blobKey);
        assert.AuthenticationInstant = system.now();
        assert.AuthenticationMethod = 'Password';
        assert.SecurityDomain = 'VGA';
        
        DocuSignAPI.RequestRecipientTokenClientURLs clientURLs = new DocuSignAPI.RequestRecipientTokenClientURLs();
        
        clientURLs.OnAccessCodeFailed = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnAccessCodeFailed&envelopeid=' + envelopeID;
        clientURLs.OnCancel = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnCancel&envelopeid=' + envelopeID;
        clientURLs.OnDecline = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnDecline&envelopeid=' + envelopeID;
        clientURLs.OnException = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnException&envelopeid=' + envelopeID;
        clientURLs.OnFaxPending = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnFaxPending&envelopeid=' + envelopeID;
        clientURLs.OnIdCheckFailed = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnIdCheckFailed&envelopeid=' + envelopeID;
        clientURLs.OnSessionTimeout = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnSessionTimeout&envelopeid=' + envelopeID;
        clientURLs.OnSigningComplete = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnSigningComplete&envelopeid=' + envelopeID;
        clientURLs.OnTTLExpired = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnTTLExpired&envelopeid=' + envelopeID;
        clientURLs.OnViewingComplete = getPopURL(brand, objID) + '?Id=' + contract.id + '&event=OnViewingComplete&envelopeid=' + envelopeID;

        // assumes apiService = preconfigured api proxy
        try {
            if(Test.isRunningTest())
            {            
                token= 'test';
            }
            else 
            {            
                 token = dsApiSend.RequestRecipientToken(envelopeId,recipient_dealer.captiveinfo.ClientUserId,recipient_dealer.UserName,recipient_dealer.Email,assert,clientURLs);
            }
        }  catch ( CalloutException e) {
                System.debug('Exception - ' + e );
            	return 'ERROR: Webservice error.';
        }	
        
        return token;
        
    }

public static string getPopURL(String brand, Id case_id) 
{
    return System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + brand + '/s/case/' + case_id + '/detail';
}    
    
}