public class VGA_UserProfileHandler{

    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<VGA_User_Profile__c>) trigger.new);
        }
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<VGA_User_Profile__c>) trigger.new, (Map<Id, VGA_User_Profile__c>) trigger.OldMap);
        }
        
    }
    
    public void onBeforeInsert(List<VGA_User_Profile__c> lstTriggerNew)
    {

    }
    
    public void onBeforeUpdate(List<VGA_User_Profile__c> lstTriggerNew, Map<Id,VGA_User_Profile__c> triggeroldmap)
    {
       validateAvalibleToReceiveLeads(lstTriggerNew);   
    }

    /**
    * @author Ganesh M
    * @date 23/11/2018
    * @description   It Should not be able to set nominate user as unavailable to receive leads
    * @param  lsttriggernew is a collection of new User Profile Records
    * @return  Null
    */
    private static void validateAvalibleToReceiveLeads(list<VGA_User_Profile__c> lsttriggernew)
    {

        set<id> contactIdSet = new set<id>();
        Map<String, VGA_User_Profile__c> userProfilePerContactKeyMap = new Map<String, VGA_User_Profile__c>();
        map<id,VGA_Dealership_Profile__c> dealershipmap = new map<id,VGA_Dealership_Profile__c>();
        
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {

            for(VGA_User_Profile__c objUserProfile : lsttriggernew)
            {
                String key = objUserProfile.VGA_Contact__c + objUserProfile.VGA_Sub_Brand__c;
                contactIdSet.add(objUserProfile.VGA_Contact__c);
                userProfilePerContactKeyMap.put(key, objUserProfile);
            }
            

            for(VGA_Dealership_Profile__c objdealerProfile :[SELECT Id,VGA_Distribution_Method__c ,VGA_Nominated_Contact__c ,name,VGA_Sub_Brand__c  
                                                             FROM VGA_Dealership_Profile__c 
                                                             WHERE VGA_Nominated_Contact__c IN: contactIdSet
                                                             AND VGA_Distribution_Method__c = 'Nominated User'  ])
            {
                String key = objdealerProfile.VGA_Nominated_Contact__c + objdealerProfile.VGA_Sub_Brand__c;
                if(userProfilePerContactKeyMap.containsKey(key)){
                    dealershipmap.put(objdealerProfile.VGA_Nominated_Contact__c,objdealerProfile);
                }
                
            }


            for(VGA_User_Profile__c objUserProfile : lsttriggernew)
            {
                if(objUserProfile.VGA_Available__c == false 
                  && dealershipmap.containskey(objUserProfile.VGA_Contact__c))
                {
                    objUserProfile.VGA_Available__c.addError('Cannot change available to receive leads as user is set to nominated user.');

                }
            }

        }
    }
}