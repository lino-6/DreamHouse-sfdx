/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_ContentDocumentTriggerHandlerTest {

    
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c CustomTrigger =new VGA_Triggers__c();
        CustomTrigger = VGA_CommonTracker.buildCustomSettingTrigger('ContentDocument',true);
        insert CustomTrigger;
    }
    
    static testMethod void myUnitTest()
    {
    	 ContentVersion contentVersion_1 = new ContentVersion(
	      Title = 'Penguins',
	      PathOnClient = 'Penguins.jpg',
	      VersionData = Blob.valueOf('Test Content')
	      );
	      
    	 insert contentVersion_1;
    	 
    	 ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];
    	 
    	 List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
    	 
    	System.assertEquals(documents.size(), 1);
    	System.assertEquals(documents[0].Id, contentVersion_2.ContentDocumentId);
    	System.assertEquals(documents[0].LatestPublishedVersionId, contentVersion_2.Id);
    	System.assertEquals(documents[0].Title, contentVersion_2.Title);
    	
    	 delete documents;
    }
}