/**
* @author Mohammed Ishaque Shaikh
* @date 05/Mar/2019
* @description Test Class for the VGA_TDAttachment.
*              It tests the collectAttachment  method that returns information about Attachment.
*/
@isTest
public class VGA_TDAttachment_Test {
    
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we pass valid Attachment Id for testing
     * 
     */
    @isTest
    static void testWithAttachmentId(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	req.addParameter('id', att.Id);
       		req.requestURI = '/services/apexrest/getAttachment';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_TDAttachment.collectAttachment();
        	System.assertEquals(200,((VGA_TDAttachment.AttachementResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_TDAttachment.AttachementResponseWrapper.class)).statuscode);
        Test.stopTest();
    }
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we wont be pass Attachment Id  for testing 
     * 
     */
    @isTest
    static void testWithoutAttachmentId(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	//req.addParameter('id', att.Id);
       		req.requestURI = '/services/apexrest/getAttachment';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_TDAttachment.collectAttachment();
        	System.assertEquals(500,((VGA_TDAttachment.AttachementResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_TDAttachment.AttachementResponseWrapper.class)).statuscode);
        Test.stopTest();
    }
    /*
     * @author Mohammed Ishaque Shaikh
     * @description In this Method we pass Invalid Attachment Id for testing
     * 
     */
    @isTest
    static void testWithInvalidAttachmentId(){
        Account acc=VGA_CommonTracker.buildDealerAccount('Test','0007');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;
        Attachment att=new Attachment();
        att.parentId=acc.Id;
        att.Body=EncodingUtil.base64Decode('image data');
        att.Name='consentform';
        insert att;
        Test.startTest();
        	RestRequest req = new RestRequest(); 
       		RestResponse res = new RestResponse();
         	req.addParameter('id', 'dfsdfs');
       		req.requestURI = '/services/apexrest/getAttachment';  
       		req.httpMethod = 'GET';
       		RestContext.request = req;
       		RestContext.response= res;
       		VGA_TDAttachment.collectAttachment();
        	System.assertEquals(500,((VGA_TDAttachment.AttachementResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_TDAttachment.AttachementResponseWrapper.class)).statuscode);
        Test.stopTest();
    }
    
}