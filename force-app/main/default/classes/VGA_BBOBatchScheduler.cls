global class VGA_BBOBatchScheduler implements Schedulable {

	global void execute(SchedulableContext sc)
    {
        // Implement any logic to be scheduled
       
        // We now call the batch class to be scheduled
        VGA_BBOBatchUpdate b = new VGA_BBOBatchUpdate();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        //Apex callout limit is 100
        database.executebatch(b,100);
    }
    
}