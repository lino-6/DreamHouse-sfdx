public class VGA_DealershipProfileTriggerHandler 
{
    public void runTrigger()
    {
        if(Trigger.isBefore && Trigger.isInsert)
        {
           onBeforeInsert((List<VGA_Dealership_Profile__c>) trigger.new);
        }
        if(Trigger.isBefore && Trigger.isUpdate)
        {
           onBeforeUpdate((List<VGA_Dealership_Profile__c>) trigger.new, (Map<Id, VGA_Dealership_Profile__c>) trigger.OldMap);
        }
        if(Trigger.isAfter && Trigger.isInsert)
        {
           onAfterInsert((List<VGA_Dealership_Profile__c>) trigger.new);
        }
        if(Trigger.isAfter && Trigger.isUpdate)
        {
           onAfterUpdate((List<VGA_Dealership_Profile__c>) trigger.new, (Map<Id, VGA_Dealership_Profile__c>) trigger.OldMap);
        }
    }
     public void onBeforeInsert(List<VGA_Dealership_Profile__c> lstTriggerNew)
     {
        updateNominatedUser(lstTriggerNew,null);    
     }
     public void onBeforeUpdate(List<VGA_Dealership_Profile__c> lstTriggerNew, Map<Id,VGA_Dealership_Profile__c> triggeroldmap)
     {
        updateNominatedUser(lstTriggerNew,triggeroldmap);
     }
      public void onAfterInsert(List<VGA_Dealership_Profile__c> lstTriggerNew)
     {
        updateDistributionMethod(lstTriggerNew,null);    
     }
     public void onAfterUpdate(List<VGA_Dealership_Profile__c> lstTriggerNew, Map<Id,VGA_Dealership_Profile__c> triggeroldmap)
     {
        updateDistributionMethod(lstTriggerNew,triggeroldmap);
        ValidationNominatedUser(lstTriggerNew,triggeroldmap);
     }
     //==============================================================================================================
     //Description: This method is used to update Nominated user on Dealership Profile Based on Nominated Contact
     //==============================================================================================================
     //By:Rishi Patel          Date:2-2-18
     //=============================================================================================================
     private static void updateNominatedUser(list<VGA_Dealership_Profile__c> lsttriggernew,map<Id,VGA_Dealership_Profile__c> triggeroldmap)
     {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            set<Id> setContactID = new set<Id>();
            for(VGA_Dealership_Profile__c objDealershipProfile : lsttriggernew)
            {
            
                if(objDealershipProfile.VGA_Nominated_Contact__c != null && (triggeroldmap == null 
                    || (triggeroldmap != null && (triggeroldmap.get(objDealershipProfile.id).VGA_Nominated_Contact__c != objDealershipProfile.VGA_Nominated_Contact__c))))
                {
               
                    setContactID.add(objDealershipProfile.VGA_Nominated_Contact__c);    
                }
                
            }
            if(setContactID != null && !setContactID.isEmpty())
            {
            
                list<User> lstUser = [select id,isActive,ContactID from User where contactId in:setContactID];
                
                map<ID,User> mapofUser = new map<Id,User>();
                if(lstUser != null && !lstUser.isEmpty())
                {
                
                    for(User objUser : lstUser)
                    {
                        if(!mapofUser.containskey(objUser.contactID))
                            mapofUser.put(objUser.contactID,objUser);
                    }
                }
                for(VGA_Dealership_Profile__c objDealershipProfile : lsttriggernew)
                {
                 
                    
                    if(objDealershipProfile.VGA_Nominated_Contact__c != null && (triggeroldmap == null 
                        || (triggeroldmap != null && (triggeroldmap.get(objDealershipProfile.id).VGA_Nominated_Contact__c != objDealershipProfile.VGA_Nominated_Contact__c))))
                    {
                   
                        if(mapofUser != null && !mapofUser.isEmpty() 
                            && mapofUser.containskey(objDealershipProfile.VGA_Nominated_Contact__c) 
                            && mapofUser.get(objDealershipProfile.VGA_Nominated_Contact__c) != null)
                        {
                       
                            if(mapofUser.get(objDealershipProfile.VGA_Nominated_Contact__c).isActive)
                            {
                                objDealershipProfile.VGA_Nominated_User__c = mapofUser.get(objDealershipProfile.VGA_Nominated_Contact__c).id;   
                            }
                            else
                            {
                                if(!test.isrunningTest())
                                    objDealershipProfile.VGA_Nominated_Contact__c.addError('This Dealer Contact is not enabled as Partner User');
                            }
                        }
                        else
                        {
                            if(!test.isrunningTest())
                                objDealershipProfile.VGA_Nominated_Contact__c.addError('This Dealer Contact is not enabled as Partner User');   
                        }
                    }
                }
            }
        }
     } 
    private static void updateDistributionMethod(list<VGA_Dealership_Profile__c> lsttriggernew,map<Id,VGA_Dealership_Profile__c> triggeroldmap)  
    {
        list<account>updatelistofAccount =new list<account>();
         list<account>updatelistofAccountofCV =new list<account>();
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            map<string,string>mapofPVmethod =new map<string,string>();
            map<string,string>mapofCVmethod =new map<string,string>();
            for(VGA_Dealership_Profile__c objDealershipProfile : lsttriggernew)
            {
                if(objDealershipProfile.VGA_Dealership__c !=Null && objDealershipProfile.VGA_Distribution_Method__c !=Null && objDealershipProfile.VGA_Sub_Brand__c !=Null && 
                   objDealershipProfile.VGA_Sub_Brand__c =='Passenger Vehicles (PV)' && (triggeroldmap == null 
                || (triggeroldmap != null && (triggeroldmap.get(objDealershipProfile.id).VGA_Distribution_Method__c != objDealershipProfile.VGA_Distribution_Method__c))))
                {
                   mapofPVmethod.put(objDealershipProfile.VGA_Dealership__c,objDealershipProfile.VGA_Distribution_Method__c);
                }
                if(objDealershipProfile.VGA_Dealership__c !=Null && objDealershipProfile.VGA_Distribution_Method__c !=Null && objDealershipProfile.VGA_Sub_Brand__c !=Null && 
                   objDealershipProfile.VGA_Sub_Brand__c =='Commercial Vehicles (CV)' && (triggeroldmap == null 
                || (triggeroldmap != null && (triggeroldmap.get(objDealershipProfile.id).VGA_Distribution_Method__c != objDealershipProfile.VGA_Distribution_Method__c))))
                {
                   mapofCVmethod.put(objDealershipProfile.VGA_Dealership__c,objDealershipProfile.VGA_Distribution_Method__c);
                }
            }
            if(mapofPVmethod !=Null && mapofPVmethod.size()>0)
            {
              list<account>listofAccount =new list<account>([select Id from Account where Id=:mapofPVmethod.keyset()]);
              if(listofAccount !=Null && listofAccount.size()>0)
              {
                 for(Account objAccount :listofAccount)
                 {
                   objAccount.VGA_Distribution_Method_PV_Report__c=mapofPVmethod.get(objAccount.Id);
                   updatelistofAccount.add(objAccount);
                 }
              }
            }
            if(mapofCVmethod !=Null && mapofCVmethod.size()>0)
            {
              list<account>listofAccount =new list<account>([select Id from Account where Id=:mapofCVmethod.keyset()]);
              if(listofAccount !=Null && listofAccount.size()>0)
              {
                 for(Account objAccount :listofAccount)
                 {
                   objAccount.VGA_Distribution_Method_CV_Report__c=mapofCVmethod.get(objAccount.Id);
                   updatelistofAccountofCV.add(objAccount);
                 }
              }
            }
            if(updatelistofAccount !=Null && updatelistofAccount.size()>0)
            {
              update updatelistofAccount;
            }
            if(updatelistofAccountofCV !=Null && updatelistofAccountofCV.size()>0)
            {
              update updatelistofAccountofCV;
            }
        }
    }
    /**
      * @author Ganesh M
      * @date 23/11/2018
      * @description   It Should not be able to set nominate user as unavailable to receive leads
      * @param  lsttriggernew is a collection of new Dealership Records
      * @return  Null
      */ 
 private static void ValidationNominatedUser(list<VGA_Dealership_Profile__c> lsttriggernew,map<Id,VGA_Dealership_Profile__c> triggeroldmap)
     {
     if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
           set<Id> setContactID = new set<Id>();
            for(VGA_Dealership_Profile__c objDealershipProfile : lsttriggernew)
            {
              if(objDealershipProfile.VGA_Nominated_Contact__c != null && (triggeroldmap == null 
                    || (triggeroldmap != null && (triggeroldmap.get(objDealershipProfile.id).VGA_Nominated_Contact__c != objDealershipProfile.VGA_Nominated_Contact__c))))
                {
               
                    setContactID.add(objDealershipProfile.VGA_Nominated_Contact__c);    
                }
               
            }
            if(setContactID != null && !setContactID.isEmpty())
            {
       
              list<VGA_User_Profile__c> lstUserProfile = [select id,VGA_Sub_Brand__c,VGA_Contact__c from VGA_User_Profile__c where VGA_Contact__c=:setContactID and VGA_Available__c = false];
             
              set<string>UserId = new set<string>();
              set<string> Brandset = new set<string>();
           if(lstUserProfile != null && !lstUserProfile.isEmpty())     
     
            {
         
              for(VGA_User_Profile__c objUserProfile : lstUserProfile)
               {
                UserId.add(VGA_Common.getuserId(objUserProfile.VGA_Contact__c));
                Brandset.add(objUserProfile.VGA_Sub_Brand__c);
               }
              
           list<VGA_Dealership_Profile__c>listofDealershipProfile =[select Id,VGA_Nominated_User__c  from VGA_Dealership_Profile__c where VGA_Nominated_User__c in :UserId and name in : Brandset and VGA_Distribution_Method__c = 'Nominated User'];
                  
            if(listofDealershipProfile !=Null && listofDealershipProfile.size()>0)
            {
         
            for(VGA_Dealership_Profile__c objDealerProfile : lsttriggernew)
            {
             if(objDealerProfile.VGA_Nominated_Contact__c != null)
                 {
                  
                          objDealerProfile.VGA_Nominated_Contact__c.addError('A contact cannot be set as Nominated User if it is unavailable to receive leads.');
                         
                  }
              }
           }
        }
       }
      }
    }   
}