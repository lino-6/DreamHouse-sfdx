//=================================================================================
// Tracker class for VGA_ManualAssignmentTriggerHandler class.
// =================================================================================
// Created by Nitisha Prasad on 21-Nov-2017.
// =================================================================================
@isTest
private class VGA_ManualAssignmentTHTracker
{
    public static VGA_Manual_Assignment_Configuration__c objPre;
    public static Product2 objProduct;    
    static testMethod void unitTestUpdate() 
    {
        LoadData();
        boolean isClosed;
        Test.startTest();        
        objPre.VGA_Status__c = 'New';
        objPre.VGA_Tracking_Id__c = '734';
        objPre.VGA_Number_of_Leads_per_Distribution__c =1;
        objPre.VGA_Time_Interval__c = 1;        
        if(objPre.VGA_Status__c=='New')
   		{
            objPre.VGA_Status__c = 'In Progress';
            objPre.VGA_Start_Batch__c = true;
            objPre.VGA_Immediate__c   = true;
            isClosed = false;                                 
 		}
        update objPre;
            
        objPre.VGA_Status__c = 'Closed';
        objPre.VGA_Tracking_Id__c = '734';
        objPre.VGA_Number_of_Leads_per_Distribution__c =1;
        objPre.VGA_Time_Interval__c = 1;
        objPre.VGA_Start_Batch__c = true;
        objPre.VGA_Immediate__c   = true;
        if(objPre.VGA_Status__c=='Closed')
   		{
           
            objPre.VGA_Start_Batch__c = false;
            objPre.VGA_Immediate__c   = false;
            isClosed = true;
            
                               
 		}
       
        update objPre;
        Test.stopTest();

    }
  
    public static void LoadData()
    {
       VGA_Triggers__c objCustom = new VGA_Triggers__c();
       objCustom.Name = 'VGA_Manual_Assignment_Configuration__c';
       objCustom.VGA_Is_Active__c= true;
         
        
       insert objCustom;
        
       objProduct = VGA_CommonTracker.createProduct('test');
       insert objProduct;
       objPre = VGA_CommonTracker.createPreLaunch(objProduct.id);
       insert objPre;    
    }
}