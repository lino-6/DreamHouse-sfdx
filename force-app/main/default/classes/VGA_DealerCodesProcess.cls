public class VGA_DealerCodesProcess
{
    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that is being called from the before insert and before update methods on the Post Code Mapping object.
    *              This method process all the post code mapping records that have been inserted/updated and populate the dealer lookup fields
    *              based on the dealer code. The following fields will be populated VWP Dealer, VWC Dealer, SKP Dealer, VWP Dealer, VWC Dealer and SKP Dealer
    *              The last three fields were added as per changes made in the last update
    */
    public  static void checkDealerCodes(list<VGA_Post_Code_Mapping__c> lsttriggernew,map<ID,VGA_Post_Code_Mapping__c> triggeroldmap){
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            //We will use only one Set to store all the codes
            Set<String> setDealerCode = new Set<String>();

            for(VGA_Post_Code_Mapping__c newPostCodeMapping : lsttriggernew)
            {   
                VGA_Post_Code_Mapping__c oldPostCodeMapping = null;

                if(triggeroldmap != null 
                    && triggeroldmap.containsKey(newPostCodeMapping.id)){
                    oldPostCodeMapping = triggeroldmap.get(newPostCodeMapping.id);
                }
                  
                checkDealerCodeforSKP(newPostCodeMapping, oldPostCodeMapping, setDealerCode);
                checkDealerCodeforVWP(newPostCodeMapping, oldPostCodeMapping, setDealerCode);
                checkDealerCodeforVWC(newPostCodeMapping, oldPostCodeMapping, setDealerCode);
            }

            if(setDealerCode != null && !setDealerCode.isEmpty()) 
            {
                //Only one map where all the accounts will be stored group by the code
                Map<String,Account> mapofDealerCodesAccount = new Map<String,Account>();
                
                for(Account objAccount : [select Id, VGA_Dealer_Code__c, VGA_Brand__c, VGA_Sub_Brand__c from Account where VGA_Dealer_Code__c IN : setDealerCode])
                {
                    mapofDealerCodesAccount.put(objAccount.VGA_Dealer_Code__c,objAccount);                       
                }

                for(VGA_Post_Code_Mapping__c objPCM : lsttriggernew)
                {
                    updateDealerCodeforSKP(objPCM, mapofDealerCodesAccount, setDealerCode);
                    updateDealerCodeforVWP(objPCM, mapofDealerCodesAccount, setDealerCode);
                    updateDealerCodeforVWC(objPCM, mapofDealerCodesAccount, setDealerCode);
                }

            }
        }
    }

    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that checks the SKP dealer code and add it to a set with
    *              all the dealer codes that have changed if it is an update transaction
    *              or that are not null if it is an insert transaction.
    *              In case the value has been updated and it is now null then
    *              the related dealer will be updated so it is also null.
    */
    private static void checkDealerCodeforSKP(VGA_Post_Code_Mapping__c newPostCodeMapping, VGA_Post_Code_Mapping__c oldPostCodeMapping, Set<String> setDealerCode)
    {
        if((newPostCodeMapping.VGA_SKP_Dealer_Code__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_SKP_Dealer_Code__c != newPostCodeMapping.VGA_SKP_Dealer_Code__c))
        {
            if(newPostCodeMapping.VGA_SKP_Dealer_Code__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_SKP_Dealer_Code__c);       
            } else {
                newPostCodeMapping.VGA_SKP_Dealer__c = null;
            }
            
        }
        
        if((newPostCodeMapping.VGA_SKP_Dealer_Code_2nd__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_SKP_Dealer_Code_2nd__c != newPostCodeMapping.VGA_SKP_Dealer_Code_2nd__c))
        {
            if(newPostCodeMapping.VGA_SKP_Dealer_Code_2nd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_SKP_Dealer_Code_2nd__c);          
            } else {
                newPostCodeMapping.VGA_SKP_Dealer_2nd__c = null;
            }
                
        }
        
        if((newPostCodeMapping.VGA_SKP_Dealer_Code_3rd__c != null && Trigger.isInsert)
         || (Trigger.isUpdate && oldPostCodeMapping.VGA_SKP_Dealer_Code_3rd__c != newPostCodeMapping.VGA_SKP_Dealer_Code_3rd__c))
        {
            if(newPostCodeMapping.VGA_SKP_Dealer_Code_3rd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_SKP_Dealer_Code_3rd__c);          
            } else {
                newPostCodeMapping.VGA_SKP_Dealer_3rd__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_SKP_Dealer_Code_4th__c != null && Trigger.isInsert )
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_SKP_Dealer_Code_4th__c != newPostCodeMapping.VGA_SKP_Dealer_Code_4th__c))
        {
            if(newPostCodeMapping.VGA_SKP_Dealer_Code_4th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_SKP_Dealer_Code_4th__c);          
            } else {
                newPostCodeMapping.VGA_SKP_Dealer_4th__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_SKP_Dealer_Code_5th__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_SKP_Dealer_Code_5th__c != newPostCodeMapping.VGA_SKP_Dealer_Code_5th__c))
        {
            if(newPostCodeMapping.VGA_SKP_Dealer_Code_5th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_SKP_Dealer_Code_5th__c);          
            } else {
                newPostCodeMapping.VGA_SKP_Dealer_5th__c = null;
            }     
        }
    }

    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that update the dealer lookup for all the SKP dealer codes 
    *              that have changed if it is an update transaction or that are not
    *              null if it is an insert transaction.
    *              In case the dealer code and post code did not return any account
    *              result will be showing an error in all the affected fields.
    */
    private static void updateDealerCodeforSKP(VGA_Post_Code_Mapping__c objPCM, Map<String,Account> mapofDealerCodesAccount, Set<String> setDealerCode)
    {
        
        if(objPCM.VGA_SKP_Dealer_Code__c != null && setDealerCode.contains(objPCM.VGA_SKP_Dealer_Code__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_SKP_Dealer_Code__c) && mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code__c) != null)
            {
                objPCM.VGA_SKP_Dealer__c =  mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code__c).Id;
            }
            else
            {

                objPCM.VGA_SKP_Dealer_Code__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_SKP_Dealer_Code__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_SKP_Dealer_Code_2nd__c != null && setDealerCode.contains(objPCM.VGA_SKP_Dealer_Code_2nd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_SKP_Dealer_Code_2nd__c) && mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_2nd__c) != null)
            {
                objPCM.VGA_SKP_Dealer_2nd__c =  mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_2nd__c).Id;
            }
            else
            {
                
                objPCM.VGA_SKP_Dealer_Code_2nd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_SKP_Dealer_Code_2nd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_SKP_Dealer_Code_3rd__c != null  && setDealerCode.contains(objPCM.VGA_SKP_Dealer_Code_3rd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_SKP_Dealer_Code_3rd__c) && mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_3rd__c) != null)
            {
                objPCM.VGA_SKP_Dealer_3rd__c =  mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_3rd__c).Id;
            }
            else
            {
              
                objPCM.VGA_SKP_Dealer_Code_3rd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_SKP_Dealer_Code_3rd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_SKP_Dealer_Code_4th__c != null  && setDealerCode.contains(objPCM.VGA_SKP_Dealer_Code_4th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_SKP_Dealer_Code_4th__c) && mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_4th__c) != null)
            {
                objPCM.VGA_SKP_Dealer_4th__c =  mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_4th__c).Id;
            }
            else
            {
                
                objPCM.VGA_SKP_Dealer_Code_4th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_SKP_Dealer_Code_4th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_SKP_Dealer_Code_5th__c != null  && setDealerCode.contains(objPCM.VGA_SKP_Dealer_Code_5th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_SKP_Dealer_Code_5th__c) && mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_5th__c) != null)
            {
                objPCM.VGA_SKP_Dealer_5th__c =  mapofDealerCodesAccount.get(objPCM.VGA_SKP_Dealer_Code_5th__c).Id;
            }
            else
            {
                objPCM.VGA_SKP_Dealer_Code_5th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_SKP_Dealer_Code_5th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        }      
    }
    
    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that checks the VWP dealer code and add it to a set with
    *              all the dealer codes that have changed if it is an update transaction
    *              or that are not null if it is an insert transaction.
    *              In case the value has been updated and it is now null then
    *              the related dealer will be updated so it is also null.
    */
    private static void checkDealerCodeforVWP(VGA_Post_Code_Mapping__c newPostCodeMapping, VGA_Post_Code_Mapping__c oldPostCodeMapping, Set<String> setDealerCode)
    {
        if((newPostCodeMapping.VGA_VWP_Dealer_Code__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWP_Dealer_Code__c != newPostCodeMapping.VGA_VWP_Dealer_Code__c))
        {
            if(newPostCodeMapping.VGA_VWP_Dealer_Code__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWP_Dealer_Code__c);       
            } else {
                newPostCodeMapping.VGA_VWP_Dealer__c = null;
            }
            
        }
        
        if((newPostCodeMapping.VGA_VWP_Dealer_Code_2nd__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWP_Dealer_Code_2nd__c != newPostCodeMapping.VGA_VWP_Dealer_Code_2nd__c))
        {
            if(newPostCodeMapping.VGA_VWP_Dealer_Code_2nd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWP_Dealer_Code_2nd__c);          
            } else {
                newPostCodeMapping.VGA_VWP_Dealer_2nd__c = null;
            }
                
        }
        
        if((newPostCodeMapping.VGA_VWP_Dealer_Code_3rd__c != null && Trigger.isInsert)
         || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWP_Dealer_Code_3rd__c != newPostCodeMapping.VGA_VWP_Dealer_Code_3rd__c))
        {
            if(newPostCodeMapping.VGA_VWP_Dealer_Code_3rd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWP_Dealer_Code_3rd__c);          
            } else {
                newPostCodeMapping.VGA_VWP_Dealer_3rd__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_VWP_Dealer_Code_4th__c != null && Trigger.isInsert )
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWP_Dealer_Code_4th__c != newPostCodeMapping.VGA_VWP_Dealer_Code_4th__c))
        {
            if(newPostCodeMapping.VGA_VWP_Dealer_Code_4th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWP_Dealer_Code_4th__c);          
            } else {
                newPostCodeMapping.VGA_VWP_Dealer_4th__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_VWP_Dealer_Code_5th__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWP_Dealer_Code_5th__c != newPostCodeMapping.VGA_VWP_Dealer_Code_5th__c))
        {
            if(newPostCodeMapping.VGA_VWP_Dealer_Code_5th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWP_Dealer_Code_5th__c);          
            } else {
                newPostCodeMapping.VGA_VWP_Dealer_5th__c = null;
            }     
        }
    }

    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that update the dealer lookup for all the VWP dealer codes 
    *              that have changed if it is an update transaction or that are not
    *              null if it is an insert transaction.
    *              In case the dealer code and post code did not return any account
    *              result will be showing an error in all the affected fields.
    */
    private static void updateDealerCodeforVWP(VGA_Post_Code_Mapping__c objPCM, Map<String,Account> mapofDealerCodesAccount, Set<String> setDealerCode)
    {
        
        if(objPCM.VGA_VWP_Dealer_Code__c != null && setDealerCode.contains(objPCM.VGA_VWP_Dealer_Code__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWP_Dealer_Code__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code__c) != null)
            {
                objPCM.VGA_VWP_Dealer__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code__c).Id;
            }
            else
            {

                objPCM.VGA_VWP_Dealer_Code__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWP_Dealer_Code__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWP_Dealer_Code_2nd__c != null && setDealerCode.contains(objPCM.VGA_VWP_Dealer_Code_2nd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWP_Dealer_Code_2nd__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_2nd__c) != null)
            {
                objPCM.VGA_VWP_Dealer_2nd__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_2nd__c).Id;
            }
            else
            {
                
                objPCM.VGA_VWP_Dealer_Code_2nd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWP_Dealer_Code_2nd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWP_Dealer_Code_3rd__c != null  && setDealerCode.contains(objPCM.VGA_VWP_Dealer_Code_3rd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWP_Dealer_Code_3rd__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_3rd__c) != null)
            {
                objPCM.VGA_VWP_Dealer_3rd__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_3rd__c).Id;
            }
            else
            {
              
                objPCM.VGA_VWP_Dealer_Code_3rd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWP_Dealer_Code_3rd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWP_Dealer_Code_4th__c != null  && setDealerCode.contains(objPCM.VGA_VWP_Dealer_Code_4th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWP_Dealer_Code_4th__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_4th__c) != null)
            {
                objPCM.VGA_VWP_Dealer_4th__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_4th__c).Id;
            }
            else
            {
                
                objPCM.VGA_VWP_Dealer_Code_4th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWP_Dealer_Code_4th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWP_Dealer_Code_5th__c != null  && setDealerCode.contains(objPCM.VGA_VWP_Dealer_Code_5th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWP_Dealer_Code_5th__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_5th__c) != null)
            {
                objPCM.VGA_VWP_Dealer_5th__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWP_Dealer_Code_5th__c).Id;
            }
            else
            {
                objPCM.VGA_VWP_Dealer_Code_5th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWP_Dealer_Code_5th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        }      
    }

    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that checks the VWC dealer code and add it to a set with
    *              all the dealer codes that have changed if it is an update transaction
    *              or that are not null if it is an insert transaction.
    *              In case the value has been updated and it is now null then
    *              the related dealer will be updated so it is also null.
    */
    private static void checkDealerCodeforVWC(VGA_Post_Code_Mapping__c newPostCodeMapping, VGA_Post_Code_Mapping__c oldPostCodeMapping, Set<String> setDealerCode)
    {
        if((newPostCodeMapping.VGA_VWC_Dealer_Code__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWC_Dealer_Code__c != newPostCodeMapping.VGA_VWC_Dealer_Code__c))
        {
            if(newPostCodeMapping.VGA_VWC_Dealer_Code__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWC_Dealer_Code__c);       
            } else {
                newPostCodeMapping.VGA_VWC_Dealer__c = null;
            }
            
        }
        
        if((newPostCodeMapping.VGA_VWC_Dealer_Code_2nd__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWC_Dealer_Code_2nd__c != newPostCodeMapping.VGA_VWC_Dealer_Code_2nd__c))
        {
            if(newPostCodeMapping.VGA_VWC_Dealer_Code_2nd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWC_Dealer_Code_2nd__c);          
            } else {
                newPostCodeMapping.VGA_VWC_Dealer_2nd__c = null;
            }
                
        }
        
        if((newPostCodeMapping.VGA_VWC_Dealer_Code_3rd__c != null && Trigger.isInsert)
         || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWC_Dealer_Code_3rd__c != newPostCodeMapping.VGA_VWC_Dealer_Code_3rd__c))
        {
            if(newPostCodeMapping.VGA_VWC_Dealer_Code_3rd__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWC_Dealer_Code_3rd__c);          
            } else {
                newPostCodeMapping.VGA_VWC_Dealer_3rd__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_VWC_Dealer_Code_4th__c != null && Trigger.isInsert )
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWC_Dealer_Code_4th__c != newPostCodeMapping.VGA_VWC_Dealer_Code_4th__c))
        {
            if(newPostCodeMapping.VGA_VWC_Dealer_Code_4th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWC_Dealer_Code_4th__c);          
            } else {
                newPostCodeMapping.VGA_VWC_Dealer_4th__c = null;
            }     
        }
        
        if((newPostCodeMapping.VGA_VWC_Dealer_Code_5th__c != null && Trigger.isInsert) 
            || (Trigger.isUpdate && oldPostCodeMapping.VGA_VWC_Dealer_Code_5th__c != newPostCodeMapping.VGA_VWC_Dealer_Code_5th__c))
        {
            if(newPostCodeMapping.VGA_VWC_Dealer_Code_5th__c != null){
                setDealerCode.add(newPostCodeMapping.VGA_VWC_Dealer_Code_5th__c);          
            } else {
                newPostCodeMapping.VGA_VWC_Dealer_5th__c = null;
            }     
        }
    }

    /**
    * @author Ganesh M
    * @date 26/11/2018
    * @description Method that update the dealer lookup for all the VWC dealer codes 
    *              that have changed if it is an update transaction or that are not
    *              null if it is an insert transaction.
    *              In case the dealer code and post code did not return any account
    *              result will be showing an error in all the affected fields.
    */
    private static void updateDealerCodeforVWC(VGA_Post_Code_Mapping__c objPCM, Map<String,Account> mapofDealerCodesAccount, Set<String> setDealerCode)
    {
        
        if(objPCM.VGA_VWC_Dealer_Code__c != null && setDealerCode.contains(objPCM.VGA_VWC_Dealer_Code__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWC_Dealer_Code__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code__c) != null)
            {
                objPCM.VGA_VWC_Dealer__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code__c).Id;
            }
            else
            {

                objPCM.VGA_VWC_Dealer_Code__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWC_Dealer_Code__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWC_Dealer_Code_2nd__c != null && setDealerCode.contains(objPCM.VGA_VWC_Dealer_Code_2nd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWC_Dealer_Code_2nd__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_2nd__c) != null)
            {
                objPCM.VGA_VWC_Dealer_2nd__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_2nd__c).Id;
            }
            else
            {
                
                objPCM.VGA_VWC_Dealer_Code_2nd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWC_Dealer_Code_2nd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWC_Dealer_Code_3rd__c != null  && setDealerCode.contains(objPCM.VGA_VWC_Dealer_Code_3rd__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWC_Dealer_Code_3rd__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_3rd__c) != null)
            {
                objPCM.VGA_VWC_Dealer_3rd__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_3rd__c).Id;
            }
            else
            {
              
                objPCM.VGA_VWC_Dealer_Code_3rd__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWC_Dealer_Code_3rd__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWC_Dealer_Code_4th__c != null  && setDealerCode.contains(objPCM.VGA_VWC_Dealer_Code_4th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWC_Dealer_Code_4th__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_4th__c) != null)
            {
                objPCM.VGA_VWC_Dealer_4th__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_4th__c).Id;
            }
            else
            {
                
                objPCM.VGA_VWC_Dealer_Code_4th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWC_Dealer_Code_4th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        } 

        if(objPCM.VGA_VWC_Dealer_Code_5th__c != null  && setDealerCode.contains(objPCM.VGA_VWC_Dealer_Code_5th__c)) 
        {
            if(mapofDealerCodesAccount.containskey(objPCM.VGA_VWC_Dealer_Code_5th__c) && mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_5th__c) != null)
            {
                objPCM.VGA_VWC_Dealer_5th__c =  mapofDealerCodesAccount.get(objPCM.VGA_VWC_Dealer_Code_5th__c).Id;
            }
            else
            {
                objPCM.VGA_VWC_Dealer_Code_5th__c.addError('No Dealer Found For Dealer Code ' + objPCM.VGA_VWC_Dealer_Code_5th__c+' with Post Code '+objPCM.VGA_Post_Code__c);
            }
        }      
    }
}