/**
* @author Mohammed Ishaque Shaikh
* @date 2019-03-20
* @description test class for all apex method inside VGA_TestDriveShowEventDetailsController
* @Updated on 10/04/2019
*/


@isTest
public class VGA_TestDriveShowEventDetailsTest{
	
    @TestSetup
    public static void buildConfigList()
    {  
        List<contact> conList =new List<Contact>();
        List<User> UserList=new List<User>();
        List<VGA_User_Profile__c> conUserProfileList = new List<VGA_User_Profile__c> ();
        Account acc;
        
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Lead',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Account',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Contact',true));
        insert CustomList;
            
        acc=VGA_CommonTracker.buildDealerAccount('aa','qqq');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;        
        Account AccountCon=VGA_CommonTracker.createAccount();
        //Creating Contact
        Contact con=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt1');
        conList.add(con);
        Contact con1=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt2');
        con1.VGA_Role__c ='Dealer Administrator';
        conList.add(con1);
        insert conList;
        conUserProfileList=[SELECT Id,Name,VGA_Contact__c,VGA_Available_TestDrive__c FROM VGA_User_Profile__c];
        for(VGA_User_Profile__c t:conUserProfileList){
            t.VGA_Available_TestDrive__c=true;
        }
        update conUserProfileList;
        
        //Creating User 
        User PortalUser=VGA_CommonTracker.buildUser(conList[0].Id);
        PortalUser.IsActive  = True;
        UserList.add(PortalUser);
        User PortalUser1=VGA_CommonTracker.buildUser(conList[1].Id);
        PortalUser1.IsActive  = True;
        UserList.add(PortalUser1);
        Insert UserList; 
        
    }
    
   
    /**
    * @author Hijith NS
    * @date 2019-04-10
    * @description test Method to test the getLeadDetails Method
	*                   return Event on passing valid event id or else return null
    * 
    */
    
	static testMethod void test01_getLeadDetails_WithValidAndInValidEventID_returnsEventsOrNull(){
       
        List<User> UserList =[select id,IsActive,contact.AccountId,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        
		Event eve=new Event();
        eve.Subject='Holiday';
        eve.StartDateTime=system.now();
        eve.EndDateTime=system.now().addMinutes(45);
        eve.OwnerId=UserList[0].id;
        eve.WhatId=UserList[0].contact.AccountId;
        insert eve;
        System.assertNotEquals(null,VGA_TestDriveShowEventDetailsController.getLeadDetails(eve.Id));        
        System.assertEquals(null,VGA_TestDriveShowEventDetailsController.getLeadDetails('invalidID'));
       
	}
    
   /**
    * @author Hijith NS
    * @date 2019-04-10
    * @description test Method to test the updateEvent Method
	*                   return Event on passing valid event id or else return null
    * 
    */
     static testMethod void test02_updateEvent_OnPassingValidAndInvalidEvent(){
       
        List<User> UserList =[select id,IsActive,contact.AccountId,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        
		Event eve=new Event();
        eve.Subject='Holiday';
        eve.StartDateTime=system.now();
        eve.EndDateTime=system.now().addMinutes(45);
        eve.OwnerId=UserList[0].id;
        eve.WhatId=UserList[0].contact.AccountId;
        insert eve;
        eve.Subject='HOLI';
        System.assertEquals('HOLI',VGA_TestDriveShowEventDetailsController.updateEvent(JSON.serialize(eve)).Subject);
        
        //Passing Invalid event returns Null
        System.assertEquals(Null,VGA_TestDriveShowEventDetailsController.updateEvent('gfggg')); 
	}
    
    
    /**
    * @author Hijith NS
    * @date 2019-04-10
    * @description test Method to test the deleteEvent Method
	*                   Deletes Event on passing valid event id and return ture or else return false
    * 
    */
     static testMethod void test03_deleteEvent_OnPassingValidAndInvalidEvent(){
       
        List<User> UserList =[select id,IsActive,contact.AccountId,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        
		Event eve=new Event();
        eve.Subject='Holiday';
        eve.StartDateTime=system.now();
        eve.EndDateTime=system.now().addMinutes(45);
        eve.OwnerId=UserList[0].id;
        eve.WhatId=UserList[0].contact.AccountId;
        insert eve;
        eve.Subject='HOLI';
        System.assertEquals(true,VGA_TestDriveShowEventDetailsController.deleteEvent(eve.id));
        
        //Passing Invalid event returns false
        System.assertEquals(false,VGA_TestDriveShowEventDetailsController.deleteEvent('gfggg')); 
	}
   
}