public class VGA_EmailMessageTriggerHelper
{
    /**
    * @author Lino Diaz Alonso
    * @date 12/10/2018
    * @description Method to get the VGA To email from the objemail.ToAddress
    *              If there are more than one emails we will split the email
    *              addresses first and then return the one define in the 
    *              Email_To_Case__c custom setting.
    */
    public static String getVGAToEmail(EmailMessage objemail)
    {
        List<String> emailsList = new List<String>();

        //In case the to section has more than one email we will split the emails
        //and have every email in a position of a List
        if (objemail.ToAddress != null) 
        {
            if (objemail.ToAddress.contains(',') )
            {
                emailsList = objemail.ToAddress.split(',');
            }
            else if (objemail.ToAddress.contains(';') )
            {
                emailsList = objemail.ToAddress.split(';');
            }
            else
            {
                emailsList.add(objemail.ToAddress);
            }    
        }  

        if ( emailsList.size() > 0)
        {  
            Email_To_Case__c emailToCaseCS;

            //Iterate through the email list trying to find the one defined in
            //the Email_To_Case__c custom setting.
            for(String email: emailsList){  
                
                emailToCaseCS = Email_To_Case__c.getInstance(email.trim().left(38));
                //If we are not able to find the the instance 
                //we'll try utf 8 encoding
                if (emailToCaseCS == null)
                {
                    
                    String b  = EncodingUtil.urlEncode(email.trim(), 'UTF-8');    
                    
                    emailToCaseCS = Email_To_Case__c.getInstance(b.left(38)); 
                    if(emailToCaseCS==null)
                    {
                        // Return Null if the ToAddress is not found in Custom setting 
                        return null;
                    }
                    else
                    {
                        return emailToCaseCS.Name.trim();
                    }
                } else {
                    
                    return emailToCaseCS.Name.trim();
                }                        
            }
        }
        // Return Null if the ToAddress is not found in Custom setting  
        return Null;
        
    }

    /**
    * @author Lino Diaz Alonso
    * @date 16/01/2019
    * @description Method to check if the email address coming in the email message from is defined in
    *              Email_to_Case_Exclusion_List custom setting and a autoresponse email
    *              won't have to be sent in this case.
    * @param EmailMessage EmailMessage record that was received.
    * @return Boolean true if the email address is excluded and false if it is not.
    */
    public static Boolean isEmailExcluded(EmailMessage e){

        //get from email address, only take first 38 characters.
        string fromEmailAddress = '';
        if (e.fromaddress != null)
        {
            System.Debug('@@@ From email in casecustomautoresponse: ' +e.fromaddress );
            fromEmailAddress = e.fromaddress.left(38).toLowerCase();
        }

        // Rejecting AutoResponses
        //Exact email match
        Email_to_Case_Exclusion_List__c exclusion = Email_to_Case_Exclusion_List__c.getInstance(fromEmailAddress);
        if(exclusion != null && exclusion.name == fromEmailAddress){
            
            System.debug('Found exact match in exclusion list: ' + fromEmailAddress);
            //VGA_Common.sendExceptionEmail('Found exact match in exclusion list: ', fromEmailAddress);
            return false;
        }
        else
        {
            //Domain match
            String domain = '@' + fromEmailAddress.split('@').get(1);
            exclusion = Email_to_Case_Exclusion_List__c.getInstance(domain);
            if(exclusion != null && exclusion.name == domain){
                System.debug('Found domain match in exclusion list: ' + domain);
                //VGA_Common.sendExceptionEmail('Found domain match in exclusion list: ', domain);
                return false;
            }
        }

        return true;
    }
}