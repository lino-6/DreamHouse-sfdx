//------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          5-oct-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to in CampaignActivity Component.
public class VGA_CampaignActivityController 
{
// -----------------------------------------------------------------------------------------------------------------
// This method is used to get week start Date
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - None 
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0          17-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static date getweekstartdate()
{
    date startdate  = VGA_Common.getWeekStartDate(system.today());
    startdate       = startdate+1;
    return startdate;
}
// -----------------------------------------------------------------------------------------------------------------
// This method is used to get week End Date
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - None 
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0          17-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static date getweekEnddate()
{
    date startdate= VGA_Common.getWeekStartDate(system.today());
    date Enddate   =startdate.adddays(7);
    return Enddate;
}
// -----------------------------------------------------------------------------------------------------------------
// This method is used to get list of Campaign_Activity_Detail .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string 
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0          5-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static list<VGA_WrapperofProduct>getproductDetails(string brand,string subbrandvalue, string weekstartDate,string weekendDate)
{
        list<VGA_WrapperofProduct>wrapperlist       =new list<VGA_WrapperofProduct>();
        string datevalue =weekstartDate.replace('"', '');
        String[] myDateOnly = datevalue.split(' ');
        String[] strDate = myDateOnly[0].split('-');
        Integer myIntDate = integer.valueOf(strDate[2]);
        Integer myIntMonth = integer.valueOf(strDate[1]);
        Integer myIntYear = integer.valueOf(strDate[0]);
        date startdate =Date.newInstance(myIntYear, myIntMonth, myIntDate);
        string datevalue2 =weekendDate.replace('"', '');
        String[] myDateOnly2 = datevalue2.split(' ');
        String[] strDate2 = myDateOnly2[0].split('-');
        Integer myIntDate2 = integer.valueOf(strDate2[2]);
        Integer myIntMonth2 = integer.valueOf(strDate2[1]);
        Integer myIntYear2 = integer.valueOf(strDate2[0]);
        date enddate =Date.newInstance(myIntYear2, myIntMonth2, myIntDate2);
        //string recordTypeId=VGA_Common.getproductRecordTypeId(brand);// get product recordId.
        system.debug('brand============='+brand);
        system.debug('subbrandvalue============='+subbrandvalue);
        list<VGA_Campaign_Activity_Header__c> lstobjCampaign =new list<VGA_Campaign_Activity_Header__c>([select Id,VGA_Brand__c,VGA_Sub_Brand__c,VGA_Week_Start_Date__c,VGA_Week_Ending_Date__c,
                                                  VGA_Welcome_Optimization_Details__c,VGA_Search_Impressions__c,VGA_Web_Traffic__c,VGA_Car_Configurator_Downloads__c,
                                                  VGA_Social_Details__c,VGA_Social__c,VGA_Display_Details__c,
                                                  VGA_Display__c,VGA_CRM_Details__c,VGA_CRM__c,
                                                  VGA_Above_the_line_support_Details__c,VGA_Above_the_line_support__c from VGA_Campaign_Activity_Header__c
                                                  where VGA_Brand__c LIKE :('%' + brand + '%') and VGA_Sub_Brand__c LIKE :('%' + subbrandvalue + '%') and  VGA_Week_Start_Date__c=:startdate and VGA_Week_Ending_Date__c =:enddate
                                                   limit 1
                                                 ]);
      if(lstobjCampaign !=Null && lstobjCampaign.size()>0)
      {
          for(VGA_Campaign_Activity_Header__c objCampaign :lstobjCampaign)
          {
              VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct();
              objwrap.objCampaignActivity                   = objCampaign;
              wrapperlist.add(objwrap); 
          }
          if(lstobjCampaign[0].id !=Null)
          {
            
             list<VGA_Campaign_Activity_Detail__c>listofCampaign =new list<VGA_Campaign_Activity_Detail__c>([select Id,Name,VGA_Campaign_Activity_Header__c,VGA_Active_Brochure__c,VGA_Active_Brochure__r.Name,
                                                                                                        VGA_Car_Configurator_Downloads__c,VGA_Display_Live_not__c,
                                                                                                        VGA_Product__c,VGA_Product__r.Name,VGA_Search_Impressions__c,VGA_Social_Live_not__c,
                                                                                                        VGA_Web_Traffic__c from VGA_Campaign_Activity_Detail__c where VGA_Campaign_Activity_Header__c=: lstobjCampaign[0].id limit 999]);
                if(brand == 'Skoda')
                {                                                                                       
                    if(listofCampaign !=Null && listofCampaign.size()>0)
                    {
                        //list Add into wrapper object
                        for(VGA_Campaign_Activity_Detail__c objCampaign :listofCampaign)
                        {  
                            VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct();
                            objwrap.CarConfiguratorDownloads              =objCampaign.VGA_Car_Configurator_Downloads__c;
                            objwrap.SearchImpressions                     =objCampaign.VGA_Search_Impressions__c; 
                            objwrap.WebTraffic                            =objCampaign.VGA_Web_Traffic__c;
                            objwrap.Display                               =objCampaign.VGA_Display_Live_not__c;
                            objwrap.Social                                =objCampaign.VGA_Social_Live_not__c;
                            objwrap.productName                           =objCampaign.VGA_Product__r.Name;
                            objwrap.productId                             =objCampaign.VGA_Product__c;
                            objwrap.Id                                    =objCampaign.Id;
                            wrapperlist.add(objwrap); 
                        }        
                    }
                     else
                     {              
                        //list of Active product accord to brand and Sub-Brand.
                        list<product2>listofProduct =new list<product2>([select Id,Name from product2 where Family LIKE : subbrandvalue and 
                                                                         VGA_Brand__c=:brand limit 999]);
                        if(listofProduct !=Null && listofProduct.size()>0)
                        {
                            //list Add into wrapper object
                            for(product2 objProduct :listofProduct)
                            {
                                VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct(); 
                                objwrap.CarConfiguratorDownloads               =0;
                                objwrap.SearchImpressions                     =0; 
                                objwrap.WebTraffic                            =0;
                                objwrap.Display                               ='';
                                objwrap.Social                                ='';
                                objwrap.productName                           =objProduct.Name;
                                objwrap.productId                             =objProduct.Id;
                                wrapperlist.add(objwrap);  
                            }
                        }
                     }
            }
            else
            {
                 if(listofCampaign !=Null && listofCampaign.size()>0)
                    {
                        //list Add into wrapper object
                        for(VGA_Campaign_Activity_Detail__c objCampaign :listofCampaign)
                        {  
                            VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct();
                            objwrap.CarConfiguratorDownloads              =objCampaign.VGA_Car_Configurator_Downloads__c;
                            objwrap.SearchImpressions                     =objCampaign.VGA_Search_Impressions__c; 
                            objwrap.WebTraffic                            =objCampaign.VGA_Web_Traffic__c;
                            objwrap.Display                               =objCampaign.VGA_Display_Live_not__c;
                            objwrap.Social                                =objCampaign.VGA_Social_Live_not__c;
                            objwrap.productName                           =objCampaign.VGA_Active_Brochure__r.Name;
                            objwrap.productId                             =objCampaign.VGA_Active_Brochure__c;
                            objwrap.Id                                    =objCampaign.Id;
                            wrapperlist.add(objwrap); 
                        }        
                    }
                    else
                    {
                     list<VGA_Active_Brochure__c>listofProduct =new list<VGA_Active_Brochure__c>([select Id,Name,VGA_Dealer_Traffic_Active__c from VGA_Active_Brochure__c 
                                                                                           where VGA_Sub_Brand__c includes (:subbrandvalue)  
                                                                                             and VGA_Brand__c=: brand and
                                                                                            VGA_Dealer_Traffic_Active__c=true limit 999]);
                        if(listofProduct !=Null && listofProduct.size()>0)
                        {
                            //list Add into wrapper object
                            for(VGA_Active_Brochure__c objProduct :listofProduct)
                            {
                                VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct(); 
                                objwrap.CarConfiguratorDownloads               =0;
                                objwrap.SearchImpressions                     =0; 
                                objwrap.WebTraffic                            =0;
                                objwrap.Display                               ='';
                                objwrap.Social                                ='';
                                objwrap.productName                           =objProduct.Name;
                                objwrap.productId                             =objProduct.Id;
                                wrapperlist.add(objwrap);  
                            }
                        }                                                                   
                    }
            }
          }
      }
      else
      {
           for(VGA_Campaign_Activity_Header__c objCampaign :lstobjCampaign)
           {
              VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct();
              objwrap.objCampaignActivity                   = objCampaign;
              wrapperlist.add(objwrap);  
           }
            if(brand == 'Skoda')
            {
            //list of Active product accord to brand and Sub-Brand.
            list<product2>listofProduct =new list<product2>([select Id,Name from product2 where  VGA_Dealer_Traffic_Active__c=true and 
                                                             VGA_Brand__c=:brand limit 999]);
            if(listofProduct !=Null && listofProduct.size()>0)
                {
                    //list Add into wrapper object
                    for(product2 objProduct :listofProduct)
                    {
                        VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct(); 
                        objwrap.CarConfiguratorDownloads               =0;
                        objwrap.SearchImpressions                     =0; 
                        objwrap.WebTraffic                            =0;
                        objwrap.Display                               ='';
                        objwrap.Social                                ='';
                        objwrap.productName                           =objProduct.Name;
                        objwrap.productId                             =objProduct.Id;
                        wrapperlist.add(objwrap);  
                    }
                }
            }
            else
            {
                list<VGA_Active_Brochure__c>listofProduct =new list<VGA_Active_Brochure__c>([select Id,Name,VGA_Dealer_Traffic_Active__c from VGA_Active_Brochure__c 
                                                                                           where VGA_Sub_Brand__c includes (:subbrandvalue)  
                                                                                             and VGA_Brand__c=: brand and
                                                                                            VGA_Dealer_Traffic_Active__c=true limit 999]);
                        if(listofProduct !=Null && listofProduct.size()>0)
                        {
                            //list Add into wrapper object
                            for(VGA_Active_Brochure__c objProduct :listofProduct)
                            {
                                VGA_WrapperofProduct objwrap                  =new VGA_WrapperofProduct(); 
                                objwrap.CarConfiguratorDownloads               =0;
                                objwrap.SearchImpressions                     =0; 
                                objwrap.WebTraffic                            =0;
                                objwrap.Display                               ='';
                                objwrap.Social                                ='';
                                objwrap.productName                           =objProduct.Name;
                                objwrap.productId                             =objProduct.Id;
                                wrapperlist.add(objwrap);  
                            }
                        }
            }
      }
                                                                                                     
    if(wrapperlist !=Null && wrapperlist.size()>0)
    {
        return wrapperlist;
    }
    
    return wrapperlist;
}  
// -----------------------------------------------------------------------------------------------------------------
// This method is used to save Campaign_Activity_Detail Information .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string ,string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         5-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static string saveCampaignInformation(string CampaignDetails,string brand,string subbrand)
{ 
    
    List<VGA_WrapperofProduct> listofCampaignDetails = (List<VGA_WrapperofProduct>)JSON.deserialize(CampaignDetails,List<VGA_WrapperofProduct>.class);
    list<VGA_Campaign_Activity_Detail__c>listofCampaign =new list<VGA_Campaign_Activity_Detail__c>();
    if( listofCampaignDetails !=Null && listofCampaignDetails.size()>0)
    {
      try
        {
            VGA_Campaign_Activity_Header__c ObjCampaignHeader = createCampaignheader(listofCampaignDetails,brand,subbrand);
            upsert ObjCampaignHeader;
            for(VGA_WrapperofProduct objWrapper : listofCampaignDetails)
            {  
                if(objWrapper.productId !=Null)
                {
                 VGA_Campaign_Activity_Detail__c objCampaign =createCampaign(objWrapper.WebTraffic,objWrapper.CarConfiguratorDownloads,
                                                                            objWrapper.SearchImpressions,objWrapper.Social,
                                                                            objWrapper.Display,ObjCampaignHeader.Id,
                                                                            objWrapper.productId,objWrapper.Id,brand
                                                                           );
                    listofCampaign.add(objCampaign);
                }
                
            }
             if(listofCampaign!=Null && listofCampaign.size()>0)
             {
                 upsert listofCampaign;
                 return 'Campaign Details saved';
             }
           else
           {
              return 'Campaign Details saved'; 
           }
        }
        catch(exception e)
        {
            return 'Error: ' + e.getMessage();
        }
        
}
    return null;
}
// -----------------------------------------------------------------------------------------------------------------
// This method is used to save Campaign_Activity_Detail Information .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string ,string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         5-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static VGA_Campaign_Activity_Detail__c createCampaign(Decimal webtraffic ,Decimal carconfig,Decimal searchImp,
                                                             string social,string display,string recId,string productId,
                                                             string recordId,string Brand)
{
    VGA_Campaign_Activity_Detail__c ObjCampaign                  =New VGA_Campaign_Activity_Detail__c();
    ObjCampaign.VGA_Campaign_Activity_Header__c         =recId;
    ObjCampaign.VGA_Car_Configurator_Downloads__c       =carconfig;
    ObjCampaign.VGA_Display_Live_not__c                 =display;
    ObjCampaign.VGA_Search_Impressions__c               =searchImp;
    if(Brand =='Skoda')
        {
        ObjCampaign.VGA_Product__c                          =productId;
        }
        else
        {
        ObjCampaign.VGA_Active_Brochure__c                  =productId;
        }
    ObjCampaign.VGA_Web_Traffic__c                      =webtraffic;
    ObjCampaign.Id                                      =recordId ;
    ObjCampaign.VGA_Social_Live_not__c                  =social;
    return ObjCampaign;
    
}
// -----------------------------------------------------------------------------------------------------------------
// This method is used to save Campaign_Activity_Detail Information .
// -----------------------------------------------------------------------------------------------------------------
// INPUT PARAMETERS:    - string ,string
// -----------------------------------------------------------------------------------------------------------------
// Version#     Date                       Author                          Description
// -----------------------------------------------------------------------------------------------------------------
// 1.0         5-oct-2017          surabhi Ranjan               Initial Version
// -----------------------------------------------------------------------------------------------------------------
@AuraEnabled
public static VGA_Campaign_Activity_Header__c createCampaignheader(list<VGA_WrapperofProduct> lstobjCampaign,string Brand ,string subbrand)
{
    VGA_Campaign_Activity_Header__c ObjCampaign                  =  New VGA_Campaign_Activity_Header__c();
    ObjCampaign.VGA_Above_the_line_support__c                    =  lstobjCampaign[0].objCampaignActivity.VGA_Above_the_line_support__c;
    ObjCampaign.VGA_Above_the_line_support_Details__c            =  lstobjCampaign[0].objCampaignActivity.VGA_Above_the_line_support_Details__c;
    ObjCampaign.VGA_Brand__c                                     =  Brand  ; 
    ObjCampaign.VGA_Car_Configurator_Downloads__c                =  lstobjCampaign[0].objCampaignActivity.VGA_Car_Configurator_Downloads__c;
    ObjCampaign.VGA_CRM__c                                       =  lstobjCampaign[0].objCampaignActivity.VGA_CRM__c;
    ObjCampaign.VGA_CRM_Details__c                               =  lstobjCampaign[0].objCampaignActivity.VGA_CRM_Details__c;
    ObjCampaign.VGA_Display__c                                   =  lstobjCampaign[0].objCampaignActivity.VGA_Display__c;
    ObjCampaign.VGA_Display_Details__c                           =  lstobjCampaign[0].objCampaignActivity.VGA_Display_Details__c;
    ObjCampaign.VGA_Search_Impressions__c                        =  lstobjCampaign[0].objCampaignActivity.VGA_Search_Impressions__c;
    ObjCampaign.VGA_Sub_Brand__c                                 =  subbrand;
    ObjCampaign.VGA_Social__c                                    =  lstobjCampaign[0].objCampaignActivity.VGA_Social__c;
    ObjCampaign.VGA_Social_Details__c                            =  lstobjCampaign[0].objCampaignActivity.VGA_Social_Details__c;
    ObjCampaign.VGA_Web_Traffic__c                               =  lstobjCampaign[0].objCampaignActivity.VGA_Web_Traffic__c;
    ObjCampaign.VGA_Welcome_Optimization_Details__c              =  lstobjCampaign[0].objCampaignActivity.VGA_Welcome_Optimization_Details__c;
    ObjCampaign.Id                                               =  lstobjCampaign[0].objCampaignActivity.ID;
    return ObjCampaign;
    
}
}