/**
*@author Surabhi Ranjan
*@date 07/11/2017
*@description Coverage for VGA_InvalidEmailHandler 86%
*----------------------------
*@Update on 07/05/2019 By Hijith NS
*@description  Added 3 More test methods (Test 4 to 6 )
*/
@isTest(seeAllData = false)    
public class VGA_InvalidEmailHandlerTracker 
{
    @TestSetup
    public static void buildConfigList()
    {
        Asset objAsset,objAsset1,objAsset2;
        Product2 objProduct,objProduct1,objProduct2;
        
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Asset',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Account',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Contact',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Product2',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('VGA_Invalid_Email__c',true));
        insert CustomList;
        
        Account  objAccount = VGA_CommonTracker.buildPersonAccount('Volkswagen Individual Customer','Test');
        insert objAccount;
        
        List<Product2> ProcudtList=new List<Product2>();
        objProduct = VGA_CommonTracker.createProduct('Test Product');
        objProduct.VGA_Brand__c='Volkswagen';
        objProduct.VGA_Sub_brand__c='PV';
        ProcudtList.add(objProduct);
        objProduct1 = VGA_CommonTracker.createProduct('Test Product');
        objProduct1.VGA_Brand__c='Skoda';
        objProduct1.VGA_Sub_brand__c='PV';
        ProcudtList.add(objProduct1);
        objProduct2 = VGA_CommonTracker.createProduct('Test Product');
        objProduct2.VGA_Brand__c='Volkswagen';
        objProduct2.VGA_Sub_brand__c='CV';
        ProcudtList.add(objProduct2);
        
        insert ProcudtList;
        
        objAsset = VGA_CommonTracker.buildVehicle('TEST',objAccount.id);
        objAsset.Product2Id = ProcudtList[0].id;
        insert objAsset;
        
        objAsset1 = VGA_CommonTracker.buildVehicle('TEST2',objAccount.id);
        objAsset1.Product2Id = ProcudtList[1].id;
        insert objAsset1;
        
        objAsset2 = VGA_CommonTracker.buildVehicle('TEST3',objAccount.id);
        objAsset2.Product2Id = ProcudtList[2].id;
        insert objAsset2;
    }
    
    public static testMethod void Test1()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];
        
        VGA_Customer_Order_Details__c objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount.Id;
        objCOD.VGA_Order_Date__c = system.today();
        insert objCOD; 
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        insert objEmail;
    }
    public static testMethod void Test2()
    {
        
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];
        
        VGA_Ownership__c objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount.Id;
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership; 
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        insert objEmail;
    }
    public static testMethod void Test3()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];        
        
        VGA_Ownership__c objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount.Id;
        objOwnership.VGA_Purchase_Date__c = system.today();
        insert objOwnership; 
        
        VGA_Customer_Order_Details__c objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount.Id;
        objCOD.VGA_Order_Date__c = system.today();
        insert objCOD; 
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        insert objEmail;
    }
    
    public static testMethod void Test6()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];        
        
        VGA_Ownership__c objOwnership = new VGA_Ownership__c();
        objOwnership.VGA_Owner_Name__c = objAccount.Id;
        Date NewDate = system.today();
        objOwnership.VGA_Purchase_Date__c = NewDate;
        insert objOwnership; 
        
        VGA_Customer_Order_Details__c objCOD = new VGA_Customer_Order_Details__c();
        objCOD.VGA_Customer__c = objAccount.Id;        
        objCOD.VGA_Order_Date__c = NewDate.addDays(2);
        
        insert objCOD; 
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        insert objEmail;
    }
    /**
    * @author Hijith NS
    * @date 2019-05-07
    * @description test Method to test the setBrand Method 
    *                   Update  the field VGA_Brand__c with value 'VW Passenger' when Brand= Volkswagen and sub-brand =PV 
    * 
    */
    @isTest
    public static void Test4_TestForVWPassenger()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];
        Asset objAsset = [select id,name,vga_vin__c from asset where vga_vin__c='TEST'];
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_name__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        objEmail.VGA_VIN__c = objAsset.VGA_VIN__c;
        insert objEmail;
        
        VGA_Invalid_Email__c emailObj = [select id,name,vga_brand__c,VGA_VIN__c from VGA_Invalid_Email__c where VGA_VIN__c =: objAsset.VGA_VIN__c ];
        system.assertEquals('VW Passenger', emailObj.vga_brand__c);
    }
    
     /**
    * @author Hijith NS
    * @date 2019-05-07
    * @description test Method to test the setBrand Method 
    *                   Update  the field VGA_Brand__c with value 'VW Passenger' when Brand= Volkswagen and sub-brand =CV 
    * 
    */
    @isTest
    public static void Test5_TestForVWCommercial()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];
        Asset objAsset1 = [select id,name,vga_vin__c from asset where vga_vin__c='TEST3'];
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        objEmail.VGA_VIN__c = objAsset1.VGA_VIN__c;
        insert objEmail;
        VGA_Invalid_Email__c emailObj = [select id,name,vga_brand__c,VGA_VIN__c from VGA_Invalid_Email__c where VGA_VIN__c =: objAsset1.VGA_VIN__c ];
        system.assertEquals('VW Commercial', emailObj.vga_brand__c);
    }
    /**
    * @author Hijith NS
    * @date 2019-05-07
    * @description test Method to test the setBrand Method 
    *                   Update  the field VGA_Brand__c with value 'Skoda' when Brand = Skoda
    * 
    */
    @isTest
    public static void Test6_TestForSkoda()
    {
        Account objAccount  = [select id,name from account where LastName='Test' limit 1];
        Asset objAsset2 = [select id,name,vga_vin__c from asset where vga_vin__c='TEST2'];
        
        VGA_Invalid_Email__c objEmail =new VGA_Invalid_Email__c();
        objEmail.VGA_First_nAME__C ='32323';
        objEmail.VGA_Account__c = objAccount.Id;
        objEmail.VGA_Dealer_Code__c = null;
        objEmail.VGA_Dealer_Name__c = null;
        objEmail.VGA_VIN__c = objAsset2.VGA_VIN__c;
        insert objEmail;
        VGA_Invalid_Email__c emailObj = [select id,name,vga_brand__c,VGA_VIN__c from VGA_Invalid_Email__c where VGA_VIN__c =: objAsset2.VGA_VIN__c ];
        system.assertEquals('Skoda', emailObj.vga_brand__c);
    }
}