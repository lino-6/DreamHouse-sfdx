public class VGA_ModelHandler
{

 public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<Product2>) trigger.new);
        }
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<Product2>) trigger.new, (Map<Id, Product2>) trigger.OldMap);
        }
     }
     
    public void onBeforeInsert(List<Product2> lstTriggerNew)
    {
      
        PopulateActiveBrochureField(lstTriggerNew,null);
        
    }
      public void onBeforeUpdate(List<Product2> lstTriggerNew, Map<Id,Product2> triggeroldmap)
    {
       PopulateActiveBrochureField(lstTriggerNew,triggeroldmap);
       
     }
  
  
   /**
     * @author Ganesh M
     * @date 05/12/2018
     * @description It will update the ActiveBrochure on Model based on Model Alias.
     * @param  lsttriggernew is a collection of new Model records
     
     */  
   Private static void PopulateActiveBrochureField(list<Product2> lsttriggernew, map<Id,Product2> triggeroldmap)
      {
       set<string> ModelAliasSet=new set<string>();
       Map<string,id> ActiveBrochureMap=new Map<string,id>();
       
       
       for(Product2 objProduct: lsttriggernew)
       {
         if(objProduct.VGA_Model_Alias__c != null || (objProduct.VGA_Model_Alias__c!=null && objProduct.VGA_Model_Alias__c != triggeroldmap.get(objProduct.id).VGA_Model_Alias__c))
         ModelAliasSet.add(objProduct.VGA_Model_Alias__c.touppercase());        
       }
       
        for(VGA_Active_Brochure__c Brochureobj :[select id,name,VGA_Brochure_Label__c from VGA_Active_Brochure__c where VGA_Brochure_Label__c in :ModelAliasSet])
            {
              ActiveBrochureMap.put(Brochureobj.VGA_Brochure_Label__c.touppercase(),Brochureobj.id);
            } 
       
        for(Product2 product: lsttriggernew)
          {
          if(product.VGA_Model_Alias__c != null ){
           if(ActiveBrochureMap.containskey(product.VGA_Model_Alias__c.touppercase()) && ActiveBrochureMap.get(product.VGA_Model_Alias__c.touppercase()) != null)
            {
            product.VGA_Active_Brochure__c = ActiveBrochureMap.get(product.VGA_Model_Alias__c.touppercase());
            }       
          }
          }
        }     
         
}