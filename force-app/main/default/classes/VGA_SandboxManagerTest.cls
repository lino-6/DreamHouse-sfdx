@isTest
public class VGA_SandboxManagerTest {
/*
 To set trigger custom settings to true
*/
    @TestSetup
    public static void buildConfigList()
    {
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Lead';
        objCustom.VGA_Is_Active__c= true;
        CustomList.add(objCustom);
        VGA_Triggers__c objCustom1 = new VGA_Triggers__c();
        objCustom1.Name = 'Account';
        objCustom1.VGA_Is_Active__c= true;
        CustomList.add(objCustom1);
        VGA_Triggers__c objCustom2 = new VGA_Triggers__c();
        objCustom2.Name = 'Contact';
        objCustom2.VGA_Is_Active__c= true;
        CustomList.add(objCustom2);
        VGA_Triggers__c objCustom3 = new VGA_Triggers__c();
        objCustom3.Name = 'Asset';
        objCustom3.VGA_Is_Active__c= true;
        CustomList.add(objCustom3);
        VGA_Triggers__c objCustom4 = new VGA_Triggers__c();
        objCustom4.Name = 'case';
        objCustom4.VGA_Is_Active__c= true;
        CustomList.add(objCustom4);
        insert CustomList;
    }
/**
* @author Hijith NS
* @date 21/March/2019
* @description Test method for the class VGA_SandboxManager..
*/
    @isTest
    static void testSandboxPostCopyScript() 
    { 
        Test.startTest();
        
        VGA_SandboxUserInfo__c SandboxInfo = new VGA_SandboxUserInfo__c();
        SandboxInfo.VGA_Email__c = 'hijith.n@katzion.com' ;
        SandboxInfo.VGA_FirstName__c = 'Hijith' ;
        SandboxInfo.VGA_LastName__c = 'N S' ;
        SandboxInfo.Name = 'hijithDev2';
        insert SandboxInfo;
        
        Test.testSandboxPostCopyScript(
            new VGA_SandboxManager(),
            UserInfo.getOrganizationId(),
            UserInfo.getOrganizationId(),
            'hijithDev2'
        ); 
        List<Case> intLlist = new  List<Case>();
        intLlist = [select id,subject from case];
        system.debug(intLlist);
        Test.stopTest();
    }
}