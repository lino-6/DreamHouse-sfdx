// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          01-jan-2018      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to VGA_TaskTrigger
public class VGA_TaskHandler 
{
    public void runTrigger()  
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<Task>) trigger.new);     
        }     
        if(Trigger.isAfter && Trigger.isInsert)
        {
          onAfterInsert((List<Task>) trigger.new);
        }
       
    }
    // Method calls all the methods required to be executed before insert
    public void onBeforeInsert(List<Task> lstTriggerNew)
    {
        checkBoxUpdate(lstTriggerNew,null);
        updateDealerName(lstTriggerNew,null);
        // update Dealer
        updateDealer(lstTriggerNew,null);
    }
    public void onAfterInsert(List<Task> lstTriggerNew)
    {
        deleteExistingTasks(lstTriggerNew);
    }
     
         
    // This method is used update checkBox on Task.
    public void checkBoxUpdate(list<Task> lsttriggernew,map<id,Task> triggeroldmap)
    {
       set<String>mapofContact =new set<String>();
       map<String,string>setofcontact =new map<String,string>();
       for(Task Taskobj : lsttriggernew)
        {
            if(Taskobj.whoId!=Null && Taskobj.whoId.getsobjecttype() == Contact.sobjecttype 
              && (Taskobj.whatid == null || (Taskobj.whatid != null 
              && Taskobj.whatid.getsobjecttype() != Case.sobjecttype)))
            {
               mapofContact.add(Taskobj.whoId);   
            }
        }
        if(mapofContact!=Null)
        {
          list<Contact>listobjContact =New list<Contact>([select Id,RecordType.Name,Email  from Contact where Id IN: mapofContact]);
          
          if(listobjContact  !=Null && listobjContact.size()>0)
          {
            for(Contact ObjContact :listobjContact)
            {
          if(ObjContact.RecordType.Name =='Volkswagen Dealer Contact' || ObjContact.RecordType.Name =='Skoda Dealer Contact')
            {
               setofcontact.put(ObjContact.Id,ObjContact.Email); 
            }
            }
           
          }
        }
        if(setofcontact != Null && !(setofcontact.isEmpty()))
        {
         for(Task Taskobj : lsttriggernew)
         {
             if(Taskobj.whoId!=Null && Taskobj.whoId.getsobjecttype() == Contact.sobjecttype 
               && (Taskobj.whatid == null || (Taskobj.whatid != null 
              && Taskobj.whatid.getsobjecttype() != Case.sobjecttype)))
             {
               Taskobj.VGA_Email_Notification__c=true;
               Taskobj.VGA_Contact_Email__c=setofcontact.get(Taskobj.whoId);
               
             }
         }
        }
    }
    public void updateDealerName(list<Task> lsttriggernew,map<id,Task> triggeroldmap)
    {
       set<String>mapofContact =new set<String>();
       map<String,string>setofcontact =new map<String,string>();
       for(Task Taskobj : lsttriggernew)
        {
            if(Taskobj.whoId!=Null && Taskobj.whoId.getsobjecttype() == Contact.sobjecttype)
            {
               mapofContact.add(Taskobj.whoId);   
            }
        }
        if(mapofContact!=Null)
        {
          list<Contact>listobjContact =New list<Contact>([select Id,Name,RecordType.Name,Email  from Contact where Id IN: mapofContact]);
          
          if(listobjContact  !=Null && listobjContact.size()>0)
          {
            for(Contact ObjContact :listobjContact)
            {
                if(ObjContact.RecordType.Name =='Volkswagen Dealer Contact' || ObjContact.RecordType.Name =='Skoda Dealer Contact')
                {
                   setofcontact.put(ObjContact.Id,ObjContact.Name); 
                }
            }
          }
        }
        if(setofcontact != Null && !(setofcontact.isEmpty()))
        {
         for(Task Taskobj : lsttriggernew)
         {
             if(Taskobj.whoId!=Null && Taskobj.whoId.getsobjecttype() == Contact.sobjecttype )
             {
               Taskobj.VGA_Dealer_Contact__c=setofcontact.get(Taskobj.whoId);  
             }
         }
        }
    }
    
    public void updateDealer(list<Task> lsttriggernew,map<id,Task> triggeroldmap)
    {
       set<String>mapofaccount =new set<String>();
       map<String,string>setofaccount =new map<String,string>();
       //string tid = 
       for(Task Taskobj : lsttriggernew)
        {
        string tid =Taskobj.whatid;
            if(tid !=Null && tid.startsWith('001') && Taskobj.whatid.getsobjecttype() == account.sobjecttype)
            {
               mapofaccount.add(Taskobj.whatid);   
            }
        }
        if(mapofaccount!=Null)
        {
          list<account>listobjaccount =New list<account>([select Id,Name,RecordType.Name from account where Id IN: mapofaccount]);
          
          if(listobjaccount  !=Null && listobjaccount.size()>0)
          {
            for(account Objaccount :listobjaccount)
            {
                if(Objaccount.RecordType.Name =='Volkswagen Dealer account' || Objaccount.RecordType.Name =='Skoda Dealer account')
                {
                   setofaccount.put(Objaccount.Id,Objaccount.Name); 
                }
            }
          }
        }
        if(setofaccount!= Null && !(setofaccount.isEmpty()))
        {
         for(Task Taskobj : lsttriggernew)
         {
         string tid =Taskobj.whatid;
             if(tid !=Null && tid.startsWith('001') && Taskobj.whatid.getsobjecttype() == account.sobjecttype )
             {
               Taskobj.VGA_Dealer__c=setofaccount.get(Taskobj.whatid);  
             }
         }
        }
    }
    
    
    private static void deleteExistingTasks(list<Task> lsttriggernew)
    {
      if(lsttriggernew != null && !lsttriggernew.isEmpty())
      {
        set<Id> setCaseID = new set<Id>();
        set<Id> setTaskId = new set<Id>();
        for(Task objTask : lsttriggernew)
        {
          if(objTask.whatid != null && objTask.subject != null && 
            objTask.whatid.getsobjecttype() == Case.sobjecttype
            && objTask.subject.equalsignoreCase('New Interaction Assigned'))
          {
            setCaseID.add(objTask.whatID);
            setTaskId.add(objTask.id);  
          }
        }
        if(setCaseID != null && !setCaseID.isEmpty())
        {
          list<Task> lstTasks = [select id,WhatId,subject from Task where whatid in:setCaseID and Id not in:setTaskId and subject='New Interaction Assigned'];
          
          if(lstTasks != null && !lstTasks.isEmpty())
          {
            map<id,list<Task>> mapofExistingTasks = new map<id,list<Task>>();
            list<Task> lstTask2Delete = new list<Task>();
            for(Task objTask : lstTasks)
            {
              if(!mapofExistingTasks.containskey(objTask.whatid))
                mapofExistingTasks.put(objTask.whatid,new list<Task>());
                mapofExistingTasks.get(objTask.whatid).add(objTask);
            }
            for(Task objTask : lsttriggernew)
            {
              if(objTask.whatid != null && objTask.subject != null && objTask.whatid.getsobjecttype() == Case.sobjecttype
                && objTask.subject.equalsignoreCase('New Interaction Assigned') && mapofExistingTasks.containskey(objTask.whatid) 
                && mapofExistingTasks.get(objTask.whatid).size()>0)
            {
              for(Task objTaskstoDelete : mapofExistingTasks.get(objTask.whatid))
              {
                lstTask2Delete.add(objTaskstoDelete);
              }  
            }
            }
            if(lstTask2Delete != null && !lstTask2Delete.isEmpty())
            {
              delete lstTask2Delete;
            }
          }
        }
      }
    }
}