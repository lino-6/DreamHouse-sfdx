public with sharing class VGA_OwnershipTriggerhandler 
{
    public void runTrigger()
    {
        if (Trigger.isAfter && Trigger.isInsert)
        {
            onAfterInsert((List<VGA_Ownership__c>) trigger.new);  
        }
        
        if (Trigger.isAfter && Trigger.isUpdate)
        {
            onAfterUpdate((List<VGA_Ownership__c>) trigger.new, (Map<Id, VGA_Ownership__c>) trigger.OldMap); 
        }
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<VGA_Ownership__c>) trigger.new);     
        }
        if (Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<VGA_Ownership__c>) trigger.new, (Map<Id, VGA_Ownership__c>) trigger.OldMap);    
        }
    }
    public void onBeforeInsert(List<VGA_Ownership__c> lstTriggerNew)
    {
        updateEmailValidCheckBox(lstTriggerNew,null);
        updateUsedCarCheckBox(lstTriggerNew,null);
        
    }
    public void onBeforeUpdate(List<VGA_Ownership__c> lstTriggerNew,Map<Id,VGA_Ownership__c> mapTriggerOld)
    {
         updateUsedCarCheckBox(lstTriggerNew,mapTriggerOld);
    }
    public void onafterInsert(List<VGA_Ownership__c> triggerNew) 
    {
        updateAllianz(triggerNew,Null);
        updateowneronAccount(triggerNew,Null);
        tagBBO(triggerNew);
       
    }
    public void onAfterUpdate(List<VGA_Ownership__c> triggerNew,map<id,VGA_Ownership__c> triggeroldmap)
    {
        //Updated by Alfahad on 2018-08-15
        //We should not update VGA_Allianz_Data_Mapping__c on update trigger
        //updateAllianz(triggerNew,triggeroldmap);
      
     }
    
    public void tagBBO(List<VGA_Ownership__c> triggerNew)
    {
        Set<Id> account_id = new Set<Id>();
        
        for(VGA_Ownership__c v : triggerNew)
            account_id.add(v.VGA_Owner_Name__c);
        
        List<Account> account_lst = [SELECT Id, VGA_BBO_Tag_Payload__c FROM Account WHERE isPersonAccount = true AND VGA_Brand__c != 'Skoda' AND VGA_Promotions_Flag__c  = true AND Id = :account_id];
        for(Account obj_act : account_lst)
        {
            for(VGA_Ownership__c obj : triggerNew)
            {
                    VGA_BBOService bbo = new VGA_BBOService(obj_act.VGA_BBO_Tag_Payload__c == null ? '{}' : obj_act.VGA_BBO_Tag_Payload__c);
                    VGA_BBOCustomer customer_obj = bbo.getCustomer();
                    VGA_BBOLead bbo_lead = bbo.getLead();   
                    Integer cusi = null;
                
                    if(customer_obj == null)
                    {
                        customer_obj = new VGA_BBOCustomer();
                    }
                    else {
                        cusi = customer_obj.cusi;
                    }
                
                    customer_obj.customer_type = VGA_BBOCustomer.types.UNKNOWN;
                    customer_obj.camo = obj.VGA_Model_Alias__c;
                    customer_obj.pryr = obj.VGA_VIN__r.VGA_Model_Year__c != null ? Integer.valueOf(obj.VGA_VIN__r.VGA_Model_Year__c) : null; 
                    customer_obj.camp = null;
                    customer_obj.puyr = obj.VGA_Purchase_Date__c == null ? null : obj.VGA_Purchase_Date__c.year();
                    customer_obj.cusi = cusi == null ? customer_obj.puyr : cusi;
                    customer_obj.nesd = null;         
                
                    bbo.setData(customer_obj, bbo_lead, obj_act.id);
                    BBOEncrypt(bbo.getPayload());
            }
        }
    }
    
    @future(callout=true)
    public static void BBOEncrypt(String payload)
    {
        VGA_BBOService bbo = new VGA_BBOService(payload);
        String encrypted_tag = bbo.encrypt();
        Account act = new Account(id = bbo.getAccount(), VGA_BBO_Tag_ID__c = encrypted_tag, VGA_BBO_Tag_Payload__c = payload);
        UPDATE act;
    }    
    
    private static void updateAllianz(list<VGA_Ownership__c> lsttriggernew,map<id,VGA_Ownership__c> triggeroldmap)
    {
    /*
        Modified By: Alfahad
        Modified Date: 2018-05-10
        Purpose: Fixed logic for calculating 'Current Sale Date' and 'Original Sale Date'
    */
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            
            set<string> setOfCurrentOwnerVins = new set<string>();            
            
            map<string, map<string, date>> sale_dates = new map<string, map<string, date>>();
            map<string, string> company_flag = new map<string, string>();
            
            for(VGA_Ownership__c objOwnership : lsttriggernew) {
                if(objOwnership.VGA_VIN_for_Mule__c != null && objOwnership.VGA_Purchase_Date__c != null ) {
                                            
                     
                    // Prepare a list of vins to store sale dates. We only consider current owner insert/update when calculating sale dates             
                    if(objOwnership.VGA_Type__c != null && objOwnership.VGA_Type__c == 'Current Owner') {
                        setOfCurrentOwnerVins.add(objOwnership.VGA_VIN_for_Mule__c);  

                                            
                        if(!sale_dates.containskey(objOwnership.VGA_VIN_for_Mule__c)) {
                            sale_dates.put(objOwnership.VGA_VIN_for_Mule__c, new map<string, date>());
                        }                       
                        
                        if(!sale_dates.get(objOwnership.VGA_VIN_for_Mule__c).containskey('Current Sale Date')) {
                            sale_dates.get(objOwnership.VGA_VIN_for_Mule__c).put('Current Sale Date', objOwnership.VGA_Purchase_Date__c);
                        }
                        
                        if(!sale_dates.get(objOwnership.VGA_VIN_for_Mule__c).containskey('Original Sale Date')) {
                            sale_dates.get(objOwnership.VGA_VIN_for_Mule__c).put('Original Sale Date', objOwnership.VGA_Purchase_Date__c);
                        }   
                        
                        if(!company_flag.containskey(objOwnership.VGA_VIN_for_Mule__c)) {                            
                            company_flag.put(objOwnership.VGA_VIN_for_Mule__c, (objOwnership.VGA_Customer_Type__c != null && objOwnership.VGA_Customer_Type__c == 'Person') ? 'N' : 'Y');
                        }                        
                        
                    }                        
                }                
                
            }
            
            
            if(setOfCurrentOwnerVins != null && !setOfCurrentOwnerVins.isEmpty()) {
            
                // Populate sale dates for each vin
                List<VGA_Ownership__c> all_ownership_records = [SELECT VGA_Purchase_Date__c, VGA_VIN_for_Mule__c, VGA_Type__c FROM VGA_Ownership__c WHERE VGA_VIN_for_Mule__c IN :setOfCurrentOwnerVins];
                
                
                for(VGA_Ownership__c ownership_record : all_ownership_records) {                        
                
                    if(ownership_record.VGA_Purchase_Date__c < sale_dates.get(ownership_record.VGA_VIN_for_Mule__c).get('Original Sale Date')) {
                        sale_dates.get(ownership_record.VGA_VIN_for_Mule__c).put('Original Sale Date', ownership_record.VGA_Purchase_Date__c);
                    }
            
                }
                
                list<VGA_Allianz_Data_Mapping__c> lst_ADM = [SELECT Id,VGA_VIN_Formula__c,VGA_Vehicle_Current_Sale_Date__c,VGA_Vehicle_Original_Sale_Date__c
                                                            FROM VGA_Allianz_Data_Mapping__c WHERE VGA_VIN_Formula__c IN :setOfCurrentOwnerVins];   
                                                            
                if(lst_ADM != null && !lst_ADM.isEmpty()) {

                    for(VGA_Allianz_Data_Mapping__c data_mapping : lst_ADM) {
                        data_mapping.VGA_Vehicle_Current_Sale_Date__c = sale_dates.get(data_mapping.VGA_VIN_Formula__c).get('Current Sale Date');
                        data_mapping.VGA_Vehicle_Original_Sale_Date__c = sale_dates.get(data_mapping.VGA_VIN_Formula__c).get('Original Sale Date');
                        data_mapping.VGA_Company_Flag__c = company_flag.get(data_mapping.VGA_VIN_Formula__c);
                        data_mapping.VGA_Last_Modified_Date_Time__c = system.now();
                        // data_mapping.VGA_Transaction_Code__c = 'C';
                    }
                    
                    update lst_ADM;
                }
                
                // Set selling dealer on Asset record
                List<Asset> assets = [SELECT Id, VGA_VIN__c FROM Asset WHERE VGA_VIN__c IN :setOfCurrentOwnerVins];
                
                if(assets != null && !assets.isEmpty()) {
                    for(Asset vehicle : assets) {
                        for(VGA_Ownership__c objOwnership : lsttriggernew) {
                            if(objOwnership.VGA_VIN_for_Mule__c != null && objOwnership.VGA_VIN_for_Mule__c == vehicle.VGA_VIN__c 
                               && objOwnership.VGA_Type__c != null && objOwnership.VGA_Type__c == 'Current Owner') {
                                vehicle.AssetProvidedById = objOwnership.VGA_Dealership__c;
                            }
                        } 
                    }
                    update assets;
                }
            }
                        
    
        }
    }    
    
    
   private static void updateowneronAccount(list<VGA_Ownership__c> lsttriggernew,map<id,VGA_Ownership__c> triggeroldmap)
     {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        { 
           map<Id,string> mapofAccount = new map<Id,string>();
           set<Id>setofAccountId = new set<Id>();
           list<Account>UpdatelistofAccount =new list<Account>();
           for(VGA_Ownership__c objOwnership : lsttriggernew)
           {
               if(objOwnership.VGA_Owner_Name__c !=Null && objOwnership.VGA_Type__c !=Null)
               {
                 mapofAccount.put(objOwnership.VGA_Owner_Name__c,objOwnership.VGA_Type__c);   
               } 
           }
          if(mapofAccount !=Null && mapofAccount.size()>0)
          {
              list<Account>listofAccount =New list<Account>([select Id from Account where Id IN: mapofAccount.keySet()]);
              if(listofAccount !=Null && listofAccount.size()>0)
              {
                 for(Account objAcc :listofAccount)
                  {
                         setofAccountId.add(objAcc.Id); 
                  }
              }
          }
        if(setofAccountId !=Null)
        {
             for(Id objId :setofAccountId)  
             {
                  Account objAcc =New Account();
                  objAcc.Id =objId;
                  if(mapofAccount.containsKey(objId) &&mapofAccount.get(objId) =='Current Owner')
                  {
                     objAcc.VGA_Owner__c =  'Yes';
                  }
                  else
                  {
                   objAcc.VGA_Owner__c =  'No';
                  }
                  
                  UpdatelistofAccount.add(objAcc);
             }
        }
        if(UpdatelistofAccount !=Null && UpdatelistofAccount.size()>0)
        {
               update UpdatelistofAccount;
               
        }
    }
    }
    private static void updateEmailValidCheckBox(list<VGA_Ownership__c> lsttriggernew,map<Id,VGA_Ownership__c> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            set<ID> setAccountID = new set<ID>();
            for(VGA_Ownership__c objCOD : lsttriggernew)
            {
                if(objCOD.VGA_Owner_Name__c != null)
                {
                    setAccountID.add(objCOD.VGA_Owner_Name__c);
                }
            }
            if(setAccountID != null && !setAccountID.isEmpty())
            {
                map<ID,Boolean> mapofCOD = new map<Id,Boolean>();
                for(Account objAccount : [select id,VGA_Email_Address_valid__c from Account where id in:setAccountID limit 49999])
                {
                    if(!mapofCOD.containskey(objAccount.id))
                        mapofCOD.put(objAccount.id,objAccount.VGA_Email_Address_valid__c);
                }
                if(mapofCOD != null && !mapofCOD.isEmpty())
                {
                    for(VGA_Ownership__c objCOD : lsttriggernew)
                    {
                        if(objCOD.VGA_Owner_Name__c != null && mapofCOD.containskey(objCOD.VGA_Owner_Name__c) 
                            && mapofCOD.get(objCOD.VGA_Owner_Name__c) != null)
                        {
                            objCOD.VGA_Email_Address_valid__c = mapofCOD.get(objCOD.VGA_Owner_Name__c); 
                        }
                    }
                }
            }
        }
    }
 private static void updateUsedCarCheckBox(list<VGA_Ownership__c> lsttriggernew,map<Id,VGA_Ownership__c> triggeroldmap)
    {
  
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
       
            set<ID> setAccountID = new set<ID>();
             Set<Id> AssetId =  new Set<Id>();
             map<Id,Date> mapofpurchaseDate = new map<Id,Date>();
              //list<VGA_Ownership__c> Updateownerlist  = new list<VGA_Ownership__c>();
             Id V_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Dealer Account').getRecordTypeId();
             Id S_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Dealer Account').getRecordTypeId();
             Id Vind_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
             Id Sind_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
            
            // Added by Ashok Chandra on 14-12-2018 #725
             Id Vwf_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
             Id Skf_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
             
            for(VGA_Ownership__c ownerobj : lsttriggernew)
               {
              
                if( ownerobj.VGA_Owner_Name__c != null  )
                { 
               
                  setAccountID.add(ownerobj.VGA_Previous_Owner__c);
                 AssetId.Add(ownerobj.VGA_VIN__c);
                  
                  }
               }
        
          Map<ID,VGA_Ownership__c> ownerlist = new Map<Id, VGA_Ownership__c>([select id,VGA_Owner_Name__c,VGA_Usedcar__c,VGA_Purchase_Date__c,VGA_Previous_Owner__c,VGA_Type__c,VGA_Owner_Name__r.recordtypeid  from VGA_Ownership__c where VGA_VIN__c=:AssetId
                                                                                      and VGA_Type__c = 'Previous Owner'  ]);
          
         
            for(VGA_Ownership__c ownership : lsttriggernew )
            {
           
              //mapofpurchaseDate.put(objCOD.id,objCOD.VGA_Purchase_Date__c);
              if(ownership.VGA_Previous_Owner__c!=null && ownership.VGA_Purchase_Date__c!=null && !ownerlist.isempty() && ownerlist != null)
              {
              
                   VGA_Ownership__c  ownerParent = ownerlist.get(ownership.VGA_Previous_Owner__c);
                    
                   
                 if( ownerParent != null && ownership.VGA_Owner_Name__c != null &&  (ownerParent.VGA_Owner_Name__r.recordtypeid == V_RTId || ownerParent.VGA_Owner_Name__r.recordtypeid == S_RTId)
                                                     && ownerParent.VGA_Purchase_Date__c!=null && ownership.VGA_Purchase_Date__c!=null    && (ownerParent.VGA_Purchase_Date__c).daysbetween(ownership.VGA_Purchase_Date__c)<=90  )
                {
               
                    
                              ownership.VGA_Usedcar__c = false;
                              ownership.VGA_Literature_Code__c = '';
                              
                   }
                                
                 
                else if(ownerParent != null && ownership.VGA_Owner_Name__c != null && (ownerParent.VGA_Owner_Name__r.recordtypeid == Vind_RTId || ownerParent.VGA_Owner_Name__r.recordtypeid == Sind_RTId  || ownerParent.VGA_Owner_Name__r.recordtypeid == Vwf_RTId || ownerParent.VGA_Owner_Name__r.recordtypeid == Skf_RTId))
                 {
                
                         ownership.VGA_Usedcar__c = true;
                       ownership.VGA_Literature_Code__c = 'SKOWEL004-A';
                 
                 } else if(ownerParent != null && ownership.VGA_Owner_Name__c != null   && (ownerParent.VGA_Owner_Name__r.recordtypeid == V_RTId || ownerParent.VGA_Owner_Name__r.recordtypeid == S_RTId)
                               && ownerParent.VGA_Purchase_Date__c!=null && ownership.VGA_Purchase_Date__c!=null  && (ownerParent.VGA_Purchase_Date__c).daysbetween(ownership.VGA_Purchase_Date__c)>90  )
                 {
                       
                       ownership.VGA_Usedcar__c = true;
                       ownership.VGA_Literature_Code__c = 'SKOWEL004-A';
                 
                 
                 } 
                 else if(ownerParent != null && ownership.VGA_Owner_Name__c != null && (ownerParent.VGA_Owner_Name__r.recordtypeid == V_RTId || ownerParent.VGA_Owner_Name__r.recordtypeid == S_RTId)
                                &&  ownerParent.VGA_Previous_Owner__c!=null )
                 {
                       
                       ownership.VGA_Usedcar__c = true;
                       ownership.VGA_Literature_Code__c = 'SKOWEL004-A';
                 
                 
                 } 
                
                }
               //Updateownerlist.add(objCOD);
            }
            
          
            }
            
}
}