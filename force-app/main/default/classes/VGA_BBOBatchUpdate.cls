global class VGA_BBOBatchUpdate implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts
{
    private Integer records_processed = 0;
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('start');        
        String query = 'SELECT Id FROM Account WHERE isPersonAccount = true AND VGA_Brand__c != \'Skoda\' AND VGA_BBO_Batch_Visited__c = null AND VGA_Email_Address_Valid__c = true AND VGA_Promotions_Flag__c  = true LIMIT 10000';
        return Database.getQueryLocator(query);
    }
 
    global void execute(Database.BatchableContext BC, List<Account> scope)
    {
        System.debug('execute');       
        
        Set<Id> account_set = (new Map<Id, Account>(scope)).keySet();
        Map<Id, VGA_BBOCustomer> customer_map = new Map<Id, VGA_BBOCustomer>();
        Map<Id, VGA_BBOLead> lead_map = new Map<Id, VGA_BBOLead>();
       
        List<VGA_Ownership__c> ownership_lst = [SELECT  Id, VGA_Owner_Name__c, VGA_Model_Alias__c, VGA_Purchase_Date__c, VGA_VIN__r.VGA_Model_Year__c, VGA_VIN__r.VGA_Used_Stock__c, 
                                                VGA_VIN__r.VGA_Vehicle_Use_Code__c, VGA_VIN__r.VGA_Vehicle_Use__c ,LastModifiedDate 
                                                FROM VGA_Ownership__c  c where vga_owner_name__c In :account_set order by Id DESC];
        
        for(VGA_Ownership__c obj : ownership_lst)
        {
            VGA_BBOCustomer customer_obj = customer_map.get(obj.VGA_Owner_Name__c);
            
            if(customer_obj == null)     
            {
                customer_obj = new VGA_BBOCustomer();
                customer_obj.customer_type = VGA_BBOCustomer.types.UNKNOWN;
                customer_obj.camo = obj.VGA_Model_Alias__c;
                customer_obj.pryr = obj.VGA_VIN__r.VGA_Model_Year__c != null ? Integer.valueOf(obj.VGA_VIN__r.VGA_Model_Year__c) : null; 
                customer_obj.camp = null;
                customer_obj.puyr = obj.VGA_Purchase_Date__c == null ? null : obj.VGA_Purchase_Date__c.year();
                customer_obj.cusi = customer_obj.puyr;
                customer_obj.nesd = null;
                
                customer_map.put(obj.VGA_Owner_Name__c, customer_obj);
            }
            else
            {
                customer_obj.cusi = obj.VGA_Purchase_Date__c == null ? null : obj.VGA_Purchase_Date__c.year();                   
            }
            
        }
          Set<Id> Lead_id = new Set<Id>();
          map<id,string> campaignLead = new map<id,string>();
        List<Lead> lead_lst = [SELECT Id, VGA_Customer_Account__c, VGA_Request_Type__c, VGA_Model_of_Interest__c, VGA_Campaign_Source__c, CreatedDate 
                               FROM Lead WHERE VGA_Customer_Account__c IN :account_set AND VGA_Request_Type__c != 'VWFS Parity Lead' ORDER BY Id DESC];
       for(Lead l : lead_lst )
       {
         
          Lead_id.add(l.id);
        }
        if(Lead_id.size()>0)
         {
           for(campaignmember cm : [select id,leadid,description,Campaign.Name,Campaign.description from campaignmember where leadid in :Lead_id])
           {
               
               campaignLead.put(cm.leadid,cm.Campaign.description);
              
            }
          }
        for(Lead obj : lead_lst)
        {
            VGA_BBOLead lead_obj = lead_map.get(obj.VGA_Customer_Account__c);
            
            if(lead_obj == null)     
            {
                lead_obj = new VGA_BBOLead();

                if(obj.VGA_Request_Type__c == 'Brochure Request')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.BROCHURE_LEAD_FORM;
                }
                else if(obj.VGA_Request_Type__c == 'Test Drive')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.TEST_DRIVE_LEAD_FORM;
                }
                else if(obj.VGA_Request_Type__c == 'Callback Request')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.CONTACT_LEAD_FORM;
                }         
                else if(obj.VGA_Request_Type__c == 'Registration of Interest')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.OTHER;
                } 
                else if(obj.VGA_Request_Type__c == 'Request for Information')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.INFO_REQUEST_FORM;
                }      
                else if(obj.VGA_Request_Type__c == 'Competition Entry')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.COMPETITION_REGISTRATION;
                }      
                else if(obj.VGA_Request_Type__c == 'VWFS Parity Lead')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.OTHER;
                }   
                else if(obj.VGA_Request_Type__c == 'Car Finder')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.OTHER;
                }                
                else if(obj.VGA_Request_Type__c == 'stock locator')
                {
                    lead_obj.lead_type = VGA_BBOLead.types.OTHER;
                }
                else {
                    lead_obj.lead_type = VGA_BBOLead.types.OTHER;
                }                
                    
                if(!string.isBlank(obj.VGA_Model_of_Interest__c))
                   {
                       List<String> models = obj.VGA_Model_of_Interest__c.split(',');
                       if(models.size() >= 1)
                           lead_obj.camo = models.get(0);
                   }
                
                lead_obj.pryr = obj.CreatedDate.date().year();
                //lead_obj.camp = obj.VGA_Campaign_Source__c;
                lead_obj.camp =   campaignLead.get(obj.id); 
                lead_obj.camp = obj.VGA_Campaign_Source__c;
                lead_obj.lddt = obj.CreatedDate.date();
                
                lead_map.put(obj.VGA_Customer_Account__c, lead_obj);
            }
            
        }
        
        VGA_BBOService bbo = new VGA_BBOService(null, null, null);

        for(Account obj : scope)        
        {
            bbo.setData(customer_map.get(obj.Id), lead_map.get(obj.Id), obj.id);
            obj.VGA_BBO_Tag_Payload__c = bbo.getPayload();
            if(customer_map.get(obj.Id) != null || lead_map.get(obj.Id) != null)
                obj.VGA_BBO_Tag_ID__c = bbo.encrypt();
            obj.VGA_BBO_Batch_Visited__c = 'Y';
            records_processed++;
        }
        
        UPDATE scope;
        
    }  
    
    global void finish(Database.BatchableContext BC)
    {
        System.debug('finish '+ records_processed);
        
       // VGA_Common.sendExceptionEmail('BBO Finished', 'BBO batch processing finished. ' + records_processed + ' record');
        
        if(records_processed > 0)
        {
            Datetime dt = system.now().addSeconds(10);
            
            String day = string.valueOf(dt.day());
            String month = string.valueOf(dt.month());
            String hour = string.valueOf(dt.hour());
            String minute = string.valueOf(dt.minute());
            String second = string.valueOf(dt.second());
            String year = string.valueOf(dt.year());
            
            String strJobName = 'BBO-Batch-Processing-' + String.valueof(dt);
            String strSchedule = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            
            if(!test.isRunningTest()) 
                System.schedule(strJobName, strSchedule, new VGA_BBOBatchScheduler());                                 
         
        }
        else
        {
            VGA_Common.sendExceptionEmail('BBO: Processing Complete', 'BBO batch process has completed processing all records.'); 
        }
        
        List<CronTrigger> lstCron = [SELECT Id FROM CronTrigger WHERE NextFireTime = Null AND State = 'DELETED' AND CronJobDetail.Name LIKE 'BBO-Batch-Processing-%' ];
        
        if(lstCron != Null && !lstCron.isEmpty())
        {
            try
            {
                for(CronTrigger obj : lstCron)
                {
                    system.abortjob(obj.Id);
                }
            }
            catch(Exception e)
            {
                system.debug('@@Exception in deleting job : ' + e.getMessage());
            }
        }         
    }
    
}