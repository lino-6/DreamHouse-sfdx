@isTest
private class VGA_UserProfileHandlerTest
{
    @testsetup
    Static void buildUserProfiledetails()
    {     
        List<VGA_Triggers__c> CustomTriggerList =new List<VGA_Triggers__c>();
        CustomTriggerList.add(VGA_CommonTracker.buildCustomSettingTrigger('Contact',true));
        CustomTriggerList.add(VGA_CommonTracker.buildCustomSettingTrigger('VGA_User_Profile__c',true));
        insert CustomTriggerList; 
        
        Account  objAccount = VGA_CommonTracker.createDealerAccount();
        Contact  objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        User     objUser =    VGA_CommonTracker.CreateUser(objContact.Id); 
        VGA_Dealership_Profile__c objDP = VGA_CommonTracker.createDealershipProfile(objAccount.id);
        
        
        
        VGA_User_Profile__c objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        objUserPro.name = 'passenger vehicles (pv)';
        objUserPro.VGA_Mobile__c = '1236598743';
        insert objUserPro;
        
    }  
    
    /**
    * @author Ganesh M
    * @date 07/12/2018
    * @description Method that tests Updating Available Lead field for Nominated users,System Should throw Error.
    */
    static testMethod void test01_UpdatingAvailable_field_BeforeUpdating() 
    {      
        
        Test.startTest();
        Account acc = [select id,VGA_Distribution_Method_PV_Report__c from account where VGA_Brand__c='Volkswagen' limit 1 ];
        contact objcontact = [select id,VGA_Brand__c from contact where VGA_Brand__c='Volkswagen' limit 1];
        User     objUser =   [select id,username,ContactId from user where username = 'tes2333t@test.com' limit 1];
        
        VGA_Dealership_Profile__c objDP1 = [select id,VGA_Distribution_Method__c,VGA_Nominated_Contact__c,VGA_Nominated_User__c from VGA_Dealership_Profile__c where VGA_Distribution_Method__c ='Round Robin' limit 1];
        
        objDP1.VGA_Nominated_Contact__c = objContact.id;
        objDP1.VGA_Distribution_Method__c ='Nominated User';
        objDP1.VGA_Nominated_User__c =objUser.id;
        
        update objDP1;
        
        
        VGA_User_Profile__c objUserPro = [select id,VGA_Sub_Brand__c,name,VGA_Contact__c,VGA_Mobile__c from VGA_User_Profile__c where VGA_Brand__c = 'Volkswagen' limit 1];
        objUserPro.VGA_Available__c = false;
        
        try {
            update objUserPro;
            
            System.assert(false, 'Exception expected');
            
        } catch (DMLException e) {
            System.assert(e.getMessage().contains('Cannot change available to receive leads as user is set to nominated user.'), 'message=' + e.getMessage());
        }
        
        Test.stopTest();
        
        
    }

    /**
    * @author Ganesh M
    * @date 09/01/2019
    * @description Method that tests Negitive Case, System should not throw any error while Updating Round Robin Method records.
    */
    static testMethod void test02_UpdatingAvailable_field_For_RoundRobin_BeforeUpdating() 
    {      
        
        Test.startTest();
        
        VGA_User_Profile__c objUserPro = [select id,VGA_Sub_Brand__c,name,VGA_Contact__c,VGA_Mobile__c from VGA_User_Profile__c where VGA_Brand__c = 'Volkswagen' limit 1];
        
        objUserPro.VGA_Available__c = false;
        
        update objUserPro;
        
        Test.stopTest();
        
        
    }
}