/**
* @author Archana Yerramilli
* @date 20/11/2018
* @description #552 GetDealer Laravel API
* @description Web service class to get PMA data in JSON Structure
* @param brand  
* @param subbrand
* https://uat-vga-live.cs6.force.com/carconfigurator/services/apexrest/PMA?postcode=4615&brand=Volkswagen&subbrand=pv
* https://uat-vga-live.cs6.force.com/carconfigurator/services/apexrest/PMA?postcode=4353&brand=Skoda&subbrand=cv
* https://uat-vga-live.cs6.force.com/carconfigurator/services/apexrest/PMA?postcode=9998&brand=Volkswagen&subbrand=pv
*/


@RestResource(urlMapping='/PMA/*')
global with sharing class VGA_getPMAAPI
{
@HttpGet
  global static void getPMA()
    {
           RestRequest req    = RestContext.request;
           RestResponse res   = RestContext.response;

           String brand       = req.params.get('brand');
           String subBrand    = req.params.get('subbrand');
           String postCode    = req.params.get('postcode');
        
          if(postcode!=null && postcode.length()==3)
          {
              postcode='0'+postcode;
          }
        
              
           list<VGA_Post_Code_Mapping__c>  pmaVGAVal=new list<VGA_Post_Code_Mapping__c>();
           list<VGA_Post_Code_Mapping__c>  pmaSkoVal=new list<VGA_Post_Code_Mapping__c>();
           list<VGA_Post_Code_Mapping__c>  pmaVgaCV=new list<VGA_Post_Code_Mapping__c>();
           if(brand == 'Volkswagen' && postCode != null && subBrand == 'PV')
           {
              pmaVGAVal = [SELECT ID , VGA_Post_Code__c ,
                                                VGA_VWP_Dealer_Code__c,
                                                VGA_VWP_Dealer_Code_2nd__c,
                                                VGA_VWP_Dealer_Code_3rd__c,
                                                VGA_VWP_Dealer_Code_4th__c,
                                                VGA_VWP_Dealer_Code_5th__c,
                                                VGA_VWP_Dealer__r.Name,
                                                VGA_VWP_Dealer_2nd__r.Name,
                                                VGA_VWP_Dealer_3rd__r.Name,
                                                VGA_VWP_Dealer_4th__r.Name,
                                                VGA_VWP_Dealer_5th__r.Name,
                                                VGA_VWP_Dealer__c,
                                                VGA_VWP_Dealer_2nd__c,
                                                VGA_VWP_Dealer_3rd__c,
                                                VGA_VWP_Dealer_4th__c,
                                                VGA_VWP_Dealer_5th__c
                                                FROM VGA_Post_Code_Mapping__c where VGA_Post_Code__c =: postCode ];
           }

           if(brand == 'Skoda' && postCode != null)
           {
             pmaSkoVal = [SELECT ID , VGA_Post_Code__c ,
                                                VGA_SKP_Dealer_Code__c,
                                                VGA_SKP_Dealer_Code_2nd__c,
                                                VGA_SKP_Dealer_Code_3rd__c,
                                                VGA_SKP_Dealer_Code_4th__c,
                                                VGA_SKP_Dealer_Code_5th__c,
                                                VGA_SKP_Dealer__r.Name,
                                                VGA_SKP_Dealer_2nd__r.Name,
                                                VGA_SKP_Dealer_3rd__r.Name,
                                                VGA_SKP_Dealer_4th__r.Name,
                                                VGA_SKP_Dealer_5th__r.Name
                                                FROM VGA_Post_Code_Mapping__c where VGA_Post_Code__c =: postCode ];
           }
           
           if(brand == 'Volkswagen' && postCode != null && subBrand == 'CV')
           {
              pmaVgaCV = [SELECT ID , VGA_Post_Code__c ,
                                                VGA_VWC_Dealer_Code__c,
                                                VGA_VWC_Dealer_Code_2nd__c,
                                                VGA_VWC_Dealer_Code_3rd__c,
                                                VGA_VWC_Dealer_Code_4th__c,
                                                VGA_VWC_Dealer_Code_5th__c,
                                                VGA_VWC_Dealer__r.Name,
                                                VGA_VWC_Dealer_2nd__r.Name,
                                                VGA_VWC_Dealer_3rd__r.Name,
                                                VGA_VWC_Dealer_4th__r.Name,
                                                VGA_VWC_Dealer_5th__r.Name,
                                                VGA_VWC_Dealer__c,
                                                VGA_VWC_Dealer_2nd__c,
                                                VGA_VWC_Dealer_3rd__c,
                                                VGA_VWC_Dealer_4th__c,
                                                VGA_VWC_Dealer_5th__c
                                                FROM VGA_Post_Code_Mapping__c where VGA_Post_Code__c =: postCode ];
          }

           if(pmaVGAVal!= null && !pmaVGAVal.isEmpty())
           {
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartArray();
                for(VGA_Post_Code_Mapping__c result: pmaVGAVal )
                {
                    gen.writeStartObject();
                    if(result.VGA_VWP_Dealer_Code__c != null)
                    {
                        gen.writeStringField('dealerCode',  result.VGA_VWP_Dealer_Code__c);
                    }
                    else
                    {
                        gen.writeStringField('dealerCode',  '');
                    }

                    if(result.VGA_VWP_Dealer__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_VWP_Dealer__r.Name);
                    }
                    else
                    {
                        gen.writeStringField('dealerName',  '');
                    }
                    gen.writeStringField('sortOrder' ,  '1');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    gen.writeStartObject();
                    if(result.VGA_VWP_Dealer_Code_2nd__c != null)
                    {
                    gen.writeStringField('dealerCode',  result.VGA_VWP_Dealer_Code_2nd__c);
                    }
                    else
                    {
                        gen.writeStringField('dealerCode',  '');
                    }
                    if(result.VGA_VWP_Dealer_2nd__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_VWP_Dealer_2nd__r.Name);
                    }
                    else
                    {
                        gen.writeStringField('dealerName',  '');
                    }
                    gen.writeStringField('sortOrder' ,  '2');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    ////////////////3rd/////////////////
                   gen.writeStartObject();
                   if(result.VGA_VWP_Dealer_Code_3rd__c != null)
                   {
                           gen.writeStringField('dealerCode',  result.VGA_VWP_Dealer_Code_3rd__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }
                   if(result.VGA_VWP_Dealer_3rd__r.Name != null)
                   {
                        gen.writeStringField('dealerName',  result.VGA_VWP_Dealer_3rd__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '3');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////4th/////////////////
                   gen.writeStartObject();
                   if(result.VGA_VWP_Dealer_Code_4th__c != null)
                   {
                      gen.writeStringField('dealerCode',  result.VGA_VWP_Dealer_Code_4th__c);
                   }
                   else
                   {
                      gen.writeStringField('dealerCode',  '');
                   }
                   if(result.VGA_VWP_Dealer_4th__r.Name != null)
                   {
                       gen.writeStringField('dealerName',  result.VGA_VWP_Dealer_4th__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '4');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////5th/////////////////
                   gen.writeStartObject();

                   if(result.VGA_VWP_Dealer_Code_5th__c != null)
                   {
                           gen.writeStringField('dealerCode',  result.VGA_VWP_Dealer_Code_5th__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }
                   if(result.VGA_VWP_Dealer_5th__r.Name != null)
                   {
                        gen.writeStringField('dealerName',  result.VGA_VWP_Dealer_5th__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '5');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();
                }
                gen.writeEndArray();
                res.responseBody = Blob.valueOf(gen.getAsString());

          }
      

           if(pmaSkoVal !=null && !pmaSkoVal.isEmpty())
           {
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartArray();
                for(VGA_Post_Code_Mapping__c result: pmaSkoVal )
                {
                    ////////////////1st/////////////////
                    gen.writeStartObject();
                    gen.writeStringField('dealerCode',  result.VGA_SKP_Dealer_Code__c);
                    if(result.VGA_SKP_Dealer__r.Name != null)
                    {
                         gen.writeStringField('dealerName',  result.VGA_SKP_Dealer__r.Name);
                    }
                    else
                    {
                        gen.writeStringField('dealerName',  '');
                    }
                    gen.writeStringField('sortOrder' ,  '1');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    ////////////////2nd/////////////////
                    gen.writeStartObject();
                    if(result.VGA_SKP_Dealer_Code_2nd__c != null)
                    {
                         gen.writeStringField('dealerCode',  result.VGA_SKP_Dealer_Code_2nd__c);
                    }
                    else
                    {
                        gen.writeStringField('dealerCode',  '');
                    }
                    if(result.VGA_SKP_Dealer_2nd__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_SKP_Dealer_2nd__r.Name);
                    }
                    else
                    {
                        gen.writeStringField('dealerName',  '');
                    }
                    gen.writeStringField('sortOrder' ,  '2');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    ////////////////3rd/////////////////
                   gen.writeStartObject();
                   if(result.VGA_SKP_Dealer_Code_3rd__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_SKP_Dealer_Code_3rd__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }

                   if(result.VGA_SKP_Dealer_3rd__r.Name != null)
                   {
                         gen.writeStringField('dealerName',  result.VGA_SKP_Dealer_3rd__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '3');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////4th/////////////////
                   gen.writeStartObject();
                   if(result.VGA_SKP_Dealer_Code_4th__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_SKP_Dealer_Code_4th__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }

                   if(result.VGA_SKP_Dealer_4th__r.Name != null)
                   {
                          gen.writeStringField('dealerName',  result.VGA_SKP_Dealer_4th__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '4');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////5th/////////////////
                   gen.writeStartObject();
                   if(result.VGA_SKP_Dealer_Code_5th__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_SKP_Dealer_Code_5th__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }

                   if(result.VGA_SKP_Dealer_5th__r.Name != null)
                   {
                        gen.writeStringField('dealerName',  result.VGA_SKP_Dealer_5th__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '5');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();
                }
                gen.writeEndArray();
                res.responseBody = Blob.valueOf(gen.getAsString());


          }
    

           if(pmaVgaCV!=null && !pmaVgaCV.isEmpty())
           {
                JSONGenerator gen = JSON.createGenerator(true);
                gen.writeStartArray();
                for(VGA_Post_Code_Mapping__c result: pmaVgaCV )
                {
                    gen.writeStartObject();
                    if(result.VGA_VWC_Dealer__r.Name != null)
                    {
                       gen.writeStringField('dealerCode',  result.VGA_VWC_Dealer_Code__c);
                    }
                    else
                    {
                        gen.writeStringField('dealerCode',  '');
                    }
                    if(result.VGA_VWC_Dealer__r.Name != null)
                    {
                       gen.writeStringField('dealerName',  result.VGA_VWC_Dealer__r.Name);
                    }
                    else
                    {
                        gen.writeStringField('dealerName',  '');
                    }
                    gen.writeStringField('sortOrder' ,  '1');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    gen.writeStartObject();

                    if(result.VGA_VWC_Dealer_Code_2nd__c != null)
                    {
                         gen.writeStringField('dealerCode',  result.VGA_VWC_Dealer_Code_2nd__c);
                    }
                    else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }
                    if(result.VGA_VWC_Dealer_2nd__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_VWC_Dealer_2nd__r.Name);
                    }
                    else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                    gen.writeStringField('sortOrder' ,  '2');
                    gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                    gen.writeEndObject();

                    ////////////////3rd/////////////////
                   gen.writeStartObject();
                   if(result.VGA_VWC_Dealer_Code_3rd__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_VWC_Dealer_Code_3rd__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }

                   if(result.VGA_VWC_Dealer_3rd__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_VWC_Dealer_3rd__r.Name);
                    }
                    else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '3');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////4th/////////////////
                   gen.writeStartObject();
                   if(result.VGA_VWC_Dealer_Code_4th__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_VWC_Dealer_Code_4th__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }
                   if(result.VGA_VWC_Dealer_4th__r.Name != null)
                    {
                        gen.writeStringField('dealerName',  result.VGA_VWC_Dealer_4th__r.Name);
                    }
                    else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '4');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();

                   ////////////////5th/////////////////
                   gen.writeStartObject();
                   if(result.VGA_VWC_Dealer_Code_5th__c != null)
                   {
                        gen.writeStringField('dealerCode',  result.VGA_VWC_Dealer_Code_5th__c);
                   }
                   else
                   {
                        gen.writeStringField('dealerCode',  '');
                   }
                   if(result.VGA_VWC_Dealer_5th__r.Name != null)
                   {
                       gen.writeStringField('dealerName',  result.VGA_VWC_Dealer_5th__r.Name);
                   }
                   else
                   {
                        gen.writeStringField('dealerName',  '');
                   }
                   gen.writeStringField('sortOrder' ,  '5');
                   gen.writeStringField('PostCode'  ,  result.VGA_Post_Code__c);
                   gen.writeEndObject();
                }
                gen.writeEndArray();
                res.responseBody = Blob.valueOf(gen.getAsString());


          }
        if(pmaVGAVal.isEmpty() && pmaSkoVal.isEmpty() && pmaVgaCV.isEmpty())  
         {
             JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             gen.writeEndObject();
             res.responseBody = Blob.valueOf(gen.getAsString());
         }


}

}