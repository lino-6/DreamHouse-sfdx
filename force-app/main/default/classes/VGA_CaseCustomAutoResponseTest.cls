@isTest
public class VGA_CaseCustomAutoResponseTest
{
    public static String definedEmailTo = 'test@emailtocase.net.au';

    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'EmailMessage';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;

        Email_To_Case__c etc  = new Email_To_Case__c();
        etc.Name              = definedEmailTo;                                             
        etc.Display_Name__c   = 'test';
        etc.Service_Email__c  = definedEmailTo;
        etc.Email_Template__c = 'test';
        
        insert etc;

        Set<String> subjectsSet      = new Set<String>{'TestSubject1', 'TestSubject2'};
        List<Case> casesToInsertList = VGA_CommonTracker.buildCaseRecords(subjectsSet);
        casesToInsertList[0].VGA_Omni_Queue_Name__c = 'Omni Channel Queue';
        insert casesToInsertList;
    }

    /**
      * @author Lino Diaz Alonso
      * @date 17/01/2019
      * @description Method that tests the isEmailExcluded method for a whole match.
      *              Given that the person sending an email to the Email to case functionality
      *              has been added to the Email_to_Case_Exclusion_List__c custom setting.
      *              This method will return false and an auto-response email won't be sent.
     */
    static testMethod void test01_isEmailExcluded_whenEmailIsReceive_DontSendAutoResponse_IfEmailIsExcluded() 
    {
        Case objCase = [SELECT Id,VGA_Omni_Queue_Name__c 
                        FROM Case 
                        WHERE VGA_Omni_Queue_Name__c='Omni Channel Queue'] ;

        Email_to_Case_Exclusion_List__c etcExclusionList = new Email_to_Case_Exclusion_List__c();
        etcExclusionList.Name                            = 'test@test.com';
        insert etcExclusionList;

        EmailMessage objEmailMessage = VGA_CommonTracker.buildEmailMessage('test@test.com', definedEmailTo, 
                                                                           'rishi.patel@saasfocus.com,success@test.com', 
                                                                           objCase.id);

        Boolean result = VGA_EmailMessageTriggerHelper.isEmailExcluded(objEmailMessage);
        
        //Return should be false as the from in the email we are testing is in the excluded list
        System.assertEquals(false, result);
    }

    /**
      * @author Lino Diaz Alonso
      * @date 17/01/2019
      * @description Method that tests the isEmailExcluded method when there is a match with the domain.
      *              Given that the person sending an email to the Email to case functionality
      *              has been added to the Email_to_Case_Exclusion_List__c custom setting.
      *              This method will return false and an auto-response email won't be sent.
     */
    static testMethod void test02_isEmailExcluded_whenEmailIsReceive_DontSendAutoResponse_IfDomainIsExcluded() 
    {
        Case objCase = [SELECT Id,VGA_Omni_Queue_Name__c 
                        FROM Case 
                        WHERE VGA_Omni_Queue_Name__c='Omni Channel Queue'] ;

        Email_to_Case_Exclusion_List__c etcExclusionList = new Email_to_Case_Exclusion_List__c();
        etcExclusionList.Name                            = '@test.com';
        insert etcExclusionList;

        EmailMessage objEmailMessage = VGA_CommonTracker.buildEmailMessage('test@test.com', definedEmailTo, 
                                                                           'rishi.patel@saasfocus.com,success@test.com', 
                                                                           objCase.id);

        Boolean result = VGA_EmailMessageTriggerHelper.isEmailExcluded(objEmailMessage);
        
        //Return should be false as the from in the email we are testing is in the excluded list
        System.assertEquals(false, result);
    }

    /**
      * @author Lino Diaz Alonso
      * @date 17/01/2019
      * @description Method that tests the scenario when a new email is received and the from email is not
      *              being excluded. We will be sending an auto-reponse message.
     */
   @isTest public static void test03_isValidEmail_whenEmailIsReceive_SendAutoResponse_IfEmailIsValid()
   {
        Case objCase = [SELECT Id,VGA_Omni_Queue_Name__c 
                        FROM Case 
                        WHERE VGA_Omni_Queue_Name__c='Academy Email'] ;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
        User u = new User(Alias = 'SysAdmin', Email='admin@bcinsourcing.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='admin@bcinsourcing.com');
            
        insert u;
        System.runAs(u) { 
            EmailTemplate et = new EmailTemplate (developerName = 'test', 
                                            TemplateType= 'Text', 
                                            Name = 'test', 
                                            Subject = 'AutoResponseSubject',
                                            body = 'test',
                                            FolderId = UserInfo.getUserId(),
                                            HtmlValue = 'AutoResponse'
                                            );
            insert et;
            
            Test.startTest();
            
            EmailMessage objEmailMessage = VGA_CommonTracker.buildEmailMessage('test@test.com', definedEmailTo, 
                                                                               'rishi.patel@vwgroup.com,success@test.com', 
                                                                               objCase.id);
            insert objEmailMessage;
            //Validate that we are sending an auto-response message when the email is valid.
            List<EmailMessage> autoResponse = [SELECT Id, Subject FROM EmailMessage WHERE Subject = 'AutoResponseSubject' ];
            System.assert(autoResponse.size()!=0, 'AutoResponse email has not been sent');
        }
   }

   /**
      * @author Lino Diaz Alonso
      * @date 17/01/2019
      * @description Method that tests the scenario when a new email is received and the from email is not
      *              being excluded. We will be sending an auto-reponse message.
     */
   @isTest public static void test04_isValidUTFEmail_whenEmailIsReceive_SendAutoResponse_IfEmailIsValid()
   {
        Email_To_Case__c etc = new Email_To_Case__c(Name='academy40%25%40success.skoda.net.au',
                                                    Display_Name__c ='test case',
                                                    Service_Email__c='academy@success.skoda.net.au',
                                                    Email_Template__c = 'test'
                                                  );
        insert etc;

        Case objCase = [SELECT Id,VGA_Omni_Queue_Name__c 
                        FROM Case 
                        WHERE VGA_Omni_Queue_Name__c='Academy Email'] ;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
    
        User u = new User(Alias = 'SysAdmin', Email='admin@bcinsourcing.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='admin@bcinsourcing.com');
            
        insert u;

        System.runAs(u) { 
            EmailTemplate et = new EmailTemplate (developerName = 'test', 
                                            TemplateType= 'Text', 
                                            Name = 'test', 
                                            Subject = 'AutoResponseSubject',
                                            body = 'test',
                                            FolderId = UserInfo.getUserId(),
                                            HtmlValue = 'AutoResponse'
                                            );
            insert et;
            
            Test.startTest();
            
            EmailMessage objEmailMessage = VGA_CommonTracker.buildEmailMessage('test@test.com', 'academy40%@success.skoda.net.au', 
                                                                               'academytest@success.skoda.net.au,success@test.com', 
                                                                               objCase.id);
            insert objEmailMessage;
            //Validate that we are sending an auto-response message when the email is valid.
            List<EmailMessage> autoResponse = [SELECT Id, Subject FROM EmailMessage WHERE Subject = 'AutoResponseSubject' ];
            System.assert(autoResponse.size()!=0, 'AutoResponse email has not been sent');
        }
   }
}