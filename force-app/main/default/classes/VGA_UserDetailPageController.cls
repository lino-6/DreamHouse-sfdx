public Without Sharing class VGA_UserDetailPageController 
{  
    
    @AuraEnabled
    public static User getlogginguser()
    {
        User objUser =VGA_Common.getloggingAccountDetails();
        return objUser;
    }

    @AuraEnabled
    public static list<VGA_User_Profile__c> getlistofuserProfile(string radiobuttonvalue,String sortField, boolean isAsc)
    {
        list<VGA_User_Profile__c> objUserProfile =New list<VGA_User_Profile__c>();
        User ObjUser = [select Id, ContactId, VGA_Account_Id__c,VGA_Brand1__c from User where Id =: UserInfo.getUserId() limit 1];
        string Dealerid =ObjUser.VGA_Account_Id__c;
        if(ObjUser !=Null && Dealerid !=Null)
        {
           string queryString;
           queryString =  'select Id,Name,VGA_Account_Id__c,VGA_Available__c, VGA_Available_TestDrive__c, VGA_Contact__r.VGA_Partner_User_Enabled__c,VGA_Contact__r.VGA_Partner_User_Status__c,VGA_Brand__c,VGA_Contact__c,VGA_Contact__r.Name,VGA_Contact__r.VGA_username__c,VGA_Contact__r.Email,VGA_Contact__r.MobilePhone,VGA_Mobile__c,VGA_Role__c,VGA_Sub_Brand__c';
           queryString += ' from VGA_User_Profile__c ';
           queryString += ' where VGA_Account_Id__c =: Dealerid';
           queryString += ' and VGA_Sub_Brand__c LIKE \'%' + radiobuttonvalue + '%\' '; 
           queryString += ' and VGA_Contact_Status__c =\'Enabled\'';
            
           if (sortField != '') 
           {
              queryString += ' order by ' + sortField;
              if (isAsc) 
              {
                queryString += ' asc';
              } 
              else 
              {
                queryString += ' desc';
              }
           }
            queryString += ' LIMIT 49999';
             objUserProfile = Database.query(queryString); 
         }
       
        return objUserProfile;
    }

    @AuraEnabled
    public static List<User> getlistofUser()
    {
        List<User> listofUser = new List<User>();
        User ObjUser = [SELECT Id, ContactId, Contact.AccountId,VGA_Brand1__c 
                        FROM User 
                        WHERE Id =: UserInfo.getUserId() 
                        LIMIT 1];
        
        if(ObjUser != Null 
          && ObjUser.Contact.AccountId != Null)
        {
            listofUser = [SELECT Id, IsActive, Email, Name,VGA_Brand1__c,Username, Phone,
                          Contact.AccountId, UserRole.Name,
                          MobilePhone 
                          FROM User 
                          WHERE Contact.AccountId =: ObjUser.Contact.AccountId 
                          ORDER BY createddate DESC 
                          LIMIT 999];
        }
        return listofUser;
    }
    
    @AuraEnabled
    public static VGA_User_Profile__c getUserDetail(string recordId)
    {
        List<User> listofUserProfile = new List<User>();
        VGA_User_Profile__c objUser =[SELECT Id,Name,VGA_Account_Id__c,VGA_Available__c,VGA_Active__c,VGA_Brand__c,VGA_Contact__c,
                                            VGA_Contact__r.Name,VGA_Contact__r.VGA_username__c,VGA_Contact__r.Email,VGA_Contact__r.MobilePhone,VGA_Mobile__c,VGA_Contact__r.FirstName,VGA_Contact__r.LastName,VGA_Contact__r.VGA_Job_Title__c,
                                            VGA_Role__c,VGA_Sub_Brand__c, VGA_Available_TestDrive__c
                                      FROM VGA_User_Profile__c 
                                      WHERE Id=:recordId 
                                      LIMIT 1]; 
       return objUser;        
    }
    @AuraEnabled
    public static list<VGA_Working_Hour__c> getWorkingHour(string recordId)
    {
        list<VGA_Working_Hour__c>objWorkingHours=new list<VGA_Working_Hour__c>([select VGA_Working__c,VGA_User_Profile__c,Name,VGA_Timezone__c,
                                                                                VGA_Start_Time__c,VGA_End_Time__c,VGA_Day__c from VGA_Working_Hour__c where VGA_User_Profile__c=:recordId order By createddate DESC limit 999 ]);
        return objWorkingHours;        
    }
    @AuraEnabled
    public static list<VGA_Subscription__c> getSubscription(string recordId)
    {
         string recordTypeIdValue =VGA_Common.getSubscriptionRecordTypeId();
         list<VGA_Subscription__c>objSubscription=new list<VGA_Subscription__c>([select Id, VGA_Delivery_Method__c,VGA_Frequency__c,LastModifiedDate,
                                                                                 VGA_Report_Subscription_Type__c,VGA_Subscription__c,
                                                                                 VGA_Subscription_Type__c,VGA_Subscription_Value__c,VGA_User_Profile__c 
                                                                                 from VGA_Subscription__c where VGA_User_Profile__c=:recordId and recordTypeId=:recordTypeIdValue order By createddate DESC limit 999]);
       return objSubscription; 
    }
    @AuraEnabled
    public static list<VGA_Subscription__c> getReportSubscription(string recordId)
    {
         string recordTypeIdValue =VGA_Common.getReportSubscriptionRecordTypeId();
         list<VGA_Subscription__c>objSubscription=new list<VGA_Subscription__c>([select Id, VGA_Delivery_Method__c,VGA_Frequency__c,LastModifiedDate,
                                                                                 VGA_Report_Subscription_Type__c,VGA_Subscription__c,
                                                                                 VGA_Subscription_Type__c,VGA_Subscription_Value__c,VGA_User_Profile__c 
                                                                                 from VGA_Subscription__c where VGA_User_Profile__c=:recordId and recordTypeId=:recordTypeIdValue order By createddate DESC limit 999]);
       return objSubscription; 
    }
    @AuraEnabled
    public static VGA_Working_Hour__c getworkingHoursDetail(string recordId)
    {
       VGA_Working_Hour__c objWorkingHours=[select VGA_Working__c,VGA_User_Profile__c,Name,VGA_Account_ID__c,VGA_Sub_Brand__c,VGA_Timezone__c,
                                           VGA_Start_Time__c,VGA_End_Time__c,VGA_Day__c from 
                                            VGA_Working_Hour__c where Id=:recordId  limit 1];
       return objWorkingHours;        
    }
    @AuraEnabled
    public static string resetPassword(VGA_User_Profile__c objUserProfile)
    {
         list<User>listofContact =New list<User>([select Id,Email,VGA_Brand1__c from User where ContactId =:objUserProfile.VGA_Contact__c limit 1]);
         if(listofContact !=Null && listofContact.size()>0)
         {
             try
             {
                 System.resetPassword(listofContact[0].Id,true); 
                 string Email =listofContact[0].Email;
                 return 'Password has been reset and sent to ' + Email;
             }
             catch(exception e)
             {
                    return 'Error: ' + e.getMessage();
             }
         } 
        return null;
    }
    @AuraEnabled
    public static VGA_Subscription__c getSubscriptiondetails(string recordId)
    {
      VGA_Subscription__c objSubscription=[select Id,VGA_Delivery_Method__c,VGA_Frequency__c,LastModifiedDate,
                                                  VGA_Report_Subscription_Type__c,VGA_Subscription__c,
                                                  VGA_Subscription_Type__c,VGA_Subscription_Value__c,VGA_User_Profile__c 
                                                  from VGA_Subscription__c where Id=:recordId]; 
      return objSubscription;
    }
    @AuraEnabled
    public static string saveWorkingHours(VGA_Working_Hour__c objWorkingHours)
    {
       
        datetime dToday2 =system.today();
        string TimeZonevalue =objWorkingHours.VGA_Timezone__c;
        Integer FirststartdateTimevalue =Integer.valueof(objWorkingHours.VGA_Start_Time__c.substring(0,2));
        Integer secondstartdateTimevalue =Integer.valueof(objWorkingHours.VGA_Start_Time__c.substring(3,5));
        string thirdstartdateTimevalue =objWorkingHours.VGA_Start_Time__c.substring(6,8);
        Integer finalhoursvalue;
        if(thirdstartdateTimevalue  =='AM')
         {
             if(FirststartdateTimevalue ==12)
             {
              finalhoursvalue=00;
             }
             else
             {
             finalhoursvalue=FirststartdateTimevalue;
             }   
         }
         else
         {
            if(FirststartdateTimevalue ==12)
             {
              finalhoursvalue=FirststartdateTimevalue;
             }
            else
            {
            finalhoursvalue=FirststartdateTimevalue+12 ;
            }
         }
         DateTime startDateTime = DateTime.newInstance(dToday2.year(), 
                                                   dToday2.month(),
                                                   dToday2.day(),
                                                   finalhoursvalue,
                                                   secondstartdateTimevalue,
                                                   0);

        string strCreatedDate = startDateTime.format('yyyy-MM-dd HH:mm:ss');
        list<string> lstSplitDateandTime = strCreatedDate.split(' ');
        list<string> lstSplitDateValue = lstSplitDateandTime[0].split('-');
        list<string> lstsplitTimeValue = lstSplitDateandTime[1].split(':');
        Datetime startDateTimeValue = datetime.newinstancegmt (integer.valueof(lstSplitDateValue[0]),
                                                          integer.valueof(lstSplitDateValue[1]),
                                                          integer.valueof(lstSplitDateValue[2]),
                                                          integer.valueof(lstsplitTimeValue[0]),
                                                          integer.valueof(lstsplitTimeValue[1]),
                                                          integer.valueof(lstsplitTimeValue[2]));
       
        Integer FirstEnddateTimevalue =Integer.valueof(objWorkingHours.VGA_End_Time__c.substring(0,2));
        Integer secondEnddateTimevalue =Integer.valueof(objWorkingHours.VGA_End_Time__c.substring(3,5));
        string thirdEnddateTimevalue =objWorkingHours.VGA_End_Time__c.substring(6,8);
        Integer secondfinalhoursvalue;
        if(thirdEnddateTimevalue  =='AM')
         {
             if(FirstEnddateTimevalue ==12)
             {
              secondfinalhoursvalue=00;
             }
             else
             {
             secondfinalhoursvalue=FirstEnddateTimevalue;
             }   
         }
         else
         {
            if(FirstEnddateTimevalue ==12)
             {
              secondfinalhoursvalue=FirstEnddateTimevalue;
             }
            else
            {
            secondfinalhoursvalue=FirstEnddateTimevalue+12 ;
            }
         }
         DateTime startDateTime2 = DateTime.newInstance(dToday2.year(), 
                                                   dToday2.month(),
                                                   dToday2.day(),
                                                   secondfinalhoursvalue,
                                                   secondEnddateTimevalue,
                                                   0);
        string strCreatedDate2 = startDateTime2.format('yyyy-MM-dd HH:mm:ss');
        list<string> lstSplitDateandTime2 = strCreatedDate2.split(' ');
        list<string> lstSplitDateValue2 = lstSplitDateandTime2[0].split('-');
        list<string> lstsplitTimeValue2 = lstSplitDateandTime2[1].split(':');
        Datetime EndDateTimeValue = datetime.newinstancegmt (integer.valueof(lstSplitDateValue2[0]),
                                                          integer.valueof(lstSplitDateValue2[1]),
                                                          integer.valueof(lstSplitDateValue2[2]),
                                                          integer.valueof(lstsplitTimeValue2[0]),
                                                          integer.valueof(lstsplitTimeValue2[1]),
                                                          integer.valueof(lstsplitTimeValue2[2]));
       list<VGA_Trading_Hour__c >objTradingHours;
        if(!test.isRunningTest())
        {
            objTradingHours =new list<VGA_Trading_Hour__c>([select Id,Name,VGA_Start_Time__c,VGA_Dealer_Account__c,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,
                                                 VGA_Sub_Brand__c,VGA_End_Time__c,VGA_Day__c from VGA_Trading_Hour__c where 
                                                 Name=:objWorkingHours.Name and 
                                                 VGA_Dealer_Account__c=:objWorkingHours.VGA_Account_ID__c and 
                                                 VGA_Sub_Brand__c=:objWorkingHours.VGA_Sub_Brand__c limit 1]);
             
        }
        else
        {
            objTradingHours =new list<VGA_Trading_Hour__c>([select Id,Name,VGA_Start_Time__c,VGA_Dealer_Account__c,VGA_Start_Date_Date_Time__c,VGA_End_Time_Date_Time__c,
                                                 VGA_Sub_Brand__c,VGA_End_Time__c,VGA_Day__c from VGA_Trading_Hour__c where 
                                                 Name=:objWorkingHours.Name]);
           
            startDateTimeValue = objTradingHours[0].VGA_Start_Date_Date_Time__c;
            EndDateTimeValue = objTradingHours[0].VGA_End_Time_Date_Time__c;
        }
         if(startDateTimeValue>=objTradingHours[0].VGA_Start_Date_Date_Time__c &&  
              EndDateTimeValue <=objTradingHours[0].VGA_End_Time_Date_Time__c)
         {
                 try
                 {
                     update objWorkingHours;
                     return 'Working hours saved';
                 }
                 catch(exception e)
                 {
                        return 'Error: ' + e.getMessage();
                 }    
         }
        else
        {
            
            return 'Error: ' +' Working hours must be within the dealer trading times.';
        }  
    }
    @AuraEnabled
    public static string saveSubscription(VGA_Subscription__c objSubscription, string userProfile,boolean subcription)
    {
       string recordTypeId =VGA_Common.getSubscriptionRecordTypeId();
       string message;
       list<VGA_Subscription__c>listofSubscription =new list<VGA_Subscription__c>([select Id from VGA_Subscription__c where VGA_User_Profile__c =:userProfile
                                                                                  and VGA_Subscription_Type__c =:objSubscription.VGA_Subscription_Type__c ]);
       if(listofSubscription !=Null && listofSubscription.size()>0)
       {
         message ='Already exist with same Subscription Type.'  ;
         return 'Error: ' + message;
       }
       else
       {
           try
           {
             if(userProfile !=Null )
             {
               objSubscription.VGA_User_Profile__c = userProfile;
               objSubscription.recordTypeId =recordTypeId;  
             }
             upsert objSubscription ;
             message ='Subscriptions Updated';
             return message;
           }
           catch(exception e)
           {
                 message =e.getMessage();
                 return 'Error: ' + message;
           }
       }
    }
    @AuraEnabled
    public static string deleteSubscription( VGA_Subscription__c userProfile)
    {
        try
           { 
            delete userProfile;
            return 'Subscription deleted';
           }
          catch(exception e)
           {
                 return 'Error: ' + e.getMessage();
           } 
    }
    @AuraEnabled
    public static string saveReportSubscription(VGA_Subscription__c objSubscription, string userProfile)
    {
       string recordTypeId =VGA_Common.getReportSubscriptionRecordTypeId();
       string message;
       list<VGA_Subscription__c>listofSubscription =new list<VGA_Subscription__c>([select Id from VGA_Subscription__c where VGA_User_Profile__c =:userProfile
                                                                                  and VGA_Report_Subscription_Type__c =:objSubscription.VGA_Report_Subscription_Type__c ]);
        if(listofSubscription !=Null && listofSubscription.size()>0)
       {
         message ='Already exist with same Subscription Type.'  ;
         return 'Error: ' + message;
       }
       else
       {
           try
           {
              if(userProfile !=Null)
              {
                   objSubscription.VGA_User_Profile__c = userProfile; 
                   objSubscription.recordTypeId=recordTypeId;
              }
              if(objSubscription.ID !=Null)
              {
               update objSubscription ;   
              }
              else
              {
                  insert objSubscription;
                  String myLabel = System.Label.Task_Email;
                  if(userProfile !=Null)
                  {
                      VGA_User_Profile__c objUserprofile  =[select VGA_Contact__r.Name from VGA_User_Profile__c where Id= :userProfile  limit 1]; 
                      if(myLabel !=Null)
                      {
                          list<user>objuser =new list<user>([select Id from user where UserName=:myLabel and isActive=true limit 1]);
                           
                          if(objuser !=Null && objuser.size()>0)
                          {
                             Task objTask =new Task(); 
                              objTask.Subject = 'Assign'+' '+ objSubscription.VGA_Report_Subscription_Type__c +' ' + 'report to'+' '+objUserprofile.VGA_Contact__r.Name;
                              objTask.OwnerId=objuser[0].Id;
                              objTask.ActivityDate =system.today().adddays(1);
                              objTask.Priority='Standard';
                              objTask.status ='Open';
                              insert objTask;  
                          } 
                      } 
                  }
              }
             message ='Subscriptions Updated';
             return message;
           }
           catch(exception e)
           {
                 message =e.getMessage();

                 return 'Error: ' + message;
           }
       }
    }
    @AuraEnabled
    public static string saveduserDetail(VGA_User_Profile__c objUserProfile)
    {
        list<User>listofConatct =New list<User>([select Id from User where ContactId =:objUserProfile.VGA_Contact__c limit 1]);
        string str1;
        string  str2;

        if(objUserProfile.VGA_Contact__r.Name !=Null)
        {
            if(objUserProfile.VGA_Contact__r.Name.contains(' '))
            {
               str1=objUserProfile.VGA_Contact__r.Name.substring(0,objUserProfile.VGA_Contact__r.Name.indexOf(' '));
               str2=objUserProfile.VGA_Contact__r.Name.substring(objUserProfile.VGA_Contact__r.Name.indexOf(' ')+1);
            }
            else
            {
               str2=objUserProfile.VGA_Contact__r.Name;
            }
        }


        Contact objcon =New Contact();
        objcon.Id =objUserProfile.VGA_Contact__c;
        objcon.FirstName =str1;
        objcon.LastName  =str2; 
        objcon.email=objuserprofile.VGA_Contact__r.email;
        objcon.VGA_Job_Title__c=objuserprofile.VGA_Contact__r.VGA_Job_Title__c;
        if(String.isNotBlank(objUserProfile.VGA_Sub_Brand__c))
        {
            if(objUserProfile.VGA_Sub_Brand__c.contains('Passenger'))
            {
                objcon.VGA_PV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
            else
            {
                objcon.VGA_CV_Mobile__c =objUserProfile.VGA_Mobile__c;
            }
        }
        User ObjUser  =New User ();
        if(listofConatct !=Null && listofConatct.size()>0)
        {
        ObjUser.Id =listofConatct[0].Id;
        ObjUser.FirstName=str1;
        ObjUser.LastName  =str2;
        }
        try
        {
          update objcon;
          if(listofConatct !=Null && listofConatct.size()>0)
             {
             Update ObjUser;
             }
         update objUserProfile; 
         return 'User Saved';
        }
        catch(exception e)
        { 
            //Call a method to handle the exception and show an error message.
            return VGA_UserDetailPageControllerHelper.getErrorMessage(e);
        }
    }
    
    /**
      * @author Lino Diaz Alonso
      * @date 03/01/2018
      * @description Method to disable a contact and its related user if the contact 
      *              doesn't have any subrand. 
      * @param objUserProfile User profile related to the contact that we are disabling.
      * @param selected Name of the subbrand for which we will be disabling the contact.
      * @return String Error/Success message that we will be displying in the dealer porta.
     */       
   @AuraEnabled
   public static string deleteUserProfie (VGA_User_Profile__c objUserProfile,string selected)
   {
        String subbrands = selected =='Passenger' ? 'Passenger Vehicles (PV)' :  'Commercial Vehicles (CV)';
        string strmessage;
        string userId =VGA_Common.getuserId(objUserProfile.VGA_Contact__c);
        
        if(VGA_UserDetailPageControllerHelper.isNominatedUser(userId, subbrands)){
          return 'Error: This user is a nominated user.';
        }
        
         
       list<Lead> listofLead = new list<Lead>([SELECT Id,VGA_Brand__c,VGA_Sub_Brand__c 
                                               FROM Lead 
                                               WHERE OwnerId=:UserId
                                               AND (Status ='New'OR Status ='Accepted') 
                                               AND VGA_Sub_Brand__c includes (:subbrands) ]);
        
        //If the user that we are trying to disable it has leads related to it
        if(listofLead !=null && listofLead.size()>0)
        {
            return 'Lead:' +objUserProfile;
        }
        else
        {
            Contact objCon     =  [SELECT VGA_Brand__c,VGA_Sub_Brand__c,VGA_PV_Mobile__c,
                                          VGA_cv_Mobile__c,Recordtypeid 
                                   FROM Contact 
                                   WHERE Id=:objUserProfile.VGA_Contact__c 
                                   LIMIT 1];

            String  subbrand   = objCon.VGA_Sub_Brand__c;
            List<VGA_User_Profile__c>listofUserProfile = new list<VGA_User_Profile__c>([SELECT Id
                                                                                        FROM VGA_User_Profile__c
                                                                                        WHERE VGA_Contact__c =: objUserProfile.VGA_Contact__c]);
            if(listofUserProfile !=Null)
            {
                return VGA_UserDetailPageControllerHelper.disableContact(objCon, subbrand, objUserProfile, listofUserProfile);
            }
            
            return strmessage;          
        }
   }
     
     @AuraEnabled
     public static map<string,string> listofuser(VGA_User_Profile__c objUserProfile,string selected)
     {
        String subbrands = selected =='Passenger' ? 'Passenger Vehicles (PV)' :  'Commercial Vehicles (CV)';

        string UserId =VGA_Common.getuserId(objUserProfile.VGA_Contact__c);
        Map<String,string> mapofContactNameandId = new Map<String,string>();
        mapofContactNameandId.put('--None--','--None--') ;
        list<user>listofUser;

        Set<Id> contactIdSet = VGA_UserDetailPageControllerHelper.getContactsAvailableToReceiveSet(objUserProfile.VGA_Account_Id__c, subbrands);

        listofUser =new list<user>([select Id,Name,ContactId,Contact.Name,contact.AccountId 
                                    FROM User 
                                    WHERE contactId IN:contactIdSet
                                    AND IsActive=true
                                    AND Id !=: UserId]);

        if(listofUser !=Null && listofUser.size()>0)
        {
            for(user objuser :listofUser)
            {
                mapofContactNameandId.put(objuser.Contact.Name,objuser.Id);
            }   
        }
        return mapofContactNameandId;
     }
    
    /**
      * @author Lino Diaz Alonso
      * @date 03/01/2018
      * @description Method to disable a contact and its related user if the contact 
      *              doesn't have any subrand. It will also reassign all the leads that 
      *              the related user owns. New owner will be specified in the 
      *              selecteduserId field.
      * @param objUserProfile User profile related to the contact that we are disabling.
      * @param selected Name of the subbrand for which we will be disabling the contact.
      * @param selecteduserId Id of the user that will be the new owner of the leads.
      * @return String Error/Success message that we will be displying in the dealer porta.
     */
    @AuraEnabled
    public static string ressignLead(VGA_User_Profile__c objUserProfile,string selected,string selecteduserId)
    {
        string strmessage ;
        list<Lead>updateListofLead =New list<Lead>();
        String subbrands = selected =='Passenger' ? 'Passenger Vehicles (PV)' :  'Commercial Vehicles (CV)';
        Contact objCon     =[SELECT VGA_Brand__c,VGA_Sub_Brand__c,recordtypeid 
                             FROM Contact 
                             WHERE Id=:objUserProfile.VGA_Contact__c 
                             LIMIT 1];
        string  subbrand   =objCon.VGA_Sub_Brand__c;
        string UserId =VGA_Common.getuserId(objUserProfile.VGA_Contact__c);
        list<Lead>listofLead = new list<Lead>([SELECT Id,VGA_Brand__c,VGA_Sub_Brand__c 
                                               FROM Lead 
                                               WHERE OwnerId=:UserId 
                                               AND (status='New'or status='Accepted') 
                                               AND VGA_Sub_Brand__c includes (:subbrands)]);
        Set<Id> leadIdSet = new Set<Id>();
        
        if(listofLead !=Null && listofLead.size()>0)
        {
            for(Lead objLead :listofLead)
            {
              objLead.OwnerId= selecteduserId;
              leadIdSet.add(objLead.Id);
              updateListofLead.add(objLead);
            }
        }

        list<VGA_User_Profile__c>listofUserProfile =New list<VGA_User_Profile__c>([select Id from VGA_User_Profile__c
                                                                                   where VGA_Contact__c=:objUserProfile.VGA_Contact__c limit 2]);
        
        if(listofUserProfile !=Null)
        {
            return VGA_UserDetailPageControllerHelper.updateContactAndReassignLeads(subbrand, updateListofLead, leadIdSet, objUserProfile, listofUserProfile, selecteduserId);
        }

        return strmessage;
    }
   
    /**
    * @author Ashok Chandra
    * @date 18/09/2018
    * @ Ticket 170
    * @description It accepts one Contact and selected valued from external
    * user and create the contact if the contact is not exist. Update the contact if contact is exist.
    * @param Contact
    * @param selectedvalue i.e. 'Commercial' or 'Passenger'
    * @return string Either Success User created successfully or Error Duplicate User
    */
     @AuraEnabled
    public static string createUserProfie(Contact objContact,string selectedValue)
    {
      //To rollback if error occurs in user creation
      Savepoint sp = Database.setSavepoint();
       
      User objUser = VGA_Common.getloggingAccountDetails();

      String subbrands = selectedValue =='Passenger' ? 'Passenger Vehicles (PV)' :  'Commercial Vehicles (CV)';

      String typeofSubbrand = selectedValue =='Commercial' ? 'Commercial Vehicles (CV)' : 'Passenger Vehicles (PV)';
      String subbrand;

      //Get contact rectord type based on the contact brand
      Id devRecordTypeId = objUser.Contact.VGA_Brand__c =='Volkswagen' ? Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Dealer Contact').getRecordTypeId() : Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Dealer Contact').getRecordTypeId();

      //Get record type Name based on the user brand
      String recordTypeName = objUser.VGA_Brand__c != null && objUser.VGA_Brand__c.contains('Volkswagen') ? 'Volkswagen Dealer Contact' : 'Skoda Dealer Contact';
            
      try
      { 
          // Added by Ashok Chandra on 18-09-2018 for ticket 170
          // Getting the existing contact by using Email
          List<contact> objexistContact = [SELECT Id,VGA_Brand__c,Recordtypeid,Name,
                                                  VGA_Partner_User_Status__c,LastName,
                                                  FirstName,Email, VGA_Role__c, MobilePhone, 
                                                  Phone, VGA_Username__c,Accountid,
                                                  VGA_Sub_Brand__c,Account.Name 
                                           FROM contact 
                                           WHERE Email=:objcontact.Email 
                                           AND accountid=:objUser.VGA_Account_Id__c   
                                           LIMIT 1 ];
               
          // IF there is no contact exist with Email then we are creating the contact in follow code.
          if(objexistContact.size()==0)
          {
            objContact = VGA_UserDetailPageControllerHelper.createNewContact(objContact, recordTypeName, selectedValue, objUser);
          }
          // Added by Ashok Chandra on 18-09-2018 for ticket 170
          // if the contact exist then update the contact sub brand based on selection 
          // i.e. if subbrand contains Passenger Vehicles (PV) and user selected
          //  Commercial Vehicles (CV) so we are adding Commercial Vehicles (CV) 
          // in subbrand and viceversa
          else if(!objexistContact.isEmpty())
          {
            objContact = VGA_UserDetailPageControllerHelper.updateExistingContact(objexistContact[0], selectedValue);    
            if(objContact == null){
              return 'Error: Duplicate Username';
            } 
          }
          string  str = UpdatePortalUser(objContact);
          
          if(str.contains('Error') )
          {
              Database.rollback(sp);
              return str;
          }
          else
          {
           return 'User created successfully'; 
          }
      }
      catch(exception e)
      {
               return 'Error: ' + e.getMessage(); 
      }  
        
    } 
    
    @AuraEnabled
    public static contact getcontactDetail(string RecordId)
    {
      contact objContact =[SELECT Id,VGA_Brand__c,Recordtypeid,Name,VGA_Partner_User_Status__c,
                                  LastName,FirstName,Email, VGA_Role__c, MobilePhone, Phone, 
                                  VGA_Username__c, VGA_Sub_Brand__c
                           FROM Contact  
                           WHERE Id=:RecordId 
                           LIMIT 1 ];
      return objContact;
    }

    @AuraEnabled
    public static string createContactandUser(Contact objContact)
    {
        String strContactId = '';
        User objUser = [select Id,VGA_Brand__c, ContactId, Contact.AccountId from User where Id =: UserInfo.getUserId() limit 1];
            
        if(objUser.Contact.AccountId != Null)
        {
            try
            {
                Contact ObjCon = new Contact(); 
                ObjCon = objContact;
                ObjCon.AccountId = objuser.Contact.AccountId;
                objCon.VGA_Partner_User_Status__c = 'Enabled';
                insert ObjCon;
                
                strContactId = ObjCon.Id;
            }
            catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            }
        }
        return strContactId;
    }
    
    /**
      * @author Ganesh M
      * @date 25/10/2018
      * @description Method that is called when a contact is updated and it is now 
      *              Enabled or Disabled. We will update the related user so it is now 
      *              inactive if the user has been disabled or we will create a new
      *              user for this contact if the contact has been enabled.
      * @param objContact Contact that has been updated.
      * @param 
      * @return string String Status of the process rather if it was successful or if there
      *                was any error.
     */
    @AuraEnabled
    public static String UpdatePortalUser(Contact objContact)
    {
        String strStatus = '';
        string strmessage;
        List<User> lstUser = new List<User>();
        list<VGA_Dealership_Profile__c> listofDealershipProfile = new list<VGA_Dealership_Profile__c>();
        Id VWRTId = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact');
        Id SKRTId = VGA_Common.GetRecordTypeId('Contact','Skoda Dealer Contact');
        
        if(objContact != Null)
        {
            
            lstUser = [SELECT Id, isActive,VGA_Brand__c,ContactId,Accountid, VGA_Brand1__c 
                       FROM User 
                       WHERE ContactId =: objContact.Id 
                       LIMIT 1];
            
            if(lstUser != Null && lstUser.size() > 0)
            {   
                if(objcontact.VGA_Partner_User_Status__c == 'Enabled')
                {
                    //Make sure the user has user profiles related to it
                    if(VGA_UserDetailPageControllerHelper.getTopMostRole(objContact) ==''){
                      return 'Error: User Profile Information not found. Sub-brand must be provided before enabling the contact.';
                    }
                    
                    lstUser[0].IsActive = True;
                    lstUser[0].Email    = objContact.Email;
                    lstUser[0].Username = objContact.VGA_Username__c;
                    objcontact.VGA_active__c=true;
                }
                else if(objcontact.VGA_Partner_User_Status__c == 'Disabled')
                {
                  if(VGA_UserDetailPageControllerHelper.isNominatedUser(lstUser[0].Id, objcontact.VGA_Sub_Brand__c)){
                    return 'Error: This user is a nominated user.';
                  }
                  //Contacts owned by this disabled user will be updated.
                  VGA_UserDetailPageControllerHelper.updateContactsOwnedByDisableUser(lstUser[0].Id);
                  
                  //Deactivate user and contact
                  lstUser[0].IsActive         = false;
                  objcontact.VGA_active__c    = false;
                  objcontact.VGA_Sub_Brand__c = null;
                }
            }
            else
            {
                if(objcontact.VGA_Partner_User_Status__c == 'Enabled')
                {
                    User objuser = VGA_UserDetailPageControllerHelper.buildUserFromContact(objContact);
                    
                    //If a sub-brand has not been provided we should not be able to enable 
                    //a contact using the Enable/Disable Partner User button in Salesforce.
                    if(objuser == null){
                        strStatus = 'Error: User Profile Information not found. Sub-brand must be provided before enabling the contact.';
                        return strStatus;  
                    }

                    lstUser.add(objuser);
                } else if(objcontact.VGA_Partner_User_Status__c == 'Disabled')
                {
                    objcontact.VGA_active__c    = false;
                    objcontact.VGA_Sub_Brand__c = null;
                    updatecontact(objContact.Id,objContact.VGA_Partner_User_Status__c,objcontact.vga_active__c, objcontact.VGA_Username__c,objcontact.VGA_Sub_Brand__c);
                    strStatus = 'Record updated successfully.';
                    return strStatus;
                }
                
            }
               
            if(lstUser != Null && lstUser.size() > 0)
            {
                try
                {
                    Database.upsert(lstUser,true);
                    updatecontact(objContact.Id,objContact.VGA_Partner_User_Status__c,objcontact.vga_active__c, objcontact.VGA_Username__c,objcontact.VGA_Sub_Brand__c);
                    VGA_UserDetailPageControllerHelper.AddUsertoGroup(lstUser);
                    strStatus = 'Record updated successfully.';                   
                }
                catch(Exception ex)
                {                   
                    //Call a method to handle the exception and show an error message.
                    return VGA_UserDetailPageControllerHelper.getErrorMessage(ex);
                }
            }
        }
        else
        {
            strStatus = 'Error: Contact information not found.';
            return strStatus;
        }
        
        return strStatus;
    }

    
    @future
    public static void updatecontact(string objConId,String StatusValue,boolean isactive, string username, string subbrand)
    {
       System.debug('Update contact' + subbrand);

       Contact objCon = new Contact(Id = objConId,
                                    VGA_Partner_User_Status__c = StatusValue,
                                    vga_active__c   = isactive, 
                                    VGA_Username__c = username,
                                    VGA_Sub_Brand__c= subbrand);
       update  objCon ;
    } 
}