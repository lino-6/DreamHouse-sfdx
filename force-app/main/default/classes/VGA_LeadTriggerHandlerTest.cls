@isTest
private class VGA_LeadTriggerHandlerTest
{
    @testSetup
    static void buildConfigList() {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Lead';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;

        VGA_Profanity__c exactMatchProf = VGA_CommonTracker.buildProfanity('ass');
        exactMatchProf.VGA_Profanity_Exact_Match__c = true;
        insert exactMatchProf;

        VGA_Profanity__c partialMatchProf = VGA_CommonTracker.buildProfanity('stupid');
        partialMatchProf.VGA_Profanity_Exact_Match__c = false;
        insert partialMatchProf;

        Account dealerAcc = VGA_Commontracker.buildDealerAccount('test Dealer', '2505');
        dealerAcc.BillingState = 'NSW';
        dealerAcc.VGA_Timezone__c = 'Australia/Sydney';
        insert dealerAcc;

        VGA_Dealership_Profile__c dealershipProfile = VGA_Commontracker.createDealershipProfile(dealerAcc.Id);

        VGA_Commontracker.createWeekTradingHours(dealershipProfile.Id);

        VGA_Public_Holiday__c statePublicHoliday = VGA_Commontracker.createPublicHoliday(Date.today(),'test holiday','Single','NSW');
        insert statePublicHoliday;

        VGA_Public_Holiday__c statePublicHoliday1 = VGA_Commontracker.createPublicHoliday(Date.today().addDays(10),'test holiday1','Recurring','NSW');
        insert statePublicHoliday1;

        VGA_Public_Holiday__c allPublicHoliday = VGA_Commontracker.createPublicHoliday(Date.today().addDays(1),'test holiday all','Single','All');
        insert allPublicHoliday;

        VGA_Public_Holiday__c allPublicHoliday1 = VGA_Commontracker.createPublicHoliday(Date.today().addDays(15),'test holiday1 all','Recurring','All');
        insert allPublicHoliday1;

        List<Lead> dealerLeadsList = VGA_CommonTracker.buildLeads('dealerLeads', 2);
        dealerLeadsList[0].VGA_Dealer_Account__c = dealerAcc.Id;
        dealerLeadsList[0].VGA_Assigned_to_Dealership__c = System.Now().addDays(-3);
        dealerLeadsList[0].VGA_Brand__c='Volkswagen';
        dealerLeadsList[0].VGA_Sub_Brand__c='Passenger Vehicles (PV)';
        insert dealerLeadsList;
    }
    
    /**
    * @author Lino Diaz
    * @date 15/10/2018
    * @description Method that tests the checkProfanity method
    *              When the First, Last Name or Email contains one
    *              of the profanity words then the lead will be updated so it
    *              can be handled as a profane lead.
    */
    static testMethod void test01_VGA_checkProfanity_whenLeadMeetsProfanity_ProfanityShouldBeTrue() 
    {
        Test.startTest();
        List<Lead> partialMatchLeadList = VGA_CommonTracker.buildLeads('stupid', 5);
        insert partialMatchLeadList;

        List<Lead> exactMatchLeadList = VGA_CommonTracker.buildLeads('test', 1);
        exactMatchLeadList[0].FirstName = 'ass';
        insert exactMatchLeadList;
        Test.stopTest();
        
        List<Lead> updatedLeadsList = [SELECT Id, LastName,Status, OwnerId, VGA_Profanity__c
                                       FROM Lead
                                       WHERE Id IN: partialMatchLeadList
                                       OR Id IN: exactMatchLeadList];

        //Check that all the leads have been updated and are now Closed,
        // Owner is now the profanity queue and the Profanity flag is now true
        Group  profanityQueue = [select Id, Name from Group where  Type = 'Queue' AND Name = : Label.VGA_Profanity_Queue];
        for(Lead uLead : updatedLeadsList){
            System.assertEquals('Closed', uLead.Status);
            System.assertEquals(profanityQueue.Id, uLead.OwnerId);
            System.assertEquals(True, uLead.VGA_Profanity__c);    
        }
    }

    /**
    * @author Lino Diaz
    * @date 15/10/2018
    * @description Method that tests the negative case of the checkProfanity method
    *              When the First, Last Name or Email doesn't meet the criteria
    *              to be considered a profane lead then the lead won't be updated
    *              and it will be processed as a valid lead.
    */
    static testMethod void test02_VGA_checkProfanityNegative_whenLeadNotMeetsProfanity_ProfanityShouldBeFalse() 
    {
        Test.startTest();
        List<Lead> leadsToInsertList = VGA_CommonTracker.buildLeads('test', 1);
        leadsToInsertList[0].FirstName = 'asstest';
        insert leadsToInsertList;
        Test.stopTest();
        
        List<Lead> updatedLeadsList = [SELECT Id, LastName,Status, OwnerId, VGA_Profanity__c
                                       FROM Lead
                                       WHERE Id IN: leadsToInsertList];

        
        Group  profanityQueue = [select Id, Name from Group where  Type = 'Queue' AND Name = : Label.VGA_Profanity_Queue];
        
        //Check that all the leads have not been updated 
        for(Lead uLead : updatedLeadsList){
            System.assertNotEquals('Closed', uLead.Status);
            System.assertNotEquals(profanityQueue.Id, uLead.OwnerId);
            System.assertEquals(False, uLead.VGA_Profanity__c); 
        }
    }
    
    /**
    * @author Lino Diaz
    * @date 16/04/2019
    * @description Method that tests the calculation of the lead duration
    *              that will be the difference between the lead accepted date and
    *              the date when the dealer got assigned to the dealership.
    *              The calculation will happen on the Lead update
    */
    static testMethod void test03_VGA_CalculateLeadDuration_whenLeadIsAccepted_CalculateLeadDuration() 
    {
        List<Lead> leadToUpdateList = new List<Lead>();

        for(Lead newLead : [SELECT Id, VGA_Lead_Accepted_Date__c FROM Lead]){
            newLead.VGA_Lead_Accepted_Date__c = System.now();
            leadToUpdateList.add(newLead);
        }

        update leadToUpdateList;
        
        List<Lead> updatedLeads = [SELECT Id, VGA_Duration_Hours1__c FROM Lead];
        System.assertNotEquals(updatedLeads[0], null);
    }
}