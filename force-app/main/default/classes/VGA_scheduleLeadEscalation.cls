global class VGA_scheduleLeadEscalation implements Schedulable 
{
    global void execute(SchedulableContext sc) 
    {
        VGA_LeadEscalationBatch b = new VGA_LeadEscalationBatch(); 
        database.executebatch(b,100);
    }
}