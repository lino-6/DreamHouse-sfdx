global class VGA_SandboxManager implements SandboxPostCopy {
    
    global static String SandBoxName;
    
    global void runApexClass(SandboxContext context) {
        
        VGA_SandboxManager.SandBoxName=context.sandboxName();
        insertData();
    }
    
    public static void insertData(){
		VGA_Triggers__c objCustom = VGA_Triggers__c.getAll().get('Case');
        system.debug('objCustom' +objCustom);
        if(objCustom!=null){
            objCustom.VGA_Is_Active__c= false;
        }else{
            objCustom = new VGA_Triggers__c();
            objCustom.Name = 'Case';
            objCustom.VGA_Is_Active__c= false;
        }
       upsert objCustom;
        
        createUser();
        insertPublicHolidays();
        createAccounts();
        createLeads();
        
        objCustom.VGA_Is_Active__c= true;
        update objCustom;
    }
    private static void createUser()
    { 
        try{
            
            map<string,VGA_SandboxUserInfo__c> mapofActionController = VGA_SandboxUserInfo__c.getall();
            if(mapofActionController != null && mapofActionController.containskey(VGA_SandboxManager.SandBoxName))
            { 
                User usr = new User();
                VGA_SandboxUserInfo__c SandboxInfo =  mapofActionController.get(VGA_SandboxManager.SandBoxName);
                usr.ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id;
                usr.LastName = SandboxInfo.VGA_LastName__c;
                usr.FirstName= SandboxInfo.VGA_FirstName__c;
                usr.Email = SandboxInfo.VGA_Email__c;
                Integer randNum = generateRandomInRange(1,20);
                usr.Username = SandboxInfo.VGA_Email__c +'.'+randNum+VGA_SandboxManager.SandBoxName;
                usr.CompanyName = 'Katzion';
                usr.IsActive = True;
                string alias = 'al_'+System.currentTimeMillis();
                usr.Alias = alias.abbreviate(8);
                usr.CommunityNickname='nk_'+System.currentTimeMillis();
                usr.TimeZoneSidKey = 'Australia/Sydney';
                usr.EmailEncodingKey = 'UTF-8';
                usr.LanguageLocaleKey = 'en_US';
                usr.LocaleSidKey = 'en_US';
                Database.DMLOptions dlo=new Database.DMLOptions();
                dlo.EmailHeader.triggerUserEmail =true;
                dlo.EmailHeader.triggerAutoResponseEmail=true;
                usr.setOptions(dlo);
                insert Usr;
            }
            
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
    
    @testVisible
    public static void createLeads(){
        Id skodaFleetRT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
        insert buildLeads('Skoda FC', 2, skodaFleetRT);
        
        Id skodaIndividualRT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
        insert buildLeads('Skoda IC', 2, skodaIndividualRT);
        
        Id volkswagenFleetRT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
        insert buildLeads('Volkswagen FC', 2, volkswagenFleetRT);
        
        Id volkswagenIndividualRT = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        insert buildLeads('Volkswagen IC', 2, volkswagenIndividualRT);
    }
    
    public static Lead buildLead(String namePrefix, String recordTypeId) {
        
        Lead newLead = new Lead();
        newLead.FirstName        = namePrefix + 'First';
        newLead.LastName         = 'LastName';
        newLead.Company          ='TestCompany';
        newLead.Status           ='New';
        newLead.VGA_Model_of_Interest__c = 'test';
        newLead.RecordTypeId     = recordTypeId;  
        return newLead;
    }
    
    /**
* This method builds the given number of leads with given namePrefix
* And returns the list of built records
*/
    public static List<Lead> buildLeads(String namePrefix, Integer numberOfRecords, String recordTypeId) {
        List<Lead> leads = new List<Lead>();
        
        for(Integer i=0; i<numberOfRecords; i++) {
            leads.add(buildLead(namePrefix+i, recordTypeId));
        }
        return leads;
    }
    
    @testVisible
    public static void createAccounts(){
        try{  
            
            List<Account> accsList = new List<Account>();
            List<Contact> cntList = new List<Contact>();
            
            //Id skodaFleetRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Dealer Account').getRecordTypeId();
            //accsList.addAll(buildAccounts('Skoda DA', 2, skodaFleetRT, 'Skoda'));
            Id skodaDealerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Dealer Account').getRecordTypeId();
            accsList.addAll(buildAccounts('Skoda DA', 1, skodaDealerRT, 'Skoda'));
            
            Id volkswagenDealerRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Dealer Account').getRecordTypeId(); 
            accsList.add(buildAccount('Academy Contact Account', volkswagenDealerRT, 'Volkswagen')); 
            accsList.addAll(buildAccounts('Volkswagen DA', 1, volkswagenDealerRT, 'Volkswagen'));
            
            //Id skodaIndividualRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
            //accsList.addAll(buildAccounts('Skoda FC', 1, skodaIndividualRT, 'Skoda'));
            Id skodaFleetRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
            accsList.addAll(buildAccounts('Skoda FC', 1, skodaFleetRT, 'Skoda'));
            
            Id volkswagenFleetRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
            accsList.addAll(buildAccounts('Volkswagen FC', 1, volkswagenFleetRT, 'Volkswagen'));
             
            insert accsList;
            
            for(Account acc : accsList){
                String AccountId = acc.id;
                String AccountName = acc.Name+System.now().millisecond();
                String NamePrefix = AccountName.replace(' ','');
                String AcountBand = acc.VGA_Brand__c;
                cntList.add(createDealerContact(AccountId,NamePrefix,AcountBand));
            }
            
            List<Asset> vehiclesList = new List<Asset>();
            List<Product2> modelsList = new List<Product2>();
            
            modelsList = buildModels('Model',10);
            Database.Insert(modelsList);
            
            Integer i = 0;
            for(Account acc : accsList){
                if(i<modelsList.size()){
                    vehiclesList.add(buildVehicle(acc.Id, modelsList[i].Id));
                    i++;
                } else {
                    i=0;
                    vehiclesList.add(buildVehicle(acc.Id, modelsList[i].Id));
                }   
            }
            
            if(vehiclesList.size()>0)
                insert vehiclesList;
            
            List<VGA_Ownership__c> ownershipList = new List<VGA_Ownership__c>();
            for(Asset vehicle : vehiclesList){
                ownershipList.add(buildOwnership(vehicle.Id,UserInfo.getUserId()));
            }
            insert ownershipList;
            
            List<Account> personAccsList = new List<Account>();
            
            Id skodaIndividualCRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();
            personAccsList.addAll(buildAccounts('Skoda_Individual_Cust', 2, skodaIndividualCRT, 'Skoda'));
            
            Id volkswagenIndividualCRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
            personAccsList.add(buildAccount('Event_Travel_Management_Etm_Individual', volkswagenIndividualCRT, 'Volkswagen')); 
            personAccsList.addAll(buildAccounts('Volkswagen_Individual_Cust', 2, volkswagenIndividualCRT, 'Volkswagen'));
            
            insert personAccsList;
            
            Account eventAcc = [SELECT Id, Name FROM Account WHERE Name ='Event_Travel_Management_Etm_Individual' Limit 1];
            
            List<Academy_Itinerary__c> mcs = Academy_Itinerary__c.getall().values();
            
            for(Academy_Itinerary__c cs : mcs){
                cs.Account_Id__c = eventAcc.Id;
            }
            update mcs;
            
            vehiclesList = new List<Asset>();
            i = 0;
            for(Account acc : personAccsList){
                if(i<modelsList.size()){
                    vehiclesList.add(buildVehicle(acc.Id, modelsList[i].Id));
                    i++;
                } else {
                    i=0;
                    vehiclesList.add(buildVehicle(acc.Id, modelsList[i].Id));
                }   
            }
            insert vehiclesList;
            List<Case> interactionList = buildInteractions(vehiclesList,accsList);
            insert interactionList;
        }
        catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
    Public static Contact createDealerContact(String AccountID,String AccountName,String AccountBrand) 
    {
        
        Contact objContact         = new Contact();
        objContact.FirstName       = AccountName + 'FirstName';
        objContact.LastName        = AccountName + 'LastName';
        objContact.VGA_Role__c     = 'Consultant';
        objContact.VGA_Brand__c    = AccountBrand;
        objContact.VGA_Sub_Brand__c= 'Passenger Vehicles (PV)';
        objContact.AccountId       = AccountID;
        objContact.Email           = AccountName + '@testorg.com';
        objContact.MobilePhone     = '9999999999';
        objContact.VGA_Available_to_Receive_Leads__c=true;
        Return objContact;
    }
    public static Account buildAccount(String namePrefix, String recordTypeId, String brand) {
        
        Account newAccount     = New Account();
        newAccount.RecordTypeId=recordTypeId;
        
        if(namePrefix.contains('Individual')){
            newAccount.LastName     = namePrefix;
            newAccount.PersonEmail  = namePrefix + '@test.com';
            newAccount.Phone = '9999999999';
        } else {
            newAccount.Name     = namePrefix;
            newAccount.VGA_Brand__c     =brand;
            newAccount.VGA_Sub_Brand__c ='Passenger Vehicles (PV)'; 
            newAccount.VGA_Dealership_Type__c = 'Metro';
            newAccount.VGA_Timezone__c  = 'Australia/Sydney';
        }
        return newAccount;
    }
    
    /**
* This method builds the given number of leads with given namePrefix
* And returns the list of built records
*/
    public static List<Account> buildAccounts(String namePrefix, Integer numberOfRecords, String recordTypeId, String brand) {
        
        List<Account> accs = new List<Account>();
        
        for(Integer i=0; i<numberOfRecords; i++) {
            accs.add(buildAccount(namePrefix+i, recordTypeId, brand));
        }
        return accs;
    }
    
    public static Asset buildVehicle(Id accId, Id modelId) {
        
        Asset newVehicle = new Asset();
        String vin = 'WV1ZZZ7HZ9H'+generateRandomInRange(100000, 999999);
        newVehicle.Name           = vin;
        newVehicle.VGA_Colour__c  = 'GREY WHITE';
        newVehicle.VGA_Engine__c  = 'BPC 058100';
        newVehicle.AccountId      = accId;
        newVehicle.VGA_VIN__c     = vin;
        newVehicle.VGA_Model__c   = modelId;
        newVehicle.VGA_Model_Card_No__c = 'TestCar' + generateRandomInRange(100000, 999999);
        return newVehicle;
    }
    /**
* This method builds the given number of leads with given namePrefix
* And returns the list of built records
*/
    public static List<Product2> buildModels(String NamePrefix,Integer NoOfRecords) {
        
        List<Product2> modelsList = new List<Product2>();
        for (Integer i = 0 ;  i<=NoOfRecords; i++)
        { 
            Product2 objProduct = new Product2();
            objProduct.name = NamePrefix+i;  
            objProduct.ProductCode = ''+generateRandomInRange(2000,4000);
            modelsList.add(objProduct);  
        }
        return modelsList;
    }
    
    public static VGA_Ownership__c buildOwnership(Id vehicleId, Id ownerId){
        
        VGA_Ownership__c ownerShip = new VGA_Ownership__c();
        ownerShip.OwnerId    = ownerId;
        ownerShip.VGA_VIN__c = vehicleId;
        return ownerShip;
    }
    
    // Random integer within range
    private static Integer generateRandomInRange(Integer bound1, Integer bound2) {
        Integer min = Math.min(bound1, bound2);
        Integer max = Math.max(bound1, bound2);
        Integer r = min + (Integer)(Math.random() * (max - min + 1));
        return r;
    }
    
    public static List<Case> buildInteractions(List<Asset> relatedVehicles,List<account> dealerAccountList) {
       
        List<Case> interactionsToInsert = new List<Case>();
        
        Id academyRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();
        
        for(Asset vehicle : relatedVehicles){
            
            string ContactId = [Select id,name from Contact where AccountId =:vehicle.AccountId limit 1 ].id;
            Case newInteraction = new Case();
            newInteraction.AccountId    = vehicle.AccountId;
            newInteraction.RecordTypeId = academyRT;
            newInteraction.Status = 'New';
            newInteraction.ContactId = ContactId;
            newInteraction.VGA_Category_1__c = 'Sales Product Training';
            newInteraction.VGA_Category_2__c = 'Bookings';
            newInteraction.Origin       = 'Academy Email';
            newInteraction.Subject      = 'FW: Itinerary: Volkswagen Advanced Technician 1, 07th - 10th May, Melbourne: WYSZYNSKI/JOSHUA MR - Revised';
            newInteraction.Description  = 'Dear Joshua, \n The attached itinerary has been confirmed for you as part of the Volkswagen Advanced Technician 1, 07th - 10th May, Melbourne. \n If you require any changes to your booking, please contact Volkswagen Group Academy at academy@volkswagen.com.au<mailto:academy@volkswagen.com.au>';
            newInteraction.Priority     = 'Medium';
            newInteraction.SuppliedEmail= 'test@travelctm.com';
            interactionsToInsert.add(newInteraction);
        }
        
        Id  buyBackRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Buy Back').getRecordTypeId();
        boolean z=true;
        for(Asset vehicle : relatedVehicles){
            Case newInteraction = new Case();
            newInteraction.AccountId    = vehicle.AccountId;
            newInteraction.VGA_Dealer__c =z? dealerAccountList[0].Id:dealerAccountList[1].Id;
            newInteraction.VGA_Brand__c ='PV Volkswagen';
            newInteraction.VGA_Customer_Type__c = 'Owner';
            newInteraction.Status = 'New';
            newInteraction.RecordTypeId = buyBackRT;
            newInteraction.Origin       = 'VW Email';
            newInteraction.VGA_Description_of_Enquiry__c = 'Customer is seeking a replacement vehicle due to ongoing concerns with high oil consumption - Vehicle requires replacement base engine';
            newInteraction.VGA_Confirmation_of_Enquiry__c= 'Customer complaint of high oil consumption - Has had tests carried out, and confirmed that base engine is required now - ealer has emailed through a copy of buyback calculator - See attached';
            newInteraction.Priority     = 'Medium';
            newInteraction.SuppliedEmail= 'test@travelctm.com';
            newInteraction.VGA_Category_1__c = 'Assisted Buyback';
            newInteraction.AssetId      = vehicle.Id;
            interactionsToInsert.add(newInteraction);
            z=z?false:true;
        }
        
        Id  enquiryRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Enquiry').getRecordTypeId();
        
        for(Asset vehicle : relatedVehicles){
            Case newInteraction = new Case();
            newInteraction.AccountId    = vehicle.AccountId;
            newInteraction.RecordTypeId = enquiryRT;
            newInteraction.Origin       = 'VW Email';
            newInteraction.Subject      = 'Coles Services Daily Report# 21/09/2018 08:00:00 To: 24/09/2018 08:00';
            newInteraction.Priority     = 'Medium';
            newInteraction.SuppliedEmail= 'test@travelctm.com';
            newInteraction.VGA_Description_of_Enquiry__c = 'Customer is seeking a replacement parts due to ongoing concerns with high oil consumption';
            newInteraction.VGA_Category_1__c = 'After Sales';
            newInteraction.VGA_Category_2__c = 'Parts';
            newInteraction.AssetId      = vehicle.Id;
            
            interactionsToInsert.add(newInteraction);
        }
        
        Id  goodwillRT = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Goodwill').getRecordTypeId();
        
        for(Asset vehicle : relatedVehicles){
            Case newInteraction = new Case();
            newInteraction.AccountId    = vehicle.AccountId;
            newInteraction.RecordTypeId = goodwillRT;
            newInteraction.Origin       = 'Goodwill VW';
            newInteraction.Subject      = 'Re: WVWZZZAAZDD032703 - 20214 - Goodwill';
            newInteraction.Priority     = 'Medium';
            newInteraction.Status = 'New';
            newInteraction.SuppliedEmail= 'test@travelctm.com';
            newInteraction.VGA_Customer_Type__c = 'Owner';
            newInteraction.AssetId      = vehicle.Id;
            newInteraction.VGA_Kilometers__c =  generateRandomInRange(1000, 100000);
            newInteraction.VGA_Category_1__c = 'Goodwill Enquiry';
            newInteraction.Description  = 'Just submitted goodwill on the above vehicle forgot to mention the DISS # 103208016 is applicable \n Sorry for the omission \n Kind regards';
            
            interactionsToInsert.add(newInteraction);
        }
        return interactionsToInsert;
    }
    
    public static void insertPublicHolidays(){
        List<VGA_Public_Holiday__c> publicHolidayList = new List<VGA_Public_Holiday__c> {new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Anzac Day',VGA_Holiday_Date__c  = date.parse('25/04/2018'),VGA_Holiday_Type__c  = 'Recurring',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Boxing Day',VGA_Holiday_Date__c  = date.parse('26/12/2018'),VGA_Holiday_Type__c  = 'Recurring',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Christmas Day',VGA_Holiday_Date__c  = date.parse('25/12/2018'),VGA_Holiday_Type__c  = 'Recurring',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Australia Day',VGA_Holiday_Date__c  = date.parse('26/01/2019'),VGA_Holiday_Type__c  = 'Recurring',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'New Years Day',VGA_Holiday_Date__c  = date.parse('1/01/2019'),VGA_Holiday_Type__c  = 'Recurring',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('7/05/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('7/05/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'QLD'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Reconciliation Day',VGA_Holiday_Date__c  = date.parse('28/05/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'ACT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Western Australia Day',VGA_Holiday_Date__c  = date.parse('4/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'WA')};
            insert publicHolidayList;
        
        List<VGA_Public_Holiday__c> publicHolidayList1 = new List<VGA_Public_Holiday__c> {new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'ACT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NSW'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'SA'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queens Birthday',VGA_Holiday_Date__c  = date.parse('11/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'VIC'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Borroloola Show Day',VGA_Holiday_Date__c  = date.parse('29/06/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Alice Springs Show Day',VGA_Holiday_Date__c  = date.parse('6/07/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Tennant Creek Show Day',VGA_Holiday_Date__c  = date.parse('13/07/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Katherine Show Day',VGA_Holiday_Date__c  = date.parse('20/07/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Darwin Show Day',VGA_Holiday_Date__c  = date.parse('27/07/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Picnic Day',VGA_Holiday_Date__c  = date.parse('6/08/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Ekka Wednesday',VGA_Holiday_Date__c  = date.parse('15/08/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'QLD'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queen\'s Birthday',VGA_Holiday_Date__c  = date.parse('24/09/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'WA'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'AFL Grand Final Friday',VGA_Holiday_Date__c  = date.parse('28/09/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'VIC'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('1/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'ACT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('1/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NSW'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('1/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'SA'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Queen\'s Birthday',VGA_Holiday_Date__c  = date.parse('1/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'QLD')};
            insert publicHolidayList1;
        
        List<VGA_Public_Holiday__c> publicHolidayList2 = new List<VGA_Public_Holiday__c> {new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Burnie Show',VGA_Holiday_Date__c  = date.parse('5/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Royal Launceston Show',VGA_Holiday_Date__c  = date.parse('11/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Flinders Island Show',VGA_Holiday_Date__c  = date.parse('19/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Royal Hobart Show',VGA_Holiday_Date__c  = date.parse('25/10/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Recreation Day',VGA_Holiday_Date__c  = date.parse('5/11/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Melbourne Cup Day',VGA_Holiday_Date__c  = date.parse('6/11/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'VIC'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Devonport Show',VGA_Holiday_Date__c  = date.parse('30/11/2018'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Devonport Cup',VGA_Holiday_Date__c  = date.parse('9/01/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Royal Hobart Regatta',VGA_Holiday_Date__c  = date.parse('11/02/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Launceston Cup',VGA_Holiday_Date__c  = date.parse('27/02/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('5/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'WA'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'King Island Show',VGA_Holiday_Date__c  = date.parse('6/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Canberra Day',VGA_Holiday_Date__c  = date.parse('11/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'ACT')};
            insert publicHolidayList2;
        
        List<VGA_Public_Holiday__c> publicHolidayList3 = new List<VGA_Public_Holiday__c> {new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'March Public Holiday',VGA_Holiday_Date__c  = date.parse('11/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'SA'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Eight Hours Day',VGA_Holiday_Date__c  = date.parse('11/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Labour Day',VGA_Holiday_Date__c  = date.parse('11/03/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'VIC'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Australia Day Holiday',VGA_Holiday_Date__c  = date.parse('28/01/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Good Friday',VGA_Holiday_Date__c  = date.parse('19/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Sunday',VGA_Holiday_Date__c  = date.parse('21/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'ACT'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Sunday',VGA_Holiday_Date__c  = date.parse('21/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'NSW'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Sunday',VGA_Holiday_Date__c  = date.parse('21/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'QLD'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Sunday',VGA_Holiday_Date__c  = date.parse('21/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'VIC'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Monday',VGA_Holiday_Date__c  = date.parse('22/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'All'),new VGA_Public_Holiday__c(VGA_Holiday_Name__c  = 'Easter Tuesday',VGA_Holiday_Date__c  = date.parse('23/04/2019'),VGA_Holiday_Type__c  = 'Single',VGA_State_Region__c  = 'TAS')};
            insert publicHolidayList3;
    }
}