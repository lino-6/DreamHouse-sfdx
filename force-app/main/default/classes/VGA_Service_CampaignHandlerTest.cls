@isTest
private class VGA_Service_CampaignHandlerTest
{
    @testsetup
    static void buildServiceCampaigndetails()
    {    
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'VGA_Service_Campaigns__c';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom; 
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        Asset objAsset = VGA_CommonTracker.createAsset('test');
        objAsset.VGA_VIN__c = 'WVWZZZAUZJP002582';
        objAsset.AccountId = objAcc.id;
        insert objAsset;
        
        VGA_Service_Campaigns__c  objservicecampaign  = VGA_CommonTracker.buildTakataServiceCampaing();
        objservicecampaign.VGA_SC_Code__c    = '69Q7';
        objservicecampaign.VGA_SC_Vehicle__c = objAsset.Id;
        
        insert objservicecampaign;
    }  
    
    /**
    * @author Ganesh M
    * @date 25/10/2018
    * @description Method that tests updated driver_airbag details for completed status takata recalls.
    */
    static testMethod void test01_UpdateServiceCampaigns_whenStatusupdated() 
    {      
        
        Test.startTest();
        
        VGA_Service_Campaigns__c sc=[select id, VGA_SC_Vehicle__c,VGA_SC_Vin__c,driver_airbag_date_replaced__c  from VGA_Service_Campaigns__c  where  VGA_SC_Vin__c  ='12345'];
        
        sc.VGA_SC_Status__c = 'completed';
        sc.VGA_SC_Completetion_Date__c = system.today();
        sc.driver_airbag_date_replaced__c = system.today() ;
        update sc;
        
        Test.stopTest();
        VGA_Service_Campaigns__c sc1=[select id, VGA_SC_Vehicle__c,VGA_SC_Vin__c,driver_airbag_date_replaced__c  from VGA_Service_Campaigns__c  where  Id = :sc.Id];
        System.assertEquals(sc.driver_airbag_date_replaced__c,system.today());
    }
    
  
    
    
     /**
    * @author Ganesh M
    * @date 22/03/2019
    * @description Method that tests Inserting Mandatory Fields when Mule Processed.
    */
    static testMethod void test02_InsertMantoryFields_whenMuleprocessed() 
    {      
        
        Test.startTest();
        VGA_Service_Campaigns__c objservicecampaign = new VGA_Service_Campaigns__c();
      
        objservicecampaign.VGA_SC_Code__c = '69Q7';
        objservicecampaign.Name__c = 'Emissions';
        objservicecampaign.VGA_SC_Vin__c = '123456';
        objservicecampaign.VGA_SC_Sub_Brand__c = 'PV';
        objservicecampaign.VGA_SC_Brand__c = 'Volkswagen';
        objservicecampaign.VGA_SC_Description__c = 'test';
        objservicecampaign.Name__c = 'Takata';
        objservicecampaign.VGA_External_ID__c = '12345';
        insert objservicecampaign;
        
        VGA_Service_Campaigns__c objservicecampaign1 = new VGA_Service_Campaigns__c();
       
        objservicecampaign1.VGA_SC_Code__c = '69Q7';
        objservicecampaign1.VGA_SC_Vin__c = '123456';
        objservicecampaign1.VGA_SC_Sub_Brand__c = 'PV';
        objservicecampaign1.VGA_SC_Brand__c = 'Volkswagen';
        objservicecampaign1.VGA_SC_Description__c = 'test';
        objservicecampaign1.Name__c = 'Emissions';
        objservicecampaign1.VGA_External_ID__c = '123';
        insert objservicecampaign1;
       
       
        list<VGA_Service_Campaigns__c> sc=[select id,VGA_SC_Code__c, VGA_SC_Vehicle__c,VGA_SC_Vin__c,driver_airbag_date_replaced__c,RecordType.name,Campaign_Code__c  from VGA_Service_Campaigns__c  where  VGA_SC_Vin__c  ='123456'];
       
        Test.stopTest();
        
        System.assertEquals(sc[0].RecordType.name,'Takata');
      
    }
    
    
}