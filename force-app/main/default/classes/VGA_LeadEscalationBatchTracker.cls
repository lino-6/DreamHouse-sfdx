//=================================================================================
// Tracker class for VGA_LeadEscalationBatch class.
// =================================================================================
// Created by Nitisha Prasad on 22-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_LeadEscalationBatchTracker
{
	public static lead objLead;
	public static Account objAcc;
	public static user objUser2;
	public static Contact objCon2;
	public static Contact objCon;
	public static VGA_Dealership_Profile__c objDealer;
	public static VGA_User_Profile__c objUserPro;
	public static VGA_User_Profile__c objUserPro2;
	public static VGA_Working_Hour__c objWork;
	public static VGA_Working_Hour__c objWork3;
	public static User objUser;
	public static VGA_Subscription__c objSubscription;
	 
	static testmethod void unittestPublicHolidaySingle()
	{
		loadData();
		
		VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','All');   
       	insert objPH;
		
		VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
	}
	static testMethod void unittestPublicHolidayRecurring() 
    {
        LoadData();
       
       	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','All');   
       	insert objPH;
       	
        
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
	
    }
    static testMethod void myUnitTestPublicHolidayRecurringState() 
    {
        LoadData();
       
       	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Recurring','ACT');   
       	insert objPH;
       	
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
	
    } 
    static testMethod void myUnitTestPublicHolidaySingleState() 
    {
        LoadData();
       
       	VGA_Public_Holiday__c objPH = VGA_CommonTracker.createPublicHoliday(system.today(),'test','Single','ACT');   
       	insert objPH;
       	
        VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
    }
	static testmethod void unittest1()
	{
		loadData();
		
		VGA_Notification_Template__c objCS = new VGA_Notification_Template__c();
        objCS.Name = 'All Escalations';
        objCS.VGA_Email_Body__c = 'hi';
        objCS.VGA_Email_From_Address__c = 'rishi.patel@saasfocus.com';
        objCS.VGA_SMS_Body__c = 'hi';
        objCS.VGA_SMS_From_Id__c = 'rishi.patel@saasfocus.com';
        insert objCS;
		
		objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'All Escalations';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
		
		VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
	}
	static testmethod void unittest2()
	{
		loadData();
		
		VGA_Notification_Template__c objCS = new VGA_Notification_Template__c();
        objCS.Name = 'New Lead Notification';
        objCS.VGA_Email_Body__c = 'hi';
        objCS.VGA_Email_From_Address__c = 'rishi.patel@saasfocus.com';
        objCS.VGA_SMS_Body__c = 'hi';
        objCS.VGA_SMS_From_Id__c = 'rishi.patel@saasfocus.com';
        insert objCS;
		
		objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'New Lead Notification';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
		
		objLead.VGA_Escalation__c =1;
		objLead.FirstName ='Rishi';
		update objLead;
			
		VGA_LeadEscalationBatch obj  = new VGA_LeadEscalationBatch();
		Database.executeBatch(obj);
	}
	
	
	public static void loadData()
	{
		objAcc = VGA_CommonTracker.createDealerAccount();
        objAcc.VGA_Timezone__c = 'Australia/Sydney';
        objAcc.VGA_Dealer_Code__c = '1234';
        objAcc.billingstate='ACT';
        update objAcc;
        
        objDealer = new VGA_Dealership_Profile__c();
        objDealer.Name = 'Passenger Vehicles (PV)';
        objDealer.VGA_Dealership__c = objAcc.id;
        objDealer.VGA_Escalate_Minutes__c = 20;
        insert objDealer;
        
        objCon = VGA_CommonTracker.createDealercontact(objAcc.id);
        
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Contact__c = objCon.id;
        objUserPro.VGA_Available__c = true;
        insert objUserPro;
        
        objUser = VGA_CommonTracker.CreateUser(objCon.id);
        
        Id idgetDealerContact = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact'); 
         objCon2 = new Contact();
         
            
            objCon2.FirstName ='TestDeal333er';
            objCon2.LastName ='TestDeal33333er';
            objCon2.RecordTypeId=idgetDealerContact;
            objCon2.VGA_Role__c ='Consultant';
            objCon2.VGA_Brand__c='Volkswagen';
            objCon2.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
            objCon2.AccountId=objAcc.id;
            objCon2.VGA_Available_to_Receive_Leads__c=true;
            objCon2.Email ='testt344444444432@live.com';
           objCon2.MobilePhone='21111111131111111';
            //objContact.VGA_Available__c = true;
            insert objCon2;  
            
        objUserPro2 = VGA_CommonTracker.createUserProfile('test');
        objUserPro2.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objUserPro2.VGA_Brand__c = 'Volkswagen';
        objUserPro2.VGA_Contact__c = objCon2.id;
        objUserPro2.VGA_Available__c = true;
        insert objUserPro2;
        
        objWork3 = VGA_CommonTracker.createWorkingHour('Tuesday','08:30 AM','01:30 PM');
        objWork3.VGA_User_Profile__c = objUserPro2.id;
        objWork3.Name = System.Now().format('EEEE');
        objWork3.VGA_Working__c = true;
        objWork3.VGA_End_Time__c = '11:00 PM';
        objWork3.VGA_Start_Time__c ='07:00 AM';
        insert objWork3;
        
        VGA_Working_Hour__c objWork4 = [select id, VGA_End_Time_Date_Time__c,VGA_Start_Date_Date_Time__c from 
        								VGA_Working_Hour__c where VGA_User_Profile__c =:  objUserPro2.id];
        
        objWork = VGA_CommonTracker.createWorkingHour('Tuesday','08:30 AM','01:30 PM');
        objWork.VGA_User_Profile__c = objUserPro.id;
        objWork.Name = System.Now().format('EEEE');
        objWork.VGA_Working__c = true;
        objWork.VGA_End_Time__c = '11:00 PM';
        objWork.VGA_Start_Time__c ='07:00 AM';
        insert objWork;
        
        profile p = [SELECT Id FROM Profile WHERE Name='Volkswagen Dealer Consultant Profile' LIMIT 1];
         	 objUser2 = new User();
            objUser2.ContactId =objCon2.id;
            objUser2.ProfileId = p.Id;
            objUser2.username = 'tes2333t@test.com123';
            objUser2.FirstName = 'First';
            objUser2.LastName = '23232';
            objUser2.email = 'test@test.com';
            objUser2.VGA_Brand1__c = 'Volkswagen';
            String alias = objUser2.username;
            if(alias.length() > 5){
                alias = alias.substring(0,5);
            }
            objUser2.alias = alias;
            objUser2.languagelocalekey =' en_US';
            objUser2.localesidkey = 'en_US';
            objUser2.emailencodingkey = 'UTF-8';
            objUser2.TimezoneSidKey = 'America/Los_Angeles';
            
            insert objUser2;
		
		objLead = new Lead();
        objLead.Status = 'New';
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.LastName = 'Prasad';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Dealer_Code__c = '1234';
        objLead.VGA_Dealer_Account__c = objAcc.id;
        objLead.VGA_Assignment_Done__c = true;
        objLead.VGA_Escalated_Date_Time__c = System.now().addDays(-10);
        objLead.OwnerId = objUser2.id;
        insert objLead;
	}
    //===========================================================================================
    //Description:Test class for leadEscalationReschedule method.
    //===========================================================================================
    //Name:Ganesh M      Date:29-11-2018   
    //===========================================================================================
     static testMethod void myUnitTest3() 
    {
         Test.startTest();
          
        VGA_scheduleLeadEscalation obj = new VGA_scheduleLeadEscalation();
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('LeadAssignmentJob', CRON_EXP, obj);
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));  
        Test.stopTest(); 
    }
    
}