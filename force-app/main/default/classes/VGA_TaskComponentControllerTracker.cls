//Tracker class for VGA_TaskComponentController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for VGA_TaskComponentController on 				8-Nov-2017		90%
//===============================================================================================
@isTest(seeAllData = false)       
public class VGA_TaskComponentControllerTracker 
{
    public static testMethod void Test1()
    {
         Account objAccount =VGA_CommonTracker.createDealerAccount();
         Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
         Task objTask       =VGA_CommonTracker.createTask(objContact.Id);
         User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
         VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
         objProfile.VGA_Role__c = 'Dealer Administrator';
         objProfile.VGA_Contact__c = objUser.ContactId;
         insert objProfile;
         string strTask = VGA_TaskComponentController.updatetaskDetails(objTask);
         VGA_TaskComponentController.updateTaskstatus(objTask.Id);
         VGA_TaskComponentController.getAttachmentlist(objTask.Id);
         system.runAs(objUser)
         {
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Quarter','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('This Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Tomorrow','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today + Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('All Open','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Next 7 Days','createdDate',false);
         VGA_TaskComponentController.getlogginguser();
         VGA_TaskComponentController.gettaskDetails(objTask.Id); 
        }      
    }
    public static testMethod void Test2()
    {
         Account objAccount =VGA_CommonTracker.createDealerAccount();
         Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
         Task objTask       =VGA_CommonTracker.createTask(objContact.Id);
         User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
         VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
         objProfile.VGA_Role__c = 'Consultant';
         objProfile.VGA_Contact__c = objUser.ContactId;
         insert objProfile;
         system.runAs(objUser)
         {
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Quarter','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('This Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Tomorrow','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today + Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('All Open','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Next 7 Days','createdDate',false);
         }
    }
    public static testMethod void Test3()
    {
         Account objAccount =VGA_CommonTracker.createDealerAccount();
         Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
         Task objTask       =VGA_CommonTracker.createTask(objContact.Id);
         User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
         VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
         objProfile.VGA_Role__c = 'Lead Controller';
         objProfile.VGA_Contact__c = objUser.ContactId;
         insert objProfile;
         system.runAs(objUser)
         {
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Quarter','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('This Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Tomorrow','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today + Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('All Open','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Next 7 Days','createdDate',false);
         }
    }
    public static testMethod void Test4()
    {
         Account objAccount =VGA_CommonTracker.createDealerAccount();
         Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
         Task objTask       =VGA_CommonTracker.createTask(objContact.Id);
         User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
         VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
         objProfile.VGA_Role__c = 'Dealer Portal Group';
         objProfile.VGA_Contact__c = objUser.ContactId;
         insert objProfile;
         system.runAs(objUser)
         {
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Closed Tasks this Quarter','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('This Month','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Tomorrow','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today + Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Overdue','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Today','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('All Open','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('Next 7 Days','createdDate',false);
          VGA_TaskComponentController.getTasksMethod('All Closed Tasks','createdDate',false);
             
         }
    }

}