@isTest
public class VGA_TestDriveCalendarControllerTest {
    
    @TestSetup
    public static void buildConfigList()
    {
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Lead';
        objCustom.VGA_Is_Active__c= true;
        CustomList.add(objCustom);
        VGA_Triggers__c objCustom1 = new VGA_Triggers__c();
        objCustom1.Name = 'Account';
        objCustom1.VGA_Is_Active__c= true;
        CustomList.add(objCustom1);
        VGA_Triggers__c objCustom2 = new VGA_Triggers__c();
        objCustom2.Name = 'Contact';
        objCustom2.VGA_Is_Active__c= true;
        CustomList.add(objCustom2);
        insert CustomList;
    }
    
    Public static Event buildEvent(String eventId,Datetime startDate,Datetime endDate)
    {
        Event objEvent = new Event();
        objEvent.WhoId =eventId;
        objEvent.StartDateTime = startDate ;
        objEvent.EndDateTime = endDate;
        objEvent.Subject = 'Test';
        return objEvent;
    }
    
 /**
* @author Hijith NS
* @date 19/03/2019
* @description Test method to test all the methods in VGA_TestDriveCalendarController.
*              It tests the loggedInUsers event creation and update methods.
*/
    
    @isTest
    Public static void test01_testAllMethodsInVGA_TestDriveCalendarController(){
        
        List<Event> eventList = new List<Event>();
        List<Event> eventList2 = new List<Event>();
        Event eventobj = new Event();
        DateTime oldStartDate,newStartDate,oldEndDate,newEndDate;
        String eventid,id;
        
        
        //Creating Account
        Account ObjAcc=VGA_CommonTracker.buildDealerAccount('TestDealerAccount','2211');
        ObjAcc.VGA_Pilot_Dealer__c=true;
        insert ObjAcc;
        
        //Creating Contact
        Contact ObjCnt=VGA_CommonTracker.buildDealerContact(ObjAcc.Id,'TestDealerContact');
        ObjCnt.VGA_Role__c = 'Dealer Administrator';        
        ObjCnt.VGA_Available_to_Receive_Leads__c=true;
        insert ObjCnt;
        
        //User profile 
        List<VGA_User_Profile__c> userProfileList=[SELECT Id,Name,VGA_Contact__c,VGA_Available_TestDrive__c FROM VGA_User_Profile__c];
        for(VGA_User_Profile__c usr:userProfileList){
            usr.VGA_Available_TestDrive__c=true;
            usr.VGA_Role__c = 'Dealer Administrator';
        }
        update userProfileList;
        
        //Creating User 
        User PortalUser=VGA_CommonTracker.buildUser(ObjCnt.Id);
        insert PortalUser;
        String testDriveRecordTypeID = Schema.SObjectType.Event.getRecordTypeInfosByName().get('Test Drive Appointments').getRecordTypeId();  
        
        Test.startTest();
        System.runAs(PortalUser){
            Lead objLead = VGA_CommonTracker.buildLead('Test');
            Insert objLead; 
            
            oldStartDate =system.now().addDays(1);
            oldEndDate = oldStartDate.addMinutes(45);
            
            Event objEvent = buildEvent(objLead.id,oldStartDate,oldEndDate);           
            objEvent.RecordTypeId= testDriveRecordTypeID ;            
            insert objEvent;
            eventid = objEvent.id;
            newStartDate = system.now().addDays(2);
            id =UserInfo.getUserId();        
            VGA_TestDriveCalendarController.createEvent(eventid, ''+newStartDate);
            VGA_TestDriveCalendarController.updateEvent(eventid, ''+newStartDate);
            VGA_TestDriveCalendarController.updateEventEndDate(eventid, ''+newStartDate);
            VGA_TestDriveCalendarController.collectUsersInfo();
            VGA_TestDriveCalendarController.getlistofUser();
            eventList = VGA_TestDriveCalendarController.getAllEvents(id);
            eventobj = VGA_TestDriveCalendarController.collectEventDetails(eventid);
        }
        Test.stopTest();
        
        Event evntobj = [select StartDateTime,EndDateTime from event where id =:eventid];
        
        System.assert(eventList.size()>0);        
        System.assert(eventobj!=null);
        System.assertNotEquals(newStartDate,newEndDate);
        System.assertNotEquals(oldStartDate,oldEndDate);
        
    }
}