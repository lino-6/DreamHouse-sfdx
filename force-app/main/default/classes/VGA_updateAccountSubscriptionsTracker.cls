//=================================================================================
// Tracker class for VGA_updateAccountSubscriptions class.
// =================================================================================
// =================================================================================
@isTest(seeAllData = false)
private class VGA_updateAccountSubscriptionsTracker
{
    public static VGA_updateAccountSubscriptions objBatch;
    public static Account objAccount2 ;
    public static Account objAccount3; 
    public static Account objAccount4;
    public static void LoadData()
    {
        objBatch = new VGA_updateAccountSubscriptions();
        objAccount2 =new account();   
        objAccount2.RecordTypeId = VGA_Common.GetRecordTypeId('Account','Volkswagen Individual Customer');
        objAccount2.VGA_Owner__c = 'Yes';
        objAccount2.PersonEmail='90876543@live.com';
        objAccount2.VGA_Brand__c='Volkswagen';
        objAccount2.LastName='TEST1.1';
        objAccount2.VGA_Subscription_Batch__c=TRUE;
        insert objAccount2;
        objAccount2.VGA_Marketing_Opt_In__c=true;
        update objAccount2 ;
         objAccount3 =new account();   
        objAccount3.RecordTypeId = VGA_Common.GetRecordTypeId('Account','Volkswagen Individual Customer');
        objAccount3.PersonEmail='90876543@live.com';
        objAccount3.VGA_Brand__c='Volkswagen';
        objAccount3.LastName='TEST1.2';
        objAccount3.VGA_Subscription_Batch__c=false;
        insert objAccount3;
        objAccount3.VGA_Marketing_Opt_In__c=true;
        update objAccount3;
        
        objAccount4 =new account();   
        objAccount4.RecordTypeId = VGA_Common.GetRecordTypeId('Account','Volkswagen Individual Customer');
        objAccount4.VGA_Owner__c = 'Yes';
        objAccount4.PersonEmail='90876543@live.com';
        objAccount4.VGA_Brand__c='Volkswagen';
        objAccount4.LastName='TEST1.1';
        objAccount4.VGA_Subscription_Batch__c=TRUE;
        insert objAccount4;
        objAccount4 =new account();   
        objAccount4.RecordTypeId = VGA_Common.GetRecordTypeId('Account','Volkswagen Individual Customer');
        objAccount4.VGA_Owner__c = 'Yes';
        objAccount4.PersonEmail='90876544334433@live.com';
        objAccount4.VGA_Brand__c='Volkswagen';
        objAccount4.LastName='TEST1.1';
        objAccount4.VGA_Subscription_Batch__c=TRUE;
        insert objAccount4;
    }
    static testMethod void myUnitTest() 
    {
        LoadData();
       
        Test.startTest();
        
        
        Database.executeBatch(objBatch);
        
        Test.stopTest();
    } 
}