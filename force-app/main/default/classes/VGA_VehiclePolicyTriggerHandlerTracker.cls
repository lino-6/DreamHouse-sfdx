@isTest(seeAllData = false)
private class VGA_VehiclePolicyTriggerHandlerTracker
{
    @testsetup
    Static void buildConfigList()
    {     
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'VGA_Vehicle_Policy__c';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        VGA_Triggers__c objCustom1 = new VGA_Triggers__c();
        objCustom1.Name = 'VGA_Allianz_Data_Mapping__c';
        objCustom1.VGA_Is_Active__c= true;
        insert objCustom1;

        Account obj_account = VGA_CommonTracker.buildPersonAccount('Volkswagen Individual Customer', 'testIndividual');
        insert obj_account;
        
        Asset obj_asset = VGA_CommonTracker.buildVehicle('WVWZZZ1KZ5U026277', obj_account.id);
        insert obj_asset;
        
        VGA_Ownership__c obj_ownership = VGA_CommonTracker.buildOwnership(obj_asset.id, UserInfo.getUserId());
        obj_ownership.VGA_Type__c          = 'Current Owner';
        obj_ownership.VGA_Purchase_Date__c = date.ValueOf('2019-01-10');
        insert obj_ownership;        
        
        obj_ownership = VGA_CommonTracker.buildOwnership(obj_asset.id, UserInfo.getUserId());
        obj_ownership.VGA_Type__c = 'Previous Owner';
        obj_ownership.VGA_Purchase_Date__c = date.ValueOf('2018-01-10');
        insert obj_ownership;
    } 

    /**
    * @author Ganesh
    * @date 28/05/2018
    * @description Method that tests the updateLiteratureCode on the VGA_VehiclePolicyTriggerHandler class.
    *              After a new VehiclePolicy is created/update we will update the related ownership record
    *              based on some defined conditions.
    */
    static testMethod void test01_VGA_updateLiteratureCode_whenPolicyCreatedOrUpdated_UpdateOwnershipLiteratureCode() 
    {      
        Test.startTest();
        
        Asset obj_asset = [SELECT Id, Name, VGA_VIN__c, AccountId FROM Asset LIMIT 1];
                
        VGA_Vehicle_Policy__c objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c          = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c      = 'Passenger Vehicles (PV)';
        objVehicle.VGA_Component_Code__c = '';
        objVehicle.VGA_VIN__c            = obj_asset.id;
        
        insert objVehicle;    
       
        objVehicle.VGA_Term__c = '12';
        update objVehicle;
        
        objVehicle.VGA_Brand__c          = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c      = 'Passenger Vehicles (PV)';
        objVehicle.VGA_Component_Code__c = '';
        objVehicle.VGA_VIN__c            = obj_asset.id;
        update objVehicle;
       
        
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objVehicle.VGA_Component_Code__c = 'SP3YR';
        objVehicle.VGA_VIN__c = obj_asset.id;
        insert objVehicle; 
        
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objVehicle.VGA_Component_Code__c = 'SP5YR';
        objVehicle.VGA_VIN__c = obj_asset.id;
        insert objVehicle;     
       
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objVehicle.VGA_VIN__c = obj_asset.id;
        insert objVehicle; 

        VGA_Ownership__c obj_ownership = [SELECT Id, VGA_Literature_Code__c FROM VGA_Ownership__c WHERE VGA_Type__c = 'Previous Owner'];
        obj_ownership.VGA_Literature_Code__c = 'SKOWEL004-A';
        
        Test.stopTest();
        System.assert(obj_ownership.VGA_Literature_Code__c=='SKOWEL004-A','Previous Owner'+obj_ownership.VGA_Literature_Code__c);
    } 

    /**
    * @author Ganesh
    * @date 28/05/2018
    * @description Method that tests the populateAllianzDataMapping on the VGA_VehiclePolicyTriggerHandler class.
    *              After a new VehiclePolicy is created/update we will populate the related Allianz Data Mapping 
    *              records based on the Vehicle Policy values.
    */
    static testMethod void test02_VGA_populateAllianzDataMapping_whenPolicyCreatedOrUpdated_PopulateAllianzaDataMapping() 
    {      
        Asset obj_asset = [SELECT Id, Name, VGA_VIN__c, AccountId FROM Asset LIMIT 1];

        Test.startTest();
        VGA_Vehicle_Policy__c objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Skoda';  
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'DW12';
        objVehicle.VGA_VIN__c            = obj_asset.Id;
        insert objVehicle;    
        
        objVehicle.VGA_Term__c = '12';
        update objVehicle;

        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'ADRD12';
        insert objVehicle;    
        
        objVehicle.VGA_Term__c = '12';
        update objVehicle;

        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'FW24';
        insert objVehicle;    
        
        objVehicle.VGA_Term__c = '12';
        update objVehicle;

        Test.stopTest();
    } 
    
    /**
    * @author Athira
    * @date 09/07/2019
    * @description Method that tests the update of the Allianz Policy record with Action 'D' 
    *              if the Vehicle Policy is deleted.
    */
    static testMethod void test03_VGA_beforeDelete_whenPolicyIsDeleted_UpdateRelatedAllianzDataMapping() 
    {      
         
        Test.startTest();
        
        VGA_Vehicle_Policy__c objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'STDWTY';
        objVehicle.VGA_Component_Code__c = 'ADRD12';
        
        
        objVehicle.VGA_External_Id__c = 'WVWZZZ1KZ5U026277STDWTY1210201521052018';
        insert objVehicle;    
        
        List<VGA_Vehicle_Policy__c> policies = [SELECT Id, VGA_VIN_No__c,VGA_External_Id__c , VGA_Component_Type__c 
                                                FROM VGA_Vehicle_Policy__c 
                                                WHERE VGA_External_Id__c = 'WVWZZZ1KZ5U026277STDWTY1210201521052018' 
                                                OR VGA_External_Id__c = 'WVWZZZ1KZ5U026277ROADSIDE1210201521052018'];
        
        system.assertEquals(1, policies.size(), 'Count incorrect. Roadside policy not created.');
        
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c = true;
        objVehicle.VGA_Component_Type__c = 'Roadside';
        objVehicle.VGA_Component_Code__c = 'FW24';
        insert objVehicle;    
        
        objVehicle.VGA_Term__c = '12';
        update objVehicle;
        
        delete objVehicle;
        Test.stopTest();

        List<VGA_Allianz_Data_Mapping__c >  updatedAllianzList = [SELECT Id,VGA_Transaction_Code__c  
                                                                  FROM VGA_Allianz_Data_Mapping__c 
                                                                  WHERE VGA_Vehicle_Policy_ID__c =: objVehicle.Id ];

        if(updatedAllianzList.size() > 0){ 
            for(VGA_Allianz_Data_Mapping__c obj_allianz : updatedAllianzList ){
                System.assertEquals('D', obj_allianz.VGA_Transaction_Code__c, 'Transaction code has not been updated');
            }
        }
    }    
}