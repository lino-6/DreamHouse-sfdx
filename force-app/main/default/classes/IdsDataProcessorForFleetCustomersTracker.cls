@isTest(seeAllData = false)
public class IdsDataProcessorForFleetCustomersTracker 
{
    public static Account VWobjAccount; 
    public static Account SKObjAccount;
    public static Contact VWContact;
    public static Contact SKContact;
    static testmethod void unittestForException(){
        try{
        VWobjAccount = new Account();
        VWobjAccount.name = 'VWFleet';
        VWobjAccount.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        VWobjAccount.VGA_Fleet_External_ID__c ='Test21';
        //insert  VWobjAccount;
        string strVWFleetAccount = JSON.serialize(VWobjAccount);
        //string strVWFleetContact = JSON.serialize(VWContact);
        IdsDataProcessorForFleetCustomers.processData(strVWFleetAccount,null,null,null);
        }
        catch(Exception e){}
    }
    static testmethod void unittest1()
    {
        loadData();
        
        string strVWFleetAccount = JSON.serialize(VWobjAccount);
        string strSKFleetAccount = JSON.serialize(SKObjAccount);
        string strVWFleetContact = JSON.serialize(VWContact);
        string strSKFleetContact = JSON.serialize(SKContact);
        IdsDataProcessorForFleetCustomers.processData(strVWFleetAccount,strSKFleetAccount,strVWFleetContact,strSKFleetContact);
    }
    static testmethod void unittest2()
    {
        loadData();
        
        string strVWFleetAccount = JSON.serialize(VWobjAccount);
        string strSKFleetAccount = JSON.serialize(SKObjAccount);
        string strVWFleetContact = JSON.serialize(VWContact);
        string strSKFleetContact = JSON.serialize(SKContact);
        IdsDataProcessorForFleetCustomers.processData(null,null,strVWFleetContact,strSKFleetContact);
    }
    public static void loadData()
    {
        VWobjAccount = new Account();
        VWobjAccount.name = 'VWFleet';
        VWobjAccount.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
        VWobjAccount.VGA_Fleet_External_ID__c ='Test21';
        insert  VWobjAccount;
        
        SKObjAccount = new Account();
        SKObjAccount.Name = 'SKFleet';
        SKObjAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Fleet Customer').getRecordTypeId();
        SKObjAccount.VGA_Fleet_External_ID__c = 'T786';
        insert  SKObjAccount;
        
        VWContact = new Contact();
        VWContact.lastname = 'VWFleet';
        VWContact.AccountId = VWobjAccount.Id;
        VWContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Volkswagen Fleet Customer Contact').getRecordTypeId();
        VWContact.VGA_External_Id__c = VWobjAccount.id;
        insert VWContact;
        
        SKContact = new Contact();
        SKContact.lastname = 'SKFleet';
        SKContact.AccountId = SKObjAccount.Id;
        SKContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Skoda Fleet Customer Contact').getRecordTypeId();
        SKContact.VGA_External_Id__c = SKObjAccount.id;
        insert SKContact; 
    }
}