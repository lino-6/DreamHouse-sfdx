global class VGA_CreateCampaignMembers implements Database.Batchable<sObject>
{
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strQuery='select id,VGA_Tracking_ID__c from lead  where VGA_Tracking_ID__c !=null';
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<Lead> leads)
    {
    
        if(leads != null && !leads.isEmpty())
        {
            try{
                set<String> setTrackingId = new set<string>();
                
                for(Lead objLead : leads)
                {
                    if(objLead.VGA_Tracking_ID__c != null)
                    {
                        setTrackingId.add(objLead.VGA_Tracking_ID__c);  
                    }
                }
                
                if(setTrackingId != null && !setTrackingId.isEmpty())
                {
                    list<Campaign> lstCampaigns = [select id,VGA_Tracking_ID__c from Campaign 
                                                   where VGA_Tracking_ID__c in:setTrackingId];
                                                 
                    
                    if(lstCampaigns != null && !lstCampaigns.isEmpty())
                    {
                        map<string,Campaign> mapofCampaign = new map<string,Campaign>();
                        
                        for(Campaign objCampaign : lstCampaigns)
                        {
                            if(!mapofCampaign.containskey(objCampaign.VGA_Tracking_ID__c))
                                mapofCampaign.put(objCampaign.VGA_Tracking_ID__c,objCampaign);
                        }
                        
                        if(mapofCampaign != null && !mapofCampaign.isEmpty())
                        {
                            list<CampaignMember> lstCampaignMember = new list<CampaignMember>(); 
                            
                            for(lead objLead : leads)
                            {
                                if(objLead.VGA_Tracking_ID__c != null && mapofCampaign.containskey(objLead.VGA_Tracking_ID__c) 
                                   && mapofCampaign.get(objLead.VGA_Tracking_ID__c) != null)
                                {
                                    CampaignMember objCampaignMember = new CampaignMember();
                                    objCampaignMember.LeadId  = objLead.id;
                                    objCampaignMember.CampaignId = mapofCampaign.get(objLead.VGA_Tracking_ID__c).id;
                                    
                                    lstCampaignMember.add(objCampaignMember);
                                }
                            }
                            
                            if(lstCampaignMember != null && !lstCampaignMember.isEmpty())
                            {
                                insert lstCampaignMember;
                                
                                system.debug('Campaigns are' +lstCampaignMember);
                            }
                        }
                    }
                }
            }//end try
            catch(exception e)
            {
                system.debug('@@@'+e.getMessage());
            }
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
        
    }
}