@isTest
private class VGA_PostCodeMappingTriggerHandlerTracker
{
    @testsetup
    static void buildPMAdetails()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'VGA_Post_Code_Mapping__c';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;

        Account objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '1234';
        update objAccount;

        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.Name = '123 Dealer Account';
        objAccount.VGA_Dealer_Code__c = '123';
        update objAccount;

        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        insert objPostCode;

        VGA_Post_Code_Mapping__c objPostCode2 = VGA_CommonTracker.createPostCodemapping(null,'1234',null);
        insert objPostCode2;

        VGA_Post_Code_Mapping__c objPostCode3 = VGA_CommonTracker.createPostCodemapping(null,null,'1234');
        insert objPostCode3;
    }

    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to populate the VWC dealer lookup fields.
    *              When the VWC dealer codes are updated then we will need to update 
    *              the dealer lookups fields based on dealer code and postcode.
     */
    static testMethod void test01_UpdateDealerVWCField_WhenVWCFieldsAreUpdated_VWCDealerwillBePopulated()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_VWC_Dealer_Code__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_5th__c ='1234';
        
        Test.startTest();
        insert objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_VWC_Dealer__c, VGA_VWC_Dealer_2nd__c,
                              VGA_VWC_Dealer_3rd__c, VGA_VWC_Dealer_4th__c,
                              VGA_VWC_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWC_Dealer__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWC_Dealer_2nd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWC_Dealer_3rd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWC_Dealer_4th__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWC_Dealer_5th__c);
    }

    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to update the VWC dealer lookup fields to null.
    *              When the VWC dealer codes are updated to null then we will need to update 
    *              the dealer lookups fields so they are null too.
     */
    static testMethod void test02_UpdateDealerVWCField_WhenVWCFieldsAreUpdatedToNull_VWCDealerwillBeNull()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_VWC_Dealer_Code__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_5th__c ='1234';
        insert objPostCode;

        Test.startTest();
        objPostCode.VGA_VWC_Dealer_Code__c     = null;
        objPostCode.VGA_VWC_Dealer_Code_2nd__c = null;
        objPostCode.VGA_VWC_Dealer_Code_3rd__c = null;
        objPostCode.VGA_VWC_Dealer_Code_4th__c = null;
        objPostCode.VGA_VWC_Dealer_Code_5th__c = null;
        update objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_VWC_Dealer__c, VGA_VWC_Dealer_2nd__c,
                              VGA_VWC_Dealer_3rd__c, VGA_VWC_Dealer_4th__c,
                              VGA_VWC_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(null , objPostCode.VGA_VWC_Dealer__c);
        System.AssertEquals(null , objPostCode.VGA_VWC_Dealer_2nd__c);
        System.AssertEquals(null , objPostCode.VGA_VWC_Dealer_3rd__c);
        System.AssertEquals(null , objPostCode.VGA_VWC_Dealer_4th__c);
        System.AssertEquals(null , objPostCode.VGA_VWC_Dealer_5th__c);
    }

    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to populate the SKP dealer lookup fields.
    *              When the SKP dealer codes are updated then we will need to update 
    *              the dealer lookups fields based on dealer code and postcode.
     */
    static testMethod void test03_UpdateDealerSKPField_WhenSKPFieldsAreUpdated_SKPDealerwillBePopulated()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_SKP_Dealer_Code__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_5th__c ='1234';
        
        Test.startTest();
        insert objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_SKP_Dealer__c, VGA_SKP_Dealer_2nd__c,
                              VGA_SKP_Dealer_3rd__c, VGA_SKP_Dealer_4th__c,
                              VGA_SKP_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_SKP_Dealer__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_SKP_Dealer_2nd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_SKP_Dealer_3rd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_SKP_Dealer_4th__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_SKP_Dealer_5th__c);
    }

    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to update the SKP dealer lookup fields to null.
    *              When the SKP dealer codes are updated to null then we will need to update 
    *              the dealer lookups fields so they are null too.
     */
    static testMethod void test04_UpdateDealerSKPField_WhenSKPFieldsAreUpdatedToNull_SKPDealerwillBeNull()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_SKP_Dealer_Code__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_5th__c ='1234';
        insert objPostCode;

        Test.startTest();
        objPostCode.VGA_SKP_Dealer_Code__c     = null;
        objPostCode.VGA_SKP_Dealer_Code_2nd__c = null;
        objPostCode.VGA_SKP_Dealer_Code_3rd__c = null;
        objPostCode.VGA_SKP_Dealer_Code_4th__c = null;
        objPostCode.VGA_SKP_Dealer_Code_5th__c = null;
        update objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_SKP_Dealer__c, VGA_SKP_Dealer_2nd__c,
                              VGA_SKP_Dealer_3rd__c, VGA_SKP_Dealer_4th__c,
                              VGA_SKP_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(null , objPostCode.VGA_SKP_Dealer__c);
        System.AssertEquals(null , objPostCode.VGA_SKP_Dealer_2nd__c);
        System.AssertEquals(null , objPostCode.VGA_SKP_Dealer_3rd__c);
        System.AssertEquals(null , objPostCode.VGA_SKP_Dealer_4th__c);
        System.AssertEquals(null , objPostCode.VGA_SKP_Dealer_5th__c);
    }

        /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to populate the VWP dealer lookup fields.
    *              When the VWP dealer codes are updated then we will need to update 
    *              the dealer lookups fields based on dealer code and postcode.
     */
    static testMethod void test05_UpdateDealerVWPField_WhenVWPFieldsAreUpdated_VWPDealerwillBePopulated()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_VWP_Dealer_Code__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_5th__c ='1234';
        
        Test.startTest();
        insert objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_VWP_Dealer__c, VGA_VWP_Dealer_2nd__c,
                              VGA_VWP_Dealer_3rd__c, VGA_VWP_Dealer_4th__c,
                              VGA_VWP_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWP_Dealer__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWP_Dealer_2nd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWP_Dealer_3rd__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWP_Dealer_4th__c);
        System.AssertEquals(assignedDealer.Id , objPostCode.VGA_VWP_Dealer_5th__c);
    }

        /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to update the VWP dealer lookup fields to null.
    *              When the VWP dealer codes are updated to null then we will need to update 
    *              the dealer lookups fields so they are null too.
     */
    static testMethod void test06_UpdateDealerVWPField_WhenVWPFieldsAreUpdatedToNull_VWPDealerwillBeNull()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_VWP_Dealer_Code__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_5th__c ='1234';
        insert objPostCode;

        Test.startTest();
        objPostCode.VGA_VWP_Dealer_Code__c     = null;
        objPostCode.VGA_VWP_Dealer_Code_2nd__c = null;
        objPostCode.VGA_VWP_Dealer_Code_3rd__c = null;
        objPostCode.VGA_VWP_Dealer_Code_4th__c = null;
        objPostCode.VGA_VWP_Dealer_Code_5th__c = null;
        update objPostCode;
        Test.stopTest();

        Account assignedDealer = [SELECT Id FROM Account where VGA_Dealer_Code__c = '1234'];
        objPostCode = [SELECT Id, VGA_VWP_Dealer__c, VGA_VWP_Dealer_2nd__c,
                              VGA_VWP_Dealer_3rd__c, VGA_VWP_Dealer_4th__c,
                              VGA_VWP_Dealer_5th__c
                       FROM VGA_Post_Code_Mapping__c 
                       WHERE Id =: objPostCode.Id];
        
        System.AssertEquals(null , objPostCode.VGA_VWP_Dealer__c);
        System.AssertEquals(null , objPostCode.VGA_VWP_Dealer_2nd__c);
        System.AssertEquals(null , objPostCode.VGA_VWP_Dealer_3rd__c);
        System.AssertEquals(null , objPostCode.VGA_VWP_Dealer_4th__c);
        System.AssertEquals(null , objPostCode.VGA_VWP_Dealer_5th__c);
    }

    /**
    * @author Ganesh M
    * @date 21/11/2018
    * @description Method that tests the logic to update the dealer fields
    *              When the dealer code doesn't exist on the system
    *              We will be showing an error letting us know that the dealer code
    *              for a particular post code doesn't exist.
    */
    static testMethod void test07_UpdateDealerFields_WhenDealerCodeDoesntExist_WeWillBeShowingAnError()
    {
    
        VGA_Post_Code_Mapping__c objPostCode = VGA_CommonTracker.createPostCodemapping('123',null,null);
        objPostCode.VGA_VWC_Dealer_Code__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWC_Dealer_Code_5th__c ='1234';

        objPostCode.VGA_VWP_Dealer_Code__c     ='1234';
        objPostCode.VGA_VWP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_VWP_Dealer_Code_5th__c ='1234';

        objPostCode.VGA_SKP_Dealer_Code__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_2nd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_3rd__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_4th__c ='1234';
        objPostCode.VGA_SKP_Dealer_Code_5th__c ='1234';
        insert objPostCode;

        Test.startTest();
        objPostCode.VGA_VWC_Dealer_Code__c     = '555';
        objPostCode.VGA_VWC_Dealer_Code_2nd__c = '555';
        objPostCode.VGA_VWC_Dealer_Code_3rd__c = '555';
        objPostCode.VGA_VWC_Dealer_Code_4th__c = '555';
        objPostCode.VGA_VWC_Dealer_Code_5th__c = '555';

        objPostCode.VGA_VWP_Dealer_Code__c     = '555';
        objPostCode.VGA_VWP_Dealer_Code_2nd__c = '555';
        objPostCode.VGA_VWP_Dealer_Code_3rd__c = '555';
        objPostCode.VGA_VWP_Dealer_Code_4th__c = '555';
        objPostCode.VGA_VWP_Dealer_Code_5th__c = '555';

        objPostCode.VGA_SKP_Dealer_Code__c     = '555';
        objPostCode.VGA_SKP_Dealer_Code_2nd__c = '555';
        objPostCode.VGA_SKP_Dealer_Code_3rd__c = '555';
        objPostCode.VGA_SKP_Dealer_Code_4th__c = '555';
        objPostCode.VGA_SKP_Dealer_Code_5th__c = '555';
        
        try {
            update objPostCode;
            System.assert(false, 'Exception expected');
        } catch (DMLException e) {
            System.assert(e.getMessage().contains('No Dealer Found For Dealer Code 555'), 'message=' + e.getMessage());
        }
        
        Test.stopTest();
    }
}