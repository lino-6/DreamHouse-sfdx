public with sharing class VGA_Service_Campaign_NotificationHandler
{
    public void runTrigger()
    {
    if(Trigger.isafter && Trigger.isInsert)
        {
            onafterInsert((List<VGA_Service_Campaign_Notification__c>) trigger.new);     
        }
        if (Trigger.isBefore && Trigger.isUpdate)
        {
          //  onBeforeUpdate((List<VGA_Service_Campaign_Notification__c>) trigger.new, (Map<Id, VGA_Service_Campaign_Notification__c>) trigger.OldMap);    
        }
        }
    public void onafterInsert(List<VGA_Service_Campaign_Notification__c> lstTriggerNew)
    {
       updateServiceNotificationDate(lstTriggerNew,null);
        
    }
   
    /**
    * @author Ganesh M
    * @date 31/10/2018
    * @description Method that tests updated Driver,Passenger RecallInitiated details for  Takata recalls.
    * @description Method that tests When a Service Campaign Notification is created for a Service Campaign the Status of the Service Campaign should automatically be set to 'Initiated'.
    * Assembla Ticket #515,#509
    * Added Line No 70
    */
  private static void updateServiceNotificationDate(list<VGA_Service_Campaign_Notification__c> lsttriggernew,map<Id,VGA_Service_Campaign_Notification__c> triggeroldmap)
    {  
    
         set<id> campaignid = new set<id>();
         list<VGA_Service_Campaigns__c> UpdateSCList = new list<VGA_Service_Campaigns__c>();
         Id SC_RTId = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId(); 

        
                for(VGA_Service_Campaign_Notification__c objB: lsttriggernew)
                 {
                            campaignid.add(objB.VGA_SC_ID__c);

                     }

        
        list<VGA_Service_Campaign_Notification__c> objSCN = [select id,name,VGA_SC_ID__c,CreatedDate from VGA_Service_Campaign_Notification__c 
                                                                    where VGA_SC_ID__c in: campaignid order by CreatedDate limit 1];

        List<VGA_Service_Campaigns__c> objSCList = [select id,RecordTypeId,driver_airbag_Recall_Scheduled_Commencem__c,driver_airbag_inflator_affected__c,passenger_airbag_inflator_affected__c,VGA_SC_Status__c,
                                                               passenger_airbag_replaced__c,passenger_airbag_recall_initiated__c
                                                          from VGA_Service_Campaigns__c where id in:campaignid];

           for (VGA_Service_Campaigns__c objSC: objSCList)
           {
           
                    Datetime  dt =objSCN[0].createdDate;
                    Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
               if(objSC.RecordTypeId == SC_RTId)
               {     
                  if(objSC.driver_airbag_inflator_affected__c == 'Y')
                  {  
                    objSC.Driver_Airbag_Recall_Initiated__c = myDate;
                    }
                   if(objSC.passenger_airbag_inflator_affected__c == 'Y')
                   {
                    objSC.passenger_airbag_recall_initiated__c = myDate;
                    
                    }
                     
                  } 
               if(objSC.VGA_SC_Status__c == 'Scheduled')
                  {  
                  objSC.VGA_SC_Status__c = 'Initiated';
                  }  
                 // Prec.VGA_SC_Dealer_Code__c = objrecschild[0].name;
                  UpdateSCList.add(objSC);
           }
     Update UpdateSCList;

   }


    
      
     
}