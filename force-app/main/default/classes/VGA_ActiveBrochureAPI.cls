@RestResource(urlMapping='/getActiveBrochures/*')
global class VGA_ActiveBrochureAPI{
    
    /**
* @author Lino Diaz Alonso
* @date 10/12/2018
* @description REST API to get active brochure list from Salesforce.
*              We are using a class called VGA_ActiveBrochureJSON to generate
*              the JSON with the data in the right format.
*/
    @HttpGet
    global static void getActiveBrochures() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        //Get brand name parameter
        String brandName = req.params.get('brand');
        //Get brand TrackingCode parameter
        String trackingcode = req.params.get('tracking_code');
        
        
        Map<Id, VGA_Active_Brochure__c> activeBrochuresPerIdMap = getActiveBrochuresPerIdMap(brandName,trackingcode);        
        Map<Id, List<VGA_Model_Attribute__c>> modelAttributesPerActiveBrochureMap = getModelAttributesPerActiveBrochureMap(brandName,trackingcode); 
        Map<Id, List<VGA_Excites_Options__c>> modelExcitesPerActiveBrochureMap = getModelExcitesPerActiveBrochureMap(brandName,trackingcode); 
        
        system.debug('####'+activeBrochuresPerIdMap.size());
        List<VGA_ActiveBrochureJSON> activeBrochureJSONList = new List<VGA_ActiveBrochureJSON>();
        
        for(Id activeBrochureId : activeBrochuresPerIdMap.KeySet()){
            
            VGA_ActiveBrochureJSON activeBrochureJSON = new VGA_ActiveBrochureJSON(activeBrochuresPerIdMap.get(activeBrochureId), modelAttributesPerActiveBrochureMap.containsKey(activeBrochureId)?modelAttributesPerActiveBrochureMap.get(activeBrochureId):new List<VGA_Model_Attribute__c>(), modelExcitesPerActiveBrochureMap.containsKey(activeBrochureId)?modelExcitesPerActiveBrochureMap.get(activeBrochureId): new List<VGA_Excites_Options__c>());
            activeBrochureJSONList.add(activeBrochureJSON);
            
        }
        
        String gCalJSONAgent = VGA_ActiveBrochureJSON.serializeSorted(activeBrochureJSONList);
        
        //res.responseBody = Blob.valueOf(gCalJSONAgent.replace('"default_x":', '"default":'));
        res.responseBody = Blob.valueOf(gCalJSONAgent.replace('"default_x":', '"default":').replace('null', '""'));
        //System.debug(gCalJSONAgent.replace('"default_x":', '"default":').replace('null', '""'));
    }// method
    
    /**
* @author Ganesh M
* @date 20/02/2019
* @description This Method is used to retrieve the list of model Excites Options group by
*              its related active brochure Id
* @param brandName Name of the brand that we will use to filter the model attributes
* @return Map of model Excites group by its related Active Brochure Id.
*/
    private static Map<Id, List<VGA_Excites_Options__c>> getModelExcitesPerActiveBrochureMap(String brandName,string trackingcode){
        List<campaign> campaignList = new List<campaign>();
        list<VGA_Excites_Options__c> results = new list<VGA_Excites_Options__c>();
        campaignList = [SELECT ID FROM Campaign where VGA_Tracking_ID__c =: trackingcode];
        
        String querystring = 'SELECT Id, Name,VGA_Active_Brochure__c ';
        querystring += ' FROM VGA_Excites_Options__c  WHERE VGA_Active_Brochure__r.VGA_Brand__c =: brandName' ;                                        
        
        if(trackingcode != null && campaignList != null)
        {
            if(campaignList.size() > 0){
                querystring += ' AND VGA_Active_Brochure__r.VGA_campaign__r.VGA_Tracking_ID__c = \'' + trackingcode + '\'';
            }
        }  
        system.debug(querystring);
        results = Database.query(querystring);
        Map<Id, List<VGA_Excites_Options__c>> ModelExcitesPerActiveBrochureCampaignMap = new Map<Id, List<VGA_Excites_Options__c>>();
        
        for(VGA_Excites_Options__c modelExcite : results)
        {
            
            if(ModelExcitesPerActiveBrochureCampaignMap.containsKey(modelExcite.VGA_Active_Brochure__c))
            {
                ModelExcitesPerActiveBrochureCampaignMap.get(modelExcite.VGA_Active_Brochure__c).add(modelExcite);
            }
            else{
                List<VGA_Excites_Options__c> tempList1 = new List<VGA_Excites_Options__c>();
                tempList1.add(modelExcite);
                ModelExcitesPerActiveBrochureCampaignMap.put(modelExcite.VGA_Active_Brochure__c, tempList1);
            }
        }
        
        system.debug('@@@@'+ModelExcitesPerActiveBrochureCampaignMap);
        return ModelExcitesPerActiveBrochureCampaignMap;
        
    }   
    
    
    /**
* @author Lino Diaz Alonso
* @date 10/12/2018
* @description This Method is used to retrieve the list of model attributes group by
*              its related active brochure Id
* @param brandName Name of the brand that we will use to filter the model attributes
* @return Map of model attributes group by its related Active Brochure Id.
*@Date 28/01/2019 Added  new field in Select Query VGA_ModelCar_Configurator_URL__c #856 
* @date 05/02/2019 Added trackingcode we will use to filter the Model Attributes with Campaign Tracking code #923
*/
    private static Map<Id, List<VGA_Model_Attribute__c>> getModelAttributesPerActiveBrochureMap(String brandName,string trackingcode){
        List<campaign> campaignList = new List<campaign>();
        list<VGA_Model_Attribute__c> results = new list<VGA_Model_Attribute__c>();
        campaignList = [SELECT ID FROM Campaign where VGA_Tracking_ID__c =: trackingcode];
        
        String querystring = 'SELECT Id, Name, VGA_Active_Brochure__c, VGA_Model_image_URL__c,VGA_Preference__c, VGA_Is_Default_Preference__c,VGA_Sort_Order__c, ';
        querystring += ' VGA_Version_Sales_Group_Id__c,VGA_Trim_Name__c,VGA_Car_Configurator_Url__c,VGA_Carline_Id__c, VGA_Carline_Name__c,VGA_Version_Sales_Group_Name__c, VGA_Trim_Id__c,VGA_Active__c ';
        querystring += ' FROM VGA_Model_Attribute__c  WHERE VGA_Active_Brochure__r.VGA_Brand__c =: brandName and VGA_Active__c=True' ;                                        
        
        if(trackingcode != null && campaignList != null)
        {
            if(campaignList.size() > 0){
                querystring += ' AND VGA_Active_Brochure__r.VGA_campaign__r.VGA_Tracking_ID__c = \'' + trackingcode + '\'';
            }
        }  
        
        results = Database.query(querystring);
        Map<Id, List<VGA_Model_Attribute__c>> ModelAttributesPerActiveBrochureCampaignMap = new Map<Id, List<VGA_Model_Attribute__c>>();
        
        for(VGA_Model_Attribute__c modelAttribute : results)
        {
            
            if(ModelAttributesPerActiveBrochureCampaignMap.containsKey(modelAttribute.VGA_Active_Brochure__c))
            {
                ModelAttributesPerActiveBrochureCampaignMap.get(modelAttribute.VGA_Active_Brochure__c).add(modelAttribute);
            }
            else{
                List<VGA_Model_Attribute__c> tempList1 = new List<VGA_Model_Attribute__c>();
                tempList1.add(modelAttribute);
                ModelAttributesPerActiveBrochureCampaignMap.put(modelAttribute.VGA_Active_Brochure__c, tempList1);
            }
        }
        
        
        return ModelAttributesPerActiveBrochureCampaignMap;
    }   
    
    
    /**
* @author Lino Diaz Alonso
* @date 10/12/2018
* @description This Method is used to retrieve the list of active brouchers group by
*              its Id
* @param brandName Name of the brand that we will use to filter the active brochures
* @return Map of active brochures group by its Id.
*@Date 28/01/2019 Added  new field in Select Query VGA_ModelCar_Configurator_URL__c #856 
* @date 05/02/2019 Added trackingcode we will use to filter the Acivebrochure with Campaign Tracking code #923
*/
    
    private static Map<Id, VGA_Active_Brochure__c> getActiveBrochuresPerIdMap(String brandName, string trackingcode)
    {
        List<campaign> campaignList = new List<campaign>();
        list<VGA_Active_Brochure__c> Brochureresults = new list<VGA_Active_Brochure__c>();
        Map<Id, VGA_Active_Brochure__c> activeBrochuresPerIdMap = new Map<Id, VGA_Active_Brochure__c>();
        campaignList = [SELECT ID FROM Campaign where VGA_Tracking_ID__c =: trackingcode];
        string querystring;
        querystring = 'SELECT Id, Name, VGA_Brand__c, VGA_Brochure_Code_Formula__c,VGA_Brochure_File_URL__c, VGA_Brochure_Label__c,VGA_Image_URL__c, VGA_Sub_Brand__c, VGA_Test_Drive_Active__c,VGA_Brochure_Request_Active__c,VGA_Dealer_Callback_Active__c, VGA_ModelCar_Configurator_URL__c,VGA_Brochure_Banner_Image_URL__c,';
        querystring += ' VGA_Thumbnail_Url__c,VGA_Sort_Order__c FROM VGA_Active_Brochure__c';
        querystring +=' where VGA_Brand__c =: brandName' ;                                       
        if(trackingcode != null && campaignList !=null)
        {
            if(campaignList.size() > 0){
                queryString += ' AND VGA_campaign__r.VGA_Tracking_ID__c = \'' + trackingcode + '\'';
            }    
        }  
        querystring +=' and VGA_Sort_Order__c!= null  ORDER BY VGA_Sort_Order__c ASC';
        system.debug(querystring);
        Brochureresults = Database.query(querystring); 
        for(VGA_Active_Brochure__c brochure :Brochureresults) 
        {
            activeBrochuresPerIdMap.put(brochure.id,brochure);
        }
        
        return activeBrochuresPerIdMap;
    }
    
}