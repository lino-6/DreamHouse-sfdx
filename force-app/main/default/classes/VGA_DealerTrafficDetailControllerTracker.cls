//Tracker class for VGA_DealerTrafficDetailController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for VGA_DealerTrafficDetailController on                 8-Nov-2017      90%
//===============================================================================================
@isTest(seeAllData = false)      
public class VGA_DealerTrafficDetailControllerTracker  
{
    public static Account objAccount;
    public static Contact objContact;
    public static user objUser;
    public static VGA_Dealer_Traffic_Header__c objDealerTrafficHeader;
    public static VGA_Dealer_Traffic_Detail__c objDealerTrafficDetails;
    public static VGA_Active_Brochure__c objActiveBrochure;
    public static Product2 objProduct;
    public static date startdate;
    public static date Enddate;
    Public static VGA_Public_Holiday__c publicholiday;
    public static testmethod void unittest1()
    {
        LoadData();   
        objDealerTrafficHeader = VGA_CommonTracker.createDealerTrafficHeader(objAccount.id);
        objDealerTrafficHeader.VGA_Start_Date__c = VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        objDealerTrafficHeader.VGA_End_Date__c = objDealerTrafficHeader.VGA_Start_Date__c.adddays(6);
        objDealerTrafficHeader.VGA_Sub_Brand__c =   'Passenger Vehicles (PV)';
        objDealerTrafficHeader.VGA_Brand__c = 'Volkswagen';
        update objDealerTrafficHeader;  
        
        objActiveBrochure = VGA_CommonTracker.createActiveBrochure();
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        insert objActiveBrochure; 
        objDealerTrafficDetails = VGA_CommonTracker.createDealerTrafficDetail(objDealerTrafficHeader.id);
        objDealerTrafficDetails.VGA_Active_Brochure__c = objActiveBrochure.id;
        insert objDealerTrafficDetails;
        
        publicholiday=VGA_CommonTracker.createPublicHoliday(date.today(),'test holiday','Single','All');
        insert publicholiday;
         
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger');
            list<VGA_WrapperofProduct> lstgetproductDetails2 = VGA_DealerTrafficDetailController.getproductDetails('Commercial');
        }
    }
    public static testmethod void unittest2()
    {
        LoadData();
        objActiveBrochure                  = VGA_CommonTracker.createActiveBrochure();
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c     = 'Volkswagen';
        objActiveBrochure.VGA_Dealer_Traffic_Active__c = true;
        insert objActiveBrochure;
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger');
            list<VGA_WrapperofProduct> lstgetproductDetails2 = VGA_DealerTrafficDetailController.getproductDetails('Commercial');  
        }
    }
    public static testmethod void unittest3()
    {
        LoadData();
        objContact.VGA_Brand__c = 'Skoda';
        objContact.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        update objContact;
        
        objDealerTrafficHeader = VGA_CommonTracker.createDealerTrafficHeader(objAccount.id);
        objDealerTrafficHeader.VGA_Start_Date__c = VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        objDealerTrafficHeader.VGA_End_Date__c = objDealerTrafficHeader.VGA_Start_Date__c.adddays(6);
        objDealerTrafficHeader.VGA_Sub_Brand__c =   'Passenger Vehicles (PV)';
        objDealerTrafficHeader.VGA_Brand__c = 'Volkswagen';
        update objDealerTrafficHeader;
        system.debug('@@@objDealerTrafficHeader'+objDealerTrafficHeader.VGA_Account__c);
        
        objActiveBrochure = VGA_CommonTracker.createActiveBrochure();
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        objActiveBrochure.VGA_Dealer_Traffic_Active__c = true;
        insert objActiveBrochure;
        
        objProduct = new product2();
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.Name = 'test';
        objProduct.VGA_Dealer_Traffic_Active__c=true;
        insert objProduct;
        
        objDealerTrafficDetails = VGA_CommonTracker.createDealerTrafficDetail(objDealerTrafficHeader.id);
        objDealerTrafficDetails.VGA_Active_Brochure__c = objActiveBrochure.id;
        objDealerTrafficDetails.VGA_Product__c = objProduct.id;
        insert objDealerTrafficDetails; 
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger'); 
            list<VGA_WrapperofProduct> lstgetproductDetails2 = VGA_DealerTrafficDetailController.getproductDetails('Commercial');
        }
    }
    public static testmethod void unittest4()
    {
        LoadData();
        objContact.VGA_Brand__c = 'Skoda';
        objContact.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        update objContact;
        objProduct = new product2();
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.Name = 'test';
        objProduct.VGA_Dealer_Traffic_Active__c=true;
        objProduct.VGA_Brand__c = 'skoda';
        insert objProduct;
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger'); 
        }
    }
    public static testmethod void unittest1_1()
    {
        LoadData(); 
        objDealerTrafficHeader = VGA_CommonTracker.createDealerTrafficHeader(objAccount.id);
        objDealerTrafficHeader.VGA_Start_Date__c = VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        objDealerTrafficHeader.VGA_End_Date__c = objDealerTrafficHeader.VGA_Start_Date__c.adddays(6);
        objDealerTrafficHeader.VGA_Sub_Brand__c =   'Passenger Vehicles (PV)';
        objDealerTrafficHeader.VGA_Brand__c = 'Volkswagen';
        update objDealerTrafficHeader;
        system.debug('@@@objDealerTrafficHeader'+objDealerTrafficHeader.VGA_Account__c); 
        objActiveBrochure = VGA_CommonTracker.createActiveBrochure();
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        insert objActiveBrochure;
        objProduct = new product2();
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.Name = 'test';
        //objProduct.recordtypeID = VGA_Common.getproductRecordTypeId('Volkswagen');
        objProduct.VGA_Brand__c = 'skoda';
        objProduct.VGA_Dealer_Traffic_Active__c=true;
        insert objProduct;
        objDealerTrafficDetails = VGA_CommonTracker.createDealerTrafficDetail(objDealerTrafficHeader.id);
        objDealerTrafficDetails.VGA_Active_Brochure__c = objActiveBrochure.id;
        insert objDealerTrafficDetails;
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser(); 
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger'); 
          //  VGA_Dealer_Traffic_Detail__c objDealertrafficDetail = VGA_DealerTrafficDetailController.createDealerTrafficHeader(20.0, 210.12, 123.213, 3412.0, objDealerTrafficHeader.id ,objProduct.id, objDealerTrafficDetails.id, 'Volkswagen');
        }
    }
    public static testmethod void unittest1_2()
    {
        LoadData(); 
        objDealerTrafficHeader = VGA_CommonTracker.createDealerTrafficHeader(objAccount.id);
        objDealerTrafficHeader.VGA_Start_Date__c = VGA_Common.getWeekStartDate(startdate.addDays(-2)).adddays(1);
        objDealerTrafficHeader.VGA_End_Date__c = objDealerTrafficHeader.VGA_Start_Date__c.adddays(6);
        objDealerTrafficHeader.VGA_Sub_Brand__c =   'Passenger Vehicles (PV)';
        objDealerTrafficHeader.VGA_Brand__c = 'Volkswagen';
        update objDealerTrafficHeader;
        system.debug('@@@objDealerTrafficHeader'+objDealerTrafficHeader.VGA_Account__c); 
        objActiveBrochure = VGA_CommonTracker.createActiveBrochure();
        objActiveBrochure.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objActiveBrochure.VGA_Brand__c = 'Volkswagen';
        insert objActiveBrochure;
        objProduct = new product2();
        objProduct.Family = 'Passenger Vehicles (PV)';
        objProduct.Name = 'test';
        objProduct.VGA_Dealer_Traffic_Active__c=true;
        //objProduct.recordtypeID = VGA_Common.getproductRecordTypeId('Volkswagen');
        objProduct.VGA_Brand__c = 'skoda';
        insert objProduct; 
        objDealerTrafficDetails = VGA_CommonTracker.createDealerTrafficDetail(objDealerTrafficHeader.id);
        objDealerTrafficDetails.VGA_Active_Brochure__c = objActiveBrochure.id;
        insert objDealerTrafficDetails;    
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger');    
         //   VGA_Dealer_Traffic_Detail__c objDealertrafficDetail = VGA_DealerTrafficDetailController.createDealerTrafficHeader(20.0, 210.12, 123.213, 3412.0, objDealerTrafficHeader.id ,objProduct.id, objDealerTrafficDetails.id, 'Skoda'); 
            VGA_WrapperofProduct OBJ =  VGA_DealerTrafficDetailController.getTotalValue('Passenger');
        }
    }
    public static testmethod void unittest1_3()
    {
        LoadData();
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            VGA_WrapperofProduct OBJ =  VGA_DealerTrafficDetailController.getTotalValue('Passenger');
            string str1 = VGA_DealerTrafficDetailController.getendDate();
            string str2 = VGA_DealerTrafficDetailController.getendDatePlusone();  
        }
    }
    public static testmethod void unittest1_4()
    {
        LoadData();
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('Passenger');
            string str1 = VGA_DealerTrafficDetailController.saveDealerDetails('[{"productName":"test"}]','Passenger');
        }
    }
    public static testmethod void unittest1_5()
    {
        LoadData();
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            list<VGA_WrapperofProduct> lstgetproductDetails = VGA_DealerTrafficDetailController.getproductDetails('commercial');
            string str1 = VGA_DealerTrafficDetailController.saveDealerDetails('[{"productName":"test"}]','commercial');
        }
    }
    
    public static testmethod void unittest1_6()
    {
        LoadData();
        system.runAs(objUser)
        {
            user objUser = VGA_DealerTrafficDetailController.getlogginguser();
            Date dt = VGA_DealerTrafficDetailController.CalculateNextworkingdate(System.today());
        }        
    }
    
    public static void LoadData()
    {
        objAccount = VGA_CommonTracker.createDealerAccount();
        //insert objAccount;
        objContact = VGA_CommonTracker.createDealercontact(objAccount.id);
        //insert objContact;
        objUser = VGA_CommonTracker.CreateUser(objContact.id);  
        //insert objUser;
        startdate =VGA_Common.getWeekStartDate(system.today()).adddays(1);
       Enddate   =startdate.adddays(6);
    }
}