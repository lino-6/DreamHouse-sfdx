public class VGA_ServiceIntervalTriggerHandler {
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description The method cheks the service order,if the service order is repeating or not.it throws an error if repeating.
    * @param newList VGA_Service_Interval__c
    *  @param newMap VGA_Service_Interval__c
    */ 
    //Changed api name of fields  on 29-06-2019
    public static  void ServiceOrderCheck(List<VGA_Service_Interval__c> newList,Map<Id,VGA_Service_Interval__c> newMap){
        try{
            Set<String> CodeSet=new Set<String>();
            Set<String> ModelSet=new Set<String>();
            Map<String,List<VGA_Service_Interval__c>> CodeModel_NEW_ServiceIntervalMap=new Map<String,List<VGA_Service_Interval__c>>();
            Map<String,List<VGA_Service_Interval__c>> CodeModel_AVAILABLE_ServiceIntervalMap=new Map<String,List<VGA_Service_Interval__c>>();
            for(VGA_Service_Interval__c temp:newMap.values()){
                if((temp.VGA_Component_Code__c!=null || temp.VGA_Component_Code__c.trim()!='') && (temp.VGA_KMS__c!=null) && (temp.VGA_Months__c!=null) && (temp.VGA_Product2__c!=null) && (temp.VGA_Service_Order__c!=null)){
                    CodeSet.add(temp.VGA_Component_Code__c);
                    ModelSet.add(temp.VGA_Product2__c);
                    if(!CodeModel_NEW_ServiceIntervalMap.containsKey(temp.VGA_Component_Code__c+'_'+temp.VGA_Product2__c))
                        CodeModel_NEW_ServiceIntervalMap.put(temp.VGA_Component_Code__c+'_'+temp.VGA_Product2__c,new List<VGA_Service_Interval__c>());
                    CodeModel_NEW_ServiceIntervalMap.get(temp.VGA_Component_Code__c+'_'+temp.VGA_Product2__c).add(temp);   
                }else{
                    temp.addError('Please Fill the Data');
                }
            }
            if(CodeModel_NEW_ServiceIntervalMap.size()>0){
                for(String temp:CodeModel_NEW_ServiceIntervalMap.keySet()){
                    if(checkNewInfo(CodeModel_NEW_ServiceIntervalMap.get(temp))){
                        newList[0].addError('Duplicate Service Order cannot be Accepted.');
                    }
                }
            }

            if(CodeSet.size()>0 && ModelSet.size()>0){
                CodeModel_AVAILABLE_ServiceIntervalMap=collectIntervals(CodeSet,ModelSet,newMap.keyset()); 
                if(CodeModel_AVAILABLE_ServiceIntervalMap.size()>0){
                    for(String temp:CodeModel_NEW_ServiceIntervalMap.keySet()){
                        List<VGA_Service_Interval__c> ServiceIntervalList=new List<VGA_Service_Interval__c>();
                        ServiceIntervalList.addAll(CodeModel_NEW_ServiceIntervalMap.get(temp));
                        if(CodeModel_AVAILABLE_ServiceIntervalMap.containsKey(temp))
                            ServiceIntervalList.addAll(CodeModel_AVAILABLE_ServiceIntervalMap.get(temp));
                        if(checkNewInfo(ServiceIntervalList)){
                            newList[0].addError('Duplicate Service Order cannot be Accepted.');
                        }
                    }
                }
            }
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
     /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description It collects the interval corresponding to a model,Component code.
    * @param newList VGA_Service_Interval__c
    *  @param newMap VGA_Service_Interval__c
    */
    private static  Map<String,List<VGA_Service_Interval__c>> collectIntervals(Set<String> ServiceCodeSet,Set<String> ModelIdSet,Set<Id> IdSet){
        try{
            Map<String,List<VGA_Service_Interval__c>> temp=new  Map<String,List<VGA_Service_Interval__c>>();
            Map<Id,VGA_Service_Interval__c> SFDATA=new Map<Id,VGA_Service_Interval__c>([select Id,VGA_Component_Code__c,VGA_KMS__c,VGA_Months__c,VGA_Product2__c,VGA_Service_Order__c  from VGA_Service_Interval__c where VGA_Component_Code__c in:ServiceCodeSet AND VGA_Product2__c in:ModelIdSet and Id not in:IdSet]);
            for(VGA_Service_Interval__c tmp:SFDATA.values()){
                 if(!temp.containsKey(tmp.VGA_Component_Code__c+'_'+tmp.VGA_Product2__c))
                    temp.put(tmp.VGA_Component_Code__c+'_'+tmp.VGA_Product2__c,new List<VGA_Service_Interval__c>());
                temp.get(tmp.VGA_Component_Code__c+'_'+tmp.VGA_Product2__c).add(tmp);
            }
            return temp;
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
        return  new  Map<String,List<VGA_Service_Interval__c>>();
    }
      /**
    * @author Mohammed Ishaque Shaikh
    * @date 21/05/2019
    * @description Checks for VGA_Service_Order__c,if it is not repeating.
    * @param newList VGA_Service_Interval__c
    * @return boolean  
    */

    private static boolean checkNewInfo(List<VGA_Service_Interval__c>  IntervalList){
        try{
            set<String> tempSet=new Set<String>();
            for(VGA_Service_Interval__c tmp:IntervalList){
                if(!tempSet.contains(''+tmp.VGA_Service_Order__c)){
                    tempSet.add(''+tmp.VGA_Service_Order__c);
                }else{
                    return true;
                }
            }
            return false;
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
            return true;
        }
    }

}