/**
* @author Archana Yerramilli
* @date 17/12/2018
* @description This class is created to separate Task creation related logic from Case Trigger.
*/

public class VGA_CaseTriggerHelperForTask {
    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description The below method is used to create tasks for new interaction ,check for Academy, check SLAs, create tasks for inbound email.
    * @param Case List
    */
    public static void createFollowUpTask(List<Case> lstNew, Map<Id,Case> mapOld)
    {
        ID academyRtID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Academy').getRecordTypeId();

        Set<Id> setUser = new Set<Id>();
        Map<String,VGA_Case_SLA__c> mapSLA = VGA_Case_SLA__c.getAll();
        List<Task> lstTask = new List<Task>();
        Map<Id,User> mapOwner = new Map<Id,User>();

        Map<String, Id> taskRecordTypesPerNameMap = getRecordTypesPerNameMap('Task');

        Map<String,BusinessHours> mapBH = new Map<String,BusinessHours>();

        //Build map with Business Hour records grouped by Name
        for(BusinessHours objbh : [SELECT Id, Name FROM BusinessHours WHERE IsActive = true])
        {
            mapBH.put(objbh.Name,objbh);
        }

        //Build set of users with the interaction owners Id
        for(Case obj : lstNew)
         {
            if(obj.OwnerId != Null)
            {
                 setUser.add(obj.OwnerId);
            }
        }

        if(setUser != Null && setUser.size() > 0)
        {
            mapOwner = new Map<Id,User>([SELECT Id
                                         FROM User
                                         WHERE Id IN : setUser
                                         AND isActive = true]);
        }

        Set<ID> setReopenCaseID = new Set<ID>();

        for(Case obj : lstNew)
        {
            BusinessHours bh;
            if(mapBH != null && !mapBH.isEmpty() )
            {
               bh = getBusinessHours(obj, mapBH);
            }

             //If the interaction has been accepted through the Omni Channel
             if(Trigger.isUpdate
                && (obj.VGA_Omni_Accepted_Date_Time__c != Null
                    && obj.VGA_Omni_Channel_Status__c == 'Opened'
                    && mapOld.get(obj.Id).VGA_Omni_Channel_Status__c != 'Opened')
                )
             {
                 if(obj.VGA_Omni_Queue_Name__c != Null
                    && mapSLA.containsKey(obj.VGA_Omni_Queue_Name__c)
                    && mapSLA.get(obj.VGA_Omni_Queue_Name__c) != Null
                    && mapOwner.containsKey(obj.ownerId)
                    && mapOwner.get(obj.ownerId) != Null)
                 {
                    DateTime slaDT = getSLADTForOmniChannelInter(obj, bh, mapSLA);
                    Id taskRecordTypeId = taskRecordTypesPerNameMap.containsKey('New Task') ? taskRecordTypesPerNameMap.get('New Task') : null;

                    //A new interaction assigned task will be created.
                    Task objtask = createNewTask(taskRecordTypeId, 'New Interaction Assigned', 'New Interaction Assigned',
                                                  'Open', 'Low', obj, slaDT);

                    lstTask.add(objtask);
                 }
             } //If the MapSLA contains the origin of a new created interaction
               //  cases created by SystemAutomation we are not creating Tasks
             else if((Trigger.isInsert || (Trigger.isUpdate && obj.ownerid != mapOld.get(obj.Id).ownerid && obj.status != 'closed'  ))
                     && obj.Origin != Null
                     && mapSLA.containsKey(obj.Origin)
                     && mapSLA.get(obj.Origin) != Null
                     && mapOwner.containsKey(obj.ownerId)
                     && mapOwner.get(obj.ownerId) != Null
                     && obj.ownerId != Label.VGA_UserId)
             {
                DateTime slaDT = getSLADTForChangeOwnerInteraction(obj, bh, mapSLA);
                Id taskRecordTypeId = taskRecordTypesPerNameMap.containsKey('New Task') ? taskRecordTypesPerNameMap.get('New Task') : null;
                Task objtask = createNewTask(taskRecordTypeId, 'New Interaction Assigned', 'New Interaction Assigned',
                                                  'Open', 'Medium', obj, slaDT);
                lstTask.add(objtask);
             }
             else if(
                      ((
                        (obj.Origin=='VW Email' || obj.Origin=='Skoda Email')
                        && obj.VGA_No_of_Incoming_Mails__c > 1
                      )
                      ||
                      (
                        obj.Origin!='VW Email'
                        && obj.Origin!='Skoda Email'
                        && obj.VGA_No_of_Incoming_Mails__c >= 1
                      ))
                      && Trigger.isUpdate
                      && obj.VGA_No_of_Incoming_Mails__c != Null
                      && obj.VGA_No_of_Incoming_Mails__c != mapOld.get(obj.Id).VGA_No_of_Incoming_Mails__c
                      && mapOwner.containsKey(obj.ownerId)
                      && mapOwner.get(obj.ownerId) != Null
                    )
             {
                // for including BusinessHours
                DateTime Dt = BusinessHours.nextStartDate(bh.id,System.Now().adddays(1));
                DateTime slaDT = Datetime.newInstance(Dt.date(),system.now().time());

                Id taskRecordTypeId = getTaskRecordType(taskRecordTypesPerNameMap, obj, academyRtID);
                Task objtask = createNewTask(taskRecordTypeId, 'New Inbound Email', 'Inbound Email',
                                             'Open', 'Medium', obj, slaDT);
                lstTask.add(objtask);
             }
             if(obj.VGA_Reopened_Case__c && obj.ownerid != null && string.valueof(obj.ownerid).startswith('005')
                && (mapOld == NULL || (mapOld != null && !string.valueof(mapOld.get(obj.id).ownerid).startswith('005'))))
             {
                 DateTime Dt = BusinessHours.nextStartDate(bh.id,System.Now());
                 DateTime slaDT = Datetime.newInstance(Dt.date(),system.now().time());

                Id taskRecordTypeId = getTaskRecordType(taskRecordTypesPerNameMap, obj, academyRtID);
                Task objtask = createNewTask(taskRecordTypeId, 'New Inbound Email', 'Inbound Email',
                                             'Open', 'Medium', obj, slaDT);

                lstTask.add(objtask);
                setReopenCaseID.add(obj.id);
             }
         }

         if(lstTask != Null && lstTask.size() > 0)
         {
             insert lstTask;
         }
         if(setReopenCaseID != null && !setReopenCaseID.isEmpty())
         {
             list<Case> lstCase2update = [SELECT Id,VGA_Reopened_Case__c
                                          FROM Case
                                          WHERE Id IN :setReopenCaseID];

             if(lstCase2update != null && !lstCase2update.isEmpty())
             {
                 for(Case objCase : lstCase2update)
                 {
                     objCase.VGA_Reopened_Case__c = false;
                 }
                 update lstCase2update;
             }
         }
    }

    /**
    * @author Archana Yerramilli
    * @date  18/12/2018
    * @description This method returns a bussiness hours record based on the business hour name.
    *              Currently we have three business hours Academy Team, Customer Support Team
    *              and Default. If the interaction origin is Academy Email then we will
    *              assign the Academy Team Business Hours.
    * @param Case interaction that will give us the origin
    * @param Map<String,BusinessHours> mapBH: Map with the Business Hours group by its name
    * @return BusinessHours record for this particular interaction
    */
    private static BusinessHours getBusinessHours(Case interaction,
                                                  Map<String,BusinessHours> mapBH){

        BusinessHours bh;

        if(interaction.Origin == 'Academy Email'
            && mapBH != Null
            && mapBH.containsKey('Academy Team')
            && mapBH.get('Academy Team') != Null)
        {
            bh = mapBH.get('Academy Team');
        }
        else if(mapBH != Null
                && mapBH.containsKey('Customer Support Team')
                && mapBH.get('Customer Support Team') != Null)
        {
            bh = mapBH.get('Customer Support Team');
        }
        else
        {
            bh = mapBH.get('Default');
        }

        return bh;
    }

    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description Method that returns a map with the record types of a sObject group by
    *              its Name
    * @param String objectAPIName: Name of the object for which we will get the record types.
    * @return Map<String, Id> Map with the record types Ids group by its Name
    */
    private static Map<String, Id> getRecordTypesPerNameMap(String objectAPIName){

        Schema.DescribeSObjectResult sobjectResult = Schema.getGlobalDescribe().get(objectAPIName).getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = sobjectResult.getRecordTypeInfos();
        Map<String,Id> mapofsObjectRecordTypeNameandId = new Map<String,Id>();

        for(Schema.RecordTypeInfo info : recordTypeInfo){
            mapofsObjectRecordTypeNameandId.put(info.getName(),info.getRecordTypeId());
        }

        return mapofsObjectRecordTypeNameandId;
    }

    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description Method that returns the record type Id of the task that we will be creating.
    * @param Map<String, Id> taskRecordTypesPerNameMap: Map with the task record types grouped by Name
    * @param Case interaction: Interaction that will be related to the task.
    * @param Id academyRtID: Id of the academy record type in the Interaction object.
    * @return Id of the task we will be creating. We'll check the related interaction
    *         record type to decide which value we will be returning.
    */
    private static Id getTaskRecordType(Map<String, Id> taskRecordTypesPerNameMap, Case interaction,
                                        Id academyRtID){
        if(interaction.RecordtypeID == academyRtID)
        {
           return taskRecordTypesPerNameMap.containsKey('New Task Academy') ? taskRecordTypesPerNameMap.get('New Task Academy') : null;
        }

        return taskRecordTypesPerNameMap.containsKey('New Task') ? taskRecordTypesPerNameMap.get('New Task') : null;
    }

    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description Method that returns SLA date time value when a new interaction is created when
    *              a interaction is accepted from the Omni Channel
    * @param Case interaction: Interaction that will be related to the new task
    * @param BusinessHours bh: BusinessHours that we will be using to calculate the SLA datetime.
    * @param Map<String,VGA_Case_SLA__c> mapSLA: Map with the Case SLA custom setting grouped by its name
    * @return Datetime with the SLA of the new task that will be created.
    */
    private static Datetime getSLADTForOmniChannelInter(Case interaction, BusinessHours bh,
                                                         Map<String,VGA_Case_SLA__c> mapSLA){

        VGA_Case_SLA__c objsla = mapSLA.get(interaction.VGA_Omni_Queue_Name__c);
        DateTime slaDT;

        if(interaction.VGA_Omni_Accepted_Date_Time__c != Null)
        {
            if(objsla.VGA_SLA_Mins__c == 1440)
            {
                DateTime Dt = BusinessHours.nextStartDate(bh.id,System.Now().adddays(1));
                slaDT = Datetime.newInstance(Dt.date(),System.Now().time());
            }
            else
            {
            slaDT = BusinessHours.add(bh.id,interaction.VGA_Omni_Accepted_Date_Time__c,integer.valueof(objsla.VGA_SLA_Mins__c)*60000);
            }
        }
        else
        {
            if(objsla.VGA_SLA_Mins__c == 1440)
            {
                DateTime Dt = BusinessHours.nextStartDate(bh.id,System.Now().adddays(1));
                slaDT = Datetime.newInstance(Dt.date(),System.Now().time());
            }
            else
            {
                slaDT = BusinessHours.add(bh.id,System.Now(),integer.valueof(objsla.VGA_SLA_Mins__c)*60000);
            }
        }

        return slaDT;
    }

    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description Method that returns SLA date time value when a new interaction is created when
    *              a change of ownership happens in an interaction.
    * @param Case interaction: Interaction that will be related to the new task
    * @param BusinessHours bh: BusinessHours that we will be using to calculate the SLA datetime.
    * @param Map<String,VGA_Case_SLA__c> mapSLA: Map with the Case SLA custom setting grouped by its name
    * @return Datetime with the SLA of the new task that will be created.
    */
    private static Datetime getSLADTForChangeOwnerInteraction(Case interaction, BusinessHours bh,
                                                              Map<String,VGA_Case_SLA__c> mapSLA){
        VGA_Case_SLA__c objsla = mapSLA.get(interaction.Origin);
        DateTime slaDT;

        if(objsla.VGA_SLA_Mins__c == 1440)
        {
            DateTime Dt = BusinessHours.nextStartDate(bh.id,System.Now().adddays(1));
            slaDT = Datetime.newInstance(Dt.date(),System.Now().time());
        }
        else
        {
            slaDT = BusinessHours.add(bh.id,interaction.CreatedDate,integer.valueof(objsla.VGA_SLA_Mins__c)*60000);
        }

        return slaDT;
    }

    /**
    * @author Archana Yerramilli
    * @date  17/12/2018
    * @description Method that creates a new task related to an interaction.
    *              The values of the fields will be coming as parameters.
    * @param Different fields that will be used to create the new task.
    * @return Task new created task.
    */
    public static Task createNewTask(Id recordTypeId, String subject, String actionRequired,
                              String taskStatus, String priority, Case interaction,
                              DateTime slaDT){

        Task objtask = new Task();
        objtask.RecordTypeId = recordTypeId;
        objtask.Subject      = subject;
        objtask.Status       = taskStatus;
        objtask.Priority     = priority;
        objtask.WhoId        = interaction.ContactId != Null ? interaction.ContactId : null;
        objtask.WhatId       = interaction.Id;
        objtask.OwnerId      = interaction.OwnerId;
        objtask.ActivityDate =  date.valueOf(slaDT.format('yyyy-MM-dd', 'Australia/Sydney'));
        objtask.VGA_Due_Date_Time__c       = slaDT;
        objtask.VGA_Interaction_Channel__c = objtask.VGA_Interaction_Channel__c != null ? interaction.Origin : null;
        objtask.VGA_Interaction_Queue__c   = objtask.VGA_Interaction_Queue__c   != null ? interaction.VGA_Omni_Queue_Name__c : null;
        objtask.VGA_Action_Required__c     = actionRequired;

        return objtask;
    }
}