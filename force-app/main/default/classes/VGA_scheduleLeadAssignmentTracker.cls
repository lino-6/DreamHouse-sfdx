//=================================================================================
// Tracker class for VGA_scheduleLeadAssignment class.
// =================================================================================
// Created by Nitisha Prasad on 22-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_scheduleLeadAssignmentTracker
{
    public static VGA_scheduleLeadAssignment sh1;
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        sh1 = new VGA_scheduleLeadAssignment();
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        System.schedule('test', sch, sh1); 
        Test.stopTest();
    } 
}