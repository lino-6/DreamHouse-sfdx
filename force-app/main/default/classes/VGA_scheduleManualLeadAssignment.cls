global class VGA_scheduleManualLeadAssignment implements Schedulable 
{
    global final string strRecordId;
    
    global VGA_scheduleManualLeadAssignment(String recordid)
    {
        strRecordId = recordid;
    }
    
    global void execute(SchedulableContext sc) 
    {
        VGA_ManualAssignmentBatch b = new VGA_ManualAssignmentBatch(strRecordId); 
        database.executebatch(b);
    }
}