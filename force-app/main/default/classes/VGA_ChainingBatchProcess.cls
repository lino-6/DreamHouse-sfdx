global class VGA_ChainingBatchProcess implements Database.Batchable<sObject>
{
    global Database.querylocator start(Database.BatchableContext bc)
    {
        string strAccountQuery = 'select Id from Account';
        strAccountQuery += ' where VGA_Subscription_Batch__c=true'; 
        return Database.getQueryLocator(strAccountQuery);
    }
    global void execute(Database.BatchableContext BC, List<Account> lstAccount)
    {
        try
        {
            list<Account>UpdateListofAccount =new list<Account>();
            list<Account>UpdateBatchAccount =new list<Account>();
            if(lstAccount != null && !lstAccount.isEmpty())
            { 
                for(Account objAccount : lstAccount)
                {
                    objAccount.VGA_Subscription_Batch__c =false;
                    objAccount.VGA_Batch__c =true;
                    UpdateListofAccount.add(objAccount);
                }
                 if(UpdateListofAccount !=Null && UpdateListofAccount.size()>0)
                 {
                 Database.update(UpdateListofAccount);
                 }
                for( Account objAccount :[select Id from Account where VGA_Batch__c =true])
                   {
                     objAccount.VGA_Batch__c =false; 
                     UpdateBatchAccount.add(objAccount);
                   }
                   if(UpdateBatchAccount!=Null && UpdateBatchAccount.size()>0)
            {
             Database.update(UpdateBatchAccount);
            }
            }
           
            
        }
        catch(Exception e)
        {
          system.debug('============'+e.getmessage());  
        }
     }
     global void finish(Database.BatchableContext BC)
     {
     }
}