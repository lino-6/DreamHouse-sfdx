/**
* @author Hijith NS
* @date 05/03/2019
* @description Test class to test VGA_SaveAttachmentAPI - for testdrive leads.
*/
@isTest
private class VGA_SaveAttachmentAPITest {

/*
*  @description test method  to test api request withoud attachment id.
*/
    @isTest static void testAttachmentWithOutId()
    {
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.VGA_Primary_id__c = 'dddddddddd';
        Insert objLead;

        Attachment attch = new Attachment();
        String pId =  objLead.VGA_Primary_id__c;
        String name = 'TestImage.jpg';
        String contentType = 'image/jpg';
        string body = 'test body';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('name', name);
        req.addParameter('pId', pId);
        req.addParameter('contentType', contentType);
        req.requestBody = Blob.valueof(body);
        req.requestURI = '/services/apexrest/saveAttachment';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        VGA_SaveAttachmentAPI.saveFile();
        Attachment testattch = [select id,name,ParentId,ContentType,Body from Attachment where ParentId =: objLead.Id And name =: name limit 1];

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('id', testattch.id);
        gen.writeEndObject();
        Test.stopTest();
        System.assertEquals(Blob.valueOf(gen.getAsString()),res.responseBody);
        System.assert(testattch!=Null);


    }
/*
*  @description test method  to test api request with attachment id.
*/
    @isTest static void testAttachmentWithId()
    {
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.VGA_Primary_id__c = 'dddddddddd';
        Insert objLead;

        Attachment attch = new Attachment();
        attch.ParentId = objLead.id;
        attch.Name = 'TestImage.jpg';
        attch.ContentType = 'image/jpg';
        attch.Body = Blob.valueof('test body');
        insert attch;

        String pId = objLead.VGA_Primary_id__c;
        String name = 'TestImage22.jpg';
        String id = attch.Id;
        String contentType = 'image/jpg';
        string body = 'test body';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('name', name);
        req.addParameter('pId', pId);
        req.addParameter('contentType', contentType);
        req.addParameter('id', id);
        req.requestBody = Blob.valueof(body);
        req.requestURI = '/services/apexrest/saveAttachment';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        VGA_SaveAttachmentAPI.saveFile();

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('id', attch.id);
        gen.writeEndObject();
        Test.stopTest();

        System.assertEquals(Blob.valueOf(gen.getAsString()),res.responseBody);

    }
/*
*  @description test method  to test api request with invalid attachment id.
*/
    @isTest static void testAttachmentWithInvalidId()
    {
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.VGA_Primary_id__c = 'dddddddd';
        Insert objLead;

        String pId = objLead.VGA_Primary_id__c;
        String name = 'TestImage22.jpg';
        String id = 'dsfsfsfsf';
        String contentType = 'image/jpg';
        string body = 'test body';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('name', name);
        req.addParameter('pId', pId);
        req.addParameter('contentType', contentType);
        req.addParameter('id', id);
        req.requestBody = Blob.valueof(body);
        req.requestURI = '/services/apexrest/saveAttachment';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        VGA_SaveAttachmentAPI.saveFile();
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeEndObject();
        Test.stopTest();
        System.assertEquals(gen.getAsString(),res.responseBody.toString());

    }
/*
*  @description test method  to test api request with invalid lead id.
*/
    @isTest static void testAttachmentWithInvalidLeadId()
    {
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.VGA_Primary_id__c = 'ddddd';
        Insert objLead;

        String pId = objLead.VGA_Primary_id__c;
        String name = 'TestImage22.jpg';
        String contentType = 'image/jpg';
        string body = 'test body';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('name', name);
        req.addParameter('pId', pId);
        req.addParameter('contentType', contentType);
        req.requestBody = Blob.valueof(body);
        req.requestURI = '/services/apexrest/saveAttachment';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        VGA_SaveAttachmentAPI.saveFile();

        id id = [select id from Attachment where ParentId =: objLead.Id and name =: name].id;
        req.addParameter('pId', 'sdsdsds');
        req.addParameter('name', 'newname');
        req.addParameter('id', id);
        VGA_SaveAttachmentAPI.saveFile();
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeEndObject();
        Test.stopTest();
        System.assertEquals(Blob.valueOf(gen.getAsString()),res.responseBody);

    }
/*
*  @description test method  to test api request with null lead id.
*/
    @isTest static void testAttachmentWithNullLeadId()
    {
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.VGA_Primary_id__c = 'ddddss';
        Insert objLead;

        String pId = objLead.VGA_Primary_id__c;
        String name = 'TestImage22.jpg';
        String contentType = 'image/jpg';
        string body = 'test body';
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.addParameter('name', name);
        req.addParameter('pId', pId);
        req.addParameter('contentType', contentType);
        req.requestBody = Blob.valueof(body);
        req.requestURI = '/services/apexrest/saveAttachment';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        VGA_SaveAttachmentAPI.saveFile();
        id id = [select id from Attachment where ParentId =: objLead.Id and name =: name].id;
        req.addParameter('pId', Null);
        req.addParameter('name', 'newname');
        req.addParameter('id', id);
        VGA_SaveAttachmentAPI.saveFile();
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeEndObject();
        Test.stopTest();
        System.assertEquals(Blob.valueOf(gen.getAsString()),res.responseBody);

    }
}