public without sharing class VGA_TestDriveCalendarController
{
	 /**
    * @author Archana Y
    * @date 04/02/2019
    * @description The below method is used to fetch all events for a particular user
    * @param  User ID
    * @return  List of Events          
    */
    @AuraEnabled
    public static list<Event> getAllEvents(String userId)
    {
     	try
        {
            List<Id> RecordTypeIdList=new List<Id>();
            RecordTypeIdList.add(Schema.SObjectType.Event.getRecordTypeInfosByName().get('Unavailable').getRecordTypeId());
            RecordTypeIdList.add(Schema.SObjectType.Event.getRecordTypeInfosByName().get('Test Drive Appointments').getRecordTypeId());
            return [SELECT Id, WhoID, DurationInMinutes, ActivityDateTime,RecordType.Name,RecordTypeId , EndDateTime,StartDateTime,Subject, OwnerId, Location FROM Event where OwnerId =: userId AND RecordTypeId in:RecordTypeIdList];
        }
        catch(Exception ex){
            System.debug(ex+' '+ex.getMessage()+' '+ex.getLineNumber()); 
        }
        
        return new List<Event>();
    }
    
    /**
    * @author Lino D Alonso
    * @date 04/02/2019
    * @description The below method is used to Update event/ Save event start date time to event object
    * @param  Event ID and Start Date
    * @return Void         
    */
    @AuraEnabled
    public static void updateEvent(string eventid,string eventdate)
    {
        string eventdatelist = eventdate.replace('T',' ');        
        Event e= new event(id = eventid);
        e.StartDateTime = DateTime.valueof(eventdatelist);
        update e;
    }

    /**
    * @author Lino D Alonso
    * @date 04/02/2019
    * @description The below method is used to Update event/ Save event end date/time to event object
    * @param  Event ID and End Date
    * @return  List of Events          
    */
    @AuraEnabled
    public static void updateEventEndDate(string eventid,string eventdate)
    {
        string eventdatelist = eventdate.replace('T',' ');
        Event e= new event(id = eventid);
        e.EndDateTime = DateTime.valueof(eventdatelist);
        update e;
   
    }
    
    /**
    * @author Archana
    * @date 05/02/2019
    * @description The below method creates an event
    * @param  Event ID and Date
    * @return         
    */    
    @AuraEnabled
    public static void createEvent(string eventid,string eventdate)
    {
        string eventdatelist = eventdate.replace('T',' ');       
        Event e= new event(id = eventid);
        e.StartDateTime = DateTime.valueof(eventdatelist);

        Upsert e;
   
    }

    /**
    * @author Archana
    * @date 05/02/2019
    * @description The below method is used to get users list
    * @param
    * @return List of Users /Consultants picklist         
    */ 
    @AuraEnabled
    public static List<User> collectUsersInfo(){
        String Ids =UserInfo.getUserId();
        String AccountIds = VGA_Common.getloggingAccountId();
        return null;
    }

    /**
    * @author Archana
    * @date 05/02/2019
    * @description The below method is used to get users list
    * @param
    * @return List of Users /Consultants picklist         
    */ 
    @AuraEnabled
    public static List<User> getlistofUser()
    {
        try{
            String Ids =UserInfo.getUserId();
            String AccountIds =VGA_Common.getloggingAccountId();
            Map<Id,User> MapUser=new Map<Id,User>([SELECT Id,Name,ContactId,Contact.Name,contact.AccountId 
                                                   FROM User 
                                                   WHERE contact.AccountId =: AccountIds
                                                   AND Contact.VGA_Partner_User_Status__c = 'Enabled' 
                                                   AND IsActive=true  limit 999]);
            List<User> FinalUserList=new List<User>();
            User CurrentUser;

            for(User u:MapUser.values()){
                if(u.Id==Ids){
                    FinalUserList.add(u);
                    MapUser.remove(u.Id);
                    break;
                }
            }
            FinalUserList.addAll(MapUser.values());
            return FinalUserList;
        }catch(Exception ex){
            System.debug(ex+' '+ex.getMessage()+' '+ex.getLineNumber());
        }
        return null;
    }

    /**
    * @author Lino D Alonso
    * @date 05/02/2019
    * @description The below method is used to event details
    * @param Event ID
    * @return Event Obj        
    */ 
    @AuraEnabled
    public static Event collectEventDetails(String EventId)
    {
            return [select Id, EndDateTime, StartDateTime,RecordType.Id,RecordType.Name, Subject from Event where Id=:EventId];
    }
}