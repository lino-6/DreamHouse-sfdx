// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          1-Nov-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This class is used in InvalidEmail Component.
public class VGA_WrapperofInvalidEmail 
{   
    @AuraEnabled
    public String Email{get;set;}  //To get Email Value.
    @AuraEnabled
    public VGA_Invalid_Email__c objInvalidEmail{get;set;}  //To get Email Value.
}