//Tracker class for VGA_InvalidEmailController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          7-Nov-2017 
//===============================================================================================
//Coverage for VGA_InvalidEmailController on                7-Nov-2017      86%
//===============================================================================================
@isTest    
public class VGA_InvalidEmailControllerTracker 
{
    public static VGA_Invalid_Email__c objInvalidEmail;
    public static Account objAccount;
    
    @testsetup
    Static void insertcustomsetting()
    {
        list<VGA_MuleUrl__c> customsettingdata =   VGA_CommonTracker.buildCustomSettingforMule();
        insert customsettingdata;
        
        VGA_Invalid_EmailCS__c objInvalidEmail1 = new VGA_Invalid_EmailCS__c();
        objInvalidEmail1.Name = 'Invalid Email';
        objInvalidEmail1.VGA_Active__c = true;
        insert objInvalidEmail1;
        
        objAccount = VGA_CommonTracker.createDealerAccount();
        
        User objUser = VGA_InvalidEmailController.getlogginguser();
        objInvalidEmail = VGA_CommonTracker.createInvalidEmail(objAccount.id);
        
    }
     /**
    * @author Ganesh M
    * @date 09/07/2019
    * @description Method that tests updated Email with given Emailid.
    */
    static testMethod void test01_UpdatedEmail_whenEmailidgiven() 
    
    
    {
    
       Test.startTest();
        VGA_InvalidEmailController.getlogginguser();
        
       VGA_Invalid_Email__c objInvalidEmail=[select id,VGA_Brand__c from VGA_Invalid_Email__c where VGA_Brand__c='Volkswagen'];
       system.debug('*********'+objInvalidEmail);
               
       VGA_InvalidEmailController.getlistofInvalidEmail('createdDate',false);
        
        string strupdateInvalidEmail = VGA_InvalidEmailController.updateInvalidEmail('rkp@rkp.com',objInvalidEmail.id);
        string strupdateIgnore = VGA_InvalidEmailController.updateIgnore(objInvalidEmail.id);
        string strgetuserInformation = VGA_InvalidEmailController.getuserInformation();
         VGA_Invalid_Email__c objInvalidEmailtest=[select id,VGA_Brand__c,VGA_Email_Address__c,VGA_Invalid_Email__c from VGA_Invalid_Email__c where VGA_Brand__c='Volkswagen'];
         System.assertEquals(objInvalidEmailtest.VGA_Email_Address__c,'rkp@rkp.com');
      
       Test.stopTest();
    }  
    public static testMethod void Test2() 
    {
      
       
       Test.startTest();
        VGA_InvalidEmailController.getlogginguser();
        
        VGA_Invalid_Email__c objInvalidEmail=[select id,VGA_Brand__c from VGA_Invalid_Email__c where VGA_Brand__c='Volkswagen'];
        
        VGA_InvalidEmailController.getlistofInvalidEmail('createdDate',false);
        
        string strupdateInvalidEmail = VGA_InvalidEmailController.updateInvalidEmail('rkp',objInvalidEmail.id);
        string strupdateIgnore = VGA_InvalidEmailController.updateIgnore(null);
        string strgetuserInformation = VGA_InvalidEmailController.getuserInformation();
       Test.stopTest();
    }   
   
    
}