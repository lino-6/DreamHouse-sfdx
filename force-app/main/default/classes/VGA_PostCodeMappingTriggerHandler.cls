public class VGA_PostCodeMappingTriggerHandler 
{
    public void runTrigger()
    {
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((list<VGA_Post_Code_Mapping__c>) trigger.new);
        }
        
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((list<VGA_Post_Code_Mapping__c>) trigger.new,(map<id,VGA_Post_Code_Mapping__c>) trigger.oldmap);
        }
    }
    
    private void onBeforeInsert(list<VGA_Post_Code_Mapping__c> lsttriggernew)
    {
        //Call methods to populate the dealers before insert
        VGA_DealerCodesProcess.checkDealerCodes(lsttriggernew,null);
    }
    
    private void onBeforeUpdate(list<VGA_Post_Code_Mapping__c> lsttriggernew,map<ID,VGA_Post_Code_Mapping__c> triggeroldmap)
    {
        //Call methods to populate the dealers before update
        VGA_DealerCodesProcess.checkDealerCodes(lsttriggernew,triggeroldmap);
    }
}