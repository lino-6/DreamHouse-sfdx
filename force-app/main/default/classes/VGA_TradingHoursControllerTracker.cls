//Tracker class for VGA_TradingHoursController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for VGA_TradingHoursController on                8-Nov-2017      90%
//===============================================================================================
@isTest(seeAllData = false) 
public class VGA_TradingHoursControllerTracker 
{
    public static testMethod void Test1()
    {
        Account objAccount =VGA_CommonTracker.createDealerAccount();
        Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
        User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
        VGA_Dealership_Profile__c objDealership =VGA_CommonTracker.createDealershipProfile(objAccount.Id);
        VGA_Trading_Hour__c objTrading         =VGA_CommonTracker.createTrading_Hour(objDealership.Id);
        
        system.runAs(objUser)
        {
        VGA_TradingHoursController.listofTradingHours('Passenger');
        VGA_TradingHoursController.getDealershipDetails('Passenger');
        VGA_TradingHoursController.getTradingDetails(objTrading.Id);
        VGA_TradingHoursController.saveDealershipDetails(objDealership, objUser.Id);
        VGA_TradingHoursController.saveTradingDetails(objTrading)    ;
        VGA_TradingHoursController.getlogginguser();
        VGA_TradingHoursController.listofuser(objDealership,'Passenger');
        }

    }
    public static testMethod void Test2()
    {
        Account objAccount =VGA_CommonTracker.createDealerAccount();
        Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
        User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
        VGA_Dealership_Profile__c objDealership =VGA_CommonTracker.createDealershipProfile(objAccount.Id);
        VGA_Trading_Hour__c objTrading         =VGA_CommonTracker.createTrading_Hour(objDealership.Id);
        
        system.runAs(objUser)
        {
        VGA_TradingHoursController.listofTradingHours('Passenger');
        VGA_TradingHoursController.getDealershipDetails('Passenger');
        VGA_TradingHoursController.getTradingDetails(objTrading.Id);
        //VGA_TradingHoursController.saveDealershipDetails(null, null);
        VGA_TradingHoursController.saveTradingDetails(null)    ;
        VGA_TradingHoursController.getlogginguser();
        VGA_TradingHoursController.listofuser(objDealership,'Passenger');
        }

    }

}