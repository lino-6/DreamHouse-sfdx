/*
*@Author : Ganesh M
*@DATE   : 10/07/2019
*@Description : Test class for the class MuleAPI. 

*/
@isTest
private class VGA_MuleApiTest {
    
    @testsetup
    Static void insertcustomsetting()
    {
         list<VGA_MuleUrl__c> customsettingdata =   VGA_CommonTracker.buildCustomSettingforMule();
        
        insert customsettingdata;
        Account personAccount = new Account();
        // personAccount.RecordTypeId = personAccountRT;
        personAccount.LastName     = 'Testlastname';
        personAccount.PersonEmail  =  't@test.com';
        personAccount.Phone = '5698754236';
        // personAccount.VGA_Region_code__c = '5698';
        PersonAccount.BillingPostalCode = '2000';
        insert personAccount;
        
        
        
    }
    /**
  * @author Ganesh M
  * @date 10/07/2019
  * @description Method that tests the MuleApi.
  *              Methos return PrimaryID;
  */
    @isTest static void test_01() {
        // Set mock callout class 
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : "12365445"}');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
        
        Muleapi m = new Muleapi(); 
        
        string res = m.personAccountSearch();
        system.assertEquals(res, '12365445');
        
        Test.stopTest();
        
        
    }
    @isTest static void test_02() {
        // Set mock callout class 
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : "12365445"}');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
        
        Muleapi m = new Muleapi(); 
        
        string res = m.businessContactSearch();
        
        
        Test.stopTest();
        
        
    }
    @isTest static void test_03() {
        // Set mock callout class 
        MuleRequestMock fakeResponse = new MuleRequestMock(200, '{"primary_id" : "12365445"}');
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
        
        Muleapi m = new Muleapi(); 
        
        string res = m.businessAccountSearch();
       
        Test.stopTest();
        
        
    }
}