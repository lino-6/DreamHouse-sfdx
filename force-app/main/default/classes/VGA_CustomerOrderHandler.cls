// ------------------------------------------------------------------------------------------------------
// Version#     Date                Author                    
// ------------------------------------------------------------------------------------------------------
// 1.0          31-Oct-2017      Surabhi Ranjan         
// ------------------------------------------------------------------------------------------------------
//Description: This  class is used to VGA_CustomerOrderTrigger
public class VGA_CustomerOrderHandler 
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
            onBeforeInsert((List<VGA_Customer_Order_Details__c>) trigger.new);     
        }
        
        // Method will be called to handle After Insert events
        if (Trigger.isAfter && Trigger.isInsert)
        {
        
        }
        
        // Method will be called to handle Before Update events
        if (Trigger.isBefore && Trigger.isUpdate)
        {
            onBeforeUpdate((List<VGA_Customer_Order_Details__c>) trigger.new, (Map<Id, VGA_Customer_Order_Details__c>) trigger.OldMap);    
        }
        
        // Method will be called to handle After Update events
        if (Trigger.isAfter && Trigger.isUpdate)
        {
            
        }
    }
    
    // Method calls all the methods required to be executed before insert
    public void onBeforeInsert(List<VGA_Customer_Order_Details__c> lstTriggerNew)
    {
        tagDealerAccount(lstTriggerNew,null);
        updateEmailValidCheckBox(lstTriggerNew,null);
    }
    public void onBeforeUpdate(List<VGA_Customer_Order_Details__c> lstTriggerNew,Map<Id,VGA_Customer_Order_Details__c> mapTriggerOld)
    {
       // updateEmailValidCheckBox(lstTriggerNew,mapTriggerOld);
    }
    
         
    // This method is used for tag dealer Account according to dealer code.
    public void tagDealerAccount(list<VGA_Customer_Order_Details__c> lsttriggernew,map<id,VGA_Customer_Order_Details__c> triggeroldmap)
    {
        map<String,Id>mapofAccount =new map<String,Id>();
        set<String>setofDealercode =new set<String>();
        
        for(VGA_Customer_Order_Details__c objcustomer : lsttriggernew)
        {
            if(objcustomer.VGA_Dealer_Code__c!=Null)
            {
                setofDealercode.add(objcustomer.VGA_Dealer_Code__c);
            }
        }
        if(setofDealercode !=Null)
        {
            list<Account>listofAccount =new list<Account>([select Id,Name,VGA_Dealer_Code__c from Account where VGA_Dealer_Code__c IN:setofDealercode]);
            if(listofAccount !=Null && listofAccount.size()>0)
            {
                for(Account objAccount:listofAccount)
                {
                    mapofAccount.put(objAccount.VGA_Dealer_Code__c,objAccount.Id);  
                }
            }
        }
        if(mapofAccount !=Null)
        {
            for(VGA_Customer_Order_Details__c objcustomer : lsttriggernew)
            {
                if(objcustomer.VGA_Dealer_Code__c!=Null)
                {
                    objcustomer.VGA_Dealer_Account__c= mapofAccount.get(objcustomer.VGA_Dealer_Code__c);
                }
            } 
        }
    }
    private static void updateEmailValidCheckBox(list<VGA_Customer_Order_Details__c> lsttriggernew,map<Id,VGA_Customer_Order_Details__c> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            set<ID> setAccountID = new set<ID>();
            for(VGA_Customer_Order_Details__c objCOD : lsttriggernew)
            {
                if(objCOD.VGA_Customer__c != null)
                {
                    setAccountID.add(objCOD.VGA_Customer__c);
                }
            }
            if(setAccountID != null && !setAccountID.isEmpty())
            {
                map<ID,Boolean> mapofCOD = new map<Id,Boolean>();
                for(Account objAccount : [select id,VGA_Email_Address_valid__c from Account where id in:setAccountID limit 49999])
                {
                    if(!mapofCOD.containskey(objAccount.id))
                        mapofCOD.put(objAccount.id,objAccount.VGA_Email_Address_valid__c);
                }
                if(mapofCOD != null && !mapofCOD.isEmpty())
                {
                    for(VGA_Customer_Order_Details__c objCOD : lsttriggernew)
                    {
                        if(objCOD.VGA_Customer__c != null && mapofCOD.containskey(objCOD.VGA_Customer__c) 
                            && mapofCOD.get(objCOD.VGA_Customer__c) != null)
                        {
                            objCOD.VGA_Email_Address_valid__c = mapofCOD.get(objCOD.VGA_Customer__c);   
                        }
                    }
                }
            }
        }
    }
}