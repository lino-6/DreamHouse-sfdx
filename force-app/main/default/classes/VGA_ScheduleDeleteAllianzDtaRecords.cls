global class VGA_ScheduleDeleteAllianzDtaRecords implements Schedulable  
{
    global void execute(SchedulableContext sc)
    {
        VGA_DeleteAllianzDataRecords objBatch = new VGA_DeleteAllianzDataRecords();
        Database.executeBatch(objBatch);
    }
}