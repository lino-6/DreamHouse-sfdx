@isTest
public class VGA_TestDataFactory
{
    /* @date 10/12/2018
    * @description Method that receives a list of active brochures names and create a related
    *              record for all of them. Adding values to all the fields including the sort order.
    *              It returns the list of new Active Brochures.
    */
    public static List<VGA_Active_Brochure__c> buildActiveBrochures(Set<String> namesSet)
    {
        Integer sortOrder = 1; 
        List<VGA_Active_Brochure__c> activeBrochureList = new List<VGA_Active_Brochure__c>();

        for(String name : namesSet){
            VGA_Active_Brochure__c activeBrochure = new VGA_Active_Brochure__c();
            activeBrochure.VGA_Brand__c = 'Volkswagen';
            activeBrochure.Name         = name;
            activeBrochure.VGA_Brochure_Request_Active__c = true;
            activeBrochure.VGA_Test_Drive_Active__c       = true;
            activeBrochure.VGA_Sub_Brand__c               = 'Passenger Vehicles (PV)';
            activeBrochure.VGA_Image_URL__c               = 'https://secure.volkswagenaustralia.com.au/vehicle-image/Polo_Launch_Edition_MY18.5.jpg';
            activeBrochure.VGA_Brochure_Label__c          =  name;
            activeBrochure.VGA_Brochure_File_URL__c       = 'https://secure.volkswagenaustralia.com.au/brochures/MY18_5/Polo_Brochure_MY18.5.pdf';
            activeBrochure.VGA_Sort_Order__c              = sortOrder;
            
            sortOrder ++;
            
            activeBrochureList.add(activeBrochure);
        }


        return activeBrochureList;
    }

    /* @date 10/12/2018
    * @description Method that receives a set of preference names and its related active brochure Id
    *              and created a new record for all of them.
    *              It will return a list with the new model attributes
    */
    public static List<VGA_Model_Attribute__c> buildModelAttributes(Set<String> prefNamesSet, Id activeBrochureId)
    {
        Integer sortOrder = 1; 
        List<VGA_Model_Attribute__c> modelAttributeList = new List<VGA_Model_Attribute__c>();

        for(String prefName : prefNamesSet){
            VGA_Model_Attribute__c modelAttribute = new VGA_Model_Attribute__c();
            modelAttribute.VGA_Active_Brochure__c = activeBrochureId;
            modelAttribute.VGA_Preference__c      = prefName;
            modelAttribute.VGA_Model_image_URL__c = 'https://secure.volkswagenaustralia.com.au/vehicle-image/ro-welcome--AW19TY/18--Z1Z1.png';
            modelAttribute.VGA_Sort_Order__c      = sortOrder;
        
            
            sortOrder ++;
            
            modelAttributeList.add(modelAttribute);
        }


        return modelAttributeList;
    }
}