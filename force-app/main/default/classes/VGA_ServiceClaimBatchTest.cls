/*
*author : MOHAMMED ISHAQUE SHAIKH
*description : Test Class is used to cover VGA_ServiceClaimBatch
*Created Date : 2019-07-10
*/
@isTest(seeAllData=false)
public with sharing class VGA_ServiceClaimBatchTest {
    
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name : buildConfig
    * Description : to Setup Testdata for VGA_ServiceClaimBatch
    */
    @TestSetup
    static void buildConfig(){
        Account DealerAccount=VGA_CommonTracker.createDealerAccount();
        Contact DealerContact=VGA_CommonTracker.createDealercontact(DealerAccount.Id);
        User ContactUser=VGA_CommonTracker.CreateUser(DealerContact.Id);
        Product2 Model=VGA_CommonTracker.createProduct('MERCEDES');
        Model.VGA_Brand__c = 'PV Volkswagen';
        Model.VGA_Model_Year__c='2019'; 
        insert Model;
        Asset Vehicle=VGA_CommonTracker.buildVehicle('TESTVIN',DealerAccount.Id);
        Vehicle.Product2Id=Model.Id;
        insert Vehicle;
        Asset Vehicle1=VGA_CommonTracker.buildVehicle('TESTVIN1',DealerAccount.Id);
        Vehicle1.Product2Id=Model.Id;
        insert Vehicle1;
        VGA_Vehicle_Policy__c Policy=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy.VGA_Component_Type__c  = 'SVCPACK';
        Policy.VGA_Component_Code__c  = 'SP3YR_1';
        Policy.Is_Extended_Warranty__c= false;
        Policy.VGA_Policy_Start_Date__c=System.today();
        Policy.VGA_Policy_End_Date__c=System.today().addYears(3);
        insert Policy;

        VGA_Vehicle_Policy__c Policy1=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy1.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy1.VGA_Component_Type__c  = 'SVCPACK';
        Policy1.VGA_Component_Code__c  = 'SP3YR';
        Policy1.Is_Extended_Warranty__c= false;
        Policy1.VGA_Policy_Start_Date__c=System.today().addYears(2);
        Policy1.VGA_Policy_End_Date__c=System.today().addYears(5);
        insert Policy1;

        VGA_Vehicle_Policy__c Policy2=VGA_CommonTracker.buildVehiclePolicy(Vehicle.Id);
        Policy2.VGA_Sub_Brand__c       = 'Passenger Vehicles (PV)';
        Policy2.VGA_Component_Type__c  = 'SVCPACK';
        Policy2.VGA_Component_Code__c  = 'SP3YR';
        Policy2.Is_Extended_Warranty__c= false;
        Policy2.VGA_Policy_Start_Date__c=System.today().addYears(-5);
        Policy2.VGA_Policy_End_Date__c=System.today().addYears(-2);
        insert Policy2;

        
        List<VGA_Service_Interval__c> IntervalList=new List<VGA_Service_Interval__c>{
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P015','15000kms or 12 months',15000,12,Model.Id,Policy.VGA_Component_Code__c,1),
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P030','30000kms or 24 months',30000,24,Model.Id,Policy.VGA_Component_Code__c,2),
            VGA_ServicePlanClaimsControllerTest.buildServiceInterval('P045','45000kms or 36 months',45000,36,Model.Id,Policy.VGA_Component_Code__c,3)
        };
        insert IntervalList;
        List<VGA_Service_Item__c> ItemList=new List<VGA_Service_Item__c>{
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[0].Id,'L',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[0].Id,'S',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[0].Id,'P',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[1].Id,'L',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[1].Id,'P',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[1].Id,'S',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[2].Id,'L',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[2].Id,'P',55.0,100.0),
            VGA_ServicePlanClaimsControllerTest.buildServiceItem(IntervalList[2].Id,'S',55.0,100.0)
        };
        insert ItemList;
        List<VGA_Service_Claim__c> ServiceClaimList=new List<VGA_Service_Claim__c>{
            VGA_ServicePlanClaimsControllerTest.buildServiceClaim(Vehicle.Id,5000,DealerAccount.Id,IntervalList[1].Id,Policy.Id)
        };
        insert ServiceClaimList;
    }
    /*
    *author : MOHAMMED ISHAQUE SHAIKH
    *Method Name : TestAllScenarios
    * Description : to cover VGA_ServiceClaimBatch & VGA_ScheduleServiceClaim Class
    */
    @isTest
    static void TestAllScenarios(){
        id serviceClaimId =[select Id from VGA_Service_Claim__C limit 1].id ;
        VGA_ServicePlanClaimsController.Response resp=VGA_ServicePlanClaimsController.collectVINDetails('TESTVIN');
        System.assertEquals(true, resp.pass);
        resp=VGA_ServicePlanClaimsController.collectVINDetails('TESTVIN1');
        System.assertEquals(false, resp.pass);
        VGA_ServicePlanClaimsController.ClaimWrapperClass temp=new VGA_ServicePlanClaimsController.ClaimWrapperClass();
        temp.AssetId=[select Id from Asset where Name='TESTVIN'].Id;
        temp.DealerId=[select id From Account limit 1].Id;
        temp.IntervalId=[select Id from VGA_Service_Interval__c where VGA_Service_Order__c=3].Id;
        temp.KM='0';
        temp.RONumber='sdsd';
        temp.ServiceDate=''+System.today();
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=3].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today(),5000,[select id from Asset where name='TESTVIN' limit 1].id);
        System.assertEquals(true, resp.pass);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=3].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today(),1000000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=3].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today().adddays(-100),5000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.ValidateClaim([select Id from VGA_Service_Interval__c where VGA_Service_Order__c=3].Id,[select Id from VGA_Vehicle_Policy__c where VGA_Component_Code__c  = 'SP3YR_1'].id,System.today().adddays(-100),500000,[select id from Asset where name='TESTVIN' limit 1].id);
        resp=VGA_ServicePlanClaimsController.saveClaim(Json.serialize(temp));
        VGA_Service_Claim__c  data=(VGA_Service_Claim__c ) resp.result;
        System.assertEquals([select Id from VGA_Service_Claim__C where id!=:serviceClaimId limit 1].id ,data.id);

        test.startTest();
        system.schedule('ServiceClaimJob','0 0 22 * * ? *',new VGA_ScheduleServiceClaim());
        Database.executeBatch(new VGA_ServiceClaimBatch());
        system.schedule('ServiceClaimJob','0 0 22 * * ? *',new VGA_ScheduleServiceClaim());
        test.stopTest();
    }

}