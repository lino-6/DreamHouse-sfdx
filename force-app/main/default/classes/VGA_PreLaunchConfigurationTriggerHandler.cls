public with sharing class VGA_PreLaunchConfigurationTriggerHandler 
{
    public void runTrigger()
    {
        // Method will be called to handle Before Insert events
        if(Trigger.isBefore && Trigger.isInsert)
        {
           
        }
        
        // Method will be called to handle Before Update events
        if (Trigger.isBefore && Trigger.isUpdate)
        {
           onbeforeUpdate((list<VGA_Pre_Launch_Configuration__c>) trigger.new,(map<id, VGA_Pre_Launch_Configuration__c>) trigger.OldMap);
        }
        
        // Method will be called to handle After Insert events
        if (Trigger.isAfter && Trigger.isInsert)
        {
        }
        
        // Method will be called to handle After Update events
        if(Trigger.isAfter && Trigger.isUpdate)
        {
            
        }
    }
    
    private void onbeforeUpdate(list<VGA_Pre_Launch_Configuration__c> lsttriggernew,map<id,VGA_Pre_Launch_Configuration__c> triggeroldmap)
    {
        startBatch(lsttriggerNew,triggeroldmap);
    }
       
    public static void startBatch(list<VGA_Pre_Launch_Configuration__c> lsttriggernew, map<id,VGA_Pre_Launch_Configuration__c> triggeroldmap)
    {
        if(lsttriggernew != null && !lsttriggernew.isEmpty())
        {
            for(VGA_Pre_Launch_Configuration__c objPLC : lsttriggernew)
            {
                if(objPLC.VGA_Start_Batch__c && (trigger.isInsert || (trigger.isUpdate && triggeroldmap.get(objPLC.id).VGA_Start_Batch__c == false)))
                {
                    objPLC.VGA_Status__c = 'In Progress';
                    VGA_PreLaunchAssignmentBatch objCls = new VGA_PreLaunchAssignmentBatch(objPLC.id); 
                    Database.executeBatch(objCls);       
                }
            }
        }
    }  
}