@RestResource(urlMapping='/getPreBookedTimeSlot/*')
global class VGA_PrebookedTimeSlotsAPI{
    
    public static String dealerTimeZone;

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description REST API to return available time slots for Test Drive appointments.
    */
    @HttpGet
    global static void getPreBookedTimeSlots() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        //Get parameters
        String subbrand    = req.params.get('subbrand');
        String dealerCode    = req.params.get('dealerCode');
        String prebookedDate = req.params.get('date'); 

        //String subbrand       = 'Passenger Vehicles (PV)';
        //String dealerCode     = '2211';
        //String prebookedDate  = '2019-03-05'; 
        String prebookedDatetime = prebookedDate + ' 00:00:00';
        

        //Get the dealer account related to the dealer code that has been passed as parameter.
        Account dealerAccount = getDealerAccount(dealerCode);
        
        List<String> timeSlotsList = new List<String>();

        if(dealerAccount != null){
            dealerTimeZone = dealerAccount.VGA_Timezone__c;
            String dateString = Datetime.valueOf(prebookedDatetime).format('EEEE').trim().tolowercase();
            Date eventDate = Date.valueOf(prebookedDate);

            List<VGA_Trading_Hour__c> tradingHourList = getListofTradingHours(dealerAccount.Id, subbrand);
            timeSlotsList = getTimeSlotsList(dealerAccount, dateString, dealerCode, subbrand, eventDate);
        }
        
        String timeSlotsJSON = JSON.serialize(timeSlotsList);

        System.debug(timeSlotsJSON);
        res.responseBody = Blob.valueOf(timeSlotsJSON);
    }// method

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method is used to return the list of time slots available for
    *              a certain Dealer Account.
    * @param dealerAccount Dealer account where the user is trying to book the test drive
    * @param dateString Date when the appointment will take place in a string format
    * @param dealercode of the Dealer Account
    * @param subbrand of the vehicle that will be used in the test drive appointment
    * @param dateString Date when the appointment will take place in a date format
    * @return List<String> List of time slots when this dealer is available to receive test drive appointments
    */
    private static List<String> getTimeSlotsList(Account dealerAccount, String dateString, 
                                                 String dealerCode, String subbrand, 
                                                 Date eventDate){
        List<VGA_Trading_Hour__c> tradingHourList = getListofTradingHours(dealerAccount.Id, subbrand);
        Map<String, VGA_Trading_Hour__c> tradingHourPerNameMap = getTradingHoursPerDay(tradingHourList);

        List<String> timeSlotsList = new List<String>();

        if(!tradingHourPerNameMap.containsKey(dateString)){
            return timeSlotsList;
        }
        VGA_Trading_Hour__c tradHour = tradingHourPerNameMap.get(dateString);

        //If the dealer is not pilot then we will always return all the available time slots
        if(dealerAccount.VGA_Pilot_Dealer__c){
            populateDealerContactTimeSlotList( tradHour, timeSlotsList, eventDate, dealerAccount.Id, subbrand, dateString);
        } else {
            populateStandardTimeSlotList(tradHour, timeSlotsList, eventDate);
        }
        

        return timeSlotsList;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method is used to populate all the time slots for a dealer 
    *              that is not pilot. We will create 30 min intervals from the dealership
    *              open hour until the time it closes.
    * @param tradingHour Trading hour record for this particular dealership
    * @param timeSlotsList List where we will be populating the available time slots
    * @param eventDate Date when the dealership will take place
    */
    private static void populateStandardTimeSlotList(VGA_Trading_Hour__c tradingHour, 
                                                     List<String> timeSlotsList, 
                                                     Date eventDate){
        
        //Get the date of today when the dealer opens and closes
        Datetime startDt = convertToUserTimeZone(DateTime.parse(eventDate.format() + ' ' + tradingHour.VGA_Start_Time__c));
        Datetime endDt   = convertToUserTimeZone(DateTime.parse(eventDate.format() + ' ' + tradingHour.VGA_End_Time__c));
        
        while(startDt < endDt){
            timeSlotsList.add(startDt.format('hh:mm a', 'GMT'));
            startDt = startDt.addMinutes(30);
        }
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method is used to populate all the time slots for a dealer 
    *              that is a pilot. We will create 30 min intervals from the dealership
    *              open hour until the time it closes. We won't show the time slots when
    *              there is no contact available to receive test drive leads that is
    *              available at that time. List will be empty if there are no contacts
    *              available to receive test drive leads.
    * @param tradingHour Trading hour record for this particular dealership
    * @param timeSlotsList List where we will be populating the available time slots
    * @param eventDate Date when the dealership will take place
    * @param dealerAccountId Dealer account where the user is trying to book the test drive
    * @param dateString Date when the appointment will take place in a string format
    * @param subbrand of the vehicle that will be used in the test drive appointment
    */
    private static void populateDealerContactTimeSlotList(VGA_Trading_Hour__c tradingHour, List<String> timeSlotsList, 
                                                           Date eventDate, Id dealerAccountId, String subbrand, String dateString){
        
        Set<Id> contactIdSet = new Set<Id>();
        Set<Datetime> timeSlotSet = new Set<Datetime>();
    
        for(VGA_User_Profile__c userProfile : [SELECT Id, VGA_Account_Id__c, VGA_Contact__c
                                               FROM VGA_User_Profile__c
                                               WHERE VGA_Contact__r.AccountId =: dealerAccountId
                                               AND VGA_Sub_Brand__c =: subbrand
                                               AND VGA_Available_TestDrive__c = true
                                              ]){
            contactIdSet.add(userProfile.VGA_Contact__c);
        }

        Map<Id, User> userMap = new Map<Id, User>([SELECT Id, ContactId
                                                   FROM User
                                                   WHERE ContactId IN: contactIdSet]);

        if(contactIdSet.size()!=0){
            Map<Id, List<Event>> eventsPerContactIdMap = new Map<Id, List<Event>>();
            System.debug(' contactIdSet ' + contactIdSet);
            for(Event evt : [SELECT Id, Subject, WhoId, StartDateTime, EndDateTime, OwnerId
                            FROM Event
                            WHERE OwnerId IN: userMap.KeySet()
                            //AND DAY_ONLY(StartDateTime) =: eventDate
                            ]){
                Id contactId = userMap.get(evt.OwnerId).ContactId;
                if(eventsPerContactIdMap.containsKey(contactId)){
                    eventsPerContactIdMap.get(contactId).add(evt);
                } else {
                    List<Event> tempList = new List<Event>();
                    tempList.add(evt);
                    eventsPerContactIdMap.put(contactId, tempList);
                }                
            }

            Map<Id, VGA_Working_Hour__c> workingHoursMap = getContactWorkingHours(contactIdSet, subBrand, dateString);
            System.debug(' workingHoursMap ---> ' + workingHoursMap);
            if(workingHoursMap.size()!=0){
                for(Id contactId : contactIdSet){
                    if(workingHoursMap.containsKey(contactId)){
                        VGA_Working_Hour__c workHour = workingHoursMap.get(contactId);

                        Datetime startDt = convertToUserTimeZone(DateTime.parse(eventDate.format() + ' ' + workHour.VGA_Start_Time__c));
                        Datetime endDt   = convertToUserTimeZone(DateTime.parse(eventDate.format() + ' ' + workHour.VGA_End_Time__c));
                        while(startDt < endDt){
                            Boolean busySlot = false;
                            if(eventsPerContactIdMap.containsKey(contactId)){
                                List<Event> eventsList = eventsPerContactIdMap.get(contactId);
                            
                                for(Event evt : eventsList){
                                    Datetime endSlotDt = startDt.addMinutes(60);
                                    Datetime eventStartTime = convertDateTimeZone(evt.StartDateTime);
                                    Datetime eventEndTime   = convertDateTimeZone(evt.EndDateTime);

                                    System.debug(' StartDateTime ' + evt.StartDateTime);
                                    System.debug(' EndDateTime ' + evt.EndDateTime);

                                    System.debug(' StartDateTime ' + eventStartTime  + 'startDt ' + startDt);
                                    System.debug(' EndDateTime ' + eventEndTime + 'endSlotDt ' + endSlotDt);
                                    if((startDt >= eventStartTime && endSlotDt <= eventEndTime)
                                    || (startDt <= eventStartTime && endSlotDt >= eventEndTime)
                                    || (startDt > eventStartTime && startDt < eventEndTime)
                                    || (endSlotDt > eventStartTime && endSlotDt < eventEndTime)){
                                        busySlot = true;
                                        break;
                                    }
                                }
                                
                            }
                            
                            if(!busySlot){
                                //timeSlotSet.add(startDt.format('hh:mm a', 'GMT'));
                                timeSlotSet.add(startDt);
                            }
                            startDt = startDt.addMinutes(30);
                        }
                    }
                }

                List<Datetime> timeList = new List<Datetime>(timeSlotSet);
                timeList.sort();

                for(Datetime timeSlot : timeList){
                    timeSlotsList.add(timeSlot.format('hh:mm a', 'GMT'));
                }
            }
        }
        
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a Map of trading hours grouped by day name
    * @param tradingHourList List of trading hours
    * @return Map<String, VGA_Trading_Hour__c> Map of trading hours grouped by Day name
    */
    private static Map<String, VGA_Trading_Hour__c> getTradingHoursPerDay(List<VGA_Trading_Hour__c> tradingHourList){
        
        Map<String, VGA_Trading_Hour__c> tradingHourPerNameMap = new Map<String, VGA_Trading_Hour__c>();
        for(VGA_Trading_Hour__c tradHour: tradingHourList){
            if(!tradingHourPerNameMap.containsKey(tradHour.Name.trim().tolowercase())){
                tradingHourPerNameMap.put(tradHour.Name.trim().tolowercase(), tradHour);
            }
        }

        return tradingHourPerNameMap;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the current user timezone
    * @param dt Datetime that we are trying to convert
    * @return Datetime in the current user time zone
    */
    private static Datetime convertToUserTimeZone(Datetime dt){
        String userTimeZone = UserInfo.getTimeZone() + '';
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', userTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the dealer timezone
    * @param dt Datetime that we are trying to convert
    * @return Datetime in the dealer time zone
    */
    private static Datetime convertDateTimeZone(Datetime dt){
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', dealerTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a dealer account based on a dealer code
    * @param dealerCode That will use to find its related Account record
    * @return Account Dealer Account
    */
    private static Account getDealerAccount(String dealerCode){
        List<Account> relateDealer = [SELECT Id, VGA_Pilot_Dealer__c, VGA_Brand__c, VGA_Timezone__c
                                      FROM Account 
                                      WHERE VGA_Dealer_Code__c =: dealerCode];

        if(relateDealer.size()!= 0){
            return relateDealer[0];
        } 

        return null;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a Map of Working Hours grouped by
    *              contact Id.
    * @param contactIdSet Set of contacts Ids for which we need to find the related working hours
    * @param subBrand subbrand for which we need to find the working hours
    * @param dayOfWeek name of the day of the week for which we need to find the working hours
    * @return Map<Id, VGA_Working_Hour__c> Map of working hours group by Contact Id
    */
    private static Map<Id, VGA_Working_Hour__c> getContactWorkingHours(Set<Id> contactIdSet, String subBrand, String dayOfWeek){

        Map<Id, VGA_Working_Hour__c> workingHourPerContactId = new Map<Id, VGA_Working_Hour__c>();

        for(VGA_Working_Hour__c  workHour :[SELECT Id, VGA_Brand__c, VGA_Day__c, VGA_Dealer_Code__c, VGA_Contact_ID__c, VGA_End_Time__c,
                                                       VGA_Start_Time__c, VGA_Last_Assigned_Date_Time__c, VGA_Sub_Brand__c, VGA_Unique_Key__c, VGA_Working__c,
                                                       VGA_Available__c, VGA_Timezone__c, VGA_Start_Date_Date_Time__c, VGA_End_Time_Date_Time__c 
                                             FROM VGA_Working_Hour__c 
                                             WHERE VGA_Contact__c IN: contactIdSet 
                                             AND VGA_Working__c = true 
                                             AND VGA_Available_Test_Drive__c = true 
                                             AND VGA_Sub_Brand__c =: subBrand
                                             ORDER BY VGA_Last_Assigned_Date_Time__c asc NULLS FIRST]){
            if(workHour.VGA_Day__c.trim().tolowercase() == dayOfWeek.trim().tolowercase()
               && !workingHourPerContactId.containsKey(workHour.VGA_Contact_ID__c)){
                workingHourPerContactId.put(workHour.VGA_Contact_ID__c, workHour);
            }
            
        }
        
        return workingHourPerContactId;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method a list of trading hours for a dealer account and a particular 
    *              subbrand.
    * @param dealerAccountId Dealer Account Id for which we need to find the list of trading hours
    * @param subbrand subbrand for which we need to find the trading hours
    * @return List<VGA_Trading_Hour__c> List of trading hours
    */
    public static List<VGA_Trading_Hour__c> getListofTradingHours(Id dealerAccountId, String subbrand)
    {    
        List<VGA_Trading_Hour__c> tradingHourList = new List<VGA_Trading_Hour__c>(); 
        List<VGA_Dealership_Profile__c> objDealershipList =[SELECT Id,VGA_Brand__c,VGA_Dealership__c,Name,
                                                             VGA_Distribution_Method__c,VGA_Escalate_Minutes__c,
                                                                VGA_Nominated_User__c,VGA_Sub_Brand__c 
                                                            FROM VGA_Dealership_Profile__c 
                                                            WHERE VGA_Dealership__c =: dealerAccountId 
                                                            AND VGA_Sub_Brand__c =: subbrand]; 
        if(objDealershipList.size() != 0)
        {
            VGA_Dealership_Profile__c objDealership = objDealershipList[0];
            tradingHourList=[SELECT Id,VGA_Day__c,VGA_Dealership_Profile__c,VGA_End_Time__c,Name,
                                  VGA_Start_Time__c,VGA_Working__c 
                           FROM VGA_Trading_Hour__c 
                           WHERE VGA_Dealership_Profile__c=:objDealership.Id
                           AND VGA_Working__c = true]; // To get list of Trading Hoours related to Dealership profile.
            if(tradingHourList.size()>0)
            {
                return tradingHourList;
            }
        }
        
        return tradingHourList;
    }
}