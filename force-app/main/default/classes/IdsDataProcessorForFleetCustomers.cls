@RestResource(urlMapping='/idsdataprocessingforfleetcustomers')
global with sharing class IdsDataProcessorForFleetCustomers {

    @HttpPost   
    global static String processData(String seraializedAccountForVWFleet,String seraializedAccountForSkodaFleet,
    String serializedContactForVWFleet,String serializedContactForSkodaFleet) {
               
        Account sAccountForVWFleet,sAccountForSkodaFleet;
        Contact sContactForVWFleet,sContactForSkodaFleet;
                
        Database.UpsertResult upsertAccountForVWFleet;
        Database.UpsertResult upsertAccountForSkodaFleet;
        //Database.UpsertResult upsertContactForVWFleet;
        //Database.UpsertResult upsertContactForSkodaFleet;
        
        Database.SaveResult upsertContactForVWFleet;
        Database.SaveResult upsertContactForSkodaFleet;
        
        String sfResponse='';
        Boolean allSuccess=true;
        ApexResponseToMuleForFleet sfResponseToMule=new ApexResponseToMuleForFleet();
        sfResponseToMule.processFlag=1;
        Schema.SObjectField extID;
        Savepoint sp = Database.setSavepoint();
        try{
            
            if(seraializedAccountForVWFleet!=null){
                sAccountForVWFleet=(Account)JSON.deserialize(seraializedAccountForVWFleet,Account.class);
                //extID=Account.Fields.VGA_Fleet_External_ID__c;
                upsertAccountForVWFleet= Database.upsert(sAccountForVWFleet,true);
                System.debug('---upsertAccount:'+upsertAccountForVWFleet);
                
                if(serializedContactForVWFleet!=null && upsertAccountForVWFleet.isSuccess()){
                sContactForVWFleet=(Contact)JSON.deserialize(serializedContactForVWFleet,Contact.class);
                //extID=Contact.Fields.VGA_External_Id__c;
                if(sContactForVWFleet!= null && sContactForVWFleet.LastName!=null && sContactForVWFleet.LastName!='')
                {
                    sContactForVWFleet.AccountId=upsertAccountForVWFleet.getId();
                    //sContactForVWFleet.VGA_External_Id__c=upsertAccountForVWFleet.getId();
                    
                    //--- Bypassing Duplicate Rule ----//
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    
                    if(sContactForVWFleet.Id == null) {
                        upsertContactForVWFleet = Database.insert(sContactForVWFleet, dml);
                    }
                    else {
                        //upsertContactForVWFleet = Database.upsert(sContactForVWFleet,true);
                        upsertContactForVWFleet = Database.update(sContactForVWFleet, dml);
                    }
                    
                    
                    System.debug('---upsertContact:'+upsertContactForVWFleet);
                    }
                }
                else
                    allSuccess=false;
            }
            
            if(seraializedAccountForSkodaFleet!=null){
                sAccountForSkodaFleet=(Account)JSON.deserialize(seraializedAccountForSkodaFleet,Account.class);
                //extID=Account.Fields.VGA_Fleet_External_ID__c;
                upsertAccountForSkodaFleet= Database.upsert(sAccountForSkodaFleet,true);
                System.debug('---upsertAccount:'+upsertAccountForSkodaFleet);
                
                if(serializedContactForSkodaFleet!=null && upsertAccountForSkodaFleet.isSuccess()){
                    sContactForSkodaFleet=(Contact)JSON.deserialize(serializedContactForSkodaFleet,Contact.class);
                    //extID=Contact.Fields.VGA_External_Id__c;
                    if(sContactForSkodaFleet!= null && sContactForSkodaFleet.LastName!=null && sContactForSkodaFleet.LastName!=''){
                    sContactForSkodaFleet.AccountId=upsertAccountForSkodaFleet.getId();
                    //sContactForSkodaFleet.VGA_External_Id__c=upsertAccountForSkodaFleet.getId();
                    
                    //--- Bypassing Duplicate Rule ----//
                    Database.DMLOptions dml = new Database.DMLOptions();
                    dml.DuplicateRuleHeader.AllowSave = true; 
                    
                    if(sContactForSkodaFleet.Id == null) {
                        upsertContactForSkodaFleet = Database.insert(sContactForSkodaFleet, dml);
                    }
                    else {
                        //upsertContactForSkodaFleet = Database.upsert(sContactForSkodaFleet,true);
                        upsertContactForSkodaFleet = Database.update(sContactForSkodaFleet, dml);
                    }
                    
                    System.debug('---upsertContact:'+upsertContactForSkodaFleet);
                    }
                }
                else
                    allSuccess=false;
            }
            
            
            

            
            if(upsertAccountForVWFleet==null)
                sfResponseToMule.accountIdOrErrorForVWFleet='No action on Account';
            else if(upsertAccountForVWFleet.isSuccess()){
                    sfResponseToMule.accountIdOrErrorForVWFleet=upsertAccountForVWFleet.getId();
                }
            else{
                    sfResponseToMule.accountIdOrErrorForVWFleet=upsertAccountForVWFleet.getErrors()[0].getMessage();
                    allSuccess=false;
                }


            if(upsertContactForVWFleet==null)
                sfResponseToMule.contactIdOrErrorForVWFleet='No action on Contact';
            else if(upsertContactForVWFleet.isSuccess())
                sfResponseToMule.contactIdOrErrorForVWFleet=upsertContactForVWFleet.getId();
            else{
                    sfResponseToMule.contactIdOrErrorForVWFleet=upsertContactForVWFleet.getErrors()[0].getMessage();
                    allSuccess=false;
                }
            
            if(upsertAccountForSkodaFleet==null)
                sfResponseToMule.accountIdOrErrorForSkodaFleet='No action on Account';
            else if(upsertAccountForSkodaFleet.isSuccess()){
                    sfResponseToMule.accountIdOrErrorForSkodaFleet=upsertAccountForSkodaFleet.getId();
                }
            else{
                    sfResponseToMule.accountIdOrErrorForSkodaFleet=upsertAccountForSkodaFleet.getErrors()[0].getMessage();
                    allSuccess=false;
                }


            if(upsertContactForSkodaFleet==null)
                sfResponseToMule.contactIdOrErrorForSkodaFleet='No action on Contact';
            else if(upsertContactForSkodaFleet.isSuccess())
                sfResponseToMule.contactIdOrErrorForSkodaFleet=upsertContactForSkodaFleet.getId();
            else{
                    sfResponseToMule.contactIdOrErrorForSkodaFleet=upsertContactForSkodaFleet.getErrors()[0].getMessage();
                    allSuccess=false;
                }

            sfResponseToMule.isSystemException=false;
            if(!allSuccess){
                    Database.rollback(sp);
                    sfResponseToMule.processFlag=3;
                    sfResponseToMule.systemExceptionMessage='All records rolled back.';
                }
            else{
                sfResponseToMule.systemExceptionMessage='Success';
            }
            
            return JSON.serialize(sfResponseToMule);


        }
        catch (Exception e){
            System.debug('---'+e.getMessage());
            sfResponseToMule.processFlag=3;
            sfResponseToMule.isSystemException=true;
            sfResponseToMule.systemExceptionMessage=e.getMessage();
            Database.rollback(sp);
            return JSON.serialize(sfResponseToMule);
        }       
    }
    public class ApexResponseToMuleForFleet{
        public Integer processFlag{set;get;}
        public String accountIdOrErrorForVWFleet{set;get;}
        public String accountIdOrErrorForSkodaFleet{set;get;}
        public String contactIdOrErrorForVWFleet{set;get;}
        public String contactIdOrErrorForSkodaFleet{set;get;}
        public Boolean isSystemException{set;get;}
        public String systemExceptionMessage{set;get;}
    }
}