/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_WrapperofProductTracker 
{
	public static VGA_Campaign_Activity_Header__c objCampaignActivityHeader;
    static testMethod void myUnitTest() 
    {
        LoadData();
        
        Test.startTest();
	        VGA_WrapperofProduct obj = new VGA_WrapperofProduct();
			obj.CarConfiguratorDownloads=10;
	        obj.DemoCount=10;
	        obj.Display='test';
	        obj.Id = 'test';
	        obj.Internet=100;
	        obj.Newcount=9021;
	        obj.PhoneIns=12;
	        obj.productId='21';
	        obj.productName='obj';
	        obj.SearchImpressions = 100;
	        obj.Social = 'tedsa';
	        obj.TestDrives=123;
	        obj.TotalDemoCount =21;
	        obj.TotalInternet = 21;
	        obj.TotalNewcount=213;
	        obj.TotalPhoneIns=123;
	        obj.TotalTestDrives=123;
	        obj.TotalWalkIns=12313;
	        obj.WalkIns=123;
	        obj.WebTraffic=123;
	        
	        insert objCampaignActivityHeader;
	        obj.objCampaignActivity = objCampaignActivityHeader;
        Test.stopTest();
    }
    public static void LoadData()
    {
        objCampaignActivityHeader = VGA_CommonTracker.createCampaignActivityHeader();
    }
}