public class VGA_CalculateLeadDurationHours
{
    /**
    * @author Lino Diaz Alonso
    * @date 10/04/2019
    * @description Method being used to update the lead duration fields after
    *              the lead accepted date is updated. We will calculate the difference 
    *              of hours based on dealership trading hours excluding also the 
    *              public holidays that might be in between.
    * @param newLeadList List of new Leads that we are processing.
    */
    public static void updateLeadDurationField(List<Lead> newLeadList)
    {
        Map<Id, List<DateClass>> datesBetweenTwoDatesPerLeadIdMap = new Map<Id, List<DateClass>>(); 
        Map<Id, Set<String>> subBrandsPerDealershipMap = new Map<Id, Set<String>>();

        for(Lead objLead : newLeadList){
            //Get the dates between the assigned to dealership and accepted date per each lead
            if(!datesBetweenTwoDatesPerLeadIdMap.containsKey(objLead.Id)
               && objLead.VGA_Assigned_to_Dealership__c != null 
               && objLead.VGA_Lead_Accepted_Date__c != null
               && objLead.VGA_Dealer_Timezone__c != null){
                List<DateClass> datesBetweenList = getListOfDatesBetweenTwoDates(objLead.VGA_Assigned_to_Dealership__c, objLead.VGA_Lead_Accepted_Date__c, objLead.VGA_Dealer_Timezone__c);
                datesBetweenTwoDatesPerLeadIdMap.put(objLead.Id, datesBetweenList);
                
            }

            if(subBrandsPerDealershipMap.containsKey(objLead.VGA_Dealer_Account__c)){
                subBrandsPerDealershipMap.get(objLead.VGA_Dealer_Account__c).add(objLead.VGA_Sub_Brand__c);
            } else {
                Set<String> tempSet = new Set<String>();
                tempSet.add(objLead.VGA_Sub_Brand__c);
                subBrandsPerDealershipMap.put(objLead.VGA_Dealer_Account__c, tempSet);
            }
        }

        //Get maps of trading hours and public holidays that we will be using to calculate the duration
        Map<String, VGA_Trading_Hour__c> tradingHourPerKeyMap = gettradingHourPerKeyMap(subBrandsPerDealershipMap);
        Map<String,List<VGA_Public_Holiday__c>> publicHolidaysMap = getPublicHolidaysMap();
        
        calculateHoursDuration(newLeadList, datesBetweenTwoDatesPerLeadIdMap, publicHolidaysMap, tradingHourPerKeyMap);
    }
    
    /**
    * @author Lino Diaz Alonso
    * @date 10/04/2019
    * @description Method being used to calculate the lead duration in hours and in minutes.
    *              It will calculate the difference between the lead accepted date and the
    *              assigned to dealership date but only considering dealership 
    *              trading hours and working days.
    * @param newLeadList List of new Leads that we are processing.
    * @param datesBetweenTwoDatesPerLeadIdMap Map with all the days between two dates group by Lead Id
    * @param publicHolidaysMap Map of public holidays group by state region name
    * @param tradingHourPerKeyMap Trading hours per key map. 
    */
    private static void calculateHoursDuration(List<Lead> newLeadList, Map<Id, List<DateClass>> datesBetweenTwoDatesPerLeadIdMap,
                                          Map<String,List<VGA_Public_Holiday__c>> publicHolidaysMap,
                                          Map<String, VGA_Trading_Hour__c> tradingHourPerKeyMap)
    {
        Double duration = 0;
        for(Lead objLead : newLeadList){
            duration = 0;
            if(datesBetweenTwoDatesPerLeadIdMap.containsKey(objLead.Id)){
                //Iterate through the list of date to count how many hours should we consider per day
                for(DateClass dtClass : datesBetweenTwoDatesPerLeadIdMap.get(objLead.Id)){
                    Datetime dayStartDt = dtClass.startDate;
                    Datetime dayEndDt   = dtClass.endDate;
                    String dealerRegion = objLead.Dealership_Region__c != null ? objLead.Dealership_Region__c.tolowercase() : '';
                    
                    //Check if the date is a public holiday
                    if(isPublicHoliday(publicHolidaysMap, dayStartDt.dateGMT(), dealerRegion)){
                        continue;
                    }

                    //Get the key and check if there is a trading hour for that day
                    String dateString  = dayStartDt.format('EEEE').trim().tolowercase();
                    String key = String.valueOf(objLead.VGA_Dealer_Account__c).substring(0, 15) + dateString;
                    
                    if(tradingHourPerKeyMap.containsKey(key)){
                        VGA_Trading_Hour__c tradingHour = tradingHourPerKeyMap.get(key);
                        Datetime dealerStartDt = convertToUserTimeZone(DateTime.parse(dayStartDt.formatGmt('dd/MM/yyyy') + ' ' + tradingHour.VGA_Start_Time__c));
                        Datetime dealerEndDt   = convertToUserTimeZone(DateTime.parse(dayStartDt.formatGmt('dd/MM/yyyy') + ' ' + tradingHour.VGA_End_Time__c));
                        if(dayStartDt >= dealerEndDt){
                            continue;
                        } else {
                            dayStartDt = dayStartDt >= dealerStartDt ? dayStartDt : dealerStartDt;
                            dayEndDt   = dayEndDt   <= dealerEndDt   ? dayEndDt   : dealerEndDt;
                            
                            duration += getTimeDifference(dayStartDt, dayEndDt);
                        }
                    }

                }
            }
            //Update duration fields on the lead
            populateDurationFields(objLead, duration);
        }
    }

    /**
    * @author Lino Diaz Alonso
    * @date 03/05/2019
    * @description This Method populates the duration fields. If the duration is negative
    *              the duration fields will be 0
    * @param objLead Lead record that will be updated
    * @param duration Lead duration in minutes
    */
    private static void populateDurationFields(Lead objLead, Double duration)
    {
        if(duration < 0){
            objLead.VGA_Duration_Hours1__c   = 0;
            objLead.VGA_Duration_Minutes1__c = 0;
        } else {
            objLead.VGA_Duration_Hours1__c = duration/60;
            objLead.VGA_Duration_Minutes1__c = duration;
        }
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns the time difference between two dates in 
    *              minutes
    * @param startDt Starting date time
    * @param entDt End Date time
    * @return Double with the value of the difference between these two dates
    */
    private static Double getTimeDifference(Datetime startDt, Datetime endDt){
        Long dt1Long = startDt.getTime();
        Long dt2Long = endDt.getTime();
        Long milliseconds = dt2Long - dt1Long;
        Double seconds = milliseconds / 1000;
        Double minutes = seconds / 60;

        return minutes;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the user timezone
    * @param dt Datetime that we are trying to convert
    * @return Datetime in the current user time zone
    */
    private static Datetime convertToUserTimeZone(Datetime dt){
        String userTimeZone = UserInfo.getTimeZone() + '';
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', userTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a datetime converted to the dealer timezone
    * @param dt Datetime that we are trying to convert
    * @param dealerTimeZone String with the dealer time zone value
    * @return Datetime in the dealer time zone
    */
    private static Datetime convertDateTimeZone(Datetime dt, String dealerTimeZone){
        String dateNewTZ = dt.format('yyyy-MM-dd HH:mm:ss', dealerTimeZone);
        return Datetime.valueOfGmt(dateNewTZ);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a list with the dates that are between two dates
    *              each of them is one day
    * @param startDate Start date of the list
    * @param endDate End date of the list
    * @param dealerTimeZone String with the dealer time zone value
    * @return List<DateClass> List of date class. It has the start and end for each day
    */
    public static List<DateClass> getListOfDatesBetweenTwoDates(Datetime startDate, Datetime endDate, String dealerTimeZone){
        List<DateClass> dateClassList = new List<DateClass>();

        if(!startDate.isSameDay(endDate)){

            Datetime startDt = convertDateTimeZone(startDate, dealerTimeZone);
            Datetime endDt   = getEndOfDayDatetime(startDt);
            endDate = convertDateTimeZone(endDate, dealerTimeZone);

            while(startDt <= endDate
                  && endDt <= endDate){
                DateClass dtClass = new DateClass(startDt, endDt);
                dateClassList.add(dtClass);
                startDt = endDt.addSeconds(1);
                endDt = getEndOfDayDatetime(startDt);
            }
            DateClass dtClass = new DateClass(startDt, endDate);
            dateClassList.add(dtClass);
            
        } else {
            Datetime startDt = convertDateTimeZone(startDate, dealerTimeZone);
            Datetime endDt   = convertDateTimeZone(endDate, dealerTimeZone);

            DateClass dtClass = new DateClass(startDt, endDt);
            dateClassList.add(dtClass);
        }

        return dateClassList;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a date in its last minute of the day
    * @param dt Date for which we want to get the end of day value
    * @return List<DateClass> List of date class. It has the start and end for each day
    */
    private static Datetime getEndOfDayDatetime(Datetime dt){
        return Datetime.newInstanceGMT(dt.yearGmt(), dt.monthGmt(), dt.dayGmt(), 23, 59, 59);
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description Date class that we will use to store the start and end date for a 
    *              particular day
    */
    class DateClass{
        public Datetime startDate;
        public Datetime endDate;

        DateClass(Datetime startDate, Datetime endDate){
            this.startDate = startDate;
            this.endDate   = endDate;
        }

    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a Map of trading hours group by a key that is formed
    *              by the dealer id and the day when the trading hour is defined
    * @param subBrandsPerDealershipMap Map of sub brands that are available per dealership
    * @return Map<String, VGA_Trading_Hour__c> Map of trading hours group by key
    */
    public static Map<String, VGA_Trading_Hour__c> gettradingHourPerKeyMap(Map<Id, Set<String>> subBrandsPerDealershipMap)
    {    
        Set<Id> objDealershipIdSet = new Set<Id>(); 
        
        
        for(VGA_Dealership_Profile__c objDealership :[SELECT Id,VGA_Brand__c,VGA_Dealership__c,Name,
                                                             VGA_Distribution_Method__c,VGA_Escalate_Minutes__c,
                                                                VGA_Nominated_User__c,VGA_Sub_Brand__c 
                                                       FROM VGA_Dealership_Profile__c 
                                                       WHERE VGA_Dealership__c IN: subBrandsPerDealershipMap.KeySet()]){
            if(subBrandsPerDealershipMap.containsKey(objDealership.VGA_Dealership__c)
               && subBrandsPerDealershipMap.get(objDealership.VGA_Dealership__c).contains(objDealership.VGA_Sub_Brand__c)){
                   objDealershipIdSet.add(objDealership.Id);
            }
        } 

        Map<String, VGA_Trading_Hour__c> tradingHourPerKeyMap = new Map<String, VGA_Trading_Hour__c>();
        
        for(VGA_Trading_Hour__c tradHour: [SELECT Id,VGA_Day__c,VGA_Dealership_Profile__c,VGA_End_Time__c,Name,
                                                    VGA_Start_Time__c,VGA_Working__c, VGA_Dealer_Account__c
                                            FROM VGA_Trading_Hour__c 
                                            WHERE VGA_Dealership_Profile__c IN: objDealershipIdSet
                                            AND VGA_Working__c = true]){
            String key = tradHour.VGA_Dealer_Account__c + tradHour.Name.trim().tolowercase();
            if(!tradingHourPerKeyMap.containsKey(key)){
                tradingHourPerKeyMap.put(key, tradHour);
            }
        }
        
        return tradingHourPerKeyMap;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method returns a Map of public holidays group by its related
    *              state region. This will allow us to find the public holidays that
    *              are available for each specific dealership
    * @return Map<String,List<VGA_Public_Holiday__c>> List of public holidays group by State
    */
    private static Map<String,List<VGA_Public_Holiday__c>> getPublicHolidaysMap(){
        Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday = new Map<String,List<VGA_Public_Holiday__c>>(); 
                
        for(VGA_Public_Holiday__c objph : [SELECT Id, VGA_Holiday_Date__c, VGA_Holiday_Name__c, 
                                                  VGA_Holiday_Type__c, VGA_State_Region__c 
                                           FROM VGA_Public_Holiday__c])
        {
            if(!mapofPublicHoliday.containskey(objph.VGA_State_Region__c.tolowercase()))
                mapofPublicHoliday.put(objph.VGA_State_Region__c.tolowercase(),new List<VGA_Public_Holiday__c>());
            
            mapofPublicHoliday.get(objph.VGA_State_Region__c.tolowercase()).add(objph);
        }

        return mapofPublicHoliday;
    }

    /**
    * @author Lino Diaz Alonso
    * @date 08/03/2019
    * @description This Method that given a date returns true if the date is a holiday
    *              in a particular state and false if it is not.
    * @param mapofPublicHoliday Map of public holidays group by state
    * @param currentDay Day for which we will be checking if it is a public holiday or not
    * @param dealerState Dealer state where the holiday will take place
    * @return Boolean true if the date is a public holiday and false if it is not
    */
    private static Boolean isPublicHoliday(Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday, 
                                           Date currentDay, String dealerState){
        Boolean isPublicHoliday = false; 

        if(mapofPublicHoliday != null && !mapofPublicHoliday.isEmpty())
        {
            if(mapofPublicHoliday.get('all') != null 
               && mapofPublicHoliday.get('all').size() > 0)
            {
                for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get('all'))
                {
                    if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                        && objph.VGA_Holiday_Date__c.day() == currentDay.day() 
                        && objph.VGA_Holiday_Date__c.month() == currentDay.month())
                        {
                            isPublicHoliday = true;
                            break;
                        }
                    }
                    else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c == currentDay)
                        {
                            isPublicHoliday = true;
                            break; 
                        }
                    }
                }
            }
            if(mapofPublicHoliday.containskey(dealerState) 
               && mapofPublicHoliday.get(dealerState) != null)
            {
                for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get(dealerState))
                {
                    if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c.day() == currentDay.day() 
                           && objph.VGA_Holiday_Date__c.month() == currentDay.month())
                        {
                            isPublicHoliday = true;
                            break;
                        }
                    }
                    else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                    {
                        if(objph.VGA_Holiday_Date__c != null 
                           && objph.VGA_Holiday_Date__c == currentDay)
                        {
                            isPublicHoliday = true;
                            break; 
                        }
                    }
                }
            }
        }

        return isPublicHoliday;
    }
}