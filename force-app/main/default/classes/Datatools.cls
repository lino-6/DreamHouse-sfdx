public class Datatools {
    private static final String LONG_RUNNING_SERVICE_URL = 'https://Kleber.datatoolscloud.net.au/KleberWebService/DtKleberService.svc/ProcessQueryStringRequest';
    //private static final String DATATOOLS_KEY = 'RK-0FE03-02621-C50A7-9A14E-9D371-99EAA-2165D-3C692';
    private static final String DATATOOLS_KEY = 'RK-8A484-7D0AF-43235-F2C55-5ED41-96305-6CAE0-4C7D7';
    public String datatoolsTemporaryKeyLabel;
    public String DepartmentCode = 'Contact Us Form';
    public String datatoolsTemporaryKey {get; set;}
    public String datatoolsValidateEmailLabel;
    public String datatoolsEmailToValidate {get; set;}
    public Boolean datatoolsEmailValidationResult {get; set;}
    public String datatoolsValidatePhoneLabel;
    public String datatoolsPhoneToValidate {get; set;}
    public Boolean datatoolsPhoneValidationResult {get; set;}
    public String datatoolsAddressSearchLabel;
    public String datatoolsAddressToSearch {get; set;}
    public String datatoolsAddressSearchResult {get; set;}
    public String dataToolsGetAddressByIdLabel;
    public String datatoolsAddressToGet {get; set;}
    public String datatoolsGetAddressByIdResult {get; set;}
    // Constructor
    public Datatools(contactUsController contactUs) {}
    public Datatools(VGA_CarConfigurator CarConfigurator) {}
// Action method
    public Object datatoolsGetTemporaryKey() {
      // Create continuation with a timeout
      Continuation con = new Continuation(40);
      // Set callback method
      con.continuationMethod='processDatatoolsTemporaryKeyResponse';
      // Create callout request
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL + '?ExpirySeconds=600&Method='+EncodingUtil.urlEncode('DataTools.Security.GenerateTemporaryRequestKey', 'UTF-8')
                 +'&RequestKey='+EncodingUtil.urlEncode(DATATOOLS_KEY, 'UTF-8')+'&OutputFormat=json');
      req.setHeader('content-type', 'application/json; charset=utf-8');
      // Add callout request to continuation
      this.datatoolsTemporaryKeyLabel = con.addHttpRequest(req);
      // Return the continuation
      return con;
    }
    // Callback method
    public Object processDatatoolsTemporaryKeyResponse() {
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.datatoolsTemporaryKeyLabel);
      // Set the result variable that is displayed on the Visualforce page
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
      Map<String, Object> a = (Map<String, Object>)m.get('DtResponse');
      List<Object> a1 =  (List<Object>)a.get('Result');
      Map<String, Object> a2 = (Map<String, Object>)a1[0];
      this.datatoolsTemporaryKey = (String)a2.get('TemporaryRequestKey');
      // Return null to re-render the original Visualforce page
      return null;
    }
// Action method
    public Object datatoolsValidateEmail() {
      // Create continuation with a timeout
      Continuation con = new Continuation(40);
      // Set callback method
      con.continuationMethod='processDatatoolsEmailValidateResponse';
      system.debug('prassin datatools 1');
      // Create callout request
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL + '?ExpirySeconds=600&Method='+EncodingUtil.urlEncode('DataTools.Verify.Email.BriteVerify.VerifyEmail', 'UTF-8')
                 +'&RequestKey='+EncodingUtil.urlEncode(DATATOOLS_KEY, 'UTF-8')+'&OutputFormat=json&EmailAddress='+EncodingUtil.urlEncode(this.datatoolsEmailToValidate, 'UTF-8'));
      req.setHeader('content-type', 'application/json; charset=utf-8');
      // Add callout request to continuation
      this.datatoolsValidateEmailLabel= con.addHttpRequest(req);
      // Return the continuation
      return con;
    }
    public Object processDatatoolsEmailValidateResponse() {
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.datatoolsValidateEmailLabel);
      // Set the result variable that is displayed on the Visualforce page
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
      Map<String, Object> a = (Map<String, Object>)m.get('DtResponse');
      List<Object> a1 =  (List<Object>)a.get('Result');
      Map<String, Object> a2 = (Map<String, Object>)a1[0];
      Integer i = Integer.valueOf(a2.get('StatusCode'));
      if (i == 0 || i == 1 || i == 3) {
          this.datatoolsEmailValidationResult  = true;
      }
      else {
          this.datatoolsEmailValidationResult  = false;
      }
      return null;
    }
// Action method
    public Object datatoolsValidatePhone() {
      // Create continuation with a timeout
      Continuation con = new Continuation(40);
      // Set callback method
      con.continuationMethod='processDatatoolsPhoneValidateResponse';
      // Create callout request
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL + '?ExpirySeconds=600&Method='+EncodingUtil.urlEncode('DataTools.Verify.PhoneNumber.ReachTel.VerifyPhoneNumberIsConnected', 'UTF-8')
                 +'&RequestKey='+EncodingUtil.urlEncode(DATATOOLS_KEY, 'UTF-8')+'&OutputFormat=json&PhoneNumber='+EncodingUtil.urlEncode(this.datatoolsPhoneToValidate, 'UTF-8'));
      req.setHeader('content-type', 'application/json; charset=utf-8');
      // Add callout request to continuation
      this.datatoolsValidatePhoneLabel= con.addHttpRequest(req);
      System.Debug('@@@ias con is: ' +con);
      // Return the continuation
      return con;
    }
    public Object processDatatoolsPhoneValidateResponse() {
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.datatoolsValidatePhoneLabel);
      // Set the result variable that is displayed on the Visualforce page
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
      Map<String, Object> a = (Map<String, Object>)m.get('DtResponse');
      List<Object> a1 =  (List<Object>)a.get('Result');
      Map<String, Object> a2 = (Map<String, Object>)a1[0];
      Integer i = Integer.valueOf(a2.get('StatusCode'));
      if (i == 0) {
          this.datatoolsPhoneValidationResult  = true;
      }
      else {
          this.datatoolsPhoneValidationResult  = false;
      }
      return null;
    }
// Action method
    public Object datatoolsAddressSearch() {
      // Create continuation with a timeout
      Continuation con = new Continuation(40);
      // Set callback method
      con.continuationMethod='processDatatoolsAddressSearchResponse';
      // Create callout request
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL + '?ExpirySeconds=600&Method='+EncodingUtil.urlEncode('DataTools.Capture.Address.Predictive.AuPaf.SearchAddress', 'UTF-8')
                 +'&RequestKey='+EncodingUtil.urlEncode(DATATOOLS_KEY, 'UTF-8')+'&ResultLimit=10&OutputFormat=json&AddressLine='+EncodingUtil.urlEncode(this.datatoolsAddressToSearch, 'UTF-8'));
      req.setHeader('content-type', 'application/json; charset=utf-8');
      // Add callout request to continuation
      this.datatoolsAddressSearchLabel = con.addHttpRequest(req);
      // Return the continuation
      return con;
    }
    public Object processDatatoolsAddressSearchResponse() {
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.datatoolsAddressSearchLabel);
      // Set the result variable that is displayed on the Visualforce page
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
      Map<String, Object> a = (Map<String, Object>)m.get('DtResponse');
      this.datatoolsAddressSearchResult = JSON.serialize(a.get('Result'));
      return null;
    }
// Action method
    public Object datatoolsGetAddressById() {
      // Create continuation with a timeout
      Continuation con = new Continuation(40);
      // Set callback method
      con.continuationMethod='processDatatoolsGetAddressByIdResponse';
      // Create callout request
      HttpRequest req = new HttpRequest();
      req.setMethod('GET');
      req.setEndpoint(LONG_RUNNING_SERVICE_URL + '?ExpirySeconds=600&Method='+EncodingUtil.urlEncode('DataTools.Capture.Address.Predictive.AuPaf.RetrieveAddress', 'UTF-8')
                 +'&RequestKey='+EncodingUtil.urlEncode(DATATOOLS_KEY, 'UTF-8')+'&OutputFormat=json&RecordId='+EncodingUtil.urlEncode(this.datatoolsAddressToGet, 'UTF-8'));
      req.setHeader('content-type', 'application/json; charset=utf-8');
      // Add callout request to continuation
      this.dataToolsGetAddressByIdLabel = con.addHttpRequest(req);
      // Return the continuation
      return con;
    }
    public Object processDatatoolsGetAddressByIdResponse() {
      // Get the response by using the unique label
      HttpResponse response = Continuation.getResponse(this.dataToolsGetAddressByIdLabel);
      // Set the result variable that is displayed on the Visualforce page
      Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
      Map<String, Object> a = (Map<String, Object>)m.get('DtResponse');
      this.datatoolsGetAddressByIdResult = JSON.serialize(a.get('Result'));
      return null;
    }
}