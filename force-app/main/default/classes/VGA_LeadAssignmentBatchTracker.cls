//=================================================================================
// Tracker class for VGA_LeadAssignmentBatch class.
// =================================================================================
// Created by Nitisha Prasad on 22-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_LeadAssignmentBatchTracker
{
    public static VGA_LeadAssignmentBatch objBatch;
    public static Account objAccount;
	public static VGA_Trading_Hour__c objTradingHour;
	public static VGA_Dealership_Profile__c objDealershipProfile;
	public static Contact objContact;
    public static User objUser;
    public static VGA_User_Profile__c objUserPro;
	public static VGA_Working_Hour__c objWorking;   
	public static VGA_Subscription__c objSubscription;
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
       
       objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '1234';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;
       
        objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        
         objUser = VGA_CommonTracker.CreateUser(objContact.id);
         
         objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Nominated User';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        objDealershipProfile.VGA_Nominated_Contact__c = objContact.id;
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
         
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = objUserPro.id;
        objWorking.VGA_Working__c = true;
        objWorking.Name =  system.now().format('EEEE');
        insert objWorking; 
        
         objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'All New Leads';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
         
       VGA_Dealer_Notification__c objDN = new VGA_Dealer_Notification__c();
       objDN.VGA_Inserted_From_Batch__c = true;
       objDN.VGA_Notification_Sent__c = false;
       objDN.recordtypeId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
       
       insert objDN;
       
        Test.startTest();
        
        objBatch = new VGA_LeadAssignmentBatch();
        
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        Lead objLead = new Lead();
        objLead.Status = 'New';
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.LastName = 'Prasad';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Dealer_Code__c = '1234';
        objLead.VGA_Dealer_Account__c = objAcc.id;
        objLead.OwnerId = [select id from Group where Type= 'Queue' and Name = 'Dealership Queue'][0].id;
        insert objLead; 
        
        Database.executeBatch(objBatch);
        
        Test.stopTest();
    }
    static testMethod void myUnitTest2() 
    {
        // TO DO: implement unit test
        objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '1234';
        objAccount.VGA_Timezone__c = 'Australia/Sydney';
        objAccount.billingstate = 'ACT';
        update objAccount;
       
        objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        
         objUser = VGA_CommonTracker.CreateUser(objContact.id);
         
         objDealershipProfile  = new VGA_Dealership_Profile__c();
        objDealershipProfile.name = 'passenger vehicles (pv)';
        objDealershipProfile.VGA_Dealership__c = objAccount.id;
        objDealershipProfile.VGA_Brand__c  ='Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Nominated_User__c = objUSer.id;
        objDealershipProfile.VGA_Escalate_Minutes__c=20; 
        insert objDealershipProfile;
        
        objTradingHour = new VGA_Trading_Hour__c();
        objTradingHour.VGA_Working__c = true;
        objTradingHour.VGA_Dealership_Profile__c = objDealershipProfile.id;
        objTradingHour.Name = system.now().format('EEEE');
        objTradingHour.VGA_Start_Time__c = '01:00 AM';
        objTradingHour.VGA_End_Time__c = '11:59 PM';
        insert objTradingHour;
        
         
        objUserPro = VGA_CommonTracker.createUserProfile('test');
        objUserPro.VGA_Contact__c = objContact.id;
        objUserPro.VGA_Available__c = true;
        objUserPro.VGA_Brand__c = 'Volkswagen';
        objUserPro.VGA_Sub_Brand__c = 'passenger vehicles (pv)';
        insert objUserPro;
        
        objWorking = VGA_CommonTracker.createWorkingHour(system.now().format('EEEE'),'01:00 AM','11:00 PM');
        objWorking.VGA_User_Profile__c = objUserPro.id;
        objWorking.VGA_Working__c = true;
        objWorking.Name =  system.now().format('EEEE');
        insert objWorking; 
        
         objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = objUserPro.id;
        objSubscription.VGA_Subscription_Type__c = 'All New Leads';
        objSubscription.VGA_Delivery_Method__c = 'Email and SMS';
        insert objSubscription;
       
       
       
       VGA_Dealer_Notification__c objDN = new VGA_Dealer_Notification__c();
       objDN.VGA_Inserted_From_Batch__c = true;
       objDN.VGA_Notification_Sent__c = false;
       objDN.recordtypeId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
       
       insert objDN;
       
        Test.startTest();
        
        objBatch = new VGA_LeadAssignmentBatch();
        
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        Lead objLead = new Lead();
        objLead.Status = 'New';
        objLead.VGA_Brand__c = 'Volkswagen';
        objLead.LastName = 'Prasad';
        objLead.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        objLead.VGA_Dealer_Code__c = '1234';
        objLead.VGA_Dealer_Account__c = objAcc.id;
        objLead.OwnerId = [select id from Group where Type= 'Queue' and Name = 'Dealership Queue'][0].id;
        insert objLead; 
        
        Database.executeBatch(objBatch);
        
        Test.stopTest();
    }
    
     //===========================================================================================
    //Description:Test class for leadassignmentReschedule method.
    //===========================================================================================
    //Name:Lidiya Elizabeth        Date:02-11-2018   
    //===========================================================================================
     static testMethod void myUnitTest3() 
    {
         Test.startTest();
          
        VGA_scheduleLeadAssignment obj = new VGA_scheduleLeadAssignment();
        String CRON_EXP = '0 0 0 3 9 ? 2022';
        String jobId = System.schedule('LeadAssignmentJob', CRON_EXP, obj);
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));  
        Test.stopTest(); 
    }
    
    
}