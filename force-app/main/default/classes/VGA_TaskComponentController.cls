public without sharing class VGA_TaskComponentController 
{
    @AuraEnabled
    public static user getlogginguser()
    {
        return VGA_Common.getloggingAccountDetails();
    }
    @AuraEnabled
    public static List<Task> getTasksMethod(String filter,String sortField, boolean isAsc)
    {
        user objuser =VGA_Common.getloggingAccountDetails();
        string strLoggedInUserId = objuser.ContactId;
        String filterCondtn='';
        List<Task> lstTask=new list<Task>();
        string Administratorprofile;
        list<string>ProfileName=VGA_Common.getProfileName();
        string queryString;
        for(string objRole :ProfileName)
        {
            if(objRole.contains('Dealer Administrator'))
            {
               Administratorprofile=objRole;
               break;
            }
            else if(!objRole.contains('Dealer Administrator') && objRole.contains('Lead Controller') 
                     && !objRole.contains('Consultant') && !objRole.contains('Dealer Portal Group') && !objRole.contains('Dealer Portal')  )
            {
                Administratorprofile=objRole;
                break;
            }
            else if(!objRole.contains('Dealer Administrator') && !objRole.contains('Lead Controller') 
                     && objRole.contains('Consultant') && !objRole.contains('Dealer Portal Group') && !objRole.contains('Dealer Portal') )
            {
                Administratorprofile=objRole;
                break;
            }
            else if(objRole.contains('Dealer Portal Group') )
            {
                Administratorprofile=objRole;
                break;
            }
             else if(objRole.contains('Dealer Portal'))
            {
                Administratorprofile=objRole;
                break;
            }
            else
            {
                Administratorprofile='Consultant';
            }            
        }
         string strSubjects = label.VGA_Task_Subjects;
         set<String> setofSubjects = new set<String>();
         if(strSubjects != '')
         {
            for(String str : strSubjects.split(','))
            {
                setofSubjects.add(str.trim()); 
            }   
         }
         queryString = 'SELECT Id, IsClosed,ActivityDate,Status,Subject,Who.Name,WhoId,WhatId,What.Name';
         queryString += ' from Task';
        if(Administratorprofile !='Dealer Portal Group')
        {
            if(filter=='Overdue')
            {
                queryString += ' where ActivityDate<TODAY';
                queryString += ' and Status =\'Open\'';
                queryString += ' and subject Not in:setofSubjects';
                queryString += ' and WHOId =:strLoggedInUserId'; 
             }
            if(filter=='Today')
            {
                queryString += ' where ActivityDate=TODAY';
                queryString += ' and WHOId =:strLoggedInUserId';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Today + Overdue')
            {
                queryString += ' where (ActivityDate=TODAY OR (ActivityDate<TODAY))';
                queryString += ' and subject Not in:setofSubjects';
                queryString += ' and Status =\'Open\'';
                queryString += ' and WHOId =:strLoggedInUserId';
            }
            if(filter=='Tomorrow')
            {
                queryString += ' where ActivityDate=TOMORROW and WHOId =:strLoggedInUserId '; 
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Next 7 Days')
            {
                queryString += ' where ActivityDate=NEXT_N_DAYS:7 and WHOId =:strLoggedInUserId '; 
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Next 7 Days + Overdue')
            {
                queryString += ' where (ActivityDate=NEXT_N_DAYS:7 OR (ActivityDate<TODAY))';
                queryString += ' and Status =\'Open\'';
                queryString += ' and WHOId =:strLoggedInUserId';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='This Month')
            {
                queryString += ' where ActivityDate=THIS_MONTH ';
                queryString += ' and WHOId =:strLoggedInUserId';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='All Open')
            {
                queryString += ' where Status =\'Open\'';
                queryString += ' and WHOId =:strLoggedInUserId'; 
                queryString += ' and subject Not in:setofSubjects';
            } 
            if(filter=='Closed Tasks this Week')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:strLoggedInUserId'; 
                queryString += ' and ActivityDate = THIS_WEEK';  
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Closed Tasks this Month')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:strLoggedInUserId'; 
                queryString += ' and ActivityDate = THIS_MONTH';  
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Closed Tasks this Quarter')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:strLoggedInUserId'; 
                queryString += ' and ActivityDate = THIS_QUARTER';  
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='All Closed Tasks')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:strLoggedInUserId'; 
                queryString += ' and subject Not in:setofSubjects';
            }   
        }
        else
        {
            string strAccountId =objuser.VGA_Account_Id__c;
            set<string>setofIds =new set<string>();
            list<Contact>listcontact =new list <Contact>([select Id from Contact where AccountId=:strAccountId limit 49999]);
            if(listcontact !=Null && listcontact.size()>0)
            {
                for(Contact ObjContact :listcontact)
                {
                  setofIds.add(ObjContact.Id); 
                }
             }
           if(setofIds !=Null )
           {
            if(filter=='Overdue')
            {
                queryString += ' where ActivityDate<TODAY';
                queryString += ' and Status =\'Open\'';
                queryString += ' and WHOId =:setofIds'; 
                queryString += ' and subject Not in:setofSubjects';
               
            }
            if(filter=='Today')
            {
                queryString += ' where ActivityDate=TODAY';
                queryString += ' and WHOId =:setofIds';  
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Today + Overdue')
            {
                queryString += ' where (ActivityDate=TODAY OR (ActivityDate<TODAY))';
                queryString += ' and Status =\'Open\'';
                queryString += ' and WHOId =:setofIds';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Tomorrow')
            {
                queryString += ' where ActivityDate=TOMORROW and WHOId =:setofIds'; 
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Next 7 Days')
            {
                queryString += ' where ActivityDate=NEXT_N_DAYS:7 and WHOId =:setofIds '; 
                queryString += ' and subject Not in:setofSubjects';
                
            }
            if(filter=='Next 7 Days + Overdue')
            {
                queryString += ' where (ActivityDate=NEXT_N_DAYS:7 OR (ActivityDate<TODAY))';
                queryString += ' and Status =\'Open\'';
                queryString += ' and WHOId =:setofIds';
                queryString += ' and subject Not in:setofSubjects';

            }
            if(filter=='This Month')
            {
                 queryString += ' where ActivityDate=THIS_MONTH ';
                 queryString += ' and WHOId =:setofIds';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='All Open')
            {
                queryString += ' where Status =\'Open\'';
                queryString += ' and WHOId =:setofIds'; 
                queryString += ' and subject Not in:setofSubjects';
            } 
            if(filter=='Closed Tasks this Week')
            {
                 queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                 queryString += ' and WHOId =:setofIds'; 
                 queryString += ' and ActivityDate = THIS_WEEK';
                 queryString += ' and subject Not in:setofSubjects';
               
            }
            if(filter=='Closed Tasks this Month')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:setofIds'; 
                queryString += ' and ActivityDate = THIS_MONTH';
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='Closed Tasks this Quarter')
            {
                 queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                 queryString += ' and WHOId =:setofIds'; 
                 queryString += ' and ActivityDate = THIS_QUARTER'; 
                queryString += ' and subject Not in:setofSubjects';
            }
            if(filter=='All Closed Tasks')
            {
                queryString += ' where (Status=\'Dealer Completed\' OR Status=\'Closed\')';
                queryString += ' and WHOId =:setofIds'; 
                queryString += ' and subject Not in:setofSubjects';
            }  
           }
        }
        if (sortField != '') 
         {
            queryString += ' order by ' + sortField;
             if (isAsc) 
              {
                    queryString += ' asc';
              } 
              else 
              {
                   queryString += ' desc';
              }
         }
         queryString += ' LIMIT 49999';
         system.debug('queryString================'+queryString);
         lstTask = Database.query(queryString);
        
        return lstTask;
    }
    @AuraEnabled
    public static Task gettaskDetails(string RecordId)
    {
      Task objTask =[select Id,WhoId,Who.Name,Status,Owner.Name,Topic_Opportunity__c,VGA_Dealer_Comments__c,Topic__c,VGA_Dealer_Completed_Date__c,
                     Subject,Priority,Description,Meeting_PDF_Link__c,ActivityDate from Task where Id=:RecordId limit 1];
      return objTask;        
    }
     @AuraEnabled
    public static list<ContentVersion> getAttachmentlist(string RecordId)
    {
      
         list<ContentVersion>originalAttachment =new list<ContentVersion>();
         List<ContentDocumentLink> documentLinks = [SELECT ContentDocumentId 
                                          FROM ContentDocumentLink 
                                          WHERE LinkedEntityId = :RecordId limit 999];
         System.debug('documentLinks: ' + documentLinks);
         Set<Id> documentIds = new Set<Id>();

        for(ContentDocumentLink dl : documentLinks) 
        {
            documentIds.add(dl.ContentDocumentId);
        }
         return [SELECT Id, Title, ContentUrl, Description, FileType, ContentDocumentId, ContentModifiedDate 
             FROM ContentVersion 
             WHERE ContentDocumentId IN :documentIds];   
    } 
    @AuraEnabled
    public static string updatetaskDetails(Task objTask)
    {
        try
        {
          Update objTask ;
          return 'Task saved successfully';
        }
           catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            } 
    }
    @AuraEnabled
    public static string updateTaskstatus(string TaskId)
    {
        try
        {
             Task objTask=new Task();
             objTask.Status ='Dealer Completed';
             objTask.VGA_Dealer_Completed_Date__c=system.today();
             objTask.Id =TaskId;
             Update objTask ;
           
             return 'Task saved successfully';
        }
           catch(exception e)
            {
                return 'Error: ' + e.getMessage();
            }
    }
    @AuraEnabled
    public static VGA_WrapperofImage returnBlobValue(string attachmentIds)
    {
       ContentVersion objContentVersion= [SELECT Id, Title, ContentUrl, Description,VersionData,FileType, 
                                           ContentDocumentId, ContentModifiedDate  FROM ContentVersion 
             WHERE Id =:attachmentIds limit 1];  
        system.debug('objContentVersion================'+objContentVersion);
       
            VGA_WrapperofImage objWrapper =New VGA_WrapperofImage();
            objWrapper.fileType=objContentVersion.FileType;
            objWrapper.Name=objContentVersion.Title;
            objWrapper.encodeValue=Encodingutil.base64encode(objContentVersion.VersionData);
        return objWrapper;
     
    }
    
}