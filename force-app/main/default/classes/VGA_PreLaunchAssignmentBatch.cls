global class VGA_PreLaunchAssignmentBatch implements Database.Batchable<sObject> ,Database.stateful
{
    global final string strPreLaunchLeadQuery;
    global VGA_Pre_Launch_Configuration__c objplc;
    
    global VGA_PreLaunchAssignmentBatch(string recordId)
    {
        strPreLaunchLeadQuery = 'select Id, VGA_Tracking_Id__c, VGA_Number_of_Leads_per_Distribution__c, VGA_Time_Interval__c from VGA_Pre_Launch_Configuration__c where Id = \'' + recordId + '\''; 
    }
    
    global Database.querylocator start(Database.BatchableContext bc)  
    {
        return Database.getQueryLocator(strPreLaunchLeadQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<VGA_Pre_Launch_Configuration__c> lstPLC)
    {
        try
        {
            if(lstPLC != Null && !lstPLC.isEmpty())
            {
                objplc = lstPLC[0];
            }
            
            if(objplc != Null && objplc.VGA_Tracking_Id__c != Null && objplc.VGA_Number_of_Leads_per_Distribution__c > 0)
            {
                String strquery = 'select Id, VGA_Request_Type__c, VGA_is_pre_launched_Vehicle__c, RecordTypeId, VGA_Brand__c, Firstname, Lastname, Email, VGA_Profanity__c, ownerID, VGA_Follow_up_Lead__c, VGA_Duplicate_Key__c, VGA_Spam__c, VGA_Send_Notification_Email__c, VGA_Escalated_Date_Time__c, VGA_Customer_Account__c, VGA_Duplicate_Customer_Score__c, VGA_Duplicate_Customer_Id__c, VGA_Assignment_Done__c, VGA_Dealer_Account__c, VGA_Assigned_to_Dealership__c, Postalcode, VGA_Dealer_Code__c, VGA_Sub_Brand__c from Lead where VGA_Assignment_Done__c = false AND VGA_Profanity__c = false AND VGA_Spam__c = false AND VGA_Follow_up_Lead__c = true AND VGA_Tracking_ID__c = \'' + objplc.VGA_Tracking_Id__c + '\' limit ' + integer.valueof(objplc.VGA_Number_of_Leads_per_Distribution__c);    
                List<Lead> lstLeads = Database.query(strquery);
                
                if(lstLeads != Null && !lstLeads.isEmpty())
                {
                    Set<String> setofKey = new Set<String>();
                    Set<string> setofUniqueKey = new Set<string>();  
                    Set<string> setofUniquekey2 = new Set<string>();
                    Set<String> setofUniqueKey3 = new Set<String>();
                    
                    Map<String,VGA_Trading_Hour__c> mapofTH = new map<string,VGA_Trading_Hour__c>();    
                    Map<string,VGA_Dealership_Profile__c> mapofDistributionMethod = new Map<string,VGA_Dealership_Profile__c>();
                    Map<string,list<VGA_Working_Hour__c>> mapofWH = new Map<string,list<VGA_Working_Hour__c>>();
                    Map<id,User> mapOfUser = new Map<id,User>();
                    Map<Id,User> mapofUserIdwrtaccountid = new Map<Id,User>();
                    Map<Id,Contact> mapContact = new Map<Id,Contact>();
                    Set<String> setDealerCode = new Set<String>();
                    Map<String,String> mapofDealerCodeWRTBillingState = new Map<String,String>();
                    Map<String,List<VGA_Public_Holiday__c>> mapofPublicHoliday = new Map<String,List<VGA_Public_Holiday__c>>();
                    List<Lead> lstUpdateLead = new List<Lead>();
                    
                    for(VGA_Public_Holiday__c objph : [select Id, VGA_Holiday_Date__c, VGA_Holiday_Name__c, VGA_Holiday_Type__c, VGA_State_Region__c from VGA_Public_Holiday__c limit 49999])
                    {
                        if(!mapofPublicHoliday.containskey(objph.VGA_State_Region__c))
                            mapofPublicHoliday.put(objph.VGA_State_Region__c,new List<VGA_Public_Holiday__c>());
                        
                        mapofPublicHoliday.get(objph.VGA_State_Region__c).add(objph);
                    }
                    
                    for(User objUser : [select id, ContactId, AccountID from User where ContactId != Null AND isActive = true limit 49999])       
                    {    
                        mapOfUser.put(objUser.contactId,objUser);   
                        mapofUserIdwrtaccountid.put(objUser.id,objUser);      
                    }
            
                    for(Lead objLead : lstLeads)
                    {
                        if(objLead.VGA_Dealer_Code__c != null && objLead.VGA_Brand__c != null && objLead.VGA_Sub_Brand__c != null)
                        {                   
                            string strkey = '';
                            
                            strkey = objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.now().format('EEEE');
                            setofUniqueKey.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase());    
                            setofUniquekey2.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                            setofUniqueKey3.add((objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c).trim().tolowercase());
                            setDealerCode.add(objLead.VGA_Dealer_Code__c);  
                            
                            if(strkey != '')
                            {
                                setofKey.add(strkey.trim().tolowercase());
                            }
                        }
                    }
                    
                    if(setDealerCode != null && !setDealerCode.isEmpty())
                    {
                        for(Account objAccount : [select Id, BillingState, VGA_Dealer_Code__c from Account where VGA_Dealer_Code__c IN : setDealerCode AND BillingState != null])
                        {
                            mapofDealerCodeWRTBillingState.put(objAccount.VGA_Dealer_Code__c,objAccount.BillingState);
                        }
                    }
                    
                    if(setofKey != null && !setofKey.isEmpty())
                    {
                        for(VGA_Trading_Hour__c objTH : [select Id, VGA_Brand__c, VGA_Day__c, VGA_Unique_Key__c, VGA_Dealer_Code__c, VGA_End_Time__c, VGA_Start_Time__c, VGA_Sub_Brand__c,
                                                         VGA_Start_Date_Date_Time__c, VGA_End_Time_Date_Time__c, VGA_Timezone__c, VGA_Working__c from VGA_Trading_Hour__c where VGA_Unique_Key__c IN : setofKey AND VGA_Working__c = true])
                        {
                            mapofTH.put(objTH.VGA_Unique_Key__c.trim().toLowerCase(),objTH);
                        }
                    }
                    
                    if(setofUniquekey2 != null && !setofUniquekey2.isEmpty())
                    {
                        for(VGA_Dealership_Profile__c objDP : [select id, VGA_Brand__c, VGA_Escalate_Minutes__c, VGA_Nominated_User__c, VGA_Distribution_Method__c, VGA_Unique_Key_2__c, Name from VGA_Dealership_Profile__c
                                                               where VGA_Unique_Key_2__c IN : setofUniquekey2])
                        {
                            if(!mapofDistributionMethod.containskey(objDP.VGA_Unique_Key_2__c))
                                mapofDistributionMethod.put(objDP.VGA_Unique_Key_2__c.trim().tolowercase(),objDP);    
                        }
                    }
                    
                    if(setofUniqueKey != null && !setofUniqueKey.isEmpty())
                    {
                        for(VGA_Working_Hour__c objWH :  [select Id, VGA_Brand__c, VGA_Day__c, VGA_Dealer_Code__c, VGA_Contact_ID__c, VGA_End_Time__c,
                                                          VGA_Start_Time__c, VGA_Last_Assigned_Date_Time__c, VGA_Sub_Brand__c, VGA_Unique_Key__c, VGA_Working__c,
                                                          VGA_Available__c, VGA_Timezone__c, VGA_Start_Date_Date_Time__c, VGA_End_Time_Date_Time__c from VGA_Working_Hour__c 
                                                          where VGA_Unique_Key__c IN : setofUniqueKey and VGA_Working__c = true 
                                                          AND VGA_Available__c = true order by VGA_Last_Assigned_Date_Time__c asc NULLS FIRST])
                        {
                            if(!mapofWH.containskey(objWH.VGA_Unique_Key__c))
                                mapofWH.put(objWH.VGA_Unique_Key__c.trim().tolowercase(),new List<VGA_Working_Hour__c>());
                            
                            mapofWH.get(objWH.VGA_Unique_Key__c.trim().tolowercase()).add(objWH);
                        }
                    }
                    
                    for(Lead objLead : lstLeads)
                    {
                        if(objLead.VGA_Dealer_Code__c != null && objLead.VGA_Brand__c != null && objLead.VGA_Sub_Brand__c != null)
                        {                   
                            string strkey = '';
                                    
                            strkey = objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE');
                            
                            if(strkey != '')
                            {
                                strkey = strkey.trim().tolowercase();
                                string strTimeZone = '';
                                        
                                if(mapofTH.containsKey(strkey) && mapofTH.get(strkey) != Null)
                                {  
                                    strTimeZone = mapofTH.get(strkey).VGA_Timezone__c;
                                      
                                    if(strTimeZone != '')
                                    {
                                        Datetime dtStartDate = mapofTH.get(strkey).VGA_Start_Date_Date_Time__c;
                                        Datetime dtEndDate = mapofTH.get(strkey).VGA_End_Time_Date_Time__c;
                                        string strCreatedDate = system.now().format('yyyy-MM-dd HH:mm:ss',strTimeZone);
                                       
                                        list<string> lstSplitDateandTime = strCreatedDate.split(' ');
                                        list<string> lstSplitDateValue = lstSplitDateandTime[0].split('-');
                                        list<string> lstsplitTimeValue = lstSplitDateandTime[1].split(':');
                                        
                                        Datetime dtCreatedDate = datetime.newinstancegmt(integer.valueof(lstSplitDateValue[0]),integer.valueof(lstSplitDateValue[1]),integer.valueof(lstSplitDateValue[2]),integer.valueof(lstsplitTimeValue[0]),integer.valueof(lstsplitTimeValue[1]),integer.valueof(lstsplitTimeValue[2]));
                                        
                                        if(test.isRunningTest())
                                        {
                                            dtCreatedDate = dtStartDate.addHours(1);
                                        }
                                        
                                        if(dtCreatedDate >= dtStartDate && dtCreatedDate <= dtEndDate)
                                        {
                                            String strkey1 = '';
                                            String strkey2 = '';  
                                            
                                            strkey1  = (objLead.VGA_Dealer_Code__c + objLead.VGA_Brand__c + objLead.VGA_Sub_Brand__c + System.Now().format('EEEE')).trim().tolowercase(); 
                                            strkey2 = (objLead.VGA_Dealer_Code__c + objLead.VGA_Sub_Brand__c).trim().tolowercase();
                                            
                                            Boolean isPublicHoliday = false;
    
                                            if(mapofDealerCodeWRTBillingState != null && mapofDealerCodeWRTBillingState.containskey(objLead.VGA_Dealer_Code__c) && mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c) != null)
                                            {
                                                if(mapofPublicHoliday != null && !mapofPublicHoliday.isEmpty())
                                                {
                                                    if(mapofPublicHoliday.containsKey('All') && mapofPublicHoliday.get('All') != Null)
                                                    {
                                                        for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get('All'))
                                                        {
                                                            if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                                                            {
                                                                if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c.day() == system.today().day() && objph.VGA_Holiday_Date__c.month() == system.today().month())
                                                                {
                                                                    isPublicHoliday = true;
                                                                    break;
                                                                }
                                                            }
                                                            else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                                                            {
                                                                if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c == system.today())
                                                                {
                                                                    isPublicHoliday = true;
                                                                    break; 
                                                                }
                                                            }
                                                        }
                                                    }
                                                    
                                                    if(mapofPublicHoliday.containskey(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)) && mapofPublicHoliday.get(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)) != null)
                                                    {
                                                        for(VGA_Public_Holiday__c objph : mapofPublicHoliday.get(mapofDealerCodeWRTBillingState.get(objLead.VGA_Dealer_Code__c)))
                                                        {
                                                            if(objph.VGA_Holiday_Type__c.equalsignoreCase('Recurring'))
                                                            {
                                                                if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c.day() == system.today().day() && objph.VGA_Holiday_Date__c.month() == system.today().month())
                                                                {
                                                                    isPublicHoliday = true;
                                                                    break;
                                                                }
                                                            }
                                                            else if(objph.VGA_Holiday_Type__c.equalsignoreCase('Single'))
                                                            {
                                                                if(objph.VGA_Holiday_Date__c != null && objph.VGA_Holiday_Date__c == system.today())
                                                                {
                                                                    isPublicHoliday = true;
                                                                    break; 
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            
                                            if(!isPublicHoliday)
                                            {
                                                if(mapofDistributionMethod != null && !mapofDistributionMethod.isEmpty() && mapofDistributionMethod.containskey(strkey2))
                                                {
                                                    if(mapofDistributionMethod.get(strkey2).VGA_Distribution_Method__c.equalsignoreCase('Nominated User'))
                                                    {
                                                        if(mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c != null && mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c).AccountId != null)
                                                        {
                                                            if(mapofWH != null && mapofWH.containskey(strkey1) && !mapofWH.get(strkey1).isEmpty())
                                                            {
                                                                String ContactId = String.valueof(mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c).contactId).substring(0,15);
                                                                
                                                                for(VGA_Working_Hour__c obj : mapofWH.get(strkey1))
                                                                {
                                                                    if(obj.VGA_Contact_ID__c != Null && String.valueof(obj.VGA_Contact_ID__c).substring(0,15) == ContactId)
                                                                    {
                                                                        String Timezone = obj.VGA_Timezone__c;
                                                                
                                                                        Datetime dtStartDate1 = obj.VGA_Start_Date_Date_Time__c;  
                                                                        Datetime dtEndDate1 = obj.VGA_End_Time_Date_Time__c;
                                                                        String strCreatedDate1 = system.now().format('yyyy-MM-dd HH:mm:ss',Timezone);
                                                                        
                                                                        List<string> lstSplitDateandTime1 = strCreatedDate1.split(' ');
                                                                        List<string> lstSplitDateValue1 = lstSplitDateandTime1[0].split('-');
                                                                        List<string> lstsplitTimeValue1 = lstSplitDateandTime1[1].split(':');
                                                                    
                                                                        Datetime dtCreatedDate1 = datetime.newinstancegmt(integer.valueof(lstSplitDateValue1[0]),integer.valueof(lstSplitDateValue1[1]),integer.valueof(lstSplitDateValue1[2]),integer.valueof(lstsplitTimeValue1[0]),integer.valueof(lstsplitTimeValue1[1]),integer.valueof(lstsplitTimeValue1[2]));
                                                                        
                                                                        if(test.isRunningTest())
                                                                        {
                                                                            dtCreatedDate1 = dtStartDate1.addHours(1);
                                                                        }
                                                                        
                                                                        if(dtCreatedDate1 >= dtStartDate1 && dtCreatedDate1 <= dtEndDate1)
                                                                        {
                                                                            objLead.ownerID = mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c;  
                                                                            objLead.VGA_Assignment_Done__c = true;  
                                                                            objLead.VGA_Assigned_to_Dealership__c = system.now(); 
                                                                            objLead.VGA_Dealer_Account__c = mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c).AccountId;
                                                            
                                                                            if(integer.valueof(mapofDistributionMethod.get(strkey2).VGA_Escalate_Minutes__c) > 0)         
                                                                            {      
                                                                                objLead.VGA_Escalated_Date_Time__c = system.now().addminutes(integer.valueof(mapofDistributionMethod.get(strkey2).VGA_Escalate_Minutes__c));        
                                                                            }
                                                                            lstUpdateLead.add(objLead);
                                                                            
                                                                            Contact objcon = new Contact(id = mapofUserIdwrtaccountid.get(mapofDistributionMethod.get(strkey2).VGA_Nominated_User__c).ContactId);
                                                                            objcon.VGA_Last_Assigned_Date_Time__c = system.now();
                                                                            mapContact.put(objcon.Id, objcon);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if(mapofDistributionMethod.get(strkey2).VGA_Distribution_Method__c.equalsignoreCase('Round Robin'))
                                                    {
                                                        if(mapofWH != null && mapofWH.containskey(strkey1) && !mapofWH.get(strkey1).isEmpty())
                                                        {
                                                            for(VGA_Working_Hour__c obj : mapofWH.get(strkey1))
                                                            {
                                                                String Timezone = obj.VGA_Timezone__c;
                                                                
                                                                Datetime dtStartDate1 = obj.VGA_Start_Date_Date_Time__c;  
                                                                Datetime dtEndDate1 = obj.VGA_End_Time_Date_Time__c;
                                                                String strCreatedDate1 = system.now().format('yyyy-MM-dd HH:mm:ss',Timezone);
                                                                
                                                                List<string> lstSplitDateandTime1 = strCreatedDate1.split(' ');
                                                                List<string> lstSplitDateValue1 = lstSplitDateandTime1[0].split('-');
                                                                List<string> lstsplitTimeValue1 = lstSplitDateandTime1[1].split(':');
                                                            
                                                                Datetime dtCreatedDate1 = datetime.newinstancegmt(integer.valueof(lstSplitDateValue1[0]),integer.valueof(lstSplitDateValue1[1]),integer.valueof(lstSplitDateValue1[2]),integer.valueof(lstsplitTimeValue1[0]),integer.valueof(lstsplitTimeValue1[1]),integer.valueof(lstsplitTimeValue1[2]));
                                                                
                                                                if(test.isRunningTest())
                                                                {
                                                                    dtCreatedDate1 = dtStartDate1.addHours(1);
                                                                }
                                                                
                                                                if(dtCreatedDate1 >= dtStartDate1 && dtCreatedDate1 <= dtEndDate1)
                                                                {
                                                                    if(mapOfUser.containskey(obj.VGA_Contact_ID__c) && mapOfUser.get(obj.VGA_Contact_ID__c).Id != null && mapOfUser.get(obj.VGA_Contact_ID__c).accountid != null)
                                                                    {
                                                                        objLead.ownerID = mapOfUser.get(obj.VGA_Contact_ID__c).Id;
                                                                        objLead.VGA_Assignment_Done__c = true;
                                                                        objLead.VGA_Dealer_Account__c = mapOfUser.get(obj.VGA_Contact_ID__c).AccountId;
                                                                        objLead.VGA_Assigned_to_Dealership__c = system.now();
                                                                        
                                                                        if(integer.valueof(mapofDistributionMethod.get(strkey2).VGA_Escalate_Minutes__c) > 0)  
                                                                        {
                                                                            objLead.VGA_Escalated_Date_Time__c = system.now().addminutes(integer.valueof(mapofDistributionMethod.get(strkey2).VGA_Escalate_Minutes__c));
                                                                        }
                                                                        lstUpdateLead.add(objLead);
                                                                        
                                                                        Contact objcon = new Contact(id = obj.VGA_Contact_ID__c);
                                                                        objcon.VGA_Last_Assigned_Date_Time__c = system.now();
                                                                        mapContact.put(objcon.Id, objcon);
                                                                        
                                                                        break;
                                                                    } 
                                                                }
                                                            }
                                                        }
                                                    }
                                                }      
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if(lstUpdateLead != null && !lstUpdateLead.isEmpty())
                    {
                        update lstUpdateLead;
                        
                        VGA_LeadTriggerHandler.sendNotification(lstUpdateLead, true);
                    }
                    
                    if(mapContact != null && !mapContact.isEmpty())
                    {
                        update mapContact.values();
                    }
                }
            }
        }
        catch(Exception e)
        {
            VGA_Common.sendExceptionEmail('Exception in Pre-Launch Lead Batch','Hi, <br/><br/> Pre-Launch Lead Assignment Batch failed to run due to following error : <br/><br/>' + e.getMessage() + '<br/><br/>Thanks<br/>Salesforce Support');
        }
    }
    
    global void finish(Database.BatchableContext BC)
    {
       
        try
        {
            Id smsId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('SMS').getRecordTypeId();
            Id emailId = Schema.SObjectType.VGA_Dealer_Notification__c.getRecordTypeInfosByName().get('Email').getRecordTypeId();
            
            List<VGA_Dealer_Notification__c> lstId = new List<VGA_Dealer_Notification__c>();
            List<VGA_Dealer_Notification__c> lstNot = new List<VGA_Dealer_Notification__c>();
            
            for(VGA_Dealer_Notification__c objsub : [select id, recordtypeId, Dealer_Contact__c, VGA_Message_Body__c, VGA_Subscription_Type__c, VGA_Sent_To__c, VGA_Sent_From__c from VGA_Dealer_Notification__c where VGA_Inserted_From_Batch__c = true AND VGA_Notification_Sent__c = false])
            {
                if(objsub.recordtypeId == smsId)
                {
                    lstId.add(objsub);
                }
                else if(objsub.recordtypeId == emailId)
                {
                    lstNot.add(objsub);
                }
            }
            
            if(lstId != Null && !lstId.isEmpty())
            {
                VGA_NotificationTriggerHandler.sendSmsFromBatch(lstId);
            }
            
            if(lstNot != Null && !lstNot.isEmpty())
            {
                VGA_NotificationTriggerHandler.sendEmailFromBatch(lstNot);
            }
            
            if(objplc != Null && objplc.VGA_Tracking_Id__c != Null && objplc.VGA_Number_of_Leads_per_Distribution__c > 0)
            {
                String strquery = 'select Id,VGA_Follow_up_Lead__c from Lead where VGA_Follow_up_Lead__c = true AND VGA_Assignment_Done__c = false AND VGA_Profanity__c = false AND VGA_Spam__c = false AND VGA_Tracking_ID__c = \'' + objplc.VGA_Tracking_Id__c + '\' limit ' + integer.valueof(objplc.VGA_Number_of_Leads_per_Distribution__c);    
                List<Lead> lstLeads = Database.query(strquery);
                System.Debug('@@@@@@Leads are retrieved from sosL as:----' +lstLeads);
                
                if(lstLeads != Null && !lstLeads.isEmpty())
                {
                    if(objplc.VGA_Time_Interval__c != Null && objplc.VGA_Time_Interval__c > 0)
                    {
                        Datetime dt = system.now().addMinutes(Integer.valueof(objplc.VGA_Time_Interval__c));
            
                        String day = string.valueOf(dt.day());
                        String month = string.valueOf(dt.month());
                        String hour = string.valueOf(dt.hour());
                        String minute = string.valueOf(dt.minute());
                        String second = string.valueOf(dt.second());
                        String year = string.valueOf(dt.year());
                        
                                           
                        String strJobName = 'PreLaunchLeadAssignmentJob-' +objplc.VGA_Tracking_Id__c+'-' +String.valueof(dt);                        
                        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
                        
                        if(!test.isRunningTest())
                        {
                            System.schedule(strJobName, strSchedule, new VGA_schedulePreLaunchLeadAssignment(objplc.Id));
                        }
                        List<CronTrigger> lstCron = [select Id from CronTrigger where NextFireTime = Null AND State = 'DELETED'];                        
                        if(lstCron != Null && !lstCron.isEmpty())
                        {
                            for(CronTrigger obj : lstCron) 
                            {
                                system.abortjob(obj.Id);                              
                              
                            }
                        }
                    }
                }
                else
                {
                    objplc.VGA_Status__c = 'Closed';                   
                    update objplc;
                }
            }
            
           
        }
        catch(Exception e)
        {
            if(Label.VGA_Exception_Email_Address != Null)
            {
                List<String> lstaddress = Label.VGA_Exception_Email_Address.split(',');
                 
                if(lstaddress != null && !lstaddress.isEmpty())
                {
                    VGA_Common.sendExceptionEmail('Exception in Pre-Launch Lead Batch','Hi, <br/><br/> Pre-Launch Lead Assignment Batch failed to run due to following error : <br/><br/>' + e.getMessage() +' at line number'+e.getlinenumber()+'<br/><br/>Thanks<br/>Salesforce Support');
                }  
            }
        }
    }
}