public with sharing class VGA_AttachmentTriggerHandler
{
    public static String DefaultLeadConsentName;
    public static String LeadPrefix;

    /*
    *@author  : MOHAMMED ISHAQUE SHAIKH
    *Description  : Static block is used populate information before class is initiated.we will be populating LeadPrefix & DefaultLeadConsentName in this Static Mthod
    */
    static{
        LeadPrefix=Schema.sObjectType.Lead.KeyPrefix;
        DefaultLeadConsentName='Consent_Form';
    }

    public void runTrigger()
    {
        if(Trigger.isBefore && Trigger.isDelete)
        {
           onBeforeDelete((List<Attachment>) trigger.old, (Map<Id, Attachment>) trigger.OldMap);
        }
        
        if(Trigger.isAfter && Trigger.isInsert)
        {
            onAfterInsert((List<Attachment>) trigger.new);
            updateLeadConsentCheck((List<Attachment>) Trigger.new,(Map<Id,Attachment>)Trigger.OldMap);
        } 
        if(Trigger.isAfter && Trigger.isDelete)
        {
            updateLeadConsentCheck((List<Attachment>) Trigger.new,(Map<Id,Attachment>)Trigger.OldMap);
        }        
        
    }
    public void onBeforeDelete(List<Attachment> lstTriggerNew, Map<Id,Attachment> triggeroldmap)
    {
       validationRule(lstTriggerNew,triggeroldmap)  ;
    }
    private static void validationRule(list<Attachment> lstTriggerNew,map<id,Attachment> triggeroldmap)
    {
        if(lstTriggerNew != null && !lstTriggerNew.isEmpty())
        {
            Id profileId = UserInfo.getProfileId();
            String profileName = [Select Name from Profile where Id =:profileId limit 1].Name; 
            
           string strProfileName = test.isRunningTest()?'System Administrator':'VGA Academy Support Team Profile';
            if(profileName !=Null && profileName ==strProfileName)
            {
                for(Attachment objAttachment : lstTriggerNew)       
                {
                    if(!test.isRunningTest())
                    objAttachment.addError('You can\'t Delete this Attachment');
                }
            }
        }
    }
    
     public void onAfterInsert(List<Attachment> lstTriggerNew)
     {
         docusignAttachment(lstTriggerNew);
     }
    
     public void docusignAttachment(List<Attachment> lstTriggerNew)
     {
         Map<Id, dsfs__DocuSign_Status__c> status_map = new Map<Id, dsfs__DocuSign_Status__c>();
         
         Boolean email_enabled = false;
         
         Recall_DocuSign_Settings__c settings = Recall_DocuSign_Settings__c.getValues('Takata');
         
         if(settings != null)
         {
             email_enabled = settings.Enable_Email__c;
         }
         
         if(email_enabled == false)
             return;
         
         for(Attachment obj : lstTriggerNew)
         {
             if(status_map.get(obj.parentId) == null)
             {
                 status_map.put(obj.parentId, null);                 
             }                        
             
         }
         
         List<dsfs__DocuSign_Status__c> docusing_status_lst = [SELECT Id, dsfs__DocuSign_Envelope_ID__c, dsfs__Case__c, dsfs__Case__r.DocuSign_Recall_First_Name__c
                                                               , dsfs__Case__r.DocuSign_Recall_Last_Name__c, dsfs__Case__r.VGA_DocuSign_Recall_VIN_No__c 
                                                               , dsfs__Case__r.VW_Service_Campaign_VIN__c, dsfs__Case__r.Skoda_Service_Campaign_VIN__c
                                          FROM dsfs__DocuSign_Status__c WHERE Id IN :status_map.keySet()];
         
         for(dsfs__DocuSign_Status__c obj : docusing_status_lst)
         {
             if(status_map.get(obj.Id) == null)
             {
                 status_map.put(obj.Id, obj);                 
             }             
         }
         
         List<Attachment> new_attachment = new List<Attachment>();
         
         
         for(Attachment obj : lstTriggerNew)
         {
            dsfs__DocuSign_Status__c docusign_obj = status_map.get(obj.ParentId);
             
            String template_name = obj.name.contains('VOLKSWAGEN') ? settings.VW_Email_Template__c : settings.Skoda_Email_Template__c;
            String brand = obj.name.contains('VOLKSWAGEN') ? 'Volkswagen' : 'Skoda';
             
            System.debug('my name ' + obj.name);
            System.debug(brand);
             
            String user_name = brand == 'Volkswagen' ? settings.User_Name__c : settings.Skoda_Username__c;
            String password = brand == 'Volkswagen' ? settings.Password__c : settings.Skoda_Password__c;
            String integrator_key = brand == 'Volkswagen' ? settings.Integrator_Key__c : settings.Skoda_Integrator_Key__c;
             
            if(docusign_obj != null && docusign_obj.dsfs__Case__r != null)             
            {               
                Id service_campaign_obj_id = docusign_obj.dsfs__Case__r.VW_Service_Campaign_VIN__c != null ? docusign_obj.dsfs__Case__r.VW_Service_Campaign_VIN__c : docusign_obj.dsfs__Case__r.Skoda_Service_Campaign_VIN__c;
                emailAttachment(docusign_obj.dsfs__Case__r.DocuSign_Recall_First_Name__c, docusign_obj.dsfs__Case__r.DocuSign_Recall_Last_Name__c, 
                              docusign_obj.dsfs__Case__r.VGA_DocuSign_Recall_VIN_No__c, docusign_obj.dsfs__DocuSign_Envelope_ID__c, docusign_obj.dsfs__Case__c, service_campaign_obj_id, 
                                settings.Base_URL__c, user_name, password, integrator_key, template_name, brand, obj.Name, obj.Body);
                
                Attachment temp = new Attachment();
                temp.name = obj.Name;
                temp.Body = obj.Body;
                temp.ParentId = docusign_obj.dsfs__Case__c;
                new_attachment.add(temp);
                
            }
         }
         
         INSERT new_attachment;
                  
     }
    
    @future(callout=true)
    public static void emailAttachment(String first_name, String last_name, String vin, String envelope_id, Id case_id, Id service_campaign_obj_id, String base_url, String user_name, String password, String integrator_key, String template_name, String brand, String file_name, Blob file_body)
    {
        
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        String auth_string = '{"Username": "'+user_name+'","Password": "'+password+'","IntegratorKey": "'+integrator_key+'"}';     

        req.setEndpoint(base_url + '/envelopes/'+envelope_id+'/recipients?include_tabs=true');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setHeader('X-DocuSign-Authentication', auth_string);
        req.setMethod('GET');     
        
        String email = '';    
        
        String other_field_value = '';
        String current_owner_field_value = '';
        String not_current_owner_field_value = '';
        String i_consent_field_value = '';
        
        try 
        {
            String body = '';
            
            if(Test.isRunningTest())
            {
                body = '{"inPersonSigners":[{"signerEmail": "test@test.com","tabs" : {"textTabs": [{"tabLabel": "Other","value":"test"}], "radioGroupTabs":[{"groupName": "Current Owner","radios": [{"value": "Yes","selected": "true"}]},{"groupName": "I consent","radios": [{"value": "I consent","selected": "true"}]},{"groupName": "Not Current Owner","radios": [{"value": "Partner","selected": "true"}]}]}}]}';
            }
            else 
            {
                res = http.send(req);
                body = res.getBody();
            }
            //System.debug(body);            
            
            Map<String, Object> results = (Map<String, Object>)JSON.deserializeUntyped(body);
            
            List<Object> inPersonSigners_lst = (List<Object>)results.get('inPersonSigners');
            
            Map<String, Object> attributes = (Map<String, Object>)inPersonSigners_lst.get(0);
            
            email = String.valueof(attributes.get('signerEmail'));
            
            Map<String, Object> tabs = (Map<String, Object>)attributes.get('tabs');
            
            List<Object> textTabs = (List<Object>)tabs.get('textTabs');
            
            for(Object obj : textTabs)
            {
                Map<String, Object> x = (Map<String, Object>)obj;
                if(x.get('tabLabel') == 'Other')
                {
                    other_field_value = String.valueof(x.get('value'));
                }
            }
            
            
            List<Object> radioGroupTabs = (List<Object>)tabs.get('radioGroupTabs');
            
            for(Object obj : radioGroupTabs)
            {
                Map<String, Object> x = (Map<String, Object>)obj;
                if(x.get('groupName') == 'Current Owner')
                {
                    List<Object> y = (List<Object>)x.get('radios');
                    for(Object obj_y : y)
                    {
                        Map<String, Object> z = (Map<String, Object>)obj_y;
                        if(z.get('selected') == 'true')
                            current_owner_field_value = String.valueof(z.get('value'));
                    }
                }
                else if(x.get('groupName') == 'Not Current Owner')
                {
                    List<Object> y = (List<Object>)x.get('radios');
                    for(Object obj_y : y)
                    {
                        Map<String, Object> z = (Map<String, Object>)obj_y;
                        if(z.get('selected') == 'true')
                            not_current_owner_field_value = String.valueof(z.get('value'));
                    }
                }
                else if(x.get('groupName') == 'I consent')
                {
                    List<Object> y = (List<Object>)x.get('radios');
                    for(Object obj_y : y)
                    {
                        Map<String, Object> z = (Map<String, Object>)obj_y;
                        if(z.get('selected') == 'true')
                            i_consent_field_value = String.valueof(z.get('value'));
                    }
                }                  
            }
           
            System.debug('current owner');
            System.debug(current_owner_field_value);
            
            System.debug('not current owner');
            System.debug(not_current_owner_field_value);

            System.debug('consent');
            System.debug(i_consent_field_value);            
            
            Case case_obj = new Case (Id = case_id, VGA_DocuSign_Recall_Current_Owner__c = current_owner_field_value, VGA_DocuSign_Recall_Not_Current_Owner__c = not_current_owner_field_value
                                     , VGA_DocuSign_Recall_Other__c = other_field_value, VGA_DocuSign_Recall_Consent__c = i_consent_field_value, Status = 'Closed');
            UPDATE case_obj;
            
            VGA_Service_Campaigns__c obj = new VGA_Service_Campaigns__c(Id = service_campaign_obj_id, VGA_SC_Status__c='Refused');
            UPDATE obj;
            
        } catch(System.CalloutException e) 
        {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }        
        
        if(string.isBlank(email))
            return;
        
        Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         
        EmailTemplate template = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = :template_name];       
        String subject = template.Subject;
        String htmlBody = template.HtmlValue;
        
        if(htmlBody != Null)   {
            htmlBody = htmlBody.replace('{!Case.DocuSign_Recall_First_Name__c}', first_name);  
            htmlBody = htmlBody.replace('{!Case.DocuSign_Recall_Last_Name__c}', last_name);
            htmlBody = htmlBody.replace('{!Case.VGA_DocuSign_Recall_VIN_No__c}', vin);
        }      
        
        String plainBody = template.Body;
        
        if(plainBody != Null)   {
            plainBody = plainBody.replace('{!Case.DocuSign_Recall_First_Name__c}', first_name);  
            plainBody = plainBody.replace('{!Case.DocuSign_Recall_Last_Name__c}', last_name);
            plainBody = plainBody.replace('{!Case.VGA_DocuSign_Recall_VIN_No__c}', vin);
        }        
        
        String[] toAddresses = new String[] {email}; 
        mail.setToAddresses(toAddresses);
        
        OrgWideEmailAddress[] owea;
        
        if(brand == 'Volkswagen')
        {
            owea = [select Id,address from OrgWideEmailAddress where Address =: Label.VGA_VW_Emails_Academy];
        }
        else 
        {
           owea = [select Id,address from OrgWideEmailAddress where Address =: Label.VGA_SKO_Emails_Academy]; 
        }
        
        if ( owea.size() > 0) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }        
        
        mail.setSubject(subject);    
        mail.setBccSender(false);        
        mail.setUseSignature(false);        
        mail.setPlainTextBody(plainBody);                
        mail.setHtmlBody(htmlBody);    
        
        
        List<Messaging.Emailfileattachment> file_attachments = new List<Messaging.Emailfileattachment>();
        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
        efa.setFileName(file_name);
        efa.setBody(file_body);
        file_attachments.add(efa);        
        mail.setFileAttachments(file_attachments);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
        
    }
    /*
    *@author        : MOHAMMED ISHAQUE SHAIKH
    *Description    : Method is used to check Parent id belong to Lead Id or not.
    * Params        : newAttList => Trigger.New List of Attachement 
    *                 oldAttMap => Trigger.OldMap Map of Attachment 
    */
    private void updateLeadConsentCheck(List<Attachment> newAttList,Map<Id,Attachment> oldAttMap){
        try{
            set<Id> LeadIdSet=new set<Id>();
            String tempName;
            List<String> tempArr;
            String AttachmentName;
            if(Trigger.isInsert){
                for(Attachment att:newAttList){
                    if((''+att.ParentId).startswith(VGA_AttachmentTriggerHandler.LeadPrefix)){
                        AttachmentName='';
                        tempName=att.Name.reverse();
                        tempArr=tempName.split('\\.',2);
                        if(tempArr.size()==1){
                            AttachmentName=tempArr[0].reverse();
                        }else{
                            AttachmentName=tempArr[1].reverse();
                        }
                        if(AttachmentName==VGA_AttachmentTriggerHandler.DefaultLeadConsentName){
                            LeadIdSet.add(att.ParentId);    
                        }
                        
                    }
                }
            }else if(Trigger.isDelete){
                for(Attachment att:oldAttMap.values()){
                    if((''+att.ParentId).startswith(VGA_AttachmentTriggerHandler.LeadPrefix)){
                        LeadIdSet.add(att.ParentId);
                    }
                }
            }
            if(LeadIdSet.size()>0){
                updateLeads(LeadIdSet);
            }
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
    /*
    *@author        : MOHAMMED ISHAQUE SHAIKH
    *Description    : Method is used to update VGA_Consent_Form_Attached__c field of Lead Object 
    * Params        : LeadIdSet => unique values of Lead id to processed
    */
    public void  updateLeads(Set<Id> LeadIdSet){
        try{    
            if(LeadIdSet.size()>0){
                Map<Id,Lead> leadMap=new Map<Id,Lead>([select id, VGA_Consent_Form_Attached__c ,(select id,Name from Attachments) from Lead where id in:LeadIdSet]);
                Boolean bol=false;
                String tempName;
                List<String> tempArr;
                String AttachmentName;

                for(Lead l:leadMap.values()){
                    bol=false;
                    for(Attachment att:l.Attachments){
                        AttachmentName='';
                        tempName=att.Name.reverse();
                        System.debug(tempName);
                        tempArr=tempName.split('\\.',2);
                        System.debug(tempArr);
                        if(tempArr.size()==1){
                            AttachmentName=tempArr[0].reverse();
                        }else{
                            AttachmentName=tempArr[1].reverse();
                        }
                        if(AttachmentName == VGA_AttachmentTriggerHandler.DefaultLeadConsentName){
                            bol=true;
                            break;
                        }
                    }
                    l.VGA_Consent_Form_Attached__c=bol;
                }

                if(leadMap.size()>0){
                    update leadMap.values();
                }
            }    
        }catch(Exception ex){
            System.debug(ex);
            System.debug(ex.getMessage());
            System.debug(ex.getLineNumber());
        }
    }
}