/**
 * ErrorLogBuilder contains all the methods to build ErrorLogs
 * for the given conditions
 */
public class ErrorLogBuilder {

    public List<VGA_ErrorLog__c> errorLogToInsertList = new List<VGA_ErrorLog__c>();
    
    public ErrorLogBuilder(){
    }
    
    /**
     * Create a Error Log record when given a record Id, an error message, source, type and process
     */
    public void add(Map<String, Object> errorLogValues, Integer errorLogLimit){
        
        if(this.errorLogToInsertList.size() < errorLogLimit){
            VGA_ErrorLog__c errorLog = new VGA_ErrorLog__c();
            for (String field : errorLogValues.keySet()) {
                Object val = errorLogValues.get(field);
                System.debug('>>>' + field + '>>> ' + val);
                errorLog.put(field, val);
            }

            this.errorLogToInsertList.add(errorLog);
        }
    }

    /**
     * Insert the Error Log records stored in a List. Check we have the right permissions to edit all the 
     * fields and throw an exception if we don't.
     */
    public void save(){
        if (!this.errorLogToInsertList.isEmpty()) {
            if (VGA_ErrorLog__c.sObjectType.getDescribe().isCreateable()
                && Schema.sObjectType.VGA_ErrorLog__c.fields.Type__c.isCreateable()
                && Schema.sObjectType.VGA_ErrorLog__c.fields.RelatedRecordID__c.isCreateable()
                && Schema.sObjectType.VGA_ErrorLog__c.fields.RelatedObject__c.isCreateable()
                && Schema.sObjectType.VGA_ErrorLog__c.fields.Process__c.isCreateable()
                && Schema.sObjectType.VGA_ErrorLog__c.fields.Message__c.isCreateable()) {
                insert this.errorLogToInsertList;
            } else {
                NoAccessException e = new NoAccessException();
                e.setMessage('Cannot insert VGA_ErrorLog__c: Insufficient access');
                throw e;
            }

        }
    }
}