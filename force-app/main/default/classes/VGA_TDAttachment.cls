/**
* @author Mohammed Ishaque Shaikh
* @date 05/Mar/2019
* @Ticket No  #1085 getAttachment Salesforce API
* @description Apex class used to send Attachment info to third party sites .
*/
@RestResource(urlMapping='/getAttachment/*')
global class VGA_TDAttachment {
	/*
	* @author Mohammed Ishaque Shaikh
	* @description Wrapper class is used to send data in specified format 
	*/
    public class AttachementResponseWrapper{
        String name,contentType,body;
         public Integer statusCode;
        public AttachementResponseWrapper(){
            name=null;
            contentType=null;
            body=null;
            statusCode=500;
        }
    }
    
    /*
	* @author Mohammed Ishaque Shaikh
	* @description Method used to send Attachment information to third party sites.
	*/ 
    @Httpget
    global static void collectAttachment(){
        RestRequest req=RestContext.request;
        RestResponse res=RestContext.response;
        try{
            String AttachId =req.params.get('id');
            if(AttachId!=null){
                Attachment att=[SELECT ContentType, Name, Id, Body FROM Attachment where id=:AttachId];
                if(att!=null){
                    AttachementResponseWrapper data=new AttachementResponseWrapper();
                    data.name=att.Name!=null?att.Name:null;
                    data.contentType=att.ContentType!=null?att.ContentType:null;
                    data.body=att.Body!=null?EncodingUtil.base64Encode(att.Body):null;
                    data.statusCode=200;
                    res.responseBody=Blob.valueOf(JSON.serializePretty(data));
                }else{
                    res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Attachment not found"}')));
                }
            }else{
                res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Attachment not found"}')));
            }
                
        }catch(Exception ex){
            System.debug(ex+'\n'+ex.getLineNumber()+'\n'+ex.getMessage());
            res.responseBody=Blob.valueOf(JSON.serializePretty(JSON.deserializeUntyped('{"statusCode":500,"message":"Attachment not found"}')));
        }
    }
}