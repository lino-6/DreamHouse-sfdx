//=================================================================================
// Tracker class for VGA_schedulePreLaunchLeadAssignment class.
// =================================================================================
// Created by Nitisha Prasad on 27-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_schedulePreLaunchLeadAssignTest
{
    public static VGA_schedulePreLaunchLeadAssignment sh1;
    public static VGA_Pre_Launch_Configuration__c objPreLaunch;
    public static Product2 objPro;
    
    static testMethod void myUnitTest() 
    {
        LoadData();
        
        sh1 = new VGA_schedulePreLaunchLeadAssignment(objPreLaunch.id);
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
    } 
    
    public static void LoadData() 
    {
        // TO DO: implement unit test
        objPro = new Product2();
        objPro.Name = 'test';
        insert objPro;
        
        objPreLaunch = new VGA_Pre_Launch_Configuration__c();
        insert objPreLaunch;
    }
}