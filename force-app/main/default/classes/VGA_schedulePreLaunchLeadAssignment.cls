global class VGA_schedulePreLaunchLeadAssignment implements Schedulable 
{
    global final string strRecordId;
    
    global VGA_schedulePreLaunchLeadAssignment(String recordid)
    {
        strRecordId = recordid;
    }
    
    global void execute(SchedulableContext sc) 
    {
        VGA_PreLaunchAssignmentBatch b = new VGA_PreLaunchAssignmentBatch(strRecordId); 
        database.executebatch(b);
    }
}