@RestResource(urlMapping='/getCarConfiguratorConfig/*')
global class VGA_Car_Configurator_payload{
    
    /**
    * @author Ashok Chandra    
    * @date 10/07/2019
    * @description REST API to get the payload of carconfig       
    */
    @Httpget
    global Static void Getconfig() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        //Get brand name parameter
        String Apiname= req.params.get('apiName');
        //Get brand TrackingCode parameter  
        Getconfigrecord(apiname);        
    }
    
    /**
    * @author Ashok Chandra
    * @date 10/07/2019
    * @description Method will take the apiname as parameter and check for the 
    *              VGA_Car_configurator_Config__c record
    *              Get the attachment of VGA_Car_configurator_Config__c. 
    *              Send respose as json of attachment body.             
    * @param apiname. 
    */
    private static Void Getconfigrecord(string capiname)
    {
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');
        
        Date currentdate=Date.today();
        JSONGenerator gen;
        
        if(capiname!='')
        {
            list<VGA_Car_configurator_Config__c> config=[select id,VGA_Api_Name__c,VGA_Expiry_Date__c from VGA_Car_configurator_Config__c where VGA_Api_Name__c=:capiname and (VGA_Expiry_Date__c=null or Day_Only(convertTimezone(VGA_Expiry_Date__c)) >= :Currentdate)];
            
            if(!config.isEmpty())
            {          
                list<Attachment> attachmentlist=[select id,body from attachment where parentid=:config[0].id];
                res.responseBody = attachmentlist[0].body;
                res.statuscode=200;
                
            } 
            else
            {
                gen= JSON.createGenerator(true);
                gen.writeStartObject();
                gen.writeEndObject();
                res.responseBody = Blob.valueOf(gen.getAsString()); 
            }            
        }
        else
        {
            gen= JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeEndObject();            
            res.responseBody = Blob.valueOf(gen.getAsString());
        }         
    }
}