@isTest
global class DocuSignAPITracker implements WebServiceMock{

    global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
       }    
    
    @isTest static void test1() 
    {
        Test.setMock(WebServiceMock.class, new DocuSignAPITracker());
        
        new DocuSignAPI.RequestRecipientTokenClientURLs();
        new DocuSignAPI.VaultingOptions();
        new DOcuSignAPI.Tab();
        new DocuSignAPI.RecipientStatusEsignAgreementInformation();
        new DocuSignAPI.EnvelopeStatus();
        new DocuSignAPI.TabStatus();
        new DocuSignAPI.Envelope();
        new DocuSignAPI.EnvelopeInformation();
        new DocuSignAPI.AuthenticationStatus();
        new DocuSignAPI.Recipient();
        new DocuSignAPI.RecipientStatus();
        new DocuSignAPI.Document();
        new DocuSignAPI.ArrayOfTemplateReference();
        new DocuSignAPI.SSN4InformationInput();
        new DocuSignAPI.EventNotification();
        new DocuSignAPI.ArrayOfServerTemplate();
        new DocuSignAPI.ArrayOfTemplateReferenceFieldDataDataValue();
        new DocuSignAPI.Attachment();
        new DocuSignAPI.IDCheckInformation();
        new DocuSignAPI.SSN4Information();
        new DocuSignAPI.Reminders();
        new DocuSignAPI.FormDataXfdf();
        new DocuSignAPI.RecipientCaptiveInfo();
        new DocuSignAPI.ArrayOfRecipientStatus();
        new DocuSignAPI.EnvelopeEvent();
        new DocuSignAPI.RecipientPhoneAuthentication();
        new DocuSignAPI.TemplateReferenceFieldDataDataValue();
        new DocuSignAPI.RequestRecipientFaxToken_element();
        new DocuSignAPI.DOBInformationInput();
        new DocuSignAPI.RecipientSignatureInfo();
        new DocuSignAPI.InlineTemplate();
        new DocuSignAPI.AddressInformationInput();
        new DocuSignAPI.AddressInformation();
        new DocuSignAPI.TemplateReferenceRoleAssignment();
        new DocuSignAPI.Notification();
        new DocuSignAPI.ArrayOfAttachment();
        new DocuSignAPI.RequestRecipientTokenAuthenticationAssertion();
        new DocuSignAPI.ServerTemplate();
        
        DocuSignAPI.APIServiceSoap stub = new DocuSignAPI.APIServiceSoap();
        stub.SendEnvelope(null, null);           
        stub.CreateEnvelopeFromTemplates(null, null, null, null);
        stub.RequestSenderToken(null, null, null);
        stub.RequestRecipientToken(null, null, null, null, null, null);
        stub.CreateAndSendEnvelope(null);
        stub.CreateEnvelope(null);
        stub.CreateEnvelopeFromTemplatesAndForms(null, null, null);
    }
        
    
}