@isTest
private with sharing class VWMeetingCompletedControllerUnitTest {
    
    private static String ownerID = null;
    
 static testMethod void statusChangedToCompletedTest() {
     
       	//Create ten test events. 
		List<Event> testEventsForFiles = createTestEvents();
		
		//List of event ids. To be used later as a parameter with the getAllFiles method.
		List<Id> testEventIDList = new List<Id>();
		
		//Create a single test file that can be associated with events
		Id testFileID = createTestFile();
		
		//Fetch the test file id created above
		List<SObject> contentDocuments = [SELECT Id FROM ContentDocument WHERE Title = 'TestImage'];
		
		//Associate the test file with events. In practice this means creating a content document link for each association.
		for (Event e : testEventsForFiles){		
			createTestContentDocumentLink(contentDocuments[0].Id, e.Id);
			testEventIDList.add(e.Id);
		}
     
     	//Get a list of all meetings created based on the event id list
     	List<punosmobile__Meeting__c> meetingList = [SELECT Id FROM punosmobile__Meeting__c WHERE punosmobile__Event__c IN :testEventIDList];
     
     	List<String> meetingIdList = new List<String>();
         
     	for(punosmobile__Meeting__c meeting : meetingList){
        	 meetingIdList.add(meeting.Id);
     	}
     
        system.assertEquals('success', VWMeetingCompletedController.statusChangedToCompleted(meetingIdList)[0]);
    }
    
    static testMethod void createPDFTest() {
        
       	//Create ten test events. 
		List<Event> testEventsForFiles = createTestEvents();
		
		//List of event ids. To be used later as a parameter with the getAllFiles method.
		List<Id> testEventIDList = new List<Id>();
		
		//Create a single test file that can be associated with events
		Id testFileID = createTestFile();
		
		//Fetch the test file id created above
		List<SObject> documents = [SELECT Id FROM ContentDocument WHERE Title = 'TestImage'];
		
		//Associate the test file with events. In practice this means creating a content document link for each association.
		for (Event e : testEventsForFiles){		
			createTestContentDocumentLink(documents[0].Id, e.Id);
			testEventIDList.add(e.Id);
		}
     
     	//Get a list of all meetings created based on the event id list
     	List<punosmobile__Meeting__c> meetingList = [SELECT Id FROM punosmobile__Meeting__c WHERE punosmobile__Event__c IN :testEventIDList];
     
     	List<String> meetingIdList = new List<String>();
         
     	for(punosmobile__Meeting__c meeting : meetingList){
        	 meetingIdList.add(meeting.Id);
     	}
        
        Id cvId = VWMeetingCompletedController.createPDF(meetingIdList[0], 'testName');
        
        system.assertNotEquals(null, cvId);
        
        List<ContentDocument> cDocuments = [SELECT Id FROM ContentDocument WHERE Id in (SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cvId AND IsLatest = true)];
        
        system.assertEquals(false, cDocuments.isEmpty());
    }
    
    //Creates a collection of events for testing
    private static List<Event> createTestEvents(){
    	
        //Creates a test contact
        Contact testContact = new Contact(LastName = 'Test Contact');
        insert testContact;
        
    	//Creates a test user that can be used as an event owner
        OwnerID = createTestUser();
        
        List<Event> events = new List<Event>();
		
		//Test event ActivivtyDateTime is the current moment
        DateTime dt = DateTime.now();

		// Create 10 new Events
        for(Integer i=0; i < 10; i++) {
            Event event = new Event();
            event.OwnerId = ownerID;
            event.Subject='Test'+i;
            event.DurationInMinutes = 60;
            event.ActivityDateTime = dt;
            event.WhoId = testContact.Id;
            events.add(event);
        }

		insert events;

        return events;
    }
    
    	//Creates a test user for test scenarios requiring one
    private static String createTestUser(){

		//First we check that does the user already exist.
        List<User> userlist = [SELECT id From User WHERE Username = 'testStandardUser@punosmobile.com' LIMIT 1];
        if(!userlist.isEmpty()){
            return userlist[0].ID;
        }
        else //The user does not exist. We'll create a new standard user.
        	 //Please note that for different scenarios, you might need to change, e.g., the profile, locale and time zone.
        {
            Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];

            User user = new User(alias = 'tester',
                                 firstname='Test',
                                 lastname='Testing',
                                 username='testStandardUser@punosmobile.com',
                                 email='standarduser@punosmobile.com',
                                 emailencodingkey='UTF-8',
                                 languagelocalekey='en_US',
                                 localesidkey='en_US',
                                 profileid = profileId.id,
                                 timezonesidkey='America/Los_Angeles');

            insert user;
			
			//Ensure that the user was created successfully and fetch the Id
            List<User> NewUserlist = [SELECT id From User WHERE Username = 'testStandardUser@punosmobile.com' LIMIT 1];

            return NewUserlist[0].Id;
        }
    }
    
    //Creates a test file, which in practice means creating a new ContentVersion. This is later associated with a ContentDocumentLink.
    private static Id createTestFile(){
    	
    	//Image data as a base64-encoded string (actually a really small .png image).       
    	Blob blobo = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAABIAAAAPCAIAAABm5AhFAAAAA3NCSVQICAjb4U/gAAAAGXRFWHRTb2Z0d2FyZQBnbm9tZS1zY3JlZW5zaG907wO/PgAAAJFJREFUKJFj/P//PwPpgIkMPeRrY4Gz9h86dvz0OZJtc7SzMjcxJFkbAwODs721ga42ydoYGBg8XR2I0YmujZGR0dPVQVNdlTRtEJ3+Xq74dWKPACYmJn8vV2VFedK0QXQG+3nh0okvullYmIP9vGRlpEjTBtEZHuSLqZNw4mJjZQ0P8pUQFyVNG0RnZIg/sk4An4MWIuoN1jgAAAAASUVORK5CYII=');                                               
        
        //Create tbe content version
        SObject testContentVersion = Schema.getGlobalDescribe().get('ContentVersion').newSObject();
        testContentVersion.put('ContentLocation', 'S');
        testContentVersion.put('VersionData', blobo);
        testContentVersion.put('Title', 'TestImage');
        testContentVersion.put('PathOnClient', 'TestImage');   
            
        insert testContentVersion; 
         
        //Return the above created content version id. This is needed to create the content document link associating the content version with the sObject.        
        return testContentVersion.Id;
    }
    
    //Creates a ContentDocumentLink associating a ContentDocument with an Event.
    private static void createTestContentDocumentLink(Id cdiidee, Id eventiidee){
    	ContentDocumentLink testContentDocumentLink = new ContentDocumentLink();
		testContentDocumentLink.LinkedEntityId = eventiidee;
		testContentDocumentLink.ShareType = 'V'; 
		testContentDocumentLink.ContentDocumentId = cdiidee;
		testContentDocumentLink.Visibility = 'AllUsers'; 
		insert testContentDocumentLink;
    }
    
}