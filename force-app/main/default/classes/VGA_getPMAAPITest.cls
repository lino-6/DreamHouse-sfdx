@isTest
public class VGA_getPMAAPITest {
    @testsetup
    Static void buildPMAData()
    {
        Account objAccount = VGA_CommonTracker.createDealerAccount();
        objAccount.VGA_Dealer_Code__c = '1233';
        Update objAccount;
             
        Account objAccount2 = VGA_CommonTracker.createDealerAccount();
        objAccount2.VGA_Dealer_Code__c = '1235';
        Update objAccount2;
        
        Account objAccount3 = VGA_CommonTracker.createDealerAccount();
        objAccount3.VGA_Dealer_Code__c = '1236';
        Update objAccount3;
        
        Account objAccount4 = VGA_CommonTracker.createDealerAccount();
        objAccount4.VGA_Dealer_Code__c = '1237';
        Update objAccount4;
        
        VGA_Post_Code_Mapping__c objPostCodeMap = new VGA_Post_Code_Mapping__c();
        objPostCodeMap.VGA_Post_Code__c                = '2000';
        objPostCodeMap.VGA_VWP_Dealer_Code__c		   = '1212';
        objPostCodeMap.VGA_VWP_Dealer_Code_2nd__c      = '1200';
        objPostCodeMap.VGA_VWP_Dealer_Code_3rd__c	   = '1600';
        objPostCodeMap.VGA_VWP_Dealer_Code_4th__c	   = '1400';
        objPostCodeMap.VGA_VWP_Dealer_Code_5th__c      = '1234';
        objPostCodeMap.VGA_VWP_Dealer__c 			   = objAccount.id;	
        objPostCodeMap.VGA_VWP_Dealer_3rd__c 		   = objAccount2.id;	
        objPostCodeMap.VGA_VWP_Dealer_4th__c 		   = objAccount3.id;	
        objPostCodeMap.VGA_VWP_Dealer_5th__c 		   = objAccount4.id;
        INSERT objPostCodeMap;
        
        VGA_Post_Code_Mapping__c objPostCodeMapvwc = new VGA_Post_Code_Mapping__c();
        objPostCodeMapvwc.VGA_Post_Code__c                = '2010';
        objPostCodeMapvwc.VGA_VWC_Dealer_Code__c		   = '1112';
        objPostCodeMapvwc.VGA_VWC_Dealer_Code_2nd__c      = '1210';
        objPostCodeMapvwc.VGA_VWC_Dealer_Code_3rd__c	   = '1100';
        objPostCodeMapvwc.VGA_VWC_Dealer_Code_4th__c	   = '1100';
        objPostCodeMapvwc.VGA_VWC_Dealer_Code_5th__c      = '1214';
        objPostCodeMapvwc.VGA_VWC_Dealer__c 			   = objAccount.id;	
        objPostCodeMapvwc.VGA_VWC_Dealer_3rd__c 		   = objAccount2.id;	
        objPostCodeMapvwc.VGA_VWC_Dealer_4th__c 		   = objAccount3.id;	
        objPostCodeMapvwc.VGA_VWC_Dealer_5th__c 		   = objAccount4.id;
        INSERT objPostCodeMapvwc;
        
        VGA_Post_Code_Mapping__c objPostCodeMapSKO = new VGA_Post_Code_Mapping__c();
        objPostCodeMapSKO.VGA_Post_Code__c                = '2200';
        objPostCodeMapSKO.VGA_SKP_Dealer_Code__c		  = '1212';
        objPostCodeMapSKO.VGA_SKP_Dealer_Code_2nd__c      = '1220';
        objPostCodeMapSKO.VGA_SKP_Dealer_Code_3rd__c	  = '1620';
        objPostCodeMapSKO.VGA_SKP_Dealer_Code_4th__c	  = '1420';
        objPostCodeMapSKO.VGA_SKP_Dealer_Code_5th__c      = '1224';
        objPostCodeMapSKO.VGA_SKP_Dealer__c 			  = objAccount.id;	
        objPostCodeMapSKO.VGA_SKP_Dealer_3rd__c 		  = objAccount2.id;	
        objPostCodeMapSKO.VGA_SKP_Dealer_4th__c 		  = objAccount3.id;	
        objPostCodeMapSKO.VGA_SKP_Dealer_5th__c 		  = objAccount4.id;
        INSERT objPostCodeMapSKO;
        
        VGA_Post_Code_Mapping__c objPostCodeMapSKODealer = new VGA_Post_Code_Mapping__c();
        objPostCodeMapSKODealer.VGA_Post_Code__c                = '2300';
        objPostCodeMapSKODealer.VGA_SKP_Dealer_Code__c		  = '1212';
        objPostCodeMapSKODealer.VGA_SKP_Dealer_Code_2nd__c      = '1220';
        objPostCodeMapSKODealer.VGA_SKP_Dealer_Code_3rd__c	  = '1620';
        objPostCodeMapSKODealer.VGA_SKP_Dealer_Code_4th__c	  = '1420';
        objPostCodeMapSKODealer.VGA_SKP_Dealer_Code_5th__c      = '1224';
        objPostCodeMapSKODealer.VGA_SKP_Dealer__c 			  = objAccount.id;	
        INSERT objPostCodeMapSKODealer;
        
        VGA_Post_Code_Mapping__c objPostCodeMapVgaVWP = new VGA_Post_Code_Mapping__c();
        objPostCodeMapVgaVWP.VGA_Post_Code__c                 = '2930';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer_Code__c        = '1260';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer_Code_2nd__c	  = '1650';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer_Code_3rd__c	  = '1420';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer_Code_4th__c    = '1264';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer_Code_5th__c    = '1234';
        objPostCodeMapVgaVWP.VGA_VWP_Dealer__c 			      = objAccount.id;	
        INSERT objPostCodeMapVgaVWP; 
        
        VGA_Post_Code_Mapping__c objPostCodeMapVgaVWC = new VGA_Post_Code_Mapping__c();
        objPostCodeMapVgaVWC.VGA_Post_Code__c                 = '2960';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer_Code__c        = '1260';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer_Code_2nd__c	  = '1650';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer_Code_3rd__c	  = '1420';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer_Code_4th__c    = '1264';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer_Code_5th__c    = '1234';
        objPostCodeMapVgaVWC.VGA_VWP_Dealer__c 			      = objAccount.id;	
        INSERT objPostCodeMapVgaVWC; 
    }

    static testmethod void pmaapitest1(){
      Test.startTest();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/PMA'; 
        
        req.params.put('brand', 'Volkswagen');
        req.params.put('subbrand', 'PV');
        req.params.put('postcode', '2000');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();

        req.params.put('brand', 'Volkswagen');
        req.params.put('subbrand', 'cv');
        req.params.put('postcode', '2010');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        req.params.put('brand', 'Skoda');
        req.params.put('postcode', '2200');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        req.params.put('brand', 'Skoda');
        req.params.put('postcode', '2300');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        req.params.put('brand', 'Skoda');
        req.params.put('postcode', '2900');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        req.params.put('brand', 'Volkswagen');
        req.params.put('subbrand', 'pv');
        req.params.put('postcode', '2930');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        req.params.put('brand', 'Volkswagen');
        req.params.put('subbrand', 'cv');
        req.params.put('postcode', '2960');
        req.httpMethod = 'GET';
        req.addHeader('Content-Type', 'application/json'); 
        RestContext.request = req;
        RestContext.response = res;
        VGA_getPMAAPI.getPMA();
        
        Test.stopTest();   
        System.debug(EncodingUtil.base64Encode(res.responseBody));
    }

}