//Tracker class for VGA_LeadDetailsController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          7-Nov-2017 
//===============================================================================================
//Coverage for VGA_LeadDetailsController on                 7-Nov-2017      90%
//===============================================================================================
@isTest(seeAllData = false)       
public class VGA_LeadDetailsControllerTracker
{
    @TestSetup
    public static void buildConfigList()
    {  
        List<contact> conList =new List<Contact>();
        List<User> UserList=new List<User>();
        List<VGA_User_Profile__c> conUserProfileList = new List<VGA_User_Profile__c> ();
        Account acc;
        
        List<VGA_Triggers__c> CustomList=new List<VGA_Triggers__c>();
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Lead',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Account',true));
        CustomList.add(VGA_CommonTracker.buildCustomSettingTrigger('Contact',true));
        insert CustomList;
        
        acc=VGA_CommonTracker.buildDealerAccount('aa','qqq');
        acc.VGA_Pilot_Dealer__c=true;
        insert acc;        
        Account AccountCon=VGA_CommonTracker.createAccount();
        //Creating Contact
        Contact con=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt1');
        conList.add(con);
        Contact con1=VGA_CommonTracker.buildDealerContact(acc.Id,'TestCnt2');
        con1.VGA_Role__c ='Dealer Administrator';
        conList.add(con1);
        insert conList;
        conUserProfileList=[SELECT Id,Name,VGA_Contact__c,VGA_Available_TestDrive__c FROM VGA_User_Profile__c];
        for(VGA_User_Profile__c t:conUserProfileList){
            t.VGA_Available_TestDrive__c=true;
        }
        update conUserProfileList;
        
        //Creating User 
        User PortalUser=VGA_CommonTracker.buildUser(conList[0].Id);
        PortalUser.IsActive  = True;
        UserList.add(PortalUser);
        User PortalUser1=VGA_CommonTracker.buildUser(conList[1].Id);
        PortalUser1.IsActive  = True;
        UserList.add(PortalUser1);
        Insert UserList; 
        
        Lead objLead=VGA_CommonTracker.buildLead('ABC');
        //objLead.PartnerAccountId=acc.Id;
        objLead.VGA_Assigned_to_Dealership__c=System.now();
        objLead.OwnerId=UserList[0].Id;
        objLead.VGA_Primary_id__c='MyID';
        insert objLead;

        Event eve=new Event();
        eve.whoId=objLead.Id;
        eve.startdatetime=System.now();
        eve.endDateTime=System.now().addMinutes(25);
        insert eve;

        Note nt=new Note();
        nt.body='sss';          
        nt.title='sss';
        nt.ParentId=objLead.Id;
        insert nt;
        Attachment objAtt=VGA_CommonTracker.buildAttachment(objLead.Id,'Consent_Form');
        insert objAtt;
        
    }
    
    
    /**
    * @author Mohammed Ishaque Shaikh
    * @date 2019-03-19
    * @description test class for all apex method inside VGA_LeadDetailsController
    * 
    */
    
    static testMethod void allTestMethod(){
        
        List<User> UserList =[select id,IsActive,contact.AccountId,contactid,ProfileID,username,firstname,lastname,email,VGA_Brand1__c from user where firstName='First'];
        Lead objLead = [Select id,firstName from Lead where ownerid= :UserList[0].id Limit 1 ];
        Event eve = [Select id,startdatetime,endDateTime,whoId from Event where Whoid=:objLead.id Limit 1 ];
        test.startTest();
        System.runAs(UserList[1]){
            System.assertEquals('Dealer Administrator',VGA_LeadDetailsController.getuserInformation());
        }
        System.runAs(UserList[0]){
            System.assertEquals(UserList[0].Id,VGA_LeadDetailsController.getlogginguser().Id);
            
            System.AssertEquals(objLead.Id,VGA_LeadDetailsController.getLeadlist( 'Last 7 days','New', UserList[0].contact.AccountId, 'ALL','VGA_Assigned_to_Dealership__c',true)[0].Id);
            System.assertEquals(objLead.Id, VGA_LeadDetailsController.getLeadDetails(objLead.Id).Id);
            System.assertEquals(eve.Id,VGA_LeadDetailsController.getEvent(objLead.Id).Id);
            System.assertEquals(3,VGA_LeadDetailsController.getcontactDetails().size());
            Account aa=[select id,name from Account where Id=:UserList[0].contact.AccountId];
            System.assertEquals(UserList[0].contact.AccountId,VGA_LeadDetailsController.getAccountDetails().get(aa.Name));
            System.assertEquals(2,VGA_LeadDetailsController.getPilotContacts().size());
            System.assertEquals('Volkswagen Dealer Consultant Profile',VGA_LeadDetailsController.getuserInformationProfile());
            System.assertEquals(2,VGA_LeadDetailsController.getresign(objLead.Id).size());
            System.assertEquals('Consultant',VGA_LeadDetailsController.getuserInformation());
            System.assertEquals(objLead.Id,VGA_LeadDetailsController.updateleadstatus(objLead.Id));
            System.assertEquals(1,VGA_LeadDetailsController.getnotes(objLead.Id).size());
            System.assertEquals('Comment saved successfully',VGA_LeadDetailsController.createnotes('dddd','ddd',objLead.Id));
            VGA_LeadDetailsController.getLeadHistory(objLead.Id);

            System.assertEquals('The Event was successfully saved.',VGA_LeadDetailsController.saveEventDetails(eve,objLead,UserList[0].contactid));
            System.assertEquals(true,VGA_LeadDetailsController.deleteEventDetails(eve));
            System.assertEquals(objLead.Id,VGA_LeadDetailsController.updateLeadDetails(objLead,UserList[1].Id,null,null));
            VGA_LeadDetailsController.collectAttachmentWithParentId(objLead.Id);
            VGA_LeadDetailsController.getPilotContactsUsers();
            VGA_LeadDetailsController.getAttachment(objLead.Id);
            Campaign camp=VGA_CommonTracker.buildCampaign('MyCamp');
            insert camp;
            campaignmember cmm=new campaignmember();
            cmm.campaignId=camp.Id;
            cmm.LeadId=objLead.Id;
            insert cmm;
            VGA_LeadDetailsController.getcampaignMap(objLead.Id);
            
        }
        test.stopTest();
        
        
    }
    
}