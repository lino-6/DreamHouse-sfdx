//Tracker class for VGA_VGA_ExportLeadController
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for VGA_ExportLeadController on              8-Nov-2017      90%
//===============================================================================================
@isTest(seeAllData = false)       
public class VGA_ExportLeadControllerTracker 
{
    public static testMethod void Test1()
    {
         
         Account objAccount =VGA_CommonTracker.createDealerAccount();
         Contact objContact =VGA_CommonTracker.createDealercontact(objAccount.Id);
         User objUser       =VGA_CommonTracker.CreateUser(objContact.Id);
         Lead objLead       =VGA_CommonTracker.createLead();
         ApexPages.currentpage().getparameters().put('status','New'); 
         ApexPages.currentpage().getparameters().put('assign','Last 30 days'); 
         ApexPages.currentpage().getparameters().put('DealerId',objAccount.Id); 
         ApexPages.currentpage().getparameters().put('ConsultantId',objUser.Id); 
         ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objLead);
         VGA_ExportLeadController objExport = new VGA_ExportLeadController(sc);
    }       
}