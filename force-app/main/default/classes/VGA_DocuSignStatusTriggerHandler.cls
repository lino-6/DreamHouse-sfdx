public class VGA_DocuSignStatusTriggerHandler {
    
    public void runTrigger()
    {
        // Method will be called to handle After Update events
        if(Trigger.isAfter && Trigger.isUpdate)
        {
            //This logic has been moved to attachment trigger
            onAfterUpdate((List<dsfs__DocuSign_Status__c>) trigger.new, (Map<Id, dsfs__DocuSign_Status__c>) trigger.OldMap);
        }
    }
    
    public void onAfterUpdate(List<dsfs__DocuSign_Status__c> lstTriggerNew, Map<Id, dsfs__DocuSign_Status__c> mapTriggerOld)
    {
        /*
        Set<Id> obj_id = new Set<Id>();
        List<VGA_Service_Campaigns__c> service_campaign_lst = new List<VGA_Service_Campaigns__c>();
        
        for(dsfs__DocuSign_Status__c obj : lstTriggerNew)
        {
            if(obj.dsfs__Envelope_Status__c == 'Completed')
            {
                obj_id.add(obj.dsfs__Case__c);
            }
        }
        
        List<Case> case_lst = [SELECT VW_Service_Campaign_VIN__c, Skoda_Service_Campaign_VIN__c FROM Case WHERE Id IN :obj_id];
        
        for(Case obj_case : case_lst)
        {
            Id service_campaign_obj_id = obj_case.VW_Service_Campaign_VIN__c != null ? obj_case.VW_Service_Campaign_VIN__c : obj_case.Skoda_Service_Campaign_VIN__c;
            if(service_campaign_obj_id != null)
            {
                VGA_Service_Campaigns__c obj = new VGA_Service_Campaigns__c(Id = service_campaign_obj_id, VGA_SC_Status__c='Refused');
                service_campaign_lst.add(obj);
            }
            
            obj_case.Status = 'Closed';
        }
        
        if(!case_lst.isEmpty())
        	UPDATE case_lst;
        
        if(!service_campaign_lst.isEmpty())
        	UPDATE service_campaign_lst;
        
       */ 
    }
    
}