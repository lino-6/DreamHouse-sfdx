@isTest(seeAllData = false)
public class VGA_RecallDetailsControllerTracker {
    public static testMethod void loaddata()
    {
        
        Product2 p = new Product2();
        p.VGA_Brand__c = 'PV Volkswagen';
        p.name = 'Model';
        insert p;  
        
        Account acc = new Account();
        acc.lastName = 'Lastname';
        acc.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        insert acc;
        
        Asset a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'WV1ZZZ7JZBX012828';
        insert a;
        
        p = new Product2();
        p.VGA_Brand__c = 'Skoda';
        p.name = 'Model';
        insert p;                       
        list<VGA_Service_Campaigns__c>serviceList = new list<VGA_Service_Campaigns__c>();
        VGA_Service_Campaigns__c s1 = new VGA_Service_Campaigns__c();
        s1.VGA_SC_Status__c = 'Completed';
        s1.VGA_SC_Campaign_Type__c = 'Tiguan Allspace Passenger Airbag Recall';
        s1.Campaign_Code__c = '69W9';
        s1.VGA_SC_Vehicle__c = a.id;
        s1.VGA_SC_Description__c = 'test';
        s1.VGA_SC_Brand__c = 'Volkswagen';
        s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'WV1ZZZ7JZBX012828';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
        serviceList.add(s1);
        insert serviceList;
        //  List<SelectOption> selOpts=VGA_RecallDetailsController.getselectOptions();
        //List<Schema.PicklistEntry> templist =s1.values;
        
       // VGA_RecallDetailsController.PrepareINString('s');
        
       // VGA_RecallDetailsController.UpdateAllStatus(serviceList);
        VGA_RecallDetailsController.getStates();
        VGA_RecallDetailsController.getyears();
        // VGA_RecallDetailsController.UpdateStatus(serviceList);
        
        VGA_Service_Campaigns__c c= new VGA_Service_Campaigns__c();
        //insert c;
        List <string>  lstrecordid = new List <string>();
        string brand = 'Volkswagen';
        string objApiName  = 'VGA_Service_Campaigns__c'; 
        string contrfieldApiName  = 'VGA_SC_Campaign_Type__c';
        string depfieldApiName = 'Filter_criteria__c';
        lstrecordid.add(s1.id);
        VGA_RecallDetailsController.UpdateStatus(lstrecordid);
        VGA_RecallDetailsController.getModelAliasList(brand);
       // VGA_RecallDetailsController.getselectOptions(s1,fid);
        VGA_RecallDetailsController.getDependentOptionsImpl(objApiName,contrfieldApiName,depfieldApiName);
        VGA_RecallDetailsController.QueryFieldsWrapper wrp = new VGA_RecallDetailsController.QueryFieldsWrapper();
        VGA_RecallDetailsController.PicklistEntryWrapper wrp1 = new VGA_RecallDetailsController.PicklistEntryWrapper();
        
        
        wrp.Brand='TEST';
        wrp.Criteria='AND';
        wrp.ModalYear='2018';
        wrp.ModelAlias='TEST';
        wrp.States='TX';
        wrp.Cmptype = 'test';
        wrp.Cmpcode = 'test';
        VGA_RecallDetailsController.getRecalllist(JSON.serialize(wrp),false);
        wrp1.active='true';
        wrp1.defaultValue = 'test';
        wrp1.label = 'test';
        wrp1.value = 'test';
        wrp1.validFor = 'test';
        
    }
    @isTest
    public static void Test1()
    {
        
        
    }
}