/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VGA_EmailMessageTriggerHandlerTracker 
{
    public static EmailMessage objEmailMessage;
    public static EmailMessage objEmailMessage2;
   
    public static Case objCase;
   
    
    static testMethod void myUnitTest4() 
    {
        LoadData2();
        // TO DO: implement unit test
        test.startTest();
            insert objEmailMessage;
        test.stopTest();    
    }
     static testMethod void myUnitTest() 
    {
        LoadData();
        // TO DO: implement unit test
        test.startTest();
            insert objEmailMessage;
        system.debug('email****************************'+objEmailMessage);
        test.stopTest();    
    }
    static testMethod void myUnitTest2() 
    {
        LoadData1();
        // TO DO: implement unit test
        test.startTest();
            insert objEmailMessage2;
        test.stopTest();    
    }
     static testMethod void myUnitTest3() 
    {
        LoadData1();
        // TO DO: implement unit test
        test.startTest();
         insert objEmailMessage2;
        update objEmailMessage2;
        delete objEmailMessage2;
        test.stopTest();    
    }
    static testMethod void myUnitTest5() 
    {
            VGA_Triggers__c objCustom1 = new VGA_Triggers__c();
        objCustom1.Name = 'EmailMessage';
        objCustom1.VGA_Is_Active__c= true;
        insert objCustom1;
        
     case   objCase1 = new Case();
        objCase1.Status = 'New';
        objCase1.Subject = 'TEST';
        objCase1.Origin = 'Mail';
        objCase1.VGA_Omni_Queue_Name__c='Omni Channel Queue';
       insert objCase1;
    EmailMessage objEmailMessage4 = new EmailMessage();
        objEmailMessage4.FromAddress  = 'TEST@TEST.COM';
        objEmailMessage4.Incoming  = true;
        objEmailMessage4.ToAddress = 'rishi.patel@saasfocus.com,success@test.com';
        objEmailMessage4.CcAddress = 'rishi.patel@saasfocus.com,success@test.com';
        objEmailMessage4.subject = 'Test';
        objEmailMessage4.TextBody = '12345';
        objEmailMessage4.ParentId = objCase1.id; 
        insert objEmailMessage4;
    }
    public static void LoadData()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'EmailMessage';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        objCase = new Case();
        objCase.Status = 'Closed';
        objCase.Subject = 'TEST';
        objCase.Origin = 'Mail';
        objCase.VGA_Omni_Queue_Name__c='Omni Channel Queue';
       // objCase.VGA_No_of_Incoming_Mails__c  =1;
        insert objCase;
       // objCase.Status = 'Closed';
        // objCase.VGA_No_of_Incoming_Mails__c  =1;
       // update objCase;      
        
        case objCase1 = [select id,closedDate,Status,subject from Case where id =:objCase.Id]; 
          string sch = 'rishi.patel@saasfocus.com,ganesh.mupparapu@katzion.com';
        objEmailMessage = new EmailMessage();
        objEmailMessage.FromAddress  = 'TEST@TEST.COM';
        objEmailMessage.Incoming  = true;
      // objEmailMessage.ToAddress = 'rishi.patel@saasfocus.com;success@test.com';
       objEmailMessage.ToAddress = 'rishi.patel@saasfocus.com';
      //  objEmailMessage.CcAddress = 'rishi1.patel@saasfocus.com';
        objEmailMessage.subject = 'Test';
        objEmailMessage.TextBody = '12345';
        objEmailMessage.ParentId = objCase1.id; 
        
        
    }
     public static void LoadData2()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'EmailMessage';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        objCase = new Case();
        objCase.Status = 'New';
        objCase.Subject = 'TEST';
        objCase.Origin = 'Mail';
        objCase.VGA_Omni_Queue_Name__c='Omni Channel Queue';
        insert objCase;
       objCase.Status = 'Closed';
      // update objCase;      
        
        case objCase1 = [select id,closedDate,Status,subject from Case where id =:objCase.Id]; 
        
        objEmailMessage = new EmailMessage();
        objEmailMessage.FromAddress  = 'TEST@TEST.COM';
       // objEmailMessage.FromName = 'Ganesh';
        objEmailMessage.Incoming  = true;
        objEmailMessage.ToAddress = 'rishi.patel@saasfocus.com';
        objEmailMessage.subject = 'Test';
        objEmailMessage.TextBody = '12345';
        objEmailMessage.ParentId = objCase1.id; 
        
        
    }
     public static void LoadData1()
    {
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'EmailMessage';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        objCase = new Case();
        objCase.Status = 'closed';
        objCase.Subject = 'itineraryTEST';
        objCase.Origin = 'Mail';
        objCase.VGA_Omni_Queue_Name__c='Omni Channel Queue';
        insert objCase;
      // objCase.Status = 'Closed';
        //update objCase;      
        
        case objCase1 = [select id,closedDate,Status,subject from Case where id =:objCase.Id]; 
        
        objEmailMessage2 = new EmailMessage();
        objEmailMessage2.FromAddress  = 'TEST@TEST.COM';
        objEmailMessage2.Incoming  = false;
        objEmailMessage2.ToAddress = 'rishi.patel@saasfocus.com';
        objEmailMessage2.subject = 'Test';
        objEmailMessage2.TextBody = '12345';
        objEmailMessage2.ParentId = objCase1.id; 
        
        
    }
    
}