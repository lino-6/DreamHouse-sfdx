/**
* @author HIJITH NS
* @date 04/04/2019
* @description Test class to test VGA_LeadDetailAPI.
*/
@isTest
public class VGA_LeadDetailsAPITest {
 

    // test method test api calls with valid lead id   
 @isTest
    public static void test01_ValidLeadIDNInvalidID()
    {
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        objAcc.VGA_Timezone__c = 'Australia/Sydney';
        objAcc.VGA_Dealer_Code__c = '1234';
        objAcc.billingstate='ACT';
        update objAcc;
        Lead objLead = new Lead();
        objLead.LastName = 'TestLeadLastName';
        objLead.FirstName = 'TestLeadFirstName';
        objLead.Email = 'TestEmail@test.com';
        objLead.MobilePhone = '5555666622';
        objLead.VGA_Model_of_Interest__c='POLO';
        objLead.VGA_Dealer_Account__c=objAcc.Id;
        objLead.VGA_Test_Drive_Start_Date__c=System.now();
        Insert objLead;
        String LeadID =  objLead.id;
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse(); 
        req.addParameter('id', LeadID);
        req.requestURI = '/services/apexrest/getLeadDetails';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        VGA_LeadDetailAPI.getLeadByID();
        Test.stopTest();
         System.assertEquals(200,((VGA_LeadDetailAPI.LeadResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_LeadDetailAPI.LeadResponseWrapper.class)).statuscode);
        
    }
     // test method test api calls with invalid lead id 
      @isTest
    public static void test01_ValidLeadInvalidID()
    {
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse(); 
        req.addParameter('id', '55555');
        req.requestURI = '/services/apexrest/getLeadDetails';
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        VGA_LeadDetailAPI.getLeadByID();
        Test.stopTest();
                
         System.assertEquals(500,((VGA_LeadDetailAPI.LeadResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_LeadDetailAPI.LeadResponseWrapper.class)).statuscode);
        
    }
    
 // test method test api calls with null lead id 
     @isTest
    public static void test01_ValidLeadNullID()
    {
        
        Test.startTest();
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse(); 
        req.requestURI = '/services/apexrest/getLeadDetails';
        req.addParameter('id', '');
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        VGA_LeadDetailAPI.getLeadByID();
        Test.stopTest();
        System.assertEquals(500,((VGA_LeadDetailAPI.LeadResponseWrapper)JSON.deserialize(res.responseBody.toString(),VGA_LeadDetailAPI.LeadResponseWrapper.class)).statuscode);
        
    }

}