/*
* @author       : MOHAMMED ISHAQUE SHAIKH
* @description  : To Test Attachment Trigger Handler
* @Modified Date: 2019-04-08
*/

@isTest
private class VGA_AttachmentTriggerHandlerTracker{


    @testSetup
    static void buildConfigList() {
        List<VGA_Triggers__c> CustomTriggerList=new List<VGA_Triggers__c>{VGA_CommonTracker.buildCustomSettingTrigger('Attachment',true),VGA_CommonTracker.buildCustomSettingTrigger('Case',true)};
        insert CustomTriggerList;
        Recall_DocuSign_Settings__c Recall=VGA_CommonTracker.buildRecallDocusignSettings();
        insert Recall;
    }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 09-04-2019)
    * @description Method that tests updateLeadConsentCheck method on the VGA_AttachmentTriggerHandler class              
    *  Attachment name and parent id is checked to populate VGA_Consent_Form_Attached__c Field in Lead ,
    *   On INSERT Operation
    *   if parentId is type lead  and name == Consent_Form excluding extention then we will update related VGA_Consent_Form_Attached__c=true  in lead Object 
    *   on DELETE Operation
    *   if we deleting the consent_from attachement from lead then we will be updating the  VGA_Consent_Form_Attached__c field to false
    */

    @isTest 
    public static void test_updateLeadConsentCheck(){
        Account objAccount = VGA_CommonTracker.createDealerAccount();
        Contact objContact = VGA_CommonTracker.createDealercontact(objAccount.Id);
        USER objUser = VGA_CommonTracker.CreateUser(objContact.id);
        List<String> FileNameList=new List<String>{'Consent_Form','Consent_Form.txt','Consent_Form.txt.pdf','fggfg.pdf','Consent_Formdsd','Consent_Formsdsd','Consent_Form.jpg'};
        Test.startTest();
        Lead ld=VGA_CommonTracker.buildLead('TestLead');
        insert ld;
        List<Attachment> AttList=new List<Attachment>();
        for(String t:FileNameList){
            Attachment temp=VGA_CommonTracker.buildAttachment(ld.Id,t);
            AttList.add(temp);
        }
        insert AttList;
        System.assertEquals(true,[select id, VGA_Consent_Form_Attached__c from Lead limit 1].VGA_Consent_Form_Attached__c);
        delete AttList;
        System.assertEquals(false,[select id, VGA_Consent_Form_Attached__c from Lead limit 1].VGA_Consent_Form_Attached__c);
        Test.stopTest();
    }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 09-04-2019)
    * @description Method that tests docusignAttachment method on the VGA_AttachmentTriggerHandler class              
    *   On Insert Operation 
    *   This method will generate Docusign Attachement.Also we will be populating the markers with lead Data in the attachment.
    *   in the end  we will mailling document to email Id  specified in OrgwideAdresss for specified value Custom Label(VGA_VW_Emails_Academy)      
    */
    @isTest 
    public static void test_docusignAttachment(){
        ID recallRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Recall Refused').getRecordTypeId();
        VGA_Service_Campaigns__c s=VGA_CommonTracker.buildTakataServiceCampaing();
        insert s;
        Case objCase = VGA_CommonTracker.createCase(); 
        objCase.VW_Service_Campaign_VIN__c = s.id;
        objCase.RecordTypeId = recallRTID;
        objCase.Origin = 'Dealer CRM VW';
        objCase.DocuSign_Recall_First_Name__c = 'Tester';   
        objCase.DocuSign_Recall_Last_Name__c = 'Tester';
        objCase.VGA_DocuSign_Recall_Email__c = 'a@a.com';
        objCase.VGA_DocuSign_Recall_Contact_No__c = 'dddd';
        objCase.VGA_DocuSign_Recall_VIN_No__c = '12345';
        insert objCase;
        dsfs__DocuSign_Status__c d = new dsfs__DocuSign_Status__c();
        d.dsfs__DocuSign_Envelope_ID__c = 'testid';
        d.dsfs__Case__c = objCase.id;
        Insert d;
        Attachment objAttachment = VGA_CommonTracker.buildAttachment(d.id,'Unit Test Attachment');
        insert objAttachment;
        System.assertEquals(2,[Select id from  Attachment].size());

    }
}