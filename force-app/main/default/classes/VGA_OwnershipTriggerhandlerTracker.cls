//=================================================================================
// Tracker class for VGA_OwnershipTriggerhandler class.
// =================================================================================
// Created by Nitisha Prasad on 27-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_OwnershipTriggerhandlerTracker
{
    
    public static VGA_Ownership__c objOwner;
     public static VGA_Ownership__c objOwner1;
    public static VGA_Ownership__c objOwner2;
     public static VGA_Ownership__c objOwner3;
    public static VGA_Vehicle_Policy__c objVehicle;
    public static VGA_Allianz_Data_Mapping__c objData;
    
    static testMethod void myUnitTest2() 
    {      
        LoadData(); 
        Test.startTest();
        
        objOwner.VGA_Purchase_date__c    = Date.today().addDays(1);
        
        update objOwner1;

        Test.stopTest();
    } 
    public static void LoadData() 
    { 
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'VGA_Ownership__c';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
        
        Account objAcc = VGA_CommonTracker.createDealerAccount();
        
        objVehicle = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c = 'Volkswagen';
        objVehicle.VGA_Sub_Brand__c = 'Passenger Vehicles (PV)';
        insert objVehicle; 
        
        Asset objAsset = VGA_CommonTracker.createAsset('test');
        objAsset.VGA_VIN__c = 'WVWZZZAUZJP002582';
        objAsset.AccountId = objAcc.id;
        insert objAsset;
        
        objOwner = new VGA_Ownership__c();
        objOwner.VGA_Owner_Name__c = objAcc.id;
        objOwner.VGA_Policy_Code__c = objVehicle.id;
        objOwner.VGA_Purchase_Date__c = Date.today();
        objOwner.VGA_Type__c = 'Previous Owner';
        objOwner.VGA_VIN__c = objAsset.Id;
        insert objOwner;  
        
         objOwner1 = new VGA_Ownership__c();
        objOwner1.VGA_Owner_Name__c = objAcc.id;
        objOwner1.VGA_Policy_Code__c = objVehicle.id;
        objOwner1.VGA_Purchase_Date__c = Date.today().adddays(91);
        objOwner1.VGA_Type__c = 'Previous Owner';
        objOwner1.VGA_VIN__c = objAsset.Id;
        objOwner1.VGA_Previous_Owner__c = objOwner.Id;
        insert objOwner1;
        
        objOwner2 = new VGA_Ownership__c();
        objOwner2.VGA_Owner_Name__c = objAcc.id;
        objOwner2.VGA_Policy_Code__c = objVehicle.id;
        objOwner2.VGA_Purchase_Date__c = Date.today();
        objOwner2.VGA_Type__c = 'current Owner';
        objOwner2.VGA_VIN__c = objAsset.Id;
        insert objOwner2;  
        
         objOwner3 = new VGA_Ownership__c();
        objOwner3.VGA_Owner_Name__c = objAcc.id;
        objOwner3.VGA_Policy_Code__c = objVehicle.id;
        objOwner3.VGA_Purchase_Date__c = Date.today();
        objOwner3.VGA_Type__c = 'current Owner';
        objOwner3.VGA_VIN__c = objAsset.Id;
        objOwner3.VGA_Previous_Owner__c = objOwner.Id;
        insert objOwner3;
        
        VGA_Allianz_Data_Mapping__c objData = new VGA_Allianz_Data_Mapping__c();
        objData.VGA_Asset_Account__c = objAcc.id;
        objData.VGA_Vehicle_Policy_ID__c = objVehicle.id;
        insert objData;
    } 
}