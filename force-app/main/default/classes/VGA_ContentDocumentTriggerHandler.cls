public with sharing class VGA_ContentDocumentTriggerHandler 
{
   public void runTrigger()
   {
        // Method will be called to handle Before Update events
        if(Trigger.isBefore && Trigger.isDelete)
        {
           onBeforeDelete((List<ContentDocument >) trigger.new,(Map<Id, ContentDocument>) trigger.OldMap);
        }
    }
    public void onBeforeDelete(List<ContentDocument> lstTriggerNew,Map<Id,ContentDocument> mapTriggerOld)
    {
    	deleteValidation(lstTriggerNew,mapTriggerOld);	
    }
    private static void deleteValidation(List<ContentDocument> lstTriggerNew,map<id,ContentDocument> triggeroldmap)
    {
    	
    	if(triggeroldmap != null && !triggeroldmap.isEmpty())
    	{
    		Id profileId = UserInfo.getProfileId();
        	String profileName = [Select Name from Profile where Id =:profileId limit 1].Name; 
        	
    		for(ContentDocument objContentDocument : triggeroldmap.values())
    		{
    			if(profileName =='VGA Academy Support Team Profile')
    				objContentDocument.addError('You can\'t Delete this File');	
    		}
    	}
    } 
}