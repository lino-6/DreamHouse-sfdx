@isTest
public class VGA_BBOBatchSchedulerTracker {    
    
    @isTest
    public static void test1()
    {    
            Datetime dt = system.now().addSeconds(20);           
            String day = string.valueOf(dt.day());
            String month = string.valueOf(dt.month());
            String hour = string.valueOf(dt.hour());
            String minute = string.valueOf(dt.minute());
            String second = string.valueOf(dt.second());
            String year = string.valueOf(dt.year());
            
            String strJobName = 'BBO-Batch-Processing-' + String.valueof(dt);
            String strSchedule = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        Test.startTest();
            System.schedule(strJobName,strSchedule, new VGA_BBOBatchScheduler());
        Test.stopTest();
        
    }

}