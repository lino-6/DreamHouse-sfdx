//Tracker class for  VGA_dependentPicklist
//===============================================================================================
//   Name                  Version        Date 
//===============================================================================================
// Surabhi Ranjan             1.0          8-Nov-2017 
//===============================================================================================
//Coverage for  VGA_dependentPicklist on 				8-Nov-2017		90%
//===============================================================================================
@isTest
public class VGA_dependentPicklistTracker 
{
    public static testmethod void unittest1()
    {
        VGA_dependentPicklist.getFieldDependencies('Account','VGA_Brand__c', 'VGA_Sub_Brand__c');
    }

}