public with sharing class VGA_VehicleTriggerHandler
{
  public void runTrigger()
  {
     // Method will be called to handle Before Insert events
    if(Trigger.isBefore && Trigger.isInsert)
    {
        onBeforeInsert((List<Asset>) trigger.new);
    }
    if (Trigger.isAfter && Trigger.isUpdate)
    {
        onAfterUpdate((List<Asset>) trigger.new, (Map<Id, Asset>) trigger.OldMap); 
    }
  }
    public void onBeforeInsert(List<Asset> lstTriggerNew)
    {
        UpdateModelName(lstTriggerNew);  
    }
    public void onAfterUpdate(List<Asset> lstTriggerNew, Map<Id,Asset> triggeroldmap)
    {
        //updateAllianzLastModified(lstTriggerNew,triggeroldmap);
    }
  private static void UpdateModelName(list<Asset> lstTriggerNew)
  {
     for(Asset objAsset :lstTriggerNew)
     {
        if(objAsset.Product2Id !=Null && objAsset.VGA_Model__c ==Null)
        {
          objAsset.VGA_Model__c = objAsset.Product2Id;
        }
        else if(objAsset.Product2Id ==Null && objAsset.VGA_Model__c !=Null)
        {
         objAsset.Product2Id = objAsset.VGA_Model__c;
        }
     }
  }
  /*
  private static void updateAllianzLastModified(list<Asset> lsttriggernew,map<Id,Asset> triggeroldmap)
  {
    if(lsttriggernew != null && !lsttriggernew.isEmpty())
    {
        
        set<String> fieldSet = new Set<String>();
        set<string> setAssetID = new set<string>();
        for(Schema.FieldSetMember fields :Schema.SObjectType.Asset.fieldSets.getMap().get('VGA_Allianz_Asset_Field_Set').getFields())
        {
            fieldSet.add(fields.getFieldPath());
        }
        for(Asset a : lsttriggernew)
        {
            set<String> changedFieldSet  = new Set<String>();
            for(string s: fieldSet)
            {
                if(a.get(s) != trigger.oldMap.get(a.Id).get(s))
                {
                    changedFieldSet.add(s);
                }
                if(changedFieldSet != null && !changedFieldSet.isEmpty())
                {
                    setAssetID.add(a.id);   
                }
            }
        }
        if(setAssetID != null && !setAssetID.isEmpty())
        {
            uppdateAllianzRecords(setAssetID);
        }
        
    }
  }
    @future
    private static void uppdateAllianzRecords(set<string> setAssetID)
    {
        if(setAssetID != null && !setAssetID.isEmpty())
        {
            list<VGA_Allianz_Data_Mapping__c> lstADM = [select id,VGA_VIN__c from VGA_Allianz_Data_Mapping__c where 
                                                        VGA_VIN__c in:setAssetID];
            {
                if(lstADM != null && !lstADM.isEmpty())
                {
                    for(VGA_Allianz_Data_Mapping__c objADM : lstADM)
                    {
                        objADM.VGA_Last_Modified_Date_Time__c = system.now();
                    }
                    update lstADM;
                }
            } 
        }
    }
    */
}