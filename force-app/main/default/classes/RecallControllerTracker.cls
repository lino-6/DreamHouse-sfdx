@isTest
public class RecallControllerTracker 
{
    
    public static void loaddata()
    {
        
        Product2 p = new Product2();
        p.VGA_Brand__c = 'PV Volkswagen';
        p.name = 'Model';
        insert p;               
        
        Account acc = new Account();
        acc.lastName = 'Lastname';
        acc.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        insert acc;
        
        Asset a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'WV1ZZZ7JZBX012828';
        insert a;
        
        p = new Product2();
        p.VGA_Brand__c = 'Skoda';
        p.name = 'Model';
        insert p;                       
        
        VGA_Service_Campaigns__c s1 = new VGA_Service_Campaigns__c();
        s1.VGA_SC_Status__c = 'Completed';
        s1.VGA_SC_Campaign_Type__c ='Takata Airbag Recall';
        s1.VGA_SC_Vehicle__c = a.id;
        s1.VGA_SC_Description__c = 'test';
        s1.VGA_SC_Brand__c = 'Volkswagen';
        s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'WV1ZZZ7JZBX012828';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
        s1.recordtypeid = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();           
        insert s1;
        
        a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'TMBKM6NJ7HZ102744';
        insert a;          
        
        s1 = new VGA_Service_Campaigns__c();
        s1.VGA_SC_Status__c = 'Completed';
        s1.VGA_SC_Campaign_Type__c ='Takata Airbag Recall';
        s1.VGA_SC_Vehicle__c = a.id;
        s1.VGA_SC_Description__c = 'test';
        s1.VGA_SC_Brand__c = 'Skoda';
        s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'TMBKM6NJ7HZ102744';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
        s1.recordtypeid = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();            
        insert s1; 
        
        a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'TMBKM6NJ7HZ102700';
        insert a;          
        
        s1 = new VGA_Service_Campaigns__c();
        s1.VGA_SC_Status__c = 'Future';
        s1.VGA_SC_Campaign_Type__c ='Takata Airbag Recall';
        s1.VGA_SC_Vehicle__c = a.id;
        s1.VGA_SC_Description__c = 'test';
        s1.VGA_SC_Brand__c = 'Skoda';
        s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'TMBKM6NJ7HZ102700';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
        s1.recordtypeid = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();            
        insert s1; 
        
        a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'TMBKM6NJ7HZ102600';
        insert a;          
        
        s1 = new VGA_Service_Campaigns__c();
        s1.VGA_SC_Status__c = 'Scheduled';
        s1.VGA_SC_Campaign_Type__c ='Takata Airbag Recall';
        s1.VGA_SC_Vehicle__c = a.id;
        s1.VGA_SC_Description__c = 'test';
        s1.VGA_SC_Brand__c = 'Skoda';
        s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'TMBKM6NJ7HZ102600';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
        s1.recordtypeid = Schema.SObjectType.VGA_Service_Campaigns__c.getRecordTypeInfosByName().get('Takata').getRecordTypeId();            
        insert s1;         
        
    }
    
    @isTest
    public static void Test1()
    {
        loaddata();
        
        RecallController rc = new RecallController();
        rc.brand = 'Volkswagen';
        rc.vin = 'WV1ZZZ7JZBX012828';
        
        
        String nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage); 
        
        rc.checkVinStatus();
    }
    
    @isTest
    public static void Test2()
    {
        RecallController rc = new RecallController();
        rc.brand = 'Volkswagen';
        rc.vin = 'invalid';
        
        
        String nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);   
        rc.checkVinStatus();
        
        
        rc = new RecallController();
        rc.brand = 'Skoda';
        rc.vin = 'invalid';
        
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        rc.checkVinStatus();        
    }  
    
    @isTest
    public static void Test3()
    {
        loaddata();
        
        RecallController rc = new RecallController();
        rc.brand = 'Skoda';
        rc.vin = 'TMBKM6NJ7HZ102744';
        
        
        String nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);   
        rc.checkVinStatus();
                
        rc = new RecallController();
        rc.brand = 'Skoda';
        rc.vin = 'TMBKM6NJ7HZ102700';
        
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);   
        rc.checkVinStatus();        
        
        rc = new RecallController();
        rc.brand = 'Skoda';
        rc.vin = 'TMBKM6NJ7HZ102600';
        
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);
        
        nextPage = rc.submit().getUrl();
        System.debug(nextPage);   
        rc.checkVinStatus();         
        
    }    

}