//=================================================================================
// Tracker class for VGA_scheduleLeadEscalation class.
// =================================================================================
// Created by Nitisha Prasad on 22-Nov-2017.
// =================================================================================
@isTest(seeAllData = false)
private class VGA_scheduleLeadEscalationTracker
{
    public static VGA_scheduleLeadEscalation sh1;
    
    static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        sh1 = new VGA_scheduleLeadEscalation();
        Test.startTest();
        String sch = '0 0 23 * * ?'; 
        System.schedule('Test Territory Check', sch, sh1); 
        Test.stopTest();
    } 
}