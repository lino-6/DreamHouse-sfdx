/**
* @author Mohammed Ishaque Shaikh
* @date 05/Feb/2019
* @description Test method for the class VGA_LeadComponentController.
*              It tests the getloggedInUserDetails  method that returns information about .
*/
@isTest
public class VGA_LeadComponentControllerTest {
    
    public static Account objAccount;
    public static Contact objContact;
    public static User objUser; 
    
    @TestSetup
    public static void basicSetup(){
        VGA_Triggers__c objCustom = new VGA_Triggers__c();
        objCustom.Name = 'Contact';
        objCustom.VGA_Is_Active__c= true;
        insert objCustom;
    }
    
/**
* @author Mohammed Ishaque Shaikh
* @date 05/Feb/2019
* @description This is a test class for apex method getloggedInUserDetails 
* @description where this test method is for the current user role as Consultant
*/
    
    @isTest
    public static void test1_GetLoggedInUserInfo(){

        Account  objAccount = VGA_CommonTracker.createDealerAccount();
        Contact objContact = VGA_CommonTracker.buildDealerContact(objAccount.id,'aaa');
        objContact.VGA_Role__c ='Consultant';
        insert objContact;
        User objUser = VGA_CommonTracker.CreateUser(objContact.id);
        system.runAs(objUser){
            System.assertEquals('Consultant', VGA_LeadComponentController.getloggedInUserDetails().Role);
        }

    }
/**
* @author Mohammed Ishaque Shaikh
* @date 05/Feb/2019
* @description This is a test class for apex method getloggedInUserDetails 
* @description where this test method is for the current user role as Consultant
*/

	@isTest
    public static void test2_GetLoggedInUserInfo(){
        Account  objAccount = VGA_CommonTracker.createDealerAccount();
        Contact objContact = VGA_CommonTracker.buildDealerContact(objAccount.id,'aaa');
        objContact.VGA_Role__c ='Dealer Administrator';
        insert objContact;
        User objUser = VGA_CommonTracker.CreateUser(objContact.id);
        system.runAs(objUser){
            System.assertEquals('Dealer Administrator', VGA_LeadComponentController.getloggedInUserDetails().Role);
        }
    }
    

}