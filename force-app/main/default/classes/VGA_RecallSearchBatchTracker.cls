@isTest 
public class VGA_RecallSearchBatchTracker 
{
    static testMethod void testMethod1() 
    {
        List<VGA_Service_Campaigns__c> lstservice= new List<VGA_Service_Campaigns__c>();
        //for(Integer i=0 ;i <200;i++)
        //{
          
           Product2 p = new Product2();
        p.VGA_Brand__c = 'PV Volkswagen';
        p.name = 'Model';
        insert p;  
        
           Account acc = new Account();
        acc.lastName = 'Lastname';
        acc.recordtypeid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Individual Customer').getRecordTypeId();
        insert acc;
          
           Asset a = new Asset();
        a.name = 'Vehicle';
        a.AccountId = acc.id;
        a.Nevdis_Write_Off_Status__c = 'I';
        a.VGA_Model__c = p.id;
        a.VGA_VIN__c = 'WV1ZZZ7JZBX012828';
        insert a;
            VGA_Service_Campaigns__c s1= new VGA_Service_Campaigns__c();
            //s1.Name ='Name'+i;
            s1.VGA_SC_Status__c = 'Completed';
            s1.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
            s1.VGA_SC_Vehicle__c = a.id;
            s1.VGA_SC_Description__c = 'test';
            s1.VGA_SC_Brand__c = 'Volkswagen';
            s1.VGA_SC_Sub_Brand__c = 'test';
        s1.VGA_SC_Vin__c = 'WV1ZZZ7JZBX012828';
        s1.Name__c = 'Test';
        s1.VGA_SC_Code__c = '69Q7'; 
            lstservice.add(s1);
        //}
        
        insert lstservice;
        
        Test.startTest();
        

            VGA_RecallSearchBatch  obj = new VGA_RecallSearchBatch('select id,VGA_SC_Status__c from VGA_Service_Campaigns__c limit 1');
            DataBase.executeBatch(obj); 
            
        Test.stopTest();
    }
}