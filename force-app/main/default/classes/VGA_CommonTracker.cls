@isTest
public class VGA_CommonTracker
{
    public static Lead createLead()
    {
        Lead objLead             = New Lead();
        objLead.FirstName        ='TestLeadFirst';
        objLead.LastName         ='TestLeadLastName';
        objLead.Company          ='TestCompany';
        objLead.Status           ='New';
        objLead.VGA_Model_of_Interest__c = 'test';
        
        insert objLead ;
        return objLead ;   
    }

    /**
     * This method builds test lead with given namePrefix
     * And returns the built record
     */
    public static Lead buildLead(String namePrefix) {

        Lead newLead = new Lead();
        newLead.FirstName        = namePrefix + 'First';
        newLead.LastName         = namePrefix + 'LastName';
        newLead.Company          ='TestCompany';
        newLead.Status           ='New';
        newLead.VGA_Model_of_Interest__c = 'test';
        newLead.Email            = newLead.FirstName + '.' + newLead.LastName + '@test.com';
        
        return newLead;
    }

    /**
     * This method builds the given number of leads with given namePrefix
     * And returns the list of built records
     */
    public static List<Lead> buildLeads(String namePrefix, Integer numberOfRecords) {
        List<Lead> leads = new List<Lead>();

        for(Integer i=0; i<numberOfRecords; i++) {
            leads.add(buildLead(namePrefix+i));
        }
        return leads;
    }

    public static Account createDealerAccount()
    {
        Id idgetDealerAccount = VGA_Common.GetRecordTypeId('Account','Volkswagen Dealer Account');
        Account objAccount =New Account();
        objAccount.Name ='TestDealer3232';
        objAccount.RecordTypeId=idgetDealerAccount;
        objAccount.VGA_Dealer_Code__c='123432';
        objAccount.VGA_Brand__c='Volkswagen';
        objAccount.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
        insert objAccount;  
        return objAccount;
    }

    public static Account buildDealerAccount(String namePrefix, String dealerCode)
    {
        Id idgetDealerAccount = VGA_Common.GetRecordTypeId('Account','Volkswagen Dealer Account');
        Account objAccount            = new Account();
        objAccount.Name               = namePrefix;
        objAccount.RecordTypeId       = idgetDealerAccount;
        objAccount.VGA_Dealer_Code__c = dealerCode;
        objAccount.VGA_Brand__c       = 'Volkswagen';
        objAccount.VGA_Sub_Brand__c   = 'Passenger Vehicles (PV)';

        return objAccount;
    }

    public static Account createAccount()
    {
        Id idgetDealerAccount = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Volkswagen Fleet Customer').getRecordTypeId();
        Account objAccount = New Account();
        objAccount.Name ='TestDealer3232';
        objAccount.RecordTypeId=idgetDealerAccount;
        //objAccount.VGA_Dealer_Code__c='123432';
        objAccount.VGA_Brand__c='Volkswagen';
        objAccount.VGA_Sub_Brand__c='Passenger Vehicles (PV)';
        insert objAccount;  
        return objAccount;
    }

    public static Account buildPersonAccount(String recordTypeName, String lastName){
        Id personAccountRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
    
        Account personAccount = new Account();
        personAccount.RecordTypeId = personAccountRT;
        personAccount.LastName     = lastName;
        personAccount.PersonEmail  = lastName + '@test.com';

        return personAccount;
    }

    public static VGA_Dealership_Profile__c createDealershipProfile( string AccountId)
    {
        VGA_Dealership_Profile__c objDealershipProfile =New VGA_Dealership_Profile__c();
        objDealershipProfile.VGA_Dealership__c          = AccountId;
        objDealershipProfile.VGA_Brand__c               = 'Volkswagen';
        objDealershipProfile.VGA_Sub_Brand__c           = 'Passenger Vehicles (PV)';
        objDealershipProfile.Name                       = 'Passenger Vehicles (PV)';
        objDealershipProfile.VGA_Distribution_Method__c = 'Round Robin';
        objDealershipProfile.VGA_Escalate_Minutes__c    = 999;
        insert objDealershipProfile;
        return objDealershipProfile;  
    }
    public static VGA_Trading_Hour__c createTrading_Hour( string DealershipProfileId)
    {
        VGA_Trading_Hour__c objTrading =New VGA_Trading_Hour__c();
        
        objTrading.VGA_Dealership_Profile__c= DealershipProfileId;
        objTrading.VGA_Working__c           =true;
        objTrading.Name=system.now().format('EEEE');
        objTrading.VGA_End_Time__c='07:00 PM';
        objTrading.VGA_Start_Time__c='07:00 AM';
        
        
        insert objTrading;
        return objTrading; 
    }
    
    public static void createWeekTradingHours(string DealershipProfileId)
    {
        List<VGA_Trading_Hour__c> listOfTradingHours = new List<VGA_Trading_Hour__c>();
        Set<String> weekNameSet = new Set<String>{'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};

        for(String weekName : weekNameSet)
        {
            VGA_Trading_Hour__c objTrading =New VGA_Trading_Hour__c();
            objTrading.VGA_Dealership_Profile__c= DealershipProfileId;
            objTrading.VGA_Working__c           = weekName == 'Sunday' ? false : true;
            objTrading.Name                     = weekName;
            objTrading.VGA_End_Time__c          = '05:00 PM';
            objTrading.VGA_Start_Time__c        = '09:00 AM';

            listOfTradingHours.add(objTrading);
        }
        
        insert listOfTradingHours;
    }
    
    public static Contact createDealercontact(String AccountId)
     {
            Id idgetDealerContact = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact');
            Contact objContact =New Contact();
            objContact.FirstName ='TestDeal333er';
            objContact.LastName ='TestDeal33333er';
            objContact.RecordTypeId=idgetDealerContact;
            objContact.VGA_Role__c ='Consultant';
            objContact.VGA_Brand__c='Volkswagen';
            objContact.VGA_Sub_Brand__c='Passenger Vehicles (PV);Commercial Vehicles (CV)';
            objContact.AccountId=AccountId;
            objContact.VGA_Available_to_Receive_Leads__c=true;
            objContact.Email ='testt3444444444@live.com';
            objContact.MobilePhone='2111111111111111';
            //objContact.VGA_Available__c = true;
            insert objContact;  
            return objContact;
     }

     public static Contact buildDealerContact(String accountId, String namePrefix)
     {
        Id idgetDealerContact = VGA_Common.GetRecordTypeId('Contact','Volkswagen Dealer Contact');
        
        Contact objContact         = new Contact();
        objContact.FirstName       = namePrefix + 'FirstName';
        objContact.LastName        = namePrefix + 'LastName';
        objContact.RecordTypeId    = idgetDealerContact;
        objContact.VGA_Role__c     = 'Consultant';
        objContact.VGA_Brand__c    = 'Volkswagen';
        objContact.VGA_Sub_Brand__c= 'Passenger Vehicles (PV)';
        objContact.AccountId       = accountId;
        objContact.Email           = namePrefix + '@live.com';
        objContact.MobilePhone     = '2111111111111111';
        objContact.VGA_Available_to_Receive_Leads__c=true;

        return objContact;
     }

    public static Task createTask(String ContactId)
     {
            Task objTask =New Task();
            objTask.WhoId =ContactId;
            objTask.Status ='Open';
            objTask.Priority='Medium';
            insert objTask;  
            return objTask;
     }
    public Static User CreateUser(string ContactId)
    {
         Profile p = [SELECT Id FROM Profile WHERE Name='Volkswagen Dealer Consultant Profile' LIMIT 1];
         User u = new User();
            u.ContactId =ContactId;
            u.ProfileId = p.Id;
            u.username = 'tes2333t@test.com';
            u.FirstName = 'First';
            u.LastName = '23232';
            u.email = 'test@test.com';
            u.VGA_Brand1__c = 'Volkswagen';
            String alias = u.username;
            if(alias.length() > 5){
                alias = alias.substring(0,5);
            }
            u.alias = alias;
            u.languagelocalekey =' en_US';
            u.localesidkey = 'en_US';
            u.emailencodingkey = 'UTF-8';
            u.TimezoneSidKey = 'America/Los_Angeles';
            
            insert u;
       return u;
    }

    public Static User buildUser(string ContactId)
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Volkswagen Dealer Consultant Profile' LIMIT 1];
        User u = new User();
        u.ContactId =ContactId;
        u.ProfileId = p.Id;
        u.username = 'tes2333t@test.com' + String.valueOf(generateRandomInRange(10000, 90000));
        u.FirstName = 'First';
        u.LastName = '23232';
        u.email = 'test@test.com';
        u.VGA_Brand1__c = 'Volkswagen';
        String alias = u.username;
        if(alias.length() > 5){
            alias = alias.substring(0,5);
        }
        u.alias = alias;
        u.languagelocalekey =' en_US';
        u.localesidkey = 'en_US';
        u.emailencodingkey = 'UTF-8';
        u.TimezoneSidKey = 'America/Los_Angeles';

        return u;
    }

    public static VGA_Invalid_Email__c createInvalidEmail(string AccountId)
    {
       VGA_Invalid_Email__c objInvalid =New VGA_Invalid_Email__c ();
        objInvalid.VGA_Dealer_Code__c='1234';
        objInvalid.VGA_Brand__c='Volkswagen';
        objInvalid.VGA_First_Name__c='test';
        objInvalid.VGA_Last_Name__c='56672323';
        objInvalid.VGA_Invalid_Email__c=true;
        objInvalid.VGA_Dealer_Name__c =AccountId;
        insert objInvalid;
        return objInvalid;        
    }
    public static VGA_Dealer_Traffic_Header__c createDealerTrafficHeader(String AccountId)
    {
       VGA_Dealer_Traffic_Header__c objDealerTraffic =New VGA_Dealer_Traffic_Header__c();
        objDealerTraffic.VGA_Brand__c                ='Volkswagen';
        objDealerTraffic.VGA_Account__c              =AccountId;
        objDealerTraffic.VGA_Sub_Brand__c            ='Passenger Vehicles (PV)';
        insert objDealerTraffic;
        return objDealerTraffic;
        
    }
    public static VGA_Post_Code_Mapping__c createPostCodemapping(String skpDealerCode,String vwcDealerCode,String vwpDealerCode)
    {
        VGA_Post_Code_Mapping__c vga_post_code_mapping = new VGA_Post_Code_Mapping__c();
    
        vga_post_code_mapping.VGA_SKP_Dealer_Code__c = skpDealerCode != null?skpDealerCode:null;
        vga_post_code_mapping.VGA_VWC_Dealer_Code__c = vwcDealerCode != null?vwcDealerCode:null;
        vga_post_code_mapping.VGA_VWP_Dealer_Code__c = vwpDealerCode != null?vwpDealerCode:null;
        
        return  vga_post_code_mapping;
    }
    public static Product2 createProduct(string strProductName)
    {
        Product2 objProduct = new Product2();
        
        objProduct.name = strProductName != null?strProductName:'test';
        
        return objProduct;  
    }
    public static VGA_Profanity__c createProfanity(string profanityKeyword)
    {
        VGA_Profanity__c objProfanity = new VGA_Profanity__c();
        objProfanity.VGA_Is_Active__c=true;
        objProfanity.VGA_Keyword__c = profanityKeyword != null?profanityKeyword:null;
        
        return objProfanity;
    }
    public static VGA_Customer_Order_Details__c createCustomerOrderDetails()
    {
        VGA_Customer_Order_Details__c objCOD = new VGA_Customer_Order_Details__c();
        
        return objCOD;
    }
    public static VGA_User_Profile__c createUserProfile(string strName)
    {
        VGA_User_Profile__c objUserProfile = new VGA_User_Profile__c();
        objUserProfile.name = strName != null?strName:null;
        objUserProfile.VGA_Mobile__c='211111111111111';
        
        return objUserProfile;
    }
    public static VGA_Subscription__c createSubscription(ID userProfileID) 
    {
        VGA_Subscription__c objSubscription = new VGA_Subscription__c();
        objSubscription.VGA_User_Profile__c = userProfileID != null?userProfileID:null;
        
        return objSubscription;
    }
    public static VGA_Campaign_Activity_Header__c createCampaignActivityHeader()
    {
        VGA_Campaign_Activity_Header__c obj = new VGA_Campaign_Activity_Header__c();
        
        return obj;
    }
    public static VGA_Campaign_Activity_Detail__c createCampaignActivityDetail(ID CampaignActivityHeaderID)
    {
        VGA_Campaign_Activity_Detail__c obj = new VGA_Campaign_Activity_Detail__c();
        obj.VGA_Campaign_Activity_Header__c = CampaignActivityHeaderID;  
        return obj;
    }
     public static Case createCase()
    {
        Case objCase = new Case();
        
        return objCase;
    }
    public static Asset createAsset(string strAssetname)
    {
        Asset objAsset = new Asset();
        objAsset.name = strAssetname != null?strAssetname:'test';
        
        return objAsset;
    }
      public static VGA_Service_Campaigns__c  buildTakataServiceCampaing()
    {
        VGA_Service_Campaigns__c objservicecampaign = new VGA_Service_Campaigns__c();
        objservicecampaign.VGA_SC_Status__c = 'Future';
        objservicecampaign.VGA_SC_Code__c = '69Q7';
        objservicecampaign.Name__c = 'Takata';
        objservicecampaign.VGA_SC_Vin__c = '12345';
        objservicecampaign.VGA_SC_Sub_Brand__c = 'PV';
        objservicecampaign.VGA_SC_Brand__c = 'Volkswagen';
        objservicecampaign.VGA_SC_Description__c = 'Test';
        objservicecampaign.VGA_SC_Campaign_Type__c = 'Takata Airbag Recall';
        
        return objservicecampaign;
    }
    
    public static VGA_Pre_Launch_Configuration__c createPreLaunchConfig()
    {
        VGA_Pre_Launch_Configuration__c objPrelaunchConfiguration = new VGA_Pre_Launch_Configuration__c();
        objPrelaunchConfiguration.VGA_Number_of_Leads_per_Distribution__c = 1;
        objPrelaunchConfiguration.VGA_Status__c = 'New';
        objPrelaunchConfiguration.VGA_Time_Interval__c = 10;
        objPrelaunchConfiguration.VGA_Tracking_Id__c = '12345';
        return objPrelaunchConfiguration;
    }
    
    public static VGA_Manual_Assignment_Configuration__c createPreLaunch(id productid)
    {
        VGA_Manual_Assignment_Configuration__c objPre = new VGA_Manual_Assignment_Configuration__c();
        return objPre;
    }

    public static VGA_Working_Hour__c createWorkingHour(string strDay,string strStartTime,string strEndTime)
    {
        VGA_Working_Hour__c objWH = new VGA_Working_Hour__c();
        objWH.VGA_Day__c = strDay != null?strDay:null;
        objWH.VGA_Start_Time__c = strStartTime != null?strStartTime:null;
        objWH.VGA_End_Time__c = strEndTime != null?strEndTime:null;
        
        return objWH;
    }

    public static void createWeekWorkingHours(Id userProfileId)
    {
        List<VGA_Working_Hour__c> listOfTradingHours = new List<VGA_Working_Hour__c>();
        Set<String> weekNameSet = new Set<String>{'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'};

        for(String weekName : weekNameSet)
        {
            VGA_Working_Hour__c objWH = new VGA_Working_Hour__c();
            objWH.VGA_Day__c        = weekName;
            objWH.VGA_Start_Time__c = '09:00 AM';
            objWH.VGA_End_Time__c   = '05:00 PM';
            objWH.VGA_Working__c    = weekName == 'Sunday' ? false : true;
            objWH.Name              = weekName;
            objWH.VGA_User_Profile__c = userProfileId;
            //objWH.VGA_Available__c  = true;
            listOfTradingHours.add(objWH);
        }
        
        insert listOfTradingHours;
    }

    public static VGA_Dealership_Profile__c createDealershipProfile(Id AccountID,string subbrand,string distributionMethod)
    {
        VGA_Dealership_Profile__c obj =  new VGA_Dealership_Profile__c();
        obj.VGA_Dealership__c = AccountID != null ?AccountID:null;
        obj.VGA_Sub_Brand__c = subbrand != null?subbrand:null;
        obj.VGA_Distribution_Method__c = distributionMethod != null?distributionMethod:null;
        
        return obj;
    }
    public static VGA_Dealer_Traffic_Detail__c createDealerTrafficDetail(ID DealerTrafficHeader)
    {
        VGA_Dealer_Traffic_Detail__c obj = new VGA_Dealer_Traffic_Detail__c();
        obj.VGA_Dealer_Traffic_Header__c = DealerTrafficHeader != null?DealerTrafficHeader:null;
        
        return obj;
    }
    public static VGA_Active_Brochure__c createActiveBrochure() 
    {
        VGA_Active_Brochure__c obj = new VGA_Active_Brochure__c();
        
        return obj;
    }
    public static VGA_Public_Holiday__c createPublicHoliday(Date dt,string HoliDayname,string HolidayType,string strStateRegion) 
    {
        VGA_Public_Holiday__c objPH = new VGA_Public_Holiday__c();
        objPH.VGA_Holiday_Date__c = dt != null?dt:system.today();
        objPH.VGA_Holiday_Name__c = HoliDayname != null?HoliDayname:null;
        objPH.VGA_Holiday_Type__c = HolidayType != null?HolidayType:null;
        objPH.VGA_State_Region__c = strStateRegion != null?strStateRegion:null;
        
        return objPH;
    }

    public static VGA_Service_Campaigns__c buildTakataServiceCampaing(Id vehicleId) {

        VGA_Service_Campaigns__c takataServCampaign = new VGA_Service_Campaigns__c();
        takataServCampaign.Name                   = 'WV1ZZZ7HZ9H044254';
        takataServCampaign.VGA_SC_Description__c  = 'TAKATA Airbag Recall';
        takataServCampaign.Name__c                = 'TAKATA Airbag Recall';
        takataServCampaign.VGA_SC_Code__c         = '69Q7';
        takataServCampaign.VGA_SC_Brand__c        = 'Volkswagen';
        takataServCampaign.VGA_SC_Sub_Brand__c    = 'Commercial Vehicles (CV)';
        takataServCampaign.VGA_SC_Vin__c          = 'WV1ZZZ7HZ9H044254';
        takataServCampaign.VGA_SC_Status__c       = 'Future';
        takataServCampaign.VGA_SC_Campaign_Type__c= 'Takata Airbag Recall';
        takataServCampaign.Campaign_Code__c       = '69Q7';
        takataServCampaign.VGA_SC_Vehicle__c      = vehicleId;
        takataServCampaign.passenger_airbag_inflator_affected__c = 'N';
        takataServCampaign.driver_airbag_inflator_affected__c    = 'Y';
        takataServCampaign.driver_airbag_recall_pra_number__c    = 'PRJ1002801'; 

        return takataServCampaign;
    }

    public static Asset buildVehicle(String vin, Id accId) {

        Asset newVehicle = new Asset();
        newVehicle.Name           = vin;
        newVehicle.VGA_Colour__c  = 'GREY WHITE';
        newVehicle.VGA_Engine__c  = 'BPC 058100';
        newVehicle.AccountId      = accId;
        newVehicle.VGA_VIN__c     = vin;
        
        return newVehicle;
    }

    public static VGA_Vehicle_Policy__c buildVehiclePolicy(Id vehicleId) {

        VGA_Vehicle_Policy__c objVehicle  = new VGA_Vehicle_Policy__c();
        objVehicle.VGA_Brand__c           = 'Volkswagen';  
        objVehicle.VGA_Sub_Brand__c       = 'Commercial Vehicles (CV)';
        objVehicle.Is_Extended_Warranty__c= true;
        objVehicle.VGA_Component_Type__c  = 'Roadside';
        objVehicle.VGA_Component_Code__c  = 'FW24';
        objVehicle.VGA_VIN__c             = vehicleId;
        
        return objVehicle;
    }

    public static VGA_Ownership__c buildOwnership(Id vehicleId, Id ownerId){
        
        VGA_Ownership__c ownerShip = new VGA_Ownership__c();
        ownerShip.OwnerId    = ownerId;
        ownerShip.VGA_VIN__c = vehicleId;
        return ownerShip;
    }

    public static Recall_DocuSign_Settings__c buildRecallDocusignSettings(){
        Recall_DocuSign_Settings__c recallDocuSett = new Recall_DocuSign_Settings__c();
        recallDocuSett.Name                    = 'Takata';
        recallDocuSett.Account_Id__c           = '91428834-193e-4c4c-a82f-9f63ebe40ecd';
        recallDocuSett.Base_URL__c             = 'https://au.docusign.net/restapi/v2/accounts/1000268670';
        recallDocuSett.Enable_Email__c         = true;
        recallDocuSett.Enable_Production__c    = true;
        recallDocuSett.Integrator_Key__c       = '5E73AF32-B26D-4C64-B403-758A33B08504';
        recallDocuSett.Password__c             = 'vga@2018';
        recallDocuSett.Production_SOAP_URL__c  = 'https://au.docusign.net/api/3.0/dsapi.asmx';
        recallDocuSett.Sandbox_SOAP_URL__c     = 'https://demo.docusign.net/api/3.0/dsapi.asmx';
        recallDocuSett.Skoda_Account_Id__c     = '91428834-193e-4c4c-a82f-9f63ebe40ecd';
        recallDocuSett.Skoda_Email_Template__c = 'Skoda_DocuSign_Recall';
        recallDocuSett.Skoda_Integrator_Key__c = '5E73AF32-B26D-4C64-B403-758A33B08504';
        recallDocuSett.Skoda_Password__c       = 'vga@2018';
        recallDocuSett.Skoda_Template_Id__c    = '5d29ee40-b201-4973-8dc2-089c8f0966c7';
        recallDocuSett.Skoda_Username__c       = Label.VGA_SKO_Emails_Recalls;
        recallDocuSett.User_Name__c            = Label.VGA_VW_Emails_Recalls;
        recallDocuSett.VW_Email_Template__c    = 'VW_DocuSign_Recall';
        recallDocuSett.Volkswagen_Template_Id__c = '3daee679-9ec0-4ed8-aa50-0dd535f7e04b';

        return recallDocuSett; 
    }

        /**
     * This method builds test campaign with given namePrefix
     * And returns the built record
     */
    public static Campaign buildCampaign(String namePrefix) {

        Campaign newCampaign = new Campaign();
        newCampaign.name               = namePrefix;
        newCampaign.Description        = 'Test Description' + namePrefix;
        newCampaign.VGA_Tracking_ID__c = String.valueOf(generateRandomInRange(10000, 90000));
        
        return newCampaign;
    }

    /**
     * This method builds the given number of campaigns with given namePrefix
     * And returns the list of built records
     */
    public static List<Campaign> buildCampaigns(String namePrefix, Integer numberOfRecords) {
        List<Campaign> campaigns = new List<Campaign>();

        for(Integer i=0; i<numberOfRecords; i++) {
            campaigns.add(buildCampaign(namePrefix+i));
        }
        return campaigns;
    }

    public static VGA_Profanity__c buildProfanity(String keyword){
        
        VGA_Profanity__c newProf = new VGA_Profanity__c();
        newProf.VGA_Is_Active__c = true;
        newProf.VGA_Keyword__c   = keyword;

        return newProf;
    }

    private static String generateRandomNumber(){
        return String.valueOf(Math.abs(Crypto.getRandomInteger()));
    }
    
    public static List<VGA_Ownership__c> buildOwnershipRecords()
    {        
        List<VGA_Ownership__c> lstOwnership = new List <VGA_Ownership__c>();
        for(Integer i=0;i<50;i++)
         {
            lstOwnership.add(new VGA_Ownership__c());
         }         
         insert lstOwnership;
         return lstOwnership;
    }
     
    // Random integer within range
    private static Integer generateRandomInRange(Integer bound1, Integer bound2) {
        Integer min = Math.min(bound1, bound2);
        Integer max = Math.max(bound1, bound2);
        Integer r = min + (Integer)(Math.random() * (max - min + 1));
        return r;
    }

    /* @date 23/10/2018
    * @description Below two methods buildAllianzMapping and buildOwnershipRecords are created for testing Queueable Jobs in AccountTriggerHandler.
    * <P><b> MODIFIED DATE:</b> 23/10/2018</P>
    * <P><b> MODIFIED BY:</b> Archana Yerramilli  </P>
    * <P><b> ASSEMBLA TICKET:</b> 493 </P>
    * <p><P> Archana has updated this class from line number 446 to 457.  </p></P>
    */
    public static List<VGA_Allianz_Data_Mapping__c> buildAllianzMapping()
    { 
         List<VGA_Allianz_Data_Mapping__c> lstAllianz = new List <VGA_Allianz_Data_Mapping__c>();
         for(Integer i=0;i<50;i++)
         {
            lstAllianz.add(new VGA_Allianz_Data_Mapping__c  ());
         }         
         insert lstAllianz;
         return lstAllianz;
    }

    public static VGA_User_Profile__c buildUserProfile(String role, Id contactId, String brand, String subBrand)
    {
        VGA_User_Profile__c objProfile = new  VGA_User_Profile__c();
        objProfile.VGA_Role__c         = role;
        objProfile.VGA_Contact__c      = contactId;
        objProfile.VGA_Brand__c        = brand;
        objProfile.VGA_Sub_Brand__c    = subBrand;
        objProfile.VGA_Available__c    = true;
        return objProfile;
    }
    
    public static Case createCase(String subjectStr)
    { 
        Case objCase = new Case();
        objCase.Subject = subjectStr;
        return objCase;
    }
    public static List<Case> buildCaseRecords(Set<String> namesSet)
    { 
        List<Case> lstCase = new List <Case>();
         for(String name : namesSet)
         {
                Case myCase = new Case(); 
                myCase.Subject = name;
                myCase.VGA_Omni_Queue_Name__c = 'Academy Email';
                myCase.Origin = 'Academy Email';
                myCase.VGA_Omni_Accepted_Date_Time__c = system.now();
                myCase.VGA_No_of_Incoming_Mails__c = 2;
                //myCase.SuppliedEmail = 'academy@success.volkswagen.net.au';
                //myCase.VGA_To_Email__c = 'academy@success.skoda.net.au';
                myCase.VGA_Omni_Channel_Status__c  = 'Opened';
              lstCase.add(myCase);
         }
         //insert lstCase;
         return lstCase;
    }
    public static List<Case> buildCaseRecordsVWEmail(Set<String> namesSet)
    { 
        List<Case> lstCase = new List <Case>();
         for(String name : namesSet)
         {
                Case myCase = new Case(); 
                myCase.Subject = name;
                myCase.Origin = 'VW Email';
                myCase.VGA_Omni_Accepted_Date_Time__c = system.now();
                myCase.VGA_No_of_Incoming_Mails__c = 2;
                myCase.SuppliedEmail = 'academy@success.volkswagen.net.au';
                myCase.VGA_To_Email__c = 'academy@success.skoda.net.au';
                myCase.VGA_Omni_Channel_Status__c  = 'Opened';
              lstCase.add(myCase);
         }
         //insert lstCase;
         return lstCase;
    }

    public static EmailMessage buildEmailMessage(String fromAddress, String toAddress, 
                                                 String ccAddress, Id parentId){
        
        EmailMessage objEmailMessage = new EmailMessage();
        objEmailMessage.FromAddress  = fromAddress;
        objEmailMessage.Incoming  = true;
        objEmailMessage.ToAddress = toAddress;
        objEmailMessage.CcAddress = ccAddress;
        objEmailMessage.subject   = 'Test';
        objEmailMessage.TextBody  = '12345';
        objEmailMessage.ParentId  = parentId; 
        
        return  objEmailMessage;
    }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 09/04/2019
    * @description Method is used to build Attachment data
    */
    public static Attachment buildAttachment(string ParentId,String Name){
        Attachment objAttachment=new Attachment();
        objAttachment.body = blob.valueof('0');
        objAttachment.name = Name;
        objAttachment.ParentId = ParentId;
        return objAttachment;
    }

    /**
    * @author MOHAMMED ISHAQUE SHAIKH
    * @date 09/04/2019
    * @description Method is used to build Custom Setting of  VGA_Triggers__C
    */
    public static VGA_Triggers__c buildCustomSettingTrigger(string Name,Boolean active){
        VGA_Triggers__c objTriggerCustom=new VGA_Triggers__c();
        objTriggerCustom.Name = Name;
        objTriggerCustom.VGA_Is_Active__c= active;
        return objTriggerCustom;
    }
    
    /**
    * @author Ganesh M
    * @date 17/07/2019
    * @description Method is used to build Custom Setting of  VGA_MuleUrl__c
    */
     public static list<VGA_MuleUrl__c>  buildCustomSettingforMule(){
         list<VGA_MuleUrl__c> customdata=new list<VGA_MuleUrl__c>();
         VGA_MuleUrl__c vwset=new VGA_MuleUrl__c();
         VGA_MuleUrl__c skodaset=new VGA_MuleUrl__c();
       vwset.Name='Volkswagen';
       vwset.Client_ID__c='12121212121';
       vwset.Secret_Id__c='1212';
       
       skodaset.Name='Skoda';
       skodaset.Client_ID__c='12121212121';
       skodaset.Secret_Id__c='1212';
       customdata.add(vwset);
       customdata.add(skodaset);
       
       
       return customdata;
    }
     
}