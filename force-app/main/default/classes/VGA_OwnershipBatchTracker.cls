/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeallData = false)
private class VGA_OwnershipBatchTracker  
{
	public static VGA_Ownership__c objOwnership;
	public static Account objAccount;
	static testMethod void myUnitTest() 
    {
        // TO DO: implement unit test
        loadData();
        
        VGA_OwnershipBatch objCls = new VGA_OwnershipBatch();
        Database.executeBatch(objCls);
    }
    public static void loadData()
    {
        VGA_Welcome_Data_Pack_Extract__c objDataPackExtract = new VGA_Welcome_Data_Pack_Extract__c();
        objDataPackExtract.Name = 'Welcome Pack Data Extract';
        objDataPackExtract.VGA_Process__c = true;
        insert objDataPackExtract;
        
    	objAccount = new Account();
    	objAccount.lastname = 'Test';
    	objAccount.personEmail = 'rishi.patel@saasfocus.com';
    	objAccount.recordtypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Skoda Individual Customer').getRecordTypeId();

    	insert objAccount;
    	
    	objOwnership = new VGA_Ownership__c();
    	objOwnership.VGA_Owner_Name__c = objAccount.id;
    	objOwnership.VGA_Type__c = 'Current Owner';
    	
    	insert objOwnership;
    }
}