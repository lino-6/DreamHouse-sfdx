public class VGA_SMSResponse
{
    public cls_vga[] vga;
    
    public class cls_vga 
    {
        public String record_id;    //a040k000003nDuIAAU
        public Integer message_id;  //97133907
        public String send_at;  //2017-12-06 17:23:51
        public Integer recipients;  //1
        public Double cost; //0.064
        public Integer sms; //1
        public cls_delivery_stats delivery_stats;
        public cls_error error;
    }
    
    public class cls_delivery_stats 
    {
        public Integer delivered;   //0
        public Integer pending; //0
        public Integer bounced; //0
        public Integer responses;   //0
        public Integer optouts; //0
    }
    
    public class cls_error 
    {
        public String code; //SUCCESS
        public String description;  //OK
    }
    
    public static VGA_SMSResponse parse(String json)
    {
        return (VGA_SMSResponse) System.JSON.deserialize(json, VGA_SMSResponse.class);
    }
}