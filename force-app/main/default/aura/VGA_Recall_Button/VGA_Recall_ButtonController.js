({
	openDocusign: function(component, event, helper) {		
        var rec_id = component.get("v.recordId");
        var action = component.get("c.sendDocuSignContract");
        action.setParams({"case_id" :rec_id});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result = actionresult.getReturnValue();
                                   if(result.indexOf('ERROR:') > -1)
                                   {
                                        helper.showToast(component,'ERROR',result, 'error');
                                        return;                                       
                                   }
                                   else
                                   {
                                       location.href = result;                                  }
                               }
                               else
                               {
                                   helper.showToast(component,'ERROR','Webservice error', 'error');
                                   return;
                               }
                           });
        $A.enqueueAction(action);	
	},
	caseUpdate: function(component, event, helper) {		
        var rec_id = component.get("v.recordId");
        var action = component.get("c.updateCase");
        action.setParams({"case_id" :rec_id});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result = actionresult.getReturnValue();
                                   if(result.indexOf('ERROR:') > -1)
                                   {
                                        helper.showToast(component,'ERROR',result, 'error');
                                        return;                                       
                                   }
                                   else
                                   {
                                       
                                       if(result > 1)
                                       {
                                           alert('ATTENTION!!!. This VIN is part of '+result+' campaigns.');
                                       }
                                       var action = component.get("c.openDocusign");
                                       $A.enqueueAction(action);
                                       
                                   }
                               }
                               else
                               {
                                   helper.showToast(component,'ERROR','Webservice error', 'error');
                                   return;
                               }
                           });
        $A.enqueueAction(action);	
	},
    doInit : function(component, event, helper) 
    {
        var rec_id  = component.get("v.recordId");
        var action  = component.get("c.getCaseRecordType");
        action.setParams({"case_id" :rec_id});
        action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result = actionresult.getReturnValue();
                                     if(result == 'Recall Refused')
                                     {
                                         component.set("v.show_me", true);
                                     }                                                                         
                                }
                               else
                               {
                                   component.set('v.message', 'FAILED!');
                               }
                           });
         $A.enqueueAction(action);        
	}    
})