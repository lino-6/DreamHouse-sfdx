({
	loadDataToCalendar :function(component,data)
    {  
        //Find Current date for default date
        var d = new Date();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var currentDate = d.getFullYear() + '/' +
            (month<10 ? '0' : '') + month + '/' +
            (day<10 ? '0' : '') + day;         
        var self = this;
        $('#calendar').fullCalendar({
            header: 
            {
                  left: 'prev,next today',
                  center: 'title',
                  right: 'month,agendaWeek,agendaDay,listMonth'
            },
            timeFormat: 'hh:mm a',
            selectable : true,
            events:data,
            selectHelper: true,
            select: function(start, end,allDay,jsEvent) {            
                component.set("v.ShowModule", true);
                component.set("v.ShowLeadDetails", false); 
                component.set("v.ShowEventDetails",false);
                component.set("v.StartTime", start.format()); 
                component.set("v.EndTime", end.format());   
                
                
                var objCompB = component.find('compB');
                objCompB.sampleMethod(start.format(), end.format());
            },
            defaultDate: currentDate,
            editable: true,  
            eventLimit: true,  
            eventResize:true,
            draggable:true,      
            droppable: true,
            weekNumbers : true,
            eventDrop: function(event, delta, revertFunc) 
            {
                component.set("v.ShowEventDetails",false);
                component.set("v.ShowLeadDetails",false);
                component.set("v.showIASpinner", true);                
    			if (!confirm("Are you sure about this change?")) 
                {
      				revertFunc();
                    component.set("v.showIASpinner", false);
    			}
   		 		else
         		{
          			var eventid = event.id;
          			var eventdate = event.start.format();
          			self.editEvent(component,eventid,eventdate);
                    component.set("v.showIASpinner", false);
         		}
  			},
  			eventClick: function(whoid,event, jsEvent, view,recordType, id, AccountID) 
            {
               
                if(whoid.recordType != 'Unavailable'){
                    var title = event.title;
                    component.set("v.DetailFlag", true);
                    component.set("v.ShowLeadDetails",true);
                    component.set("v.ShowEventDetails",false);
                    var accID = component.get("v.AccountID");                
                    var objLeadComp = component.find('leadDetailsSection');
                    objLeadComp.iasMethod(whoid); 
                    
                } 
                else 
                {
                    component.set("v.ShowEventDetails",true);
                    component.set("v.ShowLeadDetails",false);
                    component.set("v.eventid",whoid.id);                    
                    var objEventComp = component.find('eventDetailsSection');
                    objEventComp.getEventDetails(whoid.id); 

                }              
            },
            dayClick: function (date, allDay, jsEvent, view) 
            {
               component.set("v.ShowModule", false);            
            },
            eventResize: function( event, delta, revertFunc, jsEvent, ui, view ) 
            {
                component.set("v.ShowEventDetails",false);
                component.set("v.ShowLeadDetails",false);
               if (!confirm("Are you sure about this change?")) 
               {
                     revertFunc();
               }
                   else
                {
                     var eventid = event.id;
                     var eventdate = event.end.format();
                     self.editEventEndDate(component,eventid,eventdate);
                }               
            },
            
    });
    },      
    formatFullCalendarData : function(component,events) 
    {
        var josnDataArray = [];

        for(var i = 0;i < events.length;i++){
            var startdate = $A.localizationService.formatDate(events[i].StartDateTime);
            var enddate = $A.localizationService.formatDate(events[i].EndDateTime);
            josnDataArray.push({
                'title':events[i].Subject,
                'start':startdate,
                'end':enddate,
                'id':events[i].Id
            });
        }

        return josnDataArray;
    },
    tranformToFullCalendarFormat : function(component,events) 
    {
        var browserType = navigator.sayswho= (function(){
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        })();

        var eventArr = [];
        for(var i = 0;i < events.length;i++){

            var dt =  new Date(events[i].StartDateTime);
            var timezone = $A.get("$Locale.timezone");

            var localeStartTime = dt.toLocaleString("en-US", {timeZone: timezone});
            localeStartTime = new Date(localeStartTime);

            var dt1 =  new Date(events[i].EndDateTime);

            var localeEndTime = dt1.toLocaleString("en-US", {timeZone: timezone});
            localeEndTime = new Date(localeEndTime);

            var startdate = $A.localizationService.formatDate(localeStartTime, "EEEE, MMMM dd yyyy, hh:mm:ss a");
            var enddate   = $A.localizationService.formatDate(localeEndTime, "EEEE, MMMM dd yyyy, hh:mm:ss a");

            //Fix issue with Internet Explorer and new Date() method
            //In internet explorer the time difference will need to be calculated manually
            if (browserType.startsWith("IE")) {
                var dt =  new Date(events[i].StartDateTime);
                var offset = new Date().getTimezoneOffset();
                if(timezone == "Australia/Adelaide"
                   && offset != "-570"){
                    dt.setMinutes( dt.getMinutes() - 30 );
                }
                
                
                var startdate = $A.localizationService.formatDate(dt, "EEEE, MMMM dd yyyy, hh:mm:ss a");
                
                var enddate   = $A.localizationService.formatDate(events[i].EndDateTime, "EEEE, MMMM dd yyyy, hh:mm:ss a");
            }
            
            eventArr.push({
                'id':events[i].Id,
                'start':startdate,
                'end':enddate,
                'title':events[i].Subject,
                'whoid':events[i].WhoId,
                'recordType': events[i].RecordType.Name
            });
        }
        return eventArr;
    },   
    fetchCalenderEvents : function(component) 
    {
        var action = component.get("c.getAllEvents");
        var selectedUser = $A.get("$SObjectType.CurrentUser.Id");
        action.setParams({ userId : selectedUser});     
        var self = this;
        component.set("v.showIASpinner", true);
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS") 
            {
                var eventArr = self.tranformToFullCalendarFormat(component,response.getReturnValue());
                self.loadDataToCalendar(component,eventArr);                
                component.set("v.Objectlist",eventArr);
                component.set("v.showIASpinner", false);                
            } 
            else if (state === "ERROR") 
            {
                                 
            }
        });        
        $A.enqueueAction(action);       
    },
    reFetchCalenderEvents : function(component) 
    {       
        var action = component.get("c.getAllEvents");  
        var selectedUser = $A.get("$SObjectType.CurrentUser.Id");        
        var userRole = component.get('v.UserRole');
        if(userRole == "Dealer Administrator"
            || userRole == "Lead Controller"){
            selectedUser = component.find('Users').get('v.value')
        }        
        action.setParams({ userId : selectedUser});   
        var self = this;
     
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                //remove old data
                $('#calendar').fullCalendar('removeEvents');
                var eventArr = self.tranformToFullCalendarFormat(component,response.getReturnValue());
                self.loadDataToCalendar(component,eventArr);
                component.set("v.Objectlist",eventArr);
                $("#calendar").fullCalendar('addEventSource', eventArr);
			
            } 
            else if (state === "ERROR") 
            {
                             
            }
        });        
        $A.enqueueAction(action);       
    },  
    editEventEndDate : function(component,eventid,eventdate)
    {
         var action=component.get("c.updateEventEndDate");
         action.setParams({ eventid : eventid ,
                           eventdate : eventdate});
         action.setCallback(this, function (response) {
            var state = response.getState();
           console.log(state);
           
            if (state === "SUCCESS") 
            {    
                 
            } 
            else if (state === "ERROR") 
            {                                 
            }
        });
        
        $A.enqueueAction(action);
    },  
    editEvent : function(component,eventid,eventdate)
    {
         var action=component.get("c.updateEvent");
         component.set("v.showIASpinner", true);
         action.setParams({ eventid : eventid ,
                           eventdate : eventdate});
         action.setCallback(this, function (response) {
            component.set("v.showIASpinner", false);
            var state = response.getState();
            if (state === "SUCCESS") 
            {   
             
            } 
            else if (state === "ERROR") 
            {                                 
            }
        });
        
        $A.enqueueAction(action);
    },
    createEvent : function(component,eventid,eventdate)
    {
         var action=component.get("c.createEvent");
         action.setParams({ eventid : eventid, eventdate : eventdate});
         action.setCallback(this, function (response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {            
                
            } 
            else if (state === "ERROR") 
            {                                 
            }
        });
        
        $A.enqueueAction(action);
    },
    getUserList : function(component, event, helper) 
    {
		var act= component.get('c.getlistofUser');
        component.set("v.showIASpinner", true);
        act.setCallback(this, function(resp){
            if(resp.getState()=='SUCCESS'){
                var res=resp.getReturnValue();
                component.set('v.DealerContactUsers',res);
                var userId = $A.get("$SObjectType.CurrentUser.Id");
                component.set("v.showIASpinner", false);

                component.find('Users').set('v.value',userId);
            }else{                
            }
        });
        $A.enqueueAction(act);
	}
})