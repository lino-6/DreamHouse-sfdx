({
	afterScriptsLoaded: function(component, event,helper)
    {  
        var userRole = component.get('v.UserRole');

        if(userRole == "Dealer Administrator"
            || userRole == "Lead Controller"){
            helper.getUserList(component, event, helper);
        }
        
        helper.fetchCalenderEvents(component);
        component.set("v.ShowModule", false);          
    },    
    HideMe: function(component, event, helper) {
      component.set("v.ShowModule", false);       
    },
    ShowModuleBox: function(component, event, helper) {
       component.set("v.ShowModule", true);
  
      
    },
    handleComponentEvent : function(component, event,helper) {
      var message = event.getParam("message");    
      helper.reFetchCalenderEvents(component);
      //Updating new events
      $('#calendar').fullCalendar('rerenderEvents');
      //getting latest Events
      $('#calendar').fullCalendar( 'refetchEvents' );
      // set the handler attributes based on event data
  },
  onChangeConsultant : function(component,event, helper){
      helper.reFetchCalenderEvents(component);  
      //Updating new events
      $('#calendar').fullCalendar('rerenderEvents');
      //getting latest Events
      $('#calendar').fullCalendar( 'refetchEvents' );
      // set the handler attributes based on event data   
  }
})