({
	doInit : function(component, event, helper) 
    {
			helper.getProductDetails(component, event, helper);
	},
     controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
     controllerOnChange: function(component, event, helper) 
    {
        helper.onChange(component, event, helper);  
    },
    controllersaveDealerTraffic:function(component, event, helper) 
    {
        helper.saveDealerTraffic(component, event, helper);  
    },
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    }
})