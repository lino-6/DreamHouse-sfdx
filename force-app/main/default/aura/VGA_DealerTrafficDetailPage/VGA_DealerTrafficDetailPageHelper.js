({
	getProductDetails : function(component, event, helper) 
    {
           var action=component.get("c.getlogginguser");
           action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                     component.set("v.objName",result);
                                     this.getModelDetails(component, event, helper);
                                     this.getEndDate(component, event, helper);
                                }
                              });
           $A.enqueueAction(action); 
    },
    getEndDate : function(component, event, helper) 
    {
           var action=component.get("c.getendDate");
           action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.enddate",result);  
                                }
                              });
           $A.enqueueAction(action); 
           var action2=component.get("c.getendDatePlusone");
           action2.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.endateplusone",result);
                                }
                              });
           $A.enqueueAction(action2); 
      
    },
    refreshView: function(component, event, helper) 
    {
        var selected =  component.get("v.radiobutton");
        var action=component.get("c.getproductDetails");
                                               action.setParams({"radiobutton" :selected});
                                               action.setCallback(this, function(actionresult) 
                                                                  {
                                                                    var state = actionresult.getState();
                                                                    if(state === "SUCCESS")
                                                                    {
                                                                        var result =actionresult.getReturnValue();
                                                                        component.set("v.listofProduct",result); 
                                                                    }
                                                                  });
                                              			 $A.enqueueAction(action);
                                                       var action2=component.get("c.getTotalValue");
                                                       action2.setParams({"radiobutton" :selected});
                                                       action2.setCallback(this, function(actionresult) 
                                                                          {
                                                                            var state = actionresult.getState();
                                                                            if(state === "SUCCESS")
                                                                            {
                                                                                var result =actionresult.getReturnValue();
                                                                                component.set("v.objDetails",result);
                                                                            }
                                                                          });
                                                        
                                                       $A.enqueueAction(action2);
    },
    onChange : function(component, event, helper) 
    {
       var selected = event.getSource().get("v.text");
       component.set("v.radiobutton",selected);  
       var action=component.get("c.getproductDetails");
       action.setParams({"radiobutton" :selected});
       action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.listofProduct",result); 
                            }
                          });
        
       $A.enqueueAction(action);
       var action2=component.get("c.getTotalValue");
       action2.setParams({"radiobutton" :selected});
       action2.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.objDetails",result);
                            }
                          });
        
       $A.enqueueAction(action2);
    },
    saveDealerTraffic : function(component, event, helper) 
    { 
         var selected =  component.get("v.radiobutton");
         var lst   =component.get("v.listofProduct");
         var action=component.get("c.saveDealerDetails");
        console.log(action);
        //alert();
       // console.log(JSON.stringify(lst));
         action.setParams({"DealerDetails" :JSON.stringify(lst),
                           "radiobutton" :selected});
        
         action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                var errorvalue= result.indexOf('Error:');
                                if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   {
                                       window.setTimeout(function()
                                           {
                                               var action=component.get("c.getproductDetails");
                                               action.setParams({"radiobutton" :selected});
                                               action.setCallback(this, function(actionresult) 
                                                                  {
                                                                    var state = actionresult.getState();
                                                                    if(state === "SUCCESS")
                                                                    {
                                                                        var result =actionresult.getReturnValue();
                                                                        component.set("v.listofProduct",result); 
                                                                    }
                                                                  });
                                              			 $A.enqueueAction(action);
                                                       var action2=component.get("c.getTotalValue");
                                                       action2.setParams({"radiobutton" :selected});
                                                       action2.setCallback(this, function(actionresult) 
                                                                          {
                                                                            var state = actionresult.getState();
                                                                            if(state === "SUCCESS")
                                                                            {
                                                                                var result =actionresult.getReturnValue();
                                                                                component.set("v.objDetails",result);
                                                                            }
                                                                          });
                                                        
                                                       $A.enqueueAction(action2);
  						                        }, 1000); 
                                           this.showToast(component,'Success', result, 'success');
                                   }
                            }
                          });
        $A.enqueueAction(action);
    },
    getModelDetails:function(component, event, helper) 
    {
              debugger;
             var radiobutton;
             if(component.find("myPassenger3"))
              {
                radiobutton=component.find("myPassenger3").get("v.value");
                component.set("v.radiobutton","Commercial");
              }
              else
              {
               radiobutton=false; 
               component.set("v.radiobutton","Passenger");   
              }
           var action=component.get("c.getproductDetails");
           if(radiobutton ==false)
            {
              action.setParams({"radiobutton" :'Passenger'});
            }
            else
            {
              action.setParams({"radiobutton" :'Commercial'});    
            }
            action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.listofProduct",result);
                            }
                          });
        
       $A.enqueueAction(action); 
       var action2=component.get("c.getTotalValue");
        if(radiobutton ==false)
            {
              action2.setParams({"radiobutton" :'Passenger'});
            }
            else
            {
              action2.setParams({"radiobutton" :'Commercial'});    
            }
          action2.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.objDetails",result);
                            }
                          });
        
       $A.enqueueAction(action2);
    },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      }
})