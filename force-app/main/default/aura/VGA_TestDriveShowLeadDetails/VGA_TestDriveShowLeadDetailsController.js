({
	doInit : function(component, event, helper) 
    {
        
	},
    leadTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        component.find("leadId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("leaddataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    commentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make comment tab active and show tab data
        component.find("commentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("commentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        helper.getNoteslist(component, event,helper);
    },
    historyTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make histroy tab active and show tab data
        component.find("histroyId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("histroydataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        helper.getLeadHistory(component, event,helper);
    },
    controllerGetEditLead :function(component, event, helper) 
    {
        //get lead detail
        helper.getEditLead(component, event, helper);
        helper.getEditLead(component, event, helper);
        helper.getAttachment(component, event,helper);
        helper.getLeadHistory(component, event,helper);
        helper.getNoteslist(component, event,helper);
    },
    controlleronchangeconsultantName:function(component, event, helper) 
    {
        //get lead detail
        helper.onChangeGetValue(component, event, helper);
    },
    controllerSaveLeadDetails:function(component, event, helper) 
    {
        //save Lead Details
        helper.saveLeadDetails(component, event, helper);
    },
    controllerAddComment:function(component, event, helper) 
    {
        helper.addComment(component, event, helper);
    },
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    controllerSaveNotesDetail:function(component, event, helper) 
    {
		helper.saveNotesDetail(component, event, helper);
	},
    controllerOnclickAccept:function(component, event, helper) 
    {
		helper.onclickAccept(component, event, helper);
	},
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    ControlleronChangeConsultant:function(component, event, helper) 
    {
		helper.onchangeConsultant(component, event, helper);
	},
    ControlleronChangeGetstatus:function(component, event, helper) 
    {
		helper.onChangeGetstatus(component, event, helper);
	},
    controlleronClickdownload:function(component, event, helper) 
    {
		helper.onClickdownload(component, event, helper);
	},
    controlleronchangeresignpicklist:function(component, event, helper) 
    {
		helper.onchangeresignpicklist(component, event, helper);
	},
    controlleronRefreshLeadHistroy:function(component, event, helper) 
    {
		helper.onRefreshLeadHistroy(component, event, helper);
	},
    controlleronRefreshNotes:function(component, event, helper) 
    {
		helper.onRefreshNotes(component, event, helper);
	},
    renderPage: function(component, event, helper)
    {
        helper.renderPage(component);
    },
    controlleronClickaddress: function(component, event, helper)
    {
        helper.onClickaddress(component, event, helper);
    },
    controllerclosemappopup :function(component, event, helper)
    {
        helper.closemappopup(component, event, helper);
    },
    sortAssigned:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Assigned_to_Dealership__c');
         helper.sortHelper(component, event, helper);
    },
    sortStatus:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'Status');
         helper.sortHelper(component, event, helper);
    },
    sortOwner:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Owner_Name__c');
         helper.sortHelper(component, event, helper);
    },
    sortoutcome:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Outcome__c');
         helper.sortHelper(component, event, helper);
    },
    sortEscalation:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Escalation__c');
         helper.sortHelper(component, event, helper);
    },
    doAction : function(component, event,helper) {       
        var params = event.getParam('arguments');      
        if (params) 
        {  
            var whoid = params.param1['whoid'];
            component.set("v.leadId",whoid);           
            helper.getEditLead(component, event, helper);
        }
    },
    closeToast : function(cmp, event, helper)
    {
        cmp.set("v.errormsg",'');
    },
    controllerSaveEventDetails:function(component, event, helper) 
    {
    	helper.saveEventDetails(component,event,helper); 
    },
    controllerDeleteEventDetails:function(component, event, helper) 
    {
        var confirmCheck=window.confirm('Are you sure you want to delete the event ?');
		if(confirmCheck)
        {
				helper.deleteEventDetails(component, event, helper);
		}
    },  
    controllerOnChangeResignEvent:function(component, event, helper) 
    {
		helper.onChangeReassignEvent(component, event, helper);
	}
    ,
  	/*attachmentTab: function(component, event, helper) 
	{
        helper.clearAll(component, event); 
        component.find("attachmentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("attachmentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        helper.getAttachment(component, event,helper); 
	}*/

    attachmentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        component.set('v.LeadCheck',false); 
        component.set('v.hasImageSrc','');
        component.find("attachmentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("attachmentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        component.set('v.DocusignStage1Link',false);
        component.set('v.DocusignStage2Link',false);
        component.set('v.baseUrl','');
        component.set('v.DocusignStage1Url','');
        component.set('v.DocusignStage2Url','');
        component.set("v.hasImageSrc",false);
        if(component.get("v.leadId")!='' && component.get("v.leadId")!=undefined){
            helper.getAttachment(component, event,helper); 
        }
        
    },
    navigate : function(component,event,helper){
        if(component.get('v.LeadCheck')){
            window.open(component.get('v.DocusignStage1Url')+component.get('v.baseUrl'));
        }else{
            helper.showToast(component,'ERROR','Unable to find Dealer Code / Primary Id for selected Lead!', 'error');
        }
        //window.location.replace(component.get('v.DocusignStage1Url')+component.get('v.baseUrl'));
        

    }
    
})