({
  
    clearAll: function(component, event) 
    {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) 
        {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    getEditLead: function(component, event, helper) 
    {
        component.set('v.tempStatus',undefined);
        var AccountId = component.get("v.AccountId");
        component.set('v.showIASpinner',true);
        this.getAttachment(component, event,helper);
        return new Promise($A.getCallback(function (resolve, reject) 
        { 
            var targetId;   
            
            targetId= component.get("v.leadId");         
            var action =component.get("c.getLeadDetails");
            action.setParams({"RecordId" :targetId});       
            action.setCallback(this, function(actionresult) 
            {
                var state = actionresult.getState();
                if(state == "SUCCESS")
                {
                    var result =actionresult.getReturnValue();
                    component.set("v.DetailFlag",true);
                    component.set("v.objLead",result);
                    component.set('v.tempStatus',result.VGA_Status__c==undefined?undefined:result.VGA_Status__c);
                }  
            });
            $A.enqueueAction(action);
            var action2 =component.get("c.getresign");
            action2.setParams({"RecordIds" :targetId});
            action2.setCallback(this, function(actionResult) 
            {
                var state = actionResult.getState(); 
                if(state =="SUCCESS")
                {
                    var arrayOfMapKeys = [];
                    var result =actionResult.getReturnValue();
                    for (var singlekey in result)
                    {
                        arrayOfMapKeys.push({
                            key: singlekey,
                            value: result[singlekey]
                        });
                    }             
                    component.set('v.reassignlstKey', arrayOfMapKeys);
                }
            });
            $A.enqueueAction(action2);
            var action3 =component.get("c.getEvent");
            var leadID;        
            leadID= component.get("v.leadId");  
            
            action3.setParams({"leadID" :leadID});
            action3.setCallback(this, function(resultCallback) 
            {
                var state = resultCallback.getState();                 
                if(state === "SUCCESS")
                {
                    var result =resultCallback.getReturnValue();
                    component.set("v.objEvent",result);	
                    
                }  
            });
            $A.enqueueAction(action3);
              
			var action4 =component.get("c.getPilotContacts");  
            action4.setCallback(this, function(resultPilotCallback) 
            {
                 	var arrayOfEventMapKeys = [];
                    var resultPilotContacts =resultPilotCallback.getReturnValue();
                    for (var singlekey in resultPilotContacts)
                    {
                        arrayOfEventMapKeys.push({
                            key: resultPilotContacts[singlekey].Id,
                            value: resultPilotContacts[singlekey].Name
                            
                        });
                       
                    } 
                    component.set('v.reassignEventlstKey', arrayOfEventMapKeys);
            });
            $A.enqueueAction(action4);  

            
            component.set('v.showIASpinner',false);
        }));
        
        this.getNoteslist(component, event, helper);
        this.getLeadHistory(component, event, helper);

    },
    getEvent: function(component, event, helper) 
    {
        return new Promise($A.getCallback(function (resolve, reject) 
        {
            var action3 =component.get("c.getEvent");
            var targetId;    
            component.set('v.showIASpinner',true);
            targetId= component.get("v.leadId");  
            action3.setParams({"RecordIds" :targetId});
            action3.setCallback(this, function(resultCallback) 
            {
                var state = resultCallback.getState();                 
                if(state === "SUCCESS")
                {
                    var result =resultCallback.getReturnValue();
                    component.set("v.objEvent",result);	
                    
                }  
                component.set('v.showIASpinner',false);
            }); 
            $A.enqueueAction(action3);
        }));
    },
    onChangeGetValue: function(component, event, helper) 
    {
        component.set('v.showIASpinner',true);
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id.
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var conId =component.get('v.ConsultantId');
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":conId,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue(); // get return result value.
                                   component.set("v.listofLead",result); // set list of lead on component.
                                   component.set("v.DetailFlag",false);
                                   component.set("v.maxPage", Math.floor((result.length+19)/20));
                                   this.renderPage(component);
                               }
                               component.set('v.showIASpinner',false);
                           });
        $A.enqueueAction(action);   
    },
    onChangeGetstatus: function(component, event, helper) 
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id.
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var conId =component.get('v.ConsultantId');
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":conId,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue(); // get return result value.
                                   component.set("v.listofLead",result); // set list of lead on component.   
                                   component.set("v.DetailFlag",false);
                                   component.set("v.maxPage", Math.floor((result.length+19)/20));
                                   this.renderPage(component);
                               }
                           });
        $A.enqueueAction(action);
        
    },
    onchangeConsultant: function(component, event, helper) 
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
        component.set('v.showIASpinner',true);
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var selectedValue= event.getSource().get("v.value");
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":selectedValue,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
        {
            var state = actionresult.getState(); //get state value.
            if(state === "SUCCESS")
            {
                var result =actionresult.getReturnValue(); // get return result value.
                component.set("v.listofLead",result); // set list of lead on component.
                component.set("v.ConsultantId",selectedValue); 
                component.set("v.DetailFlag",false);
                component.set("v.maxPage", Math.floor((result.length+19)/20));
                this.renderPage(component);
            }
            component.set('v.showIASpinner',false);
        });
        $A.enqueueAction(action);
        
    },
    saveLeadDetails: function(component, event, helper) 
    {
        var flag =false;
        component.set('v.showIASpinner',true);
        var lst=component.get("v.objLead");    
        var ownerIds=component.get("v.assignownerId");//Get lead details        
        var action=component.get("c.updateLeadDetails"); //To call Apex controller method      
        if($A.util.isUndefined(ownerIds) || $A.util.isEmpty(ownerIds))
        {
            action.setParams({"objLead"  :lst});             //To set parameter on Lead           
        }
        else
        {
            action.setParams({"objLead" :lst,"ownerIds" :ownerIds});             //To set parameter on Lead
            flag=true;
            
        } 
        if($A.util.isUndefined(lst.VGA_Status__c) ||
            $A.util.isEmpty(lst.VGA_Status__c) )
        {
            var tempStatus=component.get('v.tempStatus');
            if(tempStatus!=undefined ){  
                   if((tempStatus=='Accepted' || tempStatus=='Closed') && (lst.VGA_Status__c==''|| lst.VGA_Status__c==undefined )){
                       component.set('v.showIASpinner',false);
                       var inputCmp = component.find("status");
                       inputCmp.set("v.errors",[{message:"Please select Status."}]); 
                       
                       var outcomeCmp = component.find("outcome");
                       outcomeCmp.set("v.errors",null);
                       return;
                   } 
            }else if(tempStatus==undefined && lst.Status!='New'){
                component.set('v.showIASpinner',false);
                var inputCmp = component.find("status");
                inputCmp.set("v.errors",[{message:"Please select Status."}]); 
                
                var outcomeCmp = component.find("outcome");
                outcomeCmp.set("v.errors",null);
                return;
            }
            
        }
        
        if(lst.VGA_Status__c =='Closed' && ($A.util.isUndefined(lst.VGA_Outcome__c) ||$A.util.isEmpty(lst.VGA_Outcome__c)))
        {
            component.set('v.showIASpinner',false);
            
                var outcomeCmp = component.find("outcome");
                outcomeCmp.set("v.errors",[{message:"Please select Outcome."}]);
            
            return ;
        }
        //else
        //{
            
            var inputCmp = component.find("status");
            action.setCallback(this, function(actionresult) 
            {
                var state = actionresult.getState(); //get state value.
                if(state === "SUCCESS")
                {
                    
                    var result =actionresult.getReturnValue(); // get return result value.    
                    this.showToast(component,'Success', 'The lead was successfully saved.', 'success','Hide'); 
                    this.getlistvalue(component, event, helper);
                    if(flag == true)
                    {
                        component.set("v.DetailFlag",false);
                        inputCmp.set("v.errors",null);
                    }
                    else
                    {
                        component.set("v.DetailFlag",true);
                        inputCmp.set("v.errors",null);
                        this.getEditLead(component, event, helper); 
                        component.set("v.assignownerId",'');
                    }
                    component.set('v.showIASpinner',false);
                }
            });
            $A.enqueueAction(action);
        //}
        
    },
    saveEventDetails: function(component,event,helper)
    {
        var eventDet     = component.get("v.objEvent"); 
        var eventOwnerId = component.get("v.assignEventOwnerId");//Get lead details   
        console.log(' Id ---> ' + eventDet.Id); 
        console.log(' StartTime ---> ' + eventDet.StartDateTime); 
        console.log(' EndTime   ---> ' + eventDet.EndDateTime); 
        var lead =component.get("v.objLead"); 
        var saveEventAction = component.get("c.saveEventDetails"); 
        if(eventOwnerId)
        {

               saveEventAction.setParams({  "EventData": eventDet, 
                                            "lead" :lead,
                                    		"OwnerID" 	: eventOwnerId
                                         });
        }
        else
        {

               saveEventAction.setParams({"EventData": eventDet, 
                                    "lead" :lead});
        }
     

		 saveEventAction.setCallback(this,function(resp)
         {  
			if(resp.getState()=="SUCCESS")
            {
                var result =resp.getReturnValue();
                var errorvalue= result.indexOf('Error:');
                if(errorvalue != -1 )
                {
                    helper.showToast(component,'ERROR',result, 'error'); 
                }
                else
                {            
                    helper.showToast(component,'Success', result, 'success'); 								
                    // Get the component event by using the
                    // name value from aura:registerEvent
                    var cmpEvent = component.getEvent("cmpEvent");
                    cmpEvent.setParams({
                        "message" : "EventSaved"});
                    cmpEvent.fire();
                }

		   }
		   else
           {
				// Error Message to be displayed
				helper.showToast(component,'ERROR','Unable to save the Event', 'error');
		   }
		});
		$A.enqueueAction(saveEventAction);
    },
    deleteEventDetails: function(component,event,helper){
        var event =component.get("v.objEvent"); 
        component.set('v.showIASpinner',true);
        var deleteEventAction = component.get("c.deleteEventDetails"); 
         deleteEventAction.setParams({'EventId': event});
		 deleteEventAction.setCallback(this,function(resp)
         {
			if(resp.getState()=="SUCCESS"){
				var res=resp.getReturnValue();			
				if(res!=null)
                {				
					helper.showToast(component,'Success', 'The Event was successfully deleted.', 'success');
                     //component.set("v.DetailFlag",false);
					// Get the component event by using the
					// name value from aura:registerEvent
					var cmpEvent = component.getEvent("cmpEvent");
					cmpEvent.setParams({
						"message" : "EventUpdated"});
					cmpEvent.fire();
				}else{
					// Error Message to be displayed
					helper.showToast(component,'ERROR','Unable to save the Event ', 'error');
				}
                component.set('v.showIASpinner',false);
				
			}else{
				// Error Message to be displayed
				helper.showToast(component,'ERROR','Unable to save the Event', 'error');
			}
		});
		$A.enqueueAction(deleteEventAction);
    },
    addComment :function(component, event, helper) 
    {
        component.set("v.popupflag",true); 
    },
    refreshView: function(component, event, helper) 
    {
        $A.get('e.force:refreshView').fire();
    },
    saveNotesDetail:function(component, event, helper) 
    {
        var commentsvalue =component.get('v.commentvalue');
        var ledId =component.get('v.leadId');
        var titlevalue = commentsvalue.substring(0,10);
        var action=component.get("c.createnotes"); //To call Apex controller method
        action.setParams({"commentvalue" :commentsvalue,
                          "titlevalue":titlevalue,
                          "partentId" :ledId});//To set parameter on Lead
        action.setCallback(this, function(actionresult) 
        {
            var state = actionresult.getState(); //get state value.
            if(state === "SUCCESS")
            {
                var result =actionresult.getReturnValue();
                var errorvalue= result.indexOf('Error:');
                if(errorvalue != -1 )
                {
                    this.showToast(component,'ERROR',result, 'error'); 
                }
                else
                {
                    component.set("v.popupflag",false);
                    //this.showToast(component,'Success', result, 'success');
                    this.showToast(component,'Success', result, 'success');                   
                    this.getNoteslist(component, event, helper); 
                    component.set("v.commentvalue",'');     
                }  
            }
        });        
        $A.enqueueAction(action);
    },
    onRefreshNotes :function(component, event, helper)
    {
        component.set("v.popupflag",false);
        component.set("v.commentvalue",'');
        this.getNoteslist(component, event, helper); 
    },
    getNoteslist:function(component, event, helper) 
    {  
        var ledId =component.get('v.leadId');
        var action=component.get("c.getnotes"); //To call Apex controller method
        action.setParams({"partentId" :ledId});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.listofNotes",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    getLeadHistory:function(component, event, helper) 
    {
        var ledId =component.get('v.leadId');
        var action=component.get("c.getLeadHistory"); //To call Apex controller method
        action.setParams({"selectLeadId" :ledId});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   if(result)
                                   {
                                       for(var i=0;i<result.length;i++)
                                       {
                                           if(result[i].NewValue && result[i].NewValue.length >3 && result[i].NewValue.substring(0,3) =='005')
                                           {
                                               result[i].doNotRemove=true;
                                           }
                                           else
                                           {
                                               result[i].doNotRemove=false;  
                                           }
                                       }
                                   }
                                   component.set("v.listofLeadHistory",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    onRefreshLeadHistroy :function(component, event, helper)
    {
        this.getLeadHistory(component, event, helper); 
        this.getAttachment(component, event, helper);
    },
    onclickAccept:function(component, event, helper) 
    {
        var targetId =event.currentTarget.id;  
        var action=component.get("c.updateleadstatus");     //To call Apex controller method
        action.setParams({"leadId" :targetId});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
        {
            var state = actionresult.getState(); //get state value.
            if(state === "SUCCESS")
            {
                var result =actionresult.getReturnValue();
                console.log(result);
                this.showToast(component,'Success', ' successfully updated.', 'success'); 
                this.getEditLead(component, event, helper);
                this.getlistvalue(component, event, helper);
            }
        });
        $A.enqueueAction(action);
    },
    getUserRole:function(component, event, helper) 
    {
        var action=component.get("c.getuserInformation"); //To call Apex controller method
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue(); // get return result value.
                                   component.set("v.userRole",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    onClickdownload:function(component, event, helper) 
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var consultantId =component.get("v.ConsultantId");
        var lst =component.get("v.listofLead");
        if(lst.length >0)
        {
            var action=component.get("c.getuserInformationProfile");      //To call Apex controller method
            action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState(); //get state value.
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Volkswagen');
                                       if(errorvalue != -1 )
                                       {
                                           var url ="/volkswagen/apex/VGA_Export_Lead?assign=" + assignedSelectvalue +"&status=" +statusSelectvalue +"&accId=" +AccountId +"&consltId=" +consultantId;
                                           window.open(url);  
                                       }
                                       else
                                       {
                                           var url ="/skoda/apex/VGA_Export_Lead?assign=" + assignedSelectvalue +"&status=" +statusSelectvalue +"&accId=" +AccountId +"&consltId=" +consultantId;
                                           window.open(url);  
                                           
                                       }
                                   }
                               });
            $A.enqueueAction(action); 
        }
        else
        {
            this.showToast(component,'ERROR', 'No row Found.', 'error'); 
        } 
    },
    showToast : function(cmp, title, message, type, display ) 
    {
        cmp.set("v.errormsg",message);
        cmp.set("v.title",title);
        //---- Setting up severity ----//
        if(type == 'info')
        {
            cmp.set("v.type",'');
            cmp.set("v.icon",'utility:info');
        }
        else if(type == 'warning')
        {
            cmp.set("v.type",'slds-theme--error');
            cmp.set("v.icon",'utility:warning');
        }
        else if(type == 'error')
        {
                cmp.set("v.type",'slds-theme--error');
                cmp.set("v.icon",'utility:error');
        }
        else if(type == 'success')
        {
                    cmp.set("v.type",'slds-theme--success');
                    cmp.set("v.icon",'utility:success');
        }
        cmp.set("v.errormsg",message);
        window.setTimeout(function(){
            if(type == 'success')
            {
                //if(display == 'Hide')
                //{
                	cmp.set("v.ShowLeadDetails", false); 
                //}
            }
            cmp.set("v.errormsg",'');
        }, 5000);
    },
    onchangeresignpicklist:function (component, event, helper)
    {
        var selectedValue= event.getSource().get("v.value");
        component.set('v.assignownerId',selectedValue);  
    },
    onChangeReassignEvent:function (component, event, helper)
    {
        var selectedValue= event.getSource().get("v.value");
        component.set('v.assignEventOwnerId',selectedValue);  
    },
    getlistvalue:function (component, event, helper)
    {
        
        var assignedSelect =component.find("InputSelectSingle");
        //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var selectedValue= component.get("v.ConsultantId");
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":selectedValue,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState(); //get state value.
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue(); // get return result value.
                                   component.set("v.listofLead",result); // set list of lead on component.
                                   component.set("v.maxPage", Math.floor((result.length+19)/20));
                                   this.renderPage(component);
                               }
                           });
        $A.enqueueAction(action);
        
    },
    renderPage: function(component)
    {
        var records = component.get("v.listofLead"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*20, pageNumber*20);
        component.set("v.listofLeaddisplay", pageRecords);
    },
    onClickaddress: function(component, event, helper)
    {
        component.set("v.mappopup", true);
    },
    closemappopup: function(component, event, helper)
    {
        component.set("v.mappopup", false);
    },
    handleError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.addClass(component, "error");   
    },
    handleClearError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.removeClass(component, "error");   
    },
    sortHelper: function(component, event, helper) 
    {
        
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown') 
        {
            // set the arrowDirection attribute for conditionally rendred arrow sign  
            component.set("v.arrowDirection", 'arrowup');
            // set the isAsc flag to true for sort in Assending order.  
            component.set("v.isAsc", true);
        } 
        else 
        {
            component.set("v.arrowDirection", 'arrowdown');
            component.set("v.isAsc", false);
        }
        // call the onLoad function for call server side method with pass sortFieldName 
        this.getlistvalue(component, event, helper);
    },
    getAttachment : function(component, event, helper){

      //return new Promise($A.getCallback(function (resolve, reject){
        
          var urlString = window.location.href;
          var baseURL = urlString.substring(0, urlString.indexOf("/s"));
          var LeadId =component.get("v.leadId");
          var action=component.get("c.collectAttachmentWithParentId");
          component.set('v.LeadCheck',false);
          component.set('v.DocusignStage1Link',false);
          component.set('v.DocusignStage2Link',false);
          component.set('v.baseUrl','');
          component.set('v.DocusignStage1Url','');
          component.set('v.DocusignStage2Url','');
          component.set("v.hasImageSrc",false);
          action.setParams({'LeadId':LeadId});
          action.setCallback(this,function(resp){
              if(resp.getState()=='SUCCESS'){
                var result =  resp.getReturnValue();
                if(result!=null){
                  var attachment = result.AttachmentList==undefined?undefined:result.AttachmentList;
                  if(attachment!=undefined && attachment!=null && attachment.length!=0){
                      component.set("v.hasImageSrc",true);
                      component.set("v.DetailFlag",true);
                      var arrayOfMapKeys = [];
                      for (var singlekey in attachment){
                        
                          arrayOfMapKeys.push({
                              URL: baseURL + "/servlet/servlet.FileDownload?file=" + attachment[singlekey].Id,
                              value: attachment[singlekey].Name
                          });
                      }
                      component.set('v.imagelstKey', arrayOfMapKeys);
                  }
                  component.set('v.DocusignStage1Link',result.DocusignStage1Check);
                  component.set('v.DocusignStage2Link',result.DocusignStage2Check);
                  component.set('v.baseUrl',urlString);
                  component.set('v.DocusignStage1Url',result.DocusignLink);
                  component.set('v.DocusignStage2Url',result.DocusignLink);
                  component.set('v.LeadCheck',result.LeadCheck);

                }else{
                    helper.showToast(component,'ERROR','Lead Not  Found!', 'error');
                }
              }else{
                  helper.showToast(component,'ERROR','Please check Internet Connection', 'error');
              }
          });
          $A.enqueueAction(action);
      //}));
  }
    
})