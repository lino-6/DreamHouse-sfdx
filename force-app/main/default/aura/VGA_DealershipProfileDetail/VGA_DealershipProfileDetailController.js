({
	doInit : function(component, event, helper) 
    {
		helper.getDealershipProfile(component, event, helper);
	},
    controllerOnChange: function(component, event, helper) 
    {
        helper.onChange(component, event, helper);  
    },
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    controllerEditTradingHours:function(component, event, helper) 
    {
		helper.editTradingHours(component, event, helper);
	},
    controllerSaveTradingDetails:function(component, event, helper) 
    {
		helper.saveTradingHours(component, event, helper);
	},
    controllerSaveAccountDetails:function(component, event, helper) 
    {
		helper.saveAccountDetails(component, event, helper);
	},
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    onhover :function(cmp, event, helper)
    {
        
        var cmpTarget =  document.getElementById("help");
        var classstring =cmpTarget.className ;
        classstring=classstring.replace("slds-fall-into-ground", "slds-rise-from-ground");
        cmpTarget.className=classstring;
         
    },
    onmouseout :function(cmp, event, helper)
    {
      
        var cmpTarget =  document.getElementById("help");
        var classstring =cmpTarget.className ;
        classstring=classstring.replace("slds-rise-from-ground", "slds-fall-into-ground");
        cmpTarget.className=classstring;    
    },
    controlleronChangeUser :function(component, event, helper)
    {
        helper.onChangeUser(component, event, helper);
    },
    controllerclosebutton:function(component, event, helper)
    {
        helper.closebutton(component, event, helper);
    }
   
   
})