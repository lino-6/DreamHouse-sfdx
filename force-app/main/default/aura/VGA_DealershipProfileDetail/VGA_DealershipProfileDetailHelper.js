({
    getDealershipProfile : function(component, event, helper) 
    {
        var action4=component.get("c.getlogginguser");
        action4.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.objName",result);
                                    this.getlistofTradingHours(component, event, helper);
                                }
                            });
        $A.enqueueAction(action4); 
    },
    onChange : function(component, event, helper) 
    {
        var selected = event.getSource().get("v.text");
        component.set("v.radiobutton",selected); 
        var action=component.get("c.listofTradingHours");
        action.setParams({"radiobuttonvalue" :selected});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.listofTrading",result);
                               }
                           });
        $A.enqueueAction(action); 
        var action2=component.get("c.getDealershipDetails");
        action2.setParams({"radiobuttonvalue" :selected});
        action2.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.objDealer",result);
                                    this.getuserInformation(component, event, helper); 
                                }
                            });
        
        $A.enqueueAction(action2); 
        
    },
    refreshView: function(component, event, helper) 
    {
        this.onsave(component, event, helper);
    },
    onChangeUser :function(component, event, helper) 
    {
        var selectedValue= event.getSource().get("v.value");
        component.set("v.userName",selectedValue);
    },
    editTradingHours: function(component, event, helper) 
    {
        var targetId =event.target.id;
        var action =component.get("c.getTradingDetails");
        action.setParams({"RecordId" :targetId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.popupflag",true);
                                   component.set("v.objTrading",result);
                               }
                               
                           });
        $A.enqueueAction(action);
    },
    saveTradingHours: function(component, event, helper) 
    {
        var lst=component.get("v.objTrading");
        var action =component.get("c.saveTradingDetails");
        action.setParams({"objTradinglst" :lst});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                       this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   { 
                                        component.set("v.popupflag",false);
                                        this.showToast(component,'Success', result, 'success');
                                        this.onsave(component, event, helper);
                                   }
                               }
                           });
        $A.enqueueAction(action);
    },
    saveAccountDetails: function(component, event, helper) 
    {
        debugger;
        var lst=component.get("v.objDealer");
        var userid =component.get("v.userName");
        var inputCmp = component.find("maxvalue");
        if(!$A.util.isUndefined(lst.VGA_Escalate_Minutes__c) ||!$A.util.isEmpty(lst.VGA_Escalate_Minutes__c))
        {
          var str  =lst.VGA_Escalate_Minutes__c.toString().length;
         if(str<=3)
         {
                   var escalate= lst.VGA_Distribution_Method__c.indexOf('Round Robin');
                   if(escalate != -1 )
                   {
                     var action =component.get("c.saveDealershipDetails");
                     action.setParams({"objDealershiplst" :lst,
                              "Ids":userid});
                     action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                           this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       { 
                                           this.showToast(component,'Success', result, 'success');
                                           this.onsave(component, event, helper);
                                           inputCmp.set("v.errors",null);
                                       }  
                                   }
                               });
                   $A.enqueueAction(action);
               }                    
               else
               {
                 if((!$A.util.isUndefined(userid) ||!$A.util.isEmpty(userid)) &&  userid !='--None--')
                 {
                     var action =component.get("c.saveDealershipDetails");
                     action.setParams({"objDealershiplst" :lst,
                              "Ids":userid});
                      action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                           this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       { 
                                           this.showToast(component,'Success', result, 'success');
                                           this.onsave(component, event, helper);
                                           inputCmp.set("v.errors",null);
                                       }  
                                   }
                               });
                   $A.enqueueAction(action);
                 }
                   else
                   {
                      //alert('Please select Nominate User.')
                      this.showToast(component,'ERROR','Please select Nominate User.', 'error');
                   }
             
               }
         }
        else
        {
        	inputCmp.set("v.errors",[{message:"Please enter only three Number."}]); 
        }   
        }
        else
        {
            var action =component.get("c.saveDealershipDetails");
            action.setParams({"objDealershiplst" :lst,
                              "Ids":userid});
            action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                           this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       { 
                                           this.showToast(component,'Success', result, 'success');
                                           this.onsave(component, event, helper);
                                           inputCmp.set("v.errors",null);
                                       }  
                                   } 
                               });
            $A.enqueueAction(action); 
        }

       
    },
    getlistofTradingHours: function(component, event, helper) 
    {
        var radiobutton;
        if(component.find("myPassenger3"))
        {
            radiobutton=component.find("myPassenger3").get("v.value");
            component.set("v.radiobutton","Commercial");
        }
        else
        {
            radiobutton=false;   
            component.set("v.radiobutton","Passenger");   
        }
        var action=component.get("c.listofTradingHours");
        if(radiobutton ==false)
        {
            action.setParams({"radiobuttonvalue" :'Passenger'});
        }
        else
        {
            action.setParams({"radiobuttonvalue" :'Commercial'});    
        }
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.listofTrading",result);
                               }
                           });
        $A.enqueueAction(action); 
        var action2=component.get("c.getDealershipDetails");
        if(radiobutton ==false)
        {
            action2.setParams({"radiobuttonvalue" :'Passenger'});
        }
        else
        {
            action2.setParams({"radiobuttonvalue" :'Commercial'});    
        }
        action2.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.objDealer",result);
                                    this.getuserInformation(component, event, helper);
                                }
                            });
        $A.enqueueAction(action2);
    },
    showToast : function(cmp, title, message, type ) 
    {
        cmp.set("v.errormsg",message);
        cmp.set("v.title",title);
        
        //---- Setting up severity ----//
        if(type == 'info')
        {
            cmp.set("v.type",'');
            cmp.set("v.icon",'utility:info');
        }
        else if(type == 'warning')
        {
            cmp.set("v.type",'slds-theme--error');
            cmp.set("v.icon",'utility:warning');
        }
            else if(type == 'error')
            {
                cmp.set("v.type",'slds-theme--error');
                cmp.set("v.icon",'utility:error');
            }
                else if(type == 'success')
                {
                    cmp.set("v.type",'slds-theme--success');
                    cmp.set("v.icon",'utility:success');
                }
        
        cmp.set("v.errormsg",message);
        window.setTimeout(function(){
            cmp.set("v.errormsg",'');
        }, 100000);
    },
    onsave : function(component, event, helper)
    {
      var selected =  component.get("v.radiobutton");
      var action=component.get("c.listofTradingHours");
      action.setParams({"radiobuttonvalue" :selected});
      action.setCallback(this, function(actionresult) 
      {
         var state = actionresult.getState();
         if(state === "SUCCESS")
         {
           var result =actionresult.getReturnValue();
           component.set("v.listofTrading",result);
         }
       });
       $A.enqueueAction(action); 
       var action2=component.get("c.getDealershipDetails");
       action2.setParams({"radiobuttonvalue" :selected});
       action2.setCallback(this, function(actionresult) 
       {
         var state = actionresult.getState();
         if(state === "SUCCESS")
         {
          var result =actionresult.getReturnValue();
          component.set("v.objDealer",result);
          this.getuserInformation(component, event, helper); 
         }
       });
      $A.enqueueAction(action2);               
      },
      closebutton:function(component, event, helper)
      {
       component.set("v.popupflag",false);  
      },
      handleError:function(component, event, helper)
      {
        var component = event.getSource();
        $A.util.addClass(component, "error");   
      },
      handleClearError:function(component, event, helper)
      {
        var component = event.getSource();
        $A.util.removeClass(component, "error");   
      },
     getuserInformation :function(component, event, helper)
     {
        var lst=component.get("v.objDealer");
        var radiobutton=component.get("v.radiobutton");
        var action3=component.get("c.listofuser"); 
        action3.setParams({"objdealer" :lst,
                           "selected":radiobutton});   
        action3.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var arrayOfMapKeys = [];
                                    var result =actionresult.getReturnValue();
                                    
                                    for (var singlekey in result)
                                    {
                                        arrayOfMapKeys.push({
                                            key: singlekey,
                                            value: result[singlekey]
                                        });
                                    }
                                    component.set("v.userName",arrayOfMapKeys[0].value);
                                    component.set("v.userMap",arrayOfMapKeys);
                                }
                            });
        $A.enqueueAction(action3);
    }  
})