({
	onload : function(component, event, helper) 
    {
        debugger;
		var RecId =component.get("v.lRecordId");
        var action =component.get("c.getcontactDetail");
		action.setParams({"RecordId" :RecId});
        action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.objcontact",result);                                    
                                     component.set("v.username",result.Email);
                                }
                           });
         $A.enqueueAction(action);
	},
    onSaveButton :function(component, event, helper) 
    {
        if(component.get("v.username") == '')
        {
            return;
        }
       
        if(typeof component.get("v.objcontact.VGA_Username__c") == 'undefined' || component.get("v.objcontact.VGA_Username__c") == '')
        {
            component.set("v.objcontact.VGA_Username__c", component.get("v.username"))
        }
        
        var RecId =component.get("v.lRecordId");
		var lst    =component.get('v.objcontact');
        if(lst.VGA_Partner_User_Status__c !='')
        {
        var action =component.get("c.UpdatePortalUser");
		action.setParams({"objContact" :lst});
        action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                debugger;
                                 if(state == "SUCCESS")
                                 {
                                      var result =actionresult.getReturnValue();
                                      var errorvalue= result.indexOf('Error:')
                                      if(errorvalue != -1 )
                                      {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                        
                                        component.set("v.objcontact.VGA_Username__c", '');
                                      }
                                      else
                                      {
                                        
                                           window.setTimeout(function()
                                           {
                                             
                                              var navEvt = $A.get("e.force:navigateToSObject");
                                              navEvt.setParams({
                                                   "recordId": RecId,
                                                   "slideDevName": "detail",
                                                  "isredirect" :true
                                               });
                                            navEvt.fire();  
                                              $A.get('e.force:refreshView').fire();  

  						                   }, 6000);
                                         
                                         this.showToast(component,'SUCCESS',result, 'success'); 
                                      }

                                      
                                 }
                                   
                           });
         $A.enqueueAction(action);
        }
        else
        {
           this.showToast(component,'WARNING','Please select Status picklist', 'warning'); 
            
        }
	},
    showToast : function(cmp, title, message, type ) 
     {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 4000);
      }
})