({
   
	doInit : function(component, event, helper) 
    {
       helper.getweekstartdate(component, event, helper);
    },
    controllerGoClick:function(component, event, helper)
    {
       helper.getproductdetails(component, event, helper) ;  
    },
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    controllerSaveCampaignDetails:function(component, event, helper) 
    {
		helper.SaveCampaignDetails(component, event, helper);
	},
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
      PickChange : function(component, event, helper) {
        var parentValue = component.find('parentPicklist').get('v.value');
        component.set('v.dependentOptions', component.get('v.dependentPicklist')[parentValue]);
           component.set('v.details', false);
 
        if(parentValue != '')
        component.set('v.disabledPick',false);
        else
        component.set('v.disabledPick',true);
    },
    controlleronclickGo:function(component, event, helper) 
    {
       helper.onclickGo(component, event, helper); 
    },
    childPickChange : function(component, event, helper) 
    {
        component.set('v.details', false);
    }
})