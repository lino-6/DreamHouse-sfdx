({
    //This method is used for get start date of current Week.
    getweekstartdate : function(component, event, helper)
    {
        var pVals = [
            {text:"Volkswagen", value: "Volkswagen"},
            {text:"Skoda", value: "Skoda"}
        ];                                                                                //parent picklist Value. 
 
        var dPick = {
            "Volkswagen" : [
                {text:"Passenger Vehicles (PV)", value: "Passenger Vehicles (PV)"},
                {text:"Commercial Vehicles (CV)", value: "Commercial Vehicles (CV)"}
            ],
            "Skoda" : [
                {text:"Passenger Vehicles (PV)", value: "Passenger Vehicles (PV)"}
               
            ]                                                                            //child picklist Value. 
        };
        component.set('v.parentOptions', pVals);                                        //set parent picklist value .
        component.set('v.dependentPicklist', dPick);                                   //set dependentPicklist picklist value .
        var action=component.get("c.getweekstartdate");                               // call method from apex class.
        action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();                 //get state 
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();      //get result 
                                    component.set("v.startdate",result);            //set startdate in Compoment.
                                    this.getweekEnddate(component, event, helper);  // call enddate of current week.
                                }
                              });
           $A.enqueueAction(action);
        
    },
    //This method is used for get End date of current Week.
    getweekEnddate : function(component, event, helper)
    {
        var action=component.get("c.getweekEnddate");                  // call method from apex class.
        action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();  //get state 
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue(); //get result 
                                    component.set("v.enddate",result);   //set enddate in Compoment.
                                }
                              });
       $A.enqueueAction(action);   
    },
    //This method is used get CampaignActivity Details.
    onclickGo:function(component, event, helper)
    {
         var brand =component.find("parentPicklist");   //To find aura Id of Assigned picklist.
         var brandvalue =brand.get("v.value");         //To get Assigned value by aura Id.
        //Status PickList
        var subbrand =component.find("childPicklist"); //To find aura Id of status picklist.
        var subbrandvalue =subbrand.get("v.value");   //To get status value by aura Id.
        if(!brandvalue)
        {
           this.showToast(component,'error', 'Please select Brand', 'error'); //To show popup
        }
        else if(!brandvalue && !subbrandvalue)
        {
            this.showToast(component,'error', 'Please select Brand and Sub Brand', 'error'); //To show popup
        }
        else if( !subbrandvalue)
        {
            this.showToast(component,'error', 'Please select Sub Brand', 'error'); //To show popup
        }
        else
        {
           
          var startdate =component.get("v.startdate");  //get startdate of 
          var enddate   =component.get("v.enddate"); 
          var action    =component.get("c.getproductDetails"); // call method from apex class.
            
        //  alert(JSON.stringify(startdate)+ ""+JSON.stringify(enddate));  
          action.setParams({"weekstartDate":JSON.stringify(startdate),
                           "weekendDate"   :JSON.stringify(enddate),
                           "brand"    :brandvalue,
                           "subbrandvalue" :subbrandvalue});  // set value in parmeters
          action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState(); // To get state 
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue(); //To get result 
                                     component.set("v.listofProduct",result);  //set list of CampaignActivity
                                     component.set("v.details",true);   //set Boolean value. 
                                }
                              });
           $A.enqueueAction(action); 
        }
        
    },
    //This method is used refresh the table.
    refreshView: function(component, event, helper) 
    {
        $A.get('e.force:refreshView').fire();
    },
    //This method is used upsert CampaignActivity.
    SaveCampaignDetails: function(component, event, helper)
    {
         var lst   =component.get("v.listofProduct");    //get list of CampaignActivity
         var brand =component.find("parentPicklist");   //To find aura Id of Assigned picklist.
         var brandvalue =brand.get("v.value");  //To get Assigned value by aura Id.
        //Status PickList
        var subbrand =component.find("childPicklist"); //To find aura Id of status picklist.
        var subbrandvalue =subbrand.get("v.value");  //To get status value by aura Id.
         var action=component.get("c.saveCampaignInformation"); // call method from apex class.
         action.setParams({"CampaignDetails" :JSON.stringify(lst),
                           "brand" :brandvalue,
                           "subbrand" :subbrandvalue}); // set value in parmeters
         action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState(); // To get state 
                            if(state === "SUCCESS") 
                            {
                                var result =actionresult.getReturnValue(); //To get result 
                                var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); //To show error message
                                   }
                                   else
                                   { 
                                           this.showToast(component,'Success', result, 'success');//To show Sucess message
                                   }  
                               }
                          });
        $A.enqueueAction(action);
    },
    //This method is used call pop up message.
    showToast : function(cmp, title, message, type ) 
     {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 5000);
      }
})