({
    onLoad : function(component, event, helper){
        component.set('v.loading',false);
        component.set('v.VINDATA',null);
		component.set('v.ClaimPolicy',null);
		component.set('v.ClaimResult',null);
		component.set('v.SearchError',false);
		component.set('v.SearchSection',true);
		component.set('v.ConfirmationClaimPopup',false);
    },
	SearchVIN : function(component, event, helper) {
		component.set('v.ServiceError',{'ServiceDate':{'err':false,'errMsg':''},'ServiceKM':{'err':false,'errMsg':''}, 'ServiceRego':{'err':false,'errMsg':''},'ServiceInterval':{'err':false,'errMsg':''}});
		component.set('v.VINDATA',null);
		component.set('v.ClaimPolicy',null);
		component.set('v.ClaimResult',null);
		component.set('v.SearchError',false);
		component.set('v.ConfirmationClaimPopup',false);
		component.set('v.loading',true);
		var SearchText=component.find('SearchText').getElement('v.value').value;
		if(SearchText!=undefined && SearchText.trim()!=''){
			component.set('v.SearchText',SearchText.trim());
			helper.getPolicies(component,event,helper);
		}else{
			component.set('v.loading',false);
		}
	},
    cancelClick : function(component, event, helper) {
        component.set('v.VINDATA',null);
		component.set('v.ClaimPolicy',null);
		component.set('v.SearchError',false);	
		component.set('v.PolicySection',false);
		component.set('v.ClaimSection',false);
		component.set('v.ReportSection',false);
		component.set('v.ServiceClaimPopup',false);
		component.set('v.ServiceIntervalPopup',false);
		component.set('v.ConfirmationClaimPopup',false);
        component.set('v.loading',true);
		helper.getPolicies(component,event,helper);
    },
    backHome : function(component, event, helper) {
		component.set('v.SearchSection',true);
		component.set('v.VINDATA',null);
		component.set('v.ClaimPolicy',null);
		component.set('v.SearchError',false);	
		component.set('v.PolicySection',false);
		component.set('v.ClaimSection',false);
		component.set('v.ReportSection',false);
		component.set('v.ServiceClaimPopup',false);
		component.set('v.ServiceIntervalPopup',false);
		component.set('v.ConfirmationClaimPopup',false);
		var TempVal=setInterval(
			function(){
				component.find('SearchText').getElement('v.value').value='';
				clearInterval(TempVal);
			}, 
			200
		);
	},
	onClaim : function(component, event, helper) {
		component.set('v.ServiceError',{'ServiceDate':{'err':false,'errMsg':''},'ServiceKM':{'err':false,'errMsg':''}, 'ServiceRego':{'err':false,'errMsg':''},'ServiceInterval':{'err':false,'errMsg':''}});
		var TargetId=event.target.id;
		if(TargetId!=undefined){
			var VINDATA=component.get('v.VINDATA.PolicyMap.PRESENT_SVC');
			for(var i=0;i<VINDATA.length;i++){
				if(VINDATA[i].PolicyId==TargetId){
					component.set('v.ClaimPolicy',VINDATA[i]);
					component.set('v.PolicySection',false);
					component.set('v.ClaimSection',true);
					component.set('v.IntervalList',VINDATA[i].AllIntervalList);
					component.set('v.InvoiceDetails',null);
					component.set('v.ClaimObj',{'ServiceDate':null,'ServiceKMS':null,'RONumber':null,'ServiceInterval':null});
					component.find('ServiceDate').set('v.value',undefined);
					component.find('ServiceKMS').set('v.value',undefined);
					component.find('RONumber').set('v.value',undefined);
					component.find('ServiceDate').set('v.placeholder','Please select date');
					break;
				}
			}
		}
		
	},
	showDetails : function(component, event, helper) {
		var TargetId=event.target.id;
		var PolicyId=TargetId.split('_')[0];
		var DisplayType=TargetId.split('_')[1];
		if(TargetId!=undefined){
			var VINDATA=component.get('v.VINDATA.PolicyMap.PRESENT_SVC');
			for(var i=0;i<VINDATA.length;i++){
				if(VINDATA[i].PolicyId==PolicyId){
					switch(DisplayType){
						case 'CLAIM' :{
							component.set('v.ClaimDisplayList',VINDATA[i].ClaimData);
							component.set('v.ServiceClaimPopup',true);
							break;
						}
						case 'REMAIN' :{
							component.set('v.DisplayList',VINDATA[i].RemainingIntervalList);
							component.set('v.popupheading','Service Remaining');
							component.set('v.ServiceIntervalPopup',true);
							break;
						}
						case 'MISSED' :{
							component.set('v.DisplayList',VINDATA[i].MissedIntervalList);
							component.set('v.popupheading','Service Expired');
							component.set('v.ServiceIntervalPopup',true);
							break;
						}

					}
					break;
				}
			}
		}
	},
	onClosePopup : function(component, event, helper) {
		component.set('v.ServiceClaimPopup',false);
		component.set('v.ServiceIntervalPopup',false);
		component.set('v.ClaimDisplayList',[]);
		component.set('v.DisplayList',[]);
	},
	onDatePress : function(component, event, helper) {
		component.find('ServiceDate').set('v.value','');
	},
	onKeyPress : function(component, event, helper) {
		
		var val=component.find('ServiceKMS').get('v.value');
		
		if((''+val).length>8){
			component.find('ServiceKMS').set('v.value',(''+val).substring(0,8));
		}

	},
	Validate : function(component, event, helper) {
		component.set('v.ServiceError',{'ServiceDate':{'err':false,'errMsg':''},'ServiceKM':{'err':false,'errMsg':''}, 'ServiceRego':{'err':false,'errMsg':''},'ServiceInterval':{'err':false,'errMsg':''}});
		component.set('v.loading',true);
		var ServiceError=component.get('v.ServiceError');

		var bolz=false;
		var ServiceInterval=component.find('ServiceInterval');
		var ServiceDate=component.find('ServiceDate');
		var ServiceKMS=component.find('ServiceKMS');
		var ServiceRO=component.find('RONumber');
		//console.log(ServiceInterval);
		//console.log(ServiceDate);
		//console.log(ServiceKMS);
		//console.log(ServiceRO);
		var data={
			'ServiceDate':ServiceDate.get('v.value'),
			'ServiceKMS':ServiceKMS.get('v.value'),
			'RONumber':ServiceRO.get('v.value'),
			'ServiceInterval':ServiceInterval.getElement('v.value').value
		};
		//console.log(data);
		if(ServiceInterval.getElement('v.value')==undefined || ServiceInterval.getElement('v.value').value==''){
			bolz=true;
			$A.util.removeClass(ServiceInterval,'fmbr1');
			$A.util.addClass(ServiceInterval,'slds-has-error');
			ServiceError.ServiceInterval.err=true;
			ServiceError.ServiceInterval.errMsg='Please choose service interval';
		}else{
			$A.util.addClass(ServiceInterval,'fmbr1');
			$A.util.removeClass(ServiceInterval,'slds-has-error');
			ServiceError.ServiceInterval.err=false;
			ServiceError.ServiceInterval.errMsg='';
		}
		if(ServiceDate.get('v.value')==undefined || ServiceDate.get('v.value')==''){
			bolz=true;
			$A.util.removeClass(ServiceDate,'fmbr1');
			$A.util.addClass(ServiceDate,'dateinvalid');
			ServiceError.ServiceDate.err=true;
			ServiceError.ServiceDate.errMsg='Please select service date';
		}else{
			$A.util.addClass(ServiceDate,'fmbr1');
			$A.util.removeClass(ServiceDate,'dateinvalid');
			ServiceError.ServiceDate.err=false;
			ServiceError.ServiceDate.errMsg='';
		}
		if(ServiceKMS.get('v.value')==undefined ){
			bolz=true;
			$A.util.removeClass(ServiceKMS,'fmbr1');
			$A.util.addClass(ServiceKMS,'dateinvalid');
			ServiceError.ServiceKM.err=true;
			ServiceError.ServiceKM.errMsg='Please enter service Kilometre';
		}else{
			$A.util.addClass(ServiceKMS,'fmbr1');
			$A.util.removeClass(ServiceKMS,'dateinvalid');
			ServiceError.ServiceKM.err=false;
			ServiceError.ServiceKM.errMsg='';
		}
		if(ServiceRO.get('v.value')==undefined || ServiceRO.get('v.value').trim()==''){
			bolz=true;
			$A.util.removeClass(ServiceRO,'fmbr1');
			$A.util.addClass(ServiceRO,'dateinvalid');
			ServiceError.ServiceRego.err=true;
			ServiceError.ServiceRego.errMsg='Please enter Service RO Number';
		}else{
			$A.util.addClass(ServiceRO,'fmbr1');
			$A.util.removeClass(ServiceRO,'dateinvalid');
			ServiceError.ServiceRego.err=false;
			ServiceError.ServiceRego.errMsg='';
		}
		//console.log(bolz);
		component.set('v.ServiceError',ServiceError);
		if(!bolz){
			component.set('v.ClaimObj',data);
			helper.ValidateClaim(component,event,helper);
		}else{
			component.set('v.loading',false);
		}

	},
	closeToast : function(cmp, event, helper){
    	cmp.set("v.errormsg",'');
	},
	Claim : function(component, event, helper) {
		component.set('v.loading',true);
		component.set('v.ConfirmationClaimPopup',false);
		helper.claimService(component,event,helper);
	},
	presentSVC_Click : function(component, event, helper) {
		component.set('v.AccordionCheck.PRESENT_SVC',component.get('v.AccordionCheck.PRESENT_SVC')?false:true);
	},
	present_Click : function(component, event, helper) {
		component.set('v.AccordionCheck.PRESENT',component.get('v.AccordionCheck.PRESENT')?false:true);
	},
	future_Click : function(component, event, helper) {
		component.set('v.AccordionCheck.FUTURE',component.get('v.AccordionCheck.FUTURE')?false:true);
	},
	history_Click : function(component, event, helper) {
		component.set('v.AccordionCheck.PAST',component.get('v.AccordionCheck.PAST')?false:true);
	},
	ConfirmClaim : function(component, event, helper) {
		component.set('v.ConfirmationClaimPopup',true);
	},
	ConfirmNoClaim : function(component, event, helper) {
		component.set('v.ConfirmationClaimPopup',false);
		component.set('v.ReportSection',false);
		component.set('v.loading',true);
		helper.getPolicies(component,event,helper);
	},
    closeACK : function(component, event, helper) {
        component.set('v.ACKServiceClaimPopup',false);
    }
})