({
	getPolicies : function(component,event,helper) {
		var act=component.get('c.collectVINDetails');
		act.setParams({'VIN':component.get('v.SearchText')});
		act.setCallback(this,function(resp){
			if(resp.getState()=='SUCCESS'){
				var res=resp.getReturnValue();
				if(res.pass){
					component.set('v.data',JSON.stringify(res.result));	
					component.set('v.VINDATA',res.result);

					component.set('v.AccordionCheck.PRESENT_SVC',component.get('v.VINDATA.PolicyMap.PRESENT_SVC').length==0?false:true);
					component.set('v.AccordionCheck.PRESENT',component.get('v.VINDATA.PolicyMap.PRESENT').length==0?false:true);
					component.set('v.AccordionCheck.FUTURE',component.get('v.VINDATA.PolicyMap.FUTURE').length==0?false:true);
					component.set('v.AccordionCheck.PAST',component.get('v.VINDATA.PolicyMap.PAST').length==0?false:true);
					
					component.set('v.SearchSection',false);
					component.set('v.PolicySection',true);
				}else{
					component.set('v.SearchError',true);
				}
			}else{
				helper.showToast(component,'ERROR','Please check Internet Connection', 'error');
			}
			component.set('v.loading',false);
			
		});
		$A.enqueueAction(act);
	},
	ValidateClaim : function(component,event,helper) {
		var act=component.get('c.ValidateClaim');
		act.setParams({          
		 'IntervalId':component.get('v.ClaimObj.ServiceInterval'), 
		 'PolicyId':component.get('v.ClaimPolicy.PolicyId'),  
		 'ServiceDate':component.get('v.ClaimObj.ServiceDate'),  
		 'KM':component.get('v.ClaimObj.ServiceKMS'),
		 'VehicleId':component.get('v.VINDATA.VehicleId')
		});
		act.setCallback(this,function(resp){
			if(resp.getState()=='SUCCESS'){
				var res=resp.getReturnValue();
				if(res.pass){
					component.set('v.InvoiceDetails',res.result);
					component.set('v.ClaimSection',false);
					component.set('v.ReportSection',true);
				}else{
					var MSG_ERR=res.Error;
					var ServiceDate=component.find('ServiceDate');
					var ServiceKMS=component.find('ServiceKMS');

					if(MSG_ERR.length==1){
						helper.showToast(component,'ERROR',MSG_ERR.join(','), 'error');
					}else{
						switch(MSG_ERR[0]){
							case 'KM' : {
								$A.util.removeClass(ServiceKMS,'fmbr1');
								$A.util.addClass(ServiceKMS,'dateinvalid');
								component.set('v.ServiceError.ServiceKM',{'err':true,'errMsg':MSG_ERR[1]});
								break;
							}
							case 'DATE' : {
								$A.util.removeClass(ServiceDate,'fmbr1');
								$A.util.addClass(ServiceDate,'dateinvalid');
								component.set('v.ServiceError.ServiceDate',{'err':true,'errMsg':MSG_ERR[1]});
								break;
							}
							case 'POLICY':{
								$A.util.removeClass(ServiceDate,'fmbr1');
								$A.util.addClass(ServiceDate,'dateinvalid');
								$A.util.removeClass(ServiceKMS,'fmbr1');
								$A.util.addClass(ServiceKMS,'dateinvalid');
								component.set('v.ServiceError.ServiceKM',{'err':true,'errMsg':MSG_ERR[2]});
								component.set('v.ServiceError.ServiceDate',{'err':true,'errMsg':MSG_ERR[1]});
								break;
							}
						}
					}
				}
			}else{
				helper.showToast(component,'ERROR','Please check Internet Connection', 'error');
				//Lino Component Error 
			}
			component.set('v.loading',false);
			
		});
		$A.enqueueAction(act);
	},
	showToast : function(cmp, title, message, type, display ) {
        cmp.set("v.errormsg",message);
        cmp.set("v.title",title);
        //---- Setting up severity ----//
        if(type == 'info')
        {
            cmp.set("v.type",'');
            cmp.set("v.icon",'utility:info');
        }
        else if(type == 'warning')
        {
            cmp.set("v.type",'slds-theme--error');
            cmp.set("v.icon",'utility:warning');
        }
        else if(type == 'error')
        {
                cmp.set("v.type",'slds-theme--error');
                cmp.set("v.icon",'utility:error');
        }
        else if(type == 'success')
        {
                    cmp.set("v.type",'slds-theme--success');
                    cmp.set("v.icon",'utility:success');
        }
        cmp.set("v.errormsg",message);
        window.setTimeout(function(){
            if(type == 'success')
            {
                //if(display == 'Hide')
                //{
                	cmp.set("v.ShowLeadDetails", false); 
                //}
            }
            cmp.set("v.errormsg",'');
        }, 5000);
	},
	claimService : function(component,event,helper) {
		var obj={};
		obj.IntervalId=component.get('v.ClaimObj.ServiceInterval');
		obj.PolicyId=component.get('v.ClaimPolicy.PolicyId');
		obj.DealerId=component.get('v.VINDATA.LoggedInUser.Contact.AccountId'),
		obj.ServiceDate=component.get('v.ClaimObj.ServiceDate') ;
		obj.AssetId=component.get('v.VINDATA.VehicleId');
		obj.RONUmber=component.get('v.ClaimObj.RONumber');
		obj.KM=component.get('v.ClaimObj.ServiceKMS') ;
		//console.log(obj);

		var act=component.get('c.saveClaim');
		act.setParams({          
		 'data' : JSON.stringify(obj)
		});
		act.setCallback(this,function(resp){
			if(resp.getState()=='SUCCESS'){
				var res=resp.getReturnValue();
				//console.log(JSON.stringify(res));
				if(res.pass){
					//helper.showToast(component,'SUCCESS','done', 'success');
					component.set('v.SearchSection',true);
					component.set('v.VINDATA',null);
					component.set('v.ClaimPolicy',null);
					component.set('v.SearchError',false);	
					component.set('v.PolicySection',false);
					component.set('v.ClaimSection',false);
					component.set('v.ReportSection',false);
					component.set('v.ServiceClaimPopup',false);
					component.set('v.ServiceIntervalPopup',false);
					component.set('v.ConfirmationClaimPopup',false);
					component.set('v.ClaimObj',null);
					component.set('v.ClaimResult',res.result);
					//console.log(component.get('v.ClaimResult'));
					component.set('v.ACKServiceClaimPopup',true);

					
					//var TempVal=setInterval(
						//function(){
							//component.set('v.ACKServiceClaimPopup',false);
							//clearInterval(TempVal);
						//}, 
						//10000
					//);
				}else{
					//console.log(res.Error);
					helper.showToast(component,'ERROR',''+res.Error.join(','), 'error');
					///Lino Component Error 
				}
			}else{
				helper.showToast(component,'ERROR','Please check Internet Connection', 'error');
				//Lino Component Error 
			}
			component.set('v.loading',false);
			
		});
		$A.enqueueAction(act);

	}
})