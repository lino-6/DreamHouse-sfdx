({
	doInit : function(component, event, helper) 
    {
        component.set('v.showIASpinner','true');
        helper.getloggingDetails(component, event, helper);
		helper.getLeadList(component, event, helper);     
        component.set('v.showIASpinner','false');
	},
    leadTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make lead tab active and show tab data
        component.find("leadId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("leaddataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    commentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make comment tab active and show tab data
        component.find("commentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("commentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    historyTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make histroy tab active and show tab data
        component.find("histroyId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("histroydataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    ownershipsTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make ownership tab active and show tab data
        component.find("ownershipId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("ownershipdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        
        if(component.get("v.leadId")!='' && component.get("v.leadId")!=undefined){
            console.log('ssss');
            helper.getownerships(component, event,helper); 
        }
        
    },
    
    controllerGetEditLead :function(component, event, helper) 
    {
        //get lead detail
        component.set('v.assignEventownerId','');
        component.set('v.hasImageSrc','');
        helper.getEditLead(component, event, helper);
        helper.getAttachment(component, event,helper);
        helper.getLeadHistory(component, event,helper);
        helper.getNoteslist(component, event,helper);
        helper.getownerships(component, event,helper); 
    },
    controlleronchangeconsultantName:function(component, event, helper) 
    {
        //get lead detail
        helper.onChangeGetValue(component, event, helper);
    },
    controllerSaveLeadDetails:function(component, event, helper) 
    {
        //save Lead Details
        helper.saveLeadDetails(component, event, helper);
    },
    controllerAddComment:function(component, event, helper) 
    {
        helper.addComment(component, event, helper);
    },
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    controllerSaveNotesDetail:function(component, event, helper) 
    {
		helper.saveNotesDetail(component, event, helper);
	},
    controllerOnclickAccept:function(component, event, helper) 
    {
		helper.onclickAccept(component, event, helper);
	},
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    ControlleronChangeConsultant:function(component, event, helper) 
    {
		helper.onchangeConsultant(component, event, helper);
	},
    ControlleronChangeGetstatus:function(component, event, helper) 
    {
		helper.onChangeGetstatus(component, event, helper);
	},
    controlleronClickdownload:function(component, event, helper) 
    {
		helper.onClickdownload(component, event, helper);
	},
    controlleronchangeresignpicklist:function(component, event, helper) 
    {
		helper.onchangeresignpicklist(component, event, helper);
	},
    controlleronchangeresignEventpicklist : function(component,event, helper){
        helper.onchangeresignEventpicklist(component, event, helper);
    },
    controlleronRefreshLeadHistroy:function(component, event, helper) 
    {
		helper.onRefreshLeadHistroy(component, event, helper);
	},
    controlleronRefreshNotes:function(component, event, helper) 
    {
		helper.onRefreshNotes(component, event, helper);
	},
    renderPage: function(component, event, helper)
    {
        helper.renderPage(component);
    },
    controlleronClickaddress: function(component, event, helper)
    {
        helper.onClickaddress(component, event, helper);
    },
    controllerclosemappopup :function(component, event, helper)
    {
        helper.closemappopup(component, event, helper);
    },
    sortAssigned:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Assigned_to_Dealership__c');
         helper.sortHelper(component, event, helper);
    },
    sortStatus:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'Status');
         helper.sortHelper(component, event, helper);
    },
    sortOwner:function(component, event, helper)
    {
        
         component.set("v.selectedTabsoft", 'VGA_Owner_Name__c');
         helper.sortHelper(component, event, helper);
    },
    sortoutcome:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Outcome__c');
         helper.sortHelper(component, event, helper);
    },
    sortEscalation:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Escalation__c');
         helper.sortHelper(component, event, helper);
    },
    attachmentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        component.set('v.LeadCheck',false); 
        component.set('v.hasImageSrc','');
        component.find("attachmentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("attachmentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
        component.set('v.DocusignStage1Link',false);
        component.set('v.DocusignStage2Link',false);
        component.set('v.baseUrl','');
        component.set('v.DocusignStage1Url','');
        component.set('v.DocusignStage2Url','');
        component.set("v.hasImageSrc",false);
        console.log('goooo');
        if(component.get("v.leadId")!='' && component.get("v.leadId")!=undefined){
            console.log('ssss');
            helper.getAttachment(component, event,helper); 
        }
        
    },
    navigate : function(component,event,helper){
        if(component.get('v.LeadCheck')){
            window.open(component.get('v.DocusignStage1Url')+component.get('v.baseUrl'));
        }else{
            helper.showToast(component,'ERROR','Unable to find Dealer Code / Primary Id for selected Lead!', 'error');
        }
        //window.location.replace(component.get('v.DocusignStage1Url')+component.get('v.baseUrl'));
        

    },
    ControllerErrorHandle:function(component, event, helper)
    {
        helper.handleError(component, event, helper);
    },
    ControllerhandleClearError:function(component, event, helper)
    {
        helper.handleClearError(component, event, helper);
    },
    onChangeFunction: function(component,event) {
        var lst  = component.get("v.objLead");
        lst.VGA_Outcome__c = null;
    }
})