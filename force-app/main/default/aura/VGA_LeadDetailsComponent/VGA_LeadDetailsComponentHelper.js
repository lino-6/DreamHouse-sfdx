({
    getloggingDetails :function(component, event, helper)
    {
     var action=component.get("c.getlogginguser");
     component.set('v.showIASpinner',true);
     action.setCallback(this, function(actionresult)
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                     component.set("v.objName",result);
                                     component.set('v.showIASpinner',false);
                                 
                                }
                              });
     $A.enqueueAction(action);
    },
    getLeadList : function(component, event, helper)
    {
        // Initialize input select options for Assigned
          component.set('v.showIASpinner',true);
        var opts = [
            { "class": "optionClass", label: "Last 30 days", value: "Last 30 days", selected: "true" },
            { "class": "optionClass", label: "Today", value: "Today" },
            { "class": "optionClass", label: "Last 7 days", value: "Last 7 days" },
            { "class": "optionClass", label: "Last 90 days", value: "Last 90 days" },
            { "class": "optionClass", label: "More than 90 days", value: "More than 90 days" }
            ];
        component.find("InputSelectSingle").set("v.options", opts);
        // Initialize input select options for Status
        var Statusopts = [
            { "class": "optionClass", label: "New and Accepted", value: "New and Accepted", selected: "true" },
            { "class": "optionClass", label: "New", value: "New" },
            { "class": "optionClass", label: "Accepted", value: "Accepted" },
            { "class": "optionClass", label: "All", value: "All" },
            { "class": "optionClass", label: "Closed", value: "Closed" }
            ];
        component.find("InputSelectStatus").set("v.options", Statusopts);
        //To get Account Name
         var action =component.get("c.getAccountDetails");
         action.setCallback(this, function(actionResult)
          {
                    var state = actionResult.getState();
                    if(state =="SUCCESS")
                    {
                        var arrayOfMapKeys = [];
                        var result =actionResult.getReturnValue();
                        for (var singlekey in result)
                        {
                           arrayOfMapKeys.push({
                            key: singlekey,
                            value: result[singlekey]
                                              });
                        }
                        component.set('v.AccountId', arrayOfMapKeys[0].value);
                        component.set('v.AccountlstKey', arrayOfMapKeys);
                        component.set('v.showIASpinner',false);
                     }
          });
        // get all contactName
        
         var action2 =component.get("c.getcontactDetails");
         component.set('v.showIASpinner',true);
         action2.setCallback(this, function(actionResult)
          {
                    var state = actionResult.getState();
                    
                    if(state =="SUCCESS")
                    {
                        var arrayOfMapKeys = [];
                        var result =actionResult.getReturnValue();
                        for (var singlekey in result)
                        {
                            arrayOfMapKeys.push({
                                    key: singlekey,
                                    value: result[singlekey]
                                              });
                        }
                        component.set('v.ConsultantId', arrayOfMapKeys[0].value);
                        component.set('v.lstKey', arrayOfMapKeys);
                        component.set('v.showIASpinner',false);
                     }
          });
           $A.enqueueAction(action);
           $A.enqueueAction(action2);
           this.getUserRole(component, event, helper);
    },
    clearAll: function(component, event)
    {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++)
        {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    getEditLead: function(component, event, helper)
    {
        component.set('v.reassignEventlstKey',[]);
        component.set('v.ObjEvent',null);
        component.set('v.assignEventownerId','');
        component.set('v.act',null);
        component.set('v.tempStatus',undefined);
        
        

        component.set('v.showIASpinner',true);
        return new Promise($A.getCallback(function (resolve, reject)
        {
            var targetId;
            if(event.target.id)
            {
                  targetId=event.target.id;
                  component.set("v.leadId",targetId);
             }
            else
            {
                targetId=  component.get("v.leadId");
            }

            var action1 = component.get('c.getcampaignMap');
            action1.setParams({"RecordId" :targetId});
                action1.setCallback(this,function(response)
                {
                //store state of response
                var state = response.getState();
                if (state === "SUCCESS") {
                    //set response value(map) in myMap attribute on component.
                    component.set('v.campaignName',response.getReturnValue());
                }
                    
            });
            $A.enqueueAction(action1);

             var action =component.get("c.getLeadDetails");
             action.setParams({"RecordId" :targetId});
             action.setCallback(this, function(actionresult)
            {
                var state = actionresult.getState();
                
                if(state == "SUCCESS")
                {
                    
                        var result =actionresult.getReturnValue();
                        component.set("v.DetailFlag",true);
                        component.set("v.objLead",result);
                        component.set('v.tempStatus',result.VGA_Status__c==undefined?undefined:result.VGA_Status__c);
                        component.set("v.ObjEvent",null);
                        component.set('v.showIASpinner',false);
						console.log(component.get('v.tempStatus'));
                        var inputCmp = component.find("status");
                        var outcomeCmp = component.find("outcome");
                        
                        if(inputCmp){
                            inputCmp.set("v.errors",null);
                        }
                        

                        if(outcomeCmp){
                            outcomeCmp.set("v.errors",null);
                        }

                        if(result!=null && result!=undefined)
                        {

                        if(result.VGA_Request_Type__c=='Test Drive')
                        {
                            if(result.Events != undefined && result.Events[0]!=undefined)
                            {
                                component.set("v.ObjEvent",result.Events[0]);
                                var action3 =component.get("c.getPilotContactsUsers");
                                action3.setCallback(this, function(actionResult1)
                                {
                                            var state1 = actionResult1.getState();
                                    
                                            if(state1 =="SUCCESS")
                                            {
                                                
                                            var result1 =actionResult1.getReturnValue();
                                            component.set('v.reassignEventlstKey', result1);
                                            var picklistTimer=setInterval(function()
                                            {
                                                if(component.get("v.ObjEvent")!=null)
                                                {
                                                component.set('v.assignEventownerId',component.get("v.ObjEvent.OwnerId"));
                                                }
                                                clearInterval(picklistTimer);
                                            },200);
                                            }
                                });
                                $A.enqueueAction(action3);
                            }
                        }
                        }
                }
            });
             $A.enqueueAction(action);
             var action2 =component.get("c.getresign");
             action2.setParams({"RecordIds" :targetId});
             action2.setCallback(this, function(actionResult)
              {
                        var state = actionResult.getState();
                        if(state =="SUCCESS")
                        {
                            var arrayOfMapKeys = [];
                            var result =actionResult.getReturnValue();

                            for (var singlekey in result)
                            {
                               arrayOfMapKeys.push({
                                key: singlekey,
                                value: result[singlekey]
                                                  });
                            }
                           component.set('v.reassignlstKey', arrayOfMapKeys);
                         }
              });
             $A.enqueueAction(action2);
             }));
    },
    onChangeGetValue: function(component, event, helper)
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id.
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var conId =component.get('v.ConsultantId');
        component.set('v.showIASpinner',true);
        var action=component.get("c.getLeadlist"); //To call Apex controller method
       
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":conId,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue(); // get return result value.
                                component.set("v.listofLead",result); // set list of lead on component.
                                component.set("v.DetailFlag",false);
                                component.set("v.maxPage", Math.floor((result.length+19)/20));
                                this.renderPage(component);
                                component.set('v.showIASpinner',false);
                            }
                          });
        $A.enqueueAction(action);
    },
    onChangeGetstatus: function(component, event, helper)
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id.
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var conId =component.get('v.ConsultantId');
        component.set('v.showIASpinner',true);
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":conId,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue(); // get return result value.
                                component.set("v.listofLead",result); // set list of lead on component.
                                component.set("v.DetailFlag",false);
                                component.set("v.maxPage", Math.floor((result.length+19)/20));
                                this.renderPage(component);
                                component.set('v.showIASpinner',false);
                            }
                          });
        $A.enqueueAction(action);
    },
    onchangeConsultant: function(component, event, helper)
    {
        var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var selectedValue= event.getSource().get("v.value");
        component.set('v.showIASpinner',true);
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":selectedValue,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue(); // get return result value.
                                component.set("v.listofLead",result); // set list of lead on component.
                                component.set("v.ConsultantId",selectedValue);
                                component.set("v.DetailFlag",false);
                                component.set("v.maxPage", Math.floor((result.length+19)/20));
                                this.renderPage(component);
                            }
                              component.set('v.showIASpinner',false);
                          });
        $A.enqueueAction(action);
    },
    saveLeadDetails: function(component, event, helper)
    {
         var flag = false;
         var lst  = component.get("v.objLead");//Get lead details
         var ownerIds=component.get("v.assignownerId");//Get lead details
         var eve=component.get('v.ObjEvent');
         var newEveOwnerId=component.get('v.assignEventownerId');
         var eveFlag=false;
        component.set('v.showIASpinner',true);
        
        if(eve!=undefined){
          if(eve.OwnerId!=undefined){
            if(newEveOwnerId!=undefined){
              if(newEveOwnerId!=eve.OwnerId){
                eveFlag=true;
              }
            }
          }
         }
         var action=component.get("c.updateLeadDetails"); //To call Apex controller method
         
         if($A.util.isUndefined(ownerIds) || $A.util.isEmpty(ownerIds))
          {
              if(eveFlag){
                action.setParams({"objLead" :lst,"eventId":eve.Id,"EventOwnerId":newEveOwnerId});
              }else{
                action.setParams({"objLead" :lst});
              }          //To set parameter on Lead
          }
          else
          {
            if(eveFlag){
              action.setParams({"objLead" :lst,
                                "ownerIds" :ownerIds,"eventId":eve.Id,"EventOwnerId":newEveOwnerId});
            }else{
              action.setParams({"objLead" :lst,
                           "ownerIds" :ownerIds});
            }
                         //To set parameter on Lead
            flag=true
          }
          
        var inputCmp = component.find("outcome");
		var tempStatus=component.get('v.tempStatus');
  
        
        if($A.util.isUndefined(lst.VGA_Status__c) ||
            $A.util.isEmpty(lst.VGA_Status__c) )
        {
            if(tempStatus!=undefined ){  
                   if((tempStatus=='Accepted' || tempStatus=='Closed') && (lst.VGA_Status__c==''|| lst.VGA_Status__c==undefined )){
                       component.set('v.showIASpinner',false);
                       var inputCmp = component.find("status");
                       inputCmp.set("v.errors",[{message:"Please select Status."}]); 
                       
                       var outcomeCmp = component.find("outcome");
                       outcomeCmp.set("v.errors",null);
                       return;
                   } 
            }else if(tempStatus==undefined && lst.Status!='New'){
                component.set('v.showIASpinner',false);
                var inputCmp = component.find("status");
                inputCmp.set("v.errors",[{message:"Please select Status."}]); 
                
                var outcomeCmp = component.find("outcome");
                outcomeCmp.set("v.errors",null);
                return;
            }
            
        }

        else if(lst.VGA_Status__c =='Closed' 
              && ($A.util.isUndefined(lst.VGA_Outcome__c) 
                  || $A.util.isEmpty(lst.VGA_Outcome__c)))
        {
            component.set('v.showIASpinner',false);
            
            var statusCmp = component.find("status");
            statusCmp.set("v.errors",null);

           
           var inputCmp = component.find("outcome");
           inputCmp.set("v.errors",[{message:"Please select Outcome."}]);
            return;
        }
        //else
        //{
            //debugger;
            var inputCmp = component.find("status");
            var outcomeCmp = component.find("outcome");

            action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            
                            if(state == "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue(); // get return result value.
                                 if(result.search(/Error/gi)!=-1){
                                    this.showToast(component,'Error', result, 'error');
                                 }else{
                                  this.showToast(component,'Success', 'The lead was successfully saved.', 'success');
                                  this.getlistvalue(component, event, helper);
                                  if(flag == true)
                                  {
                                   component.set("v.DetailFlag",false);
                                   inputCmp.set("v.errors",null);
                                   outcomeCmp.set("v.errors",null);
                                  }
                                  else
                                  {
                                   component.set("v.DetailFlag",true);
                                   inputCmp.set("v.errors",null);
                                   outcomeCmp.set("v.errors",null);
                                   this.getEditLead(component, event, helper);
                                   component.set("v.assignownerId",'');
                                  }
                                 }
                            }
                           component.set('v.showIASpinner',false);
                          });
        $A.enqueueAction(action);
        //}
    },
    addComment :function(component, event, helper)
    {
       component.set("v.popupflag",true);
    },
    refreshView: function(component, event, helper)
    {
         $A.get('e.force:refreshView').fire();
    },
    saveNotesDetail:function(component, event, helper)
    {
       var commentsvalue =component.get('v.commentvalue');
       var ledId =component.get('v.leadId');
       var titlevalue = commentsvalue.substring(0,10);
         component.set('v.showIASpinner',true);
       var action=component.get("c.createnotes"); //To call Apex controller method
       action.setParams({"commentvalue" :commentsvalue,
                           "titlevalue":titlevalue,
                           "partentId" :ledId});             //To set parameter on Lead
       action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                 var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error');
                                   }
                                   else
                                   {
                                       component.set("v.popupflag",false);
                                       this.showToast(component,'Success', result, 'success');
                                       this.getNoteslist(component, event, helper);
                                       component.set("v.commentvalue",'');
                                   }
                               }
                                component.set('v.showIASpinner',false);
                          });
        $A.enqueueAction(action);
    },
    onRefreshNotes :function(component, event, helper)
    {
        component.set("v.popupflag",false);
        component.set("v.commentvalue",'');
        this.getNoteslist(component, event, helper);
    },
    getNoteslist:function(component, event, helper)
    {
         var ledId =component.get('v.leadId');
         var action=component.get("c.getnotes"); //To call Apex controller method
         component.set('v.showIASpinner',true);
         action.setParams({"partentId" :ledId});             //To set parameter on Lead
         action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                 component.set("v.listofNotes",result);
                            }
                            component.set('v.showIASpinner',false);
                          });
         $A.enqueueAction(action);
    },
    getLeadHistory:function(component, event, helper)
    {
         var ledId =component.get('v.leadId');
         var action=component.get("c.getLeadHistory"); //To call Apex controller method
         action.setParams({"selectLeadId" :ledId});             //To set parameter on Lead
         component.set('v.showIASpinner',true);
         action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                  if(result)
                                  {
                                      for(var i=0;i<result.length;i++)
                                      {
                                          if(result[i].NewValue && result[i].NewValue.length >3 && result[i].NewValue.substring(0,3) =='005')
                                          {
                                           result[i].doNotRemove=true;
                                          }
                                          else
                                          {
                                             result[i].doNotRemove=false;
                                          }
                                      }
                                  }
                               
                                 component.set("v.listofLeadHistory",result);
                                 component.set('v.showIASpinner',false);
                            }
                          });
         $A.enqueueAction(action);
    },
    
    
    getownerships:function(component, event, helper)
    {
         var ledId =component.get('v.leadId');
         var action=component.get("c.getOwnerships"); //To call Apex controller method
        
         action.setParams({"leadid" :ledId});             //To set parameter on Lead
         component.set('v.showIASpinner',true);
         action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                console.log(result);  
                                 component.set("v.listofownerships",result);
                                 component.set('v.showIASpinner',false);
                            }
                          });
         $A.enqueueAction(action);
    },
    
    
    onRefreshLeadHistroy :function(component, event, helper)
    {
         this.getLeadHistory(component, event, helper);
         this.getAttachment(component, event, helper);
    },
    onclickAccept:function(component, event, helper)
    {
         var targetId =event.currentTarget.id;
         var action=component.get("c.updateleadstatus");      //To call Apex controller method
         action.setParams({"leadId" :targetId});             //To set parameter on Lead
         component.set('v.showIASpinner',true);
         action.setCallback(this, function(actionresult)
        {
            var state = actionresult.getState(); //get state value.
            if(state === "SUCCESS")
            {
                var result    = actionresult.getReturnValue();
                var errorvalue= result.indexOf('Error:');
                if(errorvalue != -1 )
                {
                    this.showToast(component,'ERROR',result, 'error'); 
                }
                else
                {
                    this.showToast(component,'Success', 'Lead successfully updated.', 'success');
                    this.getEditLead(component, event, helper);
                    this.getlistvalue(component, event, helper);
                }                     
            }
            component.set('v.showIASpinner',false);
        });
         $A.enqueueAction(action);
    },
    getUserRole:function(component, event, helper)
    {
         var action=component.get("c.getuserInformation"); //To call Apex controller method
         action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue(); // get return result value.
                                 component.set("v.userRole",result);
                            }
                          });
        $A.enqueueAction(action);
    },
    onClickdownload:function(component, event, helper)
    {
       var assignedSelect =component.find("InputSelectSingle");   //To find aura Id of Assigned picklist.
       var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
       //Status PickList
       var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
       var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
       var AccountId =component.get("v.AccountId");
       var consultantId =component.get("v.ConsultantId");
       var lst =component.get("v.listofLead");
       if(lst.length >0)
       {
         var action=component.get("c.getuserInformationProfile");      //To call Apex controller method
         action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                 var errorvalue= result.indexOf('Volkswagen');
                                   if(errorvalue != -1 )
                                   {
                                    var url ="/volkswagen/apex/VGA_Export_Lead?assign=" + assignedSelectvalue +"&status=" +statusSelectvalue +"&accId=" +AccountId +"&consltId=" +consultantId;
                                    window.open(url);
                                   }
                                    else
                                    {
                                     var url ="/skoda/apex/VGA_Export_Lead?assign=" + assignedSelectvalue +"&status=" +statusSelectvalue +"&accId=" +AccountId +"&consltId=" +consultantId;
                                     window.open(url);
                                    }
                            }
                          });
         $A.enqueueAction(action);
        }
        else
        {
           this.showToast(component,'ERROR', 'No row Found.', 'error');
        }
    },
    showToast : function(cmp, title, message, type )
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      },
    onchangeresignpicklist:function (component, event, helper)
    {
      var selectedValue= event.getSource().get("v.value");
      component.set('v.assignownerId',selectedValue);
    },
    onchangeresignEventpicklist: function(component,event,helper){
      var selectedValue= event.getSource().get("v.value");
      component.set('v.assignEventownerId',selectedValue);
    },
    getlistvalue:function (component, event, helper)
    {
        //debugger;
        var assignedSelect =component.find("InputSelectSingle");
        //To find aura Id of Assigned picklist.
        var assignedSelectvalue =assignedSelect.get("v.value");  //To get Assigned value by aura Id
        //Status PickList
        var statusSelect =component.find("InputSelectStatus"); //To find aura Id of status picklist.
        var statusSelectvalue =statusSelect.get("v.value");  //To get status value by aura Id.
        var AccountId =component.get("v.AccountId");
        var selectedValue= component.get("v.ConsultantId");
        var action=component.get("c.getLeadlist"); //To call Apex controller method
        component.set('v.showIASpinner',true);
        action.setParams({"Assignedvalue" :assignedSelectvalue,
                          "StatusValue" :statusSelectvalue,
                          "DealerId":AccountId,
                          "ConsultantId":selectedValue,
                          "sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});             //To set parameter on Lead
        action.setCallback(this, function(actionresult)
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue(); // get return result value.
                                component.set("v.listofLead",result); // set list of lead on component.
                                component.set("v.maxPage", Math.floor((result.length+19)/20));
                                this.renderPage(component);
                            }
                              component.set('v.showIASpinner',false);
                          });
        $A.enqueueAction(action);
    },
    renderPage: function(component)
    {
           var records = component.get("v.listofLead"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*20, pageNumber*20);
            component.set("v.listofLeaddisplay", pageRecords);
    },
    onClickaddress: function(component, event, helper)
    {
     component.set("v.mappopup", true);
    },
    closemappopup: function(component, event, helper)
    {
     component.set("v.mappopup", false);
    },
    handleError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.addClass(component, "error");
      },
    handleClearError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.removeClass(component, "error");
    },
    sortHelper: function(component, event, helper)
    {
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown')
        {
             // set the arrowDirection attribute for conditionally rendred arrow sign
             component.set("v.arrowDirection", 'arrowup');
             // set the isAsc flag to true for sort in Assending order.
             component.set("v.isAsc", true);
        }
        else
        {
             component.set("v.arrowDirection", 'arrowdown');
             component.set("v.isAsc", false);
        }
          // call the onLoad function for call server side method with pass sortFieldName
         this.getlistvalue(component, event, helper);
      },
    getAttachment : function(component, event, helper){

      //return new Promise($A.getCallback(function (resolve, reject){
        
          var urlString = window.location.href;
          var baseURL = urlString.substring(0, urlString.indexOf("/s"));
          var LeadId =component.get("v.leadId");
          var action=component.get("c.collectAttachmentWithParentId");
          component.set('v.LeadCheck',false);
          component.set('v.DocusignStage1Link',false);
          component.set('v.DocusignStage2Link',false);
          component.set('v.baseUrl','');
          component.set('v.DocusignStage1Url','');
          component.set('v.DocusignStage2Url','');
          component.set("v.hasImageSrc",false);
          action.setParams({'LeadId':LeadId});
          action.setCallback(this,function(resp){
              if(resp.getState()=='SUCCESS'){
                var result =  resp.getReturnValue();
                if(result!=null){
                  var attachment = result.AttachmentList==undefined?undefined:result.AttachmentList;
                  if(attachment!=undefined && attachment!=null && attachment.length!=0){
                      component.set("v.hasImageSrc",true);
                      component.set("v.DetailFlag",true);
                      var arrayOfMapKeys = [];
                      for (var singlekey in attachment){
                        
                          arrayOfMapKeys.push({
                              URL: baseURL + "/servlet/servlet.FileDownload?file=" + attachment[singlekey].Id,
                              value: attachment[singlekey].Name
                          });
                      }
                      component.set('v.imagelstKey', arrayOfMapKeys);
                  }
                  component.set('v.DocusignStage1Link',result.DocusignStage1Check);
                  component.set('v.DocusignStage2Link',result.DocusignStage2Check);
                  component.set('v.baseUrl',urlString);
                  component.set('v.DocusignStage1Url',result.DocusignLink);
                  component.set('v.DocusignStage2Url',result.DocusignLink);
                  component.set('v.LeadCheck',result.LeadCheck);

                }else{
                    helper.showToast(component,'ERROR','Lead Not  Found!', 'error');
                }
              }else{
                  helper.showToast(component,'ERROR','Please check Internet Connection', 'error');
              }
          });
          $A.enqueueAction(action);
      //}));
  }
 })