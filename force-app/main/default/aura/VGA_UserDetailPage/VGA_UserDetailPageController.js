({
	doInit : function(component, event, helper) 
    {
		helper.getDealershipProfile(component, event, helper);
	},
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    leadTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make lead tab active and show tab data
        component.find("leadId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("leaddataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    commentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make comment tab active and show tab data
        component.find("commentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("commentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    historyTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make histroy tab active and show tab data
        component.find("histroyId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("histroydataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    
    controllerEditUser: function(component, event, helper) 
    {
        helper.getEditUser(component, event, helper);
    },
    controlleronclickAddSubscriptions: function(component, event, helper) 
    {
      helper.onclickAddSubscriptions(component, event, helper);
    },
    controlleronclickEditworkingHours: function(component, event, helper) 
    {
      helper.onclickEditworkingHours(component, event, helper);
    },
    controllercreateUser :function(component, event, helper) 
    {
      helper.createUser(component, event, helper);
    },
    controlleronclickEditSubscriptions: function(component, event, helper) 
    {
      helper.onclickEditSubscriptions(component, event, helper);
    },
    controllerUpdateWorkingHours: function(component, event, helper) 
    {
      helper.updateWorkingHours(component, event, helper);
    },
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    controllerupdateandCreateSubscription: function(component, event, helper) 
    {
      helper.updateandCreateSubscriptioncontroller(component, event, helper);
    },
    controllerDeleteSubscriptioncontroller: function(component, event, helper) 
    {
     helper.deleteSubscriptioncontroller(component, event, helper);   
    },
    controllerSaveUserDetails :function(component, event, helper) 
    {
		helper.saveUserDetails(component, event, helper);
	},
    controllerResetUserPassword:function(component, event, helper) 
    {
      helper.resetUserPassword(component, event, helper);
    },
    controllerdeactiveUser:function(component, event, helper) 
    {
      helper.deleteuser(component, event, helper);
    },
    controllercreateUserprofies:function(component, event, helper) 
    {
      helper.createUserprofies(component, event, helper);
    },
    controllerOnChange:function(component, event, helper) 
    { 
      helper.onChange(component, event, helper);
    },
      ControllerErrorHandle:function(component, event, helper)
    {
        helper.handleError(component, event, helper);
    },
    ControllerhandleClearError:function(component, event, helper)
    {
        helper.handleClearError(component, event, helper);
    },
    controllerRefreshWorkingHours:function(component, event, helper)
    {
        helper.refreshWorkingHours(component, event, helper);
    },
    controllerRefreshSubscriptionValue:function(component, event, helper)
    {
        helper.refreshSubscriptionValue(component, event, helper);
    },
    controllercloseSubscription:function(component, event, helper)
    {
        helper.closeSubscription(component, event, helper);
    },
    controllercloseworkingHours:function(component, event, helper)
    {
        helper.closeworkingHours(component, event, helper);
    },
    controllercloseressignPopup:function(component, event, helper)
    {
        helper.closeressignPopup(component, event, helper);
    },
     controlleronChangeUser :function(component, event, helper)
    {
        helper.onChangeUser(component, event, helper);
    },
    controllerUpdateLeadownerId:function(component, event, helper)
    {
        helper.updateLeadownerId(component, event, helper);
    },
    sortEmail :function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'VGA_Contact__r.Email');
         helper.sortHelper(component, event, helper);
    },
    sortName:function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'VGA_Contact__r.Name');
         helper.sortHelper(component, event, helper);
    },
    sortRole:function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'VGA_Role__c');
         helper.sortHelper(component, event, helper);
    },
	keyPress: function(component, event, helper){
        component.find('username').set("v.value",component.find("email").get("v.value"));
    }    
})