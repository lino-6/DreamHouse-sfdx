({
    getDealershipProfile : function(component, event, helper) 
    {
        var action=component.get("c.getlogginguser");
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objName",result);
                                   this.getUserList(component, event, helper);
                               }
                           });
        $A.enqueueAction(action); 
    },
    refreshView: function(component, event, helper) 
    {
        component.set("v.popupflag",false);
        this.getaftersaveuser(component, event, helper);
    },
    getUserList : function(component, event, helper) 
    {
       var radiobutton;
      if(component.find("myPassenger3"))
       {
        radiobutton=component.find("myPassenger3").get("v.value");
       }
       else
       {
        radiobutton=false;                  
       }
        var action=component.get("c.getlistofuserProfile");
        if(radiobutton ==false)
        {
              action.setParams({"radiobuttonvalue" :'Passenger',
                                "sortField": component.get("v.selectedTabsoft"),
                                     "isAsc": component.get("v.isAsc")});  
              component.set("v.selectedValue",'Passenger');
        }
        else
        {
              action.setParams({"radiobuttonvalue" :'Commercial',
                               "sortField": component.get("v.selectedTabsoft"),
                               "isAsc": component.get("v.isAsc")});  
              component.set("v.selectedValue",'Commercial');
        }
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.listofUser",result);
                               }
                           });
        $A.enqueueAction(action); 
    },
    clearAll: function(component, event) 
    {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) 
        {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    onChange : function(component, event, helper) 
    {
       var selected = event.getSource().get("v.text");
       var action=component.get("c.getlistofuserProfile");
       action.setParams({"radiobuttonvalue" :selected,
                         "sortField": component.get("v.selectedTabsoft"),
                         "isAsc": component.get("v.isAsc")});
       action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.listofUser",result);
                                component.set("v.selectedValue",selected);
                                component.set("v.DetailFlag",false);
                                 component.set("v.ContactDetails",'');
                            }
                          });
        $A.enqueueAction(action); 
    },
    getEditUser: function(component, event, helper) 
    {
       var conObj={'sobjectType' :'VGA_User_Profile__c',
							       'VGA_Active__c':'',
                                   'VGA_Available__c':'',
                                   'VGA_Mobile__c':'',
                                   'VGA_Role__c':'',
                                   'VGA_Contact__c':''
                                   };
        component.set("v.objUser",conObj); 
        var targetId =event.target.id;
        var action =component.get("c.getUserDetail");
        action.setParams({"recordId" :targetId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               debugger;
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.DetailFlag",true);
                                   component.set("v.objUser",result);
                                   component.set("v.onclickUserId",targetId);
                                   this.getworkingHours(component, event, helper);
                                   this.getSubscriptionValue(component, event, helper);
                                   //this.getReportSubscriptionValue(component, event, helper);
                               }  
                           });
        $A.enqueueAction(action);
    },
    getworkingHours :function(component, event, helper)
    {
        var targetId=component.get("v.onclickUserId");
        var action2 =component.get("c.getWorkingHour");
        action2.setParams({"recordId" :targetId});
        action2.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.listofworking",result);
                                    component.set("v.onclickUserId",targetId);
                                }  
                            });
         $A.enqueueAction(action2);
    },
    getSubscriptionValue :function(component, event, helper)
    { 
        var targetId=component.get("v.onclickUserId");
        var action3 =component.get("c.getSubscription");
        action3.setParams({"recordId" :targetId});
        action3.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.listofSubscription",result);
                                    component.set("v.onclickUserId",targetId);
                                }   
                            });
        $A.enqueueAction(action3);
   },
    onclickAddSubscriptions: function(component, event, helper)
    {
        var test =component.get("v.objSubscription"); 
        debugger;
         var conObj={'sobjectType' :'VGA_Subscription__c',
							'VGA_User_Profile__c':'',
                             'VGA_Delivery_Method__c':'',
                             'VGA_Frequency__c':'',
                             'VGA_Start_Date__c':'',
                             'VGA_Subscription_Type__c':'',
                     };
        component.set("v.objSubscription",conObj); 
        component.set("v.disableSubscription",false); 
        component.set("v.subscriptionPopup",true);
        component.set("v.Createsubscriptionpopup",true); 
    },
    onclickEditworkingHours:function(component, event, helper)
    {
        component.set("v.workingHoursPopup",true);
        var targetId =event.target.id;
        var action =component.get("c.getworkingHoursDetail");
        action.setParams({"recordId" :targetId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objworking",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    createUser :function(component, event, helper) 
    {
        
        var conObj={'sobjectType' :'Contact',
							 'Email':'',
                             'FirstName':'',
                             'LastName':'',
                             'MobilePhone':'',
                             'VGA_CV_Mobile__c':'',
                             'VGA_PV_Mobile__c':'',
                             'VGA_Available_to_Receive_Leads__c':'',
                             'VGA_Role__c':'',
                    		 'VGA_Username__c': ''
                                                };
        component.set("v.ContactDetails",conObj); 
        component.set("v.popupflag",true);          
    },
    onclickEditSubscriptions :function(component, event, helper)
    {
        component.set("v.subscriptionPopup",true);
        component.set("v.Createsubscriptionpopup",false); 
        component.set("v.disableSubscription",true); 
        var targetId =event.target.id;
        var action =component.get("c.getSubscriptiondetails");
        action.setParams({"recordId" :targetId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objSubscription",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    updateWorkingHours :function(component, event, helper)
    {
        var lst =component.get("v.objworking");
        var action =component.get("c.saveWorkingHours");
        action.setParams({"objWorkingHours" :lst});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   {
                                       component.set("v.workingHoursPopup",false);
                                       this.showToast(component,'Success', result, 'success');
                                       this.getworkingHours(component, event, helper);
                                   }  
                               }
                           });
        $A.enqueueAction(action);   
    },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      },
     updateandCreateSubscriptioncontroller:function(component, event, helper)
     {
        var conId =component.get("v.onclickUserId");
        var flag  =component.get("v.Createsubscriptionpopup");
        var lst =component.get("v.objSubscription");
        var action =component.get("c.saveSubscription");  
        if(flag ==true)
        {
         action.setParams({"objSubscription" :lst,
                          "userProfile" :conId,
                          "subcription" :true});
        }
        else
        {
             action.setParams({"objSubscription" :lst});  
        }
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   {
                                        component.set("v.subscriptionPopup",false);    
                                        this.showToast(component,'Success', result, 'success');
                                        this.getSubscriptionValue(component, event, helper);
                                       component.set("v.objSubscription",'');
                                   }   
                               }
                           });
        $A.enqueueAction(action);
     },
    deleteSubscriptioncontroller:function(component, event, helper)
     {
        var conId =component.get("v.objSubscription");
        var result = confirm('Are you sure you want to delete this subscription?');
         if(result ==true)
         {
         var action =component.get("c.deleteSubscription"); 
         action.setParams({"userProfile" :conId});
         action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   {
                                        component.set("v.subscriptionPopup",false);
                                        this.showToast(component,'Success', result, 'success');
                                        this.getSubscriptionValue(component, event, helper);
                                   } 
                               }
                           });
          $A.enqueueAction(action);
         }     
     },
     saveUserDetails:function(component, event, helper)
     {
         var conId =component.get("v.onclickUserId");
         var lst =component.get("v.objUser");
         var checkboxvalue =component.get("v.active");
         if(lst.VGA_Contact__r.VGA_Job_Title__c==undefined || lst.VGA_Contact__r.VGA_Job_Title__c==''){
            this.showToast(component,'ERROR','Please select Job Title.','error'); 
            return;
         }
         var action =component.get("c.saveduserDetail");
         

         action.setParams({"objUserProfile" :lst,
                           "checkbox" :checkboxvalue});
         action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                      else
                                      {
                                           this.showToast(component,'Success', result, 'success');
                                           this.getaftersaveuser(component, event, helper);
                                           this.getaftersaveUser(component, event, helper);
                                      } 
                               }   
                           });
          $A.enqueueAction(action);
     },
     resetUserPassword: function(component, event, helper) 
      {
        var lst=component.get("v.objUser");
        var action =component.get("c.resetPassword");
        action.setParams({"objUserProfile" :lst});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               debugger;
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                      else
                                      {
                                           this.showToast(component,'Success', result, 'success');  
                                      } 
                               }
                           });
        $A.enqueueAction(action);
    },
    deleteuser: function(component, event, helper)
    {
         var lst=component.get("v.objUser");
         var loginlst=component.get("v.objName");
        if(lst.VGA_Contact__c ==loginlst.ContactId)
        {
            alert('You cannot deactive current logging user.');
        }
        else
        {
         var radiobutton =component.get("v.selectedValue");
         var result = confirm('Are you sure you want to remove access to the portal for this user?');
         if(result ==true)
         {
            var action =component.get("c.deleteUserProfie"); 
            action.setParams({"objUserProfile" :lst,
                              "selected" :radiobutton});
            action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       {
                                           var leadvalue =result.indexOf('Lead:');
                                           if(leadvalue != -1 )
                                           {
                                               this.getuserInformation(component, event, helper);
                                               component.set("v.reassign",true); 
                                           }
                                          else
                                          {
                                            this.showToast(component,'Success', result, 'success');
                                            this.getaftersaveuser(component, event, helper);
                                            component.set("v.DetailFlag",false);   
                                           }
                                       }
                                       
                                   }
                               });
              $A.enqueueAction(action);
         }
         }
    },
     getuserInformation :function(component, event, helper)
     {
        var lst=component.get("v.objUser");
        var radiobutton =component.get("v.selectedValue");
        var action=component.get("c.listofuser"); 
        action.setParams({"objUserProfile" :lst,
                          "selected" :radiobutton});   
        action.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var arrayOfMapKeys = [];
                                    var result =actionresult.getReturnValue();
                                  
                                    
                                    for (var singlekey in result)
                                    {
                                        arrayOfMapKeys.push({
                                            key: singlekey,
                                            value: result[singlekey]
                                        });
                                    }
                                    component.set("v.userName",arrayOfMapKeys[0].value);
                                    component.set("v.userMap",arrayOfMapKeys);
                                }
                            });
        $A.enqueueAction(action);
    },  
    createUserprofies :function(component, event, helper)
    {
        this.requiredField(component, event, helper);
        var flag          =component.get("v.RequiredField");
        var lst           =component.get("v.ContactDetails"); 
        var selected =component.get("v.selectedValue");
        if(flag == false)
        {
            var action =component.get("c.createUserProfie"); 
            action.setParams({"objContact" :lst,
                              "selectedValue":selected});
            action.setCallback(this, function(actionresult) 
                                   {
                                       var state = actionresult.getState();
                                       if(state === "SUCCESS")
                                       {
                                           var result =actionresult.getReturnValue();
                                           var errorvalue= result.indexOf('Error:');
                                           if(errorvalue != -1 )
                                           {
                                                this.showToast(component,'ERROR',result, 'error'); 
                                           }
                                           else
                                           {
                                                component.set("v.popupflag",false);
                                                this.showToast(component,'Success', result, 'success');
                                                this.getaftersaveuser(component, event, helper);
                                                
                                           } 
                                       }
                                   });
                  $A.enqueueAction(action);
        }
        else
        {
           this.requiredField(component, event, helper); 
        }
    },
    requiredField :function(component, event, helper)
    {
        debugger;
       var emailId             =component.find("email");
       var emailName           =emailId.get("v.value"); 
       var firstName           =component.find("firstName");
       var firstNamevalue      =firstName.get("v.value");  
       var lastName            =component.find("lastName");
       var lastNamevalue       =lastName.get("v.value");  
       var role                =component.find("role");
       var rolevalue           =role.get("v.value"); 
       var username				= component.find("username");
       var username_value		= username.get("v.value") ;
        
       var job                 =component.find("job");
       var jobvalue           =job.get("v.value");
       if($A.util.isEmpty(emailName) || $A.util.isUndefined(emailName))
        {
			
             emailId.set("v.errors",[{message:"Enter Email Address."}]); 
             component.set("v.RequiredField",true);
        }
        else if($A.util.isEmpty(username_value) || $A.util.isUndefined(username_value))
        {
             username.set("v.errors",[{message:"Enter Username."}]); 
             component.set("v.RequiredField",true);  
        }        
        else if($A.util.isEmpty(firstNamevalue) || $A.util.isUndefined(firstNamevalue))
        {
             firstName.set("v.errors",[{message:"Enter First Name."}]); 
             component.set("v.RequiredField",true);  
        }
        else if($A.util.isEmpty(lastNamevalue) || $A.util.isUndefined(lastNamevalue))
        {
             lastName.set("v.errors",[{message:"Enter Last Name."}]); 
             component.set("v.RequiredField",true);  
        }
        else if($A.util.isEmpty(rolevalue) || $A.util.isUndefined(rolevalue))
        {
             role.set("v.errors",[{message:"Enter Role."}]); 
             component.set("v.RequiredField",true);  
        }
        else if($A.util.isEmpty(jobvalue) || $A.util.isUndefined(jobvalue)){
            job.set("v.errors",[{message:"Enter Job Title."}]); 
            component.set("v.RequiredField",true);
        }
        else
        {
           emailId.set("v.errors",null);
           firstName.set("v.errors",null);
           lastName.set("v.errors",null);
           username.set("v.errors",null);
           role.set("v.errors",null);
           job.set("v.errors",null);
           component.set("v.RequiredField",false);  
        }
     },
    handleError:function(component, event, helper){
        var component = event.getSource();
        $A.util.addClass(component, "error");   
    },
    handleClearError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.removeClass(component, "error");   
    },
    refreshWorkingHours:function(component, event, helper)
    {
        this.getworkingHours(component, event, helper);
    },
    refreshSubscriptionValue:function(component, event, helper)
    {
         this.getSubscriptionValue(component, event, helper);
    },
    
    closeSubscription:function(component, event, helper)
    {
        var conObj={'sobjectType' :'VGA_Subscription__c',
							'VGA_User_Profile__c':'',
                             'VGA_Delivery_Method__c':'',
                             'VGA_Frequency__c':'',
                             'VGA_Start_Date__c':'',
                             'VGA_Subscription_Type__c':'',
                     };
         component.set("v.objSubscription",conObj);   
         component.set("v.subscriptionPopup",false);
         this.getSubscriptionValue(component, event, helper);
    },
    closeworkingHours:function(component, event, helper)
    {
         component.set("v.workingHoursPopup",false);
         this.getworkingHours(component, event, helper);
    },
    getaftersaveuser :function(component, event, helper)
    {
       var radiobutton=component.get("v.selectedValue");
       var action     =component.get("c.getlistofuserProfile");
       action.setParams({"radiobuttonvalue" :radiobutton,
                        "sortField": component.get("v.selectedTabsoft"),
                                     "isAsc": component.get("v.isAsc")});
       action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.listofUser",result);
                               }
                           });
        $A.enqueueAction(action); 
    } ,
    getaftersaveUser :function(component, event, helper)
    {
        var conId =component.get("v.onclickUserId");
        var action =component.get("c.getUserDetail");
        action.setParams({"recordId" :conId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objUser",result);
                               }  
                           });
        $A.enqueueAction(action);
    },
    closeressignPopup:function(component, event, helper)
    {
        component.set("v.reassign",false);
    },
    onChangeUser :function(component, event, helper) 
    {
        var selectedValue= event.getSource().get("v.value");
        component.set("v.userName",selectedValue);
    },
    updateLeadownerId:function(component, event, helper) 
    {
        var userId =component.get("v.userName");
        if(userId =='--None--')
        {
          alert('Please Select UserName') ; 
        }
        else
        {
             var lst=component.get("v.objUser");
             var radiobutton =component.get("v.selectedValue");
             var action=component.get("c.ressignLead"); 
             action.setParams({"objUserProfile" :lst,
                               "selected" :radiobutton,
                               "selecteduserId":userId});   
             action.setCallback(this, function(actionresult) 
                            {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                     {
                                         this.showToast(component,'ERROR',result, 'error'); 
                                     }
                                     else
                                     {
                                                component.set("v.reassign",false); 
                                                this.showToast(component,'Success', result, 'success');
                                                component.set("v.DetailFlag",false);
                                                this.getaftersaveuser(component, event, helper);     
                                     } 
                                }
                            });
           $A.enqueueAction(action);
        }
    },
    sortHelper: function(component, event, helper) 
      {
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown') 
        {
             // set the arrowDirection attribute for conditionally rendred arrow sign  
             component.set("v.arrowDirection", 'arrowup');
             // set the isAsc flag to true for sort in Assending order.  
             component.set("v.isAsc", true);
        } 
        else 
        {
             component.set("v.arrowDirection", 'arrowdown');
             component.set("v.isAsc", false);
        }
          // call the onLoad function for call server side method with pass sortFieldName 
           this.getaftersaveuser(component, event, helper);
      }
})