({
	getloggingProfile : function(component, event, helper) 
    {
         var action=component.get("c.getuserInformation"); //To call Apex controller method
         action.setCallback(this, function(actionresult) 
                          {
                                var state = actionresult.getState(); //get state value.
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue(); // get return result value.
                                     component.set("v.profilename",result); 
                                    if(result =='Dealer Portal Group' || result =='Dealer Portal')
                                    {
                                        this.navigate(component, event, helper);
                                    }
                                   
                                }
                          });
        $A.enqueueAction(action);
	},
    navigate : function(component, event, helper) 
    { 
        var url;
         var action=component.get("c.fetchit"); //To call Apex controller method
         action.setCallback(this, function(actionresult) 
                          {
                                var state = actionresult.getState(); //get state value.
                             
                                if(state === "SUCCESS")
                                { 
                                      var result =actionresult.getReturnValue(); // get return result value.
                                      
                                      var urlEvent = $A.get("e.force:navigateToURL");
                                     urlEvent.setParams({
                                              "url":result 
                                                });   
                                            urlEvent.fire();
                                 }
                          });
         $A.enqueueAction(action);
    }

    
})