({
	getloggingDetails :function(component, event, helper)
    {
     var action=component.get("c.getlogginguser");
     action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.objName",result);  
                                }
                              });
           $A.enqueueAction(action);    
    },
    updateSaveTask :function (component, event, helper)
    {
        var lst=component.get("v.objTask");
        var action=component.get("c.updatetaskDetails");
        action.setParams({"objTask" :lst});
        action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                    {
                                     this.showToast(component,'ERROR',result, 'error'); 
                                    }
                                   else
                                   { 
                                       this.showToast(component,'Success', result, 'success');  
                                       this.showTask(component, event, helper);
                                       this.taskDetails(component, event, helper);
                                   }   
                                }
                              });
           $A.enqueueAction(action); 
    },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      },
      clearAll: function(component, event) 
      {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) 
        {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
     },
    onclickupdated :function (component, event, helper)
    {
       
        var targetId =event.currentTarget.id;
        var result = confirm('Are you sure you want to complete this task?');
        if(result ==true)
        {
         var action=component.get("c.updateTaskstatus");      //To call Apex controller method
         action.setParams({"TaskId" :targetId});             //To set parameter on Lead
         action.setCallback(this, function(actionresult) 
         {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue();
                                 var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                    {
                                     this.showToast(component,'ERROR',result, 'error'); 
                                    }
                                   else
                                   { 
                                       this.showToast(component,'Success', result, 'success');  
                                       this.showTask(component, event, helper);
                                       this.taskDetails(component, event, helper);
                                   }     
                            }
                            });
           $A.enqueueAction(action);
        }
    },
    taskDetails :function (component, event, helper)
    {
         var targetId  =component.get("v.taskId");
         var action =component.get("c.gettaskDetails");
		 action.setParams({"RecordId" :targetId});
         action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.DetailFlag",true);
                                     component.set("v.objTask",result);    
                                }  
                           });
         $A.enqueueAction(action);
    },
    showTask:function(component,event,helper)
    {
        var action=component.get("c.getTasksMethod");
        action.setParams({
            "filter":component.get("v.filterCond"),
            "sortField": component.get("v.selectedTabsoft"),
            "isAsc": component.get("v.isAsc")
        });
         action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.Task",result);
                                    component.set("v.maxPage", Math.floor((result.length+9)/10));
                                    component.set("v.filterCond",component.get("v.filterCond"));
                                    component.set("v.DetailFlag",false);
                                    this.renderPage(component);
                                }
                           });
                                 
        $A.enqueueAction(action);
    },
    renderPage: function(component) 
    {
		    var records = component.get("v.Task"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
            component.set("v.Taskdisplay", pageRecords);
	},
    sortHelper: function(component, event, helper) 
      {
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown') 
        {
             // set the arrowDirection attribute for conditionally rendred arrow sign  
             component.set("v.arrowDirection", 'arrowup');
             // set the isAsc flag to true for sort in Assending order.  
             component.set("v.isAsc", true);
        } 
        else 
        {
             component.set("v.arrowDirection", 'arrowdown');
             component.set("v.isAsc", false);
        }
          // call the onLoad function for call server side method with pass sortFieldName 
         this.showTask(component, event, helper);
      },
    openSingleFile: function(component, event, helper)
    { 
        var targetId =event.currentTarget.id;
        debugger;
        var action=component.get("c.returnBlobValue");
        action.setParams({
            "attachmentIds":targetId
        });
        action.setCallback(this, function(actionresult) 
          {
            var state = actionresult.getState();
            if(state === "SUCCESS")
            { 
                var result=actionresult.getReturnValue();
                var attachmentObjectBlob=result.encodeValue; 
                var url;
                
                if(result.fileType=='PDF')
                {
                     url='data:application/pdf;charset=utf-8;base64,'+attachmentObjectBlob;
                    component.set('v.downloadpopup',true);
                }
                else if(result.fileType=='JPG' || result.fileType=='JPEG')
                {
                   url='data:image/jpeg;charset=utf-8;base64,'+attachmentObjectBlob;
                    component.set('v.downloadpopup',true);
                }
               else if(result.fileType=='PNG')
                {
                   url='data:image/png;charset=utf-8;base64,'+attachmentObjectBlob;   
                    component.set('v.downloadpopup',true);
                }
               else if(result.fileType=='EXCEL_X')
                { 
                  url='data:application/x-msexcel;charset=utf-8;base64,'+attachmentObjectBlob;  
                    component.set('v.downloadpopup',true);
                }
                else if(result.fileType=='POWER_POINT_X')
                {
                  url='data:application/powerpoint;charset=utf-8;base64,'+attachmentObjectBlob;
                    component.set('v.downloadpopup',true);
                }
               else if(result.fileType=='WORD_X')
                { 
                  url='data:application/msword;charset=utf-8;base64,'+attachmentObjectBlob; 
                    component.set('v.downloadpopup',true);
                }
               else if(result.fileType=='GIF')
                {
                    url='data:image/gif;charset=utf-8;base64,'+attachmentObjectBlob; 
                    component.set('v.downloadpopup',true);
                }
                else
                {
                    alert('You can only download Image,Excel(XLSX),PDF,Word and Powerpoint files.');
                    component.set('v.downloadpopup',false);
                }
                 component.set('v.hrefAtt',url);
                 component.set('v.download',result.Name);
                 
                
                //this.refreshview(component, event, helper);
            }
            else
            {
                console.log('==='+JSON.stringify(response.getError()));
            }
            
          });
         $A.enqueueAction(action);        
     },
     Closewindow: function(component, event, helper)
     { 
      component.set('v.downloadpopup',false);   
     }
    
})