({
	doInit : function(component, event, helper) 
    {
		
        helper.getloggingDetails(component, event, helper);
         helper.showTask(component, event, helper);
	},
     leadTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make lead tab active and show tab data
        component.find("leadId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("leaddataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
     commentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make comment tab active and show tab data
        component.find("commentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("commentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    //Method to redirect the URL to the record Clicked on page
    gotoURL : function (component, event, helper) 
    {

        component.set("v.DetailFlag",false);
         var targetId  =event.target.id;
         var action =component.get("c.gettaskDetails");
		 action.setParams({"RecordId" :targetId});
         action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                               
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.DetailFlag",true);
                                     component.set("v.objTask",result);
                                     component.set("v.taskId",targetId);
                                     var action =component.get("c.getAttachmentlist");
                                     action.setParams({"RecordId" :targetId});
                                     action.setCallback(this, function(actionresult) 
                                     {
                                        var state = actionresult.getState();
                                        if(state === "SUCCESS")
                                         {
                                           var result =actionresult.getReturnValue();
                                            
                                           component.set("v.DetailFlag",true);
                                           component.set("v.attachmentList",result);   
                                        }  
                                      });
                                         $A.enqueueAction(action);
                                     
                                }  
                           });
         $A.enqueueAction(action);
	},
     closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    controllerUpdateSaveTask: function (component, event, helper) 
    {
        helper.updateSaveTask(component, event, helper);
    },
    controllerOnclickcomplete:function (component, event, helper) 
    {
       helper.onclickupdated(component, event, helper);  
    },
    onchangemethod:function (component, event, helper) 
    {
        helper.showTask(component, event, helper);
    },
    renderPage: function(component, event, helper)
    {
        helper.renderPage(component);
    },
    sortSubject : function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'Subject');
         helper.sortHelper(component, event, helper);
    },
    sortStatus: function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'Status');
         helper.sortHelper(component, event, helper);
    },
    sortActivityDate: function(component, event, helper)
    {
         component.set("v.selectedTabsoft",'ActivityDate');
         helper.sortHelper(component, event, helper);
    },
    sortWhoName: function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'Who.Name');
         helper.sortHelper(component, event, helper);
    },
    onclickopenSingleFile: function(component, event, helper)
    {
         helper.openSingleFile(component, event, helper);
    },
    onClickClosewindow : function(component, event, helper)
    {
         helper.Closewindow(component, event, helper);
    }
    
    
})