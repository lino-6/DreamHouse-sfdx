({
    showHideSpinner : function(component) 
    {
        var showValue = component.get('v.showIASpinner');
        //alert(showValue);
        if(showValue) 
        {
            var spinner = component.find("spinner");
        	$A.util.removeClass(spinner, "slds-hide");
        } 
        else 
        {
            var spinner = component.find("spinner");
        	$A.util.addClass(spinner, "slds-hide");
        }
    }

})