({
	resetPassword : function(component, event, helper) {
		helper.showSpinner(component, event, helper);
        helper.hideButton(component, event, helper);
		var rec_id  =component.get("v.recordId");
        var action =component.get("c.resetDealerContactPassword");
        
		action.setParams({"contact_id" :rec_id});
        console.log(rec_id);
        action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                console.log(actionresult.getReturnValue());
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();   
                                    if(result === false)
                                    {
                                        component.set('v.message', 'FAILED!');
                                    }
                                    else
                                    {
                                        component.set('v.message', 'SUCCESS!');
                                    }
                                }
                               else
                               {
                                   component.set('v.message', 'FAILED!');	
                               }
                               helper.hideSpinner(component, event, helper);                               
                           });

         $A.enqueueAction(action);			
	},
	
	
    toggle: function (cmp, event, helper) {
        var spinner = cmp.find("mySpinner");
        $A.util.toggleClass(spinner, "slds-hide");
    },
    
    doInit : function(component, event, helper) 
    {
        helper.onload(component, event, helper);
	},
	
   closeModel: function(component, event, helper) {
	   var dismissActionPanel = $A.get("e.force:closeQuickAction");
	   dismissActionPanel.fire();
   },
       
	
	
})