({

	onload: function(component, event, helper) 
	{
		var rec_id  =component.get("v.recordId");
        var action =component.get("c.getcontactDetail");
		action.setParams({"record_id" :rec_id});
        action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                     component.set("v.obj_contact",result);
                                     component.set('v.message', 'Are you sure, you want to reset password for ' + component.get('v.obj_contact.Name'));
                                }
                               else
                               {
                                   component.set('v.message', 'FAILED!');
                               }
                           });
         $A.enqueueAction(action);	
	},
	
   	 // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    hideButton : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.showButton", false);
    }    
			
	
})