({
    doAction : function(component, event,helper) 
    {
        var params = event.getParam('arguments');
        if (params) 
        {  
			var whoid = params.param1;
			component.set("v.EventId",whoid);
			helper.getEventData(component,event,helper);
		} 
	},
	saveEventDetail : function(component,event, helper)
    {
		var eve=component.get('v.Event');
		var bol=false;
		if(eve.Subject==null || eve.Subject==undefined || eve.Subject.trim()=='') {

			$A.util.addClass(component.find('subject'),'slds-has-error');
			bol=true;
		}else{
			$A.util.removeClass(component.find('subject'),'slds-has-error');
		}
		if(eve.StartDateTime==null || eve.StartDateTime==undefined){
			$A.util.addClass(component.find('startdate'),'slds-has-error');
			bol=true;
		}else{
			$A.util.removeClass(component.find('startdate'),'slds-has-error');
		}
		if(eve.EndDateTime==null || eve.EndDateTime==undefined){
			$A.util.addClass(component.find('enddate'),'slds-has-error');
			
			bol=true;
		}else{
			$A.util.removeClass(component.find('enddate'),'slds-has-error');
		}
		if(bol){
			helper.showToast(component,'ERROR','Please enter event details to save ', 'error');
			return;
		}
		if(eve.EndDateTime!=null && eve.StartDateTime!=null){
			
			if(new Date(eve.EndDateTime).getTime()<new Date(eve.StartDateTime).getTime()){
			
				helper.showToast(component,'ERROR','End Date must be greater then Start Date ', 'error');
				$A.util.addClass(component.find('enddate'),'slds-has-error');
				bol=true;
			}else{
				$A.util.removeClass(component.find('enddate'),'slds-has-error');
			}
		}
		if(!bol){
			helper.updateEvent(component,event,helper);
		}
		
	},
	deleteEventDetail : function(component,event, helper){
		var confirmCheck=window.confirm('Are you sure you want to delete the event ?');
		if(confirmCheck){
			helper.deleteEvent(component,event,helper);
		}
		
	},
	fireEvent : function(component,event,helper){
		// Get the component event by using the
		// name value from aura:registerEvent
		var params = event.getParam('arguments');
		
		var cmpEvent = component.getEvent("cmpEvent");
		cmpEvent.setParams({'message':params.message})
		

        cmpEvent.fire();
        
	},
	closeToast : function(cmp, event, helper)
    {
        cmp.set("v.errormsg",'');
    }
})