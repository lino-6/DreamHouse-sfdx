({
	getEventData: function(component, event, helper) 
    {
        return new Promise($A.getCallback(function (resolve, reject) {
			var targetId = component.get("v.EventId");
			var action = component.get("c.getLeadDetails");	
			action.setParams({"RecordId" :targetId});  	
			action.setCallback(this, function(response) {
				var state = response.getState();	
				if (component.isValid() && state === "SUCCESS") {
					var result = response.getReturnValue();
					component.set("v.Event",result);	
				} else {
					console.log('Error trying to create event:' + response.state);
				}            
			});	               
			$A.enqueueAction(action);
		}));
	},
	updateEvent : function(component, event, helper){		
		var act=component.get('c.updateEvent');
		act.setParams({'EventData':JSON.stringify(component.get('v.Event'))});
		act.setCallback(this,function(resp){
			if(resp.getState()=="SUCCESS"){
				var res=resp.getReturnValue();				
				if(res!=null){					
					helper.showToast(component,'Success', 'The Event was successfully saved.', 'success'); 
					component.set('v.Event',res);
					// Get the component event by using the
					// name value from aura:registerEvent
					var cmpEvent = component.getEvent("cmpEvent");
					cmpEvent.setParams({
						"message" : "EventUpdated"});
					cmpEvent.fire();
				}else{
					// Error Message to be displayed
					helper.showToast(component,'ERROR','Unable to save the Event ', 'error');
				}				
			}else{
				// Error Message to be displayed
				helper.showToast(component,'ERROR','Unable to save the Event', 'error');
			}
		});
		$A.enqueueAction(act);
	},
	deleteEvent : function(component, event, helper){
		var act=component.get('c.deleteEvent');
		act.setParams({'EventId':component.get('v.Event.Id') });
		act.setCallback(this,function(resp){
			if(resp.getState()=="SUCCESS"){
				var res=resp.getReturnValue();
				if(res){
					//fire Event to refresh
					//component.set('v.Event',null);
					helper.showToast(component,'Success', 'The Event was successfully deleted.', 'success'); 
					// Get the component event by using the
					// name value from aura:registerEvent
					var cmpEvent = component.getEvent("cmpEvent");
					cmpEvent.setParams({
						"message" : "EventDeleted"});
					cmpEvent.fire();
				}else{
					//Error Message
					// Error Message to be displayed
					helper.showToast(component,'ERROR','Unable to delete the Event ', 'error');
				}
			}else{
				// Error Message to be displayed
				helper.showToast(component,'ERROR','Unable to delete the Event ', 'error');
			}
		});
		$A.enqueueAction(act);
	},
	 showToast : function(cmp, title, message, type ) 
    {
        cmp.set("v.errormsg",message);
        cmp.set("v.title",title);
        //---- Setting up severity ----//
        if(type == 'info')
        {
        cmp.set("v.type",'');
        cmp.set("v.icon",'utility:info');
        }
        else if(type == 'warning')
        {
        cmp.set("v.type",'slds-theme--error');
        cmp.set("v.icon",'utility:warning');
        }
        else if(type == 'error')
        {
        cmp.set("v.type",'slds-theme--error');
        cmp.set("v.icon",'utility:error');
        }
        else if(type == 'success')
        {
        cmp.set("v.type",'slds-theme--success');
        cmp.set("v.icon",'utility:success');
        }
        cmp.set("v.errormsg",message);
        window.setTimeout(function(){
            if(type == 'success'){
                cmp.set("v.ShowEventDetails", false);
            }
            cmp.set("v.errormsg",'');
        }, 4000); 
    }
})