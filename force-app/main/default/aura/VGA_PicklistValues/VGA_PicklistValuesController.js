({
    doInit : function(component, event, helper) {
		helper.doInitHelper(component, event, helper);
	},
	
    onChangeParentVal : function(component, event, helper) {
		helper.fetchDependentPickValuesHelper(component, event, helper);
	},
})