({
    optionsMap : [],
	doInitHelper : function(component, event, helper) {
		var objectAPIName = component.get("v.objectAPIName");
        var fieldAPIName = component.get("v.fieldAPIName");
        
        var action = component.get("c.PicklistValues");
        action.setParams({ 
            "objectAPIName" : objectAPIName,
            "fieldAPIName" : fieldAPIName
		});
        
        action.setCallback(this,function(response){
	        var state = response.getState();
			if(state === 'SUCCESS'){
				
				var returnVal = response.getReturnValue();
                component.set("v.pickValues",returnVal);
                
                var parentFieldAPI = component.get("v.parentFieldAPI");
                if(parentFieldAPI != "" && parentFieldAPI != null && parentFieldAPI != undefined){
                    this.fetchControllingValHelper(component, event, helper);
                }
			}
		});
        $A.enqueueAction(action);
	},
	
    fetchControllingValHelper : function(component, event, helper) {
        
		var objectAPIName = component.get("v.objectAPIName");
        var fieldAPIName = component.get("v.fieldAPIName");
		var parentFieldAPI = component.get("v.parentFieldAPI");
		
		var action =component.get("c.dependentPicklistValues");
        action.setParams({
			"objectAPIName" : objectAPIName,
            "fieldAPIName" : fieldAPIName,
			"parentFieldAPI" : parentFieldAPI
		});
        action.setCallback(this, function(actionresult) {
			var state = actionresult.getState();
			debugger;
			if(state === "SUCCESS"){
                this.optionsMap = actionresult.getReturnValue();
                component.set('v.fieldDipendency',actionresult.getReturnValue());
				this.fetchDependentPickValuesHelper(component, event, helper);
			}
		});
		
        $A.enqueueAction(action);
	},
	
    fetchDependentPickValuesHelper :  function(component, event, helper) {
        var dependentList = [];
        var parentPickVal = component.get("v.parentPickVal");
        if(parentPickVal != undefined && parentPickVal != "" && parentPickVal != null){
            
            var optionMap = component.get('v.fieldDipendency')[0];
            
            if(optionMap != undefined && optionMap != null && parentPickVal in optionMap){
            dependentList = optionMap[parentPickVal];
            }
            component.set("v.pickValues",dependentList);
        }
     }
})