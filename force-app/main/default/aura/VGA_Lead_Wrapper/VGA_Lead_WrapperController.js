({
    doInit:function(component,event,helper){
        helper.collectUserInfo(component,event,helper)
    },
    displayTabs : function(component,event,helper) {
        var targetId=event.target.id;
        if(targetId=='MyLeadTab'){
            component.set('v.active_tab',false);
            $A.get('e.force:refreshView').fire();
        }else{
            component.set('v.active_tab',true);
            if(component.get('v.CalendarOneCheck')==false){
                $A.createComponent(
                    "c:VGA_TestDriveCalendar",{'aura:id':'TestDriveCalendar','LoggedInUser':component.get('v.LoggedInUser')},
                    function(cmp, status, errorMessage){
                        if (status === "SUCCESS") {
                            component.set('v.CalendarComponent',cmp);
                            component.find('TestDriveCalendar').set('v.UserRole',component.get('v.UserRole'));
                            component.set('v.CalendarOneCheck',true);
                        }else if (status === "INCOMPLETE") {
                            //console.log("No response from server or client is offline.")
                        }else if (status === "ERROR") {
                            //console.log("Error: " );
                        }
                    }
                );
            }
        }
    }
})