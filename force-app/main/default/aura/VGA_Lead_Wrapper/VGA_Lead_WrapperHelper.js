({
	collectUserInfo : function(component,event,helper) {
        let action = component.get("c.getloggedInUserDetails");
        action.setCallback(this, function(response) {
            //console.log(response);
            if(response.getState()=='SUCCESS'){
               var res=response.getReturnValue();
                //console.log(res);
               if(res!=null){
                   component.set('v.LoggedInUser',res.LoggedInUser);
                   component.set('v.isPilot',res.isPilot); 
                   component.set('v.UserRole',res.Role);
                   //console.log(component.get('v.isPilot'));
               }else{
                  component.set('v.isPilot',false); 
               }
            }else{
                component.set('v.isPilot',false);
            }
            if(component.get('v.LeadOneCheck')==false){
                $A.createComponent(
                    "c:VGA_LeadDetailsComponent",{},
                    function(cmp, status, errorMessage){
                        if (status === "SUCCESS") {
                            component.set('v.LeadComponent',cmp);
                            component.set('v.LeadOneCheck',true);
                        }else if (status === "INCOMPLETE") {
                            //console.log("No response from server or client is offline.")
                            
                        }else if (status === "ERROR") {
                            //console.log("Error: " );
                            
                        }
                    }
                ); 
            }
            
         	
        });
        $A.enqueueAction(action);
    }
})