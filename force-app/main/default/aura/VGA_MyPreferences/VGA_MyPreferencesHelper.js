({
	getDealershipProfile : function(component, event, helper) 
    {
        debugger;
      var action=component.get("c.getlogginguser");
      action.setCallback(this, function(actionresult) 
       {
           var state = actionresult.getState();
           if(state === "SUCCESS")
           {
            var result =actionresult.getReturnValue();
            component.set("v.objName",result);
         	this.getSubscriptions(component, event, helper) ;  
           }
          });
         $A.enqueueAction(action); 
      },
     getSubscriptions: function(component, event, helper) 
     {
         debugger;
      var radiobutton;
      if(component.find("myPassenger3"))
       {
        radiobutton=component.find("myPassenger3").get("v.value");
       }
       else
       {
        radiobutton=false;                  
       }
       var action=component.get("c.getSubscription");
       if(radiobutton ==false)
        {
              action.setParams({"radiobuttonvalue" :'Passenger'});
               component.set("v.selectedValue",'Passenger');
        }
        else
        {
              action.setParams({"radiobuttonvalue" :'Commercial'});  
              component.set("v.selectedValue",'Commercial');
        }
       	action.setCallback(this, function(actionresult) 
         {
          var state = actionresult.getState();
          if(state === "SUCCESS")
           {
              var result =actionresult.getReturnValue();
              component.set("v.listofSubscription",result);  
              this.getworkingHours (component, event, helper) ;
              this.onGetListofUser (component, event, helper) ;  
           }
      	});
       $A.enqueueAction(action);  
    },
    refreshView: function(component, event, helper) 
    {
      var radiobutton =component.get("v.selectedValue");
      var action=component.get("c.getSubscription");
      action.setParams({"radiobuttonvalue" :radiobutton});
      action.setCallback(this, function(actionresult) 
         {
          var state = actionresult.getState();
          if(state === "SUCCESS")
           {
              var result =actionresult.getReturnValue();
              component.set("v.listofSubscription",result);  
              this.getworkingHours (component, event, helper) ;
              this.onGetListofUser (component, event, helper) ;  
           }
      	});
       $A.enqueueAction(action);  
    },
    clearAll: function(component, event) 
    {
        // this method set all tabs to hide and inactive
        var getAllLI = document.getElementsByClassName("customClassForTab");
        var getAllDiv = document.getElementsByClassName("customClassForTabData");
        for (var i = 0; i < getAllLI.length; i++) 
        {
            getAllLI[i].className = "slds-tabs--scoped__item slds-text-title--caps customClassForTab";
            getAllDiv[i].className = "slds-tabs--scoped__content slds-hide customClassForTabData";
        }
    },
    getworkingHours: function(component, event) 
    {
      var radiobutton =component.get("v.selectedValue");
      var action=component.get("c.getWorkingHour");
      action.setParams({"radiobuttonvalue" :radiobutton});
      action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       component.set("v.listofworking",result);
                                          
                                   }
                               });
            $A.enqueueAction(action);  
    },
    onChange : function(component, event, helper) 
    {
       var selected = event.getSource().get("v.text");
       var action=component.get("c.getSubscription");
       action.setParams({"radiobuttonvalue" :selected});
       action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState();
                            if(state === "SUCCESS")
                            {
                                var result =actionresult.getReturnValue();
                                component.set("v.listofSubscription",result);
                                component.set("v.selectedValue",selected);   
                            }
                          });
        $A.enqueueAction(action);
        var action2=component.get("c.getWorkingHour");
        action2.setParams({"radiobuttonvalue" :selected});
        action2.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       component.set("v.listofworking",result);
                                       component.set("v.selectedValue",selected);        
                                   }
                               });
         $A.enqueueAction(action2);
        var action3=component.get("c.getUserDetail");
        action3.setParams({"radiobuttonvalue" :selected});
        action3.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       component.set("v.objUser",result);
                                       component.set("v.selectedValue",selected);
                                   }
                               });
            $A.enqueueAction(action3);
        
    },
     onclickEditworkingHours:function(component, event, helper)
    {
        component.set("v.workingHoursPopup",true);
        var targetId =event.target.id;
        var action =component.get("c.getworkingHours");
        action.setParams({"recordId" :targetId});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objworking",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    onGetListofUser :function(component, event, helper)
    {
         var radiobutton =component.get("v.selectedValue");
          var action =component.get("c.getUserDetail");
        
              action.setParams({"radiobuttonvalue" :radiobutton});  
         
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   component.set("v.objUser",result);
                               }
                           });
        $A.enqueueAction(action);
    },
    updateWorkingHours :function(component, event, helper)
    {
        var lst =component.get("v.objworking");
        var action =component.get("c.saveWorkingHours");
        action.setParams({"objWorkingHours" :lst});
        action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                   var result =actionresult.getReturnValue();
                                   var errorvalue= result.indexOf('Error:');
                                   if(errorvalue != -1 )
                                   {
                                        this.showToast(component,'ERROR',result, 'error'); 
                                   }
                                   else
                                   {
                                       component.set("v.workingHoursPopup",false);
                                       this.showToast(component,'Success', result, 'success');
                                       this.getworkingHours (component, event, helper) ;
                                   }  
                               }
                           });
        $A.enqueueAction(action);   
    },
    saveUserDetails:function(component, event, helper)
     {
         var lst =component.get("v.objUser");
         if(lst.VGA_Contact__r.VGA_Job_Title__c==undefined || lst.VGA_Contact__r.VGA_Job_Title__c==''){
            this.showToast(component,'ERROR','Please select Job Title.','error'); 
            return;
         }
         var action =component.get("c.saveduserDetail");
         action.setParams({"objUserProfile" :lst});
         action.setCallback(this, function(actionresult) 
                           {
                               var state = actionresult.getState();
                               if(state === "SUCCESS")
                               {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                    if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                      else
                                      {  
                                           this.showToast(component,'Success', result, 'success');
                                           this.refreshView(component, event, helper);
                                      } 
                               }   
                           });
          $A.enqueueAction(action);
     },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      },
     refreshworkingHours  :function(component, event, helper)
     {
      this.getworkingHours (component, event, helper) ;   
     },
    resetpasswordMethod :function(component, event, helper)
    {
         var result = confirm('Are you sure you want to reset password ?');
         if(result ==true)
         { 
            var lst=component.get("v.objUser");
            var action =component.get("c.resetPassword");
            action.setParams({"objUserProfile" :lst});
            action.setCallback(this, function(actionresult) 
                               {
                                   var state = actionresult.getState();
                                   debugger;
                                   if(state === "SUCCESS")
                                   {
                                       var result =actionresult.getReturnValue();
                                       var errorvalue= result.indexOf('Error:');
                                        if(errorvalue != -1 )
                                           {
                                                this.showToast(component,'ERROR',result, 'error'); 
                                           }
                                          else
                                          {
                                               this.showToast(component,'Success', result, 'success');  
                                          } 
                                   }
                               });
            $A.enqueueAction(action);
         }
    },
    popup :function(component, event, helper)
    {
     component.set("v.workingHoursPopup",false);   
    }
     
})