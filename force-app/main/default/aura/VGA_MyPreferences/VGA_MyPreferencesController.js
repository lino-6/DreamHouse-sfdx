({
	doInit : function(component, event, helper) 
    {
        debugger;
		helper.getDealershipProfile(component, event, helper);
	},
    controllerRefreshView:function(component, event, helper) 
    {
		helper.refreshView(component, event, helper);
	},
    leadTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make lead tab active and show tab data
        component.find("leadId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("leaddataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    commentTab: function(component, event, helper) 
    {
        helper.clearAll(component, event);
        //make comment tab active and show tab data
        component.find("commentId").getElement().className = 'slds-tabs--scoped__item slds-active customClassForTab';
        component.find("commentdataId").getElement().className = 'slds-tabs--scoped__content slds-show customClassForTabData';
    },
    controllerOnChange:function(component, event, helper) 
    {
        
      helper.onChange(component, event, helper);
    },
    controlleronclickEditworkingHours:function(component, event, helper) 
    {
        helper.onclickEditworkingHours(component, event, helper);
    },
    controlleronGetListofUser:function(component, event, helper) 
    {
        helper.onGetListofUser(component, event, helper);
    },
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    controllerUpdateWorkingHours: function(component, event, helper) 
    {
      helper.updateWorkingHours(component, event, helper);
    },
    controllerSaveUserDetails :function(component, event, helper) 
    {
		helper.saveUserDetails(component, event, helper);
	},
    controllerrefreshworkingHours:function(component, event, helper) 
    {
		helper.refreshworkingHours(component, event, helper);
	},
    controllerresetpasswordMethod:function(component, event, helper)
    {
        helper.resetpasswordMethod(component, event, helper);   
    },
    closepopup:function(component, event, helper)
    {
        helper.popup(component, event, helper);   
    }
})