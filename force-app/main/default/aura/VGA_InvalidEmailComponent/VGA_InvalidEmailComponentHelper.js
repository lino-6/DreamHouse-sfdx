({
	getloggingDetails :function(component, event, helper)
    {
     var action=component.get("c.getlogginguser");
     action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                     component.set("v.objName",result);  
                                }
                              });
           $A.enqueueAction(action);
        this.getUserRole(component, event, helper);
        this.getInvalidEmail(component, event, helper);
    },
    getInvalidEmail :function(component, event, helper)
    {
     var action=component.get("c.getlistofInvalidEmail");
    action.setParams({"sortField": component.get("v.selectedTabsoft"),
                          "isAsc": component.get("v.isAsc")});     
     action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                     component.set("v.listofInvalidEmail",result);
                                     component.set("v.maxPage", Math.floor((result.length+9)/10));
                                     this.renderPage(component);
                                }
                              });
           $A.enqueueAction(action); 
   
    },
    saveEmail:function(component, event, helper)
    {
        var lst   =component.get("v.listofInvalidEmailcompoment");
        var recId =event.target.id;
        var emailId;
        for(var i=0;i<lst.length;i++)
        {
            if(lst[i].objInvalidEmail.Id == recId)
            {
               if(lst[i].objInvalidEmail.VGA_Email_Address__c)
               {
                   emailId  =lst[i].objInvalidEmail.VGA_Email_Address__c;
               }
                else
                {
                    this.showToast(component,'Error', 'please enter Valid Email', 'error');
                    
                }
            }
          
          
        }
        if(emailId)
        {
            var action=component.get("c.updateInvalidEmail");
            action.setParams({"EmailId" :emailId,
                              "RecordId" :recId});             
            action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       {
                                               this.showToast(component,'Success', result, 'success');
                                               $A.get('e.force:refreshView').fire(); 
                                       }  
                               }
                          });
        $A.enqueueAction(action); 
        
        }
    },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 3000);
      },
    ignoreall:function(component, event, helper)
    {
         var recId =event.target.id;
         var result = confirm('Are you sure you want to remove this record from the welcome experience?');
         if(result ==true)
         {
          var action=component.get("c.updateIgnore");
          action.setParams({"recordId" :recId});             
          action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var errorvalue= result.indexOf('Error:');
                                       if(errorvalue != -1 )
                                       {
                                            this.showToast(component,'ERROR',result, 'error'); 
                                       }
                                       else
                                       {
                                               this.showToast(component,'Success', result, 'success');
                                               $A.get('e.force:refreshView').fire(); 
                                       }  
                               }
                          });
        $A.enqueueAction(action); 
       }
    },
    getUserRole:function(component, event, helper) 
    {
         var action=component.get("c.getuserInformation"); //To call Apex controller method
         action.setCallback(this, function(actionresult) 
                          {
                            var state = actionresult.getState(); //get state value.
                            if(state === "SUCCESS")
                            {
                                 var result =actionresult.getReturnValue(); // get return result value.
                                 component.set("v.userRole",result);
                            }
                          });
        $A.enqueueAction(action);
    },
     
    renderPage: function(component) {
		var records = component.get("v.listofInvalidEmail"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
        component.set("v.listofInvalidEmailcompoment", pageRecords);
	},
    sortHelper: function(component, event, helper) 
      {
        var currentDir = component.get("v.arrowDirection");
        if (currentDir == 'arrowdown') 
        {
             // set the arrowDirection attribute for conditionally rendred arrow sign  
             component.set("v.arrowDirection", 'arrowup');
             // set the isAsc flag to true for sort in Assending order.  
             component.set("v.isAsc", true);
        } 
        else 
        {
             component.set("v.arrowDirection", 'arrowdown');
             component.set("v.isAsc", false);
        }
          // call the onLoad function for call server side method with pass sortFieldName 
           this.getInvalidEmail(component, event, helper);
      }
   
})