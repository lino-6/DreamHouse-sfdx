({
	doInit : function(component, event, helper) 
    {
			helper.getloggingDetails(component, event, helper);
	},
    controllersaveEmail: function(component, event, helper) 
    {
        helper.saveEmail(component, event, helper);
    },
    closeToast : function(cmp, event, helper)
    {
     cmp.set("v.errormsg",'');
    },
    controllerRefreshView: function(component, event, helper)
    {
      helper.refreshView(component, event, helper);
    },
    controllerIgnore: function(component, event, helper)
    {
      helper.ignoreall(component, event, helper);
    },
    controllergetUserRole: function(component, event, helper)
    {
      helper.getUserRole(component, event, helper);
    },
    renderPage: function(component, event, helper)
    {
        helper.renderPage(component);
    },
    sortType:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Type__c');
         helper.sortHelper(component, event, helper);
    },
    sortFirst:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_First_Name__c');
         helper.sortHelper(component, event, helper);
    },
    sortVin:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_VIN__c');
         helper.sortHelper(component, event, helper);
    },
    sortCommission:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Commission_Number__c');
         helper.sortHelper(component, event, helper);
    },
    sortLastName:function(component, event, helper)
    {
         component.set("v.selectedTabsoft", 'VGA_Last_Name__c');
         helper.sortHelper(component, event, helper);
    }
})