({
    HideMe: function(component, event, helper) {
        component.set("v.ShowModule", false);
    },
    controllercreateEvents:function(component, event, helper) 
    {
        helper.createEvents(component, event, helper);
       
    },
    doneRendering: function(component, event, helper) {
        
        var startTime    = component.get("v.StartTime");
        var endTime      = component.get("v.EndTime");
            
        if(startTime 
            && endTime){
            component.set("v.isDoneRendering", true);
           var record = component.get("v.EventDetails");
        
            record.StartDateTime = startTime;
            record.EndDateTime   = endTime;

            component.set("v.EventDetails", record);
        }
      },
      doAction : function(component, event,helper) {
        function pad(n) {
            return n<10 ? '0'+n : n;
        }

        function getUTCDate(newdate) {
            var dt = new Date(newdate);
            dt.setTime(dt.getTime() - (11*60*60*1000));
            var date = dt.getDate();
            var month = dt.getMonth(); //Be careful! January is 0 not 1
            var year = dt.getFullYear();
            var hour = dt.getHours();
            var minute = dt.getMinutes();
            var second = dt.getSeconds();

            var dateString = pad(year) + "-" +pad(month + 1) + "-" + pad(date) ;
            dateString += "T" + pad(hour) + ":" +pad(minute) + ":" + pad(second) +".000Z";
            return dateString;
        }

        var params = event.getParam('arguments');
        if (params) {
            var startTime = params.param1;
            var endTime = params.param2;
            var record = component.get("v.EventDetails");
            var timezone = $A.get("$Locale.timezone");

            if(startTime
               && endTime){
                
                if(startTime.includes(":")){
                    /*record.StartDateTime = startTime + '+11:00[Australia/NSW]';
                    record.EndDateTime   = endTime   + '+11:00'; */
                    /*record.StartDateTime = $A.localizationService.formatDateTime(startTime);
                    record.EndDateTime   = $A.localizationService.formatDateTime(endTime);*/

                    
                    record.StartDateTime = getUTCDate(startTime);
                    record.EndDateTime   = getUTCDate(endTime);
                } else {
                    var dt = new Date(endTime);
                    dt.setDate( dt.getDate() - 1 );

                    var date = dt.getDate();
                    var month = dt.getMonth(); //Be careful! January is 0 not 1
                    var year = dt.getFullYear();

                    var dateString = pad(year) + "-" +pad(month + 1) + "-" + pad(date);

                    if(timezone == "Australia/Adelaide"){
                        record.StartDateTime = startTime  + 'T02:30:00.000Z';
                        record.EndDateTime   = dateString + 'T03:30:00.000Z';
                    }else{
                        record.StartDateTime = startTime  + 'T02:00:00.000Z';
                        record.EndDateTime   = dateString + 'T03:00:00.000Z';
                    }
                }
                
                component.set("v.EventDetails", record);
            }
        }
    },getNewValue: function (component, event, helper) {
        var OptionSel = component.find("select1").get("v.value");        
        if(OptionSel == "Test Drive Appointments")
        {
            component.set("v.ShowLeadDetails", true);
            var activeBList = component.get('v.ActiveBrochures');
            //We should only get the model list the first time we show this field
            if(activeBList.length ==0)
            {
                helper.getModelList(component, event, helper);
            }            
            
            helper.getPilotUsersList(component, event, helper);           
        }
        else 
        {
            component.set("v.ShowLeadDetails", false);
        }
    },closeToast : function(cmp, event, helper)
    {
        cmp.set("v.ShowModule", false);
        cmp.set("v.errormsg",'');
    },
    ControllerErrorHandle:function(component, event, helper)
    {
        helper.handleError(component, event, helper);
    },
    ControllerhandleClearError:function(component, event, helper)
    {
        helper.handleClearError(component, event, helper);
    },
    onCheck: function(cmp, evt) {
         var checkCmp = cmp.find("marketingOpt");
         var value = checkCmp.get("v.value");
         cmp.set("v.MarketingOpt",value);

    },
    controllerOnChangeResignEvent:function(component, event, helper) 
    {
		component.set("v.showIASpinner", true);
        helper.onChangeReassignEvent(component, event, helper);
	}
})