({
    createEvents :function(component, event, helper)
    {
        this.requiredField(component, event, helper);
        var flag  =component.get("v.RequiredField");
   	
        if(flag==false)
        {
            var myCustomComponent = component.find("saveBtn").getElement();
            if(myCustomComponent !=null){
                $A.util.addClass(myCustomComponent, 'disableSave');
            	event.preventDefault();
            }
            
            var eventDetails = component.get("v.EventDetails");  
            var selectedType = component.get("v.selectedValue");
            var marketingOptIn = component.get("v.MarketingOpt");
            var eventOwnerId   = component.get("v.assignEventOwnerId");

            var modelSelect = component.find('Models');
            var selectedM = '';
            if(modelSelect)
            {
                selectedM = component.find('Models').get('v.value');
            }            
            var action = component.get("c.createEvents"); 
                 action.setParams({"newEvent"       :eventDetails, 
                              "eventType"      :selectedType,
                              "selectedModel"  :selectedM,
                              "eventOwnerId"   :eventOwnerId,
                              "marketingOptIn" : marketingOptIn});
            action.setCallback(this, function(actionresult) 
            {
                var state = actionresult.getState();
                if(state === "SUCCESS")
                {
                    var result =actionresult.getReturnValue();
                    var errorvalue= result.indexOf('Error:');
                    if(errorvalue != -1 )
                    {
                        var myCustomComponent = component.find("saveBtn").getElement();
                        if(myCustomComponent !=null){
                            $A.util.removeClass(myCustomComponent, 'disableSave');
                            event.preventDefault();
                        }

                        this.showToast(component,'ERROR',result, 'error'); 
                    }
                    else
                    {
                        //component.set("v.popupflag",false);
                        this.showToast(component,'Success', result, 'success');
                        // Get the component event by using the
                        // name value from aura:registerEvent
                        var cmpEvent = component.getEvent("cmpEvent");
                        cmpEvent.setParams({
                            "message" : "A component event fired me. " +
                            "It all happened so fast. Now, I'm here!" });
                        cmpEvent.fire();
                    } 
            
                }
            });
            $A.enqueueAction(action);
        }
    },
    showToast : function(cmp, title, message, type ) 
    {
        cmp.set("v.errormsg",message);
        cmp.set("v.title",title);
        //---- Setting up severity ----//
        if(type == 'info')
        {
        cmp.set("v.type",'');
        cmp.set("v.icon",'utility:info');
        }
        else if(type == 'warning')
        {
        cmp.set("v.type",'slds-theme--error');
        cmp.set("v.icon",'utility:warning');
        }
        else if(type == 'error')
        {
        cmp.set("v.type",'slds-theme--error');
        cmp.set("v.icon",'utility:error');
        }
        else if(type == 'success')
        {
        cmp.set("v.type",'slds-theme--success');
        cmp.set("v.icon",'utility:success');
        }
        cmp.set("v.errormsg",message);
        window.setTimeout(function(){
            if(type == 'success'){
                cmp.set("v.ShowModule", false);
            }
            cmp.set("v.errormsg",'');
        }, 3000); 
    },
    getModelList : function(component, event, helper) {
		var act= component.get('c.getActiveBrochures');
        act.setCallback(this, function(resp){
            if(resp.getState()=='SUCCESS'){
                var res=resp.getReturnValue();               
                component.set('v.ActiveBrochures',res);
            }else{
                // display Error Message
                
            }
        });
        $A.enqueueAction(act);
    },
    getPilotUsersList : function(component, event, helper) {
		var action= component.get('c.getPilotContacts');
        component.set("v.showIASpinner", true);
        action.setCallback(this, function(resp){
               		var arrayOfEventMapKeys = [];
                    var resultPilotContacts =resp.getReturnValue();                    
                    for (var singlekey in resultPilotContacts)
                    {
                        //alert('result name' +resultPilotContacts[singlekey].Name );   
                        arrayOfEventMapKeys.push({
                                key: resultPilotContacts[singlekey].Id,
                                value: resultPilotContacts[singlekey].Name                                
                            });                  
                    } 
                    component.set('v.reassignEventlstKey', arrayOfEventMapKeys); 
                    component.set("v.showIASpinner", false);
        });
        $A.enqueueAction(action);
    },
    handleError:function(component, event, helper){
    
        var component = event.getSource();
        $A.util.addClass(component, "error");   
    },
    handleClearError:function(component, event, helper)
    {
        var component = event.getSource();
        $A.util.removeClass(component, "error");   
    },
    requiredField :function(component, event, helper)
    {
        var selectedType  = component.get("v.selectedValue");
        var type          = component.find("select1");
        var startTime     = component.find("startdate");
        var startTimevalue= startTime.get("v.value");
        console.log(startTimevalue);
        if($A.util.isEmpty(selectedType) || $A.util.isUndefined(selectedType) || selectedType == "Choose one...")
        {
            type.set("v.errors",[{message:"Select an Event Type."}]); 
            component.set("v.RequiredField",true);
        } 
        else if(selectedType != "Test Drive Appointments")
        {   
            var subject       =component.find("subject");
            var subjectvalue  =subject.get("v.value");
            var endTime       =component.find("enddate");
            var endTimevalue  =endTime.get("v.value");
            
            type.set("v.errors",null);

            if ($A.util.isEmpty(subjectvalue) || $A.util.isUndefined(subjectvalue)){
                subject.set("v.errors",[{message:"Enter Subject."}]); 
                component.set("v.RequiredField",true);
            } else if($A.util.isEmpty(startTimevalue) || $A.util.isUndefined(startTimevalue))
            {
                startTime.set("v.errors",[{message:"Enter Start Date Time."}]);
                component.set("v.RequiredField",true);
            }else if($A.util.isEmpty(endTimevalue) || $A.util.isUndefined(endTimevalue))
            {
                endTime.set("v.errors",[{message:"Enter End Date Time."}]);
                component.set("v.RequiredField",true);
            } else if(new Date(endTimevalue).getTime()<new Date(startTimevalue).getTime())
            {
                component.set("v.showErrorEndTime", false);
                component.set("v.showErrorStartTime", true);
                component.set("v.errorMessage", "End Date Time must be greater than Start Date Time.");
                component.set("v.RequiredField",true);
            } else
            {
                component.set("v.showErrorEndTime", false);
                component.set("v.showErrorStartTime", false);
                component.set("v.RequiredField",false);  
            }
        }else if($A.util.isEmpty(startTimevalue) || $A.util.isUndefined(startTimevalue))
        {
            component.set("v.showErrorStartTime", true);
            component.set("v.errorMessage", "Enter Start Date Time.");
            component.set("v.RequiredField",true);
        }
        else if(selectedType == "Test Drive Appointments")
        {   
            var firstName           =component.find("leadfirstname");
            var firstNamevalue      =firstName.get("v.value");
            var lastName            =component.find("leadlastname");
            var lastNamevalue       =lastName.get("v.value"); 
            var phone               =component.find("phone");
            var phonevalue          =phone.get("v.value");
            var email               =component.find("email");
            var emailvalue          =email.get("v.value");
            var modelSelect         = component.find('Models');
            var modelSelectvalue    = component.find('Models').get('v.value');

            if($A.util.isEmpty(firstNamevalue) || $A.util.isUndefined(firstNamevalue))
            {
                 firstName.set("v.errors",[{message:"Enter First Name."}]); 
                 component.set("v.RequiredField",true);  
            } else if($A.util.isEmpty(lastNamevalue) || $A.util.isUndefined(lastNamevalue))
            {
                 lastName.set("v.errors",[{message:"Enter Last Name."}]); 
                 component.set("v.RequiredField",true);  
            }else if($A.util.isEmpty(phonevalue) || $A.util.isUndefined(phonevalue))
            {
                 phone.set("v.errors",[{message:"Enter Phone."}]); 
                 component.set("v.RequiredField",true);  
            }else if($A.util.isEmpty(emailvalue) || $A.util.isUndefined(emailvalue))
            {
                 email.set("v.errors",[{message:"Enter Email."}]); 
                 component.set("v.RequiredField",true);  
            } else if($A.util.isEmpty(modelSelectvalue) || $A.util.isUndefined(modelSelectvalue))
            {
                modelSelect.set("v.errors",[{message:"Select a Model."}]); 
                component.set("v.RequiredField",true);
            } else
            {
                // Store Regular Expression That 99.99% Works.
                var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
                // check if Email field in not blank,
                // and if Email field value is valid then set error message to null, 
                // and remove error CSS class.
                // ELSE if Email field value is invalid then add Error Style Css Class.
                // and set the error Message.  
                // and set isValidEmail boolean flag value to false.
                
                if(!$A.util.isEmpty(emailvalue)){   
                        if(emailvalue.match(regExpEmailformat)){
                        email.set("v.errors", [{message: null}]);
                        $A.util.removeClass(email, 'slds-has-error');
                        component.set("v.RequiredField",false);
                    }else{
                        $A.util.addClass(email, 'slds-has-error');
                        email.set("v.errors", [{message: "Enter a valid Email Address"}]);
                        component.set("v.RequiredField",true);
                    }
                }
                  
            }
        } 
        else
        {
            component.set("v.showErrorEndTime", false);
            component.set("v.showErrorStartTime", false);
            component.set("v.RequiredField",false);  
        }
     },
    onChangeReassignEvent:function (component, event, helper)
    {
   
        var selectedValue= event.getSource().get("v.value");
        component.set('v.assignEventOwnerId',selectedValue);  
         component.set("v.showIASpinner", false);
    },
    validateEmail : function(component, event, helper) {
        var isValidEmail = true; 
        var emailField       = component.find("email");
        var emailFieldValue  = email.get("v.value");
        // Store Regular Expression That 99.99% Works. [ http://emailregex.com/] 
        var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
        // check if Email field in not blank,
        // and if Email field value is valid then set error message to null, 
        // and remove error CSS class.
        // ELSE if Email field value is invalid then add Error Style Css Class.
        // and set the error Message.  
        // and set isValidEmail boolean flag value to false.
        
        if(!$A.util.isEmpty(emailFieldValue)){   
            if(emailFieldValue.match(regExpEmailformat)){
			  emailField.set("v.errors", [{message: null}]);
              $A.util.removeClass(emailField, 'slds-has-error');
              isValidEmail = true;
        }else{
             $A.util.addClass(emailField, 'slds-has-error');
             emailField.set("v.errors", [{message: "Please Enter a Valid Email Address"}]);
             isValidEmail = false;
        }
       }
        
     // if Email Address is valid then execute code     
       if(isValidEmail){
         // code write here..if Email Address is valid. 
       }
	}
})