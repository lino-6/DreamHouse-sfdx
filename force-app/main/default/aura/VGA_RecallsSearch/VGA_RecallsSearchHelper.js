({
   display : function(component, event, helper){
  
         
       var brnd=component.find("brandValue").get("v.value");
       var searchDatabase=component.get("v.vgaCampaigns") ;
       //  alert('searchDatabase'+searchDatabase.length);
       var modalYear = component.find("yearsIds").get("v.value");
         //var modalYear = component.find("yearsIds").get("v.value");
       var modalAlias = component.find("modelAliasIds").get("v.value");
       var states=component.find("postalcodeIds").get("v.value");
       var criteria =   component.find("Filter").get("v.value"); 
       var cmptype =   component.find("conCountry").get("v.value"); 
       var cmpcode =   component.find("conState").get("v.value"); 
       var searchWrap ={};
       var wrapper=new Array();
       if(brnd == '--None--')
        {
            this.showToast(component,'error', 'Please select Brand', 'error'); 
        }
         
       else if(cmptype == '--- None ---')
        {
            this.showToast(component,'error', 'Please select Campaign Type', 'error'); //To show popup
        }
       
        searchWrap.brand=brnd;
        // alert(criteria);
		searchWrap.criteria=criteria;
       searchWrap.Cmptype=cmptype;
       searchWrap.Cmpcode=cmpcode;
        searchWrap.ModalYear=modalYear;
        searchWrap.ModelAlias=modalAlias;
        searchWrap.States =states;
        
        //var searchCmp = component.set('v.searchwrap',searchWrap);
         

         var action=component.get('c.getRecalllist');
         action.setParams({ 
             "requestParams" : JSON.stringify(searchWrap),
             "isschedule" : false
              
            
   		 });
         action.setCallback(this,function(response){
             
             if(response.getState()==='SUCCESS'){
                
                 var cmp = response.getReturnValue();
                 console.log(cmp.length);
                  if (cmp.length == 0) {
                    component.set("v.Message", true);
                } else 
                {
                    component.set("v.Message", false);
                }
                 component.set("v.vgaCampaignsList",cmp);
                 component.set("v.TotalNumberOfRecord",cmp.length);
                 component.set("v.maxPage", Math.floor((cmp.length+9)/100));
                this.renderPage(component);
             } 
         });
         $A.enqueueAction(action);
     },
    showToast : function(cmp, title, message, type ) 
     {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 5000);
      },
    
     renderPage: function(component) {
		var records = component.get("v.vgaCampaignsList"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*100, pageNumber*100);
        
         component.set("v.vgaCampaignsListcomponent", pageRecords);
         
       
	},
    
  getModelAliasList :function(component, event, helper)
    {
        var brnd=component.find("brandValue").get("v.value");
       	var action=component.get("c.getModelAliasList");
        action.setParams({ 
             "brand" : brnd
            
   		 });
        
            action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var arrayOfMapKeys = []; 
                                    arrayOfMapKeys.push({
                                       
                                        key: "--- None ---",
                                        value: ""
                                    });
                                    for (var singlekey in result)
                                    {
                                       arrayOfMapKeys.push({
                                        key: singlekey,
                                        value: result[singlekey]
                                                          });
                                    }
                                    component.set("v.modelAliasList",arrayOfMapKeys);  
                                }
                              });
               $A.enqueueAction(action);
    },
    
  getCampaigncode :function(component, event, helper)
    {
        var Cmptyp=component.find("CampaignType").get("v.value");
       	var action=component.get("c.getCampaigncode");
        action.setParams({ 
             "CampaignType" : Cmptyp
            
   		 });
        
            action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var arrayOfMapKeys = []; 
                                    arrayOfMapKeys.push({
                                       
                                        key: "--- None ---",
                                        value: ""
                                    });
                                    for (var singlekey in result)
                                    {
                                       arrayOfMapKeys.push({
                                        key: singlekey,
                                        value: result[singlekey]
                                                          });
                                    }
                                    component.set("v.compaigncodelist",arrayOfMapKeys);  
                                }
                              });
               $A.enqueueAction(action);
    },  
    
  getStates :function(component, event, helper)
    {
                 var action=component.get("c.getStates");
                    action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    var arrayOfMapKeys = []; 
                                     arrayOfMapKeys.push({
                                       
                                        key: "--- None ---",
                                        value: ""
                                    });
                                    for (var singlekey in result)
                                    {
                                       arrayOfMapKeys.push({
                                        key: singlekey,
                                        value: result[singlekey]
                                                          });
                                    }
                                    component.set("v.postalcodeList",arrayOfMapKeys);  
                                }
                              });
           $A.enqueueAction(action);
    },
    
  getyears :function(component, event, helper)
         {
              var Alias = component.find("modelAliasIds").get("v.value");
              var action=component.get("c.getyears");
             
             action.setParams({ 
             "Alias" : Alias
            
   		 });
                   // alert('start');
                      var arrayOfMapKeys = []; 
                      arrayOfMapKeys.push
                            ({            
                                key: "--- None ---",
                                value: ""
                                });
                               for (var year=2007; year < 2018; year++)
                                  {
                                        arrayOfMapKeys.push({
                                          key: year,
                                         value: year
                               });
             }
        component.set("v.yearsList",arrayOfMapKeys);
       // alert(arrayOfMapKeys[2]['key']);
        
    },
    
  fetchPickListVal: function(component, fieldName, elementId) 
    {
        
        var action = component.get("c.getselectOptions");
        
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        class: "optionClass",
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        class: "optionClass",
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.find(elementId).set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
   
  fetchPicklistValues: function(component, controllerField, dependentField) {
      // call the server side function  
      var action = component.get("c.getDependentOptionsImpl");
      
      // pass paramerters [object name , contrller field name ,dependent field name] -
      // to server side function 
 
      action.setParams({
         'objApiName': component.get("v.objInf"),
         'contrfieldApiName': controllerField,
         'depfieldApiName': dependentField
      });
      //set callback   
      action.setCallback(this, function(response) {
         if (response.getState() == "SUCCESS") {
            //store the return response from server (map<string,List<string>>)  
            var StoreResponse = response.getReturnValue();

 
            // once set #StoreResponse to depnedentFieldMap attribute 
            component.set("v.depnedentFieldMap", StoreResponse);
 
            // create a empty array for store map keys(@@--->which is controller picklist values) 
 
            var listOfkeys = []; // for store all map keys (controller picklist values)
            var ControllerField = []; // for store controller picklist value to set on ui field. 
 
            // play a for loop on Return map 
            // and fill the all map key on listOfkeys variable.
            for (var singlekey in StoreResponse) {
               listOfkeys.push(singlekey);
            }
 
            //set the controller field value for ui:inputSelect  
            if (listOfkeys != undefined && listOfkeys.length > 0) {
               ControllerField.push({
                  class: "optionClass",
                  label: "--- None ---",
                  value: "--- None ---"
               });
            }
 
            for (var i = 0; i < listOfkeys.length; i++) {
               ControllerField.push({
                  class: "optionClass",
                  label: listOfkeys[i],
                  value: listOfkeys[i]
               });
            }
            // set the ControllerField variable values to country(controller picklist field)
            component.find('conCountry').set("v.options", ControllerField);
             //component.find('Filter').set("v.options", ControllerField);
         }
      });
      $A.enqueueAction(action);
   },
    
   fetchPicklistValue: function(component, controllerField, dependentField) {
      // call the server side function  
      var action = component.get("c.getDependentOptionsImpl");
       
      // pass paramerters [object name , contrller field name ,dependent field name] -
      // to server side function 
 
      action.setParams({
         'objApiName': component.get("v.objInf"),
         'contrfieldApiName': controllerField,
         'depfieldApiName': dependentField
      });
      //set callback   
      action.setCallback(this, function(response) {
         if (response.getState() == "SUCCESS") {
            //store the return response from server (map<string,List<string>>)  
            var StoreResponse = response.getReturnValue();
 
            // once set #StoreResponse to depnedentFieldMap attribute 
            component.set("v.depnedentFieldMap1", StoreResponse);
 
            // create a empty array for store map keys(@@--->which is controller picklist values) 
 
            var listOfkeys = []; // for store all map keys (controller picklist values)
            var ControllerField = []; // for store controller picklist value to set on ui field. 
 
            // play a for loop on Return map 
            // and fill the all map key on listOfkeys variable.
            for (var singlekey in StoreResponse) {
               listOfkeys.push(singlekey);
            }
 
            //set the controller field value for ui:inputSelect  
            if (listOfkeys != undefined && listOfkeys.length > 0) {
               ControllerField.push({
                  class: "optionClass",
                  label: "--- None ---",
                  value: "--- None ---"
               });
            }
 
            for (var i = 0; i < listOfkeys.length; i++) {
               ControllerField.push({
                  class: "optionClass",
                  label: listOfkeys[i],
                  value: listOfkeys[i]
               });
            }
            // set the ControllerField variable values to country(controller picklist field)
            component.find('conCountry').set("v.options", ControllerField);
         }
      });
      $A.enqueueAction(action);
   },  
 
 fetchDepValues1: function(component, ListOfDependentFields) {
      // create a empty array var for store dependent picklist values for controller field)  
      var dependentFields = [];
 
      if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
         dependentFields.push({
            class: "optionClass",
            label: "--- None ---",
            value: "--- None ---"
         });
 
      }
      for (var i = 0; i < ListOfDependentFields.length; i++) {
         dependentFields.push({
            class: "optionClass",
            label: ListOfDependentFields[i],
            value: ListOfDependentFields[i]
         });
      }
      // set the dependentFields variable values to State(dependent picklist field) on ui:inputselect    
        //component.find('conState').set("v.options", dependentFields);
        component.find('Filter').set("v.options", dependentFields);
      // make disable false for ui:inputselect field 
      component.set("v.isDependentDisable", false);
   },
   fetchDepValues: function(component, ListOfDependentFields) {
      // create a empty array var for store dependent picklist values for controller field)  
      var dependentFields = [];
 
      if (ListOfDependentFields != undefined && ListOfDependentFields.length > 0) {
         dependentFields.push({
            class: "optionClass",
            label: "--- None ---",
            value: "--- None ---"
         });
 
      }
      for (var i = 0; i < ListOfDependentFields.length; i++) {
         dependentFields.push({
            class: "optionClass",
            label: ListOfDependentFields[i],
            value: ListOfDependentFields[i]
         });
      }
      // set the dependentFields variable values to Campaign Code(dependent picklist field) on ui:inputselect    
        component.find('conState').set("v.options", dependentFields);
        //component.find('Filter').set("v.options", dependentFields);
      // make disable false for ui:inputselect field 
      component.set("v.isDependentDisable", false);
   },  
    
 updateasBatch : function(component, event, helper)
    
    {
  
         
       var brnd=component.find("brandValue").get("v.value");
       var searchDatabase=component.get("v.vgaCampaigns") ;
       var modalYear = component.find("yearsIds").get("v.value");
       var modalAlias = component.find("modelAliasIds").get("v.value");
       var states=component.find("postalcodeIds").get("v.value");
       var criteria =   component.find("Filter").get("v.value");
        var cmptype =   component.find("conCountry").get("v.value"); 
       var cmpcode =   component.find("conState").get("v.value"); 
       var searchWrap ={};
       var wrapper=new Array();
       
        searchWrap.brand=brnd;
        // alert(criteria);
		searchWrap.criteria=criteria;
        searchWrap.Cmptype=cmptype;
       searchWrap.Cmpcode=cmpcode;
        searchWrap.ModalYear=modalYear;
        searchWrap.ModelAlias=modalAlias;
        searchWrap.States =states;
        
        //var searchCmp = component.set('v.searchwrap',searchWrap);
         

         var action=component.get('c.getRecalllist');
         action.setParams({ 
             "requestParams" : JSON.stringify(searchWrap),
             "isschedule" : true
         });
        
         action.setCallback(this,function(response)
             
              {
                     var state = response.getState();
                  
                             if (state === "SUCCESS") 
                              {
                                   var resultsToast = $A.get("e.force:showToast");
                                     resultsToast.setParams({
                                                //"title" : "Call requested",
                                               "message" : "Records Updated Sucessfully."
                                              });
                                  
                                  
                                   //Update the UI: closePanel, show toast, refresh page
                                               $A.get("e.force:closeQuickAction").fire();
                                                          resultsToast.fire();
                                                  $A.get("e.force:refreshView").fire();
                                               }
                                               else if(state === "ERROR") 
                                               {
                                        console.log('Problem updating call, response state '+state);
                                            }else
                                            {
                                                            console.log('Unknown problem: '+state);
                                             }                                  
                                  
                          
              });
         $A.enqueueAction(action);
             
          
     },
    
     
 updateSelectedHelper: function(component, event, deleteRecordsIds)
    {
  
              var vgaList=component.get('v.vgaCampaignsList');//call apex class method
        // alert(vgaList);
              var updateid = [];// create var for store record id's for selected checkboxes  
              var getAllId = component.find("boxPack");// get all checkboxes
  				
                  if(! Array.isArray(getAllId))  // If the local ID is unique[in single record case], find() returns the component. not array
                      {
                        if (getAllId.get("v.value") == true) 
                        {
                            updateid.push(getAllId.get("v.text"));
                        }
                     }
                   else{
    
     						// if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
                           for (var i = 0; i < getAllId.length; i++) 
                            {
                              if (getAllId[i].get("v.value") == true)
                              {
                               updateid.push(getAllId[i].get("v.text"));
                              }
       
                           }
                         }  
        
                  var action = component.get('c.UpdateStatus'); // pass the all selected record's Id's to apex method 
  
                 action.setParams({
                   "lstrecordid" : updateid
                });
        
                      action.setCallback(this, function(response) //store state of response
                       {
   
                            var state = response.getState();
                             if (state === "SUCCESS") 
                              {
                                 console.log(state);
                                if (response.getReturnValue() != '') 
                                {
    
                                  // fetchPicklistValues('The following error has occurred. while Update record-->' + response.getReturnValue());
                                } 
                                else {
                                   //console.log('check it--> Update successful');
                                      }
                                    // call the onLoad function for refresh the List view    
                                    //this.display(component, event);
                                }
      
                          });
              $A.enqueueAction(action);
      },

    
 onChangeGetValue: function(component, event, helper) 
    
    {
         
    }
})