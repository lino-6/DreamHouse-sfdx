({
	doInit : function(component, event, helper) 
    {   
       // alert('yes');
		helper.getModelAliasList(component, event, helper);  
        helper.getStates(component, event, helper);
        helper.getyears(component, event, helper);
        //helper.fetchPickListVal(component, 'Filter_criteria__c', 'accIndustry');
       //helper.fetchPickListVal(component, 'VGA_SC_Campaign_Type__c', 'CampaignType');

        
        helper.fetchPicklistValue(component, 'VGA_SC_Campaign_Type__c', 'Filter_criteria__c');
        helper.fetchPicklistValues(component, 'VGA_SC_Campaign_Type__c', 'Campaign_Code__c');
       // helper.getCampaigncode(component, event, helper);
       
        
	},
    
   onControllerFieldChange: function(component, event, helper) {
     //alert(event.getSource().get("v.value"));
      // get the selected value
      var controllerValueKey = event.getSource().get("v.value");
 
      // get the map values   
      var Map = component.get("v.depnedentFieldMap");
      var Map1 = component.get("v.depnedentFieldMap1");
 
      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (controllerValueKey != '--- None ---') {
 
         // get dependent values for controller field by using map[key].  
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[controllerValueKey];
          //alert(ListOfDependentFields);
          var ListOfDependentFields1 = Map1[controllerValueKey];
          helper.fetchDepValues(component, ListOfDependentFields);
          helper.fetchDepValues1(component, ListOfDependentFields1);
 
      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
          component.find('conState').set("v.options", defaultVal);
          component.find('Filter').set("v.options", defaultVal);
         component.set("v.isDependentDisable", true);
      }
   },
    
     onControllerFieldChange1: function(component, event, helper) {
        
    //  alert(event.getSource().get("v.value"));
      // get the selected value
      var controllerValueKey = event.getSource().get("v.value");
 
      // get the map values   
      var Map = component.get("v.depnedentFieldMap");
 
      // check if selected value is not equal to None then call the helper function.
      // if controller field value is none then make dependent field value is none and disable field
      if (controllerValueKey != '--- None ---') {
 
         // get dependent values for controller field by using map[key].  
         // for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
         // map['India'] = its return all dependent picklist values.
         var ListOfDependentFields = Map[controllerValueKey];
         helper.fetchDepValues(component, ListOfDependentFields);
 
      } else {
         var defaultVal = [{
            class: "optionClass",
            label: '--- None ---',
            value: '--- None ---'
         }];
         component.find('Filter').set("v.options", defaultVal);
         component.set("v.isDependentDisable", true);
      }
   },
     onDependentFieldChange: function(component, event, helper) {
      //alert(event.getSource().get("v.value"));
   },
    
   openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      
       var getSelectedNumber = component.get("v.selectedCount");
       var gettotal = component.get("v.TotalNumberOfRecord");
      if(getSelectedNumber == 0)
      {
          component.set("v.selectedCount", gettotal );
          component.set("v.selectedCountText", " vehicles");
      }
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   },
 
  
selectBrand: function(component, event, helper) 
    {
        helper.getModelAliasList(component, event, helper);
         helper.getyears(component, event, helper);
        var brnd=component.find("brandValue").get("v.value");
        //alert(brnd);
         if(brnd!='None')
        component.set('v.disabledPick',false);
        else
        component.set('v.disabledPick',true);
    },
  searchDatabase : function(component, event, helper) 
          {
                    console.log('Trying1');
        
                    //alert("displaying records");

		          helper.display(component, event, helper);
       
	    },
    
    
    
 selectyears: function(component, event, helper) 
    {
       // helper.getModelAliasList(component, event, helper);
         helper.getyears(component, event, helper);
        var Alias=component.find("modelAliasIds").get("v.value");
        //alert(brnd);
         if(Alias!='None')
        component.set('v.disabledPick',false);
        else
        component.set('v.disabledPick',true);
    },
 searchDatabase : function(component, event, helper) 
    
    {
        console.log('Trying1');
        
                    //alert("displaying records");

		 helper.display(component, event, helper);
       
	},
    
/* onPicklistChange: function(component, event, helper) {
       
    },*/
    
renderPage: function(component, event, helper) //For PageNation
    {
        helper.renderPage(component);
       
    },
  
  
 selectAll: function(component, event, helper) {
    
         var selectedHeaderCheck = event.getSource().get("v.value");//get the header checkbox value
         var getSelectedNumber = component.get("v.selectedCount"); // return the List of all checkboxs element
         var getAllId = component.find("boxPack"); // If the local ID is unique[in single record case], find() returns the component. not array 
   
     if(! Array.isArray(getAllId)){
       if(selectedHeaderCheck == true){ 
          component.find("boxPack").set("v.value", true);
          component.set("v.selectedCount", 1);
       }
       else{
           component.find("boxPack").set("v.value", false);
           component.set("v.selectedCount", 0);
       }
     }
     else{
       // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
       // and set the all selected checkbox length in selectedCount attribute.
       // if value is false then make all checkboxes false in else part with play for loop 
       // and select count as 0 
        if (selectedHeaderCheck == true) {
        for (var i = 0; i < getAllId.length; i++) {
  		  component.find("boxPack")[i].set("v.value", true);
   		 component.set("v.selectedCount", getAllId.length);
            
        }
        } else {
          for (var i = 0; i < getAllId.length; i++) {
    		component.find("boxPack")[i].set("v.value", false);
   			 component.set("v.selectedCount", 0);
              
  	    }
       } 
     }  
         
  
 
 },
    checkboxSelect: function(component, event, helper) 
    {
        
      // get the selected checkbox value  
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = 0;
        var boxPack = component.find("boxPack");
        var boxHead = component.find("box3");
        for (var i = 0; i < boxPack.length; i++) 
        {
            if(boxPack[i].get("v.value") == true)
            {
                getSelectedNumber++;
            }
        }
        //alert(count);
       component.set("v.selectedCount", getSelectedNumber); 
       boxHead.set("v.value", (boxPack.length == getSelectedNumber));
        
       if(getSelectedNumber > 1)
       {
           component.set("v.selectedCountText", " vehicles");
       }
        else
        {
            component.set("v.selectedCountText", " vehicle");
        }
 
 } ,  
 updateselected: function(component, event, helper) {
  
  
     component.set("v.selectedCount", 0);
      component.set("v.isOpen", false);
     
     var flagForCheckbox = 0;
     var getAllId = component.find("boxPack");// get all checkboxes
  				
     if(! Array.isArray(getAllId))  // If the local ID is unique[in single record case], find() returns the component. not array
     {
         if (getAllId.get("v.value") == true) 
         {
             flagForCheckbox = 1;
         }
     }
     else
     {
         // if value is checked(true) then add those Id (store in Text attribute on checkbox) in delId var.
         for (var i = 0; i < getAllId.length; i++) 
         {
             if (getAllId[i].get("v.value") == true)
             {
                flagForCheckbox = 1;
             }
             
         }
     }
     
     // call the helper function and pass all selected record id's.  
     if(flagForCheckbox == 1)  
     {
     	helper.updateSelectedHelper(component, event, helper);
     }
     else
     { 
        
     	helper.updateasBatch(component, event, helper);
       
     }
     
     //  this.searchDatabase(component, event, helper);
    // alert('Success');
     var action = component.get("c.searchDatabase");
     $A.enqueueAction(action);

        
 },    
    
})