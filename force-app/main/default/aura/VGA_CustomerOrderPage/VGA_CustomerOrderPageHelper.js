({ 
    getloggingDetails :function(component, event, helper)
    {
     var action=component.get("c.getlogginguser");
     action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                     component.set("v.objName",result);
                                    
                                }
                              });
           $A.enqueueAction(action); 
    }
   
})