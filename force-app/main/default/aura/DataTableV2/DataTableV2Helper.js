({
	queryRows : function(component, page, sOrder) {
        var action = component.get("c.getSobjects");
        //console.log("=== fields ===="+fields);
		var params = { "columnfields": component.get("v.columnfields"), "objName": component.get("v.object"), "currentPage": page, "lim": component.get("v.pagelimit") || 10
                      ,"sortField" : component.get("v.sortField"), "sortOrder" :  sOrder || "asc" , "whereCls":component.get("v.whereclause") };
        console.log(JSON.stringify(params));
        action.setParams(params);
        action.setCallback(this, function(response) {
            console.log(response.getState());
            if (response && response.getState() === "SUCCESS" && component.isValid()) {
            	var recordset = response.getReturnValue();
                if (component.get("v.columns").length <= 0) {
                	component.set("v.columns", recordset.columnHeader);   
                }
                console.log(recordset.rows);
                component.set("v.results", recordset.rows);
                component.set("v.resultsetsize", recordset.size);
            }
        });
        $A.enqueueAction(action);
	},
    getSortOrder : function(cmp, changeorder) {
        if (changeorder && changeorder === true) {
        	if (cmp.get("v.ascDescVal") === "asc") {
                cmp.set("v.ascDescVal", "desc");
            } else if (cmp.get("v.ascDescVal") === "desc") {
                cmp.set("v.ascDescVal", "asc");
            } else {
                cmp.set("v.ascDescVal", "desc");
            }    
        }
        return cmp.get("v.ascDescVal");
	},
    bulkSaveRecords: function(component, event, helper) {
		var action = component.get("c.bulkSaveRecords");
        var records = component.get("v.results");
        for (var i=0; i< records.length; i+=1) {
            console.log("record = "+JSON.stringify(records[i]));
        }
        action.setParams({"rowJSON":JSON.stringify(records), "obName":component.get("v.object")});
        action.setCallback(this, function(response) {
        	if (response && response.getState() === "SUCCESS") {
            	//console.log("bulksave resp = "+response.getReturnValue());
                var resultRsp = JSON.parse(response.getReturnValue());
                var erroroccured = false;
                for (var i=0; i<resultRsp.length; i+=1) {
                    if (resultRsp[i].success !== true || resultRsp[i].errors.length > 0) {
                    	erroroccured = true;    
                    }
                }
                //console.log("resultRsp == "+JSON.stringify(resultRsp));
                if (!erroroccured) {
                	component.set("v.messages", "Record Saved Successfully");
                	component.set("v.status", "success");     
                } else if (erroroccured === true) {
                    component.set("v.messages", "Error Occurred! Contact Your Administrator or Check Console Logs!");
                    component.set("v.status", "fail");
                }
            }
         });
        $A.enqueueAction(action);
	},
    getDealerDetails :function(component, event, helper)
    {
           var action=component.get("c.getlogginguser");
           action.setCallback(this, function(actionresult) 
                              {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                    var result =actionresult.getReturnValue();
                                    component.set("v.userId",result);
                                     if (result && result.length > 0 ) 
                                        {
                                                var whereCls = component.get("v.whereclause");
                                                whereCls = whereCls.toLowerCase().replace('{recordid}', '\''+component.get("v.recordId")+'\'');
                                                whereCls += " and VGA_Dealer_Account__c =" +"'" +result + "'";
                                                component.set("v.whereclause", whereCls);
                                                this.queryRows(component, 0, helper.getSortOrder(component, false));
                                        }
                                }
                              });
           $A.enqueueAction(action); 
    },
    onCheckBoxCheck :function (component, event, helper)
    {
        var lst=component.get('v.listofId');
        var selected =event.target.checked;
        var selectedId =event.target.id;
        if(selected)
        {
            lst.push(selectedId);  
        }
        else
        {
            if(lst && lst.length>0)
            {
                for(var i=0;i<lst.length;i++)
                {
                    if(lst[i] ==selectedId)
                    {
                        lst.splice(i,1) ; 
                    }
                }
            }
        }
        component.set("v.listofId" ,lst);
    },
    onClickMethod :function(component, event, helper)
    {
         var targetId =event.currentTarget.id;
         var result = confirm('Do you want to proceed?');
         if(result ==true)
         {
            var action =component.get("c.updateNofityCustomer");
             action.setParams({"orderIds":targetId});
             action.setCallback(this, function(actionresult) 
                           {
                                var state = actionresult.getState();
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue();
                                    $A.get('e.force:refreshView').fire();
                                }
                           });
          $A.enqueueAction(action);   
         }
    },
    deleteCustomerOrder : function(component, event, helper) 
    {
         var deleteId=component.get("v.listofId");
        if(deleteId && deleteId.length>0)
        {
             var action =component.get("c.deletecustomerOrder");
             action.setParams({"deleteIds":deleteId});
             action.setCallback(this, function(actionresult) 
                               {
                                    var state = actionresult.getState();
                                    if(state === "SUCCESS")
                                    {
                                         var result =actionresult.getReturnValue();
                                         $A.get('e.force:refreshView').fire();
                                    }
                               });
              $A.enqueueAction(action);
        }
        else
        {
          this.showToast(component,'Important', 'Please select atleast one customer order!', 'warning'); 
        }
    },
    showToast : function(cmp, title, message, type ) 
    {
             cmp.set("v.errormsg",message);
             cmp.set("v.title",title);
             
             //---- Setting up severity ----//
             if(type == 'info')
             {
              cmp.set("v.type",'');
              cmp.set("v.icon",'utility:info');
             }
             else if(type == 'warning')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:warning');
             }
             else if(type == 'error')
             {
              cmp.set("v.type",'slds-theme--error');
              cmp.set("v.icon",'utility:error');
             }
             else if(type == 'success')
             {
              cmp.set("v.type",'slds-theme--success');
              cmp.set("v.icon",'utility:success');
             }
             
             cmp.set("v.errormsg",message);
             window.setTimeout(function(){
             cmp.set("v.errormsg",'');
          }, 5000);
    },
     refreshView: function(component, event, helper) 
    {
         $A.get('e.force:refreshView').fire();
    }

})