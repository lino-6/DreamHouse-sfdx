({
    getloanCar : function(component, event, helper) 
    {
         var action=component.get("c.getloancar"); //To call Apex controller method
         action.setCallback(this, function(actionresult) 
                          {
                                var state = actionresult.getState(); //get state value.
                                if(state === "SUCCESS")
                                {
                                     var result =actionresult.getReturnValue(); // get return result value.
                                     component.set("v.loancar",result);     
                                }
                          });
        $A.enqueueAction(action);
	}
})