<apex:page showHeader="false" renderAs="pdf" sidebar="false" docType="html-5.0" >
    <head>
        <title>Volkswagen Recall Result</title>        
        <style>
        .tempName{ color: red; } 
            .btn-primary {
                color: #fff !important;
                background-color: #337ab7 !important;
                border-color: #2e6da4 !important;
            }
            .btn{
                display: inline-block !important;
                padding: 6px 12px !important;
                margin-bottom: 0 !important;
                font-size: 14px !important;
                font-weight: 400 !important;
                line-height: 1.42857143 !important;
                text-align: center !important;
                white-space: nowrap !important;
                vertical-align: middle !important;
                -ms-touch-action: manipulation !important;
                touch-action: manipulation !important;
                cursor: pointer !important;
                -webkit-user-select: none !important;
                -moz-user-select: none !important;
                -ms-user-select: none !important;
                user-select: none !important;
                background-image: none !important;
                border: 1px solid transparent !important;
                border-radius: 4px !important;            
                text-decoration: none ;
            }

            .btn:hover,active,visited
              {
                text-decoration:none;
               }
            
        @font-face {
            font-family: VWHead-Bold;
             src: url('{!URLFOR($Resource.fonts, 'VW/VWHeadWeb-Bold.woff')}') format('truetype');
          
        }

        @font-face {
             font-family: VWText-Regular;
             src: url('{!URLFOR($Resource.fonts, 'VW/VWTextWeb-Regular.woff')}') format('truetype');
             font-weight: 400; font-style: normal;
        }

        body {
            font-family: VWText-Regular,VWText,Prompt,Trebuchet MS,Helvetica,Arial,sans-serif;
        }          
            
        </style>
    </head>
    <body>
        
        <div style="text-align: right;">
             <apex:image url="{!$Resource.vwlogo}" width="125" height="63"/>
        </div>
        
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '0',true,false)}">
            <div>
                <p>
                    <b>Status: Unable to verify, please contact Volkswagen Group Australia to confirm </b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>
                    We are unable to verify if your vehicle with the Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall. We strongly recommend you contact our toll-free Volkswagen Recall Campaign Helpline on 1800 504 076 between 8:30am and 8:00pm Monday to Friday (AEDT) or email us at recalls@myvw.com.au.
                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>                       

            </div>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '1',true,false)}">
            <div>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>                    
                    We wish to confirm that your vehicle with the Vehicle Identification Number {!$CurrentPage.parameters.vin} is not affected by the Takata Airbag Safety Recall.
                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>     
                
            </div>
        </apex:outputPanel>  
        
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '2',true,false)}">
            <div>
                <p>
                    <b>Status: Complete</b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>                    
                    We wish to confirm that your {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall (PRA Number {!$CurrentPage.parameters.PRANo}) and categorised as ‘Complete’, meaning that, the Vehicle had an Affected Takata Airbag Inflator replaced and the replacement does not require future replacements.                                        
                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>                

            </div>
        </apex:outputPanel>     
        
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '3',true,false)}">
            <div>
             <p>
                    <b>Status: Active</b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>
                    <b>URGENT SAFETY RECALL - Your vehicle is installed with a faulty Takata airbag that must be replaced as soon as possible</b>
                </p>
                <p>                    
                    We wish to inform you that your {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall (PRA Number {!$CurrentPage.parameters.PRANo}) and categorised as ‘Active’ meaning the Vehicle is subject to an active recall, which requires the replacement of airbag inflator.
                </p>
                <p>
                    According to the Australian Government, your vehicle is installed with a faulty Takata airbag. As it gets older, a combination of high temperatures and humidity can affect airbags with the fault. If you are involved in a collision, the airbag can go off with too much explosive force causing sharp metal fragments to shoot out and <b>kill or seriously injure</b> people in the vehicle. It is important that you immediately contact an authorised Volkswagen Dealer to arrange for the airbag inflator to be replaced, free of charge. All affected Takata frontal airbags in Australia must be replaced by the end of 2020.
                </p>
                <p>
                    Click <a target="_blank" href="https://au.volkswagen.com.au/find-dealer/">here</a> to find your nearest authorised Volkswagen Dealer.
                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>                   

            </div>
        </apex:outputPanel>
        
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '4',true,false)}">
            <div>
                <p>
                   <b>Status: Future</b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>
                    <b>URGENT SAFETY RECALL - Your vehicle is installed with a faulty Takata airbag that will need to be replaced</b>
                </p>
                <p>
                    We wish to confirm that your {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall (PRA Number {!$CurrentPage.parameters.PRANo}) and categorised as ‘Future’ meaning the vehicle is not subject to active recall, but that recall action will be initiated for the Vehicle in the future, with the expected date of recall initiation stated according to the Recall Initiation Schedule.
                </p>
                <p>
                    According to the Australian Government, your vehicle has a faulty Takata airbag inflator. As it gets older, a combination of high temperatures and humidity can affect airbags with the fault. If you are involved in a collision, the airbag can go off with too much explosive force causing sharp metal fragments to shoot out and <b>kill or seriously injure</b> people in the vehicle. It is important that you arrange to have the airbag replaced as soon as Volkswagen notfies you that replacement parts are available and the vehicle has been placed on active recall.
                </p>
                <p>
                    For vehicles such as the affected Volkswagen vehicles that do not have Alpha airbags, the recall can be conducted in a staged process between now and the end of 2020. It is expected that customer notifications will be prioritised based on age of the vehicle (oldest first) and location (areas with high humidity will be given priority). The Recall Initiation Schedule indicates that the recall will occur on {!$CurrentPage.parameters.Sdate}.  
                </p>
                <p>
                     In the event that parts become available sooner that the Recall Initiation Schedule, Volkswagen will contact you directly to have your airbag inflator replaced, at no cost to you.  
                 </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>    

            </div>
        </apex:outputPanel>   
         <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '5',true,false)}">
            <div>
                <p>
                    Dear Volkswagen Customer,
                </p>
                <p>                    
                   This {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall and categorised as ‘Complete’, meaning that, the Vehicle had an Affected Takata Airbag Inflator which has either had its Affected Takata Airbag Inflator removed or the Affected Takata Airbag Inflator has been certified to have already deployed.  <b style="color: red;"> As this vehicle has been declared as a written-off vehicles (WoV), the Affected Takata Airbag Inflator would have not been replaced with a new airbag inflator at the time of salvage. </b> To the extent that a purchaser wishes to restore the vehicle to a roadworthy state, it will need to have a new driver-side airbag inflator (or in the case of Crafter models, a passenger and/or driver-side airbag) installed before being registered. Volkswagen Group Australia will install a new inflator <b> free of charge </b>, provided that the vehicle presents at an authorised Volkswagen Dealer fully repaired to a stock standard condition and in a roadworthy state (except for the missing driver-side front airbag inflator – or in the case of Crafter models, driver and/or passenger side front airbags).
                                       
               
                </p>
                <p>
                Bookings for the installation of a new inflator are essential and can be made by contacting your authorised <a href="https://au.volkswagen.com.au/find-dealer/ " >Volkswagen Dealer</a> or calling our dedicated Volkswagen Recall Campaign Hotline on 1800 504 076 between 8:30 am and 8:00 pm Monday to Friday (AEST) or visit our website: <u><a> www.volkswagen.com.au/en/about-volkswagen/airbag-recall.html.</a></u>
                </p>
                <p>
                This undertaking is valid until 31 December 2020.

                </p>
               <!-- <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>-->
                            

            </div>
        </apex:outputPanel>    
         <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '6',true,false)}">
            <div>
                <p>
                   <b>Status: Complete </b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                
                <p>
                    This {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall and categorised as ‘Complete’, meaning that, the Vehicle had an Affected Takata Airbag Inflator, which has had its Affected Takata Airbag Inflator removed. <b>As this vehicle has been declared as a written-off vehicle (WoV), the Affected Takata Airbag Inflator would have not been replaced with a new airbag inflator at the time of salvage.</b> To the extent that a purchaser wishes to restore the vehicle to a roadworthy state, it will need to have a new driver-side airbag inflator (or in the case of Volkswagen Up the driver-side airbag, in the case of Crafter models, a passenger and/or driver-side airbag) installed before being registered. Volkswagen Group Australia will install a new inflator <b>free of charge</b>, provided that the vehicle presents at an authorised Volkswagen Dealer fully repaired to a stock standard condition and in a roadworthy state (except for the missing driver-side front airbag inflator – or in the case of Volkswagen Up the driver-side airbag, or in the case of Crafter models the driver and/or passenger side front airbags). 
                </p>
                <p>
                   Bookings for the installation of a new inflator are essential and can be made by contacting your authorised <u><a href="https://au.volkswagen.com.au/find-dealer/ " >Volkswagen Dealer</a></u> or calling our dedicated Volkswagen Recall Campaign Hotline on 1800 504 076 between 8:30am and 8:00pm Monday to Friday (AEST) or visit our website: <u><a href="https://au.volkswagen.com.au/airbag-safety-recall/. " >www.volkswagen.com.au/en/about-volkswagen/airbag-recall.html.</a></u>
                </p>
                 <p>
                This undertaking is valid until 31 December 2020.          

                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>    
                

            </div>
        </apex:outputPanel> 
         <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '7',true,false)}">
            <div>
                 <p>
                    <b>Status: Unable to verify, please contact Volkswagen Group Australia to confirm </b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                
                <p>                    
                      We are unable to verify if your vehicle with the Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall. We strongly recommend you contact our toll-free Volkswagen Recall Campaign Helpline on 1800 504 076 between 8:30am and 8:00pm Monday to Friday (AEDT) or email us at recalls@myvw.com.au.                
                </p>
                    
                
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>    
                              

            </div>
        </apex:outputPanel>  
        <apex:outputPanel rendered="{!IF($CurrentPage.parameters.result == '8',true,false)}">
            <div>
                <p>
                   <b>Status: Complete </b>
                </p>
                <p>
                    Dear Volkswagen Customer,
                </p>
                
                <p>
                    This {!$CurrentPage.parameters.modelYear} Volkswagen {!$CurrentPage.parameters.modelName} with Vehicle Identification Number {!$CurrentPage.parameters.vin} is affected by the Takata Airbag Safety Recall and categorised as ‘Complete’, meaning that, the Vehicle had an Affected Takata Airbag Inflator, which has been certified to have already deployed. <b>As this vehicle has been declared as a written-off vehicle (WoV), the Affected Takata Airbag Inflator would have not been replaced with a new airbag inflator at the time of salvage</b>. To the extent that a purchaser wishes to restore the vehicle to a roadworthy state, it will need to have a new driver-side airbag (or in the case of Crafter models, a passenger and/or driver-side airbag) installed before being registered. Volkswagen Group Australia will install a new airbag <b>at your costs</b>, provided that the vehicle presents at an authorised Volkswagen Dealer fully repaired to a stock standard condition and in a roadworthy state.  
                </p>
                <p>
                   Bookings for the installation of a new inflator are essential and can be made by contacting your authorised <u><a href="https://au.volkswagen.com.au/find-dealer/ " >Volkswagen Dealer</a></u> or calling our dedicated Volkswagen Recall Campaign Hotline on 1800 504 076 between 8:30am and 8:00pm Monday to Friday (AEST) or visit our website: <u><a href="https://au.volkswagen.com.au/airbag-safety-recall/. " >www.volkswagen.com.au/en/about-volkswagen/airbag-recall.html.</a></u>
                </p>
                <p>
                This undertaking is valid until 31 December 2020.          

                </p>
                <p>
                   Date: 
                    <apex:outputText value="{0, date, dd/MM/yyyy}">
                        <apex:param value="{!NOW()}" />
                    </apex:outputText>
                </p>    
                

            </div>
        </apex:outputPanel>  
                         
        
    </body>
</apex:page>