<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>VGA_Brand__c</fullName>
        <externalId>false</externalId>
        <label>Brand</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Volkswagen</fullName>
                    <default>false</default>
                    <label>Volkswagen</label>
                </value>
                <value>
                    <fullName>Skoda</fullName>
                    <default>false</default>
                    <label>Skoda</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VGA_Dealer_Code__c</fullName>
        <externalId>false</externalId>
        <formula>VGA_Dealership__r.VGA_Dealer_Code_Text__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Dealer Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Dealership__c</fullName>
        <externalId>false</externalId>
        <label>Dealership</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Dealership Profile</relationshipLabel>
        <relationshipName>Dealership_Profiles</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>VGA_Distribution_Method__c</fullName>
        <externalId>false</externalId>
        <label>Distribution Method</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Round Robin</fullName>
                    <default>false</default>
                    <label>Round Robin</label>
                </value>
                <value>
                    <fullName>Nominated User</fullName>
                    <default>false</default>
                    <label>Nominated User</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VGA_Escalate_Minutes__c</fullName>
        <externalId>false</externalId>
        <label>Escalate (Minutes)</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Last_Modified_Email__c</fullName>
        <externalId>false</externalId>
        <formula>LastModifiedBy.Email</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Last Modified Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Last_User_Assigned__c</fullName>
        <externalId>false</externalId>
        <formula>VGA_Nominated_User__r.Username</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Last User Assigned</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Nominated_Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Nominated Contact</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Contact.AccountId</field>
                <operation>equals</operation>
                <valueField>$Source.VGA_Dealership__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Dealership Profiles</relationshipLabel>
        <relationshipName>Dealership_Profiles</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VGA_Nominated_User__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Nominated User</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>User.IsActive</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>User</referenceTo>
        <relationshipName>Dealership_Profiles</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>VGA_Sub_Brand__c</fullName>
        <externalId>false</externalId>
        <label>Sub Brand</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <controllingField>VGA_Brand__c</controllingField>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Passenger Vehicles (PV)</fullName>
                    <default>false</default>
                    <label>Passenger Vehicles (PV)</label>
                </value>
                <value>
                    <fullName>Commercial Vehicles (CV)</fullName>
                    <default>false</default>
                    <label>Commercial Vehicles (CV)</label>
                </value>
            </valueSetDefinition>
            <valueSettings>
                <controllingFieldValue>Volkswagen</controllingFieldValue>
                <controllingFieldValue>Skoda</controllingFieldValue>
                <valueName>Passenger Vehicles (PV)</valueName>
            </valueSettings>
            <valueSettings>
                <controllingFieldValue>Volkswagen</controllingFieldValue>
                <valueName>Commercial Vehicles (CV)</valueName>
            </valueSettings>
        </valueSet>
    </fields>
    <fields>
        <fullName>VGA_Unique_Key_2__c</fullName>
        <externalId>false</externalId>
        <formula>TRIM(LOWER(VGA_Dealership__r.VGA_Dealer_Code__c + Name ))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Unique Key 2</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_primary_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <encrypted>false</encrypted>
        <externalId>true</externalId>
        <label>VGA_primary_Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Dealership Profile</label>
    <nameField>
        <encrypted>false</encrypted>
        <label>Sub Brand</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Dealership Profiles</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Validate_Name</fullName>
        <active>true</active>
        <errorConditionFormula>AND(NOT(ISNEW()),
ISCHANGED(Name))</errorConditionFormula>
        <errorMessage>Sub Brand cant be changed manually.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_Rule_for_Round_Robin_method</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
ISPICKVAL(VGA_Distribution_Method__c , &apos;Round Robin&apos;), ISBLANK(PRIORVALUE(VGA_Nominated_Contact__c))  ,
NOT(ISBLANK(VGA_Nominated_Contact__c))
)</errorConditionFormula>
        <errorDisplayField>VGA_Nominated_Contact__c</errorDisplayField>
        <errorMessage>Nominated Contact should not have a value if the Distribution Method is not Nominated User</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validation_Rule_for_distribution_method</fullName>
        <active>true</active>
        <errorConditionFormula>AND(
ISPICKVAL(VGA_Distribution_Method__c , &apos;Nominated User&apos;) ,
ISBLANK(VGA_Nominated_Contact__c)
)</errorConditionFormula>
        <errorDisplayField>VGA_Nominated_Contact__c</errorDisplayField>
        <errorMessage>Please select Nominated Dealer Contact.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
