<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>VGA_Account_Id__c</fullName>
        <externalId>false</externalId>
        <formula>VGA_Contact__r.AccountId</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Id</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Active__c</fullName>
        <defaultValue>true</defaultValue>
        <externalId>false</externalId>
        <label>Active</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>VGA_Available_TestDrive__c</fullName>
        <defaultValue>false</defaultValue>
        <description>This contact will be counted as available for new test drive leads.</description>
        <externalId>false</externalId>
        <label>Available to receive test drive leads</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>VGA_Available__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Available to receive leads</label>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>VGA_Brand__c</fullName>
        <externalId>false</externalId>
        <label>Brand</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Volkswagen</fullName>
                    <default>false</default>
                    <label>Volkswagen</label>
                </value>
                <value>
                    <fullName>Skoda</fullName>
                    <default>false</default>
                    <label>Skoda</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VGA_Contact_Status__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(VGA_Contact__r.VGA_Partner_User_Status__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contact Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>VGA_Contact__c</fullName>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>User Profiles</relationshipLabel>
        <relationshipName>User_Profiles</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>VGA_Mobile__c</fullName>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Mobile</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>VGA_Role__c</fullName>
        <externalId>false</externalId>
        <label>Role</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Consultant</fullName>
                    <default>false</default>
                    <label>Consultant</label>
                </value>
                <value>
                    <fullName>Lead Controller</fullName>
                    <default>false</default>
                    <label>Lead Controller</label>
                </value>
                <value>
                    <fullName>Dealer Administrator</fullName>
                    <default>false</default>
                    <label>Dealer Administrator</label>
                </value>
                <value>
                    <fullName>Dealer Portal</fullName>
                    <default>false</default>
                    <label>Dealer Portal</label>
                </value>
                <value>
                    <fullName>Dealer Portal Group</fullName>
                    <default>false</default>
                    <label>Dealer Portal Group</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>VGA_Sub_Brand__c</fullName>
        <externalId>false</externalId>
        <label>Sub Brand</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Passenger Vehicles (PV)</fullName>
                    <default>false</default>
                    <label>Passenger Vehicles (PV)</label>
                </value>
                <value>
                    <fullName>Commercial Vehicles (CV)</fullName>
                    <default>false</default>
                    <label>Commercial Vehicles (CV)</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <label>User Profile</label>
    <nameField>
        <encrypted>false</encrypted>
        <label>Sub Brand</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>User Profiles</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>Validate_ActiveTestDrive</fullName>
        <active>true</active>
        <errorConditionFormula>AND(VGA_Contact__r.Account.VGA_Pilot_Dealer__c !=TRUE,VGA_Available_TestDrive__c = true)</errorConditionFormula>
        <errorMessage>The dealer should be a pilot user to be able to receive test drive leads</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validate_Name</fullName>
        <active>true</active>
        <errorConditionFormula>AND(NOT(ISNEW()), 
ISCHANGED(Name))</errorConditionFormula>
        <errorMessage>Sub Brand cant be changed manually.</errorMessage>
    </validationRules>
    <visibility>Public</visibility>
</CustomObject>
