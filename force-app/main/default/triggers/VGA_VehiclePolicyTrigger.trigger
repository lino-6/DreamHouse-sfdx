trigger VGA_VehiclePolicyTrigger on VGA_Vehicle_Policy__c (after insert, after update, before insert, before update, after delete) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Vehicle_Policy__c') && mapofActionController.get('VGA_Vehicle_Policy__c').VGA_Is_Active__c)
    {
        VGA_VehiclePolicyTriggerHandler obj = new VGA_VehiclePolicyTriggerHandler();
        obj.runTrigger(); 
    }
}