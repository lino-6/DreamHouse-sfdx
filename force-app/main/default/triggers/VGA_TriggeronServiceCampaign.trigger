trigger VGA_TriggeronServiceCampaign on VGA_Service_Campaigns__c (before insert,before update, after insert) {
    
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Service_Campaigns__c') && mapofActionController.get('VGA_Service_Campaigns__c').VGA_Is_Active__c)
    {
        VGA_Service_CampaignHandler obj = new VGA_Service_CampaignHandler();
        obj.runTrigger();
    }
    
}