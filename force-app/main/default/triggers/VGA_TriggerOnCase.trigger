trigger VGA_TriggerOnCase on Case (before insert, after insert, before update, after update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Case') && mapofActionController.get('Case').VGA_Is_Active__c)
    {
        VGA_CaseTriggerHandler obj = new VGA_CaseTriggerHandler();
        obj.runTrigger();
    }
}