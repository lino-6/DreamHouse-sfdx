trigger VGA_TriggerOnDocuSignStatus on dsfs__DocuSign_Status__c (before insert, after insert, before update, after update) {
 map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('DocuSignStatus') && mapofActionController.get('DocuSignStatus').VGA_Is_Active__c)
    {
        VGA_DocuSignStatusTriggerHandler obj = new VGA_DocuSignStatusTriggerHandler();
        obj.runTrigger();
    }
}