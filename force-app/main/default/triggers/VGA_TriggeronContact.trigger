trigger VGA_TriggeronContact on Contact (after insert, after update,before insert,before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Contact') && mapofActionController.get('Contact').VGA_Is_Active__c)
    {
    	VGA_ContactTriggerHandler obj = New VGA_ContactTriggerHandler();
    	obj.runTrigger();
    } 
}