trigger VGA_ManualAssignmentTrigger on VGA_Manual_Assignment_Configuration__c (after insert, after update, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    
    if(mapofActionController != null && mapofActionController.containskey('VGA_Manual_Assignment_Configuration__c') && mapofActionController.get('VGA_Manual_Assignment_Configuration__c').VGA_Is_Active__c)
    {
        VGA_ManualAssignmentTriggerHandler obj = new VGA_ManualAssignmentTriggerHandler();
        obj.runTrigger();
    } 
}