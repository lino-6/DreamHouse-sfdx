trigger VGA_TriggerOnNotification on VGA_Dealer_Notification__c (before insert, after insert, before update, after update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    
    if(mapofActionController != null && mapofActionController.containskey('VGA_Dealer_Notification__c') && mapofActionController.get('VGA_Dealer_Notification__c').VGA_Is_Active__c)
    {
        VGA_NotificationTriggerHandler objTriggerHandler = new VGA_NotificationTriggerHandler();
        
        if(trigger.isBefore && trigger.isInsert) 
        {
            objTriggerHandler.onbeforeInsert(trigger.new);
        }
        
        if(trigger.isafter && trigger.isInsert) 
        {
            objTriggerHandler.onafterInsert(trigger.new);
        }        
    }
}