trigger VGA_TriggeronContentDocumentLink on ContentDocumentLink (before insert) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('ContentDocumentLink') && mapofActionController.get('ContentDocumentLink').VGA_Is_Active__c)
    {
    VGA_ContentDocumentLinkTriggerhandler objClass = new VGA_ContentDocumentLinkTriggerhandler();
      objClass.runTrigger(); 
    }
}