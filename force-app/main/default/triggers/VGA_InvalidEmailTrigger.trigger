trigger VGA_InvalidEmailTrigger on VGA_Invalid_Email__c (before insert) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Invalid_Email__c') && mapofActionController.get('VGA_Invalid_Email__c').VGA_Is_Active__c)
    {
    	VGA_InvaildEmailHandler ObjInvalid =new VGA_InvaildEmailHandler();
    	ObjInvalid.runTrigger();
    }

}