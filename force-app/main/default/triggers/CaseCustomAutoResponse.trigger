trigger CaseCustomAutoResponse on EmailMessage (after insert) 
{
    System.debug('Email message trigger');
    @TestVisible
    private String getThreadId(String caseId){
       List<Messaging.RenderEmailTemplateBodyResult> renderResults =
       Messaging.renderEmailTemplate(null, caseId, new List<String>{'{!Case.Thread_Id}'});
       
       if(renderResults.size()!=0){
           System.debug('this is the Case Thread Id: ' + renderResults[0].getMergedBody());
           return renderResults[0].getMergedBody();
       }
        
       return null;       
     }    
    private String getThreadId1(String caseId){
     return '[ ref:_' 
             + UserInfo.getOrganizationId().left(5) 
             + UserInfo.getOrganizationId().mid(11,4) + '._' 
             + caseId.left(5)
             + caseId.mid(10,5) + ':ref ]';     
     } 
     private String getThreadId2(String caseId){
   
       return '[ ref:_' 
             + UserInfo.getOrganizationId().left(5) 
             + UserInfo.getOrganizationId().right(10).replace('0', '') + '._' 
             + caseId.left(5)
             + caseId.right(10).left(5).replace('0', '')
             + caseId.right(5) + ':ref ]';         
     } 
    try{  
        
        VGA_Triggers__c triggerStatus = VGA_Triggers__c.getInstance('CaseCustomAutoResponse');
        Boolean runTrigger = TRUE;
        
        if(triggerStatus != Null) {
            runTrigger = triggerStatus.VGA_Is_Active__c;
        }        
        
        if (runTrigger) {    
        
            for (EmailMessage e: Trigger.New) 
            {
                
                if(e.Incoming == false) {
                    continue;
                }
           
           	   //get from email address, only take first 38 characters.
               string fromEmailAddress = '';
               if (e.fromaddress != null){
           	   	fromEmailAddress = e.fromaddress.left(38);
               }
           
                List<Case> cl = [SELECT Id, CaseNumber, Subject, VGA_Confirmation_Email_Sent__c  FROM Case WHERE Id = :e.parentId];
                system.debug('clsize' + cl.size());    
                
                if(cl.size() <= 0) {
                    continue;
                }
                
                Case c = cl.get(0);
                
                //check if confirmation was already sent
                if(c.VGA_Confirmation_Email_Sent__c == true) {
                    continue;
                }
                else{
                    //set the flag to true
                    c.VGA_Confirmation_Email_Sent__c = true;
                    update c;
                }
                // EmailMessage em = li.get(0);
                
                 if (e.ToAddress==null || e.ToAddress.Length() < 2){               
                    continue;
                }
                List<string> lstToAddress = e.ToAddress.split(';');
                Email_To_Case__c et;
                
                if (lstToAddress.size() > 0) { 
                    for(string eml: lstToAddress){  
                        system.debug('Orignal To Address: ' + eml);  
                        et = Email_To_Case__c.getInstance(eml.trim().left(38));
                        if (et == null)
                        {
                            //try utf 8 encoding
                            string b  = EncodingUtil.urlEncode(eml.trim(), 'UTF-8');    
                            system.debug('UTF 8 - Converted To Address: ' +  b); 
                            et = Email_To_Case__c.getInstance(b.left(38)); 
                        } else {
                            break;
                        }                         
                    } 
                }

                if (et != null){
                    et.Send_Confirmation__c = et.Send_Confirmation__c == true ? VGA_EmailMessageTriggerHelper.isEmailExcluded(e) : et.Send_Confirmation__c;
                }
                    
                if (et != null && et.Send_Confirmation__c == true)
                {                 
                    Messaging.reserveSingleEmailCapacity(2);
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();     
                        
                    EmailTemplate template = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate WHERE DeveloperName = :et.Email_Template__c];
                    String threadId  = getThreadId(c.id);   
                    //String threadId1 = getThreadId1(c.id);   
                    //String threadId2 = getThreadId2(c.id);        
                    String subject   = template.Subject;
                                
                    subject = subject.replace('{!Case.CaseNumber}', c.CaseNumber);
                    if(c.Subject != Null) {
                        subject = subject.replace('{!Case.Subject}', c.Subject);
                    }
                    subject = subject.replace('{!Case.Thread_Id}', threadId);
                
                    String htmlBody = template.HtmlValue;
                              
                    htmlBody = htmlBody.replace('{!Case.Thread_Id}', threadId);  
                    htmlBody = htmlBody.replace('{!Case.CaseNumber}', c.CaseNumber);
                        
                    String plainBody = template.Body;
                    plainBody = plainBody.replace('{!Case.Thread_Id}', threadId);  
                    plainBody = plainBody.replace('{!Case.CaseNumber}', c.CaseNumber); 
                       
                    String[] toAddresses = new String[] {e.FromAddress}; 
                  
                    mail.setToAddresses(toAddresses);
                
                    mail.setReplyTo(et.Service_Email__c);
                    
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :e.ToAddress];
                    
                    if ( owea.size() > 0 ) {
                        mail.setOrgWideEmailAddressId(owea.get(0).Id);
                    }
                    else {    
                        mail.setSenderDisplayName(et.Display_Name__c);    
                    }
                    
                    mail.setSubject(subject);
                
                    mail.setBccSender(false);
                    
                    mail.setUseSignature(false);
                    
                    mail.setPlainTextBody(plainBody);
                            
                    mail.setHtmlBody(htmlBody);
                    
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
                        
                    EmailMessage emailMsg = new EmailMessage(); // Created a EmailMessage and copy all details from above.
                    emailMsg.ToAddress = (mail.getToAddresses())[0];
                    emailMsg.FromAddress = et.Service_Email__c;
                    emailMsg.Subject = mail.getSubject();
                    emailMsg.HtmlBody = mail.getHtmlBody();
                    emailMsg.TextBody = mail.getPlainTextBody();
                    emailMsg.ParentId = c.Id; //Attach with the case
                    emailMsg.MessageDate = system.now();
                    emailMsg.Status = '0'; 
                    insert emailMsg;
                }
            }
        }
    }
   
   catch(Exception e)
    {
        System.debug('---Exception Message:  ' + e.getMessage() + ' getLineNumber: ' + e.getLineNumber());
        VGA_Common.sendExceptionEmail('Exception in Case Auto Response Trigger: ', 'Hi, <br/><br/> Email Confirmation failed due to following error : <br/><br/>' + e.getMessage() +' at line number ' + e.getLineNumber() + '<br/><br/>Thanks<br/>Salesforce Support');
    }
}