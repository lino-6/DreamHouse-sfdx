trigger VGA_DealershipProfileTrigger on VGA_Dealership_Profile__c (after insert, after update, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Dealership_Profile__c') && mapofActionController.get('VGA_Dealership_Profile__c').VGA_Is_Active__c)
    {
		VGA_DealershipProfileTriggerHandler objCls = new VGA_DealershipProfileTriggerHandler();
		objCls.runTrigger();
    }
}