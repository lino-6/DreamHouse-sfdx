trigger VGA_PreLaunchTrigger on VGA_Pre_Launch_Configuration__c (after insert, after update, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Pre_Launch_Configuration__c') && mapofActionController.get('VGA_Pre_Launch_Configuration__c').VGA_Is_Active__c)
    {
    	VGA_PreLaunchConfigurationTriggerHandler obj = new VGA_PreLaunchConfigurationTriggerHandler();
    	obj.runTrigger();
    }
}