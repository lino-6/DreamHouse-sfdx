trigger VGA_TriggerOnLead on Lead (before insert,after insert, before update, after update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Lead') && mapofActionController.get('Lead').VGA_Is_Active__c)
    {
        VGA_LeadTriggerHandler objTriggerHandler = new VGA_LeadTriggerHandler();
        
        if(trigger.isBefore && trigger.isInsert) 
        {
            objTriggerHandler.onbeforeInsert(trigger.new);
        }
        
        if(trigger.isBefore && trigger.isUpdate) 
        {
            objTriggerHandler.onbeforeUpdate(trigger.New, Trigger.oldMap);
        }
        
        if(trigger.isafter && trigger.isInsert) 
        {
            objTriggerHandler.onafterInsert(trigger.new);
        }
        
        if(trigger.isafter && trigger.isUpdate) 
        {
            objTriggerHandler.onafterUpdate(trigger.new, Trigger.oldMap);
        }
    }
}