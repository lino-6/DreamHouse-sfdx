trigger VGA_TriggeronAsset on Asset (before insert,after update) 
{
 
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Asset') && mapofActionController.get('Asset').VGA_Is_Active__c)
    {
      VGA_VehicleTriggerHandler ObjVehicle =new VGA_VehicleTriggerHandler();
      ObjVehicle.runTrigger();
    }
}