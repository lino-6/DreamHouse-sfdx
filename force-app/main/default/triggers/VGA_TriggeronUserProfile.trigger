trigger VGA_TriggeronUserProfile on VGA_User_Profile__c (Before Update,Before insert ) {
    
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_User_Profile__c') && mapofActionController.get('VGA_User_Profile__c').VGA_Is_Active__c)
    {
        VGA_UserProfileHandler obj = new VGA_UserProfileHandler();
        obj.runTrigger();
    }
    
}