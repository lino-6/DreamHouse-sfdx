trigger VGA_CustomerOrderTrigger on VGA_Customer_Order_Details__c (before insert,before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Customer_Order_Details__c') && mapofActionController.get('VGA_Customer_Order_Details__c').VGA_Is_Active__c)
    {
    	VGA_CustomerOrderHandler  objCustomer =New VGA_CustomerOrderHandler ();
    	objCustomer.runTrigger();
    }
}