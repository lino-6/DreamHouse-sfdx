trigger DisableCaseCreationFromAutorespnderEmail on Case (before insert) {

    VGA_Triggers__c triggerStatus = VGA_Triggers__c.getInstance('DisableCaseCreation');
    Boolean runTrigger = TRUE;
    
    if(triggerStatus != Null) {
        runTrigger = triggerStatus.VGA_Is_Active__c;
    }

    if (runTrigger) {
        for (Case c: Trigger.New) {     
            
            if (c.Subject != Null && c.Origin != Null && c.SuppliedEmail != Null && c.Subject.tolowercase().contains('your message has been received'.tolowercase()) && c.Origin == 'Email' && c.SuppliedEmail.tolowercase().contains('@ncat.nsw.gov.au'.tolowercase())) {
                // rule 001
                c.addError('[Case Error Code: 001] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Automatic Reply'.tolowercase())) {
                // rule 002
                c.addError('[Case Error Code: 002] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Delivery Status Notification (Failure)'.tolowercase())) {
                // rule 003
                c.addError('[Case Error Code: 003] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Mail System Error'.tolowercase())) {
                // rule 004
                c.addError('[Case Error Code: 004] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Undeliverable:'.tolowercase())) {
                // rule 005
                c.addError('[Case Error Code: 005] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Undelivered Mail'.tolowercase())) {
                // rule 006
                c.addError('[Case Error Code: 006] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.Subject.tolowercase().contains('failure notice'.tolowercase())) {
                // rule 007
                c.addError('[Case Error Code: 007] Rejecting autoresponder email');
            }       
            else if (c.Subject != Null && c.Subject.tolowercase().contains('out of office'.tolowercase())) {
                // rule 008
                c.addError('[Case Error Code: 008] Rejecting autoresponder email');
            } 
            else if (c.Origin != Null && c.SuppliedEmail != Null && c.Origin == 'Academy Email' && c.SuppliedEmail.tolowercase().contains('academy@volkswagen.com.au'.tolowercase())) {
                // rule 009
                c.addError('[Case Error Code: 009] Rejecting autoresponder email');
            }
            else if (c.Subject != Null && c.SuppliedEmail != Null && c.Subject.tolowercase().startsWith('Itinerary:'.tolowercase()) && (c.SuppliedEmail.tolowercase().contains('vgagroups@travelctm.com'.tolowercase()) || c.SuppliedEmail.tolowercase().contains('alfahad.p@katzion.com'.tolowercase()) ) ) {
    
                String queueName = 'Academy Itinerary';
                List<QueueSobject> queue = [SELECT id, queueid FROM queuesobject WHERE queue.name = :queueName];
                if(queue.size() > 0) {
                    c.OwnerId = queue[0] .queueid; 
                    c.AccountId = Academy_Itinerary__c.getInstance('Id').Account_Id__c;
                    c.Status = 'Closed';  
                    System.debug('Case id: ' + c.Id+ ' Account id: ' + c.AccountId + ', Supplied email: ' + c.SuppliedEmail + ', Owner id: '+ c.OwnerId);            
                }
                    
            }
    
            else if (c.Origin != Null && c.SuppliedEmail != Null && c.Origin == 'Academy Email' && c.SuppliedEmail.tolowercase().contains('noreply@salesforce.com'.tolowercase())) {
                // rule 010
                c.addError('[Case Error Code: 010] Rejecting autoresponder email');
            }   
            else if (c.Subject != Null && c.Subject.tolowercase().contains('Auto Reply'.tolowercase())) {
                // rule 002
                c.addError('[Case Error Code: 011] Rejecting autoresponder email');
            }           
    
                                              
        }
    }
    

}