trigger VGA_TaskTrigger on Task (before insert,after insert,before update) 
{

    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Task') && mapofActionController.get('Task').VGA_Is_Active__c)
    {
       VGA_TaskHandler  ObjTask =new VGA_TaskHandler ();
       ObjTask.runTrigger();
    }



}