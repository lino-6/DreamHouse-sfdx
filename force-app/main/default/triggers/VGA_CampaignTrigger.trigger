trigger VGA_CampaignTrigger on Campaign (after insert, after update, before insert, before update) 
{
	map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Campaign') && mapofActionController.get('Campaign').VGA_Is_Active__c)
    {
		VGA_CampaignTriggerHandler objCampaign = new VGA_CampaignTriggerHandler();
		objCampaign.runTrigger();
    }    
}