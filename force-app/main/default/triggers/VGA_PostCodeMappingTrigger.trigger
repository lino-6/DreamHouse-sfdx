trigger VGA_PostCodeMappingTrigger on VGA_Post_Code_Mapping__c (after insert, after update, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Post_Code_Mapping__c') && mapofActionController.get('VGA_Post_Code_Mapping__c').VGA_Is_Active__c)
    {
    	VGA_PostCodeMappingTriggerHandler obj = new VGA_PostCodeMappingTriggerHandler();
    	obj.runTrigger();
    }
}