trigger VGA_Service_Campaign_Notificationtrigger on VGA_Service_Campaign_Notification__c (after insert,after update) {
    
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Service_Campaign_Notification__c') && mapofActionController.get('VGA_Service_Campaign_Notification__c').VGA_Is_Active__c)
    {
        VGA_Service_Campaign_NotificationHandler obj = new VGA_Service_Campaign_NotificationHandler();
        obj.runTrigger();
    }
}