trigger DisableCaseFollowupFromAutorespnderEmail on EmailMessage (before insert) {
    
    VGA_Triggers__c triggerStatus = VGA_Triggers__c.getInstance('DisableCaseFollowup');
    Boolean runTrigger = TRUE;
    
    if(triggerStatus != Null) {
        runTrigger = triggerStatus.VGA_Is_Active__c;
    }

    if (runTrigger) {    
    
        for (EmailMessage e: Trigger.New) {
            if(e.ParentId != Null && e.Incoming == TRUE && e.FromAddress.tolowercase().contains('academy@volkswagen.com.au'.tolowercase())) {
                e.addError('[EmailMessage Error Code: 001] Rejecting autoresponder email');
                System.debug('[EmailMessage Error Code: 001] Rejecting autoresponder email');           
            }
            else if(e.ParentId != Null && e.Incoming == TRUE && e.FromAddress.tolowercase().contains('noreply@salesforce.com'.tolowercase())) {
                e.addError('[EmailMessage Error Code: 002] Rejecting autoresponder email');
                System.debug('[EmailMessage Error Code: 002] Rejecting autoresponder email');           
            }        
        }
    }
}