trigger VGA_AttachmentTrigger on Attachment (after delete, after insert, after update, before delete, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Attachment') && mapofActionController.get('Attachment').VGA_Is_Active__c)
    {
        VGA_AttachmentTriggerHandler objCls = new VGA_AttachmentTriggerHandler();
        objCls.runTrigger();    
    }
}