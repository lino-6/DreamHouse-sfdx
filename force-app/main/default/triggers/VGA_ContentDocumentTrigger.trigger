trigger VGA_ContentDocumentTrigger on ContentDocument (after delete, after insert, after update, before delete, before insert, before update) 
{
    
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('ContentDocument') && mapofActionController.get('ContentDocument').VGA_Is_Active__c)
    {
        VGA_ContentDocumentTriggerHandler obj = new VGA_ContentDocumentTriggerHandler();
        obj.runTrigger(); 
    }
    
}