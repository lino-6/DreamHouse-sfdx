trigger VGA_TriggerOnAccount on Account (before insert, after insert, before update, after update,before delete) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Account') && mapofActionController.get('Account').VGA_Is_Active__c)
    {
        VGA_AccountTriggerHandler obj = new VGA_AccountTriggerHandler();
        obj.runTrigger();
    }
}