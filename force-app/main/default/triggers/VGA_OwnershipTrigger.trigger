trigger VGA_OwnershipTrigger on VGA_Ownership__c (after insert, after update, before insert, before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Ownership__c') && mapofActionController.get('VGA_Ownership__c').VGA_Is_Active__c)
    {
    	VGA_OwnershipTriggerhandler obj = new VGA_OwnershipTriggerhandler();
    	obj.runTrigger();
    }
}