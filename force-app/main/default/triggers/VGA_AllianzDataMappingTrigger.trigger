trigger VGA_AllianzDataMappingTrigger on VGA_Allianz_Data_Mapping__c (before insert,before update) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('VGA_Allianz_Data_Mapping__c') && mapofActionController.get('VGA_Allianz_Data_Mapping__c').VGA_Is_Active__c)
    {
        VGA_AllianzDataMappingTriggerHandler obj = new VGA_AllianzDataMappingTriggerHandler();
        obj.runTrigger();
    }
}