trigger VGA_TriggerOnServiceInterval on VGA_Service_Interval__c (after insert,after update) {
    Map<String,VGA_Triggers__c> MapofActionController = VGA_Triggers__c.getall();
    if(MapofActionController != null && MapofActionController.containskey('VGA_Service_Interval__c') && MapofActionController.get('VGA_Service_Interval__c').VGA_Is_Active__c){
        if(Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)){
            VGA_ServiceIntervalTriggerHandler.ServiceOrderCheck(Trigger.new,Trigger.newMap);
        }
    }
}