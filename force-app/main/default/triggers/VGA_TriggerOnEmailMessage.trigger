trigger VGA_TriggerOnEmailMessage on EmailMessage (before insert, after insert, before update, after update,before delete) 
{
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('EmailMessage') && mapofActionController.get('EmailMessage').VGA_Is_Active__c)
    {
    	VGA_EmailMessageTriggerHandler obj = new VGA_EmailMessageTriggerHandler();
    	obj.runTrigger();
    }
}