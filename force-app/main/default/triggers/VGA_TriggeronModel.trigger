trigger VGA_TriggeronModel on Product2 (before insert,before update) 
{ 
    map<string,VGA_Triggers__c> mapofActionController = VGA_Triggers__c.getall();
    if(mapofActionController != null && mapofActionController.containskey('Product2') && mapofActionController.get('Product2').VGA_Is_Active__c)
    {
        VGA_ModelHandler obj = new VGA_ModelHandler();
        obj.runTrigger();
    }
}