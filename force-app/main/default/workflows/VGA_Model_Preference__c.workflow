<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Brand_Field</fullName>
        <field>Brand__c</field>
        <formula>TEXT(VGA_Brochure_Model__r.VGA_Brand__c)</formula>
        <name>Populate Brand Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Sub_brand_field</fullName>
        <field>Sub_brand__c</field>
        <formula>IF(INCLUDES( VGA_Brochure_Model__r.VGA_Sub_Brand__c, &quot;Passenger Vehicles (PV)&quot;), &quot;PV&quot;,IF(INCLUDES( VGA_Brochure_Model__r.VGA_Sub_Brand__c, &quot;Commercial Vehicles (CV)&quot;), &quot;CV&quot;,NULL))</formula>
        <name>Populate Sub-brand field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Brand and Sub-brand</fullName>
        <actions>
            <name>Populate_Brand_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Sub_brand_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VGA_Model_Preference__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Based on the brand and sub-brand in active brochures</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
