<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Volkswagen_Dealer_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Volkswagen_Dealer_Contact</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Volkswagen Dealer Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Contact_Mailing_Country</fullName>
        <field>MailingCountry</field>
        <formula>&quot;AU&quot;</formula>
        <name>update Contact Mailing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>update Contact Country</fullName>
        <actions>
            <name>VGA_update_Contact_Mailing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.MailingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
