<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Region_Code</fullName>
        <field>VGA_Customer_Region_Code_f__c</field>
        <formula>BillingState</formula>
        <name>Populate Region Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_City</fullName>
        <field>ShippingCity</field>
        <formula>BillingCity</formula>
        <name>Update Shipping City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Country</fullName>
        <field>ShippingCountry</field>
        <formula>BillingCountry</formula>
        <name>Update Shipping Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_State_Province</fullName>
        <field>ShippingState</field>
        <formula>BillingState</formula>
        <name>Update Shipping State/Province</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Street</fullName>
        <field>ShippingStreet</field>
        <formula>BillingStreet</formula>
        <name>Update Shipping Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Shipping_Zip_Postal_Code</fullName>
        <field>ShippingPostalCode</field>
        <formula>BillingPostalCode</formula>
        <name>Update Shipping Zip/Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Billing_Country</fullName>
        <field>BillingCountry</field>
        <formula>&quot;AU&quot;</formula>
        <name>update Billing Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Email_Address_Valid_Checkbox</fullName>
        <field>VGA_Email_Address_Valid__c</field>
        <literalValue>0</literalValue>
        <name>update Email Address Valid Checkbox 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Is_Address_Changed</fullName>
        <field>VGA_Is_Address_Updated__c</field>
        <literalValue>1</literalValue>
        <name>update Is Address Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Email_Address_Valid_Checkbox</fullName>
        <field>VGA_Email_Address_Valid__c</field>
        <literalValue>1</literalValue>
        <name>update Email Address Valid Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Physical Address on Account</fullName>
        <actions>
            <name>Update_Shipping_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_State_Province</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Street</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Shipping_Zip_Postal_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.VGA_Physical_is_same_as_Postal_Address__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Region code field</fullName>
        <actions>
            <name>Populate_Region_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update Billing Country</fullName>
        <actions>
            <name>VGA_update_Billing_Country</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.BillingCountry</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>update Email Address Valid Checkbox</fullName>
        <actions>
            <name>update_Email_Address_Valid_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.VGA_Email_Validation__c</field>
            <operation>equals</operation>
            <value>Valid,Trusted Source,Bypassed Validation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update Email Validation Checkbox to False</fullName>
        <actions>
            <name>VGA_update_Email_Address_Valid_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.VGA_Email_Validation__c</field>
            <operation>notEqual</operation>
            <value>Valid,Trusted Source,Bypassed Validation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>updated Is Address Changed</fullName>
        <actions>
            <name>VGA_update_Is_Address_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( !ISNEW(), OR (  ISCHANGED( BillingAddress )  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
