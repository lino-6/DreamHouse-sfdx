<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Email_to_Dealer_Contact</fullName>
        <description>Send Email to Dealer Contact</description>
        <protected>false</protected>
        <recipients>
            <field>VGA_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>crm@volkswagen.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/VGA_Task_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_to_Dealer_Contact_Sko</fullName>
        <description>Send Email to Dealer Contact Skoda</description>
        <protected>false</protected>
        <recipients>
            <field>VGA_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>crm@volkswagen.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skoda_Email_Templates/VGA_Task_Assigned_Skoda_MeetingAssist</template>
    </alerts>
    <alerts>
        <fullName>send_task_email</fullName>
        <description>send task email</description>
        <protected>false</protected>
        <recipients>
            <field>VGA_Contact_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>crm@volkswagen.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/VGA_Task_Assigned_Contact</template>
    </alerts>
    <fieldUpdates>
        <fullName>Task_completion_date</fullName>
        <field>VGA_Task_Completed_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Task completion date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email To task Owner SKODA</fullName>
        <actions>
            <name>Send_Email_to_Dealer_Contact_Sko</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( VGA_Email_Notification__c = True, VGA_Task_Record_Type__c= &apos;New_Task_ASM_Skoda&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email To task Owner</fullName>
        <actions>
            <name>Send_Email_to_Dealer_Contact</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( VGA_Email_Notification__c = True, VGA_Task_Record_Type__c= &apos;New_Task_ASM_VW&apos; )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Email To task Owner %28Contact%29</fullName>
        <actions>
            <name>send_task_email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>VGA_Email_Notification__c = true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Email To task Owner sKODA</fullName>
        <active>false</active>
        <formula>AND ( VGA_Email_Notification__c = True, VGA_Task_Record_Type__c= &apos;New_Task_ASM_Skoda&apos;   )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Task completion date</fullName>
        <actions>
            <name>Task_completion_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.Status</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
