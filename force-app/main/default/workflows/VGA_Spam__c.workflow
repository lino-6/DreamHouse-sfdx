<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Key</fullName>
        <field>VGA_Key__c</field>
        <formula>TRIM(LOWER(TRIM(VGA_First_Name__c) &amp; TRIM(VGA_Last_Name__c) &amp; TRIM(VGA_Email__c) &amp; TRIM(VGA_Mobile__c)))</formula>
        <name>Update Key</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_Update_Key_1</fullName>
        <field>VGA_Key_1__c</field>
        <formula>TRIM(LOWER(TRIM(VGA_First_Name__c) &amp; TRIM(VGA_Last_Name__c) &amp; TRIM(VGA_Email__c)))</formula>
        <name>Update Key 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_Update_Key_2</fullName>
        <field>VGA_Key_2__c</field>
        <formula>TRIM(LOWER(TRIM(VGA_First_Name__c) &amp; TRIM(VGA_Last_Name__c) &amp; TRIM(VGA_Mobile__c)))</formula>
        <name>Update Key 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Unique Key</fullName>
        <actions>
            <name>Update_Key</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>VGA_Update_Key_1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>VGA_Update_Key_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>VGA_Spam__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
