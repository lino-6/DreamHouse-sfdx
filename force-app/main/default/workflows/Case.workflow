<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_send_to_VGA_Dealer</fullName>
        <description>Email send to VGA Dealer</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>dealersupport@myvw.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/VGA_Interaction_Updated</template>
    </alerts>
    <alerts>
        <fullName>Email_send_to_skoda_Dealer</fullName>
        <description>Email send to skoda Dealer</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>dealersupport@myskoda.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skoda_Email_Templates/VGA_Skoda_Interaction_Updated</template>
    </alerts>
    <alerts>
        <fullName>Skoda_Web_Auto_Response</fullName>
        <description>Skoda Web Auto Response</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@myskoda.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Customer_Confirmation_Skoda</template>
    </alerts>
    <alerts>
        <fullName>VGA_New_FastTrack_Loan_Car_Case_Notification</fullName>
        <description>New FastTrack Loan Car Case Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>loanvehicle@myvw.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Fast_Track_Loan_Car_Confirmation_VW</template>
    </alerts>
    <alerts>
        <fullName>VGA_SK_Enquiry_Notification</fullName>
        <description>SK Enquiry Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>dealersupport@myskoda.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Internal_Confirmation_Skoda</template>
    </alerts>
    <alerts>
        <fullName>VGA_SK_Goodwill_Notification</fullName>
        <description>SK Goodwill Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>goodwill@myskoda.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Internal_Confirmation_Skoda</template>
    </alerts>
    <alerts>
        <fullName>VGA_VW_Enquiry_Notification</fullName>
        <description>VW Enquiry Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>dealersupport@myvw.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Internal_Confirmation_VW</template>
    </alerts>
    <alerts>
        <fullName>VGA_VW_Goodwill_Notification</fullName>
        <description>VW Goodwill Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>goodwill@myvw.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Internal_Confirmation_VW</template>
    </alerts>
    <alerts>
        <fullName>Vw_Auto_Response_for_Web</fullName>
        <description>Vw Auto Response for Web</description>
        <protected>false</protected>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>hello@myvw.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Customer_Confirmation/Customer_Confirmation_VW</template>
    </alerts>
    <fieldUpdates>
        <fullName>Generate_Queue_name</fullName>
        <field>VGA_Omni_Queue_Name__c</field>
        <formula>Owner:Queue.QueueName</formula>
        <name>Generate Queue name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Sent</fullName>
        <field>VGA_Survey_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update Survey Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Sent_Date_time</fullName>
        <field>VGA_Survey_Email_Sent_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Survey Sent Date time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Case_Closed_Date</fullName>
        <field>VGA_Closed_Date__c</field>
        <formula>TODAY()</formula>
        <name>update Case Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Omni_Channel_Status</fullName>
        <field>VGA_Omni_Channel_Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>update Omni Channel Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Survey</fullName>
        <apiVersion>41.0</apiVersion>
        <description>Once Case is marked as closed.</description>
        <endpointUrl>https://test</endpointUrl>
        <fields>AccountId</fields>
        <fields>CaseNumber</fields>
        <fields>ContactEmail</fields>
        <fields>ContactMobile</fields>
        <fields>CreatedDate</fields>
        <fields>Dealer_Name__c</fields>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>Origin</fields>
        <fields>ParentId</fields>
        <fields>RecordTypeId</fields>
        <fields>Status</fields>
        <fields>VGA_Associated_Dealer_Code__c</fields>
        <fields>VGA_Brand__c</fields>
        <fields>VGA_Case_Owner__c</fields>
        <fields>VGA_Case_Record_Type__c</fields>
        <fields>VGA_Category_1__c</fields>
        <fields>VGA_Category_2__c</fields>
        <fields>VGA_Category_3__c</fields>
        <fields>VGA_Category_4__c</fields>
        <fields>VGA_Closed_Date__c</fields>
        <fields>VGA_Customer_Type__c</fields>
        <fields>VGA_Date_Time_Opened__c</fields>
        <fields>VGA_Description_of_Enquiry__c</fields>
        <fields>VGA_Model__c</fields>
        <fields>VGA_Registration_No__c</fields>
        <fields>VGA_VIN__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>augustine.alexander1@katzion.com</integrationUser>
        <name>Send Survey</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Send_Survey_Skoda1</fullName>
        <apiVersion>43.0</apiVersion>
        <description>Once Case is marked as closed.</description>
        <endpointUrl>https://test</endpointUrl>
        <fields>AccountId</fields>
        <fields>CaseNumber</fields>
        <fields>ContactEmail</fields>
        <fields>ContactMobile</fields>
        <fields>CreatedDate</fields>
        <fields>Dealer_Name__c</fields>
        <fields>Description</fields>
        <fields>Id</fields>
        <fields>Origin</fields>
        <fields>ParentId</fields>
        <fields>RecordTypeId</fields>
        <fields>Status</fields>
        <fields>VGA_Associated_Dealer_Code__c</fields>
        <fields>VGA_Brand__c</fields>
        <fields>VGA_Case_Owner__c</fields>
        <fields>VGA_Case_Record_Type__c</fields>
        <fields>VGA_Category_1__c</fields>
        <fields>VGA_Category_2__c</fields>
        <fields>VGA_Category_3__c</fields>
        <fields>VGA_Category_4__c</fields>
        <fields>VGA_Closed_Date__c</fields>
        <fields>VGA_Customer_Type__c</fields>
        <fields>VGA_Date_Time_Opened__c</fields>
        <fields>VGA_Description_of_Enquiry__c</fields>
        <fields>VGA_Model__c</fields>
        <fields>VGA_Qualtric_s_Email__c</fields>
        <fields>VGA_Registration_No__c</fields>
        <fields>VGA_VIN__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>augustine.alexander1@katzion.com</integrationUser>
        <name>Send Survey Skoda</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Email Send to Dealer for Skoda</fullName>
        <actions>
            <name>Email_send_to_skoda_Dealer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( 	AND 	( 		!ISBLANK(VGA_Dealer_Status_Update__c ) , 		ISCHANGED( VGA_Dealer_Status_Update__c ) 	), 	VGA_Skoda_Portal__c = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email Send to Dealer for VGA</fullName>
        <actions>
            <name>Email_send_to_VGA_Dealer</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND ( 	AND 	( 		!ISBLANK(VGA_Dealer_Status_Update__c), 		ISCHANGED( VGA_Dealer_Status_Update__c) 	) 	, 	VGA_Volkswagen_Portal__c  = true )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SK Enquiry Case Notification</fullName>
        <actions>
            <name>VGA_SK_Enquiry_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VGA_Created_via_Portal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.VGA_Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Dealer CRM Skoda</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SK Goodwill Case Notification</fullName>
        <actions>
            <name>VGA_SK_Goodwill_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VGA_Created_via_Portal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.VGA_Enquiry_Type__c</field>
            <operation>equals</operation>
            <value>Goodwill</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Dealer CRM Skoda</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Survey</fullName>
        <actions>
            <name>Update_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Survey_Sent_Date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Survey</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <formula>OR ( 	AND 	( 		ISPICKVAL(Status,&apos;Closed&apos;), 		OR 		( 			RecordType.Name=&apos;Enquiry&apos;, 			RecordType.Name=&apos;Complaint&apos;, 			RecordType.Name=&apos;Compliment&apos; 		), 		!ISPICKVAL(Origin,&apos;VGA Employee&apos;), 		!ISPICKVAL(Origin,&apos;SAGA&apos;), 		!ISPICKVAL(Origin,&apos;Dealer CRM VW&apos;), 		!ISPICKVAL(Origin,&apos;Dealer CRM Skoda&apos;), 		!ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;), 		!ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;), 		OR 		( 			ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;) 		), 		VGA_Survey_Email_Sent__c = false, 		ISBLANK(VGA_Parent_Case_Number__c), 		OR 		( 			AND 			( 				ISPICKVAL(Origin,&apos;VW Email&apos;), 				OR 				( 					VGA_To_Email__c = &apos;hello@success.volkswagen.net.au&apos;, 					VGA_To_Email__c = &apos;fleet@success.volkswagen.net.au&apos;, 					VGA_To_Email__c = &apos;volkswagenassist@volkswagen.com.au&apos;, 					VGA_To_Email__c = &apos;vwassist@volkswagen.com.au&apos;, 					VGA_To_Email__c = &apos;hello@success.skoda.net.au&apos;, 					VGA_To_Email__c = &apos;fleet@succes.skoda.net.au&apos;, 					VGA_To_Email__c = &apos;Info@Skoda.com.au&apos;  				) 				 			), 			AND 			( 				ISPICKVAL(Origin,&apos;Skoda Email&apos;), 				OR 				( 					VGA_To_Email__c = &apos;hello@success.volkswagen.net.au&apos;, 					VGA_To_Email__c = &apos;fleet@success.volkswagen.net.au&apos;, 					VGA_To_Email__c = &apos;volkswagenassist@volkswagen.com.au&apos;, 					VGA_To_Email__c = &apos;vwassist@volkswagen.com.au&apos;, 					VGA_To_Email__c = &apos;hello@success.skoda.net.au&apos;, 					VGA_To_Email__c = &apos;fleet@succes.skoda.net.au&apos;, 					VGA_To_Email__c = &apos;Info@Skoda.com.au&apos;  				) 			) 		) 	), 	AND 	( 		ISPICKVAL(Status,&apos;Closed&apos;), 		OR 		( 			RecordType.Name=&apos;Enquiry&apos;, 			RecordType.Name=&apos;Complaint&apos;, 			RecordType.Name=&apos;Compliment&apos; 		), 		!ISPICKVAL(Origin,&apos;VGA Employee&apos;), 		!ISPICKVAL(Origin,&apos;SAGA&apos;), 		!ISPICKVAL(Origin,&apos;Dealer CRM VW&apos;), 		!ISPICKVAL(Origin,&apos;Dealer CRM Skoda&apos;), 		!ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;), 		!ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;), 		!ISPICKVAL(Origin,&apos;VW Email&apos;), 		!ISPICKVAL(Origin,&apos;Skoda Email&apos;), 		OR 		( 			ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;), 			ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;) 		), 		VGA_Survey_Email_Sent__c = false, 		ISBLANK(VGA_Parent_Case_Number__c), 		!ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;), 		!ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;) 	) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Survey-Skoda</fullName>
        <actions>
            <name>Update_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Survey_Sent_Date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Survey_Skoda1</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>OR  (   AND   ( ISPICKVAL(VGA_Brand__c,&apos;Skoda&apos;),   ISPICKVAL(Status,&apos;Closed&apos;),   OR  (  RecordType.Name=&apos;Enquiry&apos;,   RecordType.Name=&apos;Complaint&apos;,   RecordType.Name=&apos;Compliment&apos;  ),   !ISPICKVAL(Origin,&apos;VGA Employee&apos;),   !ISPICKVAL(Origin,&apos;SAGA&apos;),    !ISPICKVAL(Origin,&apos;Dealer CRM Skoda&apos;),    !ISPICKVAL(Origin,&apos;Genius&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Social&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Spam / No Further Action&apos;),  OR  (  ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;),   ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;)  ),    VGA_Survey_Email_Sent__c = false,    ISBLANK(VGA_Parent_Case_Number__c),   OR  (AND  (  ISPICKVAL(Origin,&apos;Skoda Email&apos;),    OR  (OR(VGA_To_Email__c = &apos;hello@success.skoda.net.au&apos;,VGA_To_Email__c = &apos;hello@myskoda.com.au&apos;), OR(VGA_To_Email__c = &apos;fleet@succes.skoda.net.au&apos; ,VGA_To_Email__c = &apos;fleet@myskoda.com.au&apos;) )  )  )  ),   AND  (  ISPICKVAL(Status,&apos;Closed&apos;),  ISPICKVAL(VGA_Brand__c,&apos;Skoda&apos;),  OR  (  RecordType.Name=&apos;Enquiry&apos;,   RecordType.Name=&apos;Complaint&apos;,   RecordType.Name=&apos;Compliment&apos;  ),   !ISPICKVAL(Origin,&apos;VGA Employee&apos;),  !ISPICKVAL(Origin,&apos;SAGA&apos;),     !ISPICKVAL(Origin,&apos;Dealer CRM Skoda&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;),   !ISPICKVAL(Origin,&apos;Genius&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Social&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Spam / No Further Action&apos;),  !ISPICKVAL(Origin,&apos;Skoda Email&apos;),    OR  (  ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;)  ),   VGA_Survey_Email_Sent__c = false,  ISBLANK(VGA_Parent_Case_Number__c),   !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;)  )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Case Survey-VW</fullName>
        <actions>
            <name>Update_Survey_Sent</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Survey_Sent_Date_time</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Send_Survey</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <formula>OR  (   AND   (   OR(ISPICKVAL(VGA_Brand__c,&apos;PV Volkswagen&apos;),  ISPICKVAL(VGA_Brand__c,&apos;CV Volkswagen&apos;)),  ISPICKVAL(Status,&apos;Closed&apos;),  OR  (  RecordType.Name=&apos;Enquiry&apos;,   RecordType.Name=&apos;Complaint&apos;,   RecordType.Name=&apos;Compliment&apos; ),   !ISPICKVAL(Origin,&apos;VGA Employee&apos;),   !ISPICKVAL(Origin,&apos;SAGA&apos;),    !ISPICKVAL(Origin,&apos;Dealer CRM VW&apos;),    !ISPICKVAL(Origin,&apos;Genius&apos;),    !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Social&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Spam / No Further Action&apos;),   OR  (  ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;),   ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;),   ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;),   ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;)  ),   VGA_Survey_Email_Sent__c = false,    ISBLANK(VGA_Parent_Case_Number__c),    OR  (  AND  (  ISPICKVAL(Origin,&apos;VW Email&apos;),   OR  (OR(VGA_To_Email__c = &apos;hello@success.volkswagen.net.au&apos;,VGA_To_Email__c = &apos;hello@myvw.com.au&apos;), OR(VGA_To_Email__c = &apos;fleet@success.volkswagen.net.au&apos; ,VGA_To_Email__c = &apos;fleet@myvw.com.au&apos;) )   ) )  ),    AND     (  ISPICKVAL(Status,&apos;Closed&apos;),   OR(ISPICKVAL(VGA_Brand__c,&apos;PV Volkswagen&apos;),   ISPICKVAL(VGA_Brand__c,&apos;CV Volkswagen&apos;)),   OR  (  RecordType.Name=&apos;Enquiry&apos;,  RecordType.Name=&apos;Complaint&apos;,  RecordType.Name=&apos;Compliment&apos;  ),    !ISPICKVAL(Origin,&apos;VGA Employee&apos;),  !ISPICKVAL(Origin,&apos;SAGA&apos;),  !ISPICKVAL(Origin,&apos;Dealer CRM VW&apos;),    !ISPICKVAL(Origin,&apos;Genius&apos;), !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),  !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;),    !ISPICKVAL(VGA_Category_1__c,&apos;Social&apos;), !ISPICKVAL(VGA_Category_1__c,&apos;Spam / No Further Action&apos;),   !ISPICKVAL(Origin,&apos;VW Email&apos;), OR  (  ISPICKVAL(VGA_Customer_Type__c,&apos;Owner&apos;),    ISPICKVAL(VGA_Customer_Type__c,&apos;Prospect&apos;),  ISPICKVAL(VGA_Customer_Type__c,&apos;Fleet&apos;),     ISPICKVAL(VGA_Customer_Type__c,&apos;Driver&apos;)  ),  VGA_Survey_Email_Sent__c = false,    ISBLANK(VGA_Parent_Case_Number__c),  !ISPICKVAL(VGA_Category_1__c,&apos;Fair Trading&apos;),   !ISPICKVAL(VGA_Category_1__c,&apos;Tribunal&apos;) )  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Queue Name</fullName>
        <actions>
            <name>Generate_Queue_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( OR(ISNEW(), ISCHANGED(OwnerId)), LEFT(OwnerId, 3) != &apos;005&apos;   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>VW Enquiry Case Notification</fullName>
        <actions>
            <name>VGA_VW_Enquiry_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VGA_Created_via_Portal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Enquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Dealer CRM VW</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>VW FastTrack Loan Car Email Notification</fullName>
        <actions>
            <name>VGA_New_FastTrack_Loan_Car_Case_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VGA_Created_via_Portal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Dealer CRM VW</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>FastTrack Loan Car</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>VW Goodwill Case Notification</fullName>
        <actions>
            <name>VGA_VW_Goodwill_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.VGA_Created_via_Portal__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Goodwill</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Dealer CRM VW</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>update Case Closed Date</fullName>
        <actions>
            <name>VGA_update_Case_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
