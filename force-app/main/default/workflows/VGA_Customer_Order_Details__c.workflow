<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_External_order_status</fullName>
        <field>VGA_External_Status__c</field>
        <formula>IF(ISBLANK(VGA_Customer__c), 
&quot;&quot;, 
IF(VGA_Order_Status__c == &quot;A&quot;, 
&quot;A&quot;, 
IF(VGA_Order_Status__c == &quot;1&quot;, 
IF(ISBLANK(Previous_order_status__c), 
&quot;DB&quot;, 
&quot;DA&quot;), 
IF(VGA_Order_Status__c == &quot;5&quot;, 
IF(VGA_Notify_Customer__c, 
IF(ISBLANK(Previous_order_status__c), 
&quot;5&quot;, 
&quot;5&quot;), 
IF(ISBLANK(Previous_order_status__c), 
&quot;DB&quot;, 
&quot;DA&quot;)), 
IF(ISBLANK(Previous_order_status__c), 
VGA_Order_Status__c + &quot;B&quot;, 
VGA_Order_Status__c + &quot;A&quot;)))))</formula>
        <name>Populate External order status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Order_Status</fullName>
        <field>Previous_order_status__c</field>
        <formula>PRIORVALUE(VGA_Order_Status__c)</formula>
        <name>Update Previous Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Previous_Order_Status_to_Blank</fullName>
        <field>Previous_order_status__c</field>
        <name>Update Previous Order Status to Blank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_Populate_External_Id</fullName>
        <field>VGA_External_Id__c</field>
        <formula>VGA_Commission_Number__c  &amp;  VGA_Model_Code__c &amp; VGA_VIN__c</formula>
        <name>Populate External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate External Id</fullName>
        <actions>
            <name>VGA_Populate_External_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>VGA_Customer_Order_Details__c.VGA_Commission_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>VGA_Customer_Order_Details__c.VGA_Model_Code__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>VGA_Customer_Order_Details__c.VGA_VIN__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate External order status field</fullName>
        <actions>
            <name>Populate_External_order_status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous Order Status</fullName>
        <actions>
            <name>Update_Previous_Order_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is use to update Previous_order_status__c</description>
        <formula>AND( ISCHANGED(VGA_Order_Status__c), NOT(ISBLANK(VGA_Customer__c)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Previous Order Status to Blank</fullName>
        <actions>
            <name>Update_Previous_Order_Status_to_Blank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is use to update Previous_order_status__c to blank</description>
        <formula>AND(  ISCHANGED(VGA_Customer__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
