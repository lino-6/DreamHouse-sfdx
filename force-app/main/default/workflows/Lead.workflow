<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_to_Skdoa_CBM</fullName>
        <description>Email to Skdoa CBM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>crm@volkswagen.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Skoda_Email_Templates/Skoda_fleet_customer_notification</template>
    </alerts>
    <alerts>
        <fullName>Volkswagen_Fleet_Lead_Notification_Email</fullName>
        <description>Volkswagen Fleet Lead Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>crm@volkswagen.com.au</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Volkswagen_Email_Template/Volkswagen_fleet_customer_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Lead_Record_Type_skoda</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Skoda_Individual_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Lead Record Type skoda</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Type</fullName>
        <field>VGA_Request_Type__c</field>
        <literalValue>Callback Request</literalValue>
        <name>Update Lead Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Checkbox</fullName>
        <field>VGA_Survey_Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update Survey Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Survey_Send_Date</fullName>
        <field>VGA_Survey_Email_Sent_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Survey Send Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_lead_record_type_fleet</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Skoda_Fleet_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update lead record type fleet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_owner</fullName>
        <description>Populate Lance as Skoda fleet owner.</description>
        <field>OwnerId</field>
        <lookupValue>augustine.alexander1@katzion.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Update the Skoda fleet owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_record_type_to_fleet</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Volkswagen_Fleet_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update the record type to fleet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VGA_update_Record_Type_Id_to_VW</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Volkswagen_Individual_Customer</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>update Record Type Id to VW</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Send_Survey_to_Lead_Email</fullName>
        <apiVersion>41.0</apiVersion>
        <endpointUrl>http://test</endpointUrl>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MobilePhone</fields>
        <fields>Status</fields>
        <fields>VGA_Assign_Date__c</fields>
        <fields>VGA_Assignment_Done__c</fields>
        <fields>VGA_Brand__c</fields>
        <fields>VGA_Created_Date__c</fields>
        <fields>VGA_Customer_Account__c</fields>
        <fields>VGA_Dealer_Address__c</fields>
        <fields>VGA_Dealer_Code__c</fields>
        <fields>VGA_Dealer_Email_survey__c</fields>
        <fields>VGA_Dealer_Region_Code_Survey__c</fields>
        <fields>VGA_Model_of_Interest__c</fields>
        <fields>VGA_Outcome__c</fields>
        <fields>VGA_Request_Type__c</fields>
        <fields>VGA_Status__c</fields>
        <fields>VGA_Sub_Brand__c</fields>
        <fields>VGA_Survey_Dealer_Name__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>suraj1@saasfocus.com</integrationUser>
        <name>Send Survey to Lead Email</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>Skoda_Survey_to_Lead_Email</fullName>
        <apiVersion>18.0</apiVersion>
        <endpointUrl>https://test</endpointUrl>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>LastName</fields>
        <fields>MobilePhone</fields>
        <fields>Status</fields>
        <fields>VGA_Assign_Date__c</fields>
        <fields>VGA_Assignment_Done__c</fields>
        <fields>VGA_Brand__c</fields>
        <fields>VGA_Created_Date__c</fields>
        <fields>VGA_Customer_Account__c</fields>
        <fields>VGA_Dealer_Address__c</fields>
        <fields>VGA_Dealer_Code__c</fields>
        <fields>VGA_Dealer_Email_survey__c</fields>
        <fields>VGA_Dealer_Region_Code_Survey__c</fields>
        <fields>VGA_Model_of_Interest__c</fields>
        <fields>VGA_Outcome__c</fields>
        <fields>VGA_Request_Type__c</fields>
        <fields>VGA_Status__c</fields>
        <fields>VGA_Survey_Dealer_Name__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>suraj1@saasfocus.com</integrationUser>
        <name>Skoda Survey to Lead Email</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>Lead Survey email</fullName>
        <active>true</active>
        <formula>AND ( OR ( VGA_Customer_Account__r.VGA_News_Innovations_Flag__c=TRUE, VGA_Customer_Account__r.VGA_Vehicle_Launches_Flag__c=TRUE, VGA_Customer_Account__r.VGA_Events_opt_out__c=TRUE, VGA_Customer_Account__r.VGA_Sponsorship_out__c=TRUE, VGA_Customer_Account__r.VGA_Promotions_Flag__c=TRUE, VGA_Customer_Account__r.VGA_Welcome_experience_On_boarding__c=TRUE), OR(ISPICKVAL(VGA_Request_Type__c,&apos;Test Drive&apos;), ISPICKVAL(VGA_Request_Type__c,&apos;Callback Request&apos;)), VGA_Survey_Email_Sent__c =FALSE, RecordType.DeveloperName=&apos;Volkswagen_Individual_Customer&apos;, ISPICKVAL(VGA_Brand__c,&apos;Volkswagen&apos;), VGA_Dealer_Code__c !=&apos;9998&apos; , VGA_Dealer_Code__c != &apos;9997&apos; )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Survey_Checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Survey_Send_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Send_Survey_to_Lead_Email</name>
                <type>OutboundMessage</type>
            </actions>
            <offsetFromField>Lead.VGA_Assigned_to_Dealership__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Lead Survey email-Skoda</fullName>
        <active>true</active>
        <description>Skoda Leads survey</description>
        <formula>AND ( OR  ( VGA_Customer_Account__r.VGA_News_Innovations_Flag__c=TRUE, VGA_Customer_Account__r.VGA_Vehicle_Launches_Flag__c=TRUE,  VGA_Customer_Account__r.VGA_Events_opt_out__c=TRUE, VGA_Customer_Account__r.VGA_Sponsorship_out__c=TRUE, VGA_Customer_Account__r.VGA_Promotions_Flag__c=TRUE, VGA_Customer_Account__r.VGA_Welcome_experience_On_boarding__c=TRUE),  OR(ISPICKVAL(VGA_Request_Type__c,&apos;Test Drive&apos;), ISPICKVAL(VGA_Request_Type__c,&apos;Callback Request&apos;) ),VGA_Survey_Email_Sent__c =FALSE,  RecordType.DeveloperName=&apos;Skoda_Individual_Customer&apos;, ISPICKVAL(VGA_Brand__c,&apos;Skoda&apos;),VGA_Dealer_Code__c != &apos;9998&apos; , VGA_Dealer_Code__c != &apos;9997&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Survey_Checkbox</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Survey_Send_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Skoda_Survey_to_Lead_Email</name>
                <type>OutboundMessage</type>
            </actions>
            <offsetFromField>Lead.VGA_Assigned_to_Dealership__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for VW</fullName>
        <actions>
            <name>VGA_update_Record_Type_Id_to_VW</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VGA_Brand__c</field>
            <operation>equals</operation>
            <value>Volkswagen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>integration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Request_Type__c</field>
            <operation>notEqual</operation>
            <value>Fleet Lead</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for VW Fleet</fullName>
        <actions>
            <name>Update_the_record_type_to_fleet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5 OR 6 OR 7 OR (8 AND 9 AND 10))</booleanFilter>
        <criteriaItems>
            <field>Lead.VGA_Brand__c</field>
            <operation>equals</operation>
            <value>Volkswagen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>integration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Request_Type__c</field>
            <operation>equals</operation>
            <value>Fleet Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Volkswagen_Fleet_Size__c</field>
            <operation>equals</operation>
            <value>30-50</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Volkswagen_Fleet_Size__c</field>
            <operation>equals</operation>
            <value>51-100</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Volkswagen_Fleet_Size__c</field>
            <operation>equals</operation>
            <value>101-200</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Volkswagen_Fleet_Size__c</field>
            <operation>equals</operation>
            <value>201 or more</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Volkswagen_Fleet_Size__c</field>
            <operation>equals</operation>
            <value>0-29</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Sub_Brand__c</field>
            <operation>notEqual</operation>
            <value>Passenger Vehicles (PV)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Sub_Brand__c</field>
            <operation>notEqual</operation>
            <value>Commercial Vehicles (CV)</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for skoda</fullName>
        <actions>
            <name>Update_Lead_Record_Type_skoda</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VGA_Brand__c</field>
            <operation>equals</operation>
            <value>Skoda</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>integration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Request_Type__c</field>
            <operation>notEqual</operation>
            <value>Fleet Lead</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Lead Record Type for skoda fleet</fullName>
        <actions>
            <name>Email_to_Skdoa_CBM</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_lead_record_type_fleet</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_the_owner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VGA_Brand__c</field>
            <operation>equals</operation>
            <value>Skoda</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>integration User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Request_Type__c</field>
            <operation>equals</operation>
            <value>Fleet Lead</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Request Type on Lead</fullName>
        <actions>
            <name>Update_Lead_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.VGA_Request_Type__c</field>
            <operation>equals</operation>
            <value>Registration of Interest</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.VGA_Follow_up_Lead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
