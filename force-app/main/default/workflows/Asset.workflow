<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Asset_name_field</fullName>
        <description>Populate VIN</description>
        <field>Name</field>
        <formula>VGA_VIN__c</formula>
        <name>Update Asset name field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate VIN on Asset name field</fullName>
        <actions>
            <name>Update_Asset_name_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Asset.VGA_VIN__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>VIN is a unique case sensitive field. Populating the value from VIN on the Asset name standard field.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
