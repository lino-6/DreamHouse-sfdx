<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>VGA_Populate_External_Id</fullName>
        <field>VGA_External_Id__c</field>
        <formula>VGA_Component_Code__c &amp;  VGA_Component_Type__c &amp;  VGA_VIN__r.VGA_VIN__c</formula>
        <name>Populate External Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
